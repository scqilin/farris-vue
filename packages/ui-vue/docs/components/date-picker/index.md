# Date Picker 选择日期组件

Date Picker 组件用来选择日期。

## 基本用法

:::vdemo

```vue
{demos/date-picker/basic.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
