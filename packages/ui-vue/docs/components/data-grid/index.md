# Data Grid 表格

Data Grid 是展示数据的表格组件，提供分页展示数据，再单元格中编辑数据的功能。

## 基本用法

:::vdemo

```vue
{demos/data-grid/basic.vue}

```

:::

## 插槽

::: tip
暂无内容
:::
