# Time Picker 选择时间组件

Time Picker 组件用来选择时间。

## 基本用法

:::vdemo

```vue
{demos/time-picker/basic.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
