# Progress 进度条

Progress 进度条组件用来展示进度百分比。

## 基本用法

:::vdemo

```vue
{demos/progress/basic.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
