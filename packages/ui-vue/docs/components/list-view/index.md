# List View 列表

List View 列表组件用来以摘要信息的形式展示数据。

## 基本用法

:::vdemo

```vue
{demos/list-view/basic.vue}
```

:::

## 摘要视图

:::vdemo

```vue
{demos/list-view/common.vue}
```

:::

## 卡片视图

:::vdemo

```vue
{demos/list-view/card.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
