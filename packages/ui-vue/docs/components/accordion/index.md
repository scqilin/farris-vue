# Accordion 手风琴

Accordion 组件为开发者提供了一种可收折的导航面板。

## 基本用法

:::demo

```vue
{demos/accordion/basic.vue}
```

:::

## 属性

| 属性名      | 类型       | 默认值 | 说明             |
| :---------- | :--------- | :----- | :--------------- |
| customClass | `string[]` | []     | 组件自定义样式   |
| enableFold  | `boolean`  | true   | 是否允许收折     |
| expanded    | `boolean`  | false  | 是否展开所有面板 |
| height      | `number`   | --     | 设置组件的高度   |
| width       | `number`   | --     | 设置组件的宽度   |

## 插槽

::: tip
暂无内容
:::
