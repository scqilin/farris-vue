# Uploader 附件上传组件

Uploader 选择文件上传，支持批量选择文件，通过列表方式展示已选择的附件、附件的进度。

## 基本用法

:::vdemo

```vue
{demos/uploader/basic.vue}

```

:::

## 插槽

::: tip
暂无内容
:::
