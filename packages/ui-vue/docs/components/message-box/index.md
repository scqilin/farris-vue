# Message Box 消息弹窗

Message Box 消息弹窗采用模态对话框形式展示消息，用于提示消息、确认消息和提交内容。

## 基本用法

:::vdemo

```vue
{demos/message-box/types.vue}
```

:::

## 询问

:::vdemo

```vue
{demos/message-box/question.vue}
```

:::

## 异常

:::vdemo

```vue
{demos/message-box/errors.vue}
```

:::

## 提交内容

:::vdemo

```vue
{demos/message-box/prompt.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
