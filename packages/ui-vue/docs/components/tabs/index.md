# Tabs 标签页

Tabs 组件为开发者提供具有页签导航的布局容器。

## 基本用法

:::demo

```vue
{demos/tabs/basic.vue}
```

:::

## 禁用

:::demo

```vue
{demos/tabs/disable.vue}
```

:::

## 隐藏

:::demo

```vue
{demos/tabs/hide.vue}
```

:::

## 关闭按钮

:::demo

```vue
{demos/tabs/close.vue}
```

:::

## 类型

```typescript
export type TabType = 'fill' | 'pills' | 'default';
export type TabPosition = 'left' | 'right' | 'top' | 'bottom';
```

## 属性

| 属性名           | 类型                    | 默认值    | 说明                     |
| :--------------- | :---------------------- | :-------- | :----------------------- |
| tabType          | `string as TabType`     | 'default' | 标签页显示样式           |
| autoTitleWidth   | `boolean`               | false     | 是否自动调整标题宽度     |
| titleLength      | `number`                | 7         | 标题宽度                 |
| position         | `string as TabPosition` | 'top'     | 显示页签的位置           |
| showDropDwon     | `boolean`               | false     | 是否显示页签导航下拉按钮 |
| showTooltips     | `boolean`               | false     | 是否显示标题提示信息     |
| scrollStep       | `number`                | 1         | 滚动鼠标切换页签的步长   |
| autoResize       | `boolean`               | false     | 是否允许自动调整页签高度 |
| selectedTab      | `string`                | --        | 指定选中的页签           |
| width            | `number`                | --        | 组件宽度                 |
| height           | `number`                | --        | 组件高度                 |
| searchBoxVisible | `boolean`               | false     | 是否显示页签搜索框       |
| titleWidth       | `number`                | --        | 标题宽度                 |
| customClass      | `string`                | --        | 标签自定义样式           |
| activeId         | `string`                | --        | 被激活的页签标识         |

## 插槽

::: tip
暂无内容
:::
