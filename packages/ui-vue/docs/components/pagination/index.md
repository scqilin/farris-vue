# Pagination 分页条

Pagination 组件用来展示分页信息。

## 基本用法

:::vdemo

```vue
{demos/pagination/basic.vue}
```

:::

## 精简模式

:::vdemo

```vue
{demos/pagination/simple.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
