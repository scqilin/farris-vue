# Combo List 选择输入框

Combo List 组件提供下拉框形式选择枚举数据。

## 基本用法

:::demo

```vue
{demos/combo-list/basic.vue}
```

:::

## 属性

| 属性名    | 类型      | 默认值   | 说明     |
| :-----   | :-------  | :-----  | :------- |
| id       | `string`  |  | 组件标识  |
| editable | `boolean` | `false` | 是否可编辑|
| disabled | `boolean` | `false` | 是否禁用  |
| readonly | `boolean` | `false` | 是否只读      |
| placeholder | `string` |  | 占位符 | 
| forcePlaceholder | `boolean` | `false` | 可选，是否强制显示占位符 |
| enableClear | `boolean` | `true` | 可选，是否启用清空 |
| enableTitle | `boolean` | `true` | 可选，鼠标悬停时是否显示控件值 |
| data | `Options` | `[]` | 数据源 |
| idField | `string` | `id` | 可选，数据源id字段 |
| valueField | `string` | `id` | 可选，数据源值字段 |
| textField | `string` | `label` | 可选，数据源显示字段 |
| multiSelect | `boolean` | `false` | 可选，是否支持多选 |
| remote | `object` | `null` | 可选，远端数据源信息 |
| maxHeight | `number` | `350` | 可选，下拉面板最大高度 |
| separator | `string` | `,` | 可选，多选时值分隔符 |
| dropDownIcon | `string` |  | 可选，下拉按钮图标 |
| enableSearch | `boolean` | `false` | 可选，是否启用搜索 |
| tabIndex | `number` | `-1` | tab index |


## 插槽

::: tip
暂无内容
:::
