# Tree View 树列表组件

Tree View 组件以树形式展现层级结构数据。

## 基本用法

:::vdemo

```vue
{demos/tree-view/basic.vue}

```

:::

## 插槽

::: tip
暂无内容
:::
