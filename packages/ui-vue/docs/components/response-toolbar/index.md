# Response Toolbar 响应式工具条

Response Toolbar 是一个可以根据屏幕尺寸自定义调整显示按钮个数的工具条组件。

## 基本用法

:::vdemo

```vue
{demos/response-toolbar/basic.vue}
```

:::


## 插槽

::: tip
暂无内容
:::
