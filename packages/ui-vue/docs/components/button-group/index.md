# Button Group 按钮组

Button Group 组件为不同使用场景提供了一组按钮展示样式。

## 基本用法

:::vdemo

```vue
{demos/button-group/basic.vue}

```

:::


## 类型

```typescript
type PlacementDirection = 'top' | 'top-left' | 'top-right' | 'left' | 'left-top' | 'left-bottom' | 'bottom' | 'bottom-left' | 'bottom-right' | 'right' | 'right-top' | 'right-bottom';
```

## 属性

| 属性名    | 类型                           | 默认值   | 说明           |
| :-------- | :----------------------------- | :------- | :------------- |
| data      | `array`                        | --       | 按钮信息       |
| count     | `number`                       | 2        | 显示的按钮数量 |
| placement | `string as PlacementDirection` | 'bottom' | 按钮展示位置   |

## 事件

| 事件名      | 类型                    | 说明                                           |
| :---------- | :---------------------- | :--------------------------------------------- |
| changeState | `EventEmitter<boolean>` | 下拉框展开或者收起的回调，参数是下拉框是否展开 |
| change      | `EventEmitter<string>`  | 触发按钮的回调，参数是触发按钮的id             |

## 插槽

::: tip
暂无内容
:::
