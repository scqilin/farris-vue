import { defineConfig } from 'vite';
import vueJsx from '@vitejs/plugin-vue-jsx';
import svgLoader from 'vite-svg-loader';
import { MarkdownTransform } from './.vitepress/plugins/markdown-transform';

export default defineConfig({
    plugins: [vueJsx({}), svgLoader(), MarkdownTransform()],
    server: {
        fs: {
            strict: false
        }
    }
});
