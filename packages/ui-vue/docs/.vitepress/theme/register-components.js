// import Demo from 'vitepress-theme-demoblock/components/Demo.vue';
// import DemoBlock from 'vitepress-theme-demoblock/components/DemoBlock.vue';
// export function registerComponents(app) {
//     app.component('Demo', Demo);
//     app.component('DemoBlock', DemoBlock);
// }

import FExample from '../farris-theme/example/FExample.vue';
import VPDemo from '../farris-theme/components/VPDemo.vue';

export function registerComponents(app) {
    app.component('Demo', FExample).component('VPDemo', VPDemo);
}
