import Farris from '../../../components';
import FarrisTheme from '../farris-theme';
import { registerComponents } from './register-components.js';
import { insertBaiduScript } from './insert-baidu-script';

// import '../../../public/assets/farris-all.css';

export default {
    ...FarrisTheme,
    enhanceApp({ app }) {
        app.use(Farris);
        registerComponents(app);
        insertBaiduScript();
    }
};
