const sidebar = [
    {
        text: '介绍',
        items: [{ text: '快速开始', link: '/guide/quick-start/' }]
    },
    {
        text: '通用',
        items: [
            { text: 'Button 按钮', link: '/components/button/' },
            { text: 'Button Group 按钮组', link: '/components/button-group/' },
            { text: 'Icon 图标', link: '/components/icon/' },
            { text: 'Response Toolbar 响应式工具条', link: '/components/response-toolbar/' }
        ]
    },
    {
        text: '导航',
        items: [
            { text: 'Accordion 手风琴', link: '/components/accordion/' },
            { text: 'Step 步骤条', link: '/components/step/' },
            { text: 'Pagination 分页条', link: '/components/pagination/' }
        ]
    },
    {
        text: '布局',
        items: [
            {
                text: 'Section 面板',
                link: '/components/section/'
            },
            { text: 'Tabs 标签页', link: '/components/tabs/' }
        ]
    },
    {
        text: '录入数据',
        items: [
            { text: 'Button Edit 按钮输入框', link: '/components/button-edit/' },
            { text: 'Combo List 选择输入框', link: '/components/combo-list/' },
            { text: 'Date Picker 日期选择框', link: '/components/date-picker/' },
            { text: 'Radio Group 单选组', link: '/components/radio-group/' },
            { text: 'Swtich 开关', link: '/components/switch/' },
            { text: 'Text 静态文本', link: '/components/text/' },
            { text: 'Time Picker 时间选择框', link: '/components/time-picker/' },
            { text: 'Tags 标签', link: '/components/tags/' }
        ]
    },
    {
        text: '展示数据',
        items: [
            { text: 'Avatar 头像', link: '/components/avatar/' },
            { text: 'Calendar 日历', link: 'components/calendar/' },
            { text: 'Data Grid 表格', link: '/components/data-grid/' },
            { text: 'List View 列表', link: '/components/list-view/' },
            { text: 'Modal 模态窗口', link: '/components/modal/' },
            { text: 'Progress 进度条', link: '/components/progress/' },
            { text: 'Tree View 树列表', link: '/components/tree-view/' }
        ]
    },
    {
        text: '反馈',
        items: [
            { text: 'Message Box 消息弹窗', link: '/components/message-box/' },
            { text: 'Notify 通知消息', link: '/components/notify/' },
            { text: 'Popover 气泡提示', link: '/components/popover/' },
            { text: 'Tooltip 提示信息', link: '/components/tooltip/' }
        ]
    },
    {
        text: '操作',
        items: [
            { text: 'Uploader 附件上传', link: '/components/uploader/' }
        ]
    }
];

export default sidebar;
