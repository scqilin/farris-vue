import { ComputedRef, Ref } from "vue";

export interface UseCopy {
    copyText: ComputedRef<any>;
    onCopy: () => Promise<void>;
}

export interface UseCollapse {
    controlText: ComputedRef<any>;
    isExpanded: Ref<boolean>;
    onClickControl: () => void;
}