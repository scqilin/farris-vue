import { createApp } from 'vue';
import './style.css';
import App from './app.vue';
import Farris from '../components';

const app = createApp(App);
app.use(Farris).mount('#app');
