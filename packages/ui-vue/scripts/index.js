#!/usr/bin/env node
const { Command, Option } = require('commander');
const { build } = require('./commands/build');

const program = new Command();

program.command('build').description('构建 Farris UI Vue 组件库').action(build);

program.parse();
