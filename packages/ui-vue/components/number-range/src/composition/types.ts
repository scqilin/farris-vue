/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BigNumber } from 'bignumber.js';
import { ComputedRef, Ref } from 'vue';

export interface NumberFormatOptions {
    /** 前置符号 */
    prefix: string;
    /** 后缀 */
    suffix: string;
    /** 小数点 */
    decimalSeparator: string;
    /** 千分位符号 */
    groupSeparator: string;
    /** 千分位分组 */
    groupSize: number;
}

export interface UseFormat {
    cleanFormat: (val: any) => string;

    format: (val: any) => string;
}

export interface UseNumber {

    getRealValue: (value: any) => string | number;

    getValidNumberObject: (numberObject: BigNumber) => BigNumber;

    isEmpty: (value: string | number | undefined) => boolean;

    precision: ComputedRef<number>;
}

export interface UseSpinner {
    canDownward: () => boolean;

    canUpward: () => boolean;

    downward: () => void;

    onClickDownButton: ($event: MouseEvent) => void;

    onClickUpButton: ($event: MouseEvent) => void;

    upward: () => void;
}

export interface UseTextBox {

    onBlurTextBox: ($event: Event) => void;

    onFocusTextBox: ($event: Event) => void;

    onInput: ($event: Event) => void;

    onKeyDown: ($event: KeyboardEvent) => void;

    textBoxValue: ComputedRef<number | string>;
}
