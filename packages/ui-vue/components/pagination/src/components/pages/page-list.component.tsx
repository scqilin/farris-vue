/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, Ref } from 'vue';

export default function (currentPage: Ref<number>, currentPageSize: Ref<number>, pageList: Ref<number[]>, totalItems: Ref<number>) {
    const shouldShowPagePanel = ref(false);
    const prefixPageSize = ref('显示');
    const suffixPageSize = ref('条');

    const pageListClass = computed(() => {
        const classObject = {
            dropup: true,
            'dropdown-right': true,
            'pg-pagelist': true,
            'pagelist-disabled': totalItems.value === 0,
            show: shouldShowPagePanel.value
        } as Record<string, boolean>;
        return classObject;
    });

    const dropdownPageMenuClass = computed(() => {
        const classObject = {
            'dropdown-menu': true,
            show: shouldShowPagePanel.value
        } as Record<string, boolean>;
        return classObject;
    });

    const dropdownPageMenuItemClass = (pageSize: number) => {
        const classObject = {
            'dropdown-item': true,
            active: currentPageSize.value === pageSize
        } as Record<string, boolean>;
        return classObject;
    };

    function onMouseEnterPageList($event: MouseEvent) {
        shouldShowPagePanel.value = true;
    }

    function onMouseLeavePageList($event: MouseEvent) {
        shouldShowPagePanel.value = false;
    }

    function recalculatePageIndexByPageSize(currentPageIndex: number, originalPageSize: number, newPageSize: number) {
        const currentVisibleRecordsCount = currentPageIndex * originalPageSize;
        const currentTopVisibleRecordIndex = currentVisibleRecordsCount - originalPageSize + 1;
        const latestPageIndex = Math.ceil(currentTopVisibleRecordIndex / newPageSize);
        return latestPageIndex;
    }

    function onClickToChangePageSize($event: MouseEvent, pageSize: number) {
        currentPage.value = recalculatePageIndexByPageSize(currentPage.value, currentPageSize.value, pageSize);
        currentPageSize.value = pageSize;
        shouldShowPagePanel.value = false;
    }

    function renderPageList() {
        return (
            <li class="pagination-pagelist">
                <div
                    class={pageListClass.value}
                    onMouseenter={(payload: MouseEvent) => onMouseEnterPageList(payload)}
                    onMouseleave={(payload: MouseEvent) => onMouseLeavePageList(payload)}>
                    <div class="pg-pagelist-info">
                        <span class="pagelist-text">{prefixPageSize.value}</span>
                        <b class="cur-pagesize">{currentPageSize.value}</b>
                        <span class="pagelist-text">{suffixPageSize.value}</span>
                        <i class="f-icon f-icon-dropdown"></i>
                    </div>
                    <div class={dropdownPageMenuClass.value} style="margin-bottom: -1px;">
                        {pageList.value.map((pageSize: number) => {
                            return (
                                <li
                                    class={dropdownPageMenuItemClass(pageSize)}
                                    onClick={(payload: MouseEvent) => onClickToChangePageSize(payload, pageSize)}>
                                    <span>{pageSize}</span>
                                </li>
                            );
                        })}
                    </div>
                </div>
            </li>
        );
    }

    return { renderPageList };
}
