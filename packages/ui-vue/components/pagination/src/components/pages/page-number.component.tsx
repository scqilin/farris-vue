/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref } from 'vue';
import { PageInfo } from '../../composition/types';

export default function (currentPage: Ref<number>, pages: Ref<PageInfo[]>) {
    const pageItemClass = (page: any) => {
        const classObject = {
            'page-item': true,
            current: currentPage.value === page.value,
            ellipsis: page.label === '...'
        } as Record<string, boolean>;
        return classObject;
    };

    const pageItemSeekClass = (page: any) => {
        const classObject = {
            'f-icon': true,
            'f-icon-arrow-seek-left': page.value < currentPage.value,
            'f-icon-arrow-seek-right': page.value > currentPage.value
        } as Record<string, boolean>;
        return classObject;
    };

    function onClickToChangeCurrentPage($event: MouseEvent, pageIndex: number) {
        currentPage.value = pageIndex;
    }

    function renderPageNumbers() {
        return pages.value.map((page: any) => {
            return (
                <li class={pageItemClass(page)}>
                    {currentPage.value !== page.value && (
                        <a
                            class="page-link"
                            tabindex="0"
                            onClick={(payload: MouseEvent) => onClickToChangeCurrentPage(payload, page.value)}>
                            <span class="page-link-label">{page.label}</span>
                            {page.label === '...' && <i class={pageItemSeekClass(page)}></i>}
                        </a>
                    )}
                    {currentPage.value === page.value && <span class="page-link">{page.label}</span>}
                </li>
            );
        });
    }

    return { renderPageNumbers };
}
