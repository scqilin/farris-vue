/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, ref, Ref } from 'vue';

export default function (currentPage: Ref<number>, lastPage: ComputedRef<number>) {
    const gotoPrefix = ref('跳转至');
    const gotoSuffix = ref('');
    const pageNumber = ref(currentPage.value);

    function applyCurrentPage() {
        currentPage.value = pageNumber.value;
    }

    function goto($event: KeyboardEvent) {
        if ($event.key === 'Enter') {
            applyCurrentPage();
        }
    }

    function toChangePageNumber($event: Event) {
        pageNumber.value = ($event.target as any).valueAsNumber;
    }

    function renderGotoButton() {
        return (
            <li class="page-goto-input" style="padding-left: 10px; white-space: nowrap;">
                <span class="pg-message-text">{gotoPrefix.value}</span>
                <input
                    class="form-control farris-gotopagenumber"
                    title="page-number"
                    value={currentPage.value}
                    type="number"
                    min="1"
                    max={lastPage.value}
                    style="display: inline-block;margin-left:3px;"
                    onChange={() => applyCurrentPage()}
                    onInput={(payload: Event) => toChangePageNumber(payload)}
                    onKeyup={(payload: KeyboardEvent) => goto(payload)}></input>
                {gotoSuffix.value}
            </li>
        );
    }

    return { renderGotoButton };
}
