/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export type PaginationMode = 'default' | 'simple';

export const paginationProps = {
    currentPage: { type: Number, default: 1 },
    mode: { type: String as PropType<PaginationMode>, default: 'default' },
    pageSize: { type: Number, default: 20 },
    totalItems: { type: Number, default: 0 }
};

export type PaginationPropsType = ExtractPropTypes<typeof paginationProps>;
