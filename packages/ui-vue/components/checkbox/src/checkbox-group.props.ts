/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { Checkbox } from './composition/types';
import { EditorConfig } from '../../dynamic-form/src/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import checkboxSchema from './schema/check-box.schema.json';

export const checkboxGroupProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 禁用组件，不允许切换单选值
     */
    disable: { type: Boolean, default: false },
    /**
     * 单选组枚举数组
     */
    enumData: { type: Array<Checkbox>, default: [] },
    /**
     * 组件是否水平排列
     */
    horizontal: { type: Boolean, default: false },
    /**
     * 值类型是否为字符串
     */
    isStringValue: { type: Boolean, default: true },
    /**
     * 异步获取枚举数组方法
     */
    // loadData: () => Observable < { data: Array<Checkbox> } >
    loadData: { type: Function },
    /**
     * 组件值，字符串或者数组
     */
    modelValue: [String, Array<string>],
    /**
     * 组件名称
     */
    name: { type: String, default: '' },
    /**
     * 分隔符，默认逗号
     */
    separator: { type: String, default: ',' },
    /**
     * 输入框Tab键索引
     */
    tabIndex: { type: Number, default: 0 },
    /**
     * 枚举数组中展示文本的key值。
     */
    textField: { type: String, default: 'name' },
    /**
     * 枚举数组中枚举值的key值。
     */
    valueField: { type: String, default: 'value' }
} as Record<string, any>;

export type CheckboxGroupProps = ExtractPropTypes<typeof checkboxGroupProps>;

export const propsResolver = createPropsResolver<CheckboxGroupProps>(checkboxGroupProps, checkboxSchema, schemaMapper);
