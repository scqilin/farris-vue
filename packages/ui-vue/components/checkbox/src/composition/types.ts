/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, Ref } from 'vue';

export interface Checkbox {
    [key: string]: any;
    /**
     * 枚举值
     */
    value: any;
    /**
     * 枚举展示文本
     */
    name: string;
}

export interface ChangeCheckbox {
    enumData: Ref<Array<Checkbox>>;

    /**
     * 获取枚举值
     */
    getValue(item: Checkbox): any;
    /**
     * 获取枚举文本
     */
    getText(item: Checkbox): any;

    /**
     * 校验复选框是否为选中状态
     */
    checked(item: Checkbox): boolean;

    /**
     * 点击复选框事件
     */
    onClickCheckbox: (item: Checkbox, $event: Event) => void;
}
