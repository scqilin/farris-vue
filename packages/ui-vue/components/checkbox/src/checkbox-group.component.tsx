/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, watch } from 'vue';
import type { SetupContext } from 'vue';
import { CheckboxGroupProps, checkboxGroupProps } from './checkbox-group.props';
import { useCheckboxGroup } from './composition/use-checkbox-group';

export default defineComponent({
    name: 'FCheckboxGroup',
    props: checkboxGroupProps,
    emits: ['changeValue', 'update:modelValue']  as (string[] & ThisType<void>) | undefined,
    setup(props: CheckboxGroupProps, context: SetupContext) {
        const modelValue = ref(props.modelValue);
        const { enumData, onClickCheckbox, getValue, getText, checked } = useCheckboxGroup(props, context, modelValue);

        const horizontalClass = computed(() => ({
            'farris-checkradio-hor': props.horizontal
        }));

        watch(
            () => props.modelValue,
            (value: any) => {
                modelValue.value = value;
                context.emit('changeValue', modelValue.value);
            }
        );

        return () => {
            return (
                <div class={['farris-input-wrap', horizontalClass.value]}>
                    {enumData.value.map((item, index) => {
                        const id = 'checkbox_' + props.name + index;

                        return (
                            <div class="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    class="custom-control-input"
                                    name={props.name}
                                    id={id}
                                    value={getValue(item)}
                                    checked={checked(item)}
                                    disabled={props.disable}
                                    tabindex={props.tabIndex}
                                    onClick={(event: MouseEvent) => onClickCheckbox(item, event)}
                                />
                                <label class="custom-control-label" for={id} title={getText(item)}>
                                    {getText(item)}
                                </label>
                            </div>
                        );
                    })}
                </div>
            );
        };
    }
});
