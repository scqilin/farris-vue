import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import componentSchema from './schema/component.schema.json';

export const componentProps = {
    customClass: { type: String, default: '' },
    componentType: { type: String, default: '' }
} as Record<string, any>;

export type ComponentPropsType = ExtractPropTypes<typeof componentProps>;

export const propsResolver = createPropsResolver<ComponentPropsType>(componentProps, componentSchema, schemaMapper);
