import { SetupContext, defineComponent } from 'vue';
import { ComponentPropsType, componentProps } from './component.props';

export default defineComponent({
    name: 'FComponent',
    props: componentProps,
    emits: [],
    setup(props: ComponentPropsType, context: SetupContext) {
        return () => {
            return <div class={props.customClass}>{context.slots.default && context.slots.default()}</div>;
        };
    }
});
