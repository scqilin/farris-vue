import { SetupContext, defineComponent, inject, ref, onMounted } from 'vue';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { ComponentPropsType, componentProps } from '../component.props';
import { useDesignerRules } from './use-designer-rules';

export default defineComponent({
    name: 'FComponetDesign',
    props: componentProps,
    emits: [],
    setup(props: ComponentPropsType, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div ref={elementRef} class="drag-container" dragref={`${designItemContext.schema.id}-container`}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
