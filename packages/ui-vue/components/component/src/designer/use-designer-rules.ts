/* eslint-disable max-len */
/* eslint-disable complexity */
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";


export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

    let isInFixedContextRules = false;

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(): boolean {
        return false;
    }
    /**
     * 判断当前容器的上下文
     */
    function resolveComponentContext() {
        isInFixedContextRules = false;
        const component = schema;

        // 控件本身样式
        const componentClass = component.appearance && component.appearance.class || '';
        const componentClassList = componentClass.split(' ');

        // 子级节点
        const childContents = component.contents || [];
        const firstChildContent = childContents.length ? childContents[0] : null;
        const firstChildClass = firstChildContent && firstChildContent.appearance ? firstChildContent.appearance.class : '';
        const firstChildClassList = firstChildClass ? firstChildClass.split(' ') : [];

        // 父级节点
        const parent = parentSchema;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 卡片类组件
        if (componentClassList.includes('f-struct-wrapper') && component.componentType.includes('form') && firstChildContent && firstChildContent.type === 'section') {
            isInFixedContextRules = false;
            return;
        }
        // 列表类组件
        if (component.componentType == 'data-grid' && parent) {
            if (parent.type === 'content-container' && parentClassList.includes('f-page-main')) {
                isInFixedContextRules = true;
                return;
            }
            // 在分栏面板中的组件不支持移动
            if (parent.type === 'splitter-pane' && parentClassList.includes('f-page-content-nav')) {
                isInFixedContextRules = true;
                return;
            }
        }


    }
    function checkCanMoveComponent() {
        return !isInFixedContextRules;
    }
    function checkCanDeleteComponent() {
        return !isInFixedContextRules;
    }

    return { canAccepts, resolveComponentContext, checkCanMoveComponent, checkCanDeleteComponent };
}
