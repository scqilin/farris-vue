import { ExtractPropTypes } from 'vue';

export const hueProps = {
    color: { type: Object, default: '' },
    hue: { type: Object, default: '' },
    allowColorNull: { type: Boolean, default: false },
    onChange: { type: Function },
};
export type HueProps = ExtractPropTypes<typeof hueProps>;
