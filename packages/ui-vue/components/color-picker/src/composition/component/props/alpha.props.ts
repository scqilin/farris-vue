import { ExtractPropTypes } from 'vue';

export const alphaProps = {
    color: { type: Object, default: '' },

    randomId: { type: String, default: '' },

    allowColorNull: { type: Boolean, default: false }

};
export type AlphaProps = ExtractPropTypes<typeof alphaProps>;
