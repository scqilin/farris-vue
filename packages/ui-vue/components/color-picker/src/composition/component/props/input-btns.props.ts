import { ExtractPropTypes } from 'vue';

export const inputBtnsProps = {
    color: { type: Object, default: '' },

    hue: { type: Object, default: '' },

};
export type InputBtnsProps = ExtractPropTypes<typeof inputBtnsProps>;
