import { defineComponent, ref, SetupContext } from 'vue';
import { Color } from '../class/color.class';
import './scss/preset.scss';
import { presetProps, PresetProps } from './props/preset.props';

export default defineComponent({
    name: 'Preset',
    props: presetProps,
    emits: ['update:hue', 'update:color'] as string[] | undefined,
    setup(props: PresetProps, context: SetupContext) {
        const selectedColor = ref(new Color());
        const colorPresets = ref(props.colorPresets);
        const randomId = ref(props.randomId);

        /** 修改禁止点击状态 */
        function changeBorderAndButtonStates() {
            const isCommitBtnDisabled = document.getElementById(`farris-color-picker-plus-sure-${randomId.value}`) as HTMLElement;
            isCommitBtnDisabled.className = 'btn btn-secondary';
            const isCommitBtnNull = document.getElementById(`farris-color-picker-plus-input-${randomId.value}`) as HTMLElement;
            isCommitBtnNull.style.borderColor = '#dcdfe6';
        }
        /** 选择自定义颜色preset中的某项 */
        function onSelectionChange(color: Color): void {
            changeBorderAndButtonStates();
            const selectedRgbaColor = color.getRgba();
            const selectedHsvaColor = color.getHsva();
            const newColor = new Color().setRgba(
                selectedRgbaColor.red,
                selectedRgbaColor.green,
                selectedRgbaColor.blue,
                selectedRgbaColor.alpha
            );
            const hueColor = new Color().setHsva(selectedHsvaColor.hue);
            selectedColor.value = color;
            context.emit('update:hue', hueColor);
            context.emit('update:color', newColor);
        }

        function presetColorClass(color: Color) {
            const selectedRgba = selectedColor.value.getRgba().toString();
            const selectedHsva = selectedColor.value.getHsva().toString();
            const isSelected = color.getRgba().toString() === selectedRgba && color.getHsva().toString() === selectedHsva;
            const classObject = {
                'color-preset__color-selector': isSelected,
                selected: isSelected
            } as Record<string, boolean>;
            return classObject;
        }

        function renderPresetColor(color: Color, index: number) {
            return (
                <div class="color-preset__color-selector">
                    <div
                        key={index}
                        class={presetColorClass(color)}
                        style={{ backgroundColor: color.toRgbString() }}
                        onClick={() => onSelectionChange(color)}></div>
                </div>
            );
        }

        return () => (
            <div class="f-preset-component">
                <div class="color-preset__colors">
                    {colorPresets.value.map((color: any, index: any) => {
                        return renderPresetColor(color, index);
                    })}
                </div>
            </div>
        );
    }
});
