import { mount } from '@vue/test-utils';
import { ref } from 'vue';
import { FButtonGroup } from '..';

describe('button-group', () => {
    const mocks = {
        buttons: ref([
            { id: 'btn-add', text: '增加', type: 'primary', icon: 'f-icon-add' },
            { id: 'btn-edit', text: '编辑', type: 'secondary' },
            { id: 'btn-remove', text: '删除', type: 'danger', disabled: true },
            { id: 'btn-save', text: '保存', type: 'success' },
            { id: 'btn-close', text: '关闭', type: 'warning' }
        ])
    };

    beforeAll(() => {});

    describe('properties', () => {
        it('should show five buttons', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup count={mocks.buttons.value.length} data={mocks.buttons.value}></FButtonGroup>;
                    };
                }
            });
            expect(wrapper.find('div.f-btn-group').findAll('div.btn-group').length).toEqual(1);
            expect(wrapper.find('div.f-btn-group').find('div.f-btn-group-links').findAll('div').length).toEqual(5);
        });
        it('should show icon', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup count={mocks.buttons.value.length} data={mocks.buttons.value}></FButtonGroup>;
                    };
                }
            });
            expect(wrapper.find('div.f-btn-group').findAll('div.btn-group').length).toEqual(1);
            expect(
                wrapper
                    .find('div.f-btn-group')
                    .find('div.f-btn-group-links')
                    .findAll('div')[0]
                    .element.children[0].children[0].classList.contains('f-icon-add')
            ).toBeTruthy();
        });
        it('should disable remove button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup count={mocks.buttons.value.length} data={mocks.buttons.value}></FButtonGroup>;
                    };
                }
            });
            expect(
                wrapper
                    .find('div.f-btn-group')
                    .find('div.f-btn-group-links')
                    .findAll('div')[2]
                    .element.children[0].classList.contains('disabled')
            ).toBeTruthy();
        });
    });

    describe('render', () => {
        it('should work', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup data={mocks.buttons.value}></FButtonGroup>;
                    };
                }
            });
            expect(wrapper.find('div.f-btn-group').exists()).toBeTruthy();
            expect(wrapper.find('div.f-btn-group').find('div.btn-group').exists()).toBeTruthy();
            expect(wrapper.find('div.f-btn-group').findAll('div.btn-group').length).toEqual(2);
            expect(wrapper.find('div.f-btn-group').find('div.f-btn-group-links').findAll('div').length).toEqual(2);
            expect(wrapper.find('div.f-btn-group').find('div.f-btn-group-dropdown').find('button').exists()).toBeTruthy();
        });
        it('should show large button group', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup data={mocks.buttons.value} size="large"></FButtonGroup>;
                    };
                }
            });
            expect(wrapper.find('div.f-btn-group').find('div.btn-group').classes().includes('btn-group-lg')).toBeTruthy();
        });
        it('should show small button group by default', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup data={mocks.buttons.value}></FButtonGroup>;
                    };
                }
            });
            expect(wrapper.find('div.f-btn-group').find('div.btn-group').classes().includes('btn-group-sm')).toBeTruthy();
        });
        it('should show small button group', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup data={mocks.buttons.value} size="small"></FButtonGroup>;
                    };
                }
            });
            expect(wrapper.find('div.f-btn-group').find('div.btn-group').classes().includes('btn-group-sm')).toBeTruthy();
        });
        it('should have dropdown panel', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup data={mocks.buttons.value}></FButtonGroup>;
                    };
                }
            });
            expect(wrapper.find('.f-btn-group-dropdown-menu').exists()).toBeTruthy();
            expect(wrapper.find('.f-btn-group-dropdown-menu').element.childElementCount).toEqual(3);
        });
    });

    describe('methods', () => {});

    describe('events', () => {});

    describe('behaviors', () => {
        it('should show dropdown buttons', async () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButtonGroup data={mocks.buttons.value}></FButtonGroup>;
                    };
                }
            });
            const dropdownMenu = wrapper.find('.f-btn-group-dropdown-menu');
            expect(dropdownMenu.exists()).toBeTruthy();
            await wrapper.find('.f-btn-group-dropdown').find('button').trigger('click');
            expect(dropdownMenu.classes().includes('show')).toBeTruthy();
        });
    });
});
