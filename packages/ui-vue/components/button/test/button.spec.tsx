import { mount } from '@vue/test-utils';
import { FButton } from '..';

describe('f-button', () => {
    const mocks = {};

    beforeAll(() => {});

    describe('properties', () => {
        it('should been disabled', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton disabled={true}>it should be work</FButton>;
                    };
                }
            });
            expect(wrapper.find('button').element.hasAttribute('disabled')).toBeTruthy();
        });
    });

    describe('render', () => {
        it('should work', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton>it should be work</FButton>;
                    };
                }
            });
            expect(wrapper.find('button').exists()).toBeTruthy();
            expect(wrapper.find('button').text()).toEqual('it should be work');
            expect(wrapper.find('button').element.hasAttribute('disabled')).toBeFalsy();
        });
        it('should render primary button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton>primary button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-primary')).toBeTruthy();
        });
        it('should render danger button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="danger">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-danger')).toBeTruthy();
        });
        it('should render success button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="success">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-success')).toBeTruthy();
        });
        it('should render warning button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="warning">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-warning')).toBeTruthy();
        });
        it('should render secondary button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="secondary">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-secondary')).toBeTruthy();
        });
        it('should render link button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="link">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-link')).toBeTruthy();
        });
        it('should render small button be default', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton>default size</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-sm')).toBeTruthy();
        });
        it('should render large button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton size="large">large size</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-lg')).toBeTruthy();
        });
        it('should render small button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton size="small">small size</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-sm')).toBeTruthy();
        });
    });

    describe('methods', () => {});

    describe('events', () => {
        it('should be clicked', () => {
            let clicked = false;
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return (
                            <FButton
                                onClick={(event: MouseEvent) => {
                                    clicked = true;
                                }}>
                                small size
                            </FButton>
                        );
                    };
                }
            });
            wrapper.find('button').trigger('click');
            expect(clicked).toBeTruthy();
        });
        it('should not be clicked', () => {
            let clicked = false;
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return (
                            <FButton
                                disabled={true}
                                onClick={(event: MouseEvent) => {
                                    clicked = true;
                                }}>
                                small size
                            </FButton>
                        );
                    };
                }
            });
            wrapper.find('button').trigger('click');
            expect(clicked).toBeFalsy();
        });
    });

    describe('behaviors', () => {});
});
