/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

type PlacementDirection =
    | 'top'
    | 'top-left'
    | 'top-right'
    | 'left'
    | 'left-top'
    | 'left-bottom'
    | 'bottom'
    | 'bottom-left'
    | 'bottom-right'
    | 'right'
    | 'right-top'
    | 'right-bottom';

export const buttonGroupProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 横向纠正的参照
     */
    rectifyReferenceH: { type: Object },
    /**
     * 纵向纠正的参照
     */
    rectifyReferenceV: { type: Object },
    /**
     * 是否自动纠正位置
     */
    autoRectify: { type: Boolean, default: false },
    /**
     * 计算方向的真正placement
     */
    realPlacement: { type: String, default: 'bottom-right' },
    /**
     * 重新计算后的placement
     */
    rectifyPlacement: { type: String, default: 'bottom-right' },
    /**
     * 按钮信息
     */
    data: { type: Array },
    /**
     * 显示的按钮数量  默认为2
     */
    count: { type: Number, default: 2 },
    /**
     * 按钮大小
     */
    size: { type: String, default: 'small' },
    /**
     * 按钮样式
     */
    type: { type: String, default: 'primary' },
    /**
     * 按钮展示位置
     */
    placement: { type: String as PropType<PlacementDirection>, default: 'bottom' },
    /**
     * 下拉面板显示
     */
    showPanel: { type: Boolean, default: false },
    /**
     * 下拉面板显示标志
     */
    dpFlag: { type: Boolean, default: false }
};
export default buttonGroupProps;

export type ButtonGroupProps = ExtractPropTypes<typeof buttonGroupProps>;
