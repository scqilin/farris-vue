/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, ref } from 'vue';
import type { SetupContext } from 'vue';
import { buttonGroupProps, ButtonGroupProps } from './button-group.props';
import { useButtonGroup } from './composition/use-button-group';

export default defineComponent({
    name: 'FButtonGroup',
    props: buttonGroupProps,
    emits: ['click', 'changeState', 'change', 'clickMenuOut'] as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonGroupProps, context: SetupContext) {
        /* 显示出来的按钮组 */
        const flatButtons: any[] = (props.data && props.data.slice(0, props.count)) || [];
        const dpButtons = (props.data && props.data.slice(props.count)) || [];
        const $dpMenu = ref<any>();
        const $dpBtn = ref<any>();
        const { showPanel, clickEvent, toggle, getRealPlacement, bindMenuMouseenter, unbindMenuMouseleave, bindMenuMouseleave } =
            useButtonGroup(props, context, $dpMenu, $dpBtn);
        getRealPlacement(props.placement);

        return () => (
            <div class="f-btn-group">
                <div class={(props.size === 'large' ? 'btn-group-lg' : 'btn-group-sm') + ' btn-group f-btn-group-links'}>
                    {flatButtons.map((btn) => {
                        // (change)="changeEvent($event)"
                        return (
                            <div>
                                {btn.icon && (
                                    <button
                                        type="button"
                                        id={btn.id}
                                        title={btn.id}
                                        class={'btn btn-link btn-icontext' + (btn.disabled ? ' disabled' : '')}
                                        onClick={(event: MouseEvent) => toggle(event, btn)}>
                                        <i class={`f-icon ${btn.icon ? btn.icon : ''}`}></i>
                                    </button>
                                )}
                                {!btn.icon && (
                                    <button
                                        id={btn.id}
                                        class={
                                            'btn ' +
                                            (btn.type ? 'btn-' + btn.type : 'btn-link') +
                                            ' ' +
                                            (btn.type && btn.type !== 'link' ? 'f-btn-ml' : '') +
                                            (btn.disabled ? ' disabled' : '')
                                        }
                                        onClick={(event: MouseEvent) => toggle(event, btn)}>
                                        {btn?.text}
                                    </button>
                                )}
                            </div>
                        );
                    })}
                </div>
                {!dpButtons.length ? (
                    ''
                ) : (
                    <div class="btn-group f-btn-group-dropdown" onMouseleave={(event: MouseEvent) => unbindMenuMouseleave()}>
                        <button
                            title="button"
                            ref={$dpBtn}
                            type="button"
                            class="f-btn-dropdown"
                            onClick={(event: MouseEvent) => clickEvent(event)}>
                            <span class="f-icon f-icon-lookup"></span>
                        </button>
                        <div
                            ref={$dpMenu}
                            class={'dropdown-menu f-btn-group-dropdown-menu' + (showPanel.value ? ' show' : '')}
                            style="position:fixed;"
                            onMouseenter={(event: MouseEvent) => bindMenuMouseenter()}
                            onMouseleave={(event: MouseEvent) => bindMenuMouseleave()}>
                            {dpButtons.map((dpBtn: any) => {
                                return (
                                    <div>
                                        {
                                            <div>
                                                {dpBtn.divider && <div class="dropdown-divider"></div>}
                                                <li
                                                    id={dpBtn.id}
                                                    class={'dropdown-item' + (dpBtn.disabled ? ' disabled' : '')}
                                                    onClick={(event: MouseEvent) => toggle(event, dpBtn)}>
                                                    {dpBtn.icon && <i class={'f-icon dropdown-item-icon' + dpBtn.icon}></i>}
                                                    <span>{dpBtn?.text}</span>
                                                </li>
                                            </div>
                                        }
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )}
            </div>
        );
    }
});
