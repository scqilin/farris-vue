import { SetupContext, defineComponent } from "vue";
import { DynamicFormProps, dynamicFormProps } from "./dynamic-form.props";

export default defineComponent({
    name: 'FDynamicForm',
    props: dynamicFormProps,
    emits: [],
    setup(props: DynamicFormProps, context: SetupContext) {
        return () => {
            return <></>;
        };
    }
});
