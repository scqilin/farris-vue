import { ExtractPropTypes } from "vue";

export const dynamicFormLabelProps = {
    required: { type: Boolean, default: false },
    text: { type: String, default: '' },
    title: { type: String }
};

export type DynamicFormLabelProps = ExtractPropTypes<typeof dynamicFormLabelProps>;
