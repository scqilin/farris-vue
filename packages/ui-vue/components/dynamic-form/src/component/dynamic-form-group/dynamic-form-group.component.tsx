import { SetupContext, computed, defineComponent, inject, onMounted, ref, watch } from 'vue';
import { DynamicFormGroupProps, dynamicFormGroupProps } from './dynamic-form-group.props';
import Label from '../dynamic-form-label/dynamic-form-label.component';
import { useTypeResolver } from '../../composition/use-type-resolver';
import { DesignerItemContext } from '../../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FDynamicFormGroup',
    props: dynamicFormGroupProps,
    emits: [],
    setup(props: DynamicFormGroupProps, context: SetupContext) {
        const id = ref(props.id);
        const customClass = ref(props.customClass);
        const editor = ref(props.editor);
        const label = ref(props.label);
        const modelValue = ref(props.modelValue);
        const readonly = ref(props.readonly);
        const required = ref(props.required);
        const showLabel = ref(props.showLabel);
        const type = ref(props.type);
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        // const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext);
        componentInstance.value.canNested = false;
        componentInstance.value.canMove = true;
        componentInstance.value.canDelete = true;

        const { resolveEditorProps, resolveEditorType } = useTypeResolver();

        const conditionItemClass = computed(() => {
            const classObject = {
                'form-group': true,
                'farris-form-group': true,
                'common-group': true,
                'q-state-readonly': readonly.value
            } as Record<string, boolean>;
            return classObject;
        });

        function onChange(newValue: any) {
            modelValue.value = newValue;
            context.emit('update:modelValue', newValue);
        }

        const renderConditionEditor = computed(() => {
            const Component = resolveEditorType(editor.value.type);
            const editorProps = resolveEditorProps(editor.value.type, editor.value);
            return () => <Component {...editorProps} v-model={modelValue.value} onChange={onChange}></Component>;
        });

        function getDraggingDisplayText() {
            return label.value;
        }

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        watch(
            [
                () => props.id,
                () => props.customClass,
                () => props.editor,
                () => props.label,
                () => props.modelValue,
                () => props.readonly,
                () => props.required,
                () => props.showLabel
            ],
            ([newId, newCustomClass, newEditor, newLabel, newModelValue, newReadonly, newRequired, newShowLabel]) => {
                id.value = newId;
                customClass.value = newCustomClass;
                editor.value = newEditor;
                label.value = newLabel;
                modelValue.value = newModelValue;
                readonly.value = newReadonly;
                required.value = newRequired;
                showLabel.value = newShowLabel;
            }
        );

        return () => {
            return (
                <div id={`${id.value}-input-group`} ref={elementRef} class={customClass.value}>
                    <div class="farris-group-wrap">
                        <div class={conditionItemClass.value}>
                            {showLabel.value && (
                                <Label id={`${id.value}-lable`} required={required.value} text={label.value} title={label.value}></Label>
                            )}
                            <div class="farris-input-wrap">{renderConditionEditor.value()}</div>
                        </div>
                    </div>
                </div>
            );
        };
    }
});
