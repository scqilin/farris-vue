import { ExtractPropTypes, PropType } from 'vue';
import { EditorType, EditorConfig } from '../../types';
import { createPropsResolver } from '../../../../dynamic-resolver';
import { schemaMapper } from '../../schema/schema-mapper';
import formGroupSchema from '../../schema/form-group.schema.json';

export const dynamicFormGroupProps = {
    id: { type: String, default: '' },
    customClass: { type: String, default: '' },
    editor: { type: Object as PropType<EditorConfig>, default: {} },
    label: { type: String, default: '' },
    /** 组件值 */
    modelValue: { },
    readonly: { type: Boolean, default: false },
    required: { type: Boolean, default: false },
    showLabel: { type: Boolean, default: true },
    type: { type: String as PropType<EditorType>, default: 'input-group' }
} as Record<string, any>;

export type DynamicFormGroupProps = ExtractPropTypes<typeof dynamicFormGroupProps>;

export const formGroupPropsResolver = createPropsResolver<DynamicFormGroupProps>(dynamicFormGroupProps, formGroupSchema, schemaMapper);
