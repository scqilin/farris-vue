import FButtonEdit from '../../../button-edit/src/button-edit.component';
import FCheckbox from '../../../checkbox/src/checkbox-group.component';
import FComboList from '../../../combo-list/src/combo-list.component';
import FDatePicker from '../../../date-picker/src/date-picker.component';
import FInputGroup from '../../../input-group/src/input-group.component';
import FNumberRange from '../../../number-range/src/number-range.component';
import FNumberSpinner from '../../../number-spinner/src/number-spinner.component';
import FRadioGroup from '../../../radio-group/src/radio-group.component';
import { EditorType, EditorConfig } from "../types";
import { UseTypeResolver } from "./types";
import { resolveProps as resolveButtonEditProps } from '../../../button-edit';
import { resolveProps as resolveCheckboxProps } from '../../../checkbox';
import { resolveProps as resovleComboListProps } from '../../../combo-list';
import { resolveProps as resovleDatePickerProps } from '../../../date-picker';
import { resolveProps as resovleInputGroupProps } from '../../../input-group';
import { resolveProps as resolveNumberRangeProps } from '../../../number-range';
import { resolveProps as resolveNumberSpinnerProps } from '../../../number-spinner';
import { resolveProps as resolveRadioGroupProps } from '../../../radio-group';
import { componentMap, componentPropsConverter, loadRegister } from '../../../dynamic-view/src/components/maps';

export function useTypeResolver(): UseTypeResolver {

    // const componentMap: Record<string, any> = {
    //     'button-edit': FButtonEdit,
    //     'check-box': FCheckbox,
    //     'combo-list': FComboList,
    //     'combo-lookup': FComboList,
    //     'date-picker': FDatePicker,
    //     'date-range': FDatePicker,
    //     'datetime-picker': FDatePicker,
    //     'datetime-range': FDatePicker,
    //     'month-picker': FDatePicker,
    //     'month-range': FDatePicker,
    //     'year-picker': FDatePicker,
    //     'year-range': FDatePicker,
    //     'input-group': FInputGroup,
    //     'lookup': FButtonEdit,
    //     'number-range': FNumberRange,
    //     'number-spinner': FNumberSpinner,
    //     'radio-group': FRadioGroup,
    //     'text': FButtonEdit
    // };

    // const propsResolverMap: Record<string, (defaultValue: EditorConfig) => Record<string, any>> = {
    //     'button-edit': resolveButtonEditProps,
    //     'check-box': resolveCheckboxProps,
    //     'combo-list': resovleComboListProps,
    //     'combo-lookup': resovleComboListProps,
    //     'date-picker': resovleDatePickerProps,
    //     'date-range': resovleDatePickerProps,
    //     'datetime-picker': resovleDatePickerProps,
    //     'datetime-range': resovleDatePickerProps,
    //     'month-picker': resovleDatePickerProps,
    //     'month-range': resovleDatePickerProps,
    //     'year-picker': resovleDatePickerProps,
    //     'year-range': resovleDatePickerProps,
    //     'input-group': resovleInputGroupProps,
    //     'lookup': resolveButtonEditProps,
    //     'number-range': resolveNumberRangeProps,
    //     'number-spinner': resolveNumberSpinnerProps,
    //     'radio-group': resolveRadioGroupProps,
    //     'text': resolveButtonEditProps
    // };

    loadRegister();

    function resolveEditorProps(type: EditorType, config: EditorConfig): Record<string, any> {
        // const resolveProps = propsResolverMap[type] || resovleInputGroupProps;
        const propsConverter = componentPropsConverter[type];
        const viewProps = propsConverter ? propsConverter(config) : {};
        return viewProps;
    }

    function resolveEditorType(type: EditorType) {
        return componentMap[type] || FInputGroup;
    }

    return { resolveEditorProps, resolveEditorType };
}
