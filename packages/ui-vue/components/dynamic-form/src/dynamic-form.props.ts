import { ExtractPropTypes } from 'vue';

export const dynamicFormProps = {};

export type DynamicFormProps = ExtractPropTypes<typeof dynamicFormProps>;
