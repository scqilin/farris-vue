/* eslint-disable @typescript-eslint/indent */

export type EditorType = 'button-edit' | 'check-box' | 'combo-list' | 'combo-lookup'
    | 'date-picker' | 'date-range' | 'datetime-picker' | 'datetime-range' | 'month-picker' | 'month-range' | 'year-picker' | 'year-range'
    | 'input-group' | 'lookup' | 'number-range' | 'number-spinner' | 'radio-group' | 'text';

export interface EditorConfig {
    /** 编辑器类型 */
    type: EditorType;
    /** 自定义样式 */
    customClass?: string;
    /** 禁用 */
    disable?: boolean;
    /** 只读 */
    readonly?: boolean;
    /** 必填 */
    required?: boolean;
    /** 提示文本 */
    placeholder?: string;
    /** 其他属性 */
    [key: string]: any;
}
