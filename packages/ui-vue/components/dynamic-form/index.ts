import FDynamicForm from './src/dynamic-form.component';
import FDynamicFormGroup from './src/component/dynamic-form-group/dynamic-form-group.component';
import { formGroupPropsResolver } from './src/component/dynamic-form-group/dynamic-form-group.props';
import { App } from 'vue';

export * from './src/types';
export * from './src/composition/types';
export * from './src/dynamic-form.props';
export * from './src/component/dynamic-form-group/dynamic-form-group.props';
// export * from './src/composition/use-type-resolver';

export { FDynamicForm, FDynamicFormGroup };

export default {
    install(app: App): void {
        app.component(FDynamicForm.name, FDynamicForm)
            .component(FDynamicFormGroup.name, FDynamicFormGroup);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['form-group'] = FDynamicFormGroup;
        propsResolverMap['form-group'] = formGroupPropsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['form-group'] = FDynamicFormGroup;
        propsResolverMap['form-group'] = formGroupPropsResolver;
    }
};
