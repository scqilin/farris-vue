import { Ref, computed, ref } from "vue";
import { ExceptionInfo } from "../../message-box.props";
import { type WebkitLineClampProperty, type BoxOrientProperty } from 'csstype';

export default function (exception: Ref<ExceptionInfo | null>) {

    const showLines = ref(3);
    const exceptionMessageMaxHeight = ref(480);

    const shouldShowExceptionDate = computed(() => {
        return !!exception.value && !!exception.value.date;
    });

    const exceptionDateMessage = computed(() => {
        const exceptionDate = (exception.value && exception.value.date) || '';
        return `发生时间 : ${exceptionDate}`;
    });

    const shouldShowExceptionMessage = computed(() => {
        return !!exception.value && !!exception.value.detail;
    });

    const exceptionMessageStyle = computed(() => {
        const maxHeight = `${exceptionMessageMaxHeight.value}px`;
        const styleObject = {
            overflow: 'hidden',
            'text-overflow': 'ellipsis',
            display: '-webkit-box',
            '-webkit-box-orient': 'vertical' as BoxOrientProperty,
            '-webkit-line-clamp': showLines.value as WebkitLineClampProperty,
            'max-height': maxHeight
        };
        return styleObject;
    });

    const safeExceptionMessage = computed(() => {
        const safeMessage = (exception.value && exception.value.detail) || '';
        return safeMessage;
    });

    const shouldShowExpandHandle = computed(() => {
        return true;
    });

    const expandExceptionMessage = ref(false);
    const expandText = ref('展开');
    const collapseText = ref('收起');
    const expandHandleStyle = computed(() => {
        const styleObject = {
            display: 'block',
            color: '#2A87FF'
        } as Record<string, any>;
        styleObject['text-align'] = expandExceptionMessage.value ? '' : 'right';
        return styleObject;
    });

    function toggalExceptionMessage(expand: boolean, $event: Event) {
        expandExceptionMessage.value = !expandExceptionMessage.value;
        showLines.value = expandExceptionMessage.value ? 20 : 3;
    }

    function onClickExpand(payload: MouseEvent) {
        return toggalExceptionMessage(true, payload);
    }

    function onClickCollapse(payload: MouseEvent) {
        return toggalExceptionMessage(true, payload);
    }

    return () => {
        return (
            <div class="toast-msg-detail">
                {shouldShowExceptionDate.value && <div>{exceptionDateMessage.value}</div>}
                {shouldShowExceptionMessage.value && (
                    <div id="exception_error_msg" ref="exceptionMessageRef" style={exceptionMessageStyle.value}>
                        详细信息 : <span v-html={safeExceptionMessage.value}></span>
                    </div>
                )}
                {shouldShowExpandHandle.value && (
                    <span style={expandHandleStyle.value}>
                        {expandExceptionMessage.value && (
                            <span onClick={onClickExpand} style="cursor: pointer;">
                                {collapseText.value}
                            </span>
                        )}
                        {!expandExceptionMessage.value && (
                            <span onClick={onClickCollapse} style="cursor: pointer;">
                                {expandText.value}
                            </span>
                        )}
                    </span>
                )}
            </div>
        );
    };

}
