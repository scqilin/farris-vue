import { computed, ref } from "vue";
import { MessageBoxProps } from "../../message-box.props";
import { UseEditor } from "../../composition/types";

export default function (
    props: MessageBoxProps,
    useEditorComposition: UseEditor
) {

    const editorRender = useEditorComposition.getEditorRender(props.promptEditorType);

    return () => {
        return editorRender && editorRender();
    };
}
