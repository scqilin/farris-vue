import { Ref, computed, ref } from "vue";
import { ExceptionInfo, MessageBoxProps } from "../../message-box.props";

import getExceptionMessageRender from '../message-detail/exception-message.component';
import getStaticMessageRender from '../message-detail/static-message.component';

export default function (
    props: MessageBoxProps,
    messageType: Ref<string>,
    messageTitle: Ref<string>,
    messageDetail: Ref<string>
) {
    const exception = ref<ExceptionInfo | null>(props.exceptionInfo);
    const messageBoxIconClass = computed(() => {
        const classObject = { 'f-icon': true } as Record<string, boolean>;
        const iconClass = `f-icon-${messageType.value}`;
        classObject[iconClass] = true;
        return classObject;
    });

    const safeMessageTitle = computed(() => {
        return messageTitle.value || (exception.value && exception.value.message);
    });

    const safeMessageDetail = computed(() => {
        return messageDetail.value;
    });

    const shouldShowMessageDetail = computed(() => {
        return !!messageDetail.value;
    });

    const shouldShowExceptionMessageDetail = computed(() => {
        return !!exception.value;
    });

    function getMessageDetailRender() {
        if (shouldShowExceptionMessageDetail.value) {
            return getExceptionMessageRender(exception);
        }
        if (shouldShowMessageDetail.value) {
            return getStaticMessageRender(safeMessageDetail);
        }
        return () => <></>;
    }

    const renderMessageDetail = getMessageDetailRender();

    return () => {
        return (
            <>
                <div class="float-left modal-tips-iconwrap">
                    <span class={messageBoxIconClass.value}></span>
                </div>
                <div class="modal-tips-content">
                    <p class="toast-msg-title" v-html={safeMessageTitle.value}></p>
                    {renderMessageDetail()}
                </div>
            </>
        );
    };
}
