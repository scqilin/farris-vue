import { Ref, computed, ref } from "vue";
import { MessageBoxProps } from "../../message-box.props";

export default function (
    props: MessageBoxProps,
    messageType: Ref<string>,
    messageTitle: Ref<string>,
    messageDetail: Ref<string>
) {

    const promptFontSize = ref(14);
    const promptMaxLength = ref(140);
    const wordsTotalTips = ref('');
    const wordsTotal = ref(messageDetail.value.length);

    const safeMessageDetail = computed(() => {
        return messageDetail.value;
    });

    const promptTextAreaStyle = computed(() => {
        const fontSize = `${promptFontSize.value}px`;
        const styleObject = {
            'font-size': fontSize,
            height: '100%'
        };
        return styleObject;
    });

    function onTextChange($event: InputEvent) {
        if ($event.currentTarget) {
            const textVaue = (($event.currentTarget as any).value || '') as string;
            wordsTotal.value = textVaue.length;
        }
    }

    return () => {
        return (
            <>
                <textarea
                    title="promptMessage"
                    name="promptMessage"
                    class="form-control"
                    style={promptTextAreaStyle.value}
                    rows="4"
                    maxlength={promptMaxLength.value}
                    onInput={(payload: Event) => onTextChange(payload as InputEvent)
                    }>
                    {safeMessageDetail.value}
                </textarea>
                <span
                    class="textarea-wordcount"
                    title={wordsTotalTips.value}
                    style="position: absolute; bottom: 76px; right: 32px; cursor: pointer; text-align: right;">
                    {wordsTotal.value + ' / ' + promptMaxLength.value}
                </span>
            </>
        );
    };
};
