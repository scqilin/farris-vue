import { SetupContext, computed, ref } from "vue";
import { ModalButton } from "../../../../modal";
import { ExceptionInfo, MessageBoxProps } from "../../message-box.props";
import getCopyButton from './copy-button.component';
import { UseCopy, UseFeedback } from "../../composition/types";

export default function (
    props: MessageBoxProps,
    context: SetupContext,
    useCopyComposition: UseCopy,
    useFeedbackComposition: UseFeedback
) {
    const messageType = ref(props.type);
    const buttons = ref<ModalButton[]>([]);
    const exception = ref<ExceptionInfo | null>(props.exceptionInfo);
    const okButtonText = ref(props.okButtonText);
    const cancelButtonText = ref(props.cancelButtonText);
    const showCancelButton = ref(true);
    const showOkButton = ref(true);

    const shouldShowButtons = computed(() => {
        return !!(buttons.value && buttons.value.length);
    });

    const shouldShowCopyButton = computed(() => {
        return exception.value && exception.value.date && exception.value.message && exception.value.detail;
    });

    const shouldShowOkOrCancelButtons = computed(() => {
        return !(buttons.value && buttons.value.length) && (okButtonText.value || cancelButtonText.value);
    });

    const shouldShowOkButton = computed(() => {
        return showOkButton.value && okButtonText.value;
    });

    const shouldShowCancelButton = computed(() => {
        return showCancelButton.value && cancelButtonText.value;
    });

    const renderCopyButton = getCopyButton(props, useCopyComposition, useFeedbackComposition);

    function onClickCancelButton($event: MouseEvent) {
        if (messageType.value === 'question') {
            context.emit('reject');
        }
        context.emit('close');
    }

    function onClickOkButton($event: MouseEvent) {
        if (messageType.value === 'question') {
            context.emit('accept');
        }
        context.emit('close');
    }

    return () => {
        return (
            <div class="modal-footer">
                {shouldShowCopyButton.value && renderCopyButton()}
                {shouldShowOkOrCancelButtons.value && shouldShowCancelButton.value && (
                    <button type="button" class="btn btn-secondary btn-lg" onClick={onClickCancelButton}>
                        {cancelButtonText.value}
                    </button>
                )}
                {shouldShowOkOrCancelButtons.value && shouldShowOkButton.value &&
                    <button type="button" class="btn btn-primary btn-lg" onClick={onClickOkButton}>
                        {okButtonText.value}
                    </button>
                }
                {shouldShowButtons.value && buttons.value.length && buttons.value.map((button) => {
                    return (
                        <button type="button" onClick={button.handle} class={button.class}>
                            {button.iconClass && <span class={button.iconClass}></span>}
                            {button.text}
                        </button>
                    );
                })}
            </div>
        );
    };
}
