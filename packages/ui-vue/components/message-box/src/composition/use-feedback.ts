import { computed, ref } from "vue";
import { UseFeedback } from "./types";

export function useFeedfack(): UseFeedback {

    const toShowFeedback = ref(false);
    const feedbackMessage = ref('复制成功');
    const feedbackStyle = computed(() => {
        const styleObject = {
            position: 'absolute',
            left: '50%',
            top: '50%',
            width: '100px',
            height: '40px',
            background: '#303C53',
            'line-height': '40px',
            'text-align': 'center',
            'margin-left': '-30px',
            'margin-top': '-50px',
            'border-radius': '10px',
            'box-shadow': '0px 2px 8px 0px',
            color: '#fff',
            transition: 'all .3s ease'
        } as Record<string, any>;
        styleObject.opacity = toShowFeedback.value ? '0.8' : '0';
        styleObject.display = toShowFeedback.value ? '' : 'none';
        return styleObject;
    });

    return {
        feedbackStyle, feedbackMessage, toShowFeedback
    };
}
