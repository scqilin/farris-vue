import { Ref } from 'vue';
import { MessageBoxProps } from '../message-box.props';
import getTextAreaRender from '../components/reactive-editor/textarea.component';
import { UseEditor } from './types';

export function useEditor(
    props: MessageBoxProps,
    messageType: Ref<string>,
    messageTitle: Ref<string>,
    messageDetail: Ref<string>
): UseEditor {

    const textAreaRender = getTextAreaRender(props, messageType, messageTitle, messageDetail);

    const editorMap = new Map<string, () => JSX.Element>([['text-area', textAreaRender]]);

    function getEditorRender(editorType: string) {
        const editorRender = editorMap.get(editorType) || null;
        return editorRender;
    }

    return { getEditorRender };
}
