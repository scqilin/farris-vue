/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { reactive } from 'vue';
import type { App } from 'vue';
import { ModalService } from '../../modal';
import { ExceptionInfo } from './message-box.props';
import FMessageBox from './message-box.component';

export interface MessageBoxOption {
    type: string;
    title?: string;
    detail?: string;
    okButtonText?: string;
    cancelButtonText?: string;
    exceptionInfo?: ExceptionInfo;
    acceptCallback?: () => void;
    rejectCallback?: () => void;
}

export default class MessageBoxService {
    static show(options: MessageBoxOption): void {
        const props: MessageBoxOption = reactive({
            ...options
        });
        const showButtons = false;
        const showHeader = props.type === 'error' || props.type === 'prompt';
        const title = props.type === 'error' ? '错误提示' : props.type === 'prompt' ? (props.title || '') : '';
        const acceptCallback = props.acceptCallback || (() => { });
        const rejectCallback = props.rejectCallback || (() => { });
        let modalApp: App;
        const onClose = () => {
            if (modalApp) {
                modalApp.unmount();
            }
        };
        modalApp = ModalService.show({
            class:'modal-message modal-message-type-info',
            title,
            showButtons,
            showHeader,
            render: () => {
                return <FMessageBox {...props} onAccept={acceptCallback} onReject={rejectCallback} onClose={onClose}></FMessageBox>;
            }
        });
    }

    static info(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'info',
            title: message,
            detail,
            okButtonText: '知道了',
            cancelButtonText: ''
        });
        this.show(props);
    }

    static warning(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'warning',
            title: message,
            detail,
            okButtonText: '知道了',
            cancelButtonText: ''
        });
        this.show(props);
    }

    static success(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'success',
            title: message,
            detail,
            okButtonText: '关闭',
            cancelButtonText: ''
        });
        this.show(props);
    }

    static error(message: string, detail: string, date?: string): any {
        const props: MessageBoxOption = reactive({
            type: 'error',
            okButtonText: '关闭',
            cancelButtonText: '',
            exceptionInfo: { date, message, detail } as ExceptionInfo
        });
        this.show(props);
    }

    static prompt(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'prompt',
            title: message,
            detail,
            okButtonText: '确定',
            cancelButtonText: '取消'
        });
        this.show(props);
    }

    static question(message: string, detail: string, acceptCallback: () => void, rejectCallback: () => void) {
        const props: MessageBoxOption = reactive({
            type: 'question',
            title: message,
            detail,
            okButtonText: '确定',
            cancelButtonText: '取消',
            acceptCallback,
            rejectCallback
        });
        this.show(props);
    }
}
