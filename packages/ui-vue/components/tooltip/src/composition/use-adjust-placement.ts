/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext } from 'vue';
import { TooltipPlacement, TooltipProps } from '../tooltip.props';
import { RectDirection, RectSizeProperty } from './types';

export function useAdjustPlacement(props: TooltipProps, context: SetupContext) {
    const revertDirectionMap = new Map<RectDirection, RectDirection>([
        ['top', 'bottom'],
        ['bottom', 'top'],
        ['left', 'right'],
        ['right', 'left']
    ]);

    const directionBoundMap = new Map<RectDirection, RectSizeProperty>([
        ['top', 'height'],
        ['bottom', 'height'],
        ['left', 'width'],
        ['right', 'width']
    ]);

    function revert(placement: string, direction: RectDirection): TooltipPlacement {
        const revertDirection: RectDirection = revertDirectionMap.get(direction) || direction;
        const revertedPlacement = placement.replace(direction, revertDirection) as TooltipPlacement;
        return revertedPlacement;
    }

    function adjustPlacement(
        placementAndAlignment: TooltipPlacement,
        referenceBoundingRect: DOMRect,
        arrowReferenceBoundingRect: DOMRect,
        tooltipBound: DOMRect,
        arrowBound: DOMRect
    ): TooltipPlacement {
        let adjustedResult = placementAndAlignment;
        const placement = placementAndAlignment.split('-')[0] as RectDirection;
        const boundProperty = directionBoundMap.get(placement) as RectSizeProperty;
        const boundSize = tooltipBound[boundProperty] + arrowBound[boundProperty];
        const referenceBoundSize = Math.abs(arrowReferenceBoundingRect[placement] - referenceBoundingRect[placement]);

        if (referenceBoundSize < boundSize) {
            adjustedResult = revert(placementAndAlignment, placement);
        }
        return adjustedResult;
    }

    return { adjustPlacement };
}
