/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef } from 'vue';
import { TooltipPlacement } from '../tooltip.props';

export type RectDirection = 'top' | 'bottom' | 'right' | 'left';
export type RectSizeProperty = 'height' | 'width';
export interface RectPosition {
    top: number;
    left: number;
    right?: number;
}
export interface TooltipPosition {
    arrow: RectPosition;
    tooltip: RectPosition;
}

export interface UseTooltipPosition {
    tooltipPlacement: ComputedRef<TooltipPlacement>;
    tooltipPosition: ComputedRef<RectPosition>;
    arrowPosition: ComputedRef<RectPosition>;
}

export interface UseCalculatePosition {
    calculate: (
        placementAndAlignment: TooltipPlacement,
        hostBound: DOMRect,
        tooltipBound: DOMRect,
        tooltipContentBound: DOMRect,
        arrowBound: DOMRect
    ) => TooltipPosition;
}

export interface UseRelative {
    getRelativeElementBound: () => DOMRect;
}

export interface UseAdjustPlacement {
    adjustPlacement: (
        placementAndAlignment: TooltipPlacement,
        referenceBoundingRect: DOMRect,
        arrowReferenceBoundingRect: DOMRect,
        tooltipBound: DOMRect,
        arrowBound: DOMRect
    ) => TooltipPlacement;
}

export interface UseAdjustPosition {
    adjustPosition: (
        placementAndAlignment: TooltipPlacement,
        originalPosition: RectPosition,
        relativeElementRect: DOMRect,
        tooltipRect: DOMRect
    ) => RectPosition;
}
