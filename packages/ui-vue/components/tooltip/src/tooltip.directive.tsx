/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { App, createApp, onUnmounted, reactive } from 'vue';
import { TooltipProps } from './tooltip.props';
import Tooltip from './tooltip.component';

function initInstance(props: TooltipProps, content?: string): App {
    const container = document.createElement('div');
    const app: App = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            return () => <Tooltip {...props} onClick={app.unmount}></Tooltip>;
        }
    });
    document.body.appendChild(container);
    app.mount(container);
    return app;
}

function showTooltip(options: TooltipProps): App {
    const props: TooltipProps = reactive({
        ...options
    });
    return initInstance(props);
}

const tooltipDirective = {
    mounted: (element: any, binding: Record<string, any>, vnode: any) => {
        let app: App | null;
        const tooltipProps = Object.assign(
            {
                content: 'This is a tooltip',
                width: 100,
                customClass: '',
                placement: 'top',
                reference: element
            },
            binding.value
        );
        element.addEventListener('mouseenter', ($event: MouseEvent) => {
            $event.stopPropagation();
            if (!app) {
                app = showTooltip(tooltipProps);
            }
        });
        element.addEventListener('mouseleave', ($event: MouseEvent) => {
            $event.stopPropagation();
            if (app) {
                app.unmount();
                app = null;
            }
        });
    },
    unMounted: (element: any, binding: Record<string, any>, vnode: any) => {}
};
export default tooltipDirective;
