/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export type TooltipPlacement =
    | 'top'
    | 'top-left'
    | 'top-right'
    | 'bottom'
    | 'bottom-left'
    | 'bottom-right'
    | 'right'
    | 'right-top'
    | 'right-bottom'
    | 'left'
    | 'left-top'
    | 'left-bottom';

export const tooltipProps = {
    content: { type: String },
    width: { type: Number },
    customClass: { type: String },
    placement: { type: String as PropType<TooltipPlacement>, default: 'top' },
    reference: { type: Object, require: true },
    horizontalRelative: { type: String, defatul: '' },
    verticalRelative: { type: String, defatul: '' }
};
export type TooltipProps = ExtractPropTypes<typeof tooltipProps>;
