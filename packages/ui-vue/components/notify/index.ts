/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import Notify from './src/notify.component';
import Toast from './src/components/toast.component';
import NotifyService from './src/notify.service';

export * from './src/notify.props';
export * from './src/components/toast.props';

export { Notify, NotifyService, Toast };

export default {
    install(app: App): void {
        app.component(Notify.name, Notify);
        app.component(Toast.name, Toast);
        app.provide('NotifyService', NotifyService);
    }
};
