/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { reactive, createApp, onUnmounted } from 'vue';
import type { App } from 'vue';
import { NotifyProps } from './notify.props';
import Notify from './notify.component';

function initInstance(props: NotifyProps, content?: string): App {
    const container = document.createElement('div');
    container.style.display = 'contents';
    const app: App = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            return () => <Notify {...props} onClose={app.unmount}></Notify>;
        }
    });
    document.body.appendChild(container);
    app.mount(container);
    return app;
}

export default class NotifyService {
    static show(options: NotifyProps): void {
        const props: NotifyProps = reactive({
            ...options
        });
        initInstance(props);
    }
}
