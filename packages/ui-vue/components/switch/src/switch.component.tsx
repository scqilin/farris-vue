/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext, toRefs } from 'vue';
import { switchProps, SwitchProps } from './switch.props';
import './switch.css';

export default defineComponent({
    name: 'FSwitch',
    props: switchProps,
    emits: ['update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: SwitchProps, context: SetupContext) {
        const { disable, size, onLabel, offLabel } = toRefs(props);
        const modelValue = ref(props.modelValue);

        function getBackgroundColor() {
            return '';
        }

        function getBorderColor() {
            return '';
        }

        function getSwitchColor() {
            return '';
        }

        const switchContainerClass = computed(() => ({
            switch: true,
            'f-cmp-switch': true,
            checked: modelValue.value,
            disabled: disable.value,
            'switch-large': size.value === 'large',
            'switch-medium': size.value === 'medium',
            'switch-small': size.value === 'small'
        }));

        const switchContainerStyle = computed(() => ({
            outline: 'none',
            'backgroud-color': getBackgroundColor(),
            'border-color': getBorderColor()
        }));

        const smallStyle = computed(() => ({
            background: getSwitchColor()
        }));

        const shouldShowSwitch = computed(() => {
            // checkedLabel || uncheckedLabel
            return onLabel?.value || offLabel?.value;
        });

        function updateChecked($event: any, emitClick = true) {
            if (disable.value) {
                return;
            }
            modelValue.value = !modelValue.value;
            context.emit('update:modelValue', modelValue.value);
        }

        function onToggle($event: MouseEvent) {
            updateChecked($event);
        }

        return () => {
            return (
                <span tabindex="0" role="button" class={switchContainerClass.value} style={switchContainerStyle.value} onClick={onToggle}>
                    {shouldShowSwitch.value && (
                        <span class="switch-pane">
                            <span class="switch-label-checked">{onLabel?.value}</span>
                            <span class="switch-label-unchecked">{offLabel?.value}</span>
                        </span>
                    )}
                    <small style={smallStyle.value}>{context.slots.default && context.slots.default()}</small>
                </span>
            );
        };
    }
});
