import type { App } from 'vue';
import TimePicker from './src/time-picker.component';
import TimePickerTimeView from './src/components/time.component';
import { propsResolver } from './src/time-picker.props';

export * from './src/time-picker.props';
export * from './src/components/time.props';

export { TimePicker, TimePickerTimeView };

export default {
    install(app: App): void {
        app.component(TimePicker.name, TimePicker).component(TimePickerTimeView.name, TimePickerTimeView);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['time-picker'] = TimePicker;
        propsResolverMap['time-picker'] = propsResolver;
    }
};
