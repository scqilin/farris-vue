import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import timePickerSchema from './schema/time-picker.schema.json';

export const timePickerProps = {
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' }
} as Record<string, any>;

export type TimePickerProps = ExtractPropTypes<typeof timePickerProps>;

export const propsResolver = createPropsResolver<TimePickerProps>(timePickerProps, timePickerSchema, schemaMapper);
