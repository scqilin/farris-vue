import { SetupContext, defineComponent, ref } from 'vue';
import { TimePickerProps, timePickerProps } from './time-picker.props';
import { ButtonEdit } from '../../button-edit';

export default defineComponent({
    name: 'FTimePicker',
    props: timePickerProps,
    emits: [],
    setup(props: TimePickerProps, context: SetupContext) {
        const groupIcon = '<span class="f-icon f-icon-timepicker"></span>';
        const modelValue = ref(props.modelValue);
        const buttonEdit = ref<any>(null);

        function onClickButton() {}

        function onDatePicked(dateValue: string) {
            modelValue.value = dateValue;
            context.emit('update:modelValue', dateValue);
            context.emit('datePicked', dateValue);
            buttonEdit.value?.commitValue(dateValue);
        }

        return () => {
            return (
                <>
                    <ButtonEdit
                        ref={buttonEdit}
                        v-model={modelValue.value}
                        buttonContent={groupIcon}
                        enableClear
                        onClickButton={() => onClickButton()}>
                        <f-time-picker-time-view></f-time-picker-time-view>
                    </ButtonEdit>
                </>
            );
        };
    }
});
