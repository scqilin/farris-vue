import { parse, format as fnsFormat } from 'date-fns';

export function parseTime(text: string): Date | undefined {
    if (!text) {
        return;
    }
    return new Date(`1970-01-01 ${text}`);
}
export function format(date: Date, formatStr: string): string {
    return fnsFormat(date, formatStr);
}
