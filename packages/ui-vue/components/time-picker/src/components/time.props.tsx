import { ExtractPropTypes } from 'vue';

export const timeProps = {
    hourStep: { Type: Number, default: 1 },
    minuteStep: { Type: Number, default: 1 },
    secondStep: { Type: Number, default: 1 },
    clearText: { Type: String, default: 'clear' },
    popupClassName: { Type: String, default: '' },
    placeholder: { Type: String, default: '' },
    defaultOpenValue: { Type: Date, default: new Date() },
    format: { Type: String, default: 'HH:mm:ss' },
    isOpen: { Type: Boolean, default: false },
    use12Hours: { Type: Boolean, default: false },
    hideDisabledOptions: { Type: Boolean, default: false }
};

export type TimeProps = ExtractPropTypes<typeof timeProps>;
