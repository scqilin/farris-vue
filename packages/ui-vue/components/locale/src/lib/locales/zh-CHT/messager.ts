export const MESSAGER_LOCALE_ZHCHT = {
    yes: '是',
    no: '否',
    ok: '確定',
    cancel: '取消',
    title: '係統提示',
    errorTitle: '錯誤提示',
    prompt: {
        fontSize: {
            name:  '字體大小',
            small: '小',
            middle: '中',
            big: '大',
            large: '特大',
            huge: '超大'
        }
    },
    exception: {
        expand: '展開',
        collapse: '收起',
        happend: '發生時間',
        detail: '詳細信息',
        copy: '複制詳細信息',
        copySuccess: '複制成功',
        copyFailed: '複制失敗',
        roger: '知道了'
    }
};
