export const DATAGRID_LOCALE_ZHCHT = {
    lineNumberTitle: '序號',
    emptyMessage: '暫無數據',
    pagination: {
        previousLabel: '上一頁',
        nextLabel: '下一頁',
        message: '共 <b>{1}</b> 條 ',
        pagelist: {
            firstText: '顯示',
            lastText: '條'
        }
    },
    filter: {
        title: '過濾條件',
        reset: '重置',
        clear: '清空條件',
        clearAll: '清空所有條件',
        setting: '高級設置',
        nofilter: '[ 無 ]',
        checkAll: '全選',
        and: '並且',
        or: '或者',
        operators: {
            equal: '等於',
            notEqual: '不等於',
            greater: '大於',
            greaterOrEqual: '大於等於',
            less: '小於',
            lessOrEqual: '小於等於',
            contains: '包含',
            notContains: '不包含',
            like:  '包含',
            notLike: '不包含',
            in: '屬於',
            notIn: '不屬於',
            empty: '為空',
            notEmpty: '不為空',
            null: 'null',
            notNull: '不為null'
        },
        more: '查看更多'
    },
    settings: {
        visible: '顯示列',
        sortting: '列排序',
        title: '列配置',
        canchoose: '可選列',
        choosed: '已選列',
        asc: '升序',
        desc: '降序',
        cancelSort: '取消排序',
        ok: '確定',
        cancel: '取消',
        reset: '恢複默認',
        conciseMode: '簡潔模式',
        advancedMode: '高級模式',
        formatSetting: '列格式',
        properties: '列屬性',
        groupping: '分組',
        allColumns: '所有列',
        visibleColumns: '可見列',
        hiddenColumns: '隱藏列',
        searchPlaceholder: '請輸入列名稱',
        checkall: '全部顯示/隱藏',
        headeralign: '表頭對齊',
        dataalign: '數據對齊',
        alignLeft: '左對齊',
        alignCenter: '居中對齊',
        alignRight: '右對齊',
        summarytype: '匯總合計類型',
        summarytext: '匯總合計文本',
        summaryNone: '無',
        summarySum: '求和',
        summaryMax: '最大值',
        summaryMin: '最小值',
        summarCount: '計數',
        summaryAverage: '平均值',
        grouppingField: '分組字段',
        moreGrouppingFieldWarningMessage: '最多設置3個字段進行分組',  // Up to 3 fields are set for grouping
        grouppingSummary: '分組合計',
        addGrouppingFieldTip: '添加分組字段',
        removeGrouppingFieldTip: '移除分組字段',
        grouppingSummaryType: '分組合計類型',
        grouppingSummaryText: '分組合計文本',
        restoreDefaultSettingsText: '確認要恢複默認設置嗎?',
        simple: {
            title: '顯示列',
            tip: '選中的字段可展示到列表中，拖拽可調整在列表中的展示順序。',
            count: '已顯示 <span class="visible-cols-count">{0}</span> 列'
        }
    },
    selectionData: {
        clearAll: '清空',
        tooltip: '點擊顯示已選記錄列錶',
        currentLenth: `已選擇：<b>{0}</b> 條`
    },
    groupRow: {
        tips: '拖動列到這兒可進行數據分組',
        removeColumn: '移除分組列',
        clearTip: '清除所有分組字段',
        clear: '清空'
    }
    
};
