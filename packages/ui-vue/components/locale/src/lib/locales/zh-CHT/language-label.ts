export const LANGUAGE_LABEL_LOCALE_ZHCHT = {
    en: '英語',
    "zh-cn": '簡體中文',
    "zh-CHS": '簡體中文',
    "zh-CHT": '繁體中文',
    ok: '確定',
    cancel: '取消'
};
