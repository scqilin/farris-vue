export const TEXT_LOCALE_ZHCHT = {
    yes: '是',
    no: '否',
    zoom: '在打開的對話框中編輯內容',
    comments: {
        title: '常用意見',
        manager: '意見管理',
        empty: '暫無數據'
    }
};
