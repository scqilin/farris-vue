export const NUMERIC_LOCALE_ZHCHT = {
    placeholder: '請輸入數字',
    range: {
        begin: '請輸入開始數字',
        end: '請輸入結束數字'
    }
};
