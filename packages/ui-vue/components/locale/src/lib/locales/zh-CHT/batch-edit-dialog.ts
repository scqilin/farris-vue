export const BATCH_EDIT_DIALOG_LOCALE_ZHCHT = {
  title: '批量編輯',
  appendText: '添加新編輯列',
  appendTextTip: '添加更多列進行批量操作',
  okText: '確定',
  cancelText: '取消',
  field: '請選擇要編輯的列：',
  fieldValue: '請輸入要更改的值：',
  appendTips: '添加更多列進行批量操作',
  selected: '已選',
  row: '行',
  confirmTitle: '提示',
  neverShow: '不再提示',
  confirmText: '將修改{0}行數據，確定修改嗎？'
};
