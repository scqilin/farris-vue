import { LOOKUP_LOCALE_ZHCHT } from './lookup';
import { DATAGRID_LOCALE_ZHCHT } from './datagrid';
import { SECTION_LOCALE_ZHCHT } from './section';
import { LOADING_LOCALE_ZHCHT } from './loading';
import { FILTER_EDITOR_LOCALE_ZHCHT, ENUM_EDITOR_LOCALE_ZHCHT } from './filter-editor';
import { MESSAGER_LOCALE_ZHCHT } from './messager';
import { NOTIFY_LOCALE_ZHCHT } from './notify';
import { PAGINATION_LOCALE_ZHCHT } from './pagination';
import { SORT_EDITOR_LOCALE_ZHCHT } from './sort-editor';
import { TEXT_LOCALE_ZHCHT } from './text';
import { SIDEBAR_LOCALE_ZHCHT } from './sidebar';
import { TABS_LOCALE_ZHCHT } from './tabs';
import { RESPONSE_TOOLBAR_LOCALE_ZHCHT } from './response-toolbar';
import { MULTI_SELECT_LOCALE_ZHCHT } from './multi-select';
import { QUERY_CONDITION_LOCALE_ZHCHT } from './query-condition';
import { QUERY_SOLUTION_LOCALE_ZHCHT } from './query-solution';
import { COLLAPSE_DIRECTIVE_LOCALE_ZHCHT } from './collapse';
import { TREETABLE_LOCALE_ZHCHT } from './treetable';
import { AVATAR_LOCALE_ZHCHT } from './avatar';
import { LIST_FILTER_LOCALE_ZHCHT } from './list-filter';
import { PROGRESS_STEP_LOCALE_ZHCHT } from './progress-step';
import { LANGUAGE_LABEL_LOCALE_ZHCHT } from './language-label';
import { VERIFY_DETAIL_ZHCHT } from './verify-detail';
import { BATCH_EDIT_DIALOG_LOCALE_ZHCHT } from './batch-edit-dialog';
import { PAGE_WALKER_ZHCHT } from './page-walker';
import { FOOTER_LOCALE_ZHCHT } from './footer';
import { DISCUSSION_GROUP_LOCALE_ZHCHT } from './discussion-group';
import { TAG_LOCALE_ZHCHT } from './tag';
import { NUMERIC_LOCALE_ZHCHT } from './numeric';
import { FILTER_PANEL_LOCALE_ZHCHT } from './filter-panel';
import { SCROLLSPY_LOCALE_ZHCHT } from './scrollspy';
import { LOOKUP_CONFIG_LOCALE_ZHCHT } from './lookup-config';
import { COMBO_LOCALE } from './combo';
import { LISTVIEW_LOCALE_ZHCHT } from './list-view';

export const ZH_CHT = {
    locale: 'ZH_CHT',
    combo: COMBO_LOCALE,
    combolist: {},
    datagrid: DATAGRID_LOCALE_ZHCHT,
    filterEditor: FILTER_EDITOR_LOCALE_ZHCHT,
    enumEditor: ENUM_EDITOR_LOCALE_ZHCHT,
    lookup: LOOKUP_LOCALE_ZHCHT,
    loading: LOADING_LOCALE_ZHCHT,
    modal: {},
    messager: MESSAGER_LOCALE_ZHCHT,
    notify: NOTIFY_LOCALE_ZHCHT,
    dialog: {},
    datatable: {},
    colorPicker: {},
    numberSpinner: NUMERIC_LOCALE_ZHCHT,
    inputGroup: {},
    sortEditor: SORT_EDITOR_LOCALE_ZHCHT,
    treetable: TREETABLE_LOCALE_ZHCHT,
    multiSelect: MULTI_SELECT_LOCALE_ZHCHT,
    tabs: TABS_LOCALE_ZHCHT,
    timePicker: {},
    wizard: {},
    tree: {},
    tooltip: {},
    listview: LISTVIEW_LOCALE_ZHCHT,
    text: TEXT_LOCALE_ZHCHT,
    switch: {},
    sidebar: SIDEBAR_LOCALE_ZHCHT,
    section: SECTION_LOCALE_ZHCHT,
    pagination: PAGINATION_LOCALE_ZHCHT,
    responseToolbar: RESPONSE_TOOLBAR_LOCALE_ZHCHT,
    queryCondition: QUERY_CONDITION_LOCALE_ZHCHT,
    querySolution: QUERY_SOLUTION_LOCALE_ZHCHT,
    collapseDirective: COLLAPSE_DIRECTIVE_LOCALE_ZHCHT,
    avatar: AVATAR_LOCALE_ZHCHT,
    listFilter: LIST_FILTER_LOCALE_ZHCHT,
    progressStep: PROGRESS_STEP_LOCALE_ZHCHT,
    languageLabel: LANGUAGE_LABEL_LOCALE_ZHCHT,
    verifyDetail: VERIFY_DETAIL_ZHCHT,
    batchEditDialog: BATCH_EDIT_DIALOG_LOCALE_ZHCHT,
    pageWalker: PAGE_WALKER_ZHCHT,
    footer: FOOTER_LOCALE_ZHCHT,
    discussionGroup: DISCUSSION_GROUP_LOCALE_ZHCHT,
    tag: TAG_LOCALE_ZHCHT,
    filterPanel: FILTER_PANEL_LOCALE_ZHCHT,
    scrollspy: SCROLLSPY_LOCALE_ZHCHT,
    lookupConfig: LOOKUP_CONFIG_LOCALE_ZHCHT
};
