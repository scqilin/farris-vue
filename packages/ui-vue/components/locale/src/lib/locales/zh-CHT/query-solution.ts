export const QUERY_SOLUTION_LOCALE_ZHCHT = {
    saveAsDialog: {
        queryPlanName: '方案名稱',
        setAsDefault: '設為默認',
        confirm: '確定',
        cancel: '取消',
        placeholder: '請輸入方案名稱',
        pleaseInput: '請輸入方案名稱',
        title: '新增方案',
        maxLength: '方案名稱超出九個字'
    },
    manageDialog: {
        name: '名稱',
        property: '屬性',
        default: '默認',
        operation: '操作',
        confirm: '確定',
        cancel: '取消',
        planNameDuplicated: '方案名稱{0}重複',
        system: '係統',
        personal: '個人'
    },
    container: {
        default: '默認',
        manage: '管理',
        arrowUp: '收起',
        arrowDown: '展開',
        saveAs: '另存為方案',
        empty: '空',
        save: '保存方案',
        pleaseInput: 'formId為必傳字段，請傳入',
        saveSuccess: '查詢方案保存成功',
        saveFail: '查詢方案保存失敗',
        planManage: '方案管理',
        clear: '清空',
        require: '請填寫{fields}再進行篩選',
        defaultName: '默認篩選方案',
        histroyName: '上次篩選',
        sysPresetName: '系统预置方案'
    }
};
