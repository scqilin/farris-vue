export const QUERY_CONDITION_LOCALE_ZHCHT = {
    configDialog: {
        unSelectedOptions: '未選擇項',
        selectedOptions: '已選擇項',
        confirm: '確定',
        cancel: '取消',
        placeholder: '請輸入搜索關鍵字',
        moveUp: '上移',
        moveAllUp: '全部上移',
        moveDown: '下移',
        moveAllDown: '全部下移',
        moveRight: '右移',
        moveAllRight: '全部右移',
        moveLeft: '左移',
        moveAllLeft: '全部左移',
        pleaseSelect: '請選擇字段',
        noOptionMove: '冇有可移動字段',
        selectOptionUp: '請選擇上移字段',
        cannotMoveUp: '無法上移',
        selectOptionTop: '請選擇置頂字段',
        optionIsTop: '字段已置頂',
        selectOptionDown: '請選擇下移字段',
        cannotMoveDown: '無法下移',
        selectOptionBottom: '請選擇置底字段',
        optionIsBottom: '字段已置底'
    },
    container: {
        query: '篩選',
        saveAs: '另存為',
        save: '保存',
        config: '配置'
    }
};
