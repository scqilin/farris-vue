export const BATCH_EDIT_DIALOG_LOCALE_ZHCHS = {
  title: '批量编辑',
  appendText: '添加新编辑列',
  appendTextTip: '添加更多列进行批量操作',
  okText: '确定',
  cancelText: '取消',
  field: '请选择要编辑的列：',
  fieldValue: '请输入要更改的值：',
  appendTips: '添加更多列进行批量操作',
  selected: '已选',
  row: '行',
  confirmTitle: '提示',
  neverShow: '不再提示',
  confirmText: '将修改{0}行数据，确定修改吗？'
};
