export const LOOKUP_LOCALE_ZHCHS = {
    placeholder: '请选择',
    favorites: '收藏夹',
    selected: '已选数据',
    okText: '确定',
    cancelText: '取消',
    allColumns: '所有列',
    datalist: '数据列表',
    mustWriteSomething: '请输入关键字后查询。',
    mustChoosAdatarow: '请选择一条记录！',
    tipText: '您要找的是不是这些？',
    cascade: {
        enable: '同步选择',
        disable: '仅选择自身',
        up: '包含上级',
        down: '包含下级'
    },
    favoriteInfo: {
        addFav: '收藏成功。',
        cancelFav: '取消收藏成功。 '
    },
    getAllChilds: '获取所有子级数据',
    contextMenu: {
        expandall: '全部展开',
        collapseall: '全部收起',
        expandByLayer: '按层级展开',
        expand1: '展开 1 级',
        expand2: '展开 2 级',
        expand3: '展开 3 级',
        expand4: '展开 4 级',
        expand5: '展开 5 级',
        expand6: '展开 6 级',
        expand7: '展开 7 级',
        expand8: '展开 8 级',
        expand9: '展开 9 级'
    },
    quick: {
        notfind: '未找到搜索内容',
        more: '显示更多'
    },
    configError: '帮助显示列未配置，请检查是否已正确配置帮助数据源! ',
    selectedInfo: {
        total: '已选 <span class="selected-counter mx-1">{0}</span> 条',
        clear: '取消已选',
        remove: '删除已选({0})',
        confirm: '您确认要取消所有已选中的记录吗?'
    }
};
