export const MULTI_SELECT_LOCALE_ZHCHS = {
    leftTitle: '未选择',
    rightTitle: '已选择',
    noDataMoveMessage: '请选择要移动的数据。',
    shiftRight: '右移',
    shiftLeft: '左移',
    allShiftRight: '全部右移',
    allShiftLeft: '全部左移',
    top: '置顶',
    bottom: '置底',
    shiftUp: '上移',
    shiftDown: '下移',
    emptyData: '暂无数据',
    filterPlaceholder: '输入筛选项名称搜索'
};
