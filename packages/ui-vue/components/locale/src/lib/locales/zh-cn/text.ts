export const TEXT_LOCALE_ZHCHS = {
    yes: '是',
    no: '否',
    zoom: '在打开的对话框中编辑内容',
    comments: {
        title: '常用意见',
        manager: '意见管理',
        empty: '暂无数据'
    }
};
