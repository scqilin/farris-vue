export const AVATAR_LOCALE_ZHCHS = {
    imgtitle: '点击修改',
    typeError: '上传图片类型不正确',
    sizeError: '上传图片不能大于',
    uploadError: '图片上传失败，请重试!',
    loadError: '加载错误'
};