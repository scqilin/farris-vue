export const TREETABLE_LOCALE_ZHCHS = {
    emptyMessage: '暂无数据',
    pagination: {
        previousLabel: '上一页',
        nextLabel: '下一页',
        message: '每页 {0} 条记录，共 {1} 条记录。'
    }
};
