export const PAGINATION_LOCALE_ZHCHS = {
    message: '共 <b>{1}</b> 条 ',
    totalinfo: {
        firstText: '共',
        lastText: '条'
    },
    pagelist: {
        firstText: '每页',
        lastText: '条'
    },
    previous: '上一页',
    next: '下一页',
    goto: {
        prefix: '跳至',
        suffix: '页'
    }
};
