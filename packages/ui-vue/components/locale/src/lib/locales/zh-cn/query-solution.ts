export const QUERY_SOLUTION_LOCALE_ZHCHS = {
    saveAsDialog: {
        queryPlanName: '方案名称',
        setAsDefault: '设为默认',
        confirm: '确定',
        cancel: '取消',
        placeholder: '请输入方案名称',
        pleaseInput: '请输入方案名称',
        title: '新增方案',
        maxLength: '方案名称超出九个字'
    },
    manageDialog: {
        name: '名称',
        property: '属性',
        default: '默认',
        operation: '操作',
        confirm: '确定',
        cancel: '取消',
        planNameDuplicated: '方案名称{0}重复',
        system: '系统',
        personal: '个人'
    },
    container: {
        default: '默认',
        manage: '管理',
        arrowUp: '收起',
        arrowDown: '展开',
        saveAs: '另存为方案',
        empty: '空',
        save: '保存方案',
        pleaseInput: 'formId为必传字段，请传入',
        saveSuccess: '查询方案保存成功',
        saveFail: '查询方案保存失败',
        planManage: '方案管理',
        clear: '清空',
        require: '请填写{fields}再进行筛选',
        defaultName: '默认筛选方案',
        histroyName: '上次筛选',
        sysPresetName: '系统预置方案'
    }

};
