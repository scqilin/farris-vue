export const LOOKUP_LOCALE = {
    placeholder: 'Please choose',
    favorites: 'Favorites',
    selected: 'Selected items',
    okText: 'OK',
    cancelText: 'Cancel',
    allColumns: 'All Columns',
    datalist: 'Data items',
    mustWriteSomething: 'Please write a key words.',
    mustChoosAdatarow: 'Please choose a data row！',
    tipText: 'Are these what you are looking for?',
    cascade: {
        enable: 'Enable cascade selection',
        disable: 'Disable cascade selection',
        up: 'Cascade up selection only',
        down: 'Cascade selection only'
    },
    favoriteInfo: {
        addFav: 'Collection success.',
        cancelFav: 'Unfavorite successfully. '
    },
    getAllChilds: 'Get all children',
    contextMenu: {
        expandall: 'Expand all',
        collapseall: 'Collapse all',
        expandByLayer: 'Expand by level',
        expand1: 'Expand to level 1',
        expand2: 'Expand to level 2',
        expand3: 'Expand to level 3',
        expand4: 'Expand to level 4',
        expand5: 'Expand to level 5',
        expand6: 'Expand to level 6',
        expand7: 'Expand to level 7',
        expand8: 'Expand to level 8',
        expand9: 'Expand to level 9'
    },
    quick: {
        notfind: 'Search content not found.',
        more: 'Show more'
    },
    configError: 'The help display column is not configured. Please check whether the help data source is configured correctly.',
    selectedInfo: {
        total: 'Selected items <span class="selected-counter mx-1">{0}</span>',
        clear: 'Cancel selected',
        remove: 'Delete ({0})',
        confirm: 'Are you sure you want to cancel all selected records?'
    }
};
