export const DATAGRID_LOCALE = {
    lineNumberTitle: 'NO.',
    emptyMessage: 'Empty Data',
    pagination: {
        previousLabel: 'Prev Page',
        nextLabel: 'Next Page',
        message: 'Total <b>{1}</b> items ',
        pagelist: {
            firstText: 'Display',
            lastText: 'items'
        }
    },
    filter: {
        title: 'Conditions',
        reset: 'Reset',
        clear: 'Clear',
        clearAll: 'Clear all conditions',
        setting: 'Settings',
        nofilter: '[ Empty ]',
        checkAll: 'Check All',
        and: 'And',
        or: 'Or',
        operators: {
            equal: 'equal',
            notEqual: 'not equal',
            greater: 'greater than',
            greaterOrEqual: 'greater than or equal',
            less: 'less than',
            lessOrEqual: 'less than or equal',
            contains: 'contains',
            notContains: 'not contains',
            like: 'contains',
            notLike: 'not contains',
            in: 'in',
            notIn: 'not in',
            empty: 'empty',
            notEmpty: 'not empty',
            null: 'null',
            notNull: 'not null'
        },
        more: 'More'
    },
    settings: {
        visible: 'Visible',
        sortting: 'Sortting',
        title: 'Column Settings',
        canchoose: 'Can choose',
        choosed: 'Choosed',
        asc: 'ASC',
        desc: 'DESC',
        cancelSort: 'Cancel sortting',
        ok: 'OK',
        cancel: 'Cancel',
        reset: 'Reset',
        conciseMode: 'Concise',
        advancedMode: 'Advanced',
        formatSetting: 'Column format',
        properties: 'Column properties',
        groupping: 'Groupping',
        allColumns: 'All',
        visibleColumns: 'Visible',
        hiddenColumns: 'Hidden',
        searchPlaceholder: 'Please enter a column name',
        checkall: 'Show or hide all',
        headeralign: 'Header alignment',
        dataalign: 'Data alignment',
        alignLeft: 'Left',
        alignCenter: 'Center',
        alignRight: 'Right',
        summarytype: 'Summary type',
        summarytext: 'Summary text',
        summaryNone: 'None',
        summarySum: 'Sum',
        summaryMax: 'Max',
        summaryMin: 'Min',
        summarCount: 'Count',
        summaryAverage: 'Average',
        grouppingField: 'Groupping field',
        moreGrouppingFieldWarningMessage: 'Up to 3 fields are set for grouping',
        grouppingSummary: 'Group total',
        addGrouppingFieldTip: 'Add groupping field',
        removeGrouppingFieldTip: 'Remove groupping field',
        grouppingSummaryType: 'Group total type',
        grouppingSummaryText: 'Group total text',
        restoreDefaultSettingsText: 'Are you sure you want to restore the default settings',
        simple: {
            title: 'Show Columns',
            tip: 'The selected fields can be displayed in the list. Drag to adjust the display order in the list.',
            count: 'show <span class="visible-cols-count">{0}</span> columns'
        }
    },
    selectionData: {
        clearAll: 'Clear all',
        tooltip: 'Click here show list.',
        currentLenth: `<b>{0}</b> items selected. `
    },
    groupRow: {
        tips: 'Drag columns here to group data.',
        removeColumn: 'Remove the group column.',
        clearTip: 'Clear all grouped fields.',
        clear: 'Empty'
    }

};
