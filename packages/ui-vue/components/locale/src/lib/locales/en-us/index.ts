import { LISTVIEW_LOCALE } from './list-view';
import { COMBO_LOCALE } from './combo';
import { QUERY_SOLUTION_LOCALE } from './query-solution';
import { QUERY_CONDITION_LOCALE } from './query-condition';
import { RESPONSE_TOOLBAR_LOCALE } from './response-toolbar';
import { PAGINATION_LOCALE } from './pagination';
import { MESSAGER_LOCALE } from './messager';
import { DATAGRID_LOCALE } from './datagrid';
import { SECTION_LOCALE } from './section';
import { LOADING_LOCALE } from './loading';
import { FILTER_EDITOR_LOCALE, ENUM_EDITOR_LOCALE } from './filter-editor';
import { NOTIFY_LOCALE } from './notify';
import { SORT_EDITOR_LOCALE } from './sort-editor';
import { TEXT_LOCALE } from './text';
import { SIDEBAR_LOCALE } from './sidebar';
import { TABS_LOCALE } from './tabs';
import { MULTI_SELECT_LOCALE } from './multi-select';
import { COLLAPSE_DIRECTIVE_LOCALE } from './collapse';
import { LOOKUP_LOCALE } from './lookup';
import { TREETABLE_LOCALE } from './treetable';
import { AVATAR_LOCALE } from './avatar';
import { LIST_FILTER_LOCALE } from './list-filter';
import { PROGRESS_STEP_LOCALE } from './progress-step';
import { LANGUAGE_LABEL_LOCALE } from './language-label';
import { VERIFY_DETAIL_LOCALE } from './verify-detail';
import { BATCH_EDIT_DIALOG_LOCALE } from './batch-edit-dialog';
import { PAGE_WALKER_LOCALE } from './page-walker';
import { FOOTER_LOCALE } from './footer';
import { DISCUSSION_GROUP_LOCALE } from './discussion-group';
import { TAG_LOCALE } from './tag';
import { NUMERIC_LOCALE } from './numeric';
import { FILTER_PANEL_LOCALE } from './filter-panel';
import { SCROLLSPY_LOCALE } from './scrollspy';
import { LOOKUP_CONFIG_LOCALE } from './lookup-config';

export const EN_US = {
    locale: 'EN_US',
    combo: COMBO_LOCALE,
    combolist: {},
    datagrid: DATAGRID_LOCALE,
    filterEditor: FILTER_EDITOR_LOCALE,
    enumEditor: ENUM_EDITOR_LOCALE,
    lookup: LOOKUP_LOCALE,
    loading: LOADING_LOCALE,
    modal: {},
    messager: MESSAGER_LOCALE,
    notify: NOTIFY_LOCALE,
    dialog: {},
    datatable: {},
    colorPicker: {},
    numberSpinner: NUMERIC_LOCALE,
    inputGroup: {},
    treetable: TREETABLE_LOCALE,
    multiSelect: MULTI_SELECT_LOCALE,
    sortEditor: SORT_EDITOR_LOCALE,
    tabs: TABS_LOCALE,
    timePicker: {},
    wizard: {},
    tree: {},
    tooltip: {},
    listview: LISTVIEW_LOCALE,
    text: TEXT_LOCALE,
    switch: {},
    sidebar: SIDEBAR_LOCALE,
    section: SECTION_LOCALE,
    pagination: PAGINATION_LOCALE,
    responseToolbar: RESPONSE_TOOLBAR_LOCALE,
    queryCondition: QUERY_CONDITION_LOCALE,
    querySolution: QUERY_SOLUTION_LOCALE,
    collapseDirective: COLLAPSE_DIRECTIVE_LOCALE,
    avatar: AVATAR_LOCALE,
    listFilter: LIST_FILTER_LOCALE,
    progressStep: PROGRESS_STEP_LOCALE,
    languageLabel: LANGUAGE_LABEL_LOCALE,
    verifyDetail: VERIFY_DETAIL_LOCALE,
    batchEditDialog: BATCH_EDIT_DIALOG_LOCALE,
    pageWalker: PAGE_WALKER_LOCALE,
    footer: FOOTER_LOCALE,
    discussionGroup: DISCUSSION_GROUP_LOCALE,
    tag: TAG_LOCALE,
    filterPanel: FILTER_PANEL_LOCALE,
    scrollspy: SCROLLSPY_LOCALE,
    lookupConfig: LOOKUP_CONFIG_LOCALE
};
