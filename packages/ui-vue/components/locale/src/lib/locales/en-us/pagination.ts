export const PAGINATION_LOCALE = {
    message: 'Total <b>{1}</b> items ',
    totalinfo: {
        firstText: 'Total',
        lastText: 'items'
    },
    pagelist: {
        firstText: 'Display',
        lastText: 'items'
    },
    previous: 'Previous',
    next: 'next',
    goto: {
        prefix: 'go to',
        suffix: ''
    }
};
