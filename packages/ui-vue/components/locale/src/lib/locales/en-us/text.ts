export const TEXT_LOCALE = {
    yes: 'yes',
    no: 'no',
    zoom: 'Edit content in the dialog opened.',
    comments: {
        title: 'Common comments',
        manager: 'Management',
        empty: 'No data.'
    }
};
