export const BATCH_EDIT_DIALOG_LOCALE = {
  title: 'Batch Edit',
  appendText: 'Append new field',
  appendTextTip: 'Append more field to edit',
  okText: 'OK',
  cancelText: 'Cancel',
  field: 'Please select field',
  fieldValue: 'Please input value',
  appendTips: 'append more columns to edit.',
  selected: 'select',
  row: 'row',
  confirmTitle: 'Info',
  neverShow: 'Do not remind again',
  confirmText: 'We will update {0} rows，Are you sure?'
};
