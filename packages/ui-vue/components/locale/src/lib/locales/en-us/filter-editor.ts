export const FILTER_EDITOR_LOCALE = {
    // 取消
    cancelButton: 'Cancel',
    // 确定
    okButton: 'OK',
    // 添加子句
    addWhere: 'Add',
    clear: 'Clear',
    // 置顶
    moveTop: 'Top',
    // 上移
    moveUp: 'Up',
    // 下移
    moveDown: 'Down',
    // 置底
    moveBottom: 'Bottom',
    // 左括号
    leftBrackets: 'Left Brackets',
    // 字段
    field: 'Field Name',
    // 操作符
    operator: 'Operator',
    // 值
    value: 'Value',

    valueType: 'Value type',
    expressType: {
        value: 'Value',
        express: 'Express',
        frontExpress: 'Front Express'
    },
    // 右括号
    rightBrackets: 'Right Brackets',
    // 关系
    relation: 'Relation',
    relationValue: {
        and: 'And',
        or: 'Or'
    },

    // 设计器
    designTab: 'Design',
    // 源代码
    jsonTab: 'JSON',
    // Sql预览
    sqlTab: 'Sql',
    title: 'Filter Designer',
    message: 'Are you sure you want to clear all current data?',
    validate: {
        bracket: 'The brackets do not match, please check',
        relation: 'The condition relationship is incomplete, please check',
        field: 'Condition field is not set, please check'
    }
};


export const ENUM_EDITOR_LOCALE = {
    // 取消
    cancelButton: 'Cancel',
    // 确定
    okButton: 'OK',
    // 添加子句
    addWhere: 'Add',
    clear: 'Clear',
    // 置顶
    moveTop: 'Top',
    // 上移
    moveUp: 'Up',
    // 下移
    moveDown: 'Down',
    // 置底
    moveBottom: 'Bottom',
    title: 'Enum Data Designer',
    message: 'Are you sure you want to clear all current data?',

    value: 'Value',
    name: 'Name'
};
