export const LANGUAGE_LABEL_LOCALE = {
    en: 'English',
    "zh-cn": 'Simplified Chinese',
    "zh-CHS": 'Simplified Chinese',
    "zh-CHT": 'Traditional Chiness',
    ok: 'OK',
    cancel: 'Cancel'
};
