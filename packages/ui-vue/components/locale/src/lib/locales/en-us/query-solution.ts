export const QUERY_SOLUTION_LOCALE = {
    saveAsDialog: {
        queryPlanName: 'Query Plan Name',
        setAsDefault: 'Set as default',
        confirm: 'Confirm',
        cancel: 'Cancel',
        placeholder: 'Please input query solution name',
        pleaseInput: 'Please input query solution name',
        title: 'New Query Solution',
        maxLength: 'Query solution name exceeding nine characters'
    },
    manageDialog: {
        name: 'Name',
        property: 'Property',
        default: 'Default',
        operation: 'Action',
        confirm: 'Confirm',
        cancel: 'Cancel',
        planNameDuplicated: '{0} is duplicated',
        system: 'System Plan',
        personal: 'Personal Plan'
    },
    container: {
        default: 'Default',
        manage: 'Edit',
        arrowUp: 'Fold',
        arrowDown: 'Unfold',
        saveAs: 'Save As',
        empty: 'empty',
        save: 'Save',
        pleaseInput: 'Property \'formId\' is required',
        saveSuccess: 'Query Plan Saved',
        saveFail: 'Query Plan didn\'t save',
        planManage: 'Edit Query Plan',
        clear: 'Clear conditions',
        require: 'Please fill the {fields} before query',
        defaultName: 'Default Scheme',
        histroyName: 'Last Filter',
        sysPresetName: 'Sys Preset Scheme'
    }
};
