export const TREETABLE_LOCALE = {
    emptyMessage: 'Empty Data',
    pagination: {
        previousLabel: 'Prev Page',
        nextLabel: 'Next Page',
        message: '{0} items per page, total {1} items.'
    }
};
