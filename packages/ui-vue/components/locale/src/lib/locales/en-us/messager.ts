export const MESSAGER_LOCALE = {
    yes: 'Yes',
    no: 'No',
    ok: 'OK',
    cancel: 'Cancel',
    title: 'System Information',
    errorTitle: 'Error Information',
    prompt: {
        fontSize: {
            name:  'Font Size',
            small: 'Small',
            middle: 'Middle',
            big: 'Large',
            large: 'Extra Large',
            huge: 'Huge'
        }
    },
    exception: {
        expand: 'Expand',
        collapse: 'Collapse',
        happend: 'Happened Time',
        detail: 'Detail',
        copy: 'Copy details',
        copySuccess: 'Copy succeeded!',
        copyFailed: 'Replication failed!',
        roger: 'Got it.',
    }
};
