export const MULTI_SELECT_LOCALE = {
    leftTitle: 'Unselected',
    rightTitle: 'Selected',
    noDataMoveMessage: 'Please select the data to move.',
    shiftRight: 'Shift right',
    shiftLeft: 'Shift left',
    allShiftRight: 'Shift right all',
    allShiftLeft: 'Shift left all',
    top: 'Placed at the top',
    bottom: 'Placed at the bottom',
    shiftUp: 'Shift up',
    shiftDown: 'Shift down',
    emptyData: 'Empty data',
    filterPlaceholder: 'Filter...'
};
