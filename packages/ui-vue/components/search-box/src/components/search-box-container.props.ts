import { ExtractPropTypes } from "vue";

export const searchBoxContainerProps = {
    data: { type: Array<any>, default: [] }
};

export type SearchBoxContainerProps = ExtractPropTypes<typeof searchBoxContainerProps>;
