import { SetupContext, computed, defineComponent, ref } from "vue";
import { SearchBoxContainerProps, searchBoxContainerProps } from "./search-box-container.props";

export default defineComponent({
    name: 'FSearchBoxContainer',
    props: searchBoxContainerProps,
    emits: ['confirmResult'],
    setup(props: SearchBoxContainerProps, context: SetupContext) {
        const recommendedData = ref(props.data);
        const searchingResultListRef = ref<any>();
        // const recommendedData = [
        //     { id: '01', name: '发票类型', checked: false },
        //     { id: '02', name: '发票代码', checked: false },
        //     { id: '03', name: '开票日期', checked: false },
        //     { id: '04', name: '票价(燃油附加费)', checked: false },
        //     { id: '05', name: '税收分类编号', checked: false }
        // ];

        const searchBoxContainerClass = computed(() => {
            const classObject = {
                'search-box-container': true
            } as Record<string, boolean>;
            return classObject;
        });

        function search(searchingText: string) {
            searchingResultListRef.value?.search(searchingText);
        }

        function onConfirmSearchResult(searchResult: any[]) {
            if (searchResult.length) {
                context.emit('confirmResult', searchResult[0].name);
            }
        }

        context.expose({ search });

        return () => {
            return (
                <div class={searchBoxContainerClass.value}>
                    <div class="search-box-container-title">查询结果</div>
                    <f-list-view ref={searchingResultListRef} data={recommendedData.value} view="SingleView"
                        onClickItem={(result: any) => onConfirmSearchResult(result.data)}
                    > </f-list-view>
                </div>
            );
        };
    }
});
