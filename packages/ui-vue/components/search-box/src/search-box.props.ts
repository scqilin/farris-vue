import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { schemaMapper } from "./schema/schema-mapper";
import searchBoxSchema from './schema/search-box.schema.json';

export const searchBoxProps = {
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },

    popupHost: { type: Object as PropType<any> },

    popupRightBoundary: { type: Object as PropType<any> },

    popupOffsetX: { type: Object as PropType<any> },

    recommendedData: { type: Array<any>, default: [] }
} as Record<string, any>;

export type SearchBoxProps = ExtractPropTypes<typeof searchBoxProps>;

export const propsResolver = createPropsResolver<SearchBoxProps>(searchBoxProps, searchBoxSchema, schemaMapper);
