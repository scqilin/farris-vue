import { DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        return false;
    }

    function hideNestedPaddingInDesginerView() {
        return true;
    }

    return {
        canAccepts,
        hideNestedPaddingInDesginerView
    };
}
