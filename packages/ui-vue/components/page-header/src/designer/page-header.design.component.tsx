import { SetupContext, computed, defineComponent, inject, onMounted, ref } from 'vue';
import { PageHeaderProps, pageHeaderProps } from '../page-header.props';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerRules } from './use-designer-rules';

export default defineComponent({
    name: 'FPageHeaderDesign',
    props: pageHeaderProps,
    emits: ['Click'],
    setup(props: PageHeaderProps, context: SetupContext) {
        const items = ref(props.buttons);
        const onClickToolbarItem = (itemId: string) => {
            context.emit('Click', itemId);
        };

        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div class="f-page-header" ref={elementRef}>
                    <nav class="f-page-header-base">
                        <div class="f-title">
                            <span class="f-title-icon f-text-orna-manage">
                                <i class="f-icon f-icon-page-title-administer"></i>
                            </span>
                            <h4 class="f-title-text">{props.title}</h4>
                        </div>
                        <f-response-toolbar class="col-7" items={items.value} onClick={onClickToolbarItem}></f-response-toolbar>
                    </nav>
                </div>
            );
        };
    }
});
