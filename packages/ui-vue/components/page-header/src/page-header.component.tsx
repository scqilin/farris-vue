import { SetupContext, computed, defineComponent, ref } from "vue";
import { PageHeaderProps, pageHeaderProps } from "./page-header.props";

export default defineComponent({
    name: 'FPageHeader',
    props: pageHeaderProps,
    emits: ['Click'],
    setup(props: PageHeaderProps, context: SetupContext) {
        const items = ref(props.buttons);
        const onClickToolbarItem = (itemId: string) => {
            context.emit('Click', itemId);
        };

        return () => {
            return (
                <div class="f-page-header">
                    <nav class="f-page-header-base">
                        <div class="f-title">
                            <span class="f-title-icon f-text-orna-manage"><i class="f-icon f-icon-page-title-administer"></i></span>
                            <h4 class="f-title-text">{props.title}</h4>
                        </div>
                        <f-response-toolbar class="col-7" items={items.value} onClick={onClickToolbarItem}></f-response-toolbar>
                    </nav>
                </div>
            );
        };
    }
});
