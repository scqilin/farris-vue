import { ExtractPropTypes } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { schemaMapper } from "./schema/schema-mapper";
import pageHeaderSchema from './schema/page-header.schema.json';

export const pageHeaderProps = {
    icon: { type: String, default: '' },
    title: { type: String, default: '' },
    buttons: { type: Array<any>, default: [] }
} as Record<string, any>;
export type PageHeaderProps = ExtractPropTypes<typeof pageHeaderProps>;

export const propsResolver = createPropsResolver<PageHeaderProps>(pageHeaderProps, pageHeaderSchema, schemaMapper);
