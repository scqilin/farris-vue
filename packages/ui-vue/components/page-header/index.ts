import { App } from 'vue';
import PageHeader from './src/page-header.component';
import PageHeaderDesign from './src/designer/page-header.design.component';
import { propsResolver } from './src/page-header.props';

export * from './src/page-header.props';
export { PageHeader };

export default {
    install(app: App): void {
        app.component(PageHeader.name, PageHeader);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['page-header'] = PageHeader;
        propsResolverMap['page-header'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['page-header'] = PageHeaderDesign;
        propsResolverMap['page-header'] = propsResolver;
    }
};
