import FAccordion from '../../../accordion';
import FButtonEdit from '../../../button-edit';
import FComboList from '../../../combo-list';
import FContentContainer from '../../../content-container';
import FDataGrid from '../../../data-grid';
import FDatePicker from '../../../date-picker';
import FDynamicForm from '../../../dynamic-form';
import FInputGroup from '../../../input-group';
import FNumberSpinner from '../../../number-spinner';
import FPageHeader from '../../../page-header';
import FQuerySolution from '../../../query-solution';
import FResponseLayout from '../../../response-layout';
import FSection from '../../../section';
import FSplitter from '../../../splitter';
import FTab from '../../../tabs';
import FComponent from '../../../component';

const componentMap: Record<string, any> = {};
const componentPropsConverter: Record<string, any> = {};

FAccordion.registerDesigner(componentMap, componentPropsConverter);
FButtonEdit.registerDesigner(componentMap, componentPropsConverter);
FComboList.registerDesigner(componentMap, componentPropsConverter);
FContentContainer.registerDesigner(componentMap, componentPropsConverter);
FDataGrid.registerDesigner(componentMap, componentPropsConverter);
FDatePicker.registerDesigner(componentMap, componentPropsConverter);
FDynamicForm.registerDesigner(componentMap, componentPropsConverter);
FInputGroup.registerDesigner(componentMap, componentPropsConverter);
FNumberSpinner.registerDesigner(componentMap, componentPropsConverter);
FPageHeader.registerDesigner(componentMap, componentPropsConverter);
FQuerySolution.registerDesigner(componentMap, componentPropsConverter);
FResponseLayout.registerDesigner(componentMap, componentPropsConverter);
FSection.registerDesigner(componentMap, componentPropsConverter);
FSplitter.registerDesigner(componentMap, componentPropsConverter);
FTab.registerDesigner(componentMap, componentPropsConverter);
FComponent.registerDesigner(componentMap, componentPropsConverter);

export { componentMap, componentPropsConverter };
