import { SetupContext, computed, defineComponent } from 'vue';
import { DesignerPlaceholderPropsType, designerPlaceholderProps } from '../composition/props/designer-placeholder.props';

export default defineComponent({
    name: 'FDesignerPlaceholder',
    props: designerPlaceholderProps,
    emits: [],
    setup(props: DesignerPlaceholderPropsType, context: SetupContext) {
        const designerPlaceholderClass = computed(() => {
            const classObject = {
                'drag-and-drop-alert': true,
                alert: true,
                'alert-info': true,
                'no-drag': true,
                'w-100': true
            } as Record<string, boolean>;
            return classObject;
        });

        const designerPlaceholderStyle = computed(() => {
            const styleObject = {
                height: '60px',
                display: 'flex',
                'justify-content': 'center',
                'align-items': 'center'
            } as Record<string, any>;
            return styleObject;
        });

        return () => (
            <div
                class={designerPlaceholderClass.value}
                style={designerPlaceholderStyle.value}
                role="alert"
                data-noattach="true"
                data-position="0">
                拖拽组件到这里
            </div>
        );
    }
});
