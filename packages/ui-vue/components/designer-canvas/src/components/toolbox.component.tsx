import { SetupContext, defineComponent, ref, watch } from 'vue';
import { ToolboxPropsType, toolboxProps } from '../composition/props/toolbox.props';
import toolboxItems from './toolbox.json';
import { ToolboxCategory, ToolboxItem } from '../types';

import '../composition/class/toolbox.css';

export default defineComponent({
    name: 'FDesignerToolbox',
    props: toolboxProps,
    emits: [],
    setup(props: ToolboxPropsType, context: SetupContext) {
        const controlCategoryList = ref<ToolboxCategory[]>(toolboxItems);
        const dragularCompostion = ref(props.dragula);

        function onClickCardHeader(payload: MouseEvent, category: any) {
            category.isHide = !category.isHide;
        }

        function getCardHeaderIconClass(category: any) {
            const classObject = {
                'f-cion': true,
                'f-icon-arrow-60-down': !category.isHide,
                'f-icon-arrow-e': category.isHide
            } as Record<string, boolean>;
            return classObject;
        }

        function renderCategoryCardHeader(category: ToolboxCategory) {
            return (
                <div class="card-header" onClick={(payload: MouseEvent) => onClickCardHeader(payload, category)}>
                    <div class="panel-item-title">
                        <div class="f-section-formgroup-legend">
                            <div class="f-header px-2 col-form-label">
                                <div class="f-toolbar mr-2">
                                    <span class={getCardHeaderIconClass(category)}></span>
                                </div>
                                <div class="f-title f-utils-fill d-flex justify-content-between">
                                    <span>{category.name}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        function getControlTileClass(toolboxItem: ToolboxItem) {
            const classObject = {
                'd-none': toolboxItem.dependentParent || toolboxItem.hideInControlBox,
                controlPanel: true,
                'drag-copy': true,
                'no-drag': toolboxItem.disable
            } as Record<string, boolean>;
            return classObject;
        }

        function getToolboxItemClass(toolboxItem: ToolboxItem) {
            const classObject = {
                farrisControlIcon: true,
                'fd-i-Family': true
            } as Record<string, boolean>;
            const toolboxItemTypicalClassName = `fd_pc-${toolboxItem.icon || toolboxItem.type}`;
            classObject[toolboxItemTypicalClassName] = true;
            return classObject;
        }

        function renderControlTile(toolboxItem: ToolboxItem, category: ToolboxCategory) {
            return (
                <div
                    class={getControlTileClass(toolboxItem)}
                    data-sourceType="control"
                    data-controlType={toolboxItem.type}
                    data-controlTypeName={toolboxItem.name}
                    data-category={toolboxItem.category}
                    data-feature={toolboxItem.feature ? JSON.stringify(toolboxItem.feature) : ''}
                    data-fieldType={toolboxItem.fieldType}
                    data-templateId={toolboxItem.id}
                    data-templateCategory={toolboxItem.templateCategory}
                    hidden={category.isHide}>
                    <div>
                        <div class={getToolboxItemClass(toolboxItem)}></div>
                        <div class="controlName">{toolboxItem.name}</div>
                    </div>
                </div>
            );
        }

        function renderCategoryCardBody(category: ToolboxCategory) {
            return (
                <div class="card-body px-2 py-0 border-0 controlCategory no-drop">
                    {category.items.map((toolboxItem: any) => renderControlTile(toolboxItem, category))}
                </div>
            );
        }

        function renderCategoryCard(category: ToolboxCategory) {
            return (
                !category.hideInControlBox && (
                    <div class="farris-panel-item card border-0">
                        {renderCategoryCardHeader(category)}
                        {renderCategoryCardBody(category)}
                    </div>
                )
            );
        }

        watch(
            () => props.dragula,
            (newValue: any) => {
                dragularCompostion.value = newValue;
                dragularCompostion.value?.attachToolbox();
            }
        );

        return () => {
            return (
                <div class="controlBox f-utils-fill-flex-column">
                    <div class="farris-panel f-utils-fill-flex-column" style="overflow:auto;">
                        {controlCategoryList.value.map((category: any) => {
                            return renderCategoryCard(category);
                        })}
                    </div>
                </div>
            );
        };
    }
});
