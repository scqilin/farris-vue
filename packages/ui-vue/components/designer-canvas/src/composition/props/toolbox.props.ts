import { ExtractPropTypes } from 'vue';

export const toolboxProps = {
    id: { type: String, default: '' },
    dragula: { type: Object }
};

export type ToolboxPropsType = ExtractPropTypes<typeof toolboxProps>;
