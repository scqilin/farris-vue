import { Ref, ref } from "vue";
import { DesignerHTMLElement, UseDesignerRules } from "../types";
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../types";
import { getSchemaByType } from '../../../../dynamic-resolver/src/schema-resolver';

export function useDesignerComponent(
    elementRef: Ref<HTMLElement>,
    designItemContext: DesignerItemContext,
    designerRules?: UseDesignerRules
): Ref<DesignerComponentInstance> {
    const isInFixedContextRules = designerRules?.checkIsInFixedContextRules();
    const styles = (designerRules && designerRules.getStyles && designerRules.getStyles()) || '';
    /**
     * 校验组件是否支持移动
     */
    function checkCanMoveComponent(): boolean {
        if (designItemContext?.schema.componentType) {
            return false;
        }
        if (isInFixedContextRules) {
            return false;
        }
        return true;
    }

    /**
     * 校验组件是否支持选中父级
     */
    function checkCanSelectParentComponent(): boolean {
        return false;
    }

    /**
     * 校验组件是否支持删除
     */
    function checkCanDeleteComponent() {
        if (designItemContext?.schema.componentType) {
            return false;
        }
        if (isInFixedContextRules) {
            return false;
        }
        return true;
    }

    /**
     * 判断在可视化区域中是否隐藏容器间距和线条
     */
    function hideNestedPaddingInDesginerView() {
        if (isInFixedContextRules) {
            return true;
        }
        return false;
    }

    /**
         * 获取组件在表单DOM中所属的Component的实例
         * @param cmpInstance 组件实例
         */
    function getBelongedComponentInstance(componentInstance: DesignerComponentInstance): DesignerComponentInstance | null {
        if (!componentInstance) {
            return null;
        }
        if (componentInstance.schema.type === 'Component') {
            return componentInstance;
        }
        const grandParent = getBelongedComponentInstance(componentInstance.parent as DesignerComponentInstance);
        if (grandParent) {
            return grandParent;
        }
        return null;
    }

    /**
     * 判断是否可以接收拖拽新增的子级控件
     * @param data 新控件的类型、所属分类
     * @returns boolean
     */
    function canAccepts(element: DesignerHTMLElement, target?: DesignerHTMLElement, sourceContainer?: DesignerHTMLElement) {
        // const result = designItemContext?.componentInstance.value.canAccepts(element, target);
        // if (!result) {
        //     return false;
        // }
        return !!designerRules && designerRules.canAccepts();
    }

    function getDraggingDisplayText() {
        return designItemContext?.schema.label || designItemContext?.schema.title || designItemContext?.schema.name;
    }

    /**
     * 控件可以拖拽到的最外层容器，用于限制控件向外层容器拖拽的范围。不写则不限制
     */
    function getDragScopeElement(): HTMLElement | undefined {
        return undefined;
    }

    /**
     * 移动控件后事件：在可视化设计器中，将现有的控件移动到容器中
     * @param el 移动的源DOM结构
     */
    function onAcceptMovedChildElement(element: DesignerHTMLElement, soureeElement?: DesignerHTMLElement) {
        if (!soureeElement) {
            return;
        }
        designerRules?.onAcceptMovedChildElement(soureeElement);
    }

    /**
     * 当前容器接收新创建的子控件
     * @param el 移动的源DOM结构
     */
    function onAcceptNewChildElement(el: DesignerHTMLElement, targetPosition: number): ComponentSchema {
        const componentType = String(el.getAttribute('data-controltype'));
        const componentSchema = getSchemaByType(componentType);
        return componentSchema;
    }
    /**
     * 当前容器接收新创建的子控件
     * @param el 移动的源DOM结构
     */
    /**
     * 插入的ComponentSchema结构
     * @param componentSchema ComponentSchema
     */
    function onAddNewChildElement(componentSchema: ComponentSchema): number {
        if (componentSchema.contents) {
            return componentSchema.contents.length;
        }
        return -1;
    }

    /**
     * 移动内部控件后事件：在可视化设计器中，将现有的控件移动到容器中
     * @param el 移动的源DOM结构
     */
    function onChildElementMovedOut(el: HTMLElement) {

    }

    const componentInstance: DesignerComponentInstance = {
        canMove: checkCanMoveComponent(),
        canSelectParent: checkCanSelectParentComponent(),
        canDelete: checkCanDeleteComponent(),
        canNested: !hideNestedPaddingInDesginerView(),
        contents: designItemContext?.schema.contents,
        elementRef,
        parent: designItemContext?.parent?.componentInstance.value,
        schema: designItemContext?.schema,
        styles,
        canAccepts,
        getBelongedComponentInstance,
        getDraggingDisplayText,
        getDragScopeElement,
        onAcceptMovedChildElement,
        onAcceptNewChildElement,
        onAddNewChildElement,
        onChildElementMovedOut,
        triggerBelongedComponentToMoveWhenMoved: !!designerRules && designerRules.triggerBelongedComponentToMoveWhenMoved
    };

    return ref(componentInstance);

}
