import { Ref } from "vue";
import { DesignerHTMLElement, DraggingResolveContext } from "./composition/types";

export interface ComponentSchema {

    /** 设计时使用 */
    key?: string;

    id: string;

    type: string;

    contents?: ComponentSchema[];

    // 其他属性
    [propName: string]: any;
}

export interface DesignerComponentInstance {

    canMove: boolean;

    canSelectParent: boolean;

    canDelete: boolean;

    canNested: boolean;

    contents?: ComponentSchema[];

    elementRef: Ref<HTMLElement>;

    parent?: Ref<DesignerComponentInstance>;

    schema: ComponentSchema;

    styles?: string;

    canAccepts: (draggingContext: DraggingResolveContext) => boolean;

    getBelongedComponentInstance: (componentInstance: Ref<DesignerComponentInstance>) => DesignerComponentInstance | null;

    getDraggingDisplayText: () => string;

    getDragScopeElement: () => HTMLElement | undefined;

    /**
     * 移动内部控件后事件：在可视化设计器中，容器接收控件后事件
     * @param element 移动的源DOM结构
     */
    onAcceptMovedChildElement: (element: DesignerHTMLElement, soureElement?: DesignerHTMLElement) => void;

    onAcceptNewChildElement: (el: DesignerHTMLElement, targetPosition: number) => ComponentSchema | null;

    onChildElementMovedOut: (element: HTMLElement) => void;

    /**
     * 组件在拖拽时是否需要将所属的Component一起拖拽，用于form、data-grid等控件的拖拽
     */
    triggerBelongedComponentToMoveWhenMoved?: Ref<boolean>;

    /**
     * 组件在被移除时是否需要将所属的Component一起移除，用于form、data-grid等控件的拖拽
     */
    triggerBelongedComponentToDeleteWhenDeleted?: Ref<boolean>;

}

export interface DesignerItemContext {

    designerItemElementRef: Ref<HTMLElement>;

    componentInstance: Ref<DesignerComponentInstance>;

    schema: ComponentSchema;

    parent?: DesignerItemContext;

}

export interface ToolboxItem {
    id: string;
    type: string;
    name: string;
    category: string;
    icon?: string;
    feature?: any;
    dependentParent?: boolean;
    hideInControlBox?: boolean;
    disable?: boolean;
    fieldType?: string;
    templateCategory?: string;
}

export interface ToolboxCategory {
    type: string;
    name: string;
    items: ToolboxItem[];
    hideInControlBox?: boolean;
    isHide?: boolean;
};
