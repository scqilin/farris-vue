/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef } from 'vue';

export interface UseImage {
    acceptTypes: ComputedRef<string>;

    imageSource: ComputedRef<string>;

    imageTitle: ComputedRef<string>;

    onClickImage: () => void;
}

export interface ImageFile {
    size: number;
    name: string;
    type: string;
    lastModified?: string;
    lastModifiedDate?: Date;
    state?: string;
    base64?: string;
}
