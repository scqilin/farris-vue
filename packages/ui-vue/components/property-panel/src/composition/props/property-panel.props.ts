/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

type ShowMode = 'panel' | 'sidebar';

export const propertyPanelProps = {

    width: { type: String, default: '300px' },

    height: { type: Number, default: 10 },

    isWidePanel: { type: Boolean, default: false },

    /** 是否启用搜索 */
    enableSearch: { type: Boolean, default: true },

    /** 使用模式 */
    mode: { type: String as PropType<ShowMode>, default: 'panel' },

    /** 是否持有面板的隐藏显示状态 */
    isPersitOpenState: { type: Boolean, default: false },

    /** isPersitOpenState=true时，控制面板是否隐藏显示 */
    isShowPanel: { type: Boolean, default: false },

    /** 属性类型 */
    propertyConfig: { type: Array<object> },

    /** 属性值 */
    propertyData: { type: Object, default: {} },

    /** 是否展示关闭按钮 */
    showCloseBtn: { type: Boolean, default: false },

    /** 当前选中的标签页id */
    selectedTabId: { type: String, default: '' },

    /** 是否是白色主题 */
    isWhiteTheme: { type: Boolean, default: true }

};
export type PropertyPanelProps = ExtractPropTypes<typeof propertyPanelProps>;
