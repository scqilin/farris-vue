/**
 * 属性类型
 */
export enum PropertyType {
    /** 字符串 */
    string = 'string',

    /** 布尔，下拉选择 */
    boolean = 'boolean',

    /** 数字 */
    number = 'number',

    /** 下拉选择：单选 */
    select = 'select',

    /** 已废弃，请使用editableSelect */
    boolOrExp = 'boolOrExp',

    /** 可编辑的下拉选择：单选，并且可编辑 */
    editableSelect = 'editableSelect',

    /** 下拉多选 */
    multiSelect = 'multiSelect',

    /** 日期 */
    date = 'date',

    /** 日期时间 */
    datetime = 'datetime',

    /** 模态窗，自定义组件 */
    modal = 'modal',

    /** 级联 */
    cascade = 'cascade',

    /** 自定义组件 */
    custom = 'custom',

    /** 多功能属性编辑器，支持常量、变量、自定义、表达式等场景 */
    unity = 'unity',

    /** 事件编辑器集成，支持导入命令、参数编辑等场景 */
    events = 'events',

    /** 开关类编辑器，适用于布尔值属性 */
    switch = 'switch',

    /** 多语言输入框 */
    multiLanguage = 'multiLanguage'
}

/** 属性值转换器，返回模态框类属性文本框内的显示内容 */
export interface TypeConverter {
    // 由模态框转为属性框中展示的值
    convertTo(data: any, params?: any): string;
}

export interface KeyMap {
    key: any;
    value: any;
}
