/* eslint-disable radix */
/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, onMounted } from 'vue';
import { propertyPanelItemProps, PropertyPanelItemProps } from '../composition/props/property-panel-item.props';
import '../composition/class/property-panel-item.css';
import FDynamicFormGroup from '../../../dynamic-form/src/component/dynamic-form-group/dynamic-form-group.component';

export default defineComponent({
    name: 'FPropertyPanelItem',
    props: propertyPanelItemProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: PropertyPanelItemProps, context: SetupContext) {
        const elementConfig = ref(props.elementConfig);
        const elementValue = ref(props.elementValue);

        const editor = ref({
            type: 'date-picker',
            value:'check'
        });

        function refresh() {
            /** 控件类型 */
            let controlComponent = 'number-spinner';
            // 显示格式
            const modelValue = elementConfig.value?.modelValue;
            const propertyType = elementConfig.value?.propertyType;
            switch (propertyType) {
            case 'switch': {
                controlComponent = 'switch';
                break;
            }
            case 'boolean': {
                controlComponent = 'text';
                break;
            }
            case 'select': {
                controlComponent = 'combo-list';
                break;
            }
            case 'multiSelect': {
                controlComponent = 'combo-list';
                break;
            }
            case 'modal': {
                controlComponent = 'modal';
                break;
            }
            case 'number': {
                controlComponent = 'number-spinner';
                break;
            }
            case 'string': {
                controlComponent = 'text';
                break;
            }
            case 'date':
            case 'datetime': {
                controlComponent = 'date-picker';
                break;
            }
            }
            editor.value.type = controlComponent;
            editor.value.value = modelValue;
            // 默认值
            if (Object.keys(elementConfig).indexOf('defaultValue') > -1) {
                if (elementValue.value === undefined || elementValue.value === null) {
                    elementValue.value = elementConfig.value.defaultValue;
                }
            }
            // 是否显示
            if (Object.keys(elementConfig).indexOf('visible') < 0) {
                elementConfig.value.visible = true;
            }
        }
        onMounted(() => {
            refresh();
        });

        return () => {
            return (
                <>
                    <div class={['farris-group-wrap property-item', { hidden: !elementConfig.value.visible }]}>
                        <div
                            class={[
                                'form-group farris-form-group row-item',
                                { 'd-flex': elementConfig.value?.propertyType === 'switch' }
                            ]}></div>
                        <div class="component">
                            <FDynamicFormGroup
                                // customClass={conditionItemClass.value}
                                label={elementConfig.value?.propertyName}
                                editor={editor.value}
                                modelValue={editor.value.value}
                                // v-model={(condition.value as ConditionValue).value}
                            ></FDynamicFormGroup>
                        </div>
                    </div>
                </>
            );
        };
    }
});
