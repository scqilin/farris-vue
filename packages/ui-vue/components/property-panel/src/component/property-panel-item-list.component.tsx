/* eslint-disable radix */
/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, SetupContext, ref, withModifiers, watch, onBeforeUnmount, onMounted, onBeforeMount } from 'vue';
import { propertyPanelItemListProps, PropertyPanelItemListProps } from '../composition/props/property-panel-item-list.props';
import FPropertyPanelItem from '../component/property-panel-item.component';

import '../composition/class/property-panel-item-list.css';

export default defineComponent({
    name: 'FPropertyPanelItemList',
    props: propertyPanelItemListProps,
    emits: ['valueChanged', 'submitModal', 'triggerRefreshPanel'] as (string[] & ThisType<void>) | undefined,
    setup(props: PropertyPanelItemListProps, context: SetupContext) {
        /** 某一分类下的属性配置 */
        const category = ref(props.category);
        /** 属性值 */
        const propertyData = ref(props.propertyData);

        // @ViewChildren(FPropertyItemComponent) fitems: Array<FPropertyItemComponent>;

        /** 实际属性值 */
        let data: any;

        function handleExpand(propItem: any) {
            propItem.isExpand = !propItem.isExpand;
            return propItem;
        }
        function refreshPanel() {
            context.emit('triggerRefreshPanel');
        }
        /** 级联属性的汇总信息
         * @param propItem
         * @param valueObject
         */
        function cascadeValueStringify(propItem: any, valueObject: any) {
            if (!propItem || !propItem.cascadeConfig) {
                return;
            }
            if (!valueObject) {
                propItem.cascadeSummary = '';
                return;
            }
            if (!propItem.cascadeConverter || !propItem.cascadeConverter.convertTo) {
                propItem.cascadeSummary = JSON.stringify(valueObject);
            } else {
                propItem.cascadeSummary = propItem.cascadeConverter.convertTo(valueObject, propItem.cascadeConfig);
            }
        }
        /**
         * 适配属性
         */
        function adaptProperties() {
            category.value.properties.forEach((propItem: any) => {
                if (propItem.propertyType === 'cascade') {
                    cascadeValueStringify(propItem, data[propItem.propertyID]);
                    if (Object.keys(propItem).indexOf('visible') < 0) {
                        propItem.visible = true;
                    }
                }
            });
        }
        function hideCascadeTitle(propItem: any) {
            if (!propItem.hideCascadeTitle) {
                return (
                    <div class="farris-input-wrap">
                        <input type="input" class="form-control form-control-sm" value={propItem.cascadeSummary} readonly></input>
                    </div>
                );
            }
        }
        function handleExpandBtn(propItem: any) {
            return (
                <div class="landscape">
                    <button class={['btn f-btn-collapse-expand f-btn-mx px-1', { 'f-state-expand': propItem.isExpand }]}>
                        <span></span>
                    </button>
                </div>
            );
        }
        /** 处理面板数据 */
        function handleData() {
            // 若分类下有propertyData，则取分类下的propertyData；否则取整体的propertyData
            if (category.value.propertyData && category.value.enableCascade) {
                data = category.value.propertyData;
            } else {
                data = propertyData.value;
            }
        }
        function formgroup(propItem: any) {
            return (
                <div class="f-section-formgroup-legend">
                    <div class="f-header px-0  col-form-label" style="font-size: inherit;">
                        <div class="f-title f-utils-fill">{propItem.propertyName}</div>
                        <div class="f-toolbar vertical">
                            <button class={['btn f-btn-collapse-expand f-btn-mx', { 'f-state-expand': propItem.isExpand }]}>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
            );
        }
        /** 带级联属性的类型 */
        function cascadeType(propItem: any) {
            if (propItem.propertyType === 'cascade') {
                return (
                    <div class="farris-panel">
                        <div class={['propertyCascadeItem farris-panel-item card', { hidden: !propItem.visible }]}>
                            <div class="card-header" onClick={handleExpand(propItem)} >
                                {/* [attr.propertyId]="propItem.propertyID" */}
                                <div class="panel-item-title">
                                    <div class="form-group farris-form-group line-item">
                                        {formgroup(propItem)}
                                        {hideCascadeTitle(propItem)}
                                        {handleExpandBtn(propItem)}
                                    </div>
                                </div>
                            </div>
                            <div class={['card-body', { hidden: !propItem.isExpand }]}>
                                {propItem.cascadeConfig.map((cascadeItem: any) => {
                                    <div class="px-2">
                                        <FPropertyPanelItem
                                            elementValue={
                                                data[propItem.propertyID] ? data[propItem.propertyID][cascadeItem.propertyID] : ''
                                            }
                                            elementConfig={cascadeItem}></FPropertyPanelItem>

                                        {/* @valueChanged="_cascadeitemChanged($event,propItem)"
                               @submitModal="__cascadeitemSubmitModal($event, propItem)"
                               @triggerRefreshPanel="refreshPanel"*/}
                                    </div>;
                                })}
                            </div>
                        </div>
                    </div>
                );
            }
        }
        function notCascadeType(propItem: any) {
            if (propItem.propertyType !== 'cascade') {
                return (
                    <div class="px-2 mb-2">
                        <FPropertyPanelItem
                            elementValue={data[propItem.propertyID]}
                            elementConfig={propItem}
                            //   @valueChanged="_itemChanged"
                            //   @submitModal="_submitModal"
                            //   @triggerRefreshPanel="refreshPanel"
                        ></FPropertyPanelItem>
                    </div>
                );
            }
        }
        onBeforeMount(() => {
            handleData();
            adaptProperties();
        });

        watch(props.category || props.propertyData, () => {
            handleData();
            adaptProperties();
        });

        return () => {
            return (
                <>
                    {category.value.properties.map((propItem: any) => {
                        return (
                            <>
                                {notCascadeType(propItem)}
                                {cascadeType(propItem)}
                            </>
                        );
                    })}
                </>
            );
        };
    }
});
