import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class DateRangeValue implements ConditionValue {

    editorType: EditorType = 'date-range';

    begin = '';

    end = '';

    constructor(initialData: { begin: string; end: string } = { begin: '', end: '' }) {
        this.begin = initialData.begin;
        this.end = initialData.end;
    }

    clear(): void {
        this.begin = '';
        this.end = '';
    }

    getValue() {
        if (!this.begin || !this.end) {
            return '';
        }
        return `${this.begin}~${this.end}`;
    }

    setValue(value: { dataRange: string; delimiter: string }): void {
        if (value.dataRange) {
            this.begin = value.dataRange.split(value.delimiter)[0];
            this.end = value.dataRange.split(value.delimiter)[1];
        } else {
            this.clear();
        }
    }

    isEmpty(): boolean {
        return !this.begin || !this.end;
    }
}
