import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class NumberSpinnerValue implements ConditionValue {

    editorType: EditorType = 'number-spinner';

    value: number | null | undefined;

    constructor(initialData: { value: string } = { value: '' }) {
        this.value = initialData.value == null ? null : parseFloat(initialData.value);
    }

    clear(): void {
        this.value = undefined;
    }

    getValue() {
        return this.value;
    }

    setValue(value: any): void {
        // throw new Error("Method not implemented.");
        this.value = isNaN(parseFloat(value)) ? null : value;
    }

    isEmpty(): boolean {
        return this.value == null;
    }
}
