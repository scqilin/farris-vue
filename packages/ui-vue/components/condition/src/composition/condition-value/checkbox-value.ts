import { EditorType } from "../../../../dynamic-form";
import { ConditionValue } from "./types";

export class CheckBoxValue implements ConditionValue {

    editorType: EditorType = 'check-box';

    value: boolean[];

    constructor(initialData: { value: boolean[] } = { value: [] }) {
        this.value = initialData.value;
    }

    clear(): void {
        this.value = [];
    }

    setValue(value: boolean[]) {
        this.value = value;
    }

    getValue() {
        return this.value;
    }

    isEmpty(): boolean {
        return this.value.length === 0;
    }
}
