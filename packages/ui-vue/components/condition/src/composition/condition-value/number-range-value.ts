import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class NumberRangeValue implements ConditionValue {

    editorType: EditorType = 'number-range';

    begin: number | null;

    end: number | null;

    constructor(initialData: {
        begin: string | number | null;
        end: string | number | null;
    } = { begin: null, end: null }
    ) {
        this.begin = initialData.begin == null ? null : parseFloat(initialData.begin as string);
        this.end = initialData.end == null ? null : parseFloat(initialData.end as string);
    }

    clear(): void {
        this.begin = null;
        this.end = null;
    }

    getValue() {
        return {
            begin: this.begin,
            end: this.end
        };
    }

    setValue(value: {
        begin: string | number | null;
        end: string | number | null;
    }): void {
        this.begin = value.begin == null ? null : parseFloat(value.begin as string);
        this.end = value.end == null ? null : parseFloat(value.end as string);
    }

    isEmpty(): boolean {
        return (this.begin == null && this.end == null);
    }

}
