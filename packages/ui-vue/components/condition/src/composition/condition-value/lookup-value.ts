import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class LookupValue implements ConditionValue {

    editorType: EditorType = 'lookup';

    textValue: string;

    value: any[];

    valueField: string;

    // 帮助的值是否为手动输入的任意值，对应帮助的任意输入属性nosearch
    isInputText: boolean;

    constructor(initialData: {
        textValue: string;
        value: any;
        valueField: string;
        isInputText: boolean;
    } = { textValue: '', value: [], valueField: '', isInputText: false }) {
        this.value = initialData.value;
        this.valueField = initialData.valueField;
        this.textValue = initialData.textValue;
        this.isInputText = initialData.isInputText;
    }

    clear(): void {
        this.value = [];
        this.valueField = '';
        this.textValue = '';
    }

    getPropValue(item: any, fieldPath: Array<string>): any {
        if (fieldPath.length > 1) {
            const pathToken = fieldPath.shift() as string;
            return item[pathToken] ? this.getPropValue(item[pathToken], fieldPath) : null;
        }
        return item[fieldPath[0]];
    }

    getTextValue(textField: string): string {
        const fieldPath = textField.split('.');
        const displayValues = this.value.map(helpItem => this.getPropValue(helpItem, fieldPath));
        return displayValues && displayValues.length ? displayValues.join(',') : '';
    }

    getValue(): string {
        const fieldPath = this.valueField.split('.');
        const values = this.value.map(helpItem => this.getPropValue(helpItem, fieldPath));
        return values && values.length ? values.join(',') : '';
    }

    setValue(value: any): void {
        throw new Error("Method not implemented.");
    }

    isEmpty(): boolean {
        return !this.value.length;
    }
}
