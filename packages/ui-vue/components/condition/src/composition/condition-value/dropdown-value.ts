import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class ComboListValue implements ConditionValue {

    editorType: EditorType = 'combo-list';

    value: any;// 旧结构{Type: '', Content: {value: string, name: string}} 新结构Array[{value: string, name: string}]

    displayText: string;// 旧结构无，新结构为选中的value值，多选是以,分割的字符串

    constructor(initialData: { value: any; dispalyText: string } = { value: '', dispalyText: '' }) {
        this.displayText = initialData.dispalyText;
        this.value = initialData.value;
    }

    clear(): void {
        this.value = [];
        this.displayText = '';
    }

    getValue() {
        return this.displayText;
    }

    setValue(data: { value: any[]; dispalyText: string }) {
        this.displayText = data.dispalyText;
        this.value = data.value;
    }

    isEmpty(): boolean {
        return !this.displayText;
    }
}
