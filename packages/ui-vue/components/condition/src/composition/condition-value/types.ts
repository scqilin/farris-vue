import { EditorType } from '../../../../dynamic-form';

export interface ConditionValue {

    clear(): void;

    editorType: EditorType;

    value?: any;

    getValue(): any;

    setValue(value: any): void;

    isEmpty(): boolean;
}
