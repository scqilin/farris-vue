import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class RadioGroupValue implements ConditionValue {

    editorType: EditorType = 'radio-group';

    value: any;

    constructor(initialData: { value: any } = { value: null }) {
        this.value = initialData.value;
    }

    clear(): void {
        this.value = undefined;
    }

    getValue(): any {
        return this.value;
    }

    setValue(value: any): void {
        this.value = value;
    }

    isEmpty(): boolean {
        return !this.value;
    }
}
