import { cloneDeep } from 'lodash-es';
import { ConditionValue } from "./types";
import { EditorType } from '../../../../dynamic-form';

export class ComboLookupValue implements ConditionValue {

    editorType: EditorType = 'combo-lookup';

    textValue: string;

    value: string[];

    valueField: string;

    constructor(initialData: { textValue: string; value: string[]; valueField: string } = { textValue: '', value: [], valueField: '' }) {
        this.textValue = initialData.textValue;
        this.value = initialData.value;
        this.valueField = initialData.valueField;
    }

    clear(): void {
        this.value = [];
        this.valueField = '';
        this.textValue = '';
    }

    getPropValue(helpItem: any, args: Array<string>): any {
        if (args.length > 1) {
            const arg = args.shift() as string;
            return helpItem[arg] ? this.getPropValue(helpItem[arg], args) : null;
        }
        return helpItem[args[0]];
    }

    getTextValue(textField: string): string {
        const args = textField.split('.');
        const textArr = this.value.map(helpItem => this.getPropValue(helpItem, cloneDeep(args)));
        return textArr && textArr.length ? textArr.join(',') : '';
    }

    getValue(): string {
        const args = this.valueField.split('.');
        const valueArr = this.value.map(helpItem => this.getPropValue(helpItem, cloneDeep(args)));
        return valueArr && valueArr.length ? valueArr.join(',') : '';
    }

    setValue(value: any): void {
        throw new Error("Method not implemented.");
    }

    isEmpty(): boolean {
        return !this.valueField;
    }
}
