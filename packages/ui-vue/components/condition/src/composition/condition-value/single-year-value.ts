import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class YearPickerValue implements ConditionValue {

    editorType: EditorType = 'year-picker';

    value: string | undefined;

    constructor(initialData: { value: string } = { value: '' }) {
        this.value = initialData.value;
    }

    clear(): void {
        this.value = undefined;
    }

    getValue() {
        return this.value;
    }

    setValue(value: any): void {
        this.value = value.formatted;
    }

    isEmpty(): boolean {
        return !this.value;
    }
}
