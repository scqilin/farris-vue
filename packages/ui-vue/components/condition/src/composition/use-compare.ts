import { SetupContext } from "vue";
import { ConditionProps } from "../condition.props";
import { FieldConfig, Condition } from "../types";
import { UseCompare, UseFieldConfig } from "./types";

enum CompareType {
    Equal = '0',
    NotEqual = '1',
    Greater = '2',
    GreaterOrEqual = '3',
    Less = '4',
    LessOrEqual = '5',
    Like = '6',
    LikeStartWith = '7',
    LikeEndWith = '8'
    // NotLike = 9,
    // NotLikeStartWith = 10,
    // NotLikeEndWith = 11,
    // Is = 12,
    // IsNot = 13,
    // In = 14,
    // NotIn = 15
}

const CompareTypeName = [
    {
        value: '0',
        name: '等于',
    },
    {
        value: '1',
        name: '不等于',
    },
    {
        value: '2',
        name: '大于',
    },
    {
        value: '3',
        name: '大于等于',
    },
    {
        value: '4',
        name: '小于',
    },
    {
        value: '5',
        name: '小于等于',
    },
    {
        value: '6',
        name: '包含',
    },
    {
        value: '7',
        name: '开始是',
    },
    {
        value: '8',
        name: '结束是',
    }
];

const CompareTypeInEditor = {
    'button-edit': ['0', '1', ' 6', '7', '8'],
    'check-box': ['0'],
    'combo-list': ['0', '1'],
    'combo-lookup': ['0', '1', ' 6', '7', '8'],
    'date-picker': ['0', '1', '2', '3', '4', '5'],
    'date-range': [],
    'datetime-picker': ['0', '1', '2', '3', '4', '5'],
    'datetime-range': [],
    'month-picker': ['0', '1', '2', '3', '4', '5'],
    'month-range': [],
    'year-picker': ['0', '1', '2', '3', '4', '5'],
    'year-range': [],
    'input-group': ['0', '1', '6', '7', '8'],
    'lookup': ['0', '1'],
    'number-range': [],
    'number-spinner': ['0', ' 1', '2', '3', '4', '5'],
    'radio-group': ['0'],
    'text': ['0', '1', ' 6', '7', '8']
};

export {
    CompareType,
    CompareTypeName,
    CompareTypeInEditor
};

export function useCompare(
    props: ConditionProps,
    context: SetupContext,
    useFieldComposition: UseFieldConfig
): UseCompare {

    const { fieldMap } = useFieldComposition;

    function getCompareOperators(condition: Condition): { name: string; value: string }[] {

        const field = fieldMap.get(condition.fieldCode) as FieldConfig;
        if (!field) {
            return [];
        }
        const editorType = field.editor.type;
        const compareSetInEditor = new Set(CompareTypeInEditor[editorType]);
        const result = CompareTypeName.filter((compare: { value: string; name: string }) => compareSetInEditor.has(compare.value));
        return result;
    }

    return { getCompareOperators };
}
