import { EditorType } from "../../../dynamic-form";
import { CheckBoxValue } from "./condition-value/checkbox-value";
import { ComboLookupValue } from "./condition-value/combo-lookup-value";
import { DatePickerValue } from "./condition-value/date-picker-value";
import { DateRangeValue } from "./condition-value/date-range-value";
import { DateTimePickerValue } from "./condition-value/datetime-picker-value";
import { ComboListValue } from "./condition-value/dropdown-value";
import { InputGroupValue } from "./condition-value/input-group-value";
import { LookupValue } from "./condition-value/lookup-value";
import { MonthPickerValue } from "./condition-value/month-picker-value";
import { MonthRangeValue } from "./condition-value/month-range-value";
import { NumberRangeValue } from "./condition-value/number-range-value";
import { NumberSpinnerValue } from "./condition-value/number-spinner-value";
import { RadioGroupValue } from "./condition-value/radio-group-value";
import { TextValue } from "./condition-value/text-value";
import { ConditionValue } from "./condition-value/types";
import { UseConditionValue } from "./types";

/* eslint-disable @typescript-eslint/indent */
export function useConditionValue(): UseConditionValue {

    /**
     * 根据控件类型分别创建筛选条件的control对象和value对象
     * @param field 字段配置信息
     * @returns 筛选条件的control对象和value对象
     */
    function createConditionValue(editorType: EditorType, initialValue?: any): ConditionValue {
        switch (editorType) {
            case 'check-box':
                return new CheckBoxValue(initialValue);
            case 'combo-list':
                return new ComboListValue(initialValue);
            case 'combo-lookup':
                return new ComboLookupValue(initialValue);
            case 'input-group':
                return new InputGroupValue(initialValue);
            case 'date-picker':
                return new DatePickerValue(initialValue);
            case 'date-range':
                return new DateRangeValue(initialValue);
            case 'datetime-picker':
                return new DateTimePickerValue(initialValue);
            case 'datetime-range':
                return new DateRangeValue(initialValue);
            case 'lookup':
                return new LookupValue(initialValue);
            case 'month-picker':
                return new MonthPickerValue(initialValue);
            case 'month-range':
                return new MonthRangeValue(initialValue);
            case 'number-range':
                return new NumberRangeValue(initialValue);
            case 'number-spinner':
                return new NumberSpinnerValue(initialValue);
            case 'radio-group':
                return new RadioGroupValue(initialValue);
            default:
                return new TextValue(initialValue);
        }
    }

    return { createConditionValue };
}
