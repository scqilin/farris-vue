import { EditorConfig } from "../../dynamic-form";
import { ConditionValue } from "./composition/condition-value/types";
import { CompareType } from "./composition/use-compare";

export enum EnumType {
    IntType = 0,
    StringType = 1,
    BoolType = 2
}

export interface IEnumValue {
    getEnumType(): EnumType;
    getEnumValueName(): string;
    getEnumValue(): any;
}

export enum ValueType {

    /**
     *值类型
     */
    Value = 0,

    /**
     * 帮助类型
     */
    SmartHelp = 1,

    /**
     * 枚举类型
     */
    Enum = 2,

    /**
     * 表达式
     */
    Express = 3
}

/**
 * 条件实体类
 */
export interface Condition {

    /**
     * 高级模式下的值左括号
     */
    lBracket?: string;

    /**
     * 唯一性标识
     */
    id: string;

    /**
     * 字段编号
     */
    fieldCode: string;

    /**
     * 字段名称
     */
    fieldName: string;

    /**
     * 高级模式下的值关系
     */
    compareType: CompareType;

    /**
     * 值类型
     */
    valueType: ValueType;

    /**
     * 值
     * 该属性为IValue接口，根据不同的控件类型有不同的实现类：具体如下：
     * 1. 文本类型：TextValue
     * 2. 下拉列表：DropDownListValue
     * 3. 日期区间：DateRangeValue
     * 4. 数字区间：NumberRangeValue
     * 5. 帮助类型：SmartHelpValue
     * 6. 复选框: CheckBoxValue
     * 7. 单数值: SingleNumberValue
     * 8. 单年度: SingleYearValue
     * 9. 单月度: SingleMonthValue
     * 10. 月度区间: MonthRangeValue
     * 11. 单日期: SingleDateValue
     * 12. 单日期时间: SingleDateTimeValue
     * 13. 下拉帮助： ComboLookUpValue
     * 14. 单选框： RadioValue
     * 15. 智能输入框： InputGroupValue
     */
    value: ConditionValue | string;

    /**
     * 高级模式下的与下个条件的关系
     */
    relation?: number;

    /**
     * 高级模式下的值右括号
     */
    rBracket?: string;

    /**
     * 高级模式下的条件id
     */
    conditionId: number;
}

/**
 * 字段配置
 */
export interface FieldConfig {

    /**
     * 字段ID
     */
    id: string;

    /**
     * 字段标签，字段编号的唯一标识
     */
    code: string;

    labelCode: string;

    /**
     * 字段名称
     */
    name: string;

    /**
     * 控件类型，目前包括文本类型，下拉列表，日期区间，数字区间，帮助类型
     * 控件类型为IControl接口，根据不同的控件类型对应不同的实现类，具体如下：
     * 1. 文本类型：TextControl
     * 2. 下拉列表：DropDownListControl
     * 3. 日期区间：DateRangeControl
     * 4. 数字区间：NumberRangeControl
     * 5. 帮助类型：SmartHelpControl
     * 6. 单日期： SingleDateControl
     * 7. 单数值：SingleNumberControl
     * 8. 单年度：SingleYearControl
     * 9. 单月度：SingleMonthControl
     * 10. 月度区间：MonthRangeControl
     * 11. 单日期时间：SingleDateTimeControl
     * 12. 日期时间区间：DateTimeRangeControl
     * 13. 复选框：CheckBoxControl
     * 14. 下拉帮助：ComboLookUpControl
     * 15. 单选框： RadioControl
     * 16. 智能输入框： InputGroupControl
     */
    editor: EditorConfig;

    defaulValue?: ConditionValue;
}

export interface ConditionGroup {
    /**
     * 关联关系
     * @introduce 0:无，1：与，2：或
     */
    relation: number;

    items: Condition[];

    children: ConditionGroup[];

    path: number[];

    groupId: number;

    level: number;
}
