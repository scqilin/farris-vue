/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
import { ExtractPropTypes, PropType } from 'vue';
import { CalendarWeekItem } from '../../types/calendar';
import { DateObject, Period, SelectMode, weekDays } from '../../types/common';
import { defaultNameOfMonths, MonthViewItem } from '../../types/month';
import { YearViewItem } from '../../types/year';

export const datePickerContainerProps = {
    top: { type: Number, default: 0 },
    left: { type: Number, default: 0 },
    position: { type: String, default: 'bottom' },
    enablePeriod: { type: Boolean, default: false },
    dateFormat: { type: String, default: 'MM-dd-yyyy' },
    dates: { type: Array<CalendarWeekItem>, default: [] },
    daysInWeek: { type: Array<string>, default: weekDays },
    disableDates: { Type: Array<DateObject>, default: [] },
    disablePeriod: { Type: Array<Period>, default: [] },
    disableSince: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    disableWeekdays: { Type: Array<string>, default: [] },
    disableWeekends: { Type: Boolean, default: false },
    disableUntil: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    enableKeyboadNavigate: { type: Boolean, default: true },
    enableMarkCurrent: { type: Boolean, default: true },
    firstDayOfTheWeek: { type: String, default: 'Sun.' },
    highlightDates: { Type: Array<DateObject>, default: [] },
    highlightSaturday: { Type: Boolean, default: false },
    highlightSunday: { Type: Boolean, default: false },
    maxYear: { Type: Number, default: 2500 },
    minYear: { Type: Number, default: 1 },
    months: { type: Array<Array<MonthViewItem>>, default: [[]] },
    nameOfMonths: { Type: Object, default: defaultNameOfMonths },
    secondaryDates: { type: Array<CalendarWeekItem>, default: [] },
    secondaryMonths: { type: Array<Array<MonthViewItem>>, default: [[]] },
    selectedDate: { type: Object, default: null },
    selectedMonth: { type: Object, default: null },
    selectedPeriod: { type: Object, default: null },
    selectedWeek: { type: Object, default: null },
    selectMode: { type: String as PropType<SelectMode>, default: 'day' },
    showWeekNumber: { type: Boolean, default: false },
    weekTitle: { type: String, default: 'Week' },
    years: { Type: Array<Array<YearViewItem>>, default: [[]] },
};

export type DatePickerContainerProps = ExtractPropTypes<typeof datePickerContainerProps>;
