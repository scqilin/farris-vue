/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
import { ExtractPropTypes, PropType } from 'vue';
import { type SelectMode } from '../../types/common';

export const calendarNavbarProps = {
    activeMonth: { type: Object, require: true },
    ariaLabelPrevMonth: { type: String, default: '' },
    ariaLabelNextMonth: { type: String, default: '' },
    dateFormat: { type: String, default: 'yyyy-MM-dd' },
    disablePrePage: { type: Boolean, default: false },
    disablePreRecord: { type: Boolean, default: false },
    disableNextRecord: { type: Boolean, default: false },
    disableNextPage: { type: Boolean, default: false },
    years: { type: Array<object[]>, default: [[{}]] },
    selectingMonth: { type: Boolean, default: false },
    selectingYear: { type: Boolean, default: false },
    selectMode: { type: String as PropType<SelectMode>, default: 'day' }
};

export type CalendarNavbarProps = ExtractPropTypes<typeof calendarNavbarProps>;
