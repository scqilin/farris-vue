/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, ref, SetupContext } from 'vue';
import { DatePickerProps, datePickerProps } from './date-picker.props';
import { ButtonEdit } from '../../button-edit';
import FDatePickerContainer from './components/date-picker-container/date-picker-container.component';

export default defineComponent({
    name: 'FDatePicker',
    props: datePickerProps,
    emits: ['update:modelValue', 'datePicked'] as (string[] & ThisType<void>) | undefined,
    setup(props: DatePickerProps, context: SetupContext) {
        const groupIcon = '<span class="f-icon f-icon-date"></span>';
        const modelValue = ref(props.modelValue);
        const buttonEdit = ref<any>(null);

        function onClickButton() {}

        function onDatePicked(dateValue: string) {
            modelValue.value = dateValue;
            context.emit('update:modelValue', dateValue);
            context.emit('datePicked', dateValue);
            buttonEdit.value?.commitValue(dateValue);
        }

        function onClearDate() {
            context.emit('update:modelValue', modelValue.value);
        }

        return () => {
            return (
                <ButtonEdit
                    ref={buttonEdit}
                    v-model={modelValue.value}
                    buttonContent={groupIcon}
                    enableClear
                    onClear={onClearDate}
                    onClickButton={onClickButton}>
                    <FDatePickerContainer onDatePicked={(dateValue: string) => onDatePicked(dateValue)}></FDatePickerContainer>
                </ButtonEdit>
            );
        };
    }
});
