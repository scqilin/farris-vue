/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
export interface DateObject {
    year?: number;
    month?: number;
    day?: number;
    hour?: number;
    minute?: number;
    second?: number;
}

export interface Period {
    from: DateObject;
    to: DateObject;
}

export interface MarkStatus {
    marked: boolean;
    color: string;
}

export interface MarkedDates {
    dates: DateObject[];
    color: string;
}

export type SelectMode = 'day' | 'week' | 'month' | 'year';

export const weekDays = ['Sun.', 'Mon.', 'Tue.', 'Wed.', 'Thur', 'Fri.', 'Sat.'];

export enum MonthTag {
    previous = 1,
    current = 2,
    next = 3
}
