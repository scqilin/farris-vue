/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
import { DateObject } from '../types/common';
import { UseDisableMonth } from './types';
import { useCompare } from './use-compare';
import { useDate } from './use-date';

export default function useDisableMonth(): UseDisableMonth {

    const { getTimeInMilliseconds } = useDate();
    const { isInitializedDate } = useCompare();

    function isMonthDisabledByDisableUntil(date: DateObject, disableUntil: DateObject): boolean {
        return (isInitializedDate(disableUntil) && getTimeInMilliseconds(date) <= getTimeInMilliseconds(disableUntil));
    }

    function isMonthDisabledByDisableSince(date: DateObject, disableSince: DateObject): boolean {
        return (isInitializedDate(disableSince) && getTimeInMilliseconds(date) >= getTimeInMilliseconds(disableSince));
    }

    return { isMonthDisabledByDisableSince, isMonthDisabledByDisableUntil };
}
