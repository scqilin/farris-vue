/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
import { DateObject, Period } from "../types/common";
import { UseCompare } from "./types";
import { useDate } from "./use-date";

export function useCompare(): UseCompare {

    const { getTimeInMilliseconds } = useDate();

    function isInitializedDate(date: DateObject): boolean {
        return date && date.year !== 0 && date.month !== 0 && date.day !== 0;
    }

    function isInitializedMonth(date: DateObject): boolean {
        return date && date.year !== 0 && date.month !== 0;
    }

    function isInitializedYear(date: DateObject): boolean {
        return date && date.year !== 0;
    }

    function isDateEarlier(firstDate: DateObject, secondDate: DateObject): boolean {
        return getTimeInMilliseconds(firstDate) < getTimeInMilliseconds(secondDate);
    }

    function equalOrEarlier(firstDate: DateObject, secondDate: DateObject): boolean {
        return getTimeInMilliseconds(firstDate) <= getTimeInMilliseconds(secondDate);
    }

    function equal(firstDate: DateObject, secondDate: DateObject): boolean {
        return getTimeInMilliseconds(firstDate) === getTimeInMilliseconds(secondDate);
    }

    function isPoint(period: Period, date: DateObject): boolean {
        const dateMs: number = getTimeInMilliseconds(date);
        return (
            getTimeInMilliseconds(period.from) === dateMs ||
            getTimeInMilliseconds(period.to) === dateMs
        );
    }

    function inPeriod(date: DateObject, period: Period | null): boolean {
        if (!period) {
            return false;
        }
        if (!isInitializedDate(period.to) || !isInitializedDate(period.from)) {
            return false;
        }
        return equalOrEarlier(period.from, date) && equalOrEarlier(date, period.to);
    }

    function isMonthDisabledByDisableSince(date: DateObject, disableSince: DateObject): boolean {
        return (
            isInitializedDate(disableSince) &&
            getTimeInMilliseconds(date) >= getTimeInMilliseconds(disableSince)
        );
    }

    function isMonthDisabledByDisableUntil(date: DateObject, disableUntil: DateObject): boolean {
        return (
            isInitializedDate(disableUntil) &&
            getTimeInMilliseconds(date) <= getTimeInMilliseconds(disableUntil)
        );
    }

    return {
        isDateEarlier,
        equal,
        inPeriod,
        isPoint,
        equalOrEarlier,
        isInitializedDate,
        isInitializedMonth,
        isInitializedYear,
        isMonthDisabledByDisableSince,
        isMonthDisabledByDisableUntil
    };
}
