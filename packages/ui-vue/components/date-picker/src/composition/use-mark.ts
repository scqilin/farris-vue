/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
import { DateObject, MarkedDates, MarkStatus } from '../types/common';
import { useNumber } from './use-number';
import { UseMark } from './types';

export function useMark(): UseMark {

    const { getDayNumber } = useNumber();

    function isMarkedDate(date: DateObject, markedDates: MarkedDates[], markWeekends: MarkStatus): MarkStatus {
        let flattenMarkedDate: { date: DateObject; color: string }[] = [];

        flattenMarkedDate = markedDates.reduce((flattenSet, markedDates) => {
            const coloredDates = markedDates.dates.map((date: DateObject) => { return { date, color: markedDates.color }; });
            return [...flattenSet, ...coloredDates];
        }, flattenMarkedDate);

        const hasMarkedDate = flattenMarkedDate.find((target) => (target.date.year === 0 || target.date.year === date.year) &&
            (target.date.month === 0 || target.date.month === date.month) &&
            target.date.day === date.day);

        if (hasMarkedDate) {
            return { marked: true, color: hasMarkedDate.color };
        }

        if (markWeekends && markWeekends.marked) {
            const dayNbr = getDayNumber(date);
            if (dayNbr === 0 || dayNbr === 6) {
                return { marked: true, color: markWeekends.color };
            }
        }
        return { marked: false, color: '' };
    }

    function isHighlightedDate(
        date: DateObject,
        sunHighlight: boolean,
        satHighlight: boolean,
        highlightDates: DateObject[]
    ): boolean {
        const dayNbr: number = getDayNumber(date);
        if ((sunHighlight && dayNbr === 0) || (satHighlight && dayNbr === 6)) {
            return true;
        }
        const hasHighlightDate = highlightDates.find((target: DateObject) => (target.year === 0 || target.year === date.year) &&
            (target.month === 0 || target.month === date.month) &&
            target.day === date.day);

        return !!hasHighlightDate;
    }

    return { isHighlightedDate, isMarkedDate };
}
