/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { DateObject, Period } from './types/common';
import { defaultNameOfMonths } from './types/month';
import { EditorConfig } from '../../dynamic-form/src/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import datePickerSchema from './schema/date-picker.schema.json';

export const datePickerProps = {
    disableDates: { Type: Array<DateObject>, default: [] },
    disablePeriod: { Type: Array<Period>, default: [] },
    disableSince: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    disableWeekdays: { Type: Array<string>, default: [] },
    disableWeekends: { Type: Boolean, default: false },
    disableUntil: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    displayFormat: { Type: String, default: 'yyyyMMdd' },
    displayTime: { Type: Boolean, default: false },
    highlightDates: { Type: Array<DateObject>, default: [] },
    highlightSaturday: { Type: Boolean, default: false },
    highlightSunday: { Type: Boolean, default: false },
    maxYear: { Type: Number, default: 2500 },
    minYear: { Type: Number, default: 1 },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },
    nameOfMonths: { Type: Object, default: defaultNameOfMonths },
    periodDelimiter: { Type: String, default: '' },
    selectMode: { Type: String, default: 'day' },
    valueFormat: { Type: String, default: 'yyyyMMdd' },
} as Record<string, any>;

export type DatePickerProps = ExtractPropTypes<typeof datePickerProps>;

export const propsResolver = createPropsResolver<DatePickerProps>(datePickerProps, datePickerSchema, schemaMapper);
