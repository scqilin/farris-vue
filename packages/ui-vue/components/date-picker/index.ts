/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import DatePickerCalendar from './src/components/calendar/calendar.component';
import DatePickerCalendarNavBar from './src/components/calendar-navbar/calendar-navbar.component';
import DatePickerContainer from './src/components/date-picker-container/date-picker-container.component';
import DatePickerMonthView from './src/components/month/month.component';
import DatePickerYearView from './src/components/year/year.component';
import DatePicker from './src/date-picker.component';
import FDatePickerDesign from './src/designer/date-picker.design.component.tsx';
import { propsResolver } from './src/date-picker.props';

export * from './src/components/calendar/calendar.props';
export * from './src/date-picker.props';

export * from './src/types/calendar';
export * from './src/types/common';
export * from './src/types/date-model';
export * from './src/types/month';
export * from './src/types/year';

export { DatePickerCalendar };

export default {
    install(app: App): void {
        app.component(DatePickerCalendar.name, DatePickerCalendar)
            .component(DatePickerCalendarNavBar.name, DatePickerCalendarNavBar)
            .component(DatePickerContainer.name, DatePickerContainer)
            .component(DatePickerMonthView.name, DatePickerMonthView)
            .component(DatePickerYearView.name, DatePickerYearView)
            .component(DatePicker.name, DatePicker);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['date-picker'] = DatePicker;
        propsResolverMap['date-picker'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['date-picker'] = FDatePickerDesign;
        propsResolverMap['date-picker'] = propsResolver;
    }
};
