export interface navItem {
    id: string;
    text: string;
    num?: number;
    disable?: boolean;
}
