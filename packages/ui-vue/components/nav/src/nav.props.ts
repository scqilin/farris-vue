/* eslint-disable @typescript-eslint/ban-types */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { Observable } from 'rxjs';
import { navItem } from './composition/types';

export const navProps = {
    /** 未读最大值 */
    maxNum: { Type: Number, default: 99 },
    /** 导航数据 */
    navData: { Type: Array<navItem>, default: {} },
    /** 水平或垂直方向 */
    horizontal: { Type: Boolean, default: true },
    /** 当前激活的id */
    activeNavId: { Type: String, default: '1' },
    /** nav切换前事件 */
    navPicking: { Type: Function, default: (emptyObj?: {}) => Observable<any>},

};

export type NavProps = ExtractPropTypes<typeof navProps>;
