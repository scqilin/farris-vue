/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { QuerySolution } from './query-solution';
import { FieldConfig } from '../../condition/src/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import querySolutionSchema from './schema/query-solution.schema.json';

export const querySolutionProps = {
    fields: { type: Array<FieldConfig>, default: [] },
    solutions: {
        type: Array<QuerySolution>, default: [{
            id: 'default',
            code: 'default',
            name: '默认筛选方案',
            conditions: [],
            isPreset: true,
            isDefault: true,
            type: '',
            mode: '1'
        }] as QuerySolution[]
    },
} as Record<string, any>;

export type QuerySolutionProps = ExtractPropTypes<typeof querySolutionProps>;

export const propsResolver = createPropsResolver<QuerySolutionProps>(querySolutionProps, querySolutionSchema, schemaMapper);
