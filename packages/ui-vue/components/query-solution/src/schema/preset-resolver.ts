import { CompareType } from "../../../condition/src/composition/use-compare";
import { FieldConfig, ValueType } from "../../../condition/src/types";
import { QuerySolution } from "../query-solution";

export function resolvePreset(key: string, presetFields: FieldConfig[]) {
    const conditions = presetFields.map((presetField: FieldConfig, conditionId: number) => {
        const { id, labelCode: fieldCode, name: fieldName } = presetField;
        const compareType = CompareType.Equal;
        const valueType = ValueType.Value;
        const value = '';
        return { id, fieldCode, fieldName, compareType, valueType, value, conditionId };
    });
    const defaultQuerySolution = {
        id: 'default',
        code: 'default',
        name: '默认筛选方案',
        conditions,
        isPreset: true,
        isDefault: true,
        type: '',
        mode: '1'
    } as QuerySolution;
    return { "solutions": [defaultQuerySolution] };
}
