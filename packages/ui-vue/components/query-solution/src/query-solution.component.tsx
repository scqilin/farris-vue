/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, SetupContext } from 'vue';
import { querySolutionProps, QuerySolutionProps } from './query-solution.props';
import { QuerySolution } from './query-solution';
import FConditionFields from '../../condition/src/condition-fields.component';
import FConditionList from '../../condition/src/condition-list.component';
import FFilterBar from '../../filter-bar/src/filter-bar.component';

import './index.scss';

export default defineComponent({
    name: 'FQuerySolution',
    props: querySolutionProps,
    emits: ['save', 'saveAs'] as (string[] & ThisType<void>) | undefined,
    setup(props: QuerySolutionProps, context: SetupContext) {
        const fields = ref(props.fields);
        const solutions = ref(props.solutions);
        const currentSolution = ref<QuerySolution>(props.solutions[0]);

        const currentConditionMode = computed(() => currentSolution.value.mode);

        const conditionTypeMap: Record<string, any> = {
            '1': FConditionFields,
            '2': FConditionList
        };

        const title = computed(() => currentSolution.value.name || '');

        const shouldShowClearButton = computed(() => currentSolution.value.mode !== '2');

        const shouldShowSolutionList = ref(false);

        const solutionListClass = computed(() => ({
            'dropdown-menu': true,
            show: shouldShowSolutionList.value
        }));

        const querySolutionClass = computed(() => {
            const classObject = {
                'farris-panel': true,
                'position-relative': true,
                'query-solution': true
            } as Record<string, boolean>;
            return classObject;
        });

        const querySolutionStyle = computed(() => {
            const styleObject = {
                border: 'none'
            } as Record<string, any>;
            return styleObject;
        });

        function onSelectionChange(solution: QuerySolution) {
            currentSolution.value = solution;
        }

        function clear() {
            const solutionToClear = solutions.value.find((solutionItem: any) => solutionItem.id === currentSolution.value.id);
            if (solutionToClear) {
                currentSolution.value = solutionToClear;
            }
        }

        function save() {
            context.emit('save', currentSolution.value);
        }
        function saveAs() {
            context.emit('saveAs', currentSolution.value);
        }

        function renderSolutionList() {
            const solutionItems = solutions.value.map((solutionItem: any) => {
                return (
                    <li class="dropdown-item" onClick={() => onSelectionChange(solutionItem)}>
                        {solutionItem.name}
                    </li>
                );
            });
            solutionItems.push(
                <li class="solution-header-title-btns">
                    <span class="dropdown-item-btn " onClick={save}>
                        保存
                    </span>
                    <span class="dropdown-item-btn ml-2 mr-2" onClick={saveAs}>
                        另存为
                    </span>
                </li>
            );
            return <ul class={solutionListClass.value}>{solutionItems}</ul>;
        }

        function toggleSolutionList() {
            shouldShowSolutionList.value = !shouldShowSolutionList.value;
        }

        function renderSummary() {
            return (
                <div>
                    {/* <FFilterBar
                        data={currentSolution.value.conditions}
                        fields={fields.value}
                        mode="display-only"
                        show-reset={true}></FFilterBar> */}
                </div>
            );
        }

        function renderSolutionToolbar() {
            return (
                <div class="solution-action">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary ">
                            查询
                        </button>
                    </div>
                    {shouldShowClearButton.value && (
                        <div class="icon-group ml-2">
                            <span class="icon-group-remove" onClick={clear} title="清空">
                                <span class="f-icon f-icon-remove"></span>
                            </span>
                        </div>
                    )}
                </div>
            );
        }

        function renderHeader() {
            return (
                <div class="solution-header">
                    <div class="btn-group mr-3" onClick={toggleSolutionList}>
                        <div class="solution-header-title">
                            {title.value}
                            <span class="f-icon f-accordion-expand"></span>
                        </div>
                        {renderSolutionList()}
                    </div>
                    {renderSummary()}
                    {renderSolutionToolbar()}
                </div>
            );
        }

        function renderCondition() {
            const ConditionComponent = conditionTypeMap[currentConditionMode.value] || FConditionFields;
            return <ConditionComponent fields={fields.value} conditions={currentSolution.value.conditions}></ConditionComponent>;
        }

        return () => (
            <div class={querySolutionClass.value} tabindex="1" style={querySolutionStyle.value}>
                {renderHeader()}
                {renderCondition()}
            </div>
        );
    }
});
