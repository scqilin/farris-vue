import { Condition } from "../../condition/src/types";

/**
 * 查询方案实体类
 */
export interface QuerySolution {
    /**
     * 唯一性标识
     */
    id: string;

    /**
     * 查询方案编号
     */
    code: string;

    /**
     * 查询方案名称
     */
    name: string;

    /**
     * 查询条件
     */
    conditions: Condition[];

    /**
     * 是否系统预置查询方案
     */
    isPreset: boolean;

    /**
     * 是否默认查询方案
     * 初始时，为false
     */
    isDefault: boolean;

    hasChanged?: boolean;

    /**
     * 类型： pre是上次查询
     */
    type: string;

    /**
     * 高级模式新增字段，1为标准模式，2为高级模式
     */
    mode: string;
}
