/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, SetupContext, inject, onMounted } from 'vue';
import { querySolutionProps, QuerySolutionProps } from '../query-solution.props';
import FConditionFields from '../../../condition/src/condition-fields.component';

import '../index.scss';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FQuerySolutionDesign',
    props: querySolutionProps,
    emits: ['save', 'saveAs'] as (string[] & ThisType<void>) | undefined,
    setup(props: QuerySolutionProps, context: SetupContext) {
        const fields = ref(props.fields);
        const currentSolution = ref(props.solutions[0]);
        const elementRef = ref();

        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const title = computed(() => currentSolution.value.name || '');

        const shouldShowClearButton = computed(() => currentSolution.value.mode !== '2');

        const querySolutionClass = computed(() => {
            const classObject = {
                'farris-panel': true,
                'position-relative': true,
                'query-solution': true
            } as Record<string, boolean>;
            return classObject;
        });

        const querySolutionStyle = computed(() => {
            const styleObject = {
                border: 'none'
            } as Record<string, any>;
            return styleObject;
        });

        /**
         * 校验组件是否支持移动
         */
        function checkCanMoveComponent(): boolean {
            return true;
        }

        /**
         * 校验组件是否支持选中父级
         */
        function checkCanSelectParentComponent(): boolean {
            return false;
        }

        /**
         * 校验组件是否支持删除
         */
        function checkCanDeleteComponent() {
            return true;
        }

        function renderSolutionToolbar() {
            return (
                <div class="solution-action">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary ">
                            查询
                        </button>
                    </div>
                    {shouldShowClearButton.value && (
                        <div class="icon-group ml-2">
                            <span class="icon-group-remove" title="清空">
                                <span class="f-icon f-icon-remove"></span>
                            </span>
                        </div>
                    )}
                </div>
            );
        }

        function renderHeader() {
            return (
                <div class="solution-header">
                    <div class="btn-group mr-3">
                        <div class="solution-header-title">
                            {title.value}
                            <span class="f-icon f-accordion-expand"></span>
                        </div>
                    </div>
                    {renderSolutionToolbar()}
                </div>
            );
        }

        function renderCondition() {
            const ConditionComponent = FConditionFields;
            return <ConditionComponent fields={fields.value} conditions={currentSolution.value.conditions}></ConditionComponent>;
        }

        return () => (
            <div ref={elementRef} class={querySolutionClass.value} tabindex="1" style={querySolutionStyle.value}>
                {renderHeader()}
                {renderCondition()}
            </div>
        );
    }
});
