/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { computed, defineComponent, SetupContext } from 'vue';
import { InputGroupProps, inputGroupProps } from '../input-group.props';
import getEditorRender from '../components/text-edit.component';
import getAppendedButtonRender from '../components/appended-button.component';

import '../input-group.scss';
import { useTextBox } from '../composition/use-text-box';
import { useAppendedButton } from '../composition/use-appended-button';
import { useClear } from '../composition/use-clear';
import { usePassword } from '../composition/use-password';

export default defineComponent({
    name: 'FInputGroupDesign',
    props: inputGroupProps,
    emits: [
        'updateExtendInfo',
        'clear',
        'valueChange',
        'clickHandle',
        'blurHandle',
        'focusHandle',
        'enterHandle',
        'iconMouseEnter',
        'iconMouseLeave',
        'keyupHandle',
        'keydownHandle',
        'inputClick'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: InputGroupProps, context: SetupContext) {
        const useTextBoxComposition = useTextBox(props, context);
        const { disabled, editable, inputType, readonly } = useTextBoxComposition;
        const useAppendedButtonComposition = useAppendedButton(props, context);
        const { shouldShowAppendedButton } = useAppendedButtonComposition;
        const useClearComposition = useClear(props, context, useTextBoxComposition);
        const { onMouseEnter, onMouseLeave } = useClearComposition;
        const usePasswordComposition = usePassword(props, context, inputType, useAppendedButtonComposition);

        const renderEditor = getEditorRender(props, context, usePasswordComposition, useTextBoxComposition);
        const renderAppendedButton = getAppendedButtonRender(
            props,
            context,
            useAppendedButtonComposition,
            useClearComposition,
            usePasswordComposition
        );

        const inputGroupClass = computed(() => {
            const classObject = {
                'f-cmp-inputgroup': true,
                'input-group': true,
                'f-state-disabled': disabled.value,
                'f-state-editable': editable.value && !disabled.value && !readonly.value,
                'f-state-readonly': readonly.value && !disabled.value
            } as Record<string, boolean>;
            const customClassArray = (props.customClass || '').split(' ');
            customClassArray.reduce<Record<string, boolean>>((result: Record<string, boolean>, classString: string) => {
                result[classString] = true;
                return result;
            }, classObject);
            return classObject;
        });

        return () => {
            return (
                <div id="inputGroup" class={inputGroupClass.value} onMouseenter={onMouseEnter} onMouseleave={onMouseLeave}>
                    {renderEditor()}
                    {shouldShowAppendedButton.value && renderAppendedButton()}
                </div>
            );
        };
    }
});
