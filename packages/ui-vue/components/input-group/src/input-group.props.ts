/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { templateRef } from '@vueuse/core';
import { ExtractPropTypes, ref } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import inputGroupSchema from './schema/input-group.schema.json';
import { schemaMapper } from './schema/schema-mapper';

export const inputGroupProps = {
    /** 是否自动完成 */
    autocomplete: { Type: String, default: 'off' },
    /** 自定义CLASS */
    customClass: { Type: String, default: '' },
    /** 禁用 */
    disabled: { Type: Boolean, default: false },
    /** 允许编辑 */
    editable: { Type: Boolean, default: true },
    /** 启用清除按钮 */
    enableClear: { Type: Boolean, default: true },
    enableTitle: { Type: Boolean, default: true },
    enableViewPassword: { Type: Boolean, default: true },
    extendInfo: { Type: String, default: '' },
    forcePlaceholder: { Type: Boolean, default: false },
    /** 扩展按钮 */
    groupText: { Type: String, default: '' },
    groupTextTemplate: { Type: templateRef<any>, default: ref<HTMLElement | null>(null) },
    isPassword: { Type: Boolean, default: false },
    maxLength: { Type: Number || undefined, default: undefined },
    minLength: { Type: Number || undefined, default: undefined },
    /** 组件值 */
    modelValue: { type: String, default: '' },
    noborder: { Type: Boolean, default: false },
    /** 启用提示信息 */
    placeholder: { Type: String, default: '' },
    /** 只读 */
    readonly: { Type: Boolean, default: false },
    /** 当组件禁用或只读时显示后边的按钮 */
    showButtonWhenDisabled: { Type: Boolean, default: false },
    tabIndex: { Type: Number || undefined, default: undefined },
    /** 文本在输入框中的对齐方式 */
    textAlign: { Type: String, default: 'left' },
    /** 扩展信息；在输入框前面 显示 ① 图标鼠标滑过后显示 */
    useExtendInfo: { Type: Boolean, default: false }, /** 输入值 */
    value: { Type: String, default: '' },

} as Record<string, any>;

export type InputGroupProps = ExtractPropTypes<typeof inputGroupProps>;

export const propsResolver = createPropsResolver<InputGroupProps>(inputGroupProps, inputGroupSchema, schemaMapper);
