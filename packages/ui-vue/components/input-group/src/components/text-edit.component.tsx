import { SetupContext, ref } from 'vue';
import { InputGroupProps } from '../input-group.props';
import { UsePassword, UseTextBox } from '../composition/types';

export default function (
    props: InputGroupProps,
    context: SetupContext,
    usePasswordComposition: UsePassword,
    useTextBoxComposition: UseTextBox
) {
    const autocomplete = ref(props.autocomplete);
    const enableTitle = ref(props.enableTitle);
    const { disabled, modelValue } = useTextBoxComposition;
    /** 文本在输入框中的对齐方式 */
    const { isPassword } = usePasswordComposition;
    const minLength = ref(props.minLength);
    const maxLength = ref(props.maxLength);
    const tabIndex = ref(props.tabIndex);

    const {
        inputGroupEditorClass,
        inputType,
        onBlur,
        onEnter,
        onInputClick,
        onInputFocus,
        onMousedown,
        onTextBoxValueChange,
        readonly,
        textBoxPlaceholder
    } = useTextBoxComposition;

    return () => (
        <input
            name="input-group-value"
            class={inputGroupEditorClass.value}
            title={enableTitle.value && !isPassword.value ? modelValue.value : ''}
            type={inputType.value}
            placeholder={textBoxPlaceholder.value}
            autocomplete={autocomplete.value}
            readonly={readonly.value}
            value={modelValue.value}
            disabled={disabled.value}
            onBlur={(payload: FocusEvent) => onBlur(payload)}
            onMousedown={(payload: MouseEvent) => onMousedown(payload)}
            onChange={(payload: Event) => onTextBoxValueChange(payload)}
            onKeypress={(payload: KeyboardEvent) => onEnter(payload)}
            onFocus={(payload: FocusEvent) => onInputFocus(payload)}
            onClick={(payload: MouseEvent) => onInputClick(payload)}
            minlength={minLength.value}
            maxlength={maxLength.value}
            onKeyup={(payload) => context.emit('keyupHandle', payload)}
            onKeydown={(payload) => context.emit('keydownHandle', payload)}
            tabindex={tabIndex.value}
        />
    );
}
