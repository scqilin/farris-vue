import { Subscription, Observable } from 'rxjs';
import { FFileUploaderService } from '../service/uploader-service';

export interface UploaderOptions {
    concurrency?: number;
    allowedContentTypes?: string[];
    maxUploads?: number;
    maxFileSize?: number;//MB为单位
    uploadedCount?: number;
}
export enum UploadStatus {
    Queue,
    Uploading,
    Done,
    Cancelled,
    Remove,// 正在删除
    Error
}

export interface UploadProgress {
    status: UploadStatus;
    data?: {
        percentage: number;
        speed?: number;
        speedHuman?: string;
        startTime?: number | null;
        endTime?: number | null;
        eta?: number | null;
        etaHuman?: string | null;
    };
}
export interface FUploadFileExtend {
    id: string;
    name: string;
    disabled?: boolean;
    checked?: boolean;
    size?: number | undefined;//文件大小
    createTime?: string | undefined;//创建日期
    type?: string;
    extend?: any | null;//记录返回数据
    extendStatus?: number;
    extendHeaders?: { [key: string]: string } | null;
    showDownload?: boolean;
    showComments?: boolean;// 预览是否显示修订记录
}

export interface UploadFile {
    id: string;
    fileIndex: number;
    lastModifiedDate: Date;
    name: string;
    size: number;
    type: string;
    form: FormData;
    progress: UploadProgress;//
    response?: any;//记录返回数据
    responseStatus?: number;
    sub?: Subscription | any;
    nativeFile?: File;
    responseHeaders?: { [key: string]: string };
}


export interface UploadOutput {
    type: 'addedToQueue' | 'allAddedToQueue' | 'uploading' | 'done' | 'start' | 'cancelled' | 'dragOver'
    | 'dragOut' | 'drop' | 'removed' | 'allRemoved' | 'rejected' | 'allDone' | 'allStart' | 'allCancelled' | 'error';
    file?: UploadFile;
    nativeFile?: File;
    message?: String;
    files?: UploadFile[];
    checked?: boolean;
}
export interface UploadConfig {
    url?: string;
    method?: string;// POST，GET 
    id?: string;
    fieldName?: string;
    fileIndex?: number;
    file?: UploadFile;
    data?: {
        [key: string]: any
    };
    headers?: { [key: string]: string } | null;
    includeWebKitFormBoundary?: boolean; // If false, only the file is send trough xhr.send (WebKitFormBoundary is omit)
    withCredentials?: boolean;
    timeout?: number;// 暂不支持
}

export interface UploadInput extends UploadConfig {
    type: 'upload' | 'uploadAll' | 'cancel' | 'cancelAll' | 'remove' | 'removeAll' | 'config' | 'hide';
    /** 分块上传时，每块大小默认为 1M */
    chunkSize?: number
}


export abstract class UploadServerService {
    uploadConfig: UploadConfig;
    removeConfig: UploadConfig;
    constructor(uConfig: UploadConfig = {}, rConfig: UploadConfig = {}) {
        // 处理公共的配置
        let commonConfig = { type: 'config', url: '', timeout: 0, headers: null, data: {} };
        this.uploadConfig = Object.assign({}, commonConfig, uConfig);
        this.removeConfig = Object.assign({}, commonConfig, rConfig);
    }
    // extendConfig 用来处理传递的配置
    abstract upload(files: UploadFile[], event: UploadInput, extendConfig: any): Observable<any>;
    // extendConfig 用来处理传递的配置
    abstract remove(files: UploadFile[], event: UploadInput, extendConfig: any): Observable<any>;
}

/**
 * 上传预览显示列
 */
export interface FUploadPreviewColumn {
    field: string;
    width: number;
    title: string;
    checkbox?: boolean;
}
/**
 * 注入的服务
 */
export interface IUploadServer {
    (): UploadServerService;
}
/**
 * 
 */
export type TGetService = {
    getNotifyService: () => TNotifyService;
    getAPIService: () => FFileUploaderService;
}
/**
 * 
 */
export type TNotifyService = {
    show: (par: any) => any;
}

/**
 * --------------------------
 *  默认服务里使用
 * --------------------------
 */

export interface BlobFile extends Blob {
    name: string;
}