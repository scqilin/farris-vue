/* eslint-disable max-len */
import { Ref, SetupContext, computed, defineComponent, inject, onBeforeMount, onMounted, provide, ref, watch } from 'vue';
import FUploadProgress from './components/upload-progress.component';
import FPreviewCheckbox from './components/preview-checkbox.component';
import FFileSelect from './components/file-select.component';
import { uploaderProps, UploaderProps } from './uploader.props';
import './uploader.scss';
import { useUploader } from './composition/use-uploader';
import { FUploadFileExtend, FUploadPreviewColumn, UploadOutput, IUploadServer, UploadServerService } from './composition/type';
import { FFileUploaderService } from './service/uploader-service';
import { FFileUploadDefaultService } from './service/default-service';
import { getService } from './service/get-service';

export default defineComponent({
    name: 'FUploader',
    components: {
        'f-upload-progress': FUploadProgress,
        'f-preview-checkbox': FPreviewCheckbox,
        'f-file-select': FFileSelect
    },
    props: uploaderProps,
    emits: ['filePreviewEvent', 'fileDownloadEvent', 'fPreviewMultiSelectedEvent', 'fSelectedEvent', 'fileRemoveEvent',
        'fUploadDoneEvent', 'fUploadRemovedEvent', 'afterOrdered'],
    setup(props: UploaderProps, context: SetupContext) {
        const getSer=getService();

        // // // 传递上传服务
        // provide('PUploadService', ()=>getSer.getAPIService());
        // // 传递消息服务
        // provide('PNotifyService',()=> getSer.getNotifyService());
        /**
         * 获取模版相关参数
         */
        const { uploadSelectText,
            disabled,
            uploadVisible,
            uploadDisabled,
            noDownload,
            previewVisible,
            previewReadonly,
            previewColumns,
            uploadFiles,
            innerFileInfos,
            previewEnableMulti,
            previewMultiSelectedLength,
            previewSelectAllBtnChecked,
            uploadEnableMulti,
            uploadInputOpts,
            multiFileRemoveHandler,
            previewMultiSelectChangeHandler,
            fileMultiDownloadHandler,
            selectOrCancelAllHandler,
            beforeUploadSupportInfoHtml,
            hasColumnHtmlFunc,
            findColumnHtmlFunc,
            showPreviewStateColumn,
            handleFilesFromService } = useUploader(props, context,getSer);

        onBeforeMount(() => {

        });
        // const hhtpServ = inject('key', {})
        // hhtpServ.getData().then()

        return () => (
            disabled.value ? <></> : <>
                <div class="uploadAndpreview--header">
                    {uploadVisible.value ? <div class="header--left-container">
                        <f-file-select disabled={uploadDisabled.value} selectText={uploadSelectText.value}
                            enableMulti={uploadEnableMulti.value} accept={uploadInputOpts.value.allowedContentTypes} onChange={(datas: FileList) => handleFilesFromService(datas)}></f-file-select>
                    </div> : null}
                    {!uploadVisible.value && previewVisible.value && innerFileInfos.value.length > 0 ? <div class="header--left-container header--countInfo">
                        <p class="m-0">
                            共<span class="count">{uploadFiles.value.length + innerFileInfos.value.length}</span>个附件
                            {previewMultiSelectedLength.value > 0 ? <>(已选<span class="count-selected">{previewMultiSelectedLength.value}</span>个)</> : ''}
                        </p>
                    </div> : null}
                    {
                        !noDownload.value && previewVisible.value && innerFileInfos.value.length > 0 ? <div class="header--right-container">
                            <button class="btn btn-primary f-btn-ml" disabled={previewMultiSelectedLength.value === 0}
                                onClick={(ev: MouseEvent) => fileMultiDownloadHandler(ev)} >
                                下载
                            </button>
                            {previewReadonly.value ? <button class="btn btn-secondary f-btn-ml" disabled={previewMultiSelectedLength.value === 0} onClick={(ev: MouseEvent) => multiFileRemoveHandler(ev)}>
                                删除
                            </button> : ""}
                        </div> : ""
                    }
                </div>
                {
                    uploadVisible.value && !uploadDisabled.value && innerFileInfos.value.length === 0 && !disabled.value && !noDownload.value ? beforeUploadSupportInfoHtml() : ''
                }

                {
                    !disabled.value && (previewVisible.value || uploadVisible.value) ? <div class="uploadAndpreview--content">
                        {
                            (noDownload.value || (!disabled.value && !uploadVisible.value)) && uploadFiles.value.length === 0 && innerFileInfos.value.length == 0 ? <p class="uploadAndpreview--nodata">无附件信息</p> : ''
                        }
                        {
                            uploadFiles.value.length > 0 || innerFileInfos.value.length > 0 ? <table class="table table-bordered uploadAndpreview--table">
                                <thead>
                                    {
                                        previewColumns.value.map((column: FUploadPreviewColumn) => (

                                            showPreviewStateColumn(column) ?
                                                <th style={{ 'width': column.width ? column.width + 'px' : 'auto' }}
                                                    class={[(column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                                                    {
                                                        !noDownload.value && previewEnableMulti.value && column.checkbox ? <f-preview-checkbox class="preview-checkbox" id="previewMultiBtn"
                                                            checked={previewSelectAllBtnChecked.value} onCheckedChange={(data: any) => selectOrCancelAllHandler(data)}>
                                                        </f-preview-checkbox> : null
                                                    }
                                                    {column.title}
                                                </th> : null

                                        ))
                                    }
                                </thead>
                                <tbody>
                                    {
                                        uploadFiles.value.map((uploadInfo: UploadOutput) => (
                                            <tr class="uploadAndpreview--upload-item"
                                                id={'uploadAndpreview--upload-item' + uploadInfo?.file?.id}>
                                                {
                                                    previewColumns.value.map((column: FUploadPreviewColumn) => (
                                                        <td class={[(column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                                                            {
                                                                !noDownload.value && previewEnableMulti.value && column.checkbox ? <f-preview-checkbox id={uploadInfo?.file?.id} checked={uploadInfo.checked}
                                                                    disabled={uploadInfo.type !== 'done'} class="preview-checkbox" onCheckedChange={(data: any) => previewMultiSelectChangeHandler(data)}>
                                                                </f-preview-checkbox> : null
                                                            }
                                                            {
                                                                hasColumnHtmlFunc(column, 'upload') ? findColumnHtmlFunc('upload', uploadInfo, null, column) : null
                                                            }
                                                        </td>
                                                    ))
                                                }
                                            </tr>
                                        ))
                                    }
                                    {
                                        innerFileInfos.value.map((previewInfo: FUploadFileExtend) => (
                                            <tr class={["uploadAndpreview--preview-item", "previewCurrent==previewInfo.id" ? "uploadAndpreview--currentfile" : ""]}
                                                id={'uploadAndpreview--preview-item' + previewInfo.id} >
                                                {
                                                    previewColumns.value.map((column: FUploadPreviewColumn) => (
                                                        showPreviewStateColumn(column) ? <td class={[(column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                                                            {
                                                                !noDownload.value && previewEnableMulti.value && column.checkbox ? <f-preview-checkbox id={previewInfo.id} checked={previewInfo.checked} class="preview-checkbox" onCheckedChange={(data: any) => previewMultiSelectChangeHandler(data)}>
                                                                </f-preview-checkbox> : null
                                                            }
                                                            {
                                                                hasColumnHtmlFunc(column, 'preview') ? findColumnHtmlFunc('preview', null, previewInfo, column) : null
                                                            }
                                                        </td> : null
                                                    ))
                                                }
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </table> : null
                        }
                    </div> : null
                }
                {
                    uploadVisible.value && !uploadDisabled.value && innerFileInfos.value.length > 0 && !disabled.value && !noDownload.value ? beforeUploadSupportInfoHtml() : ''
                }

            </>
        );
    }
});
