import { defineComponent,  SetupContext, ref, toRefs } from 'vue';
import { PreviewCheckboxProps,previewCheckboxProps } from './sub-component.props';


export default defineComponent({
    name: 'FPreviewCheckbox',
    props: previewCheckboxProps,
    emits:['checkedChange'],
    setup(props: PreviewCheckboxProps, context: SetupContext) {
        const {checked,id,disabled} = toRefs(props);
        // 内部处理
        const innerChecked=ref(checked.value);
        // 处理点击
        const clickHandler=(ev:MouseEvent)=> {
            ev.stopPropagation();
            if (!disabled.value) {
                innerChecked.value =!innerChecked.value;
                context.emit('checkedChange', { checked: innerChecked.value, id: id.value });
            }
        }

        return () => (
            <div class="preview-checkbox d-inline-flex align-middle">
                <div class="custom-control custom-checkbox f-checkradio-single  m-0" >
                    <input class="custom-control-input" type="checkbox" disabled={disabled.value}  checked={innerChecked.value} />
                    <label class="custom-control-label" onClick={(ev:MouseEvent)=>clickHandler(ev)} onMousedown={(ev: MouseEvent) => ev.stopPropagation()}></label>
                </div>
            </div>
        );
    }
});
