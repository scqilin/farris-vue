/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { TextProps, textProps } from './text.props';

export default defineComponent({
    name: 'FText',
    props: textProps,
    emits: ['update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: TextProps, context: SetupContext) {
        const isTextArea = ref(true);
        const autoSize = ref(true);
        const textAlginment = ref('');
        const height = ref(0);
        const maxHeight = ref(0);
        const modelValue = ref(props.modelValue);
        const textClass = computed(() => ({
            'f-form-control-text': !isTextArea.value,
            'f-form-context-textarea': isTextArea,
            'f-component-text-auto-size': autoSize.value
        }));

        const textStyle = computed(() => ({
            textalign: textAlginment.value,
            height: !autoSize.value && height.value > 0 ? `${height.value}px` : '',
            'min-height': !autoSize.value && height.value > 0 ? `${height.value}px` : '',
            'max-height': !autoSize.value && maxHeight.value > 0 ? `${maxHeight.value}px` : ''
        }));

        const text = computed(() => {
            // text && text.length > 0 ?  text : control
            return '';
        });

        return () => {
            return (
                <span class={textClass.value} style={textStyle.value}>
                    {modelValue.value}
                </span>
            );
        };
    }
});
