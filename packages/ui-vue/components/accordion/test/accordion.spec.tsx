import { mount } from '@vue/test-utils';
import { Accordion, AccordionItem } from '..';

describe('f-accordion', () => {
    const mocks = {};

    beforeAll(() => {});

    describe('properties', () => {});

    describe('render', () => {
        it('should work', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return (
                            <Accordion>
                                <AccordionItem title="Accordion Panel One "></AccordionItem>
                                <AccordionItem title="Accordion Panel Two "></AccordionItem>
                            </Accordion>
                        );
                    };
                }
            });
            expect(wrapper.find('div').classes('accordion')).toBeTruthy();
            expect(wrapper.find('div').classes('farris-panel')).toBeTruthy();
        });
    });

    describe('methods', () => {});

    describe('events', () => {});

    describe('behavior', () => {});
});
