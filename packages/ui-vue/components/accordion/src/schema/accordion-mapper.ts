import { MapperFunction } from '../../../dynamic-resolver';

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['size', (key: string, value: { width: any; height: any }) => {
        const sizeObject = {} as Record<string, number>;
        if (value.width) {
            sizeObject.width = Number(value.width);
        }
        if (value.height) {
            sizeObject.height = Number(value.height);
        }
        return sizeObject;
    }]
]);
