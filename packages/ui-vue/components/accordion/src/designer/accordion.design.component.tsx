/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, SetupContext } from 'vue';
import { AccordionProps, accordionProps } from '../accordion.props';

import '../accordion.css';

export default defineComponent({
    name: 'FAccordionDesign',
    props: accordionProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: AccordionProps, context: SetupContext) {
        const elementRef = ref();
        const accordionStyle = computed(() => ({
            height: props.height ? `${props.height}px` : '',
            width: props.width ? `${props.width}px` : ''
        }));

        const accordionClass = computed(() => {
            const classObject = {
                'farris-panel': true,
                accordion: true
            } as Record<string, boolean>;
        });

        /**
         * 校验组件是否支持移动
         */
        function checkCanMoveComponent(): boolean {
            return true;
        }

        /**
         * 校验组件是否支持选中父级
         */
        function checkCanSelectParentComponent(): boolean {
            return false;
        }

        /**
         * 校验组件是否支持删除
         */
        function checkCanDeleteComponent() {
            return true;
        }

        const componentInstance = {
            canMove: checkCanMoveComponent(),
            canSelectParent: checkCanSelectParentComponent(),
            canDelete: checkCanDeleteComponent(),
            canNested: true,
            elementRef
        };

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance);

        return () => {
            return (
                <div ref={elementRef} class={accordionClass.value} style={accordionStyle.value}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
