import type { App } from 'vue';
import Capsule from './src/capsule.component';

export * from './src/capsule.props';

export { Capsule };

export default {
    install(app: App): void {
        app.component(Capsule.name, Capsule);
    }
};
