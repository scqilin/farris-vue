import { ComputedRef, ExtractPropTypes, PropType } from 'vue';

export type CapsuleColerType = 'primary' | 'secondary';

export interface CapsuleItem {
    [key: string]: any;
    /**
     * 枚举值
     */
    value: any;
    /**
     * 枚举展示文本
     */
    name: string;
}

export const capsuleProps = {
    items: { Type: Array<CapsuleItem>, default: [] },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },

    type: { type: String as PropType<CapsuleColerType>, default: 'primary' }
};

export const capsuleItemProps = {
    name: { type: String, default: '' },
    value: { type: String, default: '' },
    isActive: { type: Boolean, default: false },
    icon: { type: String, default: '' },
    index: { type: Number, default: 0 }
};

export type CapsuleProps = ExtractPropTypes<typeof capsuleProps>;

export type CapsuleItemProps = ExtractPropTypes<typeof capsuleItemProps>;
