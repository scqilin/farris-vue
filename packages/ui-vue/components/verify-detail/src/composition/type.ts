// 默认验证表单信息
export interface validatorListItem {
    id?: string;
    title: string;
    msg?: string;
    type: string;
}
// 处理后的验证表单信息
export interface ErrorListsItem {
    list: validatorListItem[];
    show: boolean;
    type: string;
}
// 表单分组信息
export interface tabListItem {
    id?: string;
    type: string;
    title: string;
    active?: boolean;
    length?: number;
    iconCls?: string;// 图标样式名，用来更换图标
    iconStyle?: string;// 图标行样式，用来给图标设置颜色等
}
