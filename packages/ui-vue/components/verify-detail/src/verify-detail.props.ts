import { ExtractPropTypes } from 'vue';

export const verifyDetailProps = {

    /** 表单验证默认显示的分组信息 */
    showType: { type: String, default: '' },

    /** 是否默认显示验证信息列表 */
    showList: { type: Boolean, default: false },

    /** 表单验证列表 */
    verifyList: {
        type: Object,
        default: [{
            id: '111',
            title: '单据信息[销售组织]',
            msg: '字段值不能为空',
            type: 'empty'
        },
        {
            id: '222',
            title: '单据信息[销售组织]',
            msg: '字段值不能为空',
            type: 'empty'
        }]
    },

    verifyType: {
        type: Object,
        default: [
            {
                id: 'vertifyType1',
                type: 'all',
                title: ''
            },
            {
                id: 'vertifyType2',
                type: 'error',
                title: ''
            },
            {
                id: 'vertifyType3',
                type: 'empty',
                title: ''
            }
        ]
    },

    maxHeight: { type: Number, default: 200 }
};

export type VerifyDetailProps = ExtractPropTypes<typeof verifyDetailProps>;
