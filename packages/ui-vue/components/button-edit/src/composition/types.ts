/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, Ref } from 'vue';

export interface UseButton {
    /**
     * 附加按钮的Class
     */
    buttonClass: ComputedRef<Record<string, boolean | undefined>>;
    /**
     * 点击附加按钮事件响应函数
     */
    onClickButton: ($event: Event) => void;
    /**
     * 鼠标移入附加按钮事件响应函数
     */
    onMouseEnterButton: ($event: MouseEvent) => void;
    /**
     * 鼠标移出附加按钮事件响应函数
     */
    onMouseLeaveButton: ($event: MouseEvent) => void;
    /**
     * 鼠标滑过附加按钮事件响应函数
     */
    onMouseOverButton: () => void;
}

export interface UseClear {
    /**
     * 启用清空按钮
     */
    enableClearButton: ComputedRef<boolean>;
    /**
     * 显示清空按钮
     */
    showClearButton: Ref<boolean>;
    /**
     * 清空输入框值事件响应函数
     */
    onClearValue: ($event: Event) => void;
    /**
     * 鼠标进入输入框事件响应函数
     */
    onMouseEnterTextBox: ($event: MouseEvent) => void;
    /**
     * 鼠标移出输入框事件响应函数
     */
    onMouseLeaveTextBox: ($event: MouseEvent) => void;
}

export interface UseTextBox {
    /**
     * 输入框是否获得焦点
     */
    hasFocusedTextBox: ComputedRef<boolean>;
    /**
     * 输入框是否处于只读状态
     */
    isTextBoxReadonly: ComputedRef<boolean>;
    /**
     * 输入框Class
     */
    textBoxClass: ComputedRef<Record<string, boolean | undefined>>;
    /**
     * 输入框提示语
     */
    textBoxPlaceholder: ComputedRef<string>;
    /**
     * 输入框提示标签
     */
    textBoxTitle: ComputedRef<string>;
    /**
     * 输入框组Class
     */
    inputGroupClass: ComputedRef<Record<string, boolean | undefined>>; /**
     * 更新输入框的值并触发change事件
     */
    changeTextBoxValue: (newValue: string, showEmitChangeEmit: boolean) => void;
    /**
     * 接受更新文本
     */
    commitValue: (value: any) => void;
    /**
     * 输入框失去焦点事件响应函数
     */
    onBlurTextBox: ($event: Event) => void;
    /**
     * 鼠标点击输入框事件响应函数
     */
    onClickTextBox: ($event: Event) => void;
    /**
     * 输入框获得焦点事件响应函数
     */
    onFocusTextBox: ($event: Event) => void;
    /**
     * 输入框值变化事件
     */
    onInput: ($event: Event) => void;
    /**
     * 鼠标点击输入框事件响应函数
     */
    onMouseDownTextBox: ($event: MouseEvent) => void;
    /**
     * 键盘在输入框按下事件
     */
    onKeyDownTextBox: ($event: Event) => void;
    /**
     * 键盘在输入框抬起事件
     */
    onKeyUpTextBox: ($event: KeyboardEvent) => void;
    /**
     * 输入框值变化事件响应函数
     */
    onTextBoxValueChange: ($event: Event) => void;
}

export interface UsePopup {

    hidePopup(): void;

    popup(): void;

    shouldPopupContent: Ref<boolean>;

    togglePopup(): void;

    popoverRef: Ref<any>;
}
