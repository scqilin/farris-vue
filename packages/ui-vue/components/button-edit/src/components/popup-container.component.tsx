import { SetupContext, ref } from "vue";
import { ButtonEditProps } from "../button-edit.props";
import { UsePopup } from "../composition/types";
import FPopover from '../../../popover/src/popover.component';

export default function (
    props: ButtonEditProps,
    context: SetupContext,
    popupComposition: UsePopup
) {
    const popupMinWidth = ref(props.popupMinWidth);
    const { hidePopup, popoverRef } = popupComposition;

    return () => {
        return <FPopover ref={popoverRef} visible={true} placement="bottom-left"
            host={props.popupHost} keep-width-with-reference={true} fitContent={true}
            right-boundary={props.popupRightBoundary} minWidth={popupMinWidth.value}
            offsetX={props.popupOffsetX}
            onHidden={hidePopup}>
            {context.slots.default?.()}
        </FPopover>;
    };
};
