import { ButtonEditProps } from "../button-edit.props";
import { UseButton, UseClear } from "../composition/types";

export default function (
    props: ButtonEditProps,
    useButtonComposition: UseButton,
    useClearComposition: UseClear
) {
    const { buttonClass, onClickButton, onMouseEnterButton, onMouseLeaveButton } = useButtonComposition;
    const { enableClearButton, showClearButton, onClearValue } = useClearComposition;

    return () => {
        return (
            <div class={buttonClass.value}>
                {enableClearButton.value && (
                    <span class="input-group-text input-group-clear" v-show={showClearButton.value} onClick={onClearValue}>
                        <i class="f-icon modal_close"></i>
                    </span>
                )}
                {props.buttonContent && (
                    <span class="input-group-text input-group-append-button" v-html={props.buttonContent}
                        onClick={onClickButton} onMouseenter={onMouseEnterButton} onMouseleave={onMouseLeaveButton}
                    ></span>
                )}
            </div>
        );
    };
}
