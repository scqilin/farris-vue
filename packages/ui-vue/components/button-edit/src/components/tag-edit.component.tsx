import { Ref, computed, ref } from 'vue';
import FTags from '../../../tags/src/tags.component';
import { ButtonEditProps } from '../button-edit.props';
import { UseTextBox } from '../composition/types';

export default function (
    props: ButtonEditProps,
    modelValue: Ref<string>,
    useTextBoxComposition: UseTextBox
) {
    const separator = ref(props.separator);
    const { changeTextBoxValue } = useTextBoxComposition;

    const tags = computed(() => {
        return modelValue.value ?
            modelValue.value.split(separator.value).map((tagValue: string) => { return { name: tagValue, selectable: true }; }) : [];
    });

    function onTagsChange(tags: any[]) {
        changeTextBoxValue(tags.map((tag: any) => tag.name).join(separator.value), true);
    }

    return () => {
        return <FTags class="form-control" data={tags.value} showClose={true} showInput={true} onChange={onTagsChange}></FTags>;
    };
}
