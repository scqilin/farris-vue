import { Ref } from "vue";
import { ButtonEditProps } from "../button-edit.props";
import { UseTextBox } from "../composition/types";

export default function (
    props: ButtonEditProps,
    modelValue: Ref<string>,
    useTextBoxComposition: UseTextBox
) {
    const { isTextBoxReadonly, textBoxClass, textBoxPlaceholder, textBoxTitle,
        onBlurTextBox, onClickTextBox, onFocusTextBox, onInput, onKeyDownTextBox, onKeyUpTextBox, onMouseDownTextBox, onTextBoxValueChange
    } = useTextBoxComposition;

    return () => {
        return <input
            name="input-group-value"
            autocomplete={!props.autoComplete ? 'off' : 'on'}
            class={textBoxClass.value}
            disabled={props.disable}
            maxlength={props.maxLength}
            minlength={props.minLength}
            placeholder={textBoxPlaceholder.value}
            readonly={isTextBoxReadonly.value}
            tabindex={props.tabIndex}
            title={textBoxTitle.value}
            type={props.inputType}
            value={modelValue.value}
            onBlur={onBlurTextBox}
            onChange={onTextBoxValueChange}
            onClick={onClickTextBox}
            onFocus={onFocusTextBox}
            onInput={onInput}
            onKeydown={onKeyDownTextBox}
            onKeyup={onKeyUpTextBox}
            onMousedown={onMouseDownTextBox}
        />;
    };
}
