/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { EditorConfig } from '../../dynamic-form/src/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import buttonEditSchema from './schema/button-edit.schema.json';

type TextAlignment = 'left' | 'center' | 'right';

export type ButtonBehavior = 'Overlay' | 'Popup' | 'Execute';

export type ButtonEditInputType = 'text' | 'tag';

export const buttonEditProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 扩展按钮显示内容，这是一段现在扩展按钮中的html标签
     */
    buttonContent: { type: String, default: '<i class="f-icon f-icon-lookup"></i>' },

    buttonBehavior: { type: String as PropType<ButtonBehavior>, default: 'Popup' },
    /**
     * 启用输入框自动完成功能
     */
    autoComplete: { type: Boolean, default: false },
    /**
     * 组件自定义样式
     */
    customClass: { type: String, default: '' },
    /**
     * 禁用组件，既不允许在输入框中录入，也不允许点击扩展按钮。
     */
    disable: { type: Boolean, default: false },
    /**
     * 允许在输入框中录入文本。
     */
    editable: { type: Boolean, default: true },
    /**
     * 显示清空文本按钮
     */
    enableClear: { type: Boolean, default: false },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },
    /**
     * 将组件设置为只读，既不允许在输入框中录入，也不允许点击扩展按钮，但是允许复制输入框中的内容。
     */
    readonly: { type: Boolean, default: false },
    /**
     * 文本对齐方式
     */
    textAlign: { type: String as PropType<TextAlignment>, default: 'left' },
    /**
     * 禁用组件时，是否显示扩展按钮
     */
    showButtonWhenDisabled: { type: Boolean, default: false },
    /**
     * 显示输入框的标签
     */
    enableTitle: { type: Boolean, default: false },
    /**
     * 输入框类型
     */
    inputType: { type: String as PropType<ButtonEditInputType>, default: 'text' },
    /**
     * 显示输入框提示信息
     */
    forcePlaceholder: { type: Boolean, default: false },
    /**
     * 输入框提示文本
     */
    placeholder: { type: String, default: '' },
    /**
     * 输入框最小长度
     */
    minLength: Number,
    /**
     * 输入框最大长度
     */
    maxLength: Number,
    /**
     * 输入框Tab键索引
     */
    tabIndex: Number,

    popupHost: { type: Object as PropType<any> },

    popupRightBoundary: { type: Object as PropType<any> },

    popupOffsetX: { type: Object as PropType<any> },

    popupOnInput: { type: Boolean, default: false },

    popupOnFocus: { type: Boolean, default: false },

    popupMinWidth: { type: Number, default: 320 },

    wrapText: { type: Boolean, default: false },
    /**
     * 可选，是否支持多选
     * 默认`false`
     */
    multiSelect: { type: Boolean, default: false },
    /**
     * 可选，分隔符
     * 默认`,`
     */
    separator: { type: String, default: ',' },
} as Record<string, any>;

export type ButtonEditProps = ExtractPropTypes<typeof buttonEditProps>;

export const propsResolver = createPropsResolver<ButtonEditProps>(buttonEditProps, buttonEditSchema, schemaMapper);
