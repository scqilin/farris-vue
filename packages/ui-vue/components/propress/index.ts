import type { App } from 'vue';
import Progress from './src/progress.component';

export * from './src/progress.props';

export { Progress };

export default {
    install(app: App): void {
        app.component(Progress.name, Progress);
    }
};
