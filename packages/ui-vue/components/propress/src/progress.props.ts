import { ExtractPropTypes, PropType } from 'vue';

export type ProgressType = 'line' | 'circle' | 'dashboard';
export type ProgressSize = 'default' | 'small';
export type ProgressStatus = 'default' | '';
export type ProgressStrokeColorType = '';
export type ProgressStrokeLinecapType = 'round' | 'square';

export const progressProps = {
    type: { type: String as PropType<ProgressType>, default: 'line' },
    strokeWidth: { type: Number, default: 0 },
    size: { type: String as PropType<ProgressSize>, default: 'default' },
    showInfo: { type: Boolean, default: true },
    progressStatus: { type: String as PropType<ProgressStatus>, default: 'default' },
    successPercent: { type: Number, default: 0 },
    strokeColor: { type: String as PropType<ProgressStrokeColorType>, default: '' },
    strokeLinecap: { type: String as PropType<ProgressStrokeLinecapType>, default: 'round' },
    width: { type: Number, default: 0 },
    percent: { type: Number, default: 0 }
};

export type ProgressProps = ExtractPropTypes<typeof progressProps>;
