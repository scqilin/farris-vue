import { of } from 'rxjs';
import { ref, SetupContext } from 'vue';
import { DiscussionEditorProps } from '../../discussion-editor.props';

export function useDiscussionEditor(props: DiscussionEditorProps, context: SetupContext) {
    const personSearchUrl = ref(props.personSearchUrl);
    const personnelsDisplayKey = ref(props.personnelsDisplayKey);
    const relativeVisible = ref(false);
    /** 选择要发送的人员列表 */
    const selectedPersonnels: any = [];
    /** 搜索人事管理弹窗 */
    const personModalVisible = ref(false);
    /** 阻止冒泡 */
    function stopBubble(e: any) {
        if (e && e.stopPropagation) {
            e.stopPropagation();
        }
    }
    /** 判断是否在数组 */
    function _isInArray(value: any, fieldInArray: string, array: any[]) {
        if (!value || !fieldInArray) {
            return false;
        }
        return array.findIndex(item => value === item[fieldInArray]) !== -1;
    }
    function getSearchData(text: string, pageIndex: number) {
        if (personSearchUrl.value) {
            const url = `${personSearchUrl.value}?param=${text}&pageSize=20&pageIndex=${pageIndex}`;
            // return http.get(url);
        }
        return of(true);
    }
    /** 获得占位头像 */
    function getAvatar(item: any) {
        if (item && item[personnelsDisplayKey.value]) {
            const str = item[personnelsDisplayKey.value];
            return str.substring(str.length - 2, str.length);
        }
        return '';
    }
    function setRelativeValue() {
        personModalVisible.value = false;
        relativeVisible.value = false;
    }

    return {
        personSearchUrl,
        personnelsDisplayKey,
        /** 搜索人事管理弹窗 */
        personModalVisible,
        relativeVisible,
        selectedPersonnels,
        stopBubble,
        _isInArray,
        getSearchData,
        getAvatar,
        setRelativeValue,
    };
}
