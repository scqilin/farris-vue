export enum MsgInfo {
    Cancel = 0,
    Confirm = 1
}
export interface editAttachFile {
    id?: string;
    name: string;
    size: number;
    metadataId: string;
}
export interface ValueConfig {
    msgInfo: MsgInfo;
    text: string;
    mailTos: Array<{ userId: string; userName: string }>;
    mailToSections: Array<any>;
    visibility: string;
    parentId: string;
    attachFiles?: editAttachFile[];
}
