export type MapperFunction = (key: string, value: any) => Record<string, any>;

export interface DynamicResolver {
    getSchemaByType: (componentType: string) => Record<string, any> | null;
};

export type SchemaResolverFunction = (
    dynamicResolver: DynamicResolver,
    schema: Record<string, any>,
    resolveContext: Record<string, any>
) => Record<string, any>;
