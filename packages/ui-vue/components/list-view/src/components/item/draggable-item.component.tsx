import { UseDataView, UseDraggable, UseHover, UseItem, UseRemove, UseSelection } from "../../composition/types";
import { ListViewProps } from "../../list-view.props";
import FListViewCheckBox from '../../components/list-view-checkbox.component';
import { SetupContext, computed, ref } from "vue";

export default function (
    props: ListViewProps,
    context: SetupContext,
    dataViewComposition: UseDataView,
    useDraggableComposition: UseDraggable,
    useHoverComposition: UseHover,
    useItemCompostion: UseItem,
    useSelectionComposition: UseSelection,
    useRemoveComposition: UseRemove
) {
    const enableMultiSelect = ref(props.multiSelect);
    const disableField = ref(props.disableField);
    const { onMouseenterItem, onMouseoverItem, onMouseoutItem } = useHoverComposition;
    const { getKey, listViewItemClass, onCheckItem, onClickItem } = useItemCompostion;
    const { dragstart, dragenter, dragover, dragend } = useDraggableComposition;
    const { removeItem } = useRemoveComposition;

    const listContentInnerStyle = computed(() => {
        const styleObject = {
            'margin': enableMultiSelect.value ? '10px 0' : '10px 0px 10px 14px'
        } as Record<string, any>;
        return styleObject;
    });

    function defaultItemContentRender(item: any) {
        return <div style={listContentInnerStyle.value}>{item.name}</div>;
    }

    function getContentRender() {
        return context.slots.itemContent ? context.slots.itemContent : defaultItemContentRender;
    }

    const renderItemContent = getContentRender();

    function renderItem(item: any, index: number, clickItem: any) {
        return (
            <li
                class={listViewItemClass(item, index)}
                id={getKey(item, index)}
                key={getKey(item, index)}
                onClick={(payload: MouseEvent) => onClickItem(payload, item, index)}
                onMouseenter={(payload: MouseEvent) => onMouseenterItem(payload, item, index)}
                onMouseover={(payload: MouseEvent) => onMouseoverItem(payload, item, index)}
                onMouseout={(payload: MouseEvent) => onMouseoutItem(payload, item, index)}
                draggable="true"
                onDragstart={(payload: DragEvent) => dragstart(payload, item, index)}
                onDragenter={(payload: DragEvent) => dragenter(payload, index)}
                onDragend={(payload: DragEvent) => dragend(payload, item)}
                onDragover={(payload: DragEvent) => dragover(payload, index)}>
                {enableMultiSelect.value && (
                    <div class="f-list-select" onClick={(payload: MouseEvent) => payload.stopPropagation()}>
                        <FListViewCheckBox
                            id={'list-' + getKey(item, index)}
                            disabled={item[disableField.value] || item.unClick}
                            checked={item.checked}
                            onChange={($event: any) => onCheckItem(item, index)}></FListViewCheckBox>
                    </div>
                )}
                <div class="f-list-content">
                    {renderItemContent(item)}
                </div>
                <div class="f-list-remove" onClick={(payload: MouseEvent) => removeItem(index)}>
                    <div class="f-list-remove-icon"><i class="f-icon f-icon-remove_face"></i></div>
                </div>
                <div class="f-list-handle"><div><i class="f-icon f-icon-drag-vertical"></i></div></div>
            </li>
        );
    }

    return { renderItem };
}
