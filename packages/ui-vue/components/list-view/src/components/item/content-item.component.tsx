import { UseDataView, UseDraggable, UseHover, UseItem, UseRemove, UseSelection } from "../../composition/types";
import { ListViewProps } from "../../list-view.props";
import FListViewCheckBox from '../../components/list-view-checkbox.component';
import { SetupContext, ref } from "vue";

export default function (
    props: ListViewProps,
    context: SetupContext,
    dataViewComposition: UseDataView,
    useDraggableComposition: UseDraggable,
    useHoverComposition: UseHover,
    useItemCompostion: UseItem,
    useSelectionComposition: UseSelection,
    useRemoveComposition: UseRemove
) {
    const enableMultiSelect = ref(props.multiSelect);
    const disableField = ref(props.disableField);
    const { onMouseenterItem, onMouseoverItem, onMouseoutItem } = useHoverComposition;
    const { getKey, listViewItemClass, onCheckItem, onClickItem } = useItemCompostion;

    function renderListViewItemContent(item: any, index: number, selectedItem: any) {
        if (context.slots.content) {
            return <>{context.slots.content && context.slots.content({ item, index, selectedItem })}</>;
        }
        return <div style="margin: 10px 0;">{item.name}</div>;
    }

    function renderItem(item: any, index: number, clickItem: any) {
        return (
            <li
                class={listViewItemClass(item, index)}
                id={getKey(item, index)}
                key={getKey(item, index)}
                onClick={(payload: MouseEvent) => onClickItem(payload, item, index)}
                onMouseenter={(payload: MouseEvent) => onMouseenterItem(payload, item, index)}
                onMouseover={(payload: MouseEvent) => onMouseoverItem(payload, item, index)}
                onMouseout={(payload: MouseEvent) => onMouseoutItem(payload, item, index)}
            >
                {enableMultiSelect.value && (
                    <div class="f-list-select" onClick={(payload: MouseEvent) => payload.stopPropagation()}>
                        <FListViewCheckBox
                            id={'list-' + getKey(item, index)}
                            disabled={item[disableField.value] || item.unClick}
                            checked={item.checked}
                            onChange={($event: any) => onCheckItem(item, index)}></FListViewCheckBox>
                    </div>
                )}
                <div class="f-list-content">
                    {renderListViewItemContent(item, index, clickItem)}
                </div>
            </li>
        );
    }

    return { renderItem };
}
