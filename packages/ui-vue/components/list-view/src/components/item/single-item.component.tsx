import { UseDataView, UseDraggable, UseHover, UseItem, UseRemove, UseSelection } from "../../composition/types";
import { ListViewProps } from "../../list-view.props";
import FListViewCheckBox from '../../components/list-view-checkbox.component';
import { SetupContext, computed, ref } from "vue";

export default function (
    props: ListViewProps,
    context: SetupContext,
    dataViewComposition: UseDataView,
    useDraggableComposition: UseDraggable,
    useHoverComposition: UseHover,
    useItemCompostion: UseItem,
    useSelectionComposition: UseSelection,
    useRemoveComposition: UseRemove
) {
    const listViewSize = ref(props.size);
    const textField = ref(props.textField);
    const enableMultiSelect = ref(props.multiSelect);
    const disableField = ref(props.disableField);
    const { onMouseenterItem, onMouseoverItem, onMouseoutItem } = useHoverComposition;
    const { getKey, listViewItemClass, onCheckItem, onClickItem } = useItemCompostion;

    const contentStyle = computed(() => {
        const styleObject = {
            'margin': listViewSize.value === 'small' ? '0.25rem 0' : '10px 0'
        } as Record<string, any>;
        return styleObject;
    });

    function renderItem(item: any, index: number, clickItem: any) {
        item.checked = useSelectionComposition.findIndexInSelectedItems(item) > -1;
        return (
            <li
                class={listViewItemClass(item, index)}
                id={getKey(item, index)}
                key={getKey(item, index)}
                onClick={(payload: MouseEvent) => onClickItem(payload, item, index)}
                onMouseenter={(payload: MouseEvent) => onMouseenterItem(payload, item, index)}
                onMouseover={(payload: MouseEvent) => onMouseoverItem(payload, item, index)}
                onMouseout={(payload: MouseEvent) => onMouseoutItem(payload, item, index)}
            >
                {enableMultiSelect.value && (
                    <div class="f-list-select" onClick={(payload: MouseEvent) => payload.stopPropagation()}>
                        <FListViewCheckBox
                            id={'list-' + getKey(item, index)}
                            disabled={item[disableField.value] || item.unClick}
                            checked={item.checked}
                            onChange={(payload: any) => onCheckItem(item, index)}></FListViewCheckBox>
                    </div>
                )}
                <div class="f-list-content">
                    <div style={contentStyle.value}>{item[textField.value]}</div>
                </div>
            </li>
        );
    }

    return { renderItem };
}
