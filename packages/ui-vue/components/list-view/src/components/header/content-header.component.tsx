import { SetupContext, computed, ref } from "vue";
import { ListViewProps } from "../../list-view.props";
import { UseSearch } from "../../composition/types";

export default function (
    props: ListViewProps,
    context: SetupContext,
    useSearchComposition: UseSearch
) {

    function renderHeader() {
        return (
            context.slots.header && <div class="f-list-view-header">{context.slots.header()}</div>
        );
    }

    return { renderHeader };

}
