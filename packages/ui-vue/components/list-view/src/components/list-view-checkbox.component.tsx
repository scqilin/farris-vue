/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, ref, SetupContext, watch } from 'vue';
import { listViewCheckBoxProps, ListViewCheckBoxProps } from './list-view-checkbox.props';

export default defineComponent({
    name: 'FListViewCheckBox',
    props: listViewCheckBoxProps,
    emits: ['change', 'update:modelValue'],
    setup(props: ListViewCheckBoxProps, context: SetupContext) {
        const checked = ref(props.checked);
        const disabled = ref(props.disabled);
        const id = ref(props.id);
        // const modelValue = ref(props.modelValue);

        function handleClick(event: MouseEvent) {
            event.stopPropagation();
            event.preventDefault();
            if (!disabled.value) {
                checked.value = !checked.value;
                // modelValue.value = !modelValue.value;
                // context.emit('update:modelValue', modelValue.value);
                // context.emit('change', checked.value);
                context.emit('change', { originalEvent: event, checked: checked.value });
            }
        }

        watch(() => props.checked, (newValue: boolean) => {
            checked.value = newValue;
        });

        return () => {
            return (
                <div class="custom-control custom-checkbox  custom-control-inline listview-checkbox">
                    <input id={id.value} title={id.value} type="checkbox" class="custom-control-input"
                        v-model={checked.value} onChange={(payload) => console.log(payload)}
                    ></input>
                    <label for={id.value} class="custom-control-label" onClick={(payload: MouseEvent) => handleClick(payload)}></label>
                </div>
            );
        };
    }
});
