import { Ref } from "vue";

export interface UseItem {

    getKey(item: any, index: number): any;

    listViewItemClass(item: any, index: number): Record<string, any>;

    onCheckItem(item: any, index: number): any;

    onClickItem($event: MouseEvent, item: any, index: number): any;
}

export interface UseSelection {
    clearSelection(): void;

    findIndexInSelectedItems(item: any): number;

    selectedItems: Ref<any[]>;

    toggleSelectItem(item: any, index: number): void;
}

export interface UseSearch {
    search(searchingText: string): void;
}

export interface UseDataView {

    dataView: Ref<any[]>;

    getSelectionItems: (selectionValues: string[]) => any[];
}

export interface UseDraggable {

    dragstart: (e: DragEvent, item: any, index: number) => void;

    dragenter: (e: DragEvent, index: number) => void;

    dragover: (e: DragEvent, index: number) => void;

    dragend: (e: DragEvent, item: any) => void;

    isDragging: Ref<boolean>;
}

export interface UseRemove {
    removeItem(index: number): void;
}

export interface UseHover {

    activeIndex: Ref<number>;

    focusedItemId: Ref<string>;

    hoverIndex: Ref<number>;

    onMouseenterItem($event: MouseEvent, item: any, index: number): any;

    onMouseoverItem($event: MouseEvent, item: any, index: number): any;

    onMouseoutItem($event: MouseEvent, item: any, index: number): any;

    resumeHover(): void;

    suspendHover(): void;
}
