import { SetupContext } from "vue";
import { ListViewProps } from "../list-view.props";
import { UseDataView, UseRemove } from "./types";

export function useRemove(
    props: ListViewProps,
    context: SetupContext,
    dataViewComposition: UseDataView
): UseRemove {

    const { dataView } = dataViewComposition;

    function removeItem(index: number) {
        if (index > -1 && index < dataView.value.length) {
            const removedItem = dataView.value.splice(index, 1);
            context.emit('removeItem', removedItem[0]);
        }
    }

    return { removeItem };
}
