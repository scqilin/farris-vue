import { Ref, SetupContext, computed, ref } from "vue";
import { ListViewProps, MultiSelectMode } from "../list-view.props";
import { UseDraggable, UseHover, UseItem, UseSelection } from "./types";
import { useHover } from "./use-hover";

export function useItem(
    props: ListViewProps,
    context: SetupContext,
    useDraggableComposition: UseDraggable,
    useHoverComposition: UseHover,
    useSelectionComposition: UseSelection,
): UseItem {
    const identifyField = ref(props.idField);
    const disableField = ref(props.disableField);
    const draggable = ref(props.draggable);
    const customListViewItemClass = ref(props.itemClass);
    // const hoverIndex = ref(-1);
    // const activeIndex = ref(-1);
    // const focusedItemId = ref('');
    const enableMultiSelect = ref(props.multiSelect);
    const multiSelectMode: Ref<MultiSelectMode> = ref(props.multiSelectMode as MultiSelectMode);
    const keepSelect = ref(true);
    // const selectedItems: Ref<any[]> = ref([]);
    const { isDragging } = useDraggableComposition;
    const { activeIndex, focusedItemId, hoverIndex } = useHoverComposition;
    const { clearSelection, selectedItems, toggleSelectItem } = useSelectionComposition;

    function listViewItemClass(item: any, index: number) {
        const classObject = {
            'f-list-view-group-item': true,
            'f-list-view-draggable-item': draggable.value,
            'f-un-click': !!item.unClick,
            'f-un-select': !!item[disableField.value],
            'f-listview-active': item.__fv_index__ === activeIndex.value,
            'f-listview-hover': !isDragging.value && index === hoverIndex.value,
            'moving': !!item.moving
        } as Record<string, boolean>;
        const customClassArray = customListViewItemClass.value.split(' ');
        customClassArray.reduce((result, className) => {
            result[className] = true;
            return result;
        }, classObject);
        return classObject;
    };

    function getKey(item: any, index: number) {
        return item[identifyField.value] || '';
    }

    const shouldFocusedItemOnCheck = computed(() => {
        return !enableMultiSelect.value;
    });

    function onCheckItem(item: any, index: number) {
        if (item.unClick || item[disableField.value]) {
            return;
        }
        if (shouldFocusedItemOnCheck.value) {
            focusedItemId.value = item[identifyField.value];
        }
        toggleSelectItem(item, index);
    }

    const shouldClearSelectionOnClick = computed(() => {
        return enableMultiSelect.value && multiSelectMode.value === 'OnCheckClearByClick';
    });

    const shouldSelectItemOnClick = computed(() => {
        return !enableMultiSelect.value ||
            (
                enableMultiSelect.value &&
                (multiSelectMode.value === 'OnCheckAndClick' || multiSelectMode.value === 'OnClick')
            );
    });

    function onClickItem($event: MouseEvent, item: any, index: number) {
        if (item.unClick || item[disableField.value]) {
            $event.preventDefault();
            $event.stopPropagation();
            return;
        }
        focusedItemId.value = item[identifyField.value];
        activeIndex.value = index;
        if (shouldClearSelectionOnClick.value) {
            clearSelection();
        }
        if (shouldSelectItemOnClick.value) {
            toggleSelectItem(item, index);
        }
        context.emit('clickItem', { data: selectedItems.value, index });
    }

    return {
        getKey,
        listViewItemClass,
        onCheckItem,
        onClickItem
    };
}
