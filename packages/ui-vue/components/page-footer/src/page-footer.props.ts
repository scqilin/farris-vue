import { ExtractPropTypes } from 'vue';

export const pageFooterProps = {
    collapsed: { type: Boolean, default: true },
    collapsedText: { type: String, default: 'More' },
    contentClass: { type: String, default: '' },
    enableCollapse: { type: Boolean, default: false },
    expandedText: { type: String, default: 'More' },
    headerClass: { type: String, default: '' },
    headerContentClass: { type: String, default: '' },
    headerToolbarClass: { type: String, default: '' },
    showHeader: { type: String, default: false },
};
export type PageFooterProps = ExtractPropTypes<typeof pageFooterProps>;
