import { App } from 'vue';
import PageFooter from './src/page-footer.component';

export * from './src/page-footer.props';
export { PageFooter };

export default {
    install(app: App): void {
        app.component(PageFooter.name, PageFooter);
    }
};
