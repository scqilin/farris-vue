import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import contentContainerSchema from './schema/content-container.schema.json';

export const contentContainerProps = {
    customClass: { type: String, default: '' }
} as Record<string, any>;

export type ContentContainerPropsType = ExtractPropTypes<typeof contentContainerProps>;

export const propsResolver = createPropsResolver<ContentContainerPropsType>(contentContainerProps, contentContainerSchema, schemaMapper);
