import { SetupContext, defineComponent, inject, onMounted, ref } from 'vue';
import { ContentContainerPropsType, contentContainerProps } from '../content-container.props';
import { useDesignerRules } from './use-designer-rules';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FContentContainerDesign',
    props: contentContainerProps,
    emits: [],
    setup(props: ContentContainerPropsType, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div ref={elementRef} class="drag-container" dragref={`${designItemContext.schema.id}-container`}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
