/* eslint-disable max-len */
/* eslint-disable complexity */
import { nextTick, ref } from "vue";
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/use-dragula-common-rule";
import { ComponentSchema } from "../../../designer-canvas/src/types";
import { getSchemaByType } from "../../../dynamic-resolver/src/schema-resolver";

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {
    /** 组件在拖拽时是否需要将所属的Component一起拖拽  */
    let triggerBelongedComponentToMoveWhenMoved = ref(false);

    /** 组件在删除时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToDeleteWhenDeleted = ref(false);

    let canAcceptChildContent = true;
    let hideNestedPadding = false;

    let schemaContext: any;

    let isInFixedContextRules = false;

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext);
        if (!basalRule) {
            return false;
        }
        if (!canAcceptChildContent) {
            return false;
        }

        if (!checkFormContainerAcceptable(draggingContext)) {
            return false;
        }
        return true;
    }

    /**
     * 用container表示的form组件，只接收form-group输入类控件
     */
    function checkFormContainerAcceptable(draggingContext: DraggingResolveContext) {

        if (schemaContext && schemaContext.componentClassList && schemaContext.componentClassList.includes('f-form-layout') && schemaContext.componentClassList.includes('farris-form')) {
            // 从工具箱拖入的输入类控件
            if (draggingContext.sourceType === 'control' && draggingContext.controlCategory === 'input') {
                return true;
            }

            // 画布中移动位置的输入类控件
            if (draggingContext.sourceType === 'move' && draggingContext.controlType === 'form-group') {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * 判断当前容器的上下文
     */
    function resolveComponentContext() {
        triggerBelongedComponentToMoveWhenMoved.value = false;
        triggerBelongedComponentToDeleteWhenDeleted.value = false;
        const component = schema;

        // 控件本身样式
        const componentClass = component.appearance && component.appearance.class || '';
        const componentClassList = componentClass.split(' ');

        // 子级节点
        const childContents = component.contents || [];
        const firstChildContent = childContents.length ? childContents[0] : null;
        const firstChildClass = firstChildContent && firstChildContent.appearance ? firstChildContent.appearance.class : '';
        const firstChildClassList = firstChildClass ? firstChildClass.split(' ') : [];

        // 父级节点
        const parent = parentSchema;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        schemaContext = {
            componentClass,
            componentClassList,
            childContents,
            firstChildContent,
            firstChildClass,
            firstChildClassList,
            parent,
            parentClass,
            parentClassList
        }
        // 1、顶层节点
        if (componentClassList.includes('f-page')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 2、页头
        if (componentClassList.includes('f-page-header')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 2、header标题模板的父级容器
        if (componentClassList.includes('f-title') && parentClassList.includes('f-page-header-base') && firstChildContent && firstChildContent.type === 'html-template') {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 3、header标题模板的祖父级容器
        if (componentClassList.includes('f-page-header-base') && parentClassList.includes('f-page-header') && firstChildClassList.includes('f-title')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 3、页面主区域
        if (componentClassList.includes('f-page-main') && parentClassList.includes('f-page')) {
            // 卡片类模板
            if (childContents.length === 1 && firstChildClassList.includes('f-struct-like-card')) {
                canAcceptChildContent = false;
            }
            // 列卡类
            if (firstChildClassList.includes('f-page-content')) {
                canAcceptChildContent = false;
            }
            // 滚动监听类
            if (firstChildContent && firstChildContent.type === "scroll-spy") {
                canAcceptChildContent = false;
            }

            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 3、页面主区域like-card，若只有一个，那么不可移动、删除，但可以接收控件
        if (componentClassList.includes('f-struct-like-card') && parent && parent.contents?.length === 1 && parentClassList.includes('f-page-main')) {
            hideNestedPadding = true;
            return true;
        }
        // 4、隐藏子表DataGrid父级间距
        if (componentClassList.includes('f-grid-is-sub') && parentClassList.includes('f-struct-is-subgrid') && firstChildClassList.includes('f-component-grid')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 4、子表三层父容器 container---section-tab 中container不接收新控件
        if (componentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-section-tabs')) {
            if (firstChildContent?.contents && firstChildContent.contents.length === 1) {
                const tabEle = firstChildContent.contents[0];
                if (tabEle.type === 'tabs') {
                    canAcceptChildContent = false;
                }
            }
        }

        // 5、子表分组容器：container-section-componentRef
        if (componentClassList.includes('f-struct-wrapper') && (firstChildClassList.includes('f-section-in-main') || firstChildClassList.includes('f-section-in-mainsubcard'))) {
            if (firstChildContent?.type === 'section' && firstChildContent.contents && firstChildContent.contents.length === 1) {
                const cmpRefEle = firstChildContent.contents[0];
                if (cmpRefEle.type === 'component') {
                    canAcceptChildContent = false;
                }
            }
        }
        // 5、滚动监听容器
        if (parent && parent.contents?.length && parent.contents?.length > 1) {
            const preSibling = parent.contents[0];
            const preSiblingType = preSibling && preSibling.type || '';
            if (componentClassList.includes('f-scrollspy-content') && parentClassList.includes('f-page-main') && preSiblingType === 'scroll-spy') {
                canAcceptChildContent = false;
                hideNestedPadding = true;
                isInFixedContextRules = true;
                return true;
            }

        }
        // 5、填报模板的框架容器
        if (componentClassList.includes('f-page-layout') && parentClassList.includes('f-page')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 5、填报模板的框架容器
        if (componentClassList.includes('f-page-container') && parentClassList.includes('f-page-layout')) {
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 5、带导航的模板的框架容器
        if (componentClassList.includes('f-page-content') && parentClassList.includes('f-page-main')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 5、带导航的模板的框架容器
        if (componentClassList.includes('f-page-content-main') && parentClassList.includes('f-page-content')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            return true;
        }
        // 5、筛选条容器
        if (componentClassList.includes('f-filter-container') && firstChildContent && firstChildContent.type === 'list-filter') {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 树组件模板、列表组件模板框架容器
        if (componentClassList.includes('f-page-module')) {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;
        }
        // 树组件模板、列表组件模板TreeGrid父级容器间距
        if (componentClassList.includes('f-utils-fill-flex-column') && parent && parent.type === 'component' && parentClassList.includes('f-utils-fill-flex-column')) {
            if (firstChildContent && (firstChildContent.type === 'tree-grid' || firstChildContent.type === 'data-grid')) {
                canAcceptChildContent = false;
                hideNestedPadding = true;
                isInFixedContextRules = true;
                return true;
            }
        }
        // ListView模板的框架容器
        if (componentClassList.includes('farris-main-area') && parent && parent.type === 'component' && parent.componentType === 'frame') {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;

        }

        // ListView模板中ListView组件---ContentContainer---ListView 的三层结构
        if (parent && parent.type === 'component' && (parent.componentType === 'list-view' || parent.componentType === 'ListView') && firstChildContent && firstChildContent.type === 'list-view') {
            canAcceptChildContent = false;
            hideNestedPadding = true;
            isInFixedContextRules = true;
            return true;

        }

        // 用container表示的form组件
        if (componentClassList.includes('f-form-layout') && componentClassList.includes('farris-form')) {
            triggerBelongedComponentToMoveWhenMoved.value = true;
            triggerBelongedComponentToDeleteWhenDeleted.value = true;
        }

        isInFixedContextRules = false;
        return false;
    }

    function onAcceptMovedChildElement(sourceElement: DesignerHTMLElement) {
        nextTick();
    }

    function getStyles() {
        const component = schema;
        if (component.componentType) {
            return 'display:inherit;flex-direction:inherit;margin-bottom:10px';
        }
        return '';
    }

    function onAcceptNewChildElement(element: DesignerHTMLElement, targetPosition: number, compnentSchema: ComponentSchema): ComponentSchema {
        const component = schema;
        // 控件本身样式
        const componentClass = component.appearance && component.appearance.class || '';
        const componentClassList = componentClass.split(' ') as string[];
        if (componentClassList.includes('f-form-layout')) {
            const label = String(element.innerText);
            const formGroupElementSchema = getSchemaByType('form-group') as ComponentSchema;
            formGroupElementSchema.editor = compnentSchema;
            formGroupElementSchema.label = label;
            return formGroupElementSchema;
        }
        return compnentSchema;
    }

    function checkCanMoveComponent() {
        return !isInFixedContextRules;
    }
    function checkCanDeleteComponent() {
        return !isInFixedContextRules;
    }

    function hideNestedPaddingInDesginerView() {
        return hideNestedPadding;
    }

    return {
        canAccepts,
        resolveComponentContext,
        getStyles,
        onAcceptNewChildElement,
        onAcceptMovedChildElement,
        triggerBelongedComponentToMoveWhenMoved,
        triggerBelongedComponentToDeleteWhenDeleted,
        checkCanMoveComponent,
        checkCanDeleteComponent,
        hideNestedPaddingInDesginerView
    };
}
