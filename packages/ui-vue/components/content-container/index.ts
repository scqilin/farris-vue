/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import ContentContainer from './src/content-container.component';
import ContentContainerDesign from './src/designer/content-container.design.component';
import { propsResolver } from './src/content-container.props';

export * from './src/content-container.props';
export { ContentContainer, ContentContainerDesign };

export default {
    install(app: App): void {
        app.component(ContentContainer.name, ContentContainer);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['content-container'] = ContentContainer;
        propsResolverMap['content-container'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['content-container'] = ContentContainerDesign;
        propsResolverMap['content-container'] = propsResolver;
    }
};
