/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref } from 'vue';
import { UseVirtualScroll } from '../../composition/types';
import { TreeViewProps } from '../../tree-view.props';

export default function (props: TreeViewProps, treeContentRef: Ref<any>, useVirtualScrollComposition: UseVirtualScroll) {
    const { horizontalScrollThumbStyle, onMouseDownScrollThumb } = useVirtualScrollComposition;

    /** 渲染水平滚动条 */
    function renderHorizontalScrollbar() {
        return (
            <div class="fv-tree-horizontal-scroll">
                <div
                    class="fv-tree-horizontal-scroll-thumb"
                    style={horizontalScrollThumbStyle.value}
                    onMousedown={(payload: MouseEvent) => onMouseDownScrollThumb(payload, treeContentRef, 'horizontal')}></div>
            </div>
        );
    }

    return { renderHorizontalScrollbar };
}
