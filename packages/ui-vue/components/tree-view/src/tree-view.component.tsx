/* eslint-disable no-use-before-define */
import { SetupContext, computed, defineComponent, ref, toRefs, watch } from 'vue';
import { TreeViewProps, treeViewProps } from './tree-view.props';
import { useTreeDataView } from './composition/use-tree-data-view';
import getTreeArea from './components/data/tree-area.component';
import { useVisualTreeNode } from './composition/use-visual-tree-node';
import './tree-view.css';

export default defineComponent({
    name: 'FTreeView',
    props: treeViewProps,
    emits: ['outputValue','currentEvent'] as (string[]) | undefined,
    setup(props: TreeViewProps, context: SetupContext) {
        const treeContentRef = ref<any>();
        const mouseInContent = ref(false);
        const { treeNodeIconsData } = toRefs(props);

        const treeClass = computed(() => {
            const classObject = {
                'fv-tree': true
            } as Record<string, boolean>;
            return classObject;
        });

        const treeContentClass = computed(() => {
            const classObject = {
                'fv-tree-content': true
            } as Record<string, boolean>;
            return classObject;
        });

        /** 处理鼠标滚轮事件 */
        function onWheel(e: MouseEvent) { }

        /** 渲染水平滚动条 */
        function renderHorizontalScrollbar() {
            return [];
        }

        /** 渲染垂直滚动条 */
        function renderVerticalScrollbar() {
            return [];
        }

        /** 处理树状数据 */
        const treeDataView = useTreeDataView(props, context);

        /** 使用可视化节点的组合函数 */
        const { visibleTreeNodes } = useVisualTreeNode(props, treeDataView);

        /** 渲染树状区域 */
        const { renderTreeArea, dictTree } = getTreeArea(props, context, treeNodeIconsData, visibleTreeNodes);

        /** 新增树节点 */
        function addNewDataItem() {
            treeDataView.addNewDataItem(props.newDataItem);
        }

        /** 删除树节点 */
        function removeDataItem(deleteIndex: number) {
            treeDataView.removeDataItem(deleteIndex, dictTree);
        }

        /** 编辑树节点 */
        function editDataItem(editIndex: string | number, newName: string) {
            treeDataView.editDataItem(editIndex, newName);
        }

        /** 确定 */
        function acceptDataItem() {
            // treeDataView.acceptEditingRow(visualDataRow);
        }

        /** 取消 */
        function cancelDataItem() {
            // treeDataView.cancelEditingRow(visualDataRow);
        }

        context.expose({
            addNewDataItem,
            removeDataItem,
            editDataItem,
            acceptDataItem,
            cancelDataItem
        });

        return () => {
            return (
                <div class={treeClass.value} style="height:600px" onWheel={onWheel}>
                    <div
                        ref={treeContentRef}
                        class={treeContentClass.value}
                        onMouseover={() => {
                            mouseInContent.value = true;
                        }}
                        onMouseleave={() => {
                            mouseInContent.value = false;
                        }}>
                        {renderTreeArea()}
                        {renderHorizontalScrollbar()}
                        {renderVerticalScrollbar()}
                    </div>
                </div>
            );
        };
    }
});
