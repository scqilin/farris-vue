import { ComputedRef, Ref } from 'vue';

export enum TreeNodeCellMode {
    readonly,
    editable,
    editing
}

export interface VisualTreeNodeCell {
    /** 实际高度 */
    actualHeight?: number;
    /** 单元格高度 */
    cellHeight?: number;
    /** 单元格数据 */
    data: any;
    /** 获取编辑器函数 */
    getEditor: (cell: VisualTreeNodeCell) => any;
    /** 单元格字段 */
    field: string;
    /** 单元格索引 */
    index: number;
    /** 单元格编辑模式 */
    mode: TreeNodeCellMode;
    /**  层级 */
    layer: any;
    /** 父级对象的id */
    parentId: any;
    /** 引用 */
    ref?: any;
    /** 是否被勾选 */
    checked?: boolean;
    /** 设置引用函数 */
    setRef: (vnode: any) => void;
    /** 更新函数 */
    update: (value: any) => void;
}

export interface VisualTreeNode {
    /**  是否折叠 */
    collapse: boolean;
    /** 数据信息 */
    data: Record<string, VisualTreeNodeCell>;
    /** 数据索引 */
    dataIndex: number;
    /** 是否展示该节点 */
    show: boolean;
    /**  详细信息 */
    details?: VisualTreeNode[];
    height?: number;
    index: number;
    /** 层级 */
    layer: number;
    /** 前一个节点 */
    pre?: any;
    /** 是否被选中 */
    selected: boolean;
    ref?: any;
    /** 刷新键 */
    refreshKey?: string;
    setRef: (vnode: any) => void;
    top: number;
}

export interface UseVirtualScroll {
    onMouseDownScrollThumb: ($event: MouseEvent, treeContentRef: Ref<any>, thumbType: 'vertical' | 'horizontal') => void;

    horizontalScrollThumbStyle: ComputedRef<Record<string, any>>;

    verticalScrollThumbStyle: ComputedRef<Record<string, any>>;
}

export interface UseTreeDataView {
    collapse: (collapseField: string, collapseValue: any) => any[];

    dataView: Ref<any[]>;

    expand: (expandField: string, expandValue: any) => any[];

    addNewDataItem: (newDataItem: any) => any;
    removeDataItem: (deleteIndex: number, dictTree: any) => any;
    editDataItem: (editIndex: string | number, newName: string) => void;
}

export interface UseVisualTreeNode {
    // visualDatas: any;
    // getVisualTreeNode: (pre?: VisualTreeNode, forceToRefresh?: boolean) => VisualTreeNode[];
    maxLayer?: number;
    visibleTreeNodes: any;
    // resetVisibleData: () => any;
}

export interface useTreeNodeIcon {
    treeNodeIconsClass: (visualTreeNode: VisualTreeNode, visualTreeNodeCell: VisualTreeNodeCell, leafNodes: any) => any[];
}

export interface UseTreeNodeLines {
    leafNodes: any;
    getPaddingBottom: (dictTree: any) => any[];
    getPaddingBottomLength: (index: number, halfBorderNodes: Array<string>) => any[];
    handlePaddingBottomReturnValue: (index: number, leafNodes: any) => any[];
    handleCollapsedPaddingBottom: (index: number) => any[];
    getLeafNodes: (dictTree: any) => any[];
}
export interface useAutoCheckChildrenAndCascade {
    onClickTreeNodeSelectableCell: (payload: any, visualTreeNode: any, visualTreeNodeCell: VisualTreeNodeCell, dictTree: any) => any[];
}

export interface UseDataList {
    addNewDataItem: (newDataItem: any) => any;
    removeDataItem: () => void;
    editDataItem: () => void;
    acceptDataItem: () => void;
    cancelDataItem: () => void;
}
