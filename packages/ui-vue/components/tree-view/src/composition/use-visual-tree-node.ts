/* eslint-disable max-len */
/* eslint-disable no-use-before-define */
import { TreeNodeCellMode, UseTreeDataView, UseVisualTreeNode, VisualTreeNode, VisualTreeNodeCell } from './types';
import { TreeViewProps } from '../tree-view.props';
import { watch, ref } from 'vue';
import { cloneDeep } from 'lodash';

/** 用于处理可视化树节点相关功能
 * @param treeDataViewComposition 树状数据的数据视图
 */
/** 用于处理可视化树节点相关功能
 * @param treeDataViewComposition 树状数据的数据视图
 */
export function useVisualTreeNode(props: TreeViewProps, treeDataViewComposition: UseTreeDataView): UseVisualTreeNode {
    /** 每行的默认高度 */
    const rowHeight = ref(props.cellHeight);
    /** 是否启用自动行高度计算 */
    const autoRowHeight = false;
    /** 树状数据的数据视图 */
    /** 树状数据的数据视图 */
    const treeDataViewCompositionRef = ref(treeDataViewComposition);

    /** 渲染树节点 */
    function renderTreeNode(dataItem: any, preDataItem: any, preMergingRow: VisualTreeNode, rowIndex: number, top: number): VisualTreeNode {
        const dataIndex = dataItem.__fv_data_index__;
        // 创建包含当前行信息的可视化树节点对象
        const currentRow: VisualTreeNode = {
            // 是否折叠
            collapse: false,
            // 是否显示
            show: true,
            // 当前行索引
            index: rowIndex,
            // 数据索引
            dataIndex,
            // 顶部位置
            top,
            // 层级
            layer: dataItem.layer || 0,
            // 是否被选中
            selected:false,
            pre: preDataItem,
            data: {},
            setRef: (vnode) => {
                currentRow.ref = vnode;
            }
        };
        if (!preDataItem) {
            dataItem.__fv_data_position__ = 0;
        }
        /** 如果不使用自动行高度 */
        if (!autoRowHeight) {
            currentRow.height = rowHeight.value;
            dataItem.__fv_data_height__ = currentRow.height;
            const preDataItem = currentRow.pre;
            if (preDataItem) {
                const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                currentRow.top = topPosition;
                dataItem.__fv_data_position__ = topPosition;
            }
        }
        /** 创建包含当前单元格信息的可视化节点单元格对象 */
        const currentCell: VisualTreeNodeCell = {
            index: dataItem.id,
            field: 'name',
            mode: TreeNodeCellMode.readonly,
            data: dataItem.name,
            layer: dataItem.layer,
            parentId: dataItem.parent,
            checked: dataItem.checked,
            getEditor(cell: VisualTreeNodeCell) { },
            setRef: (vnode: any) => {
                if (autoRowHeight && vnode) {
                    const visualData = currentCell.layer as VisualTreeNode;
                    // 单元格高度变化，更新当前单元格高度
                    if (currentCell.cellHeight !== vnode.offsetHeight) {
                        currentCell.cellHeight = vnode.offsetHeight;
                    }
                    // 如果单元格高度大于当前行高度，更新当前行高度
                    if (currentCell.cellHeight && currentCell.cellHeight > (visualData.height || 0)) {
                        visualData.height = currentCell.cellHeight;
                        dataItem.__fv_data_height__ = visualData.height;
                    }
                    const preDataItem = visualData.pre;
                    if (preDataItem) {
                        const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
                        visualData.top = topPosition;
                        dataItem.__fv_data_position__ = topPosition;
                    }
                }
            },
            update: (value: any) => { }
        };
        /** 将单元格信息设置为可视化节点的显示文本 */
        currentRow.data.displayText = currentCell;
        return currentRow;
    }
    let visualDatas: VisualTreeNode[] = [];

    watch(treeDataViewCompositionRef.value, () => {
        visibleTreeNodes.value = getVisualTreeNode();
    });

    /** 获取可视化树节点 */
    function getVisualTreeNode(pre?: any, forceToRefresh?: boolean): VisualTreeNode[] {
        const { dataView } = treeDataViewCompositionRef.value;
        visualDatas = [];
        // 数据视图中有数据
        if (dataView.length > 0) {
            // 是否强制刷新
            const refreshKey = forceToRefresh ? Date.now().toString() : '';
            for (let rowIndex = 0, visualIndex = 0; rowIndex < dataView.length; rowIndex++, visualIndex++) {
                // 当前数据项；
                const dataViewItem = dataView[rowIndex];
                // 获取前一个数据项，如果没有则使用传入的前一个数据项；
                const preDataItem = dataView[rowIndex - 1] || pre;
                // 获取前一个合并的可视化行
                const preMergingRow = visualDatas[visualIndex - 1];
                // 计算目标位置，考虑前一个数据项的位置和高度
                const targetPosition = preDataItem ? (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0) : 0;
                // 渲染当前行的可视化节点
                const currentRow = renderTreeNode(dataViewItem, preDataItem, preMergingRow, rowIndex, targetPosition);
                // 设置刷新键
                currentRow.refreshKey = refreshKey;
                // 将可视化节点添加到数组中
                visualDatas.push(currentRow);
            }
        }
        return visualDatas;
    }
    const visibleTreeNodes = ref(getVisualTreeNode());
    // 返回包含获取可视化树节点函数的对象
    return { visibleTreeNodes };
}
