/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseTreeDataView } from './types';
import { SetupContext, ref, inject } from 'vue';
import { TreeViewProps } from '../tree-view.props';
import { cloneDeep } from 'lodash';

export type DataViewFilter = (dataItem: any) => boolean;

/** 处理树状数据的数据视图 */
export function useTreeDataView(props: TreeViewProps, context: SetupContext): UseTreeDataView {
    /** 折叠状态映射 */
    const collapseMap = new Map<string, boolean>();
    /** 数据过滤器映射 */
    const filtersMap = new Map<string, DataViewFilter>();
    const originalData = ref(props.data);
    const NotifyService: any = inject('NotifyService');

    /** 重置数据视图 */
    function resetDataView() {
        const resetData = cloneDeep(originalData.value);
        let rowIndex = 0;
        const dataViewItems: any[] = [];
        if (resetData.length) {
            for (let index = 0; index < resetData.length; index++) {
                const dataItem = resetData[index] as any;
                // 当前行的索引
                rowIndex++;
                dataItem.__fv_data_index__ = rowIndex;
                dataItem.__fv_index__ = index;
                dataViewItems.push(cloneDeep(dataItem));
            }
        }
        return dataViewItems;
    }
    const dataView = ref(resetDataView());

    /** 删除树节点后，将剩余节点排序，重新赋id及parentId */
    function trimmedOriginalData(trimmedIndex: number, originalData: any, dictTree: any) {
        const trimmedChilds = dictTree[trimmedIndex].childs;
        trimmedChilds.unshift(trimmedIndex.toString());
        // 利用parent的循环，将所有多余值删除
        let trimmedOriginalData = originalData.filter((dataItem: any) => trimmedChilds.indexOf(dataItem.id) === -1);
        trimmedOriginalData = trimmedOriginalData.map((item: any, index: number) => ({ ...item, newId: (index + 1).toString() }));

        trimmedOriginalData.forEach((trimmedOriginalDataItem: any) => {
            const parentId = trimmedOriginalDataItem.parent;
            const matchingItem = trimmedOriginalData.find((item: any) => item.id === parentId);
            trimmedOriginalDataItem.newParent = matchingItem ? matchingItem.newId : null;
        });
        trimmedOriginalData = trimmedOriginalData.map((trimmedOriginalDataItem: any) => ({
            ...trimmedOriginalDataItem,
            id: trimmedOriginalDataItem.newId,
            parent: trimmedOriginalDataItem.newParent
        }));
        return trimmedOriginalData;
    }

    /** 新增树节点 */
    function addNewDataItem(newDataItem: any) {
        originalData.value.push(cloneDeep(newDataItem));
        dataView.value = resetDataView();
    }

    /** 删除树节点 */
    function removeDataItem(deleteIndex: number, dictTree: any) {
        const modifyItem = originalData.value[Number(deleteIndex) - 1];
        if (modifyItem.deletable !== false) {
            originalData.value = trimmedOriginalData(deleteIndex, originalData.value, dictTree);
            dataView.value = resetDataView();
        } else {
            NotifyService.show({
                showCloseButton: false,
                timeout: 3000,
                animate: 'fadeIn',
                toasts: [{ type: 'string', title: '提示', msg: '当前节点禁止删除' }],
            });
        }
    }

    /** 编辑树节点 */
    function editDataItem(editIndex: string | number, newName: string) {
        const modifyItem = originalData.value[Number(editIndex) - 1];
        if (modifyItem.editable !== false) {
            modifyItem.name = newName;
            dataView.value = resetDataView();
        } else {
            NotifyService.show({
                showCloseButton: false,
                timeout: 3000,
                animate: 'fadeIn',
                toasts: [{ type: 'string', title: '提示', msg: '当前节点禁止编辑' }],
            });
        }
    }

    /** 应用数据过滤器*/
    function applyFilter(filters: DataViewFilter[], reset = false) {
        // 根据“是否需要重置数据视图”，选择源数据来源
        const source = reset ? resetDataView() : dataView.value;
        dataView.value = source
            .filter((dataItem: any, index: number) => {
                return filters.reduce((result: boolean, filter: DataViewFilter) => {
                    return result && filter(dataItem);
                }, true);
            })
            .map((dataItem: any, index: number) => {
                dataItem.__fv_index__ = index;
                return dataItem;
            });
        return dataView.value;
    }

    /** 折叠选择的字段*/
    function collapse(collapseField: string, collapseValue: any) {
        const groupedRowId = `group_of_${collapseField}_${collapseValue}`;
        collapseMap.set(groupedRowId, true);
        /** 折叠过滤器 */
        const collapseFieldFilter = (dataItem: any) => dataItem[collapseField] !== collapseValue;
        filtersMap.set(`collapse_${collapseField}_${collapseValue}`, collapseFieldFilter);
        return applyFilter(Array.from(filtersMap.values()));
    }

    /** 展开选择的字段 */
    function expand(expandField: string, expandValue: any) {
        const groupedRowId = `group_of_${expandField}_${expandValue}`;
        collapseMap.set(groupedRowId, false);
        // 删除折叠过滤器
        filtersMap.delete(`collapse_${expandField}_${expandValue}`);
        return applyFilter(Array.from(filtersMap.values()), true);
    }

    return {
        collapse,
        dataView,
        expand,
        addNewDataItem,
        removeDataItem,
        editDataItem
    };
}
