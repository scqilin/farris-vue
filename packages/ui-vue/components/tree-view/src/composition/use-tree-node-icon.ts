import { Ref } from 'vue';
import { VisualTreeNode, VisualTreeNodeCell } from './types';
import { TreeViewProps } from '../tree-view.props';

/** 用于处理树节点图标相关功能 */
export function useTreeNodeIcon(props: TreeViewProps, treeNodeIconsData: Ref<any>): any {
    /** 显示图标 */
    function treeNodeIconsClass(visualTreeNode: VisualTreeNode, visualTreeNodeCell: VisualTreeNodeCell, leafNodes: any) {
        let styleObject = '';
        const visualTreeNodeCellIndex = Number(visualTreeNodeCell.index);
        const isLeafNode = leafNodes.indexOf(visualTreeNodeCellIndex.toString());
        // 叶子节点
        if (isLeafNode !== -1) {
            styleObject = treeNodeIconsData.value.leafnodes;
        } else {
            styleObject = visualTreeNode.collapse ? treeNodeIconsData.value.fold : treeNodeIconsData.value.unfold;
        }
        return styleObject;
    };
    // 返回包含获取可视化树节点函数的对象
    return { treeNodeIconsClass };
}
