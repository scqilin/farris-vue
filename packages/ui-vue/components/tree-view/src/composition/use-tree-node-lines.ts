/* eslint-disable max-len */
import { TreeViewProps } from '../tree-view.props';
import { ref } from 'vue';

/** 用于处理树节点连接线相关功能 */
export function useTreeNodeLines(props: TreeViewProps, visibleDatas: any, dictTree: any): any {
    /** 视图连接线长度 */
    const savedLength = ref(visibleDatas.length);
    /** 连接线颜色 */
    const lineColor = ref(props.lineColor);
    /** 单元格高度 */
    const cellHeight = ref(props.cellHeight);

    /** 计算连接线的长度 */
    function getPaddingBottom(dictTree: any, visibleDatasValue: any) {
        for (let i = 0; i < dictTree.length; i++) {
            let paddingBottom = 0;
            /** 该节点的临近子节点 */
            const neighborChild = dictTree[i].neighborChilds;
            // 若并非只有一个临近子节点
            if (neighborChild.length >= 2) {
                paddingBottom = (dictTree[i].childsLength) * cellHeight.value;
            }
            if (visibleDatasValue[i]) {
                visibleDatasValue[i].paddingBottom = paddingBottom;
            }
        }
        return visibleDatasValue;
    }

    /** 处理连接线显示一半的节点 */
    function getHalfBorderNodes(dictTree: any) {
        const halfBorderNodes = ['1'].concat(...dictTree
            .filter((dictTreeItem: any) => dictTreeItem.neighborChilds.length !== 0)
            .map((dictTreeItem: any) => [...dictTreeItem.neighborChilds.slice(-1)])
            .flat());
        const returnHalfBorderNodes = halfBorderNodes.sort((a, b) => Number(a) - Number(b));
        return returnHalfBorderNodes;
    }

    /** 计算连接线显示的长度  */

    function getPaddingBottomLength(returnIndex: number, dictTree: any) {
        const backgroundStyleArray = new Array(dictTree.length).fill(0);
        if (props.showLines) {
            const halfBorderNodes = getHalfBorderNodes(dictTree);
            dictTree.forEach((dictTreeItem: any, index: number) => {
                const isHalfBorder = halfBorderNodes.includes(index.toString());
                backgroundStyleArray[index] = `repeating-linear-gradient(90deg, ${lineColor.value} 0 1px, transparent 0px 2px) -10px ${(cellHeight.value / 2)}px/20px 1px no-repeat,repeating-linear-gradient(${lineColor.value} 0 1px, transparent 0px 2px) 0px 0px/1px ${isHalfBorder ? '50%' : '100%'} no-repeat`;
            });
            backgroundStyleArray[1] = '';
        }
        return backgroundStyleArray[returnIndex];
    }

    /** 处理连接线返回值 */
    function handlePaddingBottomReturnValue(visualTreeNodeCellIndex: number, leafNodes: any, dictTree: any, visibleDatasValue: any) {
        if (visibleDatasValue.length !== savedLength.value || visibleDatasValue[0].paddingBottom === undefined) {
            savedLength.value = visibleDatasValue.length;
            visibleDatasValue = getPaddingBottom(dictTree, visibleDatasValue);
        }
        if (props.showLines && visibleDatasValue) {
            const returnValueArray = dictTree.map((dictTreeItem: any, index: number) => {
                if (index !== 0) {
                    const isLeafNodes = leafNodes?.indexOf(index);
                    return isLeafNodes === -1 ? `${visibleDatasValue[Number(index - 1)].paddingBottom.toString()}px` : '0';
                }
                return 0;
            });
            return returnValueArray[visualTreeNodeCellIndex];
        }
    }

    /** 处理折叠后的连接线显示逻辑 */
    function handleCollapsedPaddingBottom(index: number, visibleDatasValue: any, leafNodes: any, dictTree: any) {
        // /** 当前节点的所有父节点 */
        const curNodeParents = dictTree[index].parents.concat(index.toString());
        // 当前节点的所有父节点
        curNodeParents.forEach((parent: any) => {
            // 针对每个父节点，若其连接线长度不为0，即至少有两个临近的子节点
            if (dictTree[parent].childWithLines.length !== 0) {
                // 则记录其父节点下的每个子节点的折叠情况
                dictTree[parent].childs.forEach((child: any) => {
                    if (!visibleDatasValue[child - 1].show) {
                        dictTree[parent].childWithLines = dictTree[parent].childWithLines.filter((element: any) => {
                            return element !== child;
                        });
                    }
                });
            }
        });
        // 折叠后需要修改的该节点的所有父节点
        curNodeParents.forEach((parent: string) => {
            // 折叠高度为折叠子节点的总高度
            visibleDatasValue[parent].paddingBottom = Math.max((dictTree[parent].childWithLines.length - 1) * cellHeight.value, 0);
        });
        return visibleDatasValue;
    }

    return { getPaddingBottom, getPaddingBottomLength, handlePaddingBottomReturnValue, handleCollapsedPaddingBottom };
}
