import type { App } from 'vue';
import Step from './src/step.component';

export * from './src/step.props';

export { Step };

export default {
    install(app: App): void {
        app.component(Step.name, Step);
    }
};
