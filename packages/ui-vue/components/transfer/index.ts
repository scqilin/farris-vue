import { App } from 'vue';
import Transfer from './src/transfer.component';

export * from './src/transfer.props';

export { Transfer };

export default {
    install(app: App): void {
        app.component(Transfer.name, Transfer);
    }
};
