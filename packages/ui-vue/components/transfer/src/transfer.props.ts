import { ExtractPropTypes } from "vue";

export const transferProps = {
    dataSource: { type: Array<object>, default: [] },
    displayType: { type: String, default: 'List' },
    enableSearch: { type: Boolean, default: true },
    identifyField: { type: String, default: 'id' },
    placeholder: { type: String, default: '' },
    selections: { type: Array<object>, default: [] },
    selectionValues: { type: Array<string>, default: [] }
};

export type TransferProps = ExtractPropTypes<typeof transferProps>;
