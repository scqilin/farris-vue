/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { Radio } from './composition/types';
import { EditorConfig } from '../../dynamic-form/src/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import radioGroupSchema from './schema/radio-group.schema.json';

export const radioGroupProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 禁用组件，不允许切换单选值
     */
    disabled: { type: Boolean, default: false },
    /**
     * 单选组枚举数组
     */
    enumData: Array<Radio>,
    /**
     * 组件是否水平排列
     */
    horizontal: { type: Boolean, default: false },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },
    /**
     * 组件名称
     */
    name: { type: String, default: '' },
    /**
     * 输入框Tab键索引
     */
    tabIndex: Number,
    /**
     * 枚举数组中展示文本的key值。
     */
    textField: { type: String, default: 'name' },
    /**
     * 枚举数组中枚举值的key值。
     */
    valueField: { type: String, default: 'value' }
} as Record<string, any>;

export type RadioGroupProps = ExtractPropTypes<typeof radioGroupProps>;

export const propsResolver = createPropsResolver<RadioGroupProps>(radioGroupProps, radioGroupSchema, schemaMapper);
