/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref } from 'vue';
import type { SetupContext } from 'vue';
import { radioGroupProps, RadioGroupProps } from './radio-group.props';
import { changeRadio } from './composition/change-radio';

export default defineComponent({
    name: 'FRadioGroup',
    props: radioGroupProps,
    emits: ['changeValue', 'update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: RadioGroupProps, context: SetupContext) {
        const modelValue = ref(props.modelValue);
        const { enumData, onClickRadio, getValue, getText } = changeRadio(props, context, modelValue);
        const horizontalClass = computed(() => ({
            'farris-checkradio-hor': props.horizontal
        }));

        return () => {
            return (
                <div class={['farris-input-wrap', horizontalClass.value]}>
                    {enumData.value.map((item, index) => {
                        const id = 'radio_' + props.name + index;

                        return (
                            <div class="custom-control custom-radio">
                                <input
                                    type="radio"
                                    class="custom-control-input"
                                    name={props.name}
                                    id={id}
                                    value={getValue(item)}
                                    checked={getValue(item) === modelValue.value}
                                    disabled={props.disabled}
                                    tabindex={props.tabIndex}
                                    onClick={(event: MouseEvent) => onClickRadio(item, event)}
                                />
                                <label class="custom-control-label" for={id}>
                                    {getText(item)}
                                </label>
                            </div>
                        );
                    })}
                </div>
            );
        };
    }
});
