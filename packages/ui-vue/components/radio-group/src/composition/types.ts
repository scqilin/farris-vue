/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, Ref } from 'vue';

export interface Radio {
    [key: string]: any;
    /**
     * 枚举值
     */
    value: ComputedRef<any>;
    /**
     * 枚举展示文本
     */
    name: ComputedRef<any>;
}

export interface ChangeRadio {
    enumData: ComputedRef<Array<Radio>>;

    /**
     * 获取枚举值
     */
    getValue(item: Radio): any;
    /**
     * 获取枚举文本
     */
    getText(item: Radio): any;

    /**
     * 切换单选按钮事件
     */
    onClickRadio: (item: Radio, $event: Event) => void;
}
