/* eslint-disable max-len */
/* eslint-disable prefer-const */
/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, SetupContext } from 'vue';
import { discussionListProps, DiscussionListProps } from './discussion-list.props';

import './discussion-list.scss';

export default defineComponent({
    name: 'FDiscussionList',
    props: discussionListProps,
    emits: ['replyMessage', 'page', 'pageSize', 'filePreview', 'fileDownload'] as (string[] & ThisType<void>) | undefined,
    setup(props: DiscussionListProps, context: SetupContext) {
        const personnelsDisplayKey = ref(props.personnelsDisplayKey);
        const pagerOnServer = ref(props.pagerOnServer);
        /** 是否支持分页 */
        const supportPaging = ref(props.supportPaging);
        /** 当前页码 */
        const _pageIndex = ref(props.pageIndex);
        /** 总条数 */
        const _total = ref(props.total);
        /** 每页显示个数 */
        const _pageSize = ref(props.pageSize);
        /** 评论数据 */
        const _discussionData = ref(props.discussionData);
        // const labels: any = {
        //     previousLabel: ' ',
        //     nextLabel: ' '
        // };
        // const directionLinks = true;
        // const maxSize = 7;
        // const responsive = true;
        // const autoHide = false;
        // const pager = ref<HTMLElement | null>(null);
        let paginationOptions: any;
        let innerDiscussionData: any = [];

        const total = computed({
            get() {
                return _total.value;
            },
            set(val) {
                _total.value = val;
                initPaginationOptions();
            }
        });

        const pageSize = computed({
            get() {
                return _pageSize.value;
            },
            set(val) {
                _pageSize.value = val;
                initPaginationOptions();
            }
        });
        onMounted(() => {
            initPaginationOptions();
        });

        function initPaginationOptions() {
            paginationOptions = {
                id: 'Farris-discussion-Pagination',
                itemsPerPage: _pageSize.value,
                currentPage: _pageIndex.value,
                pageList: [10, 20, 30, 50, 100],
                totalItems: _total,
                remote: pagerOnServer
            };
        }
        /** 点击回复留言 */
        function reply(item: any) {
            context.emit('replyMessage', item);
        }
        /** 页码变化 */
        // function onPageChange(page: { pageIndex: number; pageSize: number }) {
        //     if (_pageIndex.value !== page.pageIndex) {
        //         _pageIndex.value = page.pageIndex;
        //         paginationOptions.currentPage = page.pageIndex;
        //         context.emit('page', { pageInfo: page });
        //     }
        // }
        /** 每页显示条数变化 */
        // function onPageSizeChange(pageSize1: number) {
        //     if (pageSize.value !== pageSize1 && total) {
        //         paginationOptions.itemsPerPage = pageSize;
        //         pageSize.value = pageSize1;

        //         const _total = total;
        //         let pageLength = Math.floor(_total.value / pageSize1);
        //         if (_total.value % pageSize1 > 0) {
        //             pageLength += 1;
        //         }

        //         if (pageLength && _pageIndex.value > pageLength) {
        //             _pageIndex.value = pageLength;
        //             paginationOptions.currentPage = _pageIndex.value;
        //         }

        //         context.emit('pageSize', { pageInfo: { _pageIndex, pageSize } });
        //     }
        // }
        /** 附件预览 */
        // function filePreviewEventHandler(info: any) {
        //     context.emit('filePreview', info);
        // }
        /** 附件下载 */
        // function fileDownloadEventHandler(info: any) {
        //     context.emit('fileDownload', info);
        // }
        /** 占位头像文字 */
        // function getAvatar(item: any) {
        //     if (item && item[personnelsDisplayKey.value]) {
        //         let str = item[personnelsDisplayKey.value];
        //         return str.substring(str.length - 2, str.length);
        //     }
        //     return '';
        // }
        /** 分页函数 */
        // function paginateArray(array: any, options: any) {
        //     const { page, pageSize } = options;
        //     const startIndex = (page - 1) * pageSize;
        //     const endIndex = startIndex + pageSize;
        //     return array.slice(startIndex, endIndex);
        // }

        return () => {
            return (
                <div>
                    <div class="f-discussion-group-content">
                        {// *ngFor="let item of (supportPaging ? (innerDiscussionData | paginate: paginationOptions) : innerDiscussionData)"
                            innerDiscussionData.map((item: any) => {
                                return (
                                    <div class="f-discussion-group-content-item">
                                        <div class="discussion-item-avatar">
                                            <div>
                                                {item.imgData && (
                                                    <div>
                                                        <img src={item.imgData} alt="" class="discussion-item-avatar-img"></img>
                                                    </div>
                                                )}
                                            </div>
                                            <div>
                                                {!item.imgData && (
                                                    <div class="discussion-item-avatar-tip"
                                                    // innerHTML={(item: any) => { getAvatar(item) }}
                                                    ></div>
                                                )}
                                            </div>
                                        </div>
                                        <div class="discussion-item-inner">
                                            <div class="discussion-item-username">
                                                {item[personnelsDisplayKey.value]}
                                            </div>
                                            <div class="discussion-item-text">
                                                <span class="discussion-item-text-message"
                                                    innerHTML={item.text}
                                                ></span>
                                            </div>
                                            {item.parentData && (
                                                <div class="discussion-item-text-reply" >
                                                    <span class="discussion-item-text-reply-title">
                                                        {'discussionGroup.reply'}
                                                        <span class="discussion-item-text-reply-name">{item.parentData[personnelsDisplayKey.value]}</span>
                                                    </span>
                                                    <span class="discussion-item-text-reply-content"
                                                        innerHTML={item.parentData.text}
                                                    >
                                                    </span >
                                                </div >
                                            )}
                                            <div class="discussion-item-footer">
                                                <div class="discussion-item-time">
                                                    <span class="discussion-item-time-text">
                                                        {item.commentDate}
                                                        {/* | date:'yyyy-MM-dd HH:mm' */}
                                                    </span>
                                                </div>
                                                <div class="discussion-item-btns">
                                                    <span class="discussion-item-btns-start" onClick={(item: any) => { reply(item); }}>
                                                        <span class="f-icon f-icon-message"></span>
                                                        <span class="discussion-item-btns-start-text">
                                                            {'discussionGroup.reply'}
                                                        </span>
                                                    </span>
                                                </div>
                                            </div >
                                        </div >
                                    </div >
                                );
                            })
                        }

                        {supportPaging.value && (
                            <div class="f-discussion-group-footer" >
                                {/* <pagination-controls #pager
[id] = "paginationOptions.id"
        [maxSize] = "maxSize"
        [directionLinks] = "directionLinks"
        [autoHide] = "autoHide"
        [responsive] = "responsive"
        [previousLabel] = "labels.previousLabel"
        [nextLabel] = "labels.nextLabel"
        (pageChange) = "onPageChange($event)"
        (pageSizeChange) = "onPageSizeChange($event)"
        [showPageList] = "false"
>
    </pagination - controls > */}
                            </div >
                        )}

                    </div >
                </div >
            );
        };
    }
});
