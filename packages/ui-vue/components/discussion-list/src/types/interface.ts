export interface attachFile {
    id: string;
    commentId: string;
    name: string;
    size: number;
    sortOrder: number;
    metadataId: string;
    rootId: string;
    createdBy: string;
    createdOn: Date;
}
export interface discussionItem {
    id: string;
    userId: string;
    userName: string;
    imgData: string;
    commentDate: Date;
    text: string;
    attachFiles: attachFile[];
}
export interface discussionConfig extends discussionItem {
    parentData: discussionItem;
}
