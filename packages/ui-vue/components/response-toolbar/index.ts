import type { App } from 'vue';
import ResponseToolbar from './src/response-toolbar.component';
import { ResponseToolbarDropDownItem } from './src/types/response-toolbar-dropdwon-item';
import { ResponseToolbarGroup } from './src/types/response-toolbar-group';
import { ResponseToolbarItem } from './src/types/response-toolbar-item';

export { ResponseToolbar, ResponseToolbarDropDownItem, ResponseToolbarGroup, ResponseToolbarItem };

export default {
    install(app: App): void {
        app.component(ResponseToolbar.name, ResponseToolbar);
    }
};
