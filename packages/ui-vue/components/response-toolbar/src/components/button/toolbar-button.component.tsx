import { ref } from 'vue';
import { UseIcon } from '../../composition/types';
import { ResponseToolbarProps } from '../../response-toolbar.props';
import { ResponseToolbarItem } from '../../types/response-toolbar-item';

export default function (props: ResponseToolbarProps, useIconComposition: UseIcon) {
    const alignment = ref(props.alignment);

    function buttonClass(item: ResponseToolbarItem) {
        const classObject = {
            btn: true,
            'f-rt-btn': true,
            'f-btn-ml': alignment.value === 'right',
            'f-btn-mr': alignment.value === 'left',
            'btn-icontext': !!(item.icon && item.icon.trim())
        } as Record<string, boolean>;
        if (item.class) {
            const classNames = item.class.split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    }

    function renderToolbarButton(item: ResponseToolbarItem) {
        return (
            <button type="button" class={buttonClass(item)} id={item.id}>
                {useIconComposition.shouldShowIcon(item) && <i class={useIconComposition.iconClass(item)}></i>}
                {item.text}
            </button>
        );
    }

    return { renderToolbarButton };
}
