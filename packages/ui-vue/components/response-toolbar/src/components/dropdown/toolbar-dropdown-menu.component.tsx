import { ResponseToolbarDropDownItem } from '../../types/response-toolbar-dropdwon-item';

export default function () {
    function dropdownMenuClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            'dropdown-menu': true
        } as Record<string, boolean>;
        if (item.class) {
            const classNames = item.menuClass.split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    }

    function dropdownSubMenuItemClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            disabled: !item.enable,
            'dropdown-submenu': true,
            'f-rt-dropdown': true
        } as Record<string, boolean>;
        const classNames = item.dropdownClass.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    function dropdownMenuItemClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            disabled: !item.enable,
            'dropdown-item': true,
            'f-rt-btn': true
        } as Record<string, boolean>;
        const classNames = item.class.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    let renderDropdownMenu: (item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) => any;
    const expandedDropDownItems = new Map<string, ResponseToolbarDropDownItem>();

    function toggleDropdownSubMenu($event: MouseEvent, item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) {
        if (($event.target as any)?.id === item.id) {
            $event.stopPropagation();
        }
        const parentKey = parent ? parent.id : '__top_item__';
        if (item.children && item.children.length) {
            item.expanded = !item.expanded;
            item.expanded ? expandedDropDownItems.set(parentKey, item) : expandedDropDownItems.delete(parentKey);
        }
        if (expandedDropDownItems.has(parentKey) && expandedDropDownItems.get(parentKey) !== item) {
            const dropDwonItemToCollapse = expandedDropDownItems.get(parentKey);
            if (dropDwonItemToCollapse) {
                dropDwonItemToCollapse.expanded = false;
            }
        }
    }

    function renderDropdownMenuItem(item: ResponseToolbarDropDownItem) {
        return (
            item.children
                // .filter((item) => item.visible)
                .map((menuItem) => {
                    if (menuItem.children && menuItem.children.length) {
                        return (
                            <li
                                class={dropdownSubMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                                id={menuItem.id}
                                onClick={(payload: MouseEvent) =>
                                    menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                                }>
                                <span
                                    id={menuItem.id}
                                    class={dropdownMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                                    onMouseover={(payload: MouseEvent) =>
                                        menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                                    }>
                                    {menuItem.text}
                                    <i
                                        class="f-icon f-icon-arrow-chevron-right"
                                        style="display: inline-block;float: right;line-height: 1.25rem;"></i>
                                </span>
                                {renderDropdownMenu(menuItem as ResponseToolbarDropDownItem, item)}
                            </li>
                        );
                    }
                    return (
                        <li
                            class={dropdownMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                            id={menuItem.id}
                            onClick={(payload: MouseEvent) => menuItem.enable && menuItem.onClick(payload, menuItem.id)}
                            onMouseover={(payload: MouseEvent) =>
                                menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                            }>
                            {menuItem.text}
                        </li>
                    );
                })
        );
    }

    function dropdownStyle(item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) {
        const styleObject: Record<string, any> = {
            display: item.expanded ? 'block' : 'none'
        };
        const screenWidth = document.getElementsByTagName('body')[0].getClientRects()[0].width;
        const currentDropdownElement = document.getElementById(item.id);
        const currentDropdownElementClientRect = currentDropdownElement?.getClientRects();
        if (currentDropdownElement && currentDropdownElementClientRect && currentDropdownElementClientRect.length) {
            const currentDropdownHorizontalPosition = currentDropdownElementClientRect[0].x;
            if (screenWidth - currentDropdownHorizontalPosition <= 160) {
                styleObject.right = 0;
                styleObject.left = 'unset';
            }
        }
        if (parent) {
            styleObject.top = 0;
            styleObject.right = '160px';
            styleObject.left = 'unset';
        }
        return styleObject;
    }

    renderDropdownMenu = function (item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) {
        return (
            <ul class={dropdownMenuClass(item)} style={dropdownStyle(item, parent)} id={item.id + '_menu'}>
                {renderDropdownMenuItem(item)}
            </ul>
        );
    };

    function clearAllDropDownMenu() {
        expandedDropDownItems.forEach((dropdownItem: ResponseToolbarDropDownItem) => {
            dropdownItem.expanded = false;
        });
        expandedDropDownItems.clear();
    }

    return { renderDropdownMenu, clearAllDropDownMenu };
}
