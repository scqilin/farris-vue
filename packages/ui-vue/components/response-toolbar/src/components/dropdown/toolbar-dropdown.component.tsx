import { ref } from 'vue';
import { UseIcon } from '../../composition/types';
import { ResponseToolbarProps } from '../../response-toolbar.props';
import { ResponseToolbarDropDownItem } from '../../types/response-toolbar-dropdwon-item';
import getDropdownMenu from './toolbar-dropdown-menu.component';

export default function (props: ResponseToolbarProps, useIconComposition: UseIcon) {
    const alignment = ref(props.alignment);
    const { renderDropdownMenu, clearAllDropDownMenu } = getDropdownMenu();

    function dropdownClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            'btn-group': true,
            'f-rt-dropdown': true,
            'f-btn-ml': alignment.value === 'right',
            'f-btn-mr': alignment.value === 'left'
        } as Record<string, boolean>;
        const classNames = item.dropdownClass.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    function dropdownButtonClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            btn: true,
            disabled: !item.enable,
            'f-rt-btn': true,
            'btn-icontext': !!(item.icon && item.icon.trim())
        } as Record<string, boolean>;
        if (item.class) {
            const classNames = item.class.split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    }

    function toggleDropdownMenu($event: MouseEvent, item: ResponseToolbarDropDownItem) {
        $event.stopPropagation();
        item.expanded = !item.expanded;
    }

    function renderToolbarDropdown(item: ResponseToolbarDropDownItem) {
        return (
            <div id={item.id} class={dropdownClass(item)}>
                <div
                    class={dropdownButtonClass(item)}
                    style="display: flex;padding-right: 0.1rem;"
                    onClick={(payload: MouseEvent) => item.enable && toggleDropdownMenu(payload, item)}>
                    {useIconComposition.shouldShowIcon(item) && <i class={useIconComposition.iconClass(item)}></i>}
                    <span>{item.text}</span>
                    <i
                        class="f-icon f-icon-arrow-chevron-down"
                        style="display: inline-block;float: right;line-height: 1.25rem;margin-left: .25rem;margin-right: .25rem;"></i>
                </div>
                {renderDropdownMenu(item)}
            </div>
        );
    }

    function clearAllDropDown() {
        clearAllDropDownMenu();
    }

    return { renderToolbarDropdown, clearAllDropDown };
}
