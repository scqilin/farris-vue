import { ExtractPropTypes, PropType } from 'vue';

export type ButtonAlignment = 'right' | 'left';

export const responseToolbarProps = {
    alignment: { Type: String as PropType<ButtonAlignment>, default: 'right' },
    items: { Type: Array<any>, default: [] }
};

export type ResponseToolbarProps = ExtractPropTypes<typeof responseToolbarProps>;
