export class ResponseToolbarGroup {
    id: string;

    name: string;

    /** 记录元素的Id */
    presetId: Array<string> = [];

    /** 记录转变为下拉时元素的位置 */
    responsedIndex: Array<number> = [];

    /** 记录宽度 */
    width: number;

    constructor(_id: string, _groupName: string) {
        this.id = _id;
        this.name = _groupName;
        this.width = 0;
    }

    // 设置宽度
    setWidth(value: any) {
        this.width = parseInt(value + '', 10);
    }

    // 获取宽度
    getWidth(): any {
        return this.width;
    }

    // 更新presetIndex
    setPreset(value: Array<string> | string) {
        if (Array.isArray(value)) {
            this.presetId = this.presetId.concat(value);
        } else {
            this.presetId.push(value);
        }
    }

    // 清除preset
    delPreset() {
        this.presetId = [];
    }

    // 删除
    removeResponsed(index: number) {
        this.responsedIndex.splice(index, 1);
    }

    // 更新responsedIndex
    setResponsed(value: Array<number> | number) {
        if (Array.isArray(value)) {
            this.responsedIndex = this.responsedIndex.concat(value);
        } else {
            this.responsedIndex.push(value);
        }
    }

    // 是否已经开始处理响应式
    isResponsing() {
        return this.responsedIndex.length > 0;
    }

    // 是否已经处理完响应式
    isResponsed() {
        return this.presetId.length === this.responsedIndex.length;
    }
}
