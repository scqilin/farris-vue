export interface ResponseToolbarClickEvent {
    id: string;
    text: string;
    hidden: boolean;
}
