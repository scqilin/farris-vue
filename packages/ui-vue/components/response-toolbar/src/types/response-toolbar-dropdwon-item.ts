import { useToolbarItem } from '../composition/use-toolbar-item';
import { ResponseToolbarItem } from './response-toolbar-item';
import { ResponseToolbarItemBase } from './response-toolbar-item-base';

const { buildResponseToolbarItems } = useToolbarItem();

export class ResponseToolbarDropDownItem extends ResponseToolbarItemBase {
    placement = ''; // 下拉位置

    /** 下拉class */
    dropdownClass = '';

    /** 下拉菜单class */
    menuClass = '';

    /** 是否用分开的下拉按钮 */
    split = false;

    children: Array<ResponseToolbarItemBase> = [];

    expanded = false;

    constructor(item: any) {
        super(item);
        const propertyKeys = ['isDP', 'class', 'dropdownClass', 'menuClass', 'placement', 'split', 'expanded'];
        Object.keys(item)
            .filter((itemKey: string) => propertyKeys.indexOf(itemKey) > -1)
            .forEach((itemKey: string) => {
                this[itemKey] = item[itemKey];
            });
        if (item.children && item.children.length) {
            this.children = buildResponseToolbarItems(item.children);
        }
    }
}
