import { ResponseToolbarDropDownItem } from '../types/response-toolbar-dropdwon-item';
import { ResponseToolbarItem } from '../types/response-toolbar-item';
import { UseIcon } from './types';

export function useIcon(): UseIcon {
    function iconClass(item: ResponseToolbarItem | ResponseToolbarDropDownItem) {
        const classObject = {
            'f-icon': true
        } as Record<string, boolean>;
        if (item.icon) {
            const classNames = item.icon.trim().split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    }

    function shouldShowIcon(item: ResponseToolbarItem | ResponseToolbarDropDownItem) {
        return !!(item.icon && item.icon.trim());
    }

    return { iconClass, shouldShowIcon };
}
