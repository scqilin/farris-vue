import { ResponseToolbarDropDownItem } from '../types/response-toolbar-dropdwon-item';
import { ResponseToolbarItem } from '../types/response-toolbar-item';
import { UseToolbarItem } from './types';

export function useToolbarItem(): UseToolbarItem {

    function buildResponseToolbarItems(items: any[]): (ResponseToolbarItem | ResponseToolbarDropDownItem)[] {
        const toolbarItems: (ResponseToolbarItem | ResponseToolbarDropDownItem)[] = [];
        items.reduce<(ResponseToolbarItem | ResponseToolbarDropDownItem)[]>((items, itemOption) => {
            if (itemOption.children && itemOption.children.length > 0) {
                items.push(new ResponseToolbarDropDownItem(itemOption));
            } else {
                items.push(new ResponseToolbarItem(itemOption));
            }
            return items;
        }, toolbarItems);
        return toolbarItems;
    }

    return { buildResponseToolbarItems };
}
