/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { ImageTransform, CropperPosition } from './composition/type';

type FormatValue = 'png' | 'jpeg' | 'bmp' | 'webp' | 'ico';
type AlignImageValue = 'left' | 'center';

export const imageCropperProps = {
    /** 输出图片格式 */
    format: { type: String as PropType<FormatValue>, default: 'png' },

    /** 保持长宽比例 */
    maintainAspectRatio: { type: Boolean, default: true },

    /** scale缩放 rotate旋转 flipH横向反转 flipV垂直翻转 执行变化 */
    transform: { type: Object as PropType<ImageTransform>, default: { scale: 0, rotate: 0, flipH: false, flipV: false } },

    /** 宽高比例 width/height */
    aspectRatio: { type: Number, default: 1 },

    /** 裁剪后图片被调整为的宽度 */
    resizeToWidth: { type: Number, default: 0 },

    /** 裁剪后图片被调整为的高度 */
    resizeToHeight: { type: Number, default: 0 },

    /** 裁剪最小宽度 */
    cropperMinWidth: { type: Number, default: 0 },

    /** 裁剪最小高度 */
    cropperMinHeight: { type: Number, default: 0 },

    /** 旋转画布 1 = 90deg  */
    canvasRotation: { type: Number, default: 0 },

    /** 裁剪形状是否为圆形 */
    roundCropper: { type: Boolean, default: true },

    /** 启用这个选项将确保较小的图像不会被放大 */
    onlyScaleDown: { type: Boolean, default: false },

    /** 适用于使用jpeg或webp作为输出格式时。输入0到100之间的数字将决定输出图像的质量 */
    imageQuality: { type: Number, default: 92 },

    /** 是否每次修改cropper的位置或大小时，cropper都会发出一个图像 */
    autoCrop: { type: Boolean, default: true },

    /** 背景色 */
    backgroundColor: { type: String, default: '' },

    /** 是否在图像周围添加填充以使其适合长宽比 */
    containWithinAspectRatio: { type: Boolean, default: false },

    /** 隐藏 */
    hideResizeSquares: { type: Boolean, default: false },

    /** 图片加载错误的提示 */
    loadImageErrorText: { type: String, default: '图片加载错误' },

    /** 图片居中 */
    alignImage: { type: String as PropType<AlignImageValue>, default: 'center' },

    /** 是否禁用 */
    disabled: { type: Boolean, default: false },

    /** 图片路径 */
    imageURL: { type: String, default: '' },

    /** 图片事件变化 */
    imageChangedEvent: { type: Object, default: null },

    /** imageBase64 */
    imageBase64: { type: String, default: '' },

    /** imageFile */
    imageFile: { type: Object, default: null },

    /** 裁剪的坐标 */
    cropper: {
        type: Object as PropType<CropperPosition>, default: {
            x1: -100,
            y1: -100,
            x2: 10000,
            y2: 10000
        }
    },

};

export type ImageCropperProps = ExtractPropTypes<typeof imageCropperProps>;
