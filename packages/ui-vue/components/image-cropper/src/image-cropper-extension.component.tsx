// /* eslint-disable max-len */
// /* eslint-disable @typescript-eslint/indent */
// /* eslint-disable complexity */
// /* eslint-disable no-case-declarations */
// /* eslint-disable no-use-before-define */
// /**
//  * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
//  *
//  * Licensed under the Apache License, Version 2.0 (the "License");
//  * you may not use this file except in compliance with the License.
//  * You may obtain a copy of the License at
//  *
//  *       http://www.apache.org/licenses/LICENSE-2.0
//  *
//  * Unless required by applicable law or agreed to in writing, software
//  * distributed under the License is distributed on an "AS IS" BASIS,
//  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  * See the License for the specific language governing permissions and
//  * limitations under the License.
//  * defination
//  */
// import { defineComponent, ref, SetupContext } from 'vue';
// import { ImageCropperProps, imageCropperProps } from './image-cropper.props';
// import FImageCropper from './image-cropper.component';

// import './image-cropper.scss';

// export default defineComponent({
//     name: 'FImageCropperExtension',
//     props: imageCropperProps,
//     emits: ['imageCropped'] as (string[] & ThisType<void>) | undefined,
//     setup(props: ImageCropperProps, context: SetupContext) {
//         let canvasRotation = 0;
//         let scale = 100;
//         let transform = { scale: 0, rotate: 0, flipH: false, flipV: false };
//         const imageChangedEvent: any = ref(null);
//         const croppedImage = ref('');
//         const currentImgType = ['image/png', 'image/gif', 'image/jpg', 'image/jpeg'];
//         const fileInput = ref<HTMLInputElement | null>(null);

//         function upload() {
//             fileInput.value?.click();
//         }

//         function zoomOut() {
//             if (scale <= 5) return;
//             scale -= 5;
//             transform = {
//                 ...transform,
//                 scale: scale / 100
//             };
//         }

//         function zoomIn() {
//             scale += 5;
//             transform = {
//                 ...transform,
//                 scale: scale / 100
//             };
//         }

//         function fileChangeEvent(event: any): void {
//             if (!event.target.files) {
//                 return;
//             }
//             const fileType = event.target.files[0].type;
//             const size = event.target.files[0].size / 1024 / 1024;
//             if (currentImgType.indexOf(fileType) < 0) {
//                 // NotifyService.error('图片格式不对');
//             } else if (size > 2) {
//                 // NotifyService.error('文件过大');
//             } else {
//                 imageChangedEvent.value = event;
//             }
//         }

//         function rotateLeft() {
//             canvasRotation--;
//             flipAfterRotate();
//         }

//         function rotateRight() {
//             canvasRotation++;
//             flipAfterRotate();
//         }

//         function flipAfterRotate() {
//             const flippedH = transform.flipH;
//             const flippedV = transform.flipV;
//             transform = {
//                 ...transform,
//                 flipH: flippedV,
//                 flipV: flippedH
//             };
//         }
//         /** 裁切后的图片 */
//         const imageCropped = (event: any) => {
//             croppedImage.value = event.base64;
//             // 处理从 FImageCropper 组件传递的 croppedImage
//             console.log('Cropped image:', croppedImage.value);
//         };
//         // <!-- (imageLoaded)="imageLoaded()"
//         // (cropperReady)="cropperReady($event)"
//         // (loadImageFailed)="loadImageFailed()"  -->

//         return () => {
//             return (
//                 <div>
//                     <div class="dialog-user-avatar" ref="file">
//                         <div class="dialog-user-avatar-change">
//                             <div class="avatar-cropper">
//                                 <div class="avatar-cropper-wrapper">
//                                     <FImageCropper
//                                     // onImageCropped={imageCropped}
//                                     ></FImageCropper>
//                                 </div>
//                             </div>
//                             <div class="avatar-cropper-btns">
//                                 <div class="avatar-cropper-btns-item" onClick={rotateLeft}>
//                                     <span class="f-icon f-icon-reset-sm"></span>
//                                     <span>逆时针</span>
//                                 </div>
//                                 <div class="avatar-cropper-btns-item">
//                                     <span class="f-icon f-icon-plus avatar-cropper-scale" onClick={zoomIn}></span>
//                                     <span class="avatar-cropper-btns-item-scale">{{ scale }}%</span>
//                                     <span class="f-icon f-icon-minus avatar-cropper-scale" onClick={zoomOut}></span>
//                                 </div>
//                                 <div class="avatar-cropper-btns-item" onClick={rotateRight}>
//                                     <span class="f-icon f-icon-refresh-sm"></span>
//                                     <span>顺时针</span>
//                                 </div>
//                             </div>
//                         </div>
//                         <div class="dialog-user-avatar-view">
//                             <div class="avatar-view-title">头像预览</div>
//                             <div class="avatar-view-size">
//                                 <img src="croppedImage" alt="" class="img" />
//                                 <span class="avatar-view-size-text">大尺寸100*100</span>
//                             </div>
//                             <div class="avatar-view-size avatar-view-size-small">
//                                 <img src="croppedImage" alt="" class="img" />
//                                 <span class="avatar-view-size-text">小尺寸50*50</span>
//                             </div>
//                         </div>
//                     </div>
//                     <div class="dialog-user-avatar-tips">
//                         <div class="dialog-user-avatar-upload">
//                             <input
//                                 type="file"
//                                 class="user-avatar-upload"
//                                 accept="'image/png,image/gif,image/jpg,image/jpeg'"
//                                 style="display: none"
//                                 onChange={($event) => fileChangeEvent($event)}
//                                 ref="fileInput"
//                             />
//                             <button class="btn btn-primary" onClick={upload}>
//                                 <span class="f-icon f-icon-add"></span>
//                                 上传头像
//                             </button>
//                         </div>
//                         <div class="dialog-user-avatar-tip-content">提示：支持JPG/JPEG/PNG/GIF格式，文件小于2M</div>
//                     </div>
//                 </div>
//             );
//         };
//     }
// });
