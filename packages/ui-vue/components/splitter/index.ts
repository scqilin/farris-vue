import type { App } from 'vue';
import Splitter from './src/splitter.component';
import SplitterPane from './src/components/splitter-pane.component';
import SplitterDesign from './src/designer/splitter.design.component';
import SplitterPaneDesign from './src/designer/splitter-pane.design.component';
import { splitterPropsResolver } from './src/splitter.props';
import { splitterPanePropsResolver } from './src/components/splitter-pane.props';

export * from './src/splitter.props';
export { Splitter };

export default {
    install(app: App): void {
        app.component(Splitter.name, Splitter);
        app.component(SplitterPane.name, SplitterPane);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap.splitter = Splitter;
        propsResolverMap.splitter = splitterPropsResolver;
        componentMap['splitter-pane'] = SplitterPane;
        propsResolverMap['splitter-pane'] = splitterPanePropsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap.splitter = SplitterDesign;
        propsResolverMap.splitter = splitterPropsResolver;
        componentMap['splitter-pane'] = SplitterPaneDesign;
        propsResolverMap['splitter-pane'] = splitterPanePropsResolver;
    }
};
