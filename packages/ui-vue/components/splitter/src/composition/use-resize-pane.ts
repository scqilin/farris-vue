/* eslint-disable @typescript-eslint/indent */
import { ref } from 'vue';
import { UseResizePane, UseResizeHandle } from './types';
import { SplitterPanePropsType } from '../components/splitter-pane.props';

export function useResizePane(
    props: SplitterPanePropsType,
    useResizeHandleComposition: UseResizeHandle
): UseResizePane {
    const actualWidth = ref<number>(props.width);
    const actualHeight = ref<number>(props.height);
    const {
        horizontalResizeBarPosition,
        horizontalResizeHandleOffset,
        showHorizontalResizeHandle,
        showVerticalResizeHandle,
        verticalResizeBarPosition,
        verticalResizeHandleOffset,
        draggingHorizontalResizeHandle,
        draggingVerticalResizeHandle
    } = useResizeHandleComposition;

    let splitterPane = '';
    let splitterPaneElement: any;
    let splitterElement: any;

    function releaseMouseMove($event: MouseEvent) {

        if ((splitterPane === 'left' || splitterPane === 'right') && splitterElement) {
            const { left: splitterOffsetLeft } = ((splitterElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const { width: splitterPaneWidth } = ((splitterPaneElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const mouseReleasePosition = $event.clientX - splitterOffsetLeft;
            if (splitterPane === 'left') {
                actualWidth.value = (splitterPaneWidth || 0) + (mouseReleasePosition - horizontalResizeBarPosition.value);
            } else {
                actualWidth.value = (splitterPaneWidth || 0) - (mouseReleasePosition - horizontalResizeBarPosition.value);
            }
        }
        if ((splitterPane === 'top' || splitterPane === 'bottom') && splitterElement) {
            const { top: splitterOffsetTop } = ((splitterElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const { height: splitterPaneHeight } = ((splitterPaneElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const mouseReleasePosition = $event.clientY - splitterOffsetTop;
            if (splitterPane === 'top') {
                actualHeight.value = (splitterPaneHeight || 0) + (mouseReleasePosition - verticalResizeBarPosition.value);
            } else {
                actualHeight.value = (splitterPaneHeight || 0) - (mouseReleasePosition - verticalResizeBarPosition.value);
            }
        }
        horizontalResizeHandleOffset.value = 0;
        verticalResizeHandleOffset.value = 0;
        horizontalResizeBarPosition.value = -1;
        verticalResizeBarPosition.value = -1;
        showHorizontalResizeHandle.value = false;
        showVerticalResizeHandle.value = false;
        document.removeEventListener('mousemove', draggingHorizontalResizeHandle);
        document.removeEventListener('mouseup', releaseMouseMove);
        document.body.style.userSelect = '';
        splitterPane = '';
        splitterPaneElement = null;
        splitterElement = null;
    }

    function onClickSplitterHorizontalResizeBar($event: MouseEvent, splitterPaneName: string, element: HTMLElement) {
        splitterPane = splitterPaneName;
        splitterPaneElement = element;
        showHorizontalResizeHandle.value = true;
        const clickElementPath = $event.composedPath();
        splitterElement = clickElementPath.find((element: any) => element.className.split(' ')[0] === 'f-splitter');
        if (splitterElement) {
            const { left: splitterOffsetLeft } = ((splitterElement as HTMLElement).getBoundingClientRect() as DOMRect);
            horizontalResizeHandleOffset.value = $event.clientX - splitterOffsetLeft;
            horizontalResizeBarPosition.value = $event.clientX - splitterOffsetLeft;
            document.addEventListener('mousemove', draggingHorizontalResizeHandle);
            document.addEventListener('mouseup', releaseMouseMove);
            document.body.style.userSelect = 'none';
        }
    }

    function onClickSplitterVerticalResizeBar($event: MouseEvent, splitterPaneName: string, element: HTMLElement) {
        splitterPane = splitterPaneName;
        splitterPaneElement = element;
        showVerticalResizeHandle.value = true;
        const clickElementPath = $event.composedPath();
        splitterElement = clickElementPath.find((element: any) => element.className.split(' ')[0] === 'f-splitter');
        if (splitterElement) {
            const { top: splitterOffsetTop } = ((splitterElement as HTMLElement).getBoundingClientRect() as DOMRect);
            verticalResizeHandleOffset.value = $event.clientY - splitterOffsetTop;
            verticalResizeBarPosition.value = $event.clientY - splitterOffsetTop;
            document.addEventListener('mousemove', draggingVerticalResizeHandle);
            document.addEventListener('mouseup', releaseMouseMove);
            document.body.style.userSelect = 'none';
        }
    }

    return {
        actualWidth, actualHeight, onClickSplitterHorizontalResizeBar, onClickSplitterVerticalResizeBar
    };
}
