import { SetupContext, computed, defineComponent, provide, ref } from 'vue';
import { SplitterPropsType, splitterProps } from './splitter.props';
import './splitter.css';
import { useResizeHandle } from './composition/use-resize-handle';
import { SplitterContext } from './composition/types';

export default defineComponent({
    name: 'FSplitter',
    props: splitterProps,
    emits: [],
    setup(props: SplitterPropsType, context: SetupContext) {
        const splitterElementRef = ref<HTMLElement>();
        const useResizeHandleComposition = useResizeHandle(splitterElementRef);
        const { horizontalResizeHandleStyle, verticalResizeHandleStyle, resizeOverlayStyle } = useResizeHandleComposition;
        provide<SplitterContext>('splitter', { useResizeHandleComposition });

        const splitterStyle = computed(() => {
            const styleObject = {
                'flex-direction': props.direction === 'row' ? 'column' : 'row'
            } as Record<string, any>;
            return styleObject;
        });

        return () => {
            return (
                <div class="f-splitter" ref={splitterElementRef} style={splitterStyle.value}>
                    {context.slots.default && context.slots.default()}
                    <div class="f-splitter-resize-overlay" style={resizeOverlayStyle.value}></div>
                    <div class="f-splitter-horizontal-resize-proxy" style={horizontalResizeHandleStyle.value}></div>
                    <div class="f-splitter-vertical-resize-proxy" style={verticalResizeHandleStyle.value}></div>
                </div>
            );
        };
    }
});
