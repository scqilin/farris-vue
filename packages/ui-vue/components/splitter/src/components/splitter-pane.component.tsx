/* eslint-disable @typescript-eslint/indent */
import { SetupContext, computed, defineComponent, inject, onMounted, ref } from 'vue';
import { SplitterPanePosition, SplitterPanePropsType, splitterPaneProps } from './splitter-pane.props';
import { SplitterContext } from '../composition/types';
import { useResizePane } from '../composition/use-resize-pane';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerRules } from '../designer/splitter-use-designer-rules';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FSplitterPane',
    props: splitterPaneProps,
    emits: [],
    setup(props: SplitterPanePropsType, context: SetupContext) {
        const splitterPaneElementRef = ref<HTMLElement>();
        const splitterContext = inject<SplitterContext>('splitter');
        const { useResizeHandleComposition } = splitterContext as SplitterContext;
        const useResizePaneComposition = useResizePane(props, useResizeHandleComposition);
        const { actualHeight, actualWidth, onClickSplitterHorizontalResizeBar, onClickSplitterVerticalResizeBar } =
            useResizePaneComposition;
        const position = ref(props.position);
        const splitterPaneClass = computed(() => {
            const classObejct = {
                'f-splitter-pane': true,
                'f-splitter-pane-column': position.value === 'left' || position.value === 'right',
                'f-splitter-pane-row': position.value === 'bottom' || position.value === 'top',
                'f-splitter-pane-main': position.value === 'center'
            } as Record<string, boolean>;
            props.customClass &&
                String(props.customClass)
                    .split(' ')
                    .reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                        result[className] = true;
                        return result;
                    }, classObejct);

            return classObejct;
        });

        const splitterPaneStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if ((actualWidth.value && position.value === 'left') || position.value === 'right') {
                styleObject.width = `${actualWidth.value}px`;
            }
            if ((actualHeight.value && position.value === 'bottom') || position.value === 'top') {
                styleObject.height = `${actualHeight.value}px`;
            }
            return styleObject;
        });

        const splitterResizeBarClass = computed(() => {
            const classObejct = {
                'f-splitter-resize-bar': true,
                'f-splitter-resize-bar-e': position.value === 'left',
                'f-splitter-resize-bar-n': position.value === 'bottom',
                'f-splitter-resize-bar-s': position.value === 'top',
                'f-splitter-resize-bar-w': position.value === 'right'
            } as Record<string, boolean>;
            return classObejct;
        });

        function onClickResizeBar(payload: MouseEvent, position: SplitterPanePosition) {
            if (position === 'left' || position === 'right') {
                onClickSplitterHorizontalResizeBar(payload, position, splitterPaneElementRef.value as HTMLElement);
            }
            if (position === 'top' || position === 'bottom') {
                onClickSplitterVerticalResizeBar(payload, position, splitterPaneElementRef.value as HTMLElement);
            }
        }

        return () => {
            return (
                <div ref={splitterPaneElementRef} class={splitterPaneClass.value} style={splitterPaneStyle.value}>
                    {context.slots.default && context.slots.default()}
                    <span
                        class={splitterResizeBarClass.value}
                        onMousedown={(payload: MouseEvent) => onClickResizeBar(payload, position.value)}></span>
                </div>
            );
        };
    }
});
