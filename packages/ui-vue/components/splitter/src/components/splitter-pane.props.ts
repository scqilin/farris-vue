import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../../dynamic-resolver";
import { splitterPaneSchemaMapper } from '../schema/splitter-pane-schema-mapper';
import splitterPaneSchema from '../schema/splitter-pane.schema.json';

export type SplitterPanePosition = 'left' | 'center' | 'right' | 'top' | 'bottom';

export const splitterPaneProps = {
    customClass: { type: String, defaut: '' },
    /** 记录原始定义宽度 */
    width: { type: Number },
    /** 记录原始定义高度 */
    height: { type: Number },
    /** 面板位置 */
    position: { type: String as PropType<SplitterPanePosition>, default: 'left' },
    /** 是否显示 */
    visible: { type: Boolean, default: true },
    /** True to allow the pane can be resized. */
    resizable: { type: Boolean, default: true },
    /** True to allow the pane can be collapsed. */
    collapsable: { type: Boolean, default: false },
} as Record<string, any>;

export type SplitterPanePropsType = ExtractPropTypes<typeof splitterPaneProps>;

export const splitterPanePropsResolver = createPropsResolver<SplitterPanePropsType>(
    splitterPaneProps, splitterPaneSchema, splitterPaneSchemaMapper);
