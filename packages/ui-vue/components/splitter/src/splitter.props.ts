import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { splitterSchemaMapper } from "./schema/splitter-schema-mapper";
import splitterSchema from './schema/splitter.schema.json';

export type SpliteDirection = 'column' | 'row';

export const splitterProps = {
    direction: { Type: String as PropType<SpliteDirection>, default: 'column' }
} as Record<string, any>;

export type SplitterPropsType = ExtractPropTypes<typeof splitterProps>;

export const splitterPropsResolver = createPropsResolver<SplitterPropsType>(splitterProps, splitterSchema, splitterSchemaMapper);
