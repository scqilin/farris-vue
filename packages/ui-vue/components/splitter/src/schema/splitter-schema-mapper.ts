import { MapperFunction, resolveAppearance } from '../../../dynamic-resolver';

export const splitterSchemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance]
]);
