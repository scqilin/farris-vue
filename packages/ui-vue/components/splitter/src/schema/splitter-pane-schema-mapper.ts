import { MapperFunction, resolveAppearance } from '../../../dynamic-resolver';

export const splitterPaneSchemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance]
]);
