import { SetupContext, computed, defineComponent, inject, onMounted, provide, ref } from 'vue';
import { SplitterPropsType, splitterProps } from '../splitter.props';
import '../splitter.css';
import { useResizeHandle } from '../composition/use-resize-handle';
import { SplitterContext } from '../composition/types';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from './splitter-use-designer-rules';

export default defineComponent({
    name: 'FSplitterDesign',
    props: splitterProps,
    emits: [],
    setup(props: SplitterPropsType, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.canNested = false;
        const useResizeHandleComposition = useResizeHandle(elementRef);
        const { horizontalResizeHandleStyle, verticalResizeHandleStyle, resizeOverlayStyle } = useResizeHandleComposition;
        provide<SplitterContext>('splitter', { useResizeHandleComposition });

        const splitterStyle = computed(() => {
            const styleObject = {
                'flex-direction': props.direction === 'row' ? 'column' : 'row'
            } as Record<string, any>;
            return styleObject;
        });

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div class="f-splitter" ref={elementRef} style={splitterStyle.value}>
                    {context.slots.default && context.slots.default()}
                    <div class="f-splitter-resize-overlay" style={resizeOverlayStyle.value}></div>
                    <div class="f-splitter-horizontal-resize-proxy" style={horizontalResizeHandleStyle.value}></div>
                    <div class="f-splitter-vertical-resize-proxy" style={verticalResizeHandleStyle.value}></div>
                </div>
            );
        };
    }
});
