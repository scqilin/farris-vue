import { DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/use-dragula-common-rule";
import { ComponentSchema } from "../../../designer-canvas/src/types";

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext);
        if (!basalRule) {
            return false;
        }


        const paneCls = schema.appearance?.class || '';
        const paneClsList = paneCls.split(' ') || [];

        // 若SplitterPane是左侧区域（f-page-content-nav， 表单中一般放置单个列表或树列表），且已有子级节点，则不接收控件
        if (paneClsList.includes('f-page-content-nav')) {
            const firstChild = schema.contents && schema.contents[0];
            return firstChild && firstChild.type === 'component' ? false : true;
        }

        return true;
    }

    return { canAccepts };

}
