/* eslint-disable @typescript-eslint/indent */
import { SetupContext, computed, defineComponent, inject, onMounted, ref } from 'vue';
import { SplitterPanePropsType, splitterPaneProps } from '../components/splitter-pane.props';
import { SplitterContext } from '../composition/types';
import { useResizePane } from '../composition/use-resize-pane';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from './splitter-pane-use-designer-rules';

export default defineComponent({
    name: 'FSplitterPaneDesign',
    props: splitterPaneProps,
    emits: [],
    setup(props: SplitterPanePropsType, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.canNested = false;
        componentInstance.value.canMove = false;
        componentInstance.value.canDelete = false;

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const splitterContext = inject<SplitterContext>('splitter');
        const { useResizeHandleComposition } = splitterContext as SplitterContext;
        const useResizePaneComposition = useResizePane(props, useResizeHandleComposition);
        const { actualHeight, actualWidth } = useResizePaneComposition;
        const position = ref(props.position);
        const splitterPaneClass = computed(() => {
            const classObejct = {
                'f-splitter-pane': true,
                'f-splitter-pane-column': position.value === 'left' || position.value === 'right',
                'f-splitter-pane-row': position.value === 'bottom' || position.value === 'top',
                'f-splitter-pane-main': position.value === 'center',
                'f-component-splitter-pane': true,
                'drag-container': true
            } as Record<string, boolean>;
            props.customClass &&
                String(props.customClass)
                    .split(' ')
                    .reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                        result[className] = true;
                        return result;
                    }, classObejct);

            return classObejct;
        });

        const splitterPaneStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if ((actualWidth.value && position.value === 'left') || position.value === 'right') {
                styleObject.width = `${actualWidth.value}px`;
            }
            if ((actualHeight.value && position.value === 'bottom') || position.value === 'top') {
                styleObject.height = `${actualHeight.value}px`;
            }
            return styleObject;
        });

        return () => {
            return (
                <div
                    ref={elementRef}
                    dragref={`${designItemContext.schema.id}-container`}
                    class={splitterPaneClass.value}
                    style={splitterPaneStyle.value}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
