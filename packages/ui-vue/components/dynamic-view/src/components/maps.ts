import FAccordion from '../../../accordion';
import FButtonEdit from '../../../button-edit';
import FCheckBox from '../../../checkbox';
import FComboList from '../../../combo-list';
import FDatePicker from '../../../date-picker';
import FDataGrid from '../../../data-grid';
import FDynamicForm from '../../../dynamic-form';
import FInputGroup from '../../../input-group';
import FNumberRange from '../../../number-range';
import FNumberSpinner from '../../../number-spinner';
import FPageHeader from '../../../page-header';
import FQuerySolution from '../../../query-solution';
import FRadioGroup from '../../../radio-group';
import FResponseLayout from '../../../response-layout';
import FSearchBox from '../../../search-box';
import FSection from '../../../section';
import FSplitter from '../../../splitter';
import FSwitch from '../../../switch';
import FTabs from '../../../tabs';
import FTimePicker from '../../../time-picker';

const componentMap: Record<string, any> = {};
const componentPropsConverter: Record<string, any> = {};
let hasLoaded = false;

function loadRegister() {
    if (!hasLoaded) {
        hasLoaded = true;
        FAccordion.register(componentMap, componentPropsConverter);
        FButtonEdit.register(componentMap, componentPropsConverter);
        FCheckBox.register(componentMap, componentPropsConverter);
        FComboList.register(componentMap, componentPropsConverter);
        FDatePicker.register(componentMap, componentPropsConverter);
        FDataGrid.register(componentMap, componentPropsConverter);
        FDynamicForm.register(componentMap, componentPropsConverter);
        FNumberRange.register(componentMap, componentPropsConverter);
        FNumberSpinner.register(componentMap, componentPropsConverter);
        FInputGroup.register(componentMap, componentPropsConverter);
        FPageHeader.register(componentMap, componentPropsConverter);
        FQuerySolution.register(componentMap, componentPropsConverter);
        FRadioGroup.register(componentMap, componentPropsConverter);
        FResponseLayout.register(componentMap, componentPropsConverter);
        FSearchBox.register(componentMap, componentPropsConverter);
        FSection.register(componentMap, componentPropsConverter);
        FSplitter.register(componentMap, componentPropsConverter);
        FSwitch.register(componentMap, componentPropsConverter);
        FTabs.register(componentMap, componentPropsConverter);
        FTimePicker.register(componentMap, componentPropsConverter);
    }
}

export { componentMap, componentPropsConverter, loadRegister };
