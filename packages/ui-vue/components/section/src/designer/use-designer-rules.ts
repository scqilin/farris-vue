/* eslint-disable max-len */
/* eslint-disable complexity */
import { ref } from "vue";
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/use-dragula-common-rule";
import { ComponentSchema } from "../../../designer-canvas/src/types";

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

    /** 组件在拖拽时是否需要将所属的Component一起拖拽  */
    let triggerBelongedComponentToMoveWhenMoved = ref(false);

    /** 组件在删除时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToDeleteWhenDeleted = ref(false);

    let canAcceptChildContent = true;
    let isFixedContextRules = false;

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext);
        if (!basalRule) {
            return false;
        }

        if (!canAcceptChildContent) {
            return false;
        }
        return true;
    }

    /**
     * 判断当前容器的上下文
     */
    function resolveComponentContext() {
        triggerBelongedComponentToMoveWhenMoved.value = false;
        triggerBelongedComponentToDeleteWhenDeleted.value = false;
        isFixedContextRules = false;
        canAcceptChildContent = true;

        const component = schema;
        // 控件本身样式
        const cmpClass = component.appearance && component.appearance.class || '';
        const cmpClassList = cmpClass ? cmpClass.split(' ') : [];

        // 子级节点
        const childContents = component.contents || [];
        const firstChildContent = childContents.length ? childContents[0] : null;
        const firstChildClass = (firstChildContent && firstChildContent.appearance && firstChildContent.appearance.class) || '';
        const firstChildClassList = firstChildClass ? firstChildClass.split(' ') : [];

        // 父级节点
        const parent = parentSchema;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 1、卡片区域的section:Component-Section-Form三层结构
        if (cmpClassList.includes('f-section-form') && parent?.type === 'content-container' && firstChildClassList.includes('f-form-layout')) {
            canAcceptChildContent = false;
            triggerBelongedComponentToMoveWhenMoved.value = true;
            triggerBelongedComponentToDeleteWhenDeleted.value = false;
            isFixedContextRules = true;
            return true;
        }

        // 2、子表区域tab外的section间距
        if (cmpClassList.includes('f-section-tabs') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-component-tabs')) {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 3、隐藏DataGrid父级section间距、隐藏带筛选条的DataGrid父级Section间距
        if (cmpClassList.includes('f-section-grid') && parentClassList.includes('f-struct-wrapper') && (firstChildClassList.includes('f-component-grid') || firstChildClassList.includes('f-filter-container'))) {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 4、隐藏TreeGrid父级section间距
        if (cmpClassList.includes('f-section-treegrid') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-component-treetable')) {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 5、带导航的表格类填报模板：内部为form-table 时，隐藏间距
        if (cmpClassList.includes('f-section-oa-table') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-form-is-table')) {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 6、卡片类表格模板：内部为form-table 时，隐藏间距
        if (cmpClassList.includes('f-section-card-table') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-form-is-table')) {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 7、筛选方案的外层Section
        if (cmpClassList.includes('f-section-scheme') && firstChildContent && firstChildContent.type === 'query-solution') {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 8、section类子表：container-section-component
        if ((cmpClassList.includes('f-section-in-main') || cmpClassList.includes('f-section-in-mainsubcard')) &&
            parentClassList.includes('f-struct-wrapper') && firstChildContent && firstChildContent.type === 'content-container') {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 7、section类附件：component-section-FileUploadPreview
        if ((cmpClassList.includes('f-section-in-main') || cmpClassList.includes('f-section-in-mainsubcard')) &&
            parentClassList.includes('f-struct-wrapper') && firstChildContent && firstChildContent.type === 'file-upload-preview') {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }
        // 审批类
        if (firstChildContent && (firstChildContent.type === 'approval-comments' || firstChildContent.type === 'approval-comments')) {
            canAcceptChildContent = false;
            isFixedContextRules = false;
            return false;
        }
        // 3、隐藏预约日历父级section间距
        if (cmpClassList.includes('f-section-grid') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-component-appointment-calendar')) {
            canAcceptChildContent = false;
            isFixedContextRules = true;
            return true;
        }

        isFixedContextRules = false;
        return false;
    }

    function onAcceptMovedChildElement(sourceElement: DesignerHTMLElement) {
    }

    function hideNestedPaddingInDesginerView() {
        return isFixedContextRules;
    }

    function checkCanMoveComponent(): boolean {
        return !isFixedContextRules;
    }

    function checkCanDeleteComponent(): boolean {
        return !isFixedContextRules;
    }

    return {
        canAccepts,
        resolveComponentContext,
        onAcceptMovedChildElement,
        triggerBelongedComponentToMoveWhenMoved,
        triggerBelongedComponentToDeleteWhenDeleted,
        checkCanMoveComponent,
        checkCanDeleteComponent,
        hideNestedPaddingInDesginerView
    };

}
