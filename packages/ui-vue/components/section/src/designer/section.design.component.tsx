/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, inject, onMounted, ref, SetupContext } from 'vue';
import { SectionProps, sectionProps } from '../section.props';

import '../section.css';
import { useDesignerRules } from './use-designer-rules';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FSectionDesign',
    props: sectionProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: SectionProps, context: SetupContext) {
        const elementRef = ref();
        const sectionElementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.styles = 'display: inherit;';

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const shouldShowHeader = computed(() => {
            return props.showHeader && (!!props.mainTitle || !!props.subTitle);
        });

        const shouldShowHeaderTitle = computed(() => {
            return !!props.mainTitle || !!props.subTitle;
        });

        const shouldShowSubHeaderTitle = computed(() => {
            return !!props.subTitle;
        });

        const sectionClass = computed(() => {
            const classObject = {
                'f-section': true,
                'ide-cmp-section': true,
                'f-utils-fill': true,
                ' p-0': true
            } as Record<string, boolean>;
            return classObject;
        });

        const headerClass = computed(() => {
            const headClassObject = {
                'f-section-header': true
            } as Record<string, boolean>;
            return headClassObject;
        });

        const contentClass = computed(() => {
            const customClassArray = props.contentClass.split(' ') as string[];
            const contentClassObject = {
                'f-section-content': true,
                'drag-container': true
            } as Record<string, boolean>;
            customClassArray.reduce<Record<string, boolean>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, contentClassObject);
            return contentClassObject;
        });

        const mainTitle = ref(props.mainTitle);

        const subTitle = ref(props.subTitle);

        function renderSectionHeader() {
            return (
                shouldShowHeader.value && (
                    <div class={headerClass.value}>
                        {shouldShowHeaderTitle.value && (
                            <div class="f-title">
                                <h4 class="f-title-text">{mainTitle.value}</h4>
                                {shouldShowSubHeaderTitle.value && <span>{subTitle.value}</span>}
                            </div>
                        )}
                    </div>
                )
            );
        }

        function renderSectionContent() {
            return (
                <div ref={elementRef} class={contentClass.value} dragref={`${designItemContext.schema.id}-container`}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        }

        return () => {
            return (
                <div ref={sectionElementRef} class={sectionClass.value}>
                    {renderSectionHeader()}
                    {renderSectionContent()}
                </div>
            );
        };
    }
});
