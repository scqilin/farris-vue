import { resolveAppearance, MapperFunction } from '../../../dynamic-resolver';

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance],
    ['expanded', 'expandStatus'],
    ['title', (key: string, value: any) => ({ 'mainTitle': value })]
]);
