/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import sectionSchema from './schema/section.schema.json';

export interface ButtonAppearance {
    class: string;
}

export interface ButtonConfig {
    id: string;
    disable: boolean;
    title: string;
    click: any;
    appearance: ButtonAppearance;
    visible?: boolean;
}

export interface ToolbarConfig {
    position: string;
    contents: ButtonConfig[];
}

export const sectionProps = {
    clickThrottleTime: { type: Number, default: 350 },
    contentClass: { type: String, default: '' },
    context: { type: Object },
    customClass: { type: String, default: '' },
    enableAccordion: { type: Boolean, default: false },
    enableCollapse: { type: Boolean, default: true },
    enableMaximize: { type: Boolean, default: false },
    expandStatus: { type: Boolean, default: true },
    fill: { type: Boolean, default: false },
    headerClass: { type: String, default: '' },
    index: { type: Number },
    maxStatus: { type: Boolean, default: false },
    mainTitle: { type: String, default: '' },
    showHeader: { type: Boolean, default: true },
    showToolbarMoreButton: { type: Boolean, default: true },
    subTitle: { type: String, default: '' },
    toolbarPosition: { type: String, default: '' },
    toolbarButtons: { type: Array<object>, default: [] },
    toolbar: { type: Object as PropType<ToolbarConfig>, default: {} }
} as Record<string, any>;

export type SectionProps = ExtractPropTypes<typeof sectionProps>;

export const propsResolver = createPropsResolver<SectionProps>(sectionProps, sectionSchema, schemaMapper);
