/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { SectionProps, sectionProps } from './section.props';

import './section.css';

export default defineComponent({
    name: 'FSection',
    props: sectionProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: SectionProps, context: SetupContext) {
        const customClass = ref<string>(props.customClass);
        const shouldShowHeader = computed(() => {
            return props.showHeader && (!!props.mainTitle || !!props.subTitle);
        });

        const shouldShowHeaderTitle = computed(() => {
            return !!props.mainTitle || !!props.subTitle;
        });

        const shouldShowSubHeaderTitle = computed(() => {
            return !!props.subTitle;
        });

        const shouldShowToolbarInHeader = computed(() => {
            return false;
        });

        const shouldShowToolbarInContent = computed(() => {
            return false;
        });

        const shouldShowToolbarTemplateInContent = computed(() => {
            return false;
        });

        const shouldShowExtendArea = computed(() => {
            return false;
        });

        const toolbarButtons = ref([]);

        const sectionClass = computed(() => {
            const classObject = {
                'f-section': true
            } as Record<string, boolean>;
            if (customClass.value) {
                customClass.value.split(' ').reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
            return classObject;
        });

        const headerClass = computed(() => {
            const customClassArray = props.headerClass.split(' ') as string[];
            const headClassObject = {
                'f-section-header': true
            } as Record<string, boolean>;
            customClassArray.reduce<Record<string, boolean>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, headClassObject);
            return headClassObject;
        });

        const extendAreaClass = computed(() => ({
            'f-section-extend': true
        }));

        const contentClass = computed(() => {
            const customClassArray = props.contentClass.split(' ') as string[];
            const contentClassObject = {
                'f-section-content': true
            } as Record<string, boolean>;
            customClassArray.reduce<Record<string, boolean>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, contentClassObject);
            return contentClassObject;
        });

        function getToolbarState(buttonId: string, visiableMap: any, defaultValue: boolean) {
            return true;
        }

        const mainTitle = ref(props.mainTitle);

        const subTitle = ref(props.subTitle);

        function renderSectionHeader() {
            return (
                shouldShowHeader.value && (
                    <div class={headerClass.value}>
                        {shouldShowHeaderTitle.value && (
                            <div class="f-title">
                                <h4 class="f-title-text">{mainTitle.value}</h4>
                                {shouldShowSubHeaderTitle.value && <span>{subTitle.value}</span>}
                            </div>
                        )}
                    </div>
                )
            );
        }

        function renderSectionContent() {
            return <div class={contentClass.value}>{context.slots.default && context.slots.default()}</div>;
        }

        return () => {
            return (
                <div class={sectionClass.value}>
                    {renderSectionHeader()}
                    {renderSectionContent()}
                </div>
            );
        };
    }
});
