/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export interface ModalButton {
    class: string;
    focusedByDefault?: boolean;
    disable?: boolean;
    iconClass?: string;
    handle: ($event: MouseEvent) => void;
    text: string;
}

export const modalProps = {
    class: { type: String, default: '' },
    title: { type: String, default: '' },
    width: { type: Number, default: 500 },
    height: { type: Number, default: 320 },
    buttons: {
        type: Array<ModalButton>,
        default: []
    },
    modelValue: { type: Boolean, default: false },
    showHeader: { type: Boolean, default: true },
    showButtons: { type: Boolean, default: true }
};

export type ModalProps = ExtractPropTypes<typeof modalProps>;
