/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { listNavProps, ListNavProps } from './list-nav.props';

import './list-nav.scss';

export default defineComponent({
    name: 'FListNav',
    props: listNavProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: ListNavProps, context: SetupContext) {
        /** 初始收起状态 */
        let initCollapse = false;
        /** 导航位置 */
        const navPosition = ref(props.position);
        /** 宽度 */
        const listNavWidth = ref(props.listNavWidth);
        /** 初始时，是否隐藏Nav */
        const hideNav = computed({
            set(value: any) {
                if (value !== undefined && value !== null) {
                    if (initCollapse !== value) {
                        initCollapse = value;
                    }
                }
            },
            get() {
                return initCollapse;
            }
        });

        return () => {
            return (
                <>
                    <div class={`f-list-nav f-list-nav-${navPosition.value}`}>
                        <div
                            class="f-list-nav-in"
                            style={{
                                width: ['left', 'right'].includes(navPosition.value)
                                    ? !hideNav.value
                                        ? `${listNavWidth.value}px`
                                        : '0px'
                                    : '100%',
                                height: ['top', 'bottom'].includes(navPosition.value)
                                    ? !hideNav.value
                                        ? `${listNavWidth.value}px`
                                        : '0px'
                                    : '100%'
                            }}>
                            <div>
                                <div class="f-list-nav-main">
                                    {context.slots.navHeader && <div class="f-list-nav-header">{context.slots.navHeader()}</div>}
                                    {!context.slots.navHeader && props.title && (
                                        <div class="f-list-nav-header">
                                            <div class="f-list-nav-title">{props.title}</div>
                                        </div>
                                    )}
                                </div>
                                {context.slots.navContent && <div class="f-list-nav-content">{context.slots.navContent()}</div>}
                                {context.slots.navFooter && <div class="f-list-nav-footer">{context.slots.navFooter()}</div>}
                            </div>
                        </div>
                    </div>
                </>
            );
        };
    }
});
