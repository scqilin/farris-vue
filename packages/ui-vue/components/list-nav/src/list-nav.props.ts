/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

type positionValue = 'top' | 'left' | 'bottom' | 'right';

type dragDirection = 'e' | 's' | 'n' | 'w';

export const listNavProps = {
    /** hover */
    hover: { Type: Boolean, default: false },
    /** 是否禁用 */
    disabled: { Type: Boolean, default: false },
    /** 位置 */
    position: { Type: String as PropType<positionValue>, default: 'left' },
    /** listnav 名称 */
    title: { Type: String, default: '' },
    /** listnav宽度;不支持百分比，因为此处的宽度被用在内部是外部 */
    listNavWidth: { Type: Number, default: 218 },
    /** 显示入口 */
    showEntry: { Type: Boolean, default: true },
};

export type ListNavProps = ExtractPropTypes<typeof listNavProps>;
