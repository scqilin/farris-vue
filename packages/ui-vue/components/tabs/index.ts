/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import Tabs from './src/tabs.component';
import TabPage from './src/components/tab-page.component';
import TabsDesign from './src/designer/tabs.design.component';
import TabPageDesign from './src/designer/tab-page.design.component';
import { tabsPropsResolver } from './src/tabs.props';
import { tabPagePropsResolver } from './src/components/tab-page.props';

export * from './src/tabs.props';
export * from './src/components/tab-page.props';

export { Tabs, TabPage };

export default {
    install(app: App): void {
        app.component(Tabs.name, Tabs);
        app.component(TabPage.name, TabPage);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap.tabs = Tabs;
        componentMap['tab-page'] = TabPage;
        propsResolverMap.tabs = tabsPropsResolver;
        propsResolverMap['tab-page'] = tabPagePropsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap.tabs = TabsDesign;
        componentMap['tab-page'] = TabPageDesign;
        propsResolverMap.tabs = tabsPropsResolver;
        propsResolverMap['tab-page'] = tabPagePropsResolver;
    }
};
