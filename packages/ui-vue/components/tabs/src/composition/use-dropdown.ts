import { computed, ref } from "vue";
import { TabPageContext, UseDropdown, UseTabs } from "./types";
import { TabsProps } from "../tabs.props";

export function useDropdown(
    props: TabsProps,
    useTabsComposition: UseTabs
): UseDropdown {
    const { tabPages } = useTabsComposition;
    // 下拉框后的搜索文本
    const searchTabText = ref<string>('');

    // 显示下拉面板
    const hideDropDown = ref(true);

    const shouldShowSearchBox = computed(() => props.searchBoxVisible);

    const tabsInDropdownMenu = computed(() => {
        const tabsView: TabPageContext[] = shouldShowSearchBox.value ?
            tabPages.value.filter((tabPage: TabPageContext) => tabPage.props.title.includes(searchTabText.value)) :
            tabPages.value.slice();
        return tabsView;
    });

    return {
        searchTabText,
        hideDropDown,
        shouldShowSearchBox,
        tabsInDropdownMenu
    };
}
