/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, Ref, ShallowRef, Slots } from 'vue';
import { TabPageProps } from '../components/tab-page.props';

export interface TabPageContext {
    props: TabPageProps;
    slots: Slots;
}
export interface TabsContext {
    activeId: Ref<string | undefined>;
    addTab(props: TabPageContext): void;
    updateTab(props: TabPageContext): void;
    tabPages: Ref<TabPageContext[]>;
    tabType: Ref<string>;
    shouldFillParentContaner: ComputedRef<boolean>;
}

export type ToolbarPosition = 'inHead' | 'inContent';
export interface ToolbarContentsConf {
    id: string;
    disable: boolean;
    title: string;
    click: () => void;
    appearance: object;
}

export interface UseTabs {

    activeId: Ref<string>;

    addTab: (tabPageContext: TabPageContext) => void;

    changeTitleStyle: (titleUlElement: ShallowRef<any>) => void;

    removeTab: ($event: MouseEvent, targetTabId: string, closeDropdown?: boolean) => void;

    selectTab: (targetTabId: string) => void;

    selectTabByTabId: (targetTabId: string) => void;

    tabPages: Ref<TabPageContext[]>;

    updateTab: (tabPageContext: TabPageContext) => void;

    toolbarItems: Ref<any[]>;

}

export interface UseNav {

    previousButtonClass: ComputedRef<Record<string, boolean>>;

    nextButtonGroupClass: ComputedRef<Record<string, boolean>>;

    nextButtonClass: ComputedRef<Record<string, boolean>>;

    scrollTab: (move: number, direction: number) => void;

    selectAndScrollToTab: ($event: MouseEvent, tab: TabPageProps) => void;

    shouldShowNavigationButtons: Ref<boolean>;

    updateNavigationLayout: () => void;
}

export interface UseDropdown {

    searchTabText: Ref<string>;

    hideDropDown: Ref<boolean>;

    shouldShowSearchBox: ComputedRef<boolean>;

    tabsInDropdownMenu: ComputedRef<TabPageContext[]>;
}

export interface UseOnePage {
    scrollTo: (activePageIndex: number) => void;

    scrollToByPaggId: (activePageId: string) => void;
}
