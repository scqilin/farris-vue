import { ShallowRef, computed, nextTick, ref } from "vue";
import { TabsProps } from "../tabs.props";
import { TabPageProps } from "../components/tab-page.props";
import { TabPageContext, UseNav, UseOnePage, UseTabs } from "./types";

export function useNav(
    props: TabsProps,
    tabNavigationElementRef: ShallowRef<any>,
    useOnePageComposition: UseOnePage,
    useTabsComposition: UseTabs
): UseNav {
    // 显示左右滚动按钮
    const shouldShowNavigationButtons = ref(false);
    const { selectTabByTabId, tabPages } = useTabsComposition;
    const shrinkThreshold = 4;

    function updateNavigationLayout() {
        const tabNavigationElement = tabNavigationElementRef.value;
        const tabHeaderElement = tabNavigationElementRef.value?.parentElement;
        shouldShowNavigationButtons.value = tabHeaderElement && tabNavigationElement &&
            tabHeaderElement.offsetWidth < tabNavigationElement.scrollWidth - shrinkThreshold;
    };

    const previousButtonClass = computed(() => {
        const classObject = {
            'btn': true,
            'sc-nav-btn': true,
            'px-1': true,
            'sc-nav-lr': true,
            'd-none': !shouldShowNavigationButtons.value
        } as Record<string, boolean>;
        return classObject;
    });

    const nextButtonGroupClass = computed(() => {
        const classObject = {
            'btn-group': true,
            'sc-nav-btn': true,
            'dropdown': true,
            'd-none': !shouldShowNavigationButtons.value
        } as Record<string, boolean>;
        return classObject;
    });

    const nextButtonClass = computed(() => {
        const classObject = {
            btn: true,
            'sc-nav-rg': true,
            'd-none': !shouldShowNavigationButtons.value
        } as Record<string, boolean>;
        return classObject;
    });

    /**
     * 按照方向，滚动scrollStep距离
     * @param direction 方向
     */
    function scrollTab(move: number, direction: number) {
        if (!tabNavigationElementRef.value) {
            return;
        }
        const distScrollLeft = tabNavigationElementRef.value.scrollLeft;
        const maxScrollLeft = tabNavigationElementRef.value.scrollWidth - tabNavigationElementRef.value.offsetWidth;
        // 标签页左侧移动
        if (direction > 0) {
            if (tabNavigationElementRef.value.scrollLeft >= maxScrollLeft) {
                return;
            }
            tabNavigationElementRef.value.scrollLeft = distScrollLeft + props.scrollStep + move;
        } else if (direction < 0) {
            // 标签页右侧移动
            if (tabNavigationElementRef.value.scrollLeft <= 0) {
                return;
            }
            tabNavigationElementRef.value.scrollLeft = distScrollLeft - props.scrollStep - move;
        }
    };
    /**
     * 定位到某个tab
     * @param activeTabIndex 索引
     * @returns
     */
    function scrollToActiveTab(activeTabIndex: number) {
        if (!shouldShowNavigationButtons.value || !tabNavigationElementRef.value) {
            return;
        }
        const navItemSelector = props.tabType === 'pills' ? '.f-capsule-item' : '.nav-item';
        const tabs = tabNavigationElementRef.value.querySelectorAll(navItemSelector);
        const { parentElement } = tabNavigationElementRef.value;
        const scrollTabElement = tabs[activeTabIndex];
        if (scrollTabElement && parentElement) {
            const parentElementLeft = parentElement.getBoundingClientRect().left;
            const parentElementRight = parentElement.getBoundingClientRect().right;
            const activeTabLeft = scrollTabElement.getBoundingClientRect().left;
            const activeTabRight = scrollTabElement.getBoundingClientRect().right;
            if (activeTabLeft < parentElementLeft) {
                scrollTab(parentElementLeft - activeTabLeft, -1);
            } else if (parentElementRight < activeTabRight) {
                scrollTab(activeTabRight - parentElementRight, 1);
            }
            if (props.tabType === 'one-page') {
                useOnePageComposition.scrollTo(activeTabIndex);
            }
        }
    };

    // 选中某个tab
    function selectAndScrollToTab($event: MouseEvent, target: TabPageProps) {
        if (target.disabled) {
            return;
        }
        selectTabByTabId(target.id);
        const index = tabPages.value.findIndex((tabPage: TabPageContext) => tabPage.props.id === target.id);
        nextTick(() => {
            scrollToActiveTab(index);
        });
    };

    return {
        previousButtonClass,
        nextButtonGroupClass,
        nextButtonClass,
        scrollTab,
        selectAndScrollToTab,
        shouldShowNavigationButtons,
        updateNavigationLayout
    };
}
