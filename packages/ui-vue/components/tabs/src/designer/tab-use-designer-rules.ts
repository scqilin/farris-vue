/* eslint-disable max-len */
/* eslint-disable complexity */
import { DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";

export function useDesignerRules(schema: ComponentSchema, designerItemContext?: DesignerItemContext): UseDesignerRules {

    /** 是否隐藏tab的间距、线条 */
    let hidePadding = false;

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        return false;
    }

    /**
     * 判断当前容器的上下文
     */
    function resolveComponentContext() {
        hidePadding = false;

        // 控件本身样式
        const cmpClass = schema.appearance && schema.appearance.class || '';
        const cmpClassList = cmpClass ? cmpClass.split(' ') : [];

        // 父级节点
        const parentSchema = designerItemContext?.schema;
        const parentClass = parentSchema && parentSchema.appearance && parentSchema.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        const grandParent = designerItemContext?.parent && designerItemContext.parent.schema;
        const grandParentClass = grandParent && grandParent.appearance && grandParent.appearance.class || '';
        const grandParentClassList = grandParentClass ? grandParentClass.split(' ') : [];

        // 1、子表区域固定层级：container(f-struct-wrapper)--->section(f-section-tabs)--->tab(f-component-tabs)
        if (cmpClassList.includes('f-component-tabs') && parentClassList.includes('f-section-tabs') && grandParentClassList.includes('f-struct-wrapper')) {
            hidePadding = true;
            return true;
        }
    }

    function hideNestedPaddingInDesginerView() {
        return hidePadding;
    }

    return { canAccepts, resolveComponentContext, hideNestedPaddingInDesginerView };

}
