/* eslint-disable max-len */
/* eslint-disable complexity */
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/use-dragula-common-rule";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";

export function useDesignerRules(schema: ComponentSchema, designerItemContext?: DesignerItemContext): UseDesignerRules {

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext);
        if (!basalRule) {
            return false;
        }
        return true;
    }

    function onAcceptMovedChildElement(sourceElement: DesignerHTMLElement) {
    }


    return { canAccepts, onAcceptMovedChildElement };

}
