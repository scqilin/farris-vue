/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, SetupContext, watch, ref, onUnmounted, computed } from 'vue';
import { TabPageContext, TabsContext } from '../composition/types';
import { TabPageProps, tabPageProps } from './tab-page.props';
import FSection from '../../../section/src/section.component';

export default defineComponent({
    name: 'FTabPage',
    props: tabPageProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: TabPageProps, context: SetupContext) {
        const tabsContext = inject<TabsContext>('tabs');
        const showContent = ref(true);
        const tabPageContext: TabPageContext = {
            slots: context.slots,
            props
        };
        const tabType = ref(tabsContext?.tabType.value || 'default');

        onMounted(() => {
            const index = tabsContext?.tabPages.value.findIndex((tabPage: TabPageContext) => tabPage.props.id === props.id);
            if (!index || index === -1) {
                tabsContext?.addTab(tabPageContext);
            } else if (index > -1) {
                showContent.value = false;
                console.warn(`已经存在id为${props.id}的页签啦`);
            }
        });

        onUnmounted(() => {});

        const isActivePage = computed(() => props?.id === tabsContext?.activeId.value);

        const tabPageStyle = computed(() => {
            const styleObject = {
                display: isActivePage.value ? '' : 'none'
            } as Record<string, any>;
            return styleObject;
        });

        watch(
            () => props,
            (value) => {
                tabsContext?.updateTab({
                    props: value,
                    slots: context.slots
                });
            },
            {
                immediate: true,
                deep: true
            }
        );

        function sectionRender() {
            const content = context.slots.default?.() as any;
            return (
                <FSection main-title={props.title} class="farris-tab-page">
                    {content}
                </FSection>
            );
        }

        const tabPageClass = computed(() => {
            const classObject = {
                'farris-tab-page': true,
                'f-utils-fill-flex-column': isActivePage.value && tabsContext?.shouldFillParentContaner.value
            } as Record<string, boolean>;
            return classObject;
        });

        function defaultRender() {
            const content = context.slots.default?.() as any;
            return (
                <div class={tabPageClass.value} style={tabPageStyle.value}>
                    {content}
                </div>
            );
        }

        const pageContentRenderMap = new Map<string, () => JSX.Element>([
            ['default', defaultRender],
            ['one-page', sectionRender]
        ]);

        const pageContentRender = pageContentRenderMap.get(tabType.value) || defaultRender;

        return () => {
            // const content = context.slots.default?.() as any;
            return showContent.value ? pageContentRender() : null;
        };
    }
});
