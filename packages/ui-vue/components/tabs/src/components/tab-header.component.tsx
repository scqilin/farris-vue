import { ShallowRef, computed } from "vue";
import { TabsProps } from "../tabs.props";
import { TabPageContext, UseNav, UseOnePage, UseTabs } from "../composition/types";

import renderTabHeaderItem from './tab-header-item.component';

export default function (
    props: TabsProps,
    tabNavigationElementRef: ShallowRef<any>,
    useNavComposition: UseNav,
    useOnePageComposition: UseOnePage,
    useTabsComposition: UseTabs
) {
    const { shouldShowNavigationButtons } = useNavComposition;
    // 存储tab的props数组
    const { tabPages } = useTabsComposition;

    const tabHeaderClass = computed(() => {
        const classObject = {
            'spacer': true,
            'f-utils-fill': true,
            'spacer-sides-dropdown': shouldShowNavigationButtons.value
        } as Record<string, boolean>;
        return classObject;
    });

    const tabHeaderStyle = computed(() => {
        const styleObject = {
            'width': '100%',
            'display': 'flex',
            'justify-content': props.justifyContent
        } as Record<string, any>;
        return styleObject;
    });

    const tabNavigatorClass = computed(() => {
        const classObject = {
            'nav': true,
            'farris-nav-tabs': true,
            'flex-nowrap': true,
            'nav-fill': props.fill || props.tabType === 'fill',
            'nav-pills': props.tabType === 'pills',
            'flex-row': props.position === 'top' || props.position === 'bottom',
            'flex-column': props.position === 'left' || props.position === 'right'
        } as Record<string, boolean>;
        return classObject;
    });

    return () => {
        return (
            <div class={tabHeaderClass.value} style={tabHeaderStyle.value}>
                <ul class={tabNavigatorClass.value} ref={tabNavigationElementRef}>
                    {
                        tabPages.value.map((tabPage: TabPageContext) =>
                            renderTabHeaderItem(props, tabPage.props, tabPage, useOnePageComposition, useTabsComposition))
                    }
                </ul>
            </div>
        );
    };
}
