import { UseDropdown, UseNav, UseTabs } from "../composition/types";
import { TabsProps } from "../tabs.props";
import getDrowdownMenuRender from './tab-header-dropdown-menu.component';

export default function (
    props: TabsProps,
    useDropDownComposition: UseDropdown,
    useNavComposition: UseNav,
    useTabsComposition: UseTabs
) {
    const { hideDropDown } = useDropDownComposition;
    const { renderDropdownMenu } = getDrowdownMenuRender(props, useDropDownComposition, useNavComposition, useTabsComposition);

    function renderMorePagesButtton() {
        return <>
            <button
                title="toggle-button"
                class="btn dropdown-toggle-split dropdown-toggle"
                onClick={() => {
                    hideDropDown.value = false;
                }}></button>
            {renderDropdownMenu()}
        </>;
    }

    return { renderMorePagesButtton };
};
