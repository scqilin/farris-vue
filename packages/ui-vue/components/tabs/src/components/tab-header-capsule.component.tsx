import { ShallowRef, computed, onMounted, ref } from "vue";
import { TabsProps } from "../tabs.props";
import { TabPageContext, UseNav, UseOnePage, UseTabs } from "../composition/types";
import FCapsule from '../../../capsule/src/capsule.component';

import { CapsuleItem } from "../../../capsule";

export default function (
    props: TabsProps,
    tabNavigationElementRef: ShallowRef<any>,
    useNavComposition: UseNav,
    useOnePageComposition: UseOnePage,
    useTabsComposition: UseTabs
) {
    const capsuleInstance = ref<any>();
    const { shouldShowNavigationButtons } = useNavComposition;
    // 存储tab的props数组
    const { activeId, tabPages } = useTabsComposition;

    const tabHeaderItems = computed<CapsuleItem[]>(() => {
        return tabPages.value.map((tabPage: TabPageContext) => {
            return { name: tabPage.props.title, value: tabPage.props.id };
        });
    });

    const tabHeaderClass = computed(() => {
        const classObject = {
            'spacer': true,
            'f-utils-fill': true,
            'spacer-sides-dropdown': shouldShowNavigationButtons.value
        } as Record<string, boolean>;
        return classObject;
    });

    const tabHeaderStyle = computed(() => {
        const styleObject = {
            'width': '100%',
            'display': 'flex',
            'justify-content': props.justifyContent
        } as Record<string, any>;
        return styleObject;
    });

    onMounted(() => {
        if (capsuleInstance.value) {
            tabNavigationElementRef.value = capsuleInstance.value.$el;
        }
    });

    function onChange() {

    }

    return () => {
        return (
            <div class={tabHeaderClass.value} style={tabHeaderStyle.value}>
                <FCapsule items={tabHeaderItems.value} v-model={activeId.value}
                    onChange={onChange} ref={capsuleInstance}></FCapsule>
            </div>
        );
    };
}
