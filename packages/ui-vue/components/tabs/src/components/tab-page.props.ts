/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { ToolbarPosition, ToolbarContentsConf } from '../composition/types';
import { createPropsResolver } from '../../../dynamic-resolver';
import { schemaMapper } from '../schema/schema-mapper';
import tabPageSchema from '../schema/tab-page.schema.json';

export const tabPageProps = {
    tabWidth: { type: Number, default: -1 },
    id: { type: String, default: '' },
    customTitleClass: { type: String, default: '' },
    titleOverflow: { type: Boolean, default: false },
    title: { type: String, default: '' },
    selected: { type: Boolean, default: false },
    disabled: { type: Boolean, default: false },
    removeable: { type: Boolean, default: false },
    show: { type: Boolean, default: true },
    toolbar: { type: Object as PropType<ToolbarContentsConf>, default: {} },
    toolbarPosition: { type: String as PropType<ToolbarPosition>, default: 'inContent' }
} as Record<string, any>;

export type TabPageProps = ExtractPropTypes<typeof tabPageProps>;

export const tabPagePropsResolver = createPropsResolver<TabPageProps>(tabPageProps, tabPageSchema, schemaMapper);
