import { computed, ref } from "vue";
import { TabPageContext, UseDropdown, UseNav, UseTabs } from "../composition/types";
import { TabsProps } from "../tabs.props";

export default function (
    props: TabsProps,
    useDropDownComposition: UseDropdown,
    useNavComposition: UseNav,
    useTabsComposition: UseTabs
) {

    const { activeId, removeTab, tabPages } = useTabsComposition;
    const { selectAndScrollToTab } = useNavComposition;
    const { hideDropDown, searchTabText } = useDropDownComposition;

    const menuItemsWidth = ref<string>('auto');
    // 下拉框使用的tabs，包括搜索
    const cpTabs = computed(() => {
        let _cpTabs: TabPageContext[] = [];
        if (props.searchBoxVisible) {
            _cpTabs = tabPages.value?.filter((tabPage: TabPageContext) => tabPage.props.title.includes(searchTabText.value));
        } else {
            _cpTabs = tabPages.value?.slice();
        }
        return _cpTabs;
    });

    const dropdownMenuCls = computed(() => ({
        'dropdown-menu': true,
        'tabs-pt28': props.searchBoxVisible,
        show: !hideDropDown.value
    }));

    function getDropdownItemCls(tabPage: TabPageContext) {
        return {
            'dropdown-item': true,
            'text-truncate': true,
            'px-2': true,
            disabled: tabPage.props.disabled,
            active: tabPage.props.id === activeId.value,
            'd-none': tabPage.props.show !== true
        };
    };

    function stopBubble($event: MouseEvent) {
        $event.preventDefault();
        $event.stopPropagation();
    };

    const getMenuItemsWidth = () => {
        return { width: menuItemsWidth.value };
    };

    function renderDropdownMenu() {
        return (
            <div class={dropdownMenuCls.value}>
                {props.searchBoxVisible && (
                    <div onClick={($event) => stopBubble($event)} class="pb-1 tabs-li-absolute">
                        <input
                            title="search-box"
                            type="text"
                            class="form-control k-textbox"
                            v-model={searchTabText.value}
                        />
                        <span class="f-icon f-icon-page-title-query tabs-icon-search"></span>
                    </div>
                )}
                {cpTabs.value.length ? (
                    <ul class="tab-dropdown-menu--items" style={getMenuItemsWidth()}>
                        {cpTabs.value.map((tab) => {
                            return (
                                <li
                                    class={getDropdownItemCls(tab)}
                                    onClick={($event) => selectAndScrollToTab($event, tab.props)}>
                                    {tab.props.removeable && (
                                        <span
                                            class="float-right st-drop-close"
                                            onClick={($event) => removeTab($event, tab.props.id, true)}>
                                            <i class="f-icon f-icon-close"></i>
                                        </span>
                                    )}
                                    <a class="dropdown-title">{tab.props.title}</a>
                                </li>
                            );
                        })}
                    </ul>
                ) : (
                    <div class="dropdown-no-data">没有相关数据</div>
                )}
            </div>
        );
    }

    return { renderDropdownMenu };
};
