import type { App } from 'vue';
import Tags from './src/tags.component';

export * from './src/tags.props';

export { Tags };

export default {
    install(app: App): void {
        app.component(Tags.name, Tags);
    }
};
