import { Ref, SetupContext, ref } from "vue";
import { Tag, TagsProps } from "../tags.props";
import { TagsInnerElement, UseDraggable } from "./types";

export function useDraggable(
    props: TagsProps,
    context: SetupContext,
    innerTags: Ref<Tag[]>,
    innerElements: Ref<TagsInnerElement[]>
): UseDraggable {
    const draggingIndex = ref(-1);
    const isDragging = ref(false);

    function dragstart(e: DragEvent, targetItem: any, index: number) {
        e.stopPropagation();
        if (targetItem) {
            setTimeout(() => {
                draggingIndex.value = index;
                isDragging.value = true;
                targetItem.moving = true;
            });
        }
    }

    function dragenter(e: DragEvent, index: number) {
        e.preventDefault();
        if (draggingIndex.value !== index) {
            const draggingItem = innerElements.value[draggingIndex.value];
            const reOrderedItems = innerElements.value;
            reOrderedItems.splice(draggingIndex.value, 1);
            reOrderedItems.splice(index, 0, draggingItem);
            draggingIndex.value = index;
        }
    }
    function dragover(e: DragEvent, index: number) {
        e.preventDefault();
        if (e.dataTransfer) {
            e.dataTransfer.dropEffect = 'move';
        }
    }

    function dragend(e: DragEvent, targetItem: any) {
        if (targetItem) {
            targetItem.moving = false;
        }
        innerTags.value = innerElements.value
            .filter((innerElement: TagsInnerElement) => innerElement.type === 'Tag')
            .map((innerElement: TagsInnerElement) => innerElement.payload as Tag);
        isDragging.value = false;
        context.emit('change', innerTags.value);
    }

    return {
        dragstart,
        dragenter,
        dragover,
        dragend,
        isDragging
    };
}
