import { ExtractPropTypes, PropType } from 'vue';

export interface Tag {
    name: string;
    checked?: boolean;
    closable?: boolean;
    color?: string;
    selectable?: boolean;
    size?: string;
    tagType?: string;
    value?: string;
}

export type TagStyleType = 'default' | 'capsule';

export const tagsProps = {
    activeTag: { Type: String, default: '' },
    addButtonText: { Type: String, default: '' },
    customClass: { Type: String, default: '' },
    customStyle: { Type: String, default: '' },
    data: { type: Array<any>, default: [] },
    enableAddButton: { Type: Boolean, default: false },
    placeholder: { Type: String, default: '' },
    selectable: { Type: Boolean, default: false },
    showAddButton: { Type: Boolean, default: false },
    showClose: { Type: Boolean, default: false },
    showColor: { Type: Boolean, default: false },
    showInput: { Type: Boolean, default: false },
    tagType: { Type: String, default: '' },
    tagStyle: { Type: String as PropType<TagStyleType>, default: '' },
    wrapText: { Type: Boolean, default: false },
    draggable: { Type: Boolean, default: false }
};

export type TagsProps = ExtractPropTypes<typeof tagsProps>;
