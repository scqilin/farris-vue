/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { App } from 'vue';
import Accordion from './accordion';
import Avatar from './avatar';
import Button from './button';
import ButtonEdit from './button-edit';
import Calculator from './calculator';
import Calendar from './calendar';
import Capsule from './capsule';
import ColorPicker from './color-picker';
import CheckBox from './checkbox';
import ComboList from './combo-list';
import Component from './component';
import ContentContainer from './content-container';
import DataGrid from './data-grid';
import DatePicker from './date-picker';
import DesignerCanvas from './designer-canvas';
import DiscussionList from './discussion-list';
import DiscussionEditor from './discussion-editor';
import Dropdown from './dropdown';
import DynamicView from './dynamic-view';
import FilterBar from './filter-bar';
import ImageCopper from './image-cropper';
import InputGroup from './input-group';
import ListNav from './list-nav';
import ListView from './list-view';
import Loading from './loading';
import MessageBox from './message-box';
import Modal from './modal';
import Nav from './nav';
import Notify from './notify';
import NumberRange from './number-range';
import NumberSpinner from './number-spinner';
import Order from './order';
import PageFooter from './page-footer';
import PageHeader from './page-header';
import Pagination from './pagination';
import Popover from './popover';
import Progress from './propress';
import PropertyPanel from './property-panel';
import Condition from './condition';
import RadioGroup from './radio-group';
import Rate from './rate';
import ResonpseLayout from './response-layout';
import ResponseToolbar from './response-toolbar';
import SearchBox from './search-box';
import Section from './section';
import Splitter from './splitter';
import Step from './step';
import Switch from './switch';
import Tabs from './tabs';
import Tags from './tags';
import Text from './text';
import TimePicker from './time-picker';
import Tooltip from './tooltip';
import Transfer from './transfer';
import TreeView from './tree-view';
import Uploader from './uploader';
import VerifyDetail from './verify-detail';
import QuerySolution from './query-solution';

import '../public/assets/farris-all.css';

export { Accordion, AccordionItem } from './accordion';
export { ButtonEdit } from './button-edit';
export { FDataGrid as DataGrid } from './data-grid';
export { Section } from './section';
export * from './uploader';

export default {
    install(app: App): void {
        app.use(Accordion)
            .use(Avatar)
            .use(Button)
            .use(ButtonEdit)
            .use(Calendar)
            .use(Calculator)
            .use(Capsule)
            .use(CheckBox)
            .use(ColorPicker)
            .use(ComboList)
            .use(Component)
            .use(Condition)
            .use(ContentContainer)
            .use(DataGrid)
            .use(DatePicker)
            .use(DesignerCanvas)
            .use(DiscussionEditor)
            .use(DiscussionList)
            .use(Dropdown)
            .use(DynamicView)
            .use(FilterBar)
            .use(ImageCopper)
            .use(InputGroup)
            .use(ListNav)
            .use(ListView)
            .use(Loading)
            .use(MessageBox)
            .use(Modal)
            .use(Nav)
            .use(Notify)
            .use(NumberRange)
            .use(NumberSpinner)
            .use(Order)
            .use(PageFooter)
            .use(PageHeader)
            .use(Pagination)
            .use(Popover)
            .use(Progress)
            .use(PropertyPanel)
            .use(QuerySolution)
            .use(ResonpseLayout)
            .use(ResponseToolbar)
            .use(RadioGroup)
            .use(Rate)
            .use(SearchBox)
            .use(Section)
            .use(Splitter)
            .use(Step)
            .use(Switch)
            .use(Tabs)
            .use(Tags)
            .use(Text)
            .use(TimePicker)
            .use(Tooltip)
            .use(Transfer)
            .use(TreeView)
            .use(Uploader)
            .use(VerifyDetail);
    }
};
