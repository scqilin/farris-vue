import type { App } from 'vue';
import Calendar from './src/calendar.component';
import CalendarHeader from './src/components/header/header.component';
import CalendarDayView from './src/components/day/day-view.component';
import CalendarMonthView from './src/components/month/month-view.component';
import CalendarWeekView from './src/components/week/week-view.component';

export * from './src/calendar.props';
export * from './src/components/day/day-view.props';
export * from './src/components/header/header.props';
export * from './src/components/month/month-view.props';
export * from './src/components/week/week-view.props';

export * from './src/types/calendar';
export * from './src/types/common';
export * from './src/types/month';
export * from './src/types/month-view';
export * from './src/types/schedule';

export { Calendar, CalendarDayView, CalendarHeader, CalendarMonthView, CalendarWeekView };

export default {
    install(app: App): void {
        app.component(Calendar.name, Calendar)
            .component(CalendarDayView.name, CalendarDayView)
            .component(CalendarHeader.name, CalendarHeader)
            .component(CalendarMonthView.name, CalendarMonthView)
            .component(CalendarWeekView.name, CalendarWeekView);
    }
};
