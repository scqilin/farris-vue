import { ExtractPropTypes } from 'vue';
import { DateObject, weekDays } from '../../types/common';
import { ScheduleEvent } from '../../types/schedule';

export const weekViewProps = {
    daysInWeek: { Type: Array<string>, default: weekDays },
    enableMarkCurrent: { Type: Boolean, default: true },
    events: { Type: Array<ScheduleEvent>, default: [] },
    week: { Type: Object, default: { days: [], weekNumber: 0, year: 0 } }
};

export type WeekViewPropsType = ExtractPropTypes<typeof weekViewProps>;
