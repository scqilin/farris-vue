import { ExtractPropTypes } from 'vue';
import { DateObject, EventItem } from '../../types/common';

export const dayViewProps = {
    day: { Type: Object, default: {} },
    dayInWeek: { Type: String, default: '' },
    enableMarkCurrent: { Type: Boolean, default: true },
    events: { Type: Array<EventItem>, default: [] }
};

export type DayViewPropsType = ExtractPropTypes<typeof dayViewProps>;
