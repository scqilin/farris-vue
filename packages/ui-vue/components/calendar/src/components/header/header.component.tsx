import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { HeaderProps, headerProps } from './header.props';
import './header.css';

export default defineComponent({
    name: 'FCalendarHeader',
    props: headerProps,
    emits: ['ViewChange', 'Previous', 'Next', 'ResetToToday'],
    setup(props: HeaderProps, context: SetupContext) {
        const title = ref<string>(props.title);
        const dailyViewTitle = ref<string>(props.dailyViewTitle);
        const weeklyViewTitle = ref<string>(props.weeklyViewTitle);
        const monthlyViewTitle = ref<string>(props.monthlyViewTitle);
        const switchPadding = 2;
        const switchButtonWidth = 62;
        const activeViewIndex = ref<number>(props.activeView);
        const todayButtonText = ref<string>('Today');

        watch(
            () => props.title,
            () => {
                title.value = props.title;
            }
        );

        const activeViewStyle = computed(() => {
            const styleObject = {
                top: `${switchPadding}px`,
                left: `${activeViewIndex.value * (switchPadding + 1) + (activeViewIndex.value - 1) * (switchButtonWidth + 1)}px`
            } as Record<string, any>;
            return styleObject;
        });

        const activeViewTitle = computed(() => {
            if (activeViewIndex.value === 1) {
                return dailyViewTitle.value;
            }
            if (activeViewIndex.value === 2) {
                return weeklyViewTitle.value;
            }
            return monthlyViewTitle.value;
        });

        function changeToDailyView() {
            activeViewIndex.value = 1;
            context.emit('ViewChange', 1);
        }

        function changeToWeeklyView() {
            activeViewIndex.value = 2;
            context.emit('ViewChange', 2);
        }

        function changeToMonthlyView() {
            activeViewIndex.value = 3;
            context.emit('ViewChange', 3);
        }

        function navigateToPrevious() {
            context.emit('Previous', activeViewIndex.value);
        }

        function navigateToNext() {
            context.emit('Next', activeViewIndex.value);
        }

        function navigateToToday() {
            context.emit('ResetToToday');
        }

        return () => {
            return (
                <div class="f-calendar-header">
                    <div class="f-calendar-title">{title.value}</div>
                    <div class="f-calendar-navigator">
                        <div class="f-calendar-navigator-today btn btn-default" onClick={() => navigateToToday()}>
                            {todayButtonText.value}
                        </div>
                        <div class="f-calendar-navigator-button-group">
                            <div class="f-calendar-navigator-previous btn btn-default" onClick={() => navigateToPrevious()}>
                                <i class="k-icon k-i-arrow-chevron-left"></i>
                            </div>
                            <div class="f-calendar-navigator-next btn btn-default" onClick={() => navigateToNext()}>
                                <i class="k-icon k-i-arrow-chevron-right"></i>
                            </div>
                        </div>
                    </div>
                    <div class="f-calendar-view-switch">
                        <div class="f-calendar-view-switch-panel">
                            <span class="f-calendar-view-switch-daily" onClick={(payload: MouseEvent) => changeToDailyView()}>
                                {dailyViewTitle.value}
                            </span>
                            <span class="f-calendar-view-switch-weekly" onClick={(payload: MouseEvent) => changeToWeeklyView()}>
                                {weeklyViewTitle.value}
                            </span>
                            <span class="f-calendar-view-switch-monthly" onClick={(payload: MouseEvent) => changeToMonthlyView()}>
                                {monthlyViewTitle.value}
                            </span>
                        </div>
                        <div class="f-calendar-view-switch-active-view" style={activeViewStyle.value}>
                            {activeViewTitle.value}
                        </div>
                    </div>
                </div>
            );
        };
    }
});
