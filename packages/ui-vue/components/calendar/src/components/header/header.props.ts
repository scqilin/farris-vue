import { ExtractPropTypes } from 'vue';

export const headerProps = {
    activeView: { Type: Number, default: 3 },
    title: { Type: String, default: '' },
    dailyViewTitle: { Type: String, default: 'Day' },
    weeklyViewTitle: { Type: String, default: 'Week' },
    monthlyViewTitle: { Type: String, default: 'Month' }
};

export type HeaderProps = ExtractPropTypes<typeof headerProps>;
