export interface DateObject {
    year?: number;
    month?: number;
    day?: number;
    hour?: number;
    minute?: number;
    second?: number;
}

export interface Period {
    from: DateObject;
    to: DateObject;
}

export interface MarkStatus {
    marked: boolean;
    color: string;
}

export interface MarkedDates {
    dates: DateObject[];
    color: string;
}

export type SelectMode = 'day' | 'week' | 'month' | 'year';

export const weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export enum MonthTag {
    previous = 1,
    current = 2,
    next = 3
}

export interface EventItem {
    starts: DateObject;
    ends: DateObject;
    title: string;
}
