import { DateObject } from './common';

export interface MonthViewItem {
    date: DateObject;
    disable: boolean;
    displayText: string;
    isCurrent: boolean;
    month: number;
    range?: boolean;
    selected?: boolean;
}

export interface ActiveMonth {
    month: number;
    year: number;
    displayTextOfMonth: string;
    displayTextOfYear: string;
}

export interface NameOfMonths {
    [month: number]: string;
}

export const defaultNameOfMonths = {
    1: 'Jan',
    2: 'Feb',
    3: 'Mar',
    4: 'Apr',
    5: 'May',
    6: 'Jun',
    7: 'Jul',
    8: 'Aug',
    9: 'Sep',
    10: 'Oct',
    11: 'Nov',
    12: 'Dec'
} as Record<number, string>;
