import { ExtractPropTypes } from 'vue';
import { ScheduleEvent } from './types/schedule';

export const calendarProps = {
    events: { Type: Array<ScheduleEvent>, default: [] },
    firstDayOfTheWeek: { type: String, default: 'Sun.' }
};

export type CalendarPropsType = ExtractPropTypes<typeof calendarProps>;
