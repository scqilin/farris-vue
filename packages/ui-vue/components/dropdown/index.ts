import type { App } from 'vue';
import Dropdown from './src/dropdown.component';

export * from './src/types';
export * from './src/composition/props/dropdown.props';

export { Dropdown };

export default {
    install(app: App): void {
        app.component(Dropdown.name, Dropdown);
    }
};
