/* eslint-disable max-len */
import { ExtractPropTypes } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { schemaMapper } from "./schema/schema-mapper";
import { schemaResolver } from "./schema/schema-resolver";
import responseLayoutSchema from './schema/response-layout.schema.json';

export const responseLayoutProps = {
    customClass: { type: String, default: '' }
} as Record<string, any>;

export type ResponseLayoutPropsType = ExtractPropTypes<typeof responseLayoutProps>;

export const responseLayoutPropsResolver = createPropsResolver<ResponseLayoutPropsType>(responseLayoutProps, responseLayoutSchema, schemaMapper, schemaResolver);
