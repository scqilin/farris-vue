import { SetupContext, defineComponent, inject, onMounted, ref } from 'vue';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { ResponseLayoutItemPropsType, responseLayoutItemProps } from '../component/response-layout-item.props';
import FResponseLayoutItem from '../component/response-layout-item.component';

export default defineComponent({
    name: 'FResponseLayoutItemDesign',
    props: responseLayoutItemProps,
    emits: [],
    setup(props: ResponseLayoutItemPropsType, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);
        componentInstance.value.canNested = false;
        componentInstance.value.canMove = false;
        componentInstance.value.canDelete = false;

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <FResponseLayoutItem ref={elementRef}>
                    {context.slots.default && context.slots.default()}
                </FResponseLayoutItem>
            );
        };
    }
});
