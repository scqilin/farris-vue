/* eslint-disable max-len */
import { ExtractPropTypes } from "vue";
import { createPropsResolver } from "../../../dynamic-resolver";
import { schemaMapper } from "../schema/schema-mapper";
import responseLayoutItemSchema from '../schema/response-layout-item.schema.json';

export const responseLayoutItemProps = {
    customClass: { type: String, default: '' }
} as Record<string, any>;

export type ResponseLayoutItemPropsType = ExtractPropTypes<typeof responseLayoutItemProps>;

export const responseLayoutItemPropsResolver = createPropsResolver<ResponseLayoutItemPropsType>(responseLayoutItemProps, responseLayoutItemSchema, schemaMapper);
