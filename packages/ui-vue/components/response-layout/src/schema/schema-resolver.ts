import { ComponentSchema } from "../../../designer-canvas/src/types";
import { DynamicResolver } from "../../../dynamic-resolver";

export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    if (context.splitter) {
        schema.contents = String(context.splitter).split(':')
            .map((columnSize: string) => {
                const responseLayoutItemSchema = resolver.getSchemaByType('response-layout-item') as ComponentSchema;
                responseLayoutItemSchema.appearance = {
                    "class": `h-100 col-${columnSize} px-0`
                };
                return responseLayoutItemSchema;

            });
    }
    return schema;
}
