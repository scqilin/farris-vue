/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import ResponseLayout from './src/response-layout.component';
import ResponseLayoutItem from './src/component/response-layout-item.component';
import ResponseLayoutDesign from './src/designer/response-layout.design.component';
import ResponseLayoutItemDesign from './src/designer/response-layout-item.design.component';
import { responseLayoutPropsResolver } from './src/response-layout.props';
import { responseLayoutItemPropsResolver } from './src/component/response-layout-item.props';

export * from './src/response-layout.props';
export { ResponseLayout, ResponseLayoutItem };

export default {
    install(app: App): void {
        app.component(ResponseLayout.name, ResponseLayout)
            .component(ResponseLayoutItem.name, ResponseLayoutItem);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['response-layout'] = ResponseLayout;
        componentMap['response-layout-item'] = ResponseLayoutItem;
        propsResolverMap['response-layout'] = responseLayoutPropsResolver;
        propsResolverMap['response-layout-item'] = responseLayoutItemPropsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['response-layout'] = ResponseLayoutDesign;
        componentMap['response-layout-item'] = ResponseLayoutItemDesign;
        propsResolverMap['response-layout'] = responseLayoutPropsResolver;
        propsResolverMap['response-layout-item'] = responseLayoutItemPropsResolver;
    }
};
