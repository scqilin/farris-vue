/* eslint-disable max-len */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, Ref } from 'vue';

export interface LoadingConfig {
    width?: number;
    container?: any;
    message?: string;
    position?: string;
    template?: string;
    type?: number;
    /** 延迟1s 后在显示 */
    delay?: number;
}

export const loadingDefaultConfig = {
    width: 30,
    container: 'body',
    message: '正在加载中，请稍候...',
    posion: 'center',
    type: 0,
    delay: 300
};

// export const LOADING_DEFAULT_CONFIG = new InjectionToken<LoadingConfig>('loading default options.');

export const LOADING_STYLES = [
    `<div class="f-loading-round"></div>`,
    `<div class="f-loading-dot-wrapper">
      <div class="f-loading-dot">
          <div class="dot dot1"></div>
          <div class="dot dot2"></div>
          <div class="dot dot3"></div>
          <div class="dot dot4"></div>
      </div>
  </div>`,
    `<svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-coffee"><defs><linearGradient id="gradient" x1="0%" x2="0%" y1="0%" y2="100%"><stop offset="10%" stop-color="black" stop-opacity="0"></stop><stop offset="100%" stop-color="white" stop-opacity="1"></stop></linearGradient><mask id="mask" maskUnits="userSpaceOnUse" x="0" y="0" width="100" height="100"><rect x="22" y="8" width="56" height="54" fill="url(#gradient)"></rect></mask><path id="steam" d="M0-4c-2.1,2.6-2.1,6.4,0,9l0,0c2.1,2.6,2.1,6.4,0,9l0,0c-2.1,2.6-2.1,6.4,0,9l0,0c2.1,2.6,2.1,6.4,0,9l0,0 c-2.1,2.6-2.1,6.4,0,9l0,0c2.1,2.6,2.1,6.4,0,9c-2.1,2.6-2.1,6.4,0,9l0,0c2.1,2.6,2.1,6.4,0,9l0,0c-2.1,2.6-2.1,6.4,0,9l0,0 c2.1,2.6,2.1,6.4,0,9l0,0c-2.1,2.6-2.1,6.4,0,9l0,0c2.1,2.6,2.1,6.4,0,9c-2.1,2.6-2.1,6.4,0,9l0,0c2.1,2.6,2.1,6.4,0,9l0,0 c-2.1,2.6-2.1,6.4,0,9h0c2.1,2.6,2.1,6.4,0,9h0c-2.1,2.6-2.1,6.4,0,9h0c2.1,2.6,2.1,6.4,0,9" stroke-width="6" stroke-linecap="round" fill="#f00" stroke="#58a8f6"></path></defs><g mask="url(#mask)"><use x="29" y="1.00876" href="#steam"><animate attributeName="y" calcMode="linear" values="4;-14" keyTimes="0;1" dur="0.5" begin="0s" repeatCount="indefinite"></animate></use><use x="47" y="-5.98248" href="#steam"><animate attributeName="y" calcMode="linear" values="0;-18" keyTimes="0;1" dur="0.25" begin="0s" repeatCount="indefinite"></animate></use><use x="64" y="-8.48686" href="#steam"><animate attributeName="y" calcMode="linear" values="-4;-22" keyTimes="0;1" dur="0.3333333333333333" begin="0s" repeatCount="indefinite"></animate></use></g><path d="M81.2,52.5l-5.2,0V49c0-1.6-1.3-3-3-3H20c-1.6,0-3,1.3-3,3v11.6C17,71.3,25.7,80,36.5,80h20.1 c7.1,0,13.3-3.8,16.7-9.5h8.3c5.2,0,9.3-4.4,9-9.6C90.2,56.1,86,52.5,81.2,52.5z M81.5,67.5h-6.8c0.8-2.2,1.3-4.5,1.3-7v-5h5.5 c3.3,0,6,2.7,6,6S84.8,67.5,81.5,67.5z" fill="#58a8f6"></path><path d="M78.8,88H19.2c-1.1,0-2-0.9-2-2s0.9-2,2-2h59.5c1.1,0,2,0.9,2,2S79.9,88,78.8,88z" fill="rgba(80.78431372549018%,94.50980392156862%,72.15686274509802%,0)"></path></svg>`
];
