import FLoading from './loading.component';
import { onUnmounted, App, createApp, reactive } from 'vue';

let appInstance: App | null = null;

function initInstance(props: any): void {
    const container = document.createElement('div');
    container.style.display = 'contents';

    appInstance = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            return () => (
                <FLoading message={props.message}></FLoading>
            );
        }
    });

    document.body.appendChild(container);
    appInstance.mount(container);
}

export default class LoadingService {
    static show(msg?: string): void {
        if (!appInstance) {
            const props = {
                message: msg,
            };
            initInstance(props);
        }
    }

    static close(): void {
        if (appInstance) {
            appInstance.unmount();
            appInstance = null;
        }
    }
}
