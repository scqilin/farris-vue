/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export const loadingProps = {
    showMessage: { type: Boolean, default: true },
    message: { type: String, default: '加载中，请稍后...' },
    debug: { type: Boolean, default: false },
    isActive: { type: Boolean, default: false },
};
export type LoadingProps = ExtractPropTypes<typeof loadingProps>;
