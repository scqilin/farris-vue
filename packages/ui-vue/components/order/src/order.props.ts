import { ExtractPropTypes } from 'vue';

export type SortType = 'asc' | 'desc';

export interface OrderedItem {
    id: string;
    name: string;
    order: SortType;
}

export const orderProps = {
    dataSource: { type: Array<object>, default: [] },
    items: { type: Array<OrderedItem>, default: [] }
};

export type OrderPropsType = ExtractPropTypes<typeof orderProps>;
