import { App } from 'vue';
import Order from './src/order.component';

export * from './src/order.props';
export { Order };

export default {
    install(app: App): void {
        app.component(Order.name, Order);
    }
};
