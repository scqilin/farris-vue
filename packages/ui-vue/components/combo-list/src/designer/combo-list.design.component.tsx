/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, Ref, ref, SetupContext } from 'vue';
import { comboListProps, ComboListProps } from '../combo-list.props';
import FButtonEdit from '../../../button-edit/src/button-edit.component';

import '../combo-list.css';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FComboListDesign',
    props: comboListProps,
    emits: ['clear', 'update:modelValue', 'change'] as (string[] & ThisType<void>) | undefined,
    setup(props: ComboListProps, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <FButtonEdit
                    ref={elementRef}
                    id={props.id}
                    disable={props.disabled}
                    readonly={props.readonly}
                    forcePlaceholder={props.forcePlaceholder}
                    editable={props.editable}
                    buttonContent={props.dropDownIcon}
                    placeholder={props.placeholder}
                    enableClear={props.enableClear}
                    maxLength={props.maxLength}
                    tabIndex={props.tabIndex}
                    enableTitle={props.enableTitle}
                    multiSelect={props.multiSelect}
                    inputType={props.multiSelect ? 'tag' : 'text'}></FButtonEdit>
            );
        };
    }
});
