/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext, watch } from 'vue';
import { ListContainerProps, listContainerProps } from './list-container.props';
import FListView from '../../../list-view/src/list-view.component';

export default defineComponent({
    name: 'FComboListContainer',
    props: listContainerProps,
    emits: ['selectionChange'],
    setup(props: ListContainerProps, context: SetupContext) {
        const dataSource = ref(props.dataSource);
        const selections = ref<any[]>([]);
        const separator = ref(props.separator);
        const width = ref(props.width);
        const height = ref(props.height);
        const selectionValues = ref<string[]>(props.selectedValues.split(separator.value));

        watch(props.dataSource, () => {
            dataSource.value = props.dataSource;
        });

        const listViewHeader = computed(() => {
            return props.enableSearch ? 'SearchBar' : 'ContentHeader';
        });

        const listContainerStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if (width.value !== undefined) {
                styleObject.width = `${width.value}px`;
            }
            if (height.value !== undefined) {
                styleObject.height = `${height.value}px`;
            }
            return styleObject;
        });

        function onSelectionChange(seletedItems: any[]) {
            selections.value = seletedItems.map((item: any) => Object.assign({}, item));
            selectionValues.value = seletedItems.map((item: any) => item[props.idField]);
            context.emit('selectionChange', selections.value);
        }

        return () => {
            return (
                <div class="f-combo-list-container" style={listContainerStyle.value}>
                    <FListView
                        size="small"
                        itemClass="f-combo-list-item"
                        header={listViewHeader.value}
                        headerClass="f-combo-list-search-box"
                        data={dataSource.value}
                        idField={props.idField}
                        textField={props.textField}
                        multiSelect={props.multiSelect}
                        multiSelectMode="OnCheckAndClick"
                        selection-values={selectionValues.value}
                        onSelectionChange={onSelectionChange}></FListView>
                </div>
            );
        };
    }
});
