import { ExtractPropTypes } from "vue";
import { Option } from '../combo-list.props';

export const listContainerProps = {

    dataSource: { type: Array<Option>, default: [] },

    enableSearch: { type: Boolean, default: false },

    idField: { type: String, default: 'id' },

    multiSelect: { default: false, type: Boolean },

    selectedValues: { type: String, default: '' },

    separator: { type: String, default: ',' },

    textField: { type: String, default: 'name' },

    width: { type: Number },

    height: { type: Number },

    valueField: { type: String, default: 'id' }
};
export type ListContainerProps = ExtractPropTypes<typeof listContainerProps>;
