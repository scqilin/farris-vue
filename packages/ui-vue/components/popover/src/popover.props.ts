/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType, ref } from 'vue';

export type PopoverPlacement = 'top' | 'bottom' | 'left' | 'right' | 'auto' | 'bottom-left';

export const popoverProps = {
    title: { type: String },
    placement: { type: String as PropType<PopoverPlacement>, default: 'bottom' },
    reference: {
        type: Object as PropType<any>
    },
    host: {
        type: Object as PropType<any>
    },
    leftBoundary: {
        type: Object as PropType<any>
    },
    rightBoundary: {
        type: Object as PropType<any>
    },
    visible: { type: Boolean, default: false },
    class: { type: String },
    offsetX: { type: Object as PropType<any>, default: ref(0) },
    zIndex: { type: Number, default: -1 },
    keepWidthWithReference: { type: Boolean, default: false },
    fitContent: { type: Boolean, default: false },
    minWidth: { type: Number, default: -1 }
};

export type PopoverProps = ExtractPropTypes<typeof popoverProps>;
