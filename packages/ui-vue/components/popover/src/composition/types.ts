import { ComputedRef, Ref } from "vue";
import { PopoverPlacement } from "../popover.props";

export interface UsePosition {

    arrowStyle: ComputedRef<Record<string, any>>;

    popoverStyle: ComputedRef<Record<string, any>>;

    position: Ref<PopoverPlacement>;

    popoverWidth: Ref<number>;

    fitToReference: (referenceElement: HTMLElement) => void;

    followToReferencePosition: (referenceElement: HTMLElement) => void;

    locateToReference: (referenceElement: HTMLElement) => void;
}

export interface UseHost {

    host: any;

    hostLeft: ComputedRef<number>;

    hostTop: ComputedRef<number>;

    hostWidth: ComputedRef<number>;
}

export interface UsePopup {

    hidePopverOnClickBodyHandler: ($event: MouseEvent) => any;

    showPopover: Ref<boolean>;

}

export interface UseResize {

    onResize: () => void;
}
