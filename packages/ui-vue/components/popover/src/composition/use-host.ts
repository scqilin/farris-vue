import { computed } from "vue";
import { PopoverProps } from "../popover.props";
import { UseHost } from "./types";

export function useHost(props: PopoverProps): UseHost {

    const host = props.host ? props.host : 'body';

    const hostTop = computed(() => {
        return host === 'body' ? 0 : (host ? host.getBoundingClientRect().top : 0);
    });

    const hostLeft = computed(() => {
        return host === 'body' ? 0 : (host ? host.getBoundingClientRect().left : 0);
    });

    const hostWidth = computed(() => {
        return host === 'body' ? document.body.getBoundingClientRect().width :
            (
                host ? host.getBoundingClientRect().width -
                    (
                        props.rightBoundary ? props.rightBoundary.getBoundingClientRect().width : 0
                    )
                    : document.body.getBoundingClientRect().width
            );
    });

    return { host, hostLeft, hostTop, hostWidth };
}
