import { SetupContext, computed } from 'vue';
import { NumberSpinnerProps } from '../number-spinner.props';
import { UseTextBox } from '../composition/types';

export default function (props: NumberSpinnerProps, context: SetupContext, useTextBoxComposition: UseTextBox) {
    const { onBlurTextBox, onFocusTextBox, onInput, onKeyDown, textBoxValue } = useTextBoxComposition;
    const placeholder = computed(() => (props.disabled || props.readonly || !props.editable ? '' : props.placeholder));

    const numberTextBoxClass = computed(() => ({
        'form-control': true,
        'f-utils-fill': true
    }));

    const numberTextBoxStyle = computed(() => {
        const styleObject = {
            'text-align': props.textAlign
        } as Record<string, any>;
        return styleObject;
    });

    function onChange($event: Event) {
        $event.stopPropagation();
    }

    return () => {
        return (
            <input
                class={numberTextBoxClass.value}
                style={numberTextBoxStyle.value}
                type="text"
                value={textBoxValue.value}
                disabled={props.disabled}
                readonly={props.readonly || !props.editable}
                placeholder={placeholder.value}
                onBlur={onBlurTextBox}
                onChange={onChange}
                onFocus={onFocusTextBox}
                onInput={onInput}
                onKeydown={onKeyDown}
            />
        );
    };
}
