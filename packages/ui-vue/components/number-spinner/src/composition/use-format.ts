import { SetupContext, computed, ref } from 'vue';
import BigNumber from "bignumber.js";
import { NumberFormatOptions, UseFormat, UseNumber } from "./types";
import { NumberSpinnerProps } from '../number-spinner.props';

export function useFormat(
    props: NumberSpinnerProps,
    context: SetupContext,
    useNumberComposition: UseNumber
): UseFormat {
    const formatOptions = computed<NumberFormatOptions>(() => ({
        prefix: props.prefix,
        suffix: props.suffix,
        decimalSeparator: props.decimalSeparator,
        groupSeparator: props.useThousands ? props.groupSeparator : '',
        groupSize: props.groupSize
    }));
    const { getValidNumberObject, precision } = useNumberComposition;

    /**
     * 清洗数据为数字
     * @param val 输入值，带有前缀、后缀等
     * @returns 返回的纯数字数据
     */
    function cleanFormat(val: string) {
        val = val === null || val === undefined || val === '' ? '' : String(val);
        val = val.replace(new RegExp(props.prefix, 'g'), '').replace(new RegExp(props.suffix, 'g'), '').replace(/,/g, '');
        if (props.groupSeparator && props.groupSeparator !== ',') {
            val = val.replace(new RegExp(`\\${props.groupSeparator}`, 'g'), '');
        }

        if (props.decimalSeparator && props.decimalSeparator !== '.') {
            val = val.replace(new RegExp(`\\${props.decimalSeparator}`, 'g'), '.');
        }
        return val;
    }

    // function _toFormat(_bgNum: BigNumber, fmt: NumberFormatter) {
    //     if (props.precision !== null && props.precision !== undefined) {
    //         return _bgNum.toFormat(precision.value, fmt);
    //     }
    //     return _bgNum.toFormat(fmt);
    // }

    /** 格式化数据 */
    // function format(val: any) {
    //     val = cleanFormat(val);
    //     const bigVal = new BigNumber(val);
    //     const _bgNum = getValidNumberObject(bigVal);
    //     if (_bgNum.valueOf() === '0' && !props.showZero) {
    //         return '';
    //     }

    //     if (props.canNull && bigVal.isNaN()) {
    //         return '';
    //     }
    //     if (_bgNum.isNaN()) {
    //         return '';
    //     }

    //     if (props.formatter) {
    //         return props.formatter(_bgNum.toNumber());
    //     }
    //     return _toFormat(_bgNum, formatOptions.value);
    // }

    function applyFormatOptions(numberObject: BigNumber, formatOptions: NumberFormatOptions) {
        if (props.precision !== null && props.precision !== undefined) {
            return numberObject.toFormat(precision.value, formatOptions);
        }
        return numberObject.toFormat(formatOptions);
    }

    function format(numberString: string) {
        const rawNumberString = cleanFormat(numberString);
        const initialNumberObject = new BigNumber(rawNumberString, 10);
        const validNumberObject = getValidNumberObject(initialNumberObject);
        if (validNumberObject.valueOf() === '0' && !props.showZero) {
            return '';
        }
        if (validNumberObject.isNaN()) {
            return '';
        }
        if (props.formatter) {
            return props.formatter(validNumberObject.toNumber());
        }
        const formattedNumberString = applyFormatOptions(validNumberObject, formatOptions.value);
        return formattedNumberString;
    }

    return { cleanFormat, format };
}
