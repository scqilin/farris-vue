import { SetupContext, computed } from "vue";
import { NumberSpinnerProps } from "../number-spinner.props";
import { UseFormat, UseNumber, UseSpinner, UseTextBox } from "./types";

export function useTextBox(
    props: NumberSpinnerProps,
    context: SetupContext,
    useFormatComposition: UseFormat,
    useNumberComposition: UseNumber,
    useSpinnerComposition: UseSpinner
): UseTextBox {
    const { cleanFormat, format } = useFormatComposition;
    const { displayValue, getRealValue, modelValue, isEmpty, onNumberValueChanged } = useNumberComposition;
    const { downward, upward } = useSpinnerComposition;
    const textBoxValue = computed(() => displayValue.value);
    /**
     * 输入框失焦时执行的方法
     */
    function onBlurTextBox($event: Event) {
        $event.stopPropagation();
        if (props.readonly || props.disabled) {
            return;
        }
        const textValue = cleanFormat(($event.target as HTMLTextAreaElement)?.value || 0);
        displayValue.value = format(textValue);
        onNumberValueChanged(getRealValue(textValue));
        context.emit('blur', { event: $event, formatted: displayValue.value, value: modelValue.value });
    }

    /**
     * 输入框获取焦点时执行的方法
     */
    function onFocusTextBox($event: Event) {
        $event.stopPropagation();
        if (props.readonly || props.disabled) {
            return;
        }
        displayValue.value = isEmpty(modelValue.value) ? '' : !props.showZero && modelValue.value === '0' ? '' : String(modelValue.value);
        context.emit('focus', { event: $event, formatted: displayValue.value, value: modelValue.value });
    }

    /**
     * 输入框的input事件
     */
    function onInput($event: Event) {
        $event.stopPropagation();
        const textValue = cleanFormat(($event.target as HTMLTextAreaElement)?.value || 0);
        displayValue.value = textValue;
        onNumberValueChanged(getRealValue(textValue));
    }

    /**
     * 焦点状态下键盘监听事件
     */
    function onKeyDown($event: KeyboardEvent) {
        if ($event.key === 'ArrowDown') {
            $event.preventDefault();
            downward();
        }
        if ($event.key === 'ArrowUp') {
            $event.preventDefault();
            upward();
        }
        $event.stopPropagation();
    }

    return { textBoxValue, onBlurTextBox, onFocusTextBox, onInput, onKeyDown };
}
