/* eslint-disable max-len */
/* eslint-disable complexity */
import { ref } from "vue";
import { useDesignerRules } from "../../../component/src/designer/use-designer-rules";
import { DraggingResolveContext, DesignerHTMLElement, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";

export function useDesignerRulesForDataGrid(designItemContext: DesignerItemContext): UseDesignerRules {

    const schema = designItemContext.schema as ComponentSchema;

    /** 组件在拖拽时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToMoveWhenMoved = ref(true);
    
    /** 组件在删除时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToDeleteWhenDeleted = ref(true);

    /** data-grid所属的上级组件控制规则 */
    let belongedComponentRules: UseDesignerRules;

    /**
     * 获取data-grid所属的组件控制规则
     */
    function getBelongedComponentContext() {
        const belongedCmp = getBelongedComponentSchema(schema, designItemContext);
        if (belongedCmp && belongedCmp.cmpSchema && belongedCmp.cmpParentSchema) {
            belongedComponentRules = useDesignerRules(belongedCmp.cmpSchema, belongedCmp.cmpParentSchema);
            if (belongedComponentRules && belongedComponentRules.resolveComponentContext) {
                belongedComponentRules.resolveComponentContext();
            }
        }
    }

    /**
     * 判断data-grid上下文
     */
    function resolveComponentContext() {
        getBelongedComponentContext();
    }

    /**
     * 判断是否可以接收拖拽新增的子级控件sdsf
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        if (!draggingContext) {
            return true;
        }

        // 接收工具箱中拖拽来的输入类控件
        if (draggingContext.sourceType === 'control') {
            return draggingContext.controlCategory === 'input';
        }
        return false;

    }


    function onAcceptMovedChildElement(sourceElement: DesignerHTMLElement) {
    }

    /**
     * data-grid上级中type='component'的控件，即为data-grid所属的组件
     */
    function getBelongedComponentSchema(cmpSchema?: ComponentSchema, cmpContext?: DesignerItemContext): { cmpSchema: ComponentSchema | null, cmpParentSchema: ComponentSchema | null } | null {
        if (!cmpSchema || !cmpContext) {
            return null;
        }

        if (cmpSchema.type === 'component') {
            return {
                cmpSchema,
                cmpParentSchema: cmpContext?.parent?.schema || null
            };
        }

        const grandParent = getBelongedComponentSchema(cmpContext?.parent?.schema, cmpContext?.parent);
        if (grandParent) {
            return grandParent;
        }
        return null;
    }

    /**
     * data-grid是否支持删除，取决于所属组件是否支持删除
     */
    function checkCanDeleteComponent() {
        if (belongedComponentRules && belongedComponentRules.checkCanDeleteComponent) {
            return belongedComponentRules.checkCanDeleteComponent();
        }

        return true;
    }
    /**
     * data-grid是否支持移动，取决于所属组件是否支持移动
     */
    function checkCanMoveComponent() {

        if (belongedComponentRules && belongedComponentRules.checkCanMoveComponent) {
            return belongedComponentRules.checkCanMoveComponent();
        }

        return true;
    }
    return {
        canAccepts,
        onAcceptMovedChildElement,
        triggerBelongedComponentToMoveWhenMoved,
        triggerBelongedComponentToDeleteWhenDeleted,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        resolveComponentContext
    };

}
