import { UseVisualDataBound, VisualData, VisualDataCell } from "./types";

export function useVisualDataBound(): UseVisualDataBound {

    function updateRowPosition(visualData: VisualData, dataItem: any) {
        const preDataItem = visualData.pre;
        if (preDataItem) {
            const topPosition = (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0);
            visualData.top = topPosition;
            dataItem.__fv_data_position__ = topPosition;
        } else {
            visualData.top = 0;
            dataItem.__fv_data_position__ = 0;
        }
    }

    function updateVisualInfomation(vnode: any, targetCell: VisualDataCell, dataItem: any) {
        if (vnode) {
            const visualData = targetCell.parent as VisualData;
            if (targetCell.cellHeight !== vnode.offsetHeight) {
                targetCell.cellHeight = vnode.offsetHeight;
            }
            if (targetCell.cellHeight && targetCell.cellHeight > (visualData.height || 0)) {
                visualData.height = targetCell.cellHeight;
                dataItem.__fv_data_height__ = visualData.height;
            }
            updateRowPosition(visualData, dataItem);
        }
    }

    return { updateRowPosition, updateVisualInfomation };
}
