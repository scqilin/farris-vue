import { Ref, SetupContext, computed, ref } from "vue";
import { DataGridColumn, DataGridProps } from "../data-grid.props";
import { DataGridHeaderCell, UseColumn, UseDataView, UseDragColumn, UseGroupColumn, UseGroupData, UseVirtualScroll } from "./types";

export function useDragColumn(
    props: DataGridProps,
    context: SetupContext,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useGroupColumnComposition: UseGroupColumn,
    useGroupDataComposition: UseGroupData,
    useVirtualScrollComposition: UseVirtualScroll
): UseDragColumn {
    const { columnContext } = useColumnComposition;
    const { getGridHeaderCells } = useGroupColumnComposition;
    const { groupFields } = useGroupDataComposition;
    const draggingIndex = ref(-1);
    const isDragging = ref(false);

    const groupColumnItems = computed(() => {
        return groupFields.value && groupFields.value.map((groupField: string) => ({
            name: columnContext.value.primaryColumnsMap.get(groupField)?.title || groupField,
            value: groupField
        }));
    });

    function dragstart(e: DragEvent, targetItem: DataGridHeaderCell, index: number) {
        e.stopPropagation();
        if (targetItem) {
            const transferData = `${targetItem.column?.title}:${targetItem.column?.field}`;
            e.dataTransfer?.setData('Text', transferData);
            setTimeout(() => {
                draggingIndex.value = index;
                isDragging.value = true;
            });
        }
    }

    function dragenter(e: DragEvent, index: number) {
        e.preventDefault();
        if (draggingIndex.value !== index) {
            const { primaryColumns } = columnContext.value;
            const draggingColumn = primaryColumns[draggingIndex.value];
            const reOrderedColumns = primaryColumns;
            reOrderedColumns.splice(draggingIndex.value, 1);
            reOrderedColumns.splice(index, 0, draggingColumn);
            columnContext.value.primaryHeaderColumns = Array.from(getGridHeaderCells(reOrderedColumns).values());

            draggingIndex.value = index;
        }
    }
    function dragover(e: DragEvent, index: number) {
        e.preventDefault();
        if (e.dataTransfer) {
            e.dataTransfer.dropEffect = 'move';
        }
    }

    function dragend(e: DragEvent, targetItem: any) {
        isDragging.value = false;
        useDataViewComposition.updateDataView();
        useVirtualScrollComposition.reCalculateVisualDataRows();
    }

    function dropOnGroupPanel(e: DragEvent) {
        e.preventDefault();
        e.stopPropagation();
        const transtedData = e.dataTransfer?.getData('Text');
        if (transtedData) {
            const draggingData = transtedData.split(':');
            const name = draggingData[0];
            const value = draggingData[1];
            useGroupDataComposition.groupFields.value.push(value);
        }
        useDataViewComposition.updateDataView();
        useVirtualScrollComposition.reCalculateVisualDataRows();
    }

    return {
        dragstart,
        dragenter,
        dragover,
        dragend,
        dropOnGroupPanel,
        isDragging,
        groupColumnItems
    };
}
