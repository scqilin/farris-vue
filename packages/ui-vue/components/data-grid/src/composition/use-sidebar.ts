/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref } from 'vue';
import { DataGridProps } from '../data-grid.props';
import { UseSidebar, VisualData } from './types';

export function useSidebar(props: DataGridProps): UseSidebar {
    const defaultCheckboxWidth = 50;

    const showRowNumer = ref(props.rowNumber?.enable || false);
    const showRowCheckbox = ref(props.selection?.showCheckbox || false);

    const rowNumberWidth = ref(showRowNumer.value ? props.rowNumber?.width || 0 : 0);
    const checkboxWidth = ref(showRowCheckbox.value ? defaultCheckboxWidth : 0);

    const sidebarWidth = computed(() => {
        return checkboxWidth.value + rowNumberWidth.value;
    });

    function sidebarCellPosition(dataItem: VisualData): Record<string, any> {
        const styleObject = {
            top: `${dataItem.top}px`,
            width: `${sidebarWidth.value}px`,
            height: `${dataItem.height || ''}px`
        } as Record<string, any>;
        return styleObject;
    }

    const sidebarCornerCellStyle = computed(() => {
        const styleObject = {
            width: `${sidebarWidth.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    return { showRowCheckbox, showRowNumer, sidebarCellPosition, sidebarCornerCellStyle, sidebarWidth };
}
