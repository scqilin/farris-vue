import { ref } from "vue";
import { UseDataRow, UseSummaryRow, UseVisualDataCell, VisualData, VisualDataType } from "./types";
import { DataGridColumn, DataGridProps } from "../data-grid.props";

export function useVisualSummaryRow(
    props: DataGridProps,
    useVisualDataCell: UseVisualDataCell,
    useVisualDataRowComposition: UseDataRow
): UseSummaryRow {
    const identifyField = ref(props.idField);

    const { createCellByField } = useVisualDataCell;
    const { createEmptyRow } = useVisualDataRowComposition;

    function renderSummaryRow(
        dataItem: any, preDataItem: any, preRow: VisualData,
        rowIndex: number, top: number, columns: DataGridColumn[]
    ): VisualData {
        const groupField = dataItem.__fv_data_grid_group_field__;
        const currentRow: VisualData = createEmptyRow(VisualDataType.summary, rowIndex, dataItem, preDataItem, top);
        currentRow.data[identifyField.value] = createCellByField(identifyField.value, -1, dataItem, currentRow);
        currentRow.data[groupField] = createCellByField(groupField, 1, dataItem, currentRow, columns.length);
        return currentRow;
    }

    return { renderSummaryRow };
}
