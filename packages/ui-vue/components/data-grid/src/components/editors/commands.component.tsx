/* eslint-disable @typescript-eslint/indent */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { VisualData, VisualDataCell, VisualDataStatus } from '../../composition/types';
import { DataGridColumn, DataGridColumnCommand } from '../../data-grid.props';

export default function (cell: VisualDataCell, column: DataGridColumn, visualDataRow: VisualData) {

    function shouldShowCurrentCommandButton(command: DataGridColumnCommand) {
        switch (command.command) {
            case 'edit':
            case 'remove':
                return visualDataRow.status === VisualDataStatus.initial;
            case 'accept':
            case 'cancel':
                return visualDataRow.status === VisualDataStatus.editing;
            default:
                return !command.hidden;
        }
    }

    function excuteCommand(command: DataGridColumnCommand, payload: MouseEvent, visualDataRow: VisualData) {
        command.onClick(payload, visualDataRow.dataIndex, visualDataRow);
        switch (command.command) {
            case 'edit':
                visualDataRow.status === VisualDataStatus.editing;
                break;
            case 'accept':
            case 'cancel':
                visualDataRow.status === VisualDataStatus.initial;
                break;
        }
    }

    return <div>
        {
            column.commands && column.commands.map((command: DataGridColumnCommand) => {
                return shouldShowCurrentCommandButton(command) && <f-button
                    type={command.type}
                    onClick={(payload: MouseEvent) => excuteCommand(command, payload, visualDataRow)}
                > {command.text} </f-button>;
            })
        }
    </div>;
}
