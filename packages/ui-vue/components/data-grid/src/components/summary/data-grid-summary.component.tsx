/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref } from 'vue';
import { UseColumn, UseDataView } from '../../composition/types';
import { DataGridColumn, DataGridProps } from '../../data-grid.props';

export default function (props: DataGridProps, dataView: UseDataView, useColumnComposition: UseColumn) {
    const { columnContext } = useColumnComposition;

    const summaryOptions = ref(props.summary);

    const shouldShowSummary = computed(() => {
        const options = summaryOptions.value;
        return options && options.enable && options.groupFields && options.groupFields.length > 0;
    });

    function renderDataGridSummery() {
        return (
            shouldShowSummary.value && (
                <div class="fv-datagrid-summary">
                    <div class="fv-datagird-summary-panel">
                        <span class="fv-datagrid-summary-title">当页合计</span>
                        <div class="fv-datagrid-summary-content">
                            {columnContext.value.summaryColumns.map((column: DataGridColumn) => {
                                return (
                                    <div class="fv-datagrid-summary-field">
                                        <span class="fv-datagrid-summary-field-title">{`${column.title}:`}</span>
                                        <span class="fv-datagrid-summary-field-value">{dataView.summaries.get(column.field)}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            )
        );
    }

    return { renderDataGridSummery };
}
