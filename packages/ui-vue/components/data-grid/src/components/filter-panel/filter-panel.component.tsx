import { Ref, ref, watch } from 'vue';
import { DataGridHeaderCell, HeaderCellStatus, UseColumn, UseDataView, UseFilter, UseVirtualScroll } from '../../composition/types';
import { DataGridProps } from '../../data-grid.props';
import { Condition, FieldConfig } from '../../../../condition/src/types';

export default function (
    props: DataGridProps,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useFilterComposition: UseFilter,
    useVirtualScrollComposition: UseVirtualScroll
) {
    const { columnContext } = useColumnComposition;
    const { conditions, clearCondition, removeCondition } = useFilterComposition;
    const filterFields: Ref<FieldConfig[]> = ref([]);

    function updateFilters() {
        const filterValues: FieldConfig[] = [];
        Array.from(conditions.value).forEach((codition: Condition, index: number) => {
            const filterField = {
                id: codition.id,
                code: codition.fieldCode,
                labelCode: codition.fieldCode,
                name: codition.fieldName,
                editor: { type: codition.value.editorType }
            } as FieldConfig;
            filterValues.push(filterField);
        });
        filterFields.value = filterValues;
    }

    watch(conditions, () => updateFilters());

    function onRemoveFilter(removedFilterFieldCode: string) {
        removeCondition(`field_filter_${removedFilterFieldCode}`);
        const targetHeaderCell = columnContext.value.primaryHeaderColumns.find((headerCell: DataGridHeaderCell) => {
            return headerCell.field === removedFilterFieldCode;
        });
        if (targetHeaderCell) {
            targetHeaderCell.filterValue = null;
            let cellStatus = targetHeaderCell.status;
            cellStatus =
                (cellStatus & HeaderCellStatus.filtered) === HeaderCellStatus.filtered
                    ? cellStatus ^ HeaderCellStatus.filtered
                    : cellStatus;
            targetHeaderCell.status = cellStatus;
        }
        useDataViewComposition.refresh();
        useVirtualScrollComposition.reCalculateVisualDataRows();
    }

    function onResetFilter() {
        clearCondition();
        columnContext.value.primaryHeaderColumns.forEach((headerCell: DataGridHeaderCell) => {
            headerCell.filterValue = null;
            let cellStatus = headerCell.status;
            cellStatus =
                (cellStatus & HeaderCellStatus.filtered) === HeaderCellStatus.filtered
                    ? cellStatus ^ HeaderCellStatus.filtered
                    : cellStatus;
            headerCell.status = cellStatus;
        });
        useDataViewComposition.refresh();
        useVirtualScrollComposition.reCalculateVisualDataRows();
    }

    function renderFilterPanel() {
        return (
            <div class="fv-grid-filter-panel">
                <f-filter-bar
                    data={conditions.value}
                    fields={filterFields.value}
                    mode="display-only"
                    show-reset={true}
                    onRemove={onRemoveFilter}
                    onReset={onResetFilter}></f-filter-bar>
            </div>
        );
    }

    return { renderFilterPanel };
}
