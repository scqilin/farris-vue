/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref } from 'vue';
import { UseDataView, UseVirtualScroll } from '../../composition/types';
import { DataGridProps } from '../../data-grid.props';

export default function (props: DataGridProps, dataView: UseDataView, virtualScroll: UseVirtualScroll) {
    const pagination = ref(props.pagination);

    const totalItems = computed(() => {
        return props.data.length;
    });

    function onPageIndexChanged(pageIndex: number) {
        dataView.navigatePageTo(pageIndex);
        virtualScroll.resetScroll();
    }

    function onPageSizeChanged(newPageSize: number) {
        dataView.changePageSizeTo(newPageSize);
        virtualScroll.resetScroll();
    }

    function renderDataGridPagination() {
        return (
            pagination.value?.enable && <div class="fv-datagrid-pagination">
                <f-pagination
                    mode="default"
                    pageSize={pagination.value.size}
                    totalItems={totalItems.value}
                    onPageIndexChanged={onPageIndexChanged}
                    onPageSizeChanged={onPageSizeChanged}></f-pagination>
            </div>
        );
    }

    return { renderDataGridPagination };
}
