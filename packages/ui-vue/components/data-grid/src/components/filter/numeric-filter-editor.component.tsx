import { ref } from "vue";
import { DataGridHeaderCell } from "../../composition/types";

export default function (headerCell: DataGridHeaderCell) {
    headerCell.filter = headerCell.filter || function (dataItem: any) {
        const filterValue = Number.parseFloat(headerCell.filterValue);
        const matchingValue = Number.parseFloat(dataItem[headerCell.field]);
        return isNaN(filterValue) ? isNaN(matchingValue) : filterValue === matchingValue;
    };

    function onValueChange(newValue: any) {
        headerCell.filterValue = newValue;
    }

    return <div>
        <f-number-spinner can-null={true} precision="2" value={headerCell.filterValue} onValueChange={onValueChange}></f-number-spinner>
    </div>;
}
