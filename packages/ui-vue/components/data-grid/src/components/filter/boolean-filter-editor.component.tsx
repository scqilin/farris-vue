import { ref } from "vue";
import { DataGridHeaderCell } from "../../composition/types";
import { Checkbox } from "../../../../checkbox/src/composition/types";

const enumData: Checkbox[] = [
    { name: '是', value: true },
    { name: '否', value: false }
];
export default function (headerCell: DataGridHeaderCell) {

    headerCell.filter = headerCell.filter || function (dataItem: any) {
        const filterValues = String(headerCell.filterValue).split(',');
        return filterValues.includes(String(dataItem[headerCell.field]));
    };

    return <div>
        <f-checkbox-group v-model={headerCell.filterValue} enum-data={enumData}></f-checkbox-group>
    </div>;
}
