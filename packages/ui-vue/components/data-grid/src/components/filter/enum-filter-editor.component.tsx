import { Ref } from "vue";
import { DataGridHeaderCell, UseDataView, UseFilterHistory, UseVirtualScroll } from "../../composition/types";

export default function (
    headerCell: DataGridHeaderCell,
    gridContentRef: Ref<any>,
    rightFixedGridContentRef: Ref<any>,
    useDataViewComposition: UseDataView,
    useFilterHistoryComposition: UseFilterHistory,
    useVirtualScrollComposition: UseVirtualScroll
) {
    headerCell.filter = headerCell.filter || function (dataItem: any) {
        const filterValues = String(headerCell.filterValue).split(',');
        return filterValues.includes(String(dataItem[headerCell.field]));
    };

    const separator = ',';
    const originalValues = useDataViewComposition.rawView.value.reduce((values: any[], dataItem: any, index: number) => {
        values.push(dataItem[headerCell.field]);
        return values;
    }, []);
    const recommendedDataValues = [...new Set(originalValues)];
    const recommendedData = recommendedDataValues.map((value: any, index: number) => {
        return { id: index, name: value };
    });

    /**
     * 值到数组值的转换
     */
    function toSelectionValues(value: any): string[] {
        return value?.split(separator) || [];
    }

    /**
     * 值到字符串值的转换
     */
    function toSelectionString(selectionResult: any[]) {
        const selectionValues = selectionResult.map((selectionItem: any) => selectionItem.name)
            .join(separator);
        return selectionValues;
    }

    function onConfirmFilterItem(selectionResult: any[]) {
        headerCell.filterValue = toSelectionString(selectionResult);
    }

    return (
        <f-list-view data={recommendedData} multi-select={true} multi-select-mode='OnCheckAndClick'
            id-field="name" view="SingleView" header="Search" size="Small"
            selection-values={toSelectionValues(headerCell.filterValue)}
            onSelectChange={(result: any) => onConfirmFilterItem(result)}> </f-list-view>
    );
}
