/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseColumn, UseDataView, UseFilter, UseFitColumn, UseSort, UseVirtualScroll } from '../../composition/types';
import { DataGridProps } from '../../data-grid.props';
import { ModalService } from '../../../../modal';
import getDataGridSettingsRender from './data-grid-settings.component';
import { Ref } from 'vue';

export default function (
    props: DataGridProps,
    gridContentRef: Ref<any>,
    viewPortWidth: Ref<number>,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useFilterComposition: UseFilter,
    useFitColumnComposition: UseFitColumn,
    useSorterComposition: UseSort,
    useVirtualScrollComposition: UseVirtualScroll
) {
    function onClickColumnHandler(payload: MouseEvent) {
        const title = 'Settings';
        const width = 800;
        const showButtons = true;
        const showHeader = false;
        const {
            acceptCallback,
            rejectCallback,
            renderSettingsPanel: render
        } = getDataGridSettingsRender(
            props,
            gridContentRef,
            viewPortWidth,
            useColumnComposition,
            useDataViewComposition,
            useFilterComposition,
            useFitColumnComposition,
            useSorterComposition,
            useVirtualScrollComposition
        );
        ModalService.show({ title, width, showButtons, showHeader, render, acceptCallback, rejectCallback });
    }

    function renderGridSettingsIcon() {
        return (
            <span class="fv-grid-settings-icon" onClick={onClickColumnHandler}>
                <i class="f-icon f-icon-home-setup"></i>
            </span>
        );
    }

    return { renderGridSettingsIcon };
}
