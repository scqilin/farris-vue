/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref } from 'vue';
import { UseRow, UseSidebar, UseVirtualScroll, VisualData } from '../../composition/types';
import { DataGridProps } from '../../data-grid.props';

export default function (
    props: DataGridProps,
    useRowComposition: UseRow,
    useSidebarComposition: UseSidebar,
    useVirtualScrollComposition: UseVirtualScroll
) {
    const { onMouseoverRow, sidebarRowClass } = useRowComposition;
    const { showRowCheckbox, showRowNumer, sidebarCellPosition, sidebarWidth } = useSidebarComposition;
    const { gridSideStyle } = useVirtualScrollComposition;

    function renderSideBarCheckbox(visualData: VisualData) {
        return (
            <div class="custom-control custom-checkbox f-checkradio-single fv-grid-sidebar-row-checkbox">
                <input id={`${visualData.dataIndex}`} title={`${visualData.dataIndex}`} type="checkbox" class="custom-control-input" />
                <label class="custom-control-label" for={`${visualData.dataIndex}`}></label>
            </div>
        );
    }

    function renderSideBarRowNumber(dataItem: VisualData) {
        return (
            <div class="fv-grid-sidebar-row-number" onMouseover={(payload: MouseEvent) => onMouseoverRow(payload, dataItem)}>
                {dataItem.dataIndex}
            </div>
        );
    }

    function renderGridSideBar(visibleDatas: Ref<VisualData[]>) {
        return (
            (showRowCheckbox.value || showRowNumer.value) &&
            visibleDatas.value.map((dataItem: VisualData) => {
                return (
                    <div class={sidebarRowClass(dataItem)} style={sidebarCellPosition(dataItem)}>
                        {showRowCheckbox.value && renderSideBarCheckbox(dataItem)}
                        {showRowNumer.value && renderSideBarRowNumber(dataItem)}
                    </div>
                );
            })
        );
    }

    function renderDataGridSidebar(visibleDatas: Ref<VisualData[]>) {
        return (
            <div class="fv-grid-content-side">
                <div class="fv-grid-side" style={gridSideStyle.value}>
                    {renderGridSideBar(visibleDatas)}
                </div>
            </div>
        );
    }

    return { renderDataGridSidebar };
}
