/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { VisualData } from './composition/types';
import { EditorConfig } from '../../dynamic-form';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import dataGridSchema from './schema/data-grid.schema.json';

export type DataGridAlignment = 'left' | 'center' | 'right';

export type DataGridVerticalAlignment = 'top' | 'middle' | 'bottom';

export type SortType = 'asc' | 'desc' | 'none';

export interface DataGridColumnCommand {
    type: string;
    text: string;
    command?: string;
    icon?: string;
    hidden?: boolean;
    onClick: (e: MouseEvent, dataIndex: number, visualDataRow: VisualData) => void;
}

export interface DataGridColumn {
    dataType: string;
    editor?: EditorConfig;
    id?: string;
    parentId?: any;
    // 字段
    field: string;
    // 标题
    title: string;
    // 宽度
    width?: number | string;
    /** 记录原始定义宽度 */
    actualWidth?: number;
    /** 标题对齐方式 */
    halign?: 'left' | 'center' | 'right';
    /** 文本对齐方式 */
    align?: 'left' | 'center' | 'right';
    /** 垂直对齐方式 */
    valign?: 'top' | 'middle' | 'bottom';
    left?: number;
    /** 是否固定 */
    fixed?: 'left' | 'right';
    /** 是否显示 */
    visible?: boolean | any;
    /** 只读 */
    readonly?: boolean | any;
    /** 鼠标移动至单元格后，显示悬浮消息 */
    showTips?: boolean;
    /** 单元格提示模式：
     *  allways： 鼠标滑过即显示
     *  auto: 单元格宽度不够时才会显示
     */
    tipMode?: 'allways' | 'auto';
    /** True to allow the column can be sorted. */
    sortable?: boolean;
    sort?: SortType;
    sortOrder?: number;
    sorter?: (preValue: any, postValue: any, sortType: SortType) => number;
    /** True to allow the column can be resized. */
    resizable?: boolean;
    rowspan?: number;
    /** 列合并 */
    colspan?: number;
    /** 列合并原始值  */
    origianlColSpan?: number;
    index?: number;
    /** 允许分组，默认为 true */
    allowGrouping?: boolean;
    /** 是否多语字段 */
    isMultilingualField?: boolean;
    /** 操作列命令 */
    commands?: DataGridColumnCommand[];
    /** True to allow the column can be filtered. */
    filterable?: boolean;
    filter?: string;
    showSetting?: boolean;
}

export interface ColumnGroupItem {
    field: string;
    title?: string;
    group?: (ColumnGroupItem | string)[];
}
// export type DataGridColumnProps = ExtractPropTypes<typeof dataGridColumnProps>;

export type InteractiveMode = 'server' | 'client';

export const paginationOptions = {
    /** 启用分页 */
    enable: { type: Boolean, default: false },
    /** 当前页码 */
    index: { type: Number, default: 1 },
    /** 分页交互模式 */
    mode: { type: String as PropType<InteractiveMode>, default: 'server' },
    /** 显示页码输入框 */
    showGoto: { type: Boolean, default: false },
    /** 显示页码 */
    showIndex: { type: Boolean, default: true },
    /** 显示每页记录数 */
    showLimits: { type: Boolean, default: false },
    /** 显示分页汇总信息 */
    showPageInfo: { type: Boolean, default: true },
    /** 默认每页记录数 */
    size: { type: Number, default: 20 },
    /** 可选择的没有记录数据 */
    sizeLimits: { type: Array<number>, default: [10, 20, 30, 50, 100] },
    /** 总记录数 */
    total: { type: Number, default: 0 }
};

export type PaginatonOptions = ExtractPropTypes<typeof paginationOptions>;

export type DataGridSortOrder = 'asc' | 'desc';

export interface DataGridSortField {
    /** 排序字段名 */
    name: string;
    /** 排序方式 */
    order: DataGridSortOrder;
}

export const sortOptions = {
    /** 启用排序 */
    enable: { type: Boolean, default: false },
    /** 排序字段集合 */
    fields: { type: Array<DataGridSortField>, default: [] },
    /** 排序交互模式 */
    mode: { type: String as PropType<InteractiveMode>, default: 'client' },
    /** 多列排序 */
    multiSort: { type: Boolean, default: false }
};

export type SortOptions = ExtractPropTypes<typeof sortOptions>;

export type GroupSummaryPosition = 'merge-to-group' | 'separate';

export const groupOptions = {
    /** 自定义分组合计内容 */
    customGroupRow: { type: Function, default: () => { } },
    /** 自定义分组行样式 */
    customGroupRowStyle: { type: Function, default: () => { } },
    /** 自行分组合计行样式 */
    customSummaryStyle: { type: Function, default: () => { } },
    /** 启用行数据分组 */
    enable: { type: Boolean, default: false },
    /** 分组行合并列数 */
    groupColSpan: { type: Number, default: 1 },
    /** 行数据分组字段；多字段分组以英文逗号分隔 */
    groupFields: { type: Array<string>, default: [] },
    /** 在DataGrid中显示被分组的列 */
    showGroupedColumn: { type: Boolean, default: true },
    /** 显示分组面板 */
    showGroupPanel: { type: Boolean, default: false },
    /** 启用合计行 */
    showSummary: { type: Boolean, default: false },
    /** 显示合计行位置； */
    summaryPosition: { type: String as PropType<GroupSummaryPosition>, default: 'separate' }
};

export type GroupOptions = ExtractPropTypes<typeof groupOptions>;

export type DataGridFilterStyle = 'filter-column' | 'filter-row';

export const filterOptions = {
    /** 启用筛选 */
    enable: { type: Boolean, default: false },
    /** DataGrid筛选风格 */
    filterStyle: { type: String as PropType<DataGridFilterStyle>, default: 'filter-column' },
    /** 筛选交互模式 */
    mode: { type: String as PropType<InteractiveMode>, default: 'server' },
    /** 显示过滤条件工具条 */
    showSummary: { type: Boolean, default: true }
};

export type FilterOptions = ExtractPropTypes<typeof filterOptions>;

export const headerOptions = {
    /** 允许折行显示列标题 */
    wrapHeadings: { type: Boolean, default: false }
};

export type HeaderOptions = ExtractPropTypes<typeof headerOptions>;

export const rowNumberOptions = {
    /** 显示行号 */
    enable: { type: Boolean, default: false },
    /** 行号列表头标题 */
    heading: { type: String, default: '序号' },
    /** 行号宽度，默认为 36px */
    width: { type: Number, default: 36 }
};

export type RowNumberOptions = ExtractPropTypes<typeof rowNumberOptions>;

export const rowOptions = {
    /** 自定义行样式 */
    customRowStyle: { type: Function, default: () => { } },
    /** 禁止行选中表达式 */
    disable: { type: Function, default: () => { } },
    /** 默认行高度为 29px */
    height: { type: Number, default: 28 },
    /** 鼠标滑过行效果 */
    showHovering: { type: Boolean, default: true },
    /** 禁止数据折行 */
    wrapContent: { type: Boolean, default: false }
};

export type RowOptions = ExtractPropTypes<typeof rowOptions>;

export type DataGridSummaryPosition = 'bottom' | 'top' | 'both';

export const summaryOptions = {
    /** 显示合计信息 */
    enable: { type: Boolean, default: false },
    /** 合计行自定义样式，对启用合计行模板无效 */
    customSummaryStyle: { type: Function, default: () => { } },
    /** 分组合计字段 */
    groupFields: { type: Array<string>, default: [] },
    /** 合计交互模式 */
    mode: { type: String as PropType<InteractiveMode>, default: 'client' },
    /** 合计行显示位置， top: 顶部 bottom: 底部， both: 顶部与底部同时显示 */
    position: { type: String as PropType<DataGridSummaryPosition>, default: 'bottom' }
};

export type SummaryOptions = ExtractPropTypes<typeof summaryOptions>;

export type DataGridColumnFitMode = 'none' | 'average' | 'expand' | 'percentage';

export const columnOptions = {
    /** 自动列宽。设为true后，所有列将填满表格并不会出现横向滚动条。 */
    fitColumns: { type: Boolean, default: false },
    /** 自动适配列宽度模式 */
    fitMode: { type: String as PropType<DataGridColumnFitMode>, default: 'average' },
    groups: { type: Array<ColumnGroupItem>, defaut: [] },
    /** 允许拖动表头改变列显示顺序 */
    reorderColumn: { type: Boolean, default: false },
    /** 允许拖动改变列宽度 */
    resizeColumn: { type: Boolean, default: true },
    /** 双击表头列自适应内容宽度 */
    resizeColumnOnDoubleClick: { type: Boolean, default: true }
};

export type ColumnOptions = ExtractPropTypes<typeof columnOptions>;

export type FocuseSelectionMode = 'current' | 'all';

export const selectionOptions = {
    /** 启用多选且显示checkbox, 选中行后勾选前面的checkbox */
    checkOnSelect: { type: Boolean, default: false },
    /** 当数据源为空时，清空已选记录 */
    clearSelectionOnEmpty: { type: Boolean, default: true },
    /** 自定义已选记录列表中的显示内容 */
    customSelectionItem: { type: Function, default: () => { } },
    /** 允许选中行 */
    enabelSelectRow: { type: Boolean, default: true },
    /** 允许重复点击行是保留选中状态 */
    keepSelectingOnClick: { type: Boolean, default: true },
    /** 允许跨页多选 */
    keepSelectingOnPaging: { type: Boolean, default: true },
    /** 启用多选 */
    multiSelect: { type: Boolean, default: false },
    /** 启用多选时，点击行选中，只允许且只有一行被选中 */
    focusSelection: { type: String as PropType<FocuseSelectionMode>, default: 'current' },
    /** 启用多选且显示checkbox, 勾选后并且选中行 */
    selectOnCheck: { type: Boolean, default: false },
    /** 每行前边显示 checkbox */
    showCheckbox: { type: Boolean, default: false },
    /** 显示全选checkbox */
    showSelectAll: { type: Boolean, default: false },
    /** 显示已选数据 */
    showSelection: { type: Boolean, default: false }
};

export type SelectionOptions = ExtractPropTypes<typeof selectionOptions>;

export type DataGridEditMode = 'cell' | 'row';

export const editOptions = {
    /** 编辑时选中文本 */
    selectOnEditing: { type: Boolean, default: false },
    /** 编辑模式； row：整行编辑，cell: 单元格编辑 */
    editMode: { type: String as PropType<DataGridEditMode>, default: 'cell' }
};

export type EditOption = ExtractPropTypes<typeof editOptions>;

export const dataGridProps = {
    /** 列集合 */
    columns: { type: Array<DataGridColumn>, default: [] },
    /** 列配置 */
    columnOption: { type: Object as PropType<ColumnOptions> },
    /** 被绑定数据 */
    data: { type: Array<object>, default: [] },
    /** 禁用组件 */
    disable: { type: Boolean, default: false },
    /** 允许编辑 */
    editable: { type: Boolean, default: false },
    /** 编辑配置 */
    editOption: { type: Object as PropType<EditOption>, default: { selectOnEditing: false, editMode: 'cell' } },
    /** 筛选配置 */
    filter: { type: Object as PropType<FilterOptions> },
    /** 适配父组件尺寸 */
    fit: { type: Boolean, default: false },
    /** 分组配置 */
    group: { type: Object as PropType<GroupOptions> },
    /** 列标题配置 */
    header: { type: Object as PropType<HeaderOptions> },
    /** 高度 */
    height: { type: Number, default: 300 },
    /** DataGrid组件唯一标识 */
    id: { type: String, default: '' },
    /** 被绑定数据的标识字段 */
    idField: { type: String, default: 'id', require: true },
    /** 纵向合并具有相同值的单元格 */
    mergeCell: { type: Boolean, default: false },
    /** 分页配置 */
    pagination: { type: Object as PropType<PaginatonOptions> },
    /** 行号配置 */
    rowNumber: { type: Object as PropType<RowNumberOptions> },
    /** 行配置 */
    rowOption: { type: Object as PropType<RowOptions> },
    /** 选择配置 */
    selection: { type: Object as PropType<SelectionOptions> },
    /** 显示边框 */
    showBorder: { type: Boolean, default: false },
    /** 显示底部面板 */
    showFooter: { type: Boolean, default: false },
    /** 显示顶部面板 */
    showHeader: { type: Boolean, default: true },
    /** 显示配置按钮 */
    showOptions: { type: Boolean, default: false },
    /** 显示滚动条 */
    showScrollBar: { type: String, default: 'auto' },
    /** 显示条纹 */
    showStripe: { type: Boolean, default: true },
    /** 排序配置 */
    sort: { type: Object as PropType<SortOptions> },
    /** 合计配置 */
    summary: { type: Object as PropType<SummaryOptions> },
    /** 启用虚拟渲染 */
    virtualized: { type: Boolean, default: false },
    /** 宽度 */
    width: { type: Number, default: 800 },
    /** 新建数据 */
    newDataItem: {
        type: Function as PropType<(...args: unknown[]) => any>, default: () => { }
    },
    enableCommands: { type: Boolean, default: false },
    commands: { type: Array<DataGridColumnCommand>, default: [] }
} as Record<string, any>;

export type DataGridProps = ExtractPropTypes<typeof dataGridProps>;

export const propsResolver = createPropsResolver<DataGridProps>(dataGridProps, dataGridSchema, schemaMapper);
