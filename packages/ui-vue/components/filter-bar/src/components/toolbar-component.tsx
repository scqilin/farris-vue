import { SetupContext, computed, ref } from 'vue';
import { FilterBarProps } from '../filter-bar.props';
import { UseFilterItems } from '../composition/types';

export default function (props: FilterBarProps, context: SetupContext, useFilterItemsComposition: UseFilterItems) {
    const collapsedFilterList = ref([]);
    const enableFloat = ref(false);
    const advancedFilterText = ref('');
    const resetText = ref(props.resetText);
    const shownResetButton = ref(props.showReset);
    const { clearAll, reset } = useFilterItemsComposition;

    const shouldShowAdvancedFilter = computed(() => {
        return enableFloat.value && collapsedFilterList.value && collapsedFilterList.value.length > 0;
    });

    const shouldShowResetButton = computed(() => {
        return shownResetButton.value;
    });

    function onClickAdvancedFilter($event: MouseEvent) {}

    function onClickReset($event: MouseEvent) {
        if (props.mode === 'display-only') {
            clearAll();
        } else {
            reset();
        }
    }

    return () => {
        return (
            <div class="f-filter-toolbars">
                {shouldShowAdvancedFilter.value && (
                    <button class="btn btn-link" onClick={(payload: MouseEvent) => onClickAdvancedFilter(payload)}>
                        {advancedFilterText.value}
                    </button>
                )}
                {shouldShowResetButton.value && (
                    <button class="btn btn-link" onClick={(payload: MouseEvent) => onClickReset(payload)}>
                        {resetText.value}
                    </button>
                )}
            </div>
        );
    };
}
