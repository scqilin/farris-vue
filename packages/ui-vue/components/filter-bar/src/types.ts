import { EditorConfig } from "../../dynamic-form";

export interface FilterItem {
    id: string;
    fieldCode: string;
    fieldName: string;
    editor: EditorConfig;
    displayText?: string;
    hasValue?: boolean;
};
