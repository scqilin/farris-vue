import { Ref } from "vue";
import { FilterItem } from "../types";
import { Condition, FieldConfig } from "../../../condition/src/types";

export interface UseFilterItems {

    clearAll: () => void;

    clearFilterItem: (itemToBeRemoved: FilterItem) => void;

    currentFilterId: Ref<string>;

    filterFields: Ref<FieldConfig[]>;

    filterItems: Ref<FilterItem[]>;

    loadFilterItems: (fields: FieldConfig[], conditions: Condition[]) => void;

    removeFilterItem: (itemToBeRemoved: FilterItem) => void;

    reset: () => void;

    shouldShowClearButtonInFilterItem: (filterItem: FilterItem) => boolean;
}
