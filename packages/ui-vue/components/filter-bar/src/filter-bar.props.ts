import { ExtractPropTypes, PropType } from "vue";
import { Condition, FieldConfig } from "../../condition/src/types";

export type FilterMode = 'editable' | 'display-only';

export const filterBarProps = {
    /** 被绑定数据 */
    data: { type: Array<Condition>, default: [] },
    fields: { type: Array<FieldConfig>, default: [] },
    mode: { type: String as PropType<FilterMode>, default: 'editable' },
    resetText: { type: String, default: '清空已选' },
    showReset: { type: Boolean, default: false },
};

export type FilterBarProps = ExtractPropTypes<typeof filterBarProps>;
