import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { FilterBarProps, filterBarProps } from './filter-bar.props';
import { FilterItem } from './types';
import { useFilterItems } from './composition/use-filter-items';
import getEllipsisButtonRender from './components/ellipsis-button.component';
import getFilterItemRender from './components/filter-item.component';
import getToolbarRender from './components/toolbar-component';

import './filter-bar.css';

export default defineComponent({
    name: 'FFilterBar',
    props: filterBarProps,
    emits: ['Clear', 'Remove', 'Reset'],
    setup(props: FilterBarProps, context: SetupContext) {
        const enableExpand = ref(false);
        const expanded = ref(false);
        const shownEllipsis = ref(false);
        const shownCollapseButton = ref(false);
        const useFilterItemsComposition = useFilterItems(props, context);
        const { filterFields, filterItems, loadFilterItems } = useFilterItemsComposition;

        const filterWrapperStyle = computed(() => {
            const styleObject = {
                display: filterFields.value && filterFields.value.length ? '' : 'none'
            } as Record<string, any>;
            return styleObject;
        });

        watch([() => props.data, () => props.fields], ([latestData, latestFields]) => {
            loadFilterItems(latestFields, latestData);
        });

        const shouldShowExpandedArea = computed(() => expanded.value && enableExpand.value);
        const filterListClass = computed(() => {
            const classObject = {
                'f-filter-list': true,
                'f-filter-list-extend': shouldShowExpandedArea.value
            } as Record<string, boolean>;
            return classObject;
        });

        const shouldShowFilterItems = computed(() => filterItems.value.length > 0);
        const renderFilterItem = getFilterItemRender(props, context, useFilterItemsComposition);
        function renderFilterItems() {
            return (
                <div class={filterListClass.value}>
                    {filterItems.value.map((data: FilterItem, index: number) => {
                        return renderFilterItem(data, index);
                    })}
                </div>
            );
        }
        const shouldShowEllipsis = computed(() => shownEllipsis.value && !expanded.value && shownCollapseButton.value);
        const renderEllipsisButton = getEllipsisButtonRender(props, context);

        const shouldShowToolbar = computed(() => !expanded.value);
        const renderToolbar = getToolbarRender(props, context, useFilterItemsComposition);

        return () => {
            return (
                <div class="f-filter-wrapper" style={filterWrapperStyle.value}>
                    <div class="f-filter-wrapper-inner f-utils-fill">
                        <div class="f-filter-main f-utils-fill">
                            <div class="f-filter-list-wrapper">
                                {shouldShowFilterItems.value && renderFilterItems()}
                                {shouldShowEllipsis.value && renderEllipsisButton()}
                                {shouldShowToolbar.value && renderToolbar()}
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
    }
});
