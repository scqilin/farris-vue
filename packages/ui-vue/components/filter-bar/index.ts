import type { App } from 'vue';
import FilterBar from './src/filter-bar.component';

export * from './src/types';
export * from './src/filter-bar.props';

export default {
    install(app: App): void {
        app.component(FilterBar.name, FilterBar);
    }
};
