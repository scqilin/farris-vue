// 此处需按顺序赋予数据ID
export const data = [
    {
        id: '1',
        avatar: 'ESG',
        code: 'ESG',
        name: '浪潮数字企业',
        description: 'Created the farris vue project.',
        parent: '',
        layer: 0,
    },
    {
        id: '2',
        avatar: 'P&T',
        code: 'Platform And Technology Department',
        name: '平台与技术部',
        description: 'Created the farris vue project.',
        parent: '1',
        layer: 1
    },
    {
        id: '3',
        avatar: 'LCP',
        code: 'Low Code Development Department',
        name: '低代码平台研发部',
        description: 'Created the farris vue project.',
        parent: '2',
        layer: 2
    },
    {
        id: '4',
        avatar: 'FR',
        code: 'Front-end Team',
        name: '前端研发组',
        description: 'Created the farris vue project.',
        parent: '3',
        layer: 3
    },
    {
        id: '5',
        avatar: 'sagi',
        code: '0001',
        name: 'Sagi',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '6',
        avatar: 'aalizzwell',
        code: '0002',
        name: 'Aalizzwell',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '7',
        avatar: 'cassiel',
        code: '0003',
        name: 'Cassiel',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '8',
        avatar: 'sam',
        code: '0004',
        name: 'Sam',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '9',
        avatar: 'nancy',
        code: '0005',
        name: 'Nancy',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '10',
        avatar: 'BR',
        code: 'Back-end Team',
        name: '后端研发组',
        description: 'Created the backend project.',
        parent: '3',
        layer: 3
    },
    {
        id: '11',
        avatar: 'Jack',
        code: '0001',
        name: 'Jack',
        description: 'Created the backend01 project.',
        parent: '10',
        layer: 4
    },
];

export const complexData = [
    {
        id: '1',
        avatar: 'ESG',
        code: 'ESG',
        name: '浪潮数字企业',
        description: 'Created the farris vue project.',
        parent: '',
        layer: 0,
        checked: true
    },
    {
        id: '2',
        avatar: 'P&T',
        code: 'Platform And Technology Department',
        name: '平台与技术部',
        description: 'Created the farris vue project.',
        parent: '1',
        layer: 1,
        editable: false,
        deletable: false
    },
    {
        id: '3',
        avatar: 'LCP',
        code: 'Low Code Development Department',
        name: '低代码平台研发部',
        description: 'Created the farris vue project.',
        parent: '2',
        layer: 2,
        editable: false,
        deletable: false
    },
    {
        id: '4',
        avatar: 'FR',
        code: 'Front-end Team',
        name: '前端研发组',
        description: 'Created the farris vue project.',
        parent: '3',
        layer: 3
    },
    {
        id: '5',
        avatar: '成员A',
        code: '0001',
        name: '成员A',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '6',
        avatar: '成员B',
        code: '0002',
        name: '成员B',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '7',
        avatar: '成员C',
        code: '0003',
        name: '成员C',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '8',
        avatar: '成员D',
        code: '0004',
        name: '成员D',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '9',
        avatar: '成员E',
        code: '0005',
        name: '成员E',
        description: 'Created the farris vue project.',
        parent: '4',
        layer: 4
    },
    {
        id: '10',
        avatar: 'BR',
        code: 'Back-end Team',
        name: '后端研发组',
        description: 'Created the backend project.',
        parent: '3',
        layer: 3
    },
    {
        id: '11',
        avatar: '成员F',
        code: '0001',
        name: '成员F',
        description: 'Created the backend01 project.',
        parent: '10',
        layer: 4
    },
    {
        id: '12',
        avatar: 'ESG',
        code: 'ESG',
        name: '云计算平台部',
        description: 'Created the farris vue project.',
        parent: '1',
        layer: 1
    },
    {
        id: '13',
        avatar: 'FD',
        code: 'Finance Department',
        name: '金融部',
        description: 'Created the farris vue project.',
        parent: '12',
        layer: 2
    },
    {
        id: '14',
        avatar: 'LCP',
        code: 'Low Code Development Department',
        name: '研发部',
        description: 'Created the farris vue project.',
        parent: '12',
        layer: 2
    },
    {
        id: '15',
        avatar: 'FR',
        code: 'Front-end Team',
        name: '前端研发组',
        description: 'Created the farris vue project.',
        parent: '14',
        layer: 3
    },
    {
        id: '16',
        avatar: '',
        code: '0001',
        name: '成员G',
        description: 'Created the farris vue project.',
        parent: '15',
        layer: 4
    },
    {
        id: '17',
        avatar: 'LCP',
        code: 'check1',
        name: '智能化开发部',
        description: 'Created the farris vue project.',
        parent: '1',
        layer: 1
    },
    {
        id: '18',
        avatar: 'FR',
        code: 'check2',
        name: '研发部',
        description: 'Created the farris vue project.',
        parent: '17',
        layer: 2
    },
    {
        id: '19',
        avatar: 'check3',
        code: '0001',
        name: '生产部',
        description: 'Created the farris vue project.',
        parent: '17',
        layer: 2
    },
];

/** 树节点图标 */
export const treeNodeIconsData = {
    // 折叠状态
    fold: 'f-icon f-icon-folder',
    // 展开状态
    unfold: 'f-icon f-icon-folder-open',
    // 叶子节点
    leafnodes: 'f-icon f-icon-file',
};
