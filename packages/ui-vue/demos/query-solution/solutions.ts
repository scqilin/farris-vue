/* eslint-disable max-len */
const fieldConfigs = [
    {
        id: 'e605c9f9-a252-48ca-9c65-0bffb3427e86',
        labelCode: 'id',
        code: 'id',
        name: '标识',
        placeHolder: '',
        editor: {
            id: 'e605c9f9-a252-48ca-9c65-0bffb3427e86',
            type: 'button-edit',
            require: false,
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            }
        }
    },
    {
        id: '2691b91b-1b5e-4eca-b802-ff5cc2ee7856',
        labelCode: 'Version',
        code: 'Version',
        name: '版本',
        placeHolder: '',
        editor: {
            id: '2691b91b-1b5e-4eca-b802-ff5cc2ee7856',
            type: 'date-picker',
            require: false,
            format: 'yyyy-MM-dd',
            weekSelect: false,
            startFieldCode: 'Version',
            endFieldCode: 'Version'
        }
    },
    {
        id: '2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a',
        labelCode: 'EmployeeID.EmployeeID',
        code: 'EmployeeID',
        name: '创建人',
        placeHolder: '',
        editor: {
            type: 'lookup',
            require: false,
            uri: '',
            textField: 'name',
            valueField: 'id',
            idField: 'id',
            helpId: '915a0b20-975a-4df1-8cfd-888c3dda0009',
            displayType: 'List',
            loadTreeDataType: 'default',
            enableFullTree: false,
            editable: false,
            mapFields: { code: 'employeeID.employeeID_code', name: 'employeeID.employeeID_name' },
            singleSelect: true,
            pageSize: 20,
            pageList: '10,20,30,50',
            expandLevel: -1,
            context: {
                enableExtendLoadMethod: true
            }
        }
    },
    {
        id: '387679f1-4f61-40f3-8286-b3b55219a7aa',
        labelCode: 'EmployeeID.EmployeeID_code',
        code: 'code',
        name: '创建人工号',
        placeHolder: '',
        editor: {
            id: '387679f1-4f61-40f3-8286-b3b55219a7aa',
            type: 'input-group',
            require: false,
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            }
        }
    },
    {
        id: 'f2878e6d-99a2-45e7-8678-3635b1e0ec2a',
        labelCode: 'EmployeeID.EmployeeID_name',
        code: 'name',
        name: '创建人',
        placeHolder: '',
        editor: {
            id: 'f2878e6d-99a2-45e7-8678-3635b1e0ec2a',
            type: 'button-edit',
            require: false,
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            }
        }
    },
    {
        id: '393c7ac7-64b2-4888-83a9-c1d8b2755339',
        labelCode: 'DomainID.DomainID',
        code: 'DomainID',
        name: '业务部门',
        placeHolder: '',
        editor: {
            type: 'lookup',
            require: false,
            uri: '',
            textField: 'name',
            valueField: 'id',
            idField: 'id',
            helpId: 'b524a702-7323-4d46-998e-5ba0c6abcd49',
            displayType: 'TreeList',
            loadTreeDataType: 'default',
            enableFullTree: false,
            editable: false,
            dialogTitle: '',
            singleSelect: true,
            enableCascade: false,
            cascadeStatus: 'enable',
            pageSize: 20,
            pageList: '10,20,30,50',
            nosearch: false,
            expandLevel: -1,
            context: {
                enableExtendLoadMethod: true
            },
            quickSelect: {
                enable: false,
                showMore: true,
                showItemsCount: 10
            }
        }
    },
    {
        id: '138082d4-9b07-4da2-9858-73861e0ac127',
        labelCode: 'DomainID.DomainID_code',
        code: 'code',
        name: '部门编号',
        placeHolder: '',
        editor: {
            id: '138082d4-9b07-4da2-9858-73861e0ac127',
            type: 'input-group',
            require: false,
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            }
        }
    },
    {
        id: '99587da0-8c9d-4efb-a196-f5762ba6000e',
        labelCode: 'DomainID.DomainID_name',
        code: 'name',
        name: '业务部门',
        placeHolder: '',
        editor: {
            id: '99587da0-8c9d-4efb-a196-f5762ba6000e',
            type: 'button-edit',
            require: false,
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            }
        }
    },
    {
        id: 'd9ace6f2-a77a-4b2b-9b3c-61154bb40848',
        labelCode: 'BillCode',
        code: 'BillCode',
        name: '单据编号',
        placeHolder: '',
        editor: {
            id: 'd9ace6f2-a77a-4b2b-9b3c-61154bb40848',
            tpye: 'input-group',
            require: false,
            editable: true,
            groupText: '',
            usageMode: 'text',
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            },
            groupTextUseLang: true
        }
    },
    {
        id: '3a8403c1-aa75-4091-947e-53f1d2bb8d4d',
        labelCode: 'TotalSum',
        code: 'TotalSum',
        name: '订单金额',
        placeHolder: '',
        editor: {
            id: '3a8403c1-aa75-4091-947e-53f1d2bb8d4d',
            type: 'number-spinner',
            require: false,
            textAlign: 'left',
            precision: 2,
            isBigNumber: false,
            maxValue: 100000,
            minValue: -100000
        }
    },
    {
        id: 'c7dbe607-bddc-4391-92e2-16f0221765df',
        labelCode: 'BillType',
        code: 'BillType',
        name: '订单类型',
        placeHolder: '',
        editor: {
            type: 'combo-list',
            require: false,
            valueType: '1',
            multiSelect: true,
            data: [
                {
                    value: '1',
                    name: '1'
                },
                {
                    value: '2',
                    name: '2'
                },
                {
                    value: '3',
                    name: '3'
                }
            ]
        }
    },
    {
        id: 'd152e48d-13d1-4553-94fa-525fa67d4f2b',
        labelCode: 'BillDate',
        code: 'BillDate',
        name: '创建日期',
        placeHolder: '',
        editor: {
            id: 'd152e48d-13d1-4553-94fa-525fa67d4f2b',
            type: 'date-picker',
            require: false,
            format: 'yyyy-MM-dd',
            weekSelect: false,
            startFieldCode: 'BillDate',
            endFieldCode: 'BillDate'
        }
    },
    {
        id: 'abb7ef8e-ef19-4d83-99cc-1b7ff4f05345',
        labelCode: 'SecID',
        code: 'SecID',
        name: '密级',
        placeHolder: '',
        editor: {
            type: 'combo-list',
            require: false,
            valueType: '1',
            multiSelect: false,
            data: [
                {
                    value: 'a',
                    name: 'a'
                },
                {
                    value: 'b',
                    name: 'b'
                },
                {
                    value: 'c',
                    name: 'c'
                },
                {
                    value: 'd',
                    name: 'd'
                },
                {
                    value: 'e',
                    name: 'e'
                }
            ]
        }
    },
    {
        id: '219c9c45-3bfb-4482-8755-1ea0d3c9475c',
        labelCode: 'SecLevel',
        code: 'SecLevel',
        name: '密级',
        placeHolder: '',
        editor: {
            id: '219c9c45-3bfb-4482-8755-1ea0d3c9475c',
            type: 'number-range',
            require: false,
            textAlign: 'left',
            precision: 0,
            isBigNumber: false
        }
    },
    {
        id: '01ca22ef-fbd4-4322-8e88-503edb9e071c',
        labelCode: 'ProjectID',
        code: 'ProjectID',
        name: '所属项目',
        placeHolder: '',
        editor: {
            id: '01ca22ef-fbd4-4322-8e88-503edb9e071c',
            type: 'button-edit',
            require: false,
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            }
        }
    },
    {
        id: 'd9b196a3-2bf0-46ba-8807-22f6ac62ad78',
        labelCode: 'ProjectMrg',
        code: 'ProjectMrg',
        name: '项目经理',
        placeHolder: '',
        editor: {
            id: 'd9b196a3-2bf0-46ba-8807-22f6ac62ad78',
            type: 'input-group',
            require: false,
            modalConfig: {
                modalCmp: null,
                mapFields: null,
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            }
        }
    },
    {
        id: '6f1bf2f5-7afc-42c5-81ee-b547db370be9',
        labelCode: 'AuditStatus',
        code: 'AuditStatus',
        name: '订单状态',
        placeHolder: '',
        editor: {
            type: 'radio-group',
            require: false,
            valueType: '1',
            horizontal: true,
            showLabel: false,
            enumData: [
                {
                    value: '1',
                    name: '1'
                },
                {
                    value: '2',
                    name: '2'
                }
            ]
        }
    }
];

const solutions = [
    {
        id: 'c5df9edf-0e74-04b2-efb5-54572e1ab2e9',
        code: 'Default',
        name: '默认筛选条件',
        mode: '1',
        type: 'preset',
        conditions: [
            {
                id: '2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a',
                fieldCode: 'EmployeeID.EmployeeID',
                fieldName: '制单人',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: {
                    type: 13,
                    content: {
                        valueField: 'id',
                        value: [
                            {
                                id: '05701277-d0cf-bae1-a056-1d5ebe8ea9fa',
                                code: 'Sagi',
                                name: 'Sagi',
                                userGroup: '0',
                                sysOrgId: '001',
                                tenantId: 10000,
                                secLevel: '0',
                                userType: 0,
                                note: null
                            }
                        ],
                        textValue: 'Sagi'
                    }
                }
            },
            {
                id: '393c7ac7-64b2-4888-83a9-c1d8b2755339',
                fieldCode: 'DomainID.DomainID',
                fieldName: '业务部门',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: {
                    type: 2,
                    content: {
                        value: [
                            {
                                id: 'c4427472-95c6-e2c5-c3aa-ef6d537a1b33',
                                code: 'Inspur',
                                name: '浪潮集团',
                                treeinfo: { parentElement: '', sequence: 2, layer: 1, isDetail: false }
                            }
                        ],
                        valueField: 'id',
                        textValue: '浪潮集团',
                        isInputText: false
                    }
                }
            },
            {
                id: 'd9ace6f2-a77a-4b2b-9b3c-61154bb40848',
                fieldCode: 'BillCode',
                fieldName: '单据编号',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 15, content: { value: [], textValue: '', isInputText: true } }
            },
            {
                id: 'c7dbe607-bddc-4391-92e2-16f0221765df',
                fieldCode: 'BillType',
                fieldName: '订单类型',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 3, content: { value: [{ value: '2', name: '2' }], key: '2' } }
            },
            {
                id: 'd152e48d-13d1-4553-94fa-525fa67d4f2b',
                fieldCode: 'BillDate',
                fieldName: '创建日期',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 1, content: { dateValue: '2023-09-01' } }
            },
            {
                id: 'abb7ef8e-ef19-4d83-99cc-1b7ff4f05345',
                fieldCode: 'SecID',
                fieldName: '密级',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: {
                    type: 3,
                    content: {
                        value: [
                            { value: 'a', name: 'a' },
                            { value: 'b', name: 'b' }
                        ],
                        key: 'a,b'
                    }
                }
            },
            {
                id: '219c9c45-3bfb-4482-8755-1ea0d3c9475c',
                fieldCode: 'SecLevel',
                fieldName: '密级',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 5, content: { startValue: 123, endValue: 213 } }
            },
            {
                id: '6f1bf2f5-7afc-42c5-81ee-b547db370be9',
                fieldCode: 'AuditStatus',
                fieldName: '订单状态',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 14, content: { value: '2' } }
            },
            {
                id: '01ca22ef-fbd4-4322-8e88-503edb9e071c',
                fieldCode: 'ProjectID',
                fieldName: '所属项目',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 0, content: { value: 'dsfd ' } }
            },
            {
                id: '3a8403c1-aa75-4091-947e-53f1d2bb8d4d',
                fieldCode: 'TotalSum',
                fieldName: '订单金额',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 6, content: { numValue: 21333 } }
            },
            {
                id: 'd9b196a3-2bf0-46ba-8807-22f6ac62ad78',
                fieldCode: 'ProjectMrg',
                fieldName: '项目经理',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 0, content: {} }
            },
            {
                id: '2691b91b-1b5e-4eca-b802-ff5cc2ee7856',
                fieldCode: 'Version',
                fieldName: '版本',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 4, content: { startTime: '2023-09-01', endTime: '2023-09-15' } }
            }
        ],
        isPreset: true,
        isDefault: true,
        creatorCode: 'sagi',
        creatorId: 'sagi',
        creatorName: 'sagi'
    },
    {
        id: '3a491eb1-018e-e03e-d430-f05fccd894f3',
        code: 'Order of The Month',
        name: '本月订单',
        mode: '1',
        type: 'public',
        conditions: [
            {
                id: '2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a',
                fieldCode: '制单人',
                fieldName: '',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 13, content: { valueField: '', value: [], textValue: 'help-text-value-null' } }
            },
            {
                id: '393c7ac7-64b2-4888-83a9-c1d8b2755339',
                fieldCode: 'DomainID.DomainID',
                fieldName: '业务部门',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 2, content: { value: [], valueField: '', textValue: 'help-text-value-null', isInputText: false } }
            },
            {
                id: 'd9ace6f2-a77a-4b2b-9b3c-61154bb40848',
                fieldCode: 'BillCode',
                fieldName: '单据编号',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 15, content: { value: [], textValue: '' } }
            },
            {
                id: '3a8403c1-aa75-4091-947e-53f1d2bb8d4d',
                fieldCode: 'TotalSum',
                fieldName: '订单金额',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 5, content: { startValue: null, endValue: null } }
            },
            {
                id: 'c7dbe607-bddc-4391-92e2-16f0221765df',
                fieldCode: 'BillType',
                fieldName: '订单类型',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 3, content: { value: [] } }
            },
            {
                id: 'd152e48d-13d1-4553-94fa-525fa67d4f2b',
                fieldCode: 'BillDate',
                fieldName: '创建日期',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 1, content: { dateValue: '' } }
            },
            {
                id: 'abb7ef8e-ef19-4d83-99cc-1b7ff4f05345',
                fieldCode: 'SecID',
                fieldName: '密级',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 3, content: { value: [] } }
            },
            {
                id: '219c9c45-3bfb-4482-8755-1ea0d3c9475c',
                fieldCode: 'SecLevel',
                fieldName: '密级',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 5, content: { startValue: null, endValue: null } }
            },
            {
                id: '6f1bf2f5-7afc-42c5-81ee-b547db370be9',
                fieldCode: 'AuditStatus',
                fieldName: '订单状态',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 14, content: {} }
            },
            {
                id: '01ca22ef-fbd4-4322-8e88-503edb9e071c',
                fieldCode: 'ProjectID',
                fieldName: '所属项目',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                value: { type: 0, content: { value: '' } }
            }
        ],
        isPreset: false,
        isDefault: false,
        creatorCode: 'sagi',
        creatorId: 'sagi',
        creatorName: 'sagi'
    },
    {
        id: '2120021b-df0c-0eef-d1d5-feaa78b01d7e',
        code: 'Pending Orders of The Month',
        name: '本月待支付订单',
        mode: '2',
        type: 'private',
        conditions: [
            {
                id: 'e605c9f9-a252-48ca-9c65-0bffb3427e86',
                fieldCode: 'id',
                fieldName: '标识',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 9,
                compareType: '0',
                relation: 1,
                value: { type: 0, content: { value: '123' } }
            },
            {
                id: '6f1bf2f5-7afc-42c5-81ee-b547db370be9',
                fieldCode: 'AuditStatus',
                fieldName: '单据状态',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 6,
                compareType: '0',
                relation: 1,
                lBracket: '(',
                value: { type: 14, content: { value: '2' } }
            },
            {
                id: '2e6a0b5c-dd36-4bfd-ab3c-e3035f3a8e5a',
                fieldCode: 'EmployeeID.EmployeeID',
                fieldName: '创建人',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 1,
                compareType: '0',
                relation: 1,
                lBracket: '((',
                value: {
                    type: 13,
                    content: {
                        valueField: 'id',
                        value: [
                            {
                                id: '9e35953a-40fd-4a9a-922f-b9f59bf2a517',
                                code: 'sagi',
                                name: 'sagi',
                                userGroup: '0',
                                sysOrgId: '001',
                                tenantId: 10000,
                                secLevel: 'PUBLIC',
                                userType: 3,
                                note: null
                            }
                        ],
                        textValue: 'sagi'
                    }
                }
            },
            {
                id: '393c7ac7-64b2-4888-83a9-c1d8b2755339',
                fieldCode: 'DomainID.DomainID',
                fieldName: '业务部门',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 2,
                compareType: '0',
                relation: 1,
                rBracket: ')',
                value: {
                    type: 2,
                    content: {
                        value: [
                            {
                                id: '1640d027-7531-2388-e5eb-6fe0ae8c9734',
                                code: 'SCM',
                                name: '采购部',
                                treeinfo: { parentElement: '001', sequence: 2, layer: 2, isDetail: true }
                            },
                            {
                                id: '001',
                                code: '001',
                                name: '销售部',
                                treeinfo: { parentElement: '', sequence: 1, layer: 1, isDetail: false }
                            }
                        ],
                        valueField: 'id',
                        textValue: '采购部,销售部',
                        isInputText: false
                    }
                }
            },
            {
                id: 'd9ace6f2-a77a-4b2b-9b3c-61154bb40848',
                fieldCode: 'BillCode',
                fieldName: '单据编号',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 7,
                compareType: '0',
                relation: 1,
                lBracket: '(',
                value: { type: 15, content: { value: [], textValue: '123', isInputText: true } }
            },
            {
                id: '3a8403c1-aa75-4091-947e-53f1d2bb8d4d',
                fieldCode: 'TotalSum',
                fieldName: '订单金额',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 8,
                compareType: '0',
                relation: 1,
                rBracket: ')))',
                value: { type: 6, content: { numValue: 5 } }
            },
            {
                id: 'c7dbe607-bddc-4391-92e2-16f0221765df',
                fieldCode: 'BillType',
                fieldName: '订单类型',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 3,
                compareType: '0',
                relation: 1,
                lBracket: '(',
                value: { type: 3, content: { value: [{ value: '1', name: '1' }], key: '1' } }
            },
            {
                id: 'd152e48d-13d1-4553-94fa-525fa67d4f2b',
                fieldCode: 'BillDate',
                fieldName: '创建日期',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 4,
                compareType: '0',
                relation: 1,
                lBracket: '(',
                value: { type: 1, content: { dateValue: '2023-09-01' } }
            },
            {
                id: 'abb7ef8e-ef19-4d83-99cc-1b7ff4f05345',
                fieldCode: 'SecID',
                fieldName: '密级',
                valueType: 0,
                placeHolder: '',
                beginPlaceHolder: '',
                endPlaceHolder: '',
                visible: true,
                conditionId: 5,
                compareType: '0',
                relation: 1,
                rBracket: '))',
                value: {
                    type: 3,
                    content: {
                        value: [
                            { value: 'a', name: 'a' },
                            { value: 'b', name: 'b' },
                            { value: 'c', name: 'c' }
                        ],
                        key: 'a,b,c'
                    }
                }
            }
        ],
        isPreset: false,
        isDefault: false,
        creatorCode: 'sagi',
        creatorId: 'sagi',
        creatorName: 'sagi'
    }
];

export {
    fieldConfigs,
    solutions
};
