
##  如何本地运行项目

### 1 安装依赖组件

在本地运行项目前，请先执行以下命令，安装依赖npm包。

```
npm install
```

### 2 打包设计时组件

在git bash窗口中执行以下命令，打包设计时组件。

```
npm run formDesigner
```

### 3 运行示例站点

执行以下命令。

```
ng serve form-designer
```

在浏览器中访问：`http://localhost:4200/?metadataId=9f7177b9-c79a-4972-9bf3-b4961d77a31c` 查看示例页面。

![示例页面](./src/assets/form-designer.png)