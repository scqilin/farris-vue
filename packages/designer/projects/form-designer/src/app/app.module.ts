import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
// import { GSP, CacheService } from '@farris/ide-devkit';
import { FormEditorModule } from './form-editor/form-editor.module';
// import { IdeCodeViewModule } from '@farris/ide-code-view';
// import { IdeCodeViewConfigerModule } from '@farris/ide-code-view-configer';

// const globalGsp = (window.top as any).gsp;
// if (globalGsp && globalGsp.context) {
//     (window as any).gsp = new GSP(globalGsp);
// }

// 测试环境配置sessionId，iGix环境中删掉此行
// gsp.cache.set('sessionId', 'default');

// 运行：http://localhost:4200/?id=/wangxhApp/wangxhSu/wangxhBo/bo-wangxhbofronttest/metadata/components/card11.frm
@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        FormEditorModule,
        // IdeCodeViewModule.forRoot(),
        // IdeCodeViewConfigerModule
    ],
    // providers: [{ provide: CacheService, useValue: gsp.cache }],
    bootstrap: [AppComponent],
})
export class AppModule { }
