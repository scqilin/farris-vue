/* eslint-disable @typescript-eslint/indent */
/* eslint-disable no-restricted-syntax */
import { Component, ComponentFactoryResolver, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { NotifyService } from '@farris/ui-notify';
import { ItemCollectionEditorComponent } from '@farris/designer-devkit';
import { FormDesignerComponent } from './form-designer/form-designer.component';
// import { FormMetadataService } from './service/form-metadata.service';
import { LoadingComponent, LoadingService } from '@farris/ui-loading';
import { MessagerService } from '@farris/ui-messager';
import { EditorTypes } from '@farris/ui-datagrid-editors';
import { BsModalService } from '@farris/ui-modal';
import { Observable, of, Subject } from 'rxjs';
import { DgControl, MultiViewManageService } from '@farris/designer-ui';
import { map } from 'rxjs/operators';
// import { FormProcessService } from './service/form-process.service';
import {
    DomService, FormBasicService, StateMachineService, DataStatesService, WebCmdService, FarrisMetadataService
} from '@farris/designer-services';
import { TemplateOutlineSchemaService } from './form-designer/service/template-outline.service';
import { HttpClient } from '@angular/common/http';

// import { CmpExportComponent } from './toolbar/cmp-export/cmp-export.component';
// import { EventCommunicationComponent } from './toolbar/event-communication/event-communication.component';
import { VmDesignerComponent } from './vm-designer/vm-designer.component';
// import { FormSaveAsComponent } from './toolbar/form-save-as-dialog/form-save-as.component';
// import { PublishMenuComponent } from './toolbar/publish-menu/publish-menu.component';
import { BOTTOM_TAB_TYPE } from './form-designer/entity/form-designer-entity';
import { DesignerMetadataService } from './service/designer-metadata.service';

@Component({
    selector: 'webide-form-editor',
    templateUrl: './form-editor.component.html',
    styleUrls: ['./form-editor.component.css'],
    providers: [DesignerMetadataService]
})
export class FormEditorComponent {

    /** 切换设计器/代码视图 */
    @Output() switchView = new EventEmitter<void>();

    @Output() formSavedEmiiter = new EventEmitter<void>();

    @ViewChild(FormDesignerComponent) private formDesigner: FormDesignerComponent;
    // @ViewChild(FormSaveAsComponent) private formSaveAsComponent: FormSaveAsComponent;

    @ViewChild('vmDesigner') private vmDesignerComponent: VmDesignerComponent;

    // private metadaService: FormMetadataService;

    /** 当前展示视图：页面（formDesigner）、实体（entityDesigner） */
    showDesignerType = 'formDesigner';

    public designerToolbarItems = [];

    private iFrameTabId: string;

    constructor(
        public injector: Injector,
        private notifyService: NotifyService,
        private domService: DomService,
        private modalService: BsModalService,
        private loadingService: LoadingService,
        private msgService: MessagerService,
        public formBasicService: FormBasicService,
        private resolver: ComponentFactoryResolver,
        private stateMachineService: StateMachineService,
        private dataStatesService: DataStatesService,
        private webCmdService: WebCmdService,
        private http: HttpClient,
        private multiViewManageService: MultiViewManageService,
        private metadaService: DesignerMetadataService
    ) {
        // 设置元数据未修改标识
        // this.iFrameTabId = gsp.ide.getParam('id');
        // if (gsp.ide && gsp.ide.setDesignerStatus) {
        //     gsp.ide.setDesignerStatus(this.iFrameTabId, false);
        // }

        // 设计器未渲染完毕，不监听DOM变更
        window.suspendChangesOnForm = true;
        window.isFormMetadataChanged = false;

        this.metadaService.getFormMetadata().subscribe(domJson => {

            /** 渲染设计器 */
            this.formDesigner.render(domJson);

            // 状态机集成
            this.stateMachineService.initStateMachineMetaData();

            this.dataStatesService.getDomJson(domJson);

            this.initForm();
        });

        // 执行完毕加载进行事件订阅  不可延迟注册 因为框架是在界面打开就进行了注册
        // this.subscribeFormSaveCommand();

    }

    /**
     * 表单加载后注册 因为框架是在界面打开就进行了注册
     * 如果元数据未加载完毕，那么需要增加元数据加载完毕标识，未加载完毕不执行保存动作
     */
    // private subscribeFormSaveCommand() {

    //     const eventName = 'ideSave:' + this.iFrameTabId;

    //     gsp.eventBus.on(null, this.iFrameTabId, eventName, null, (value) => {

    //         // 执行当前表单的保存
    //         this.saveFormMetadata(null, true).subscribe((result) => {
    //             // 保存成功
    //             if (result.success) {
    //                 // 触发代码视图的保存
    //                 window.saveFormAndCodeView = true;
    //                 this.formSavedEmiiter.emit();
    //             }
    //         });
    //     });
    // }

    initForm() {

        this.multiViewManageService.newCrosstabViewIds = [];

        // 获取当前表单所属的模板大纲结构
        const templateOutlineSchemaService = new TemplateOutlineSchemaService(this.http, this.domService);
        templateOutlineSchemaService.getTemplateSchema();
        this.initDesignerToolbarItems();
    }

    /**
     * 初始工具栏配置
     */
    private initDesignerToolbarItems() {
        this.designerToolbarItems = [
            // {
            //     id: 'btn-save',
            //     text: '保存',
            //     class: 'btn-primary',
            //     isDP: true,
            //     split: true,
            //     children: [
            //         {
            //             id: 'btn-saveAs',
            //             text: '另存为'
            //         }
            //     ]
            // },
            {
                id: 'btn-compileAndPreview',
                text: '调试'
            },
            // {
            //     id: 'btn-run',
            //     text: '运行',
            //     isDP: true,
            //     split: true,
            //     children: [
            //         {
            //             id: 'btn-compileAndPreview',
            //             text: '调试'
            //         }
            //     ]
            // },
            // {
            //     id: 'btn-zuhe',
            //     text: '组合表单',
            //     isDP: true,
            //     children: [
            //         {
            //             id: 'btn-addExport',
            //             text: '组件声明'
            //         },
            //         {
            //             id: 'btn-eventCommunication',
            //             text: '组件通讯'
            //         }
            //     ]
            // },
            // {
            //     id: 'btn-showExtraModule',
            //     text: '外部模块'
            // },
            // {
            //     id: 'btn-publishMenu',
            //     text: '发布菜单'
            // },
            // {
            //     id: 'btn-publishWorkflow',
            //     text: '发布到工作流'
            // }
        ];
    }

    /**
     * 工具栏点击事件
     */
    public designerrToolbarClickHandler(args: any) {
        // switch (args.id) {
        //     // case 'btn-save': {
        //     //     this.saveForm();
        //     //     break;
        //     // }
        //     // // 运行
        //     // case 'btn-run': {
        //     //     this.runForm();
        //     //     break;
        //     // }
        //     // 解析调试
        //     case 'btn-compileAndPreview': {
        //         this.dynamicPreview();
        //         break;
        //     }
        //     // case 'btn-addExport': {
        //     //     this.addExport();
        //     //     break;
        //     // }
        //     // case 'btn-eventCommunication': {
        //     //     this.eventCommunication();
        //     //     break;
        //     // }
        //     // case 'btn-showExtraModule': {
        //     //     this.showExtraModule();
        //     //     break;
        //     // }
        //     // case 'btn-publishMenu': {
        //     //     this.publishMenu();
        //     //     break;
        //     // }
        //     // case 'btn-publishWorkflow': {
        //     //     this.publishWorkflow();
        //     //     break;
        //     // }

        // }
    }

    /**
     * 切换显示类型
     */
    onChangeShowDesigner(type: string) {
        // 从页面切换到其他页签时，先确保应用代码编辑器的内容
        if (type !== 'formDesigner' && this.showDesignerType === 'formDesigner') {
            this.formDesigner.changeBottomBar(BOTTOM_TAB_TYPE.formDesigner);
        }

        // 切换到模型时，触发模型页面的数据刷新
        if (type === 'vmDesigner' && this.vmDesignerComponent) {
            this.vmDesignerComponent.refreshVmDesigner();
        }

        // 从模型切换到页面时，触发命令刷新，以便于交互面板获取最新构件和命令
        if (type === 'formDesigner' && this.showDesignerType === 'vmDesigner') {
            this.webCmdService.checkCommands();
            this.formDesigner.updatePropertyPanelAfterModelChanged();
        }

        this.showDesignerType = type;
    }

    /**
     * 代码视图切换时刷新
     */
    refreshWebCmdAfterCodeViewSaved() {
        this.webCmdService.checkCommands().subscribe(data => {
            this.formDesigner.updatePropertyPanelAfterModelChanged();
        });
    }

    /**
     * 保存表单
     */
    // saveForm() {

    //     const loading = this.loadingService.show({ container: 'body', message: '保存中，请稍候...' });

    //     this.saveFormMetadata(loading).subscribe(result => {
    //         if (result && result.success) {
    //             this.notifyService.success('表单保存成功！');
    //         } else {
    //             loading.close();
    //         }
    //     }, () => {
    //         this.notifyService.error('表单保存失败！');
    //         loading.close();
    //     });
    // }

    // /**
    //  * 校验并保存表单元数据
    //  * @param showNotify 是否保存时显示进度提示
    //  */
    // private saveFormMetadata(loading?: LoadingComponent, showNotify = true): Observable<{ success: boolean; message?: string }> {
    //     const subject = new Subject<{ success: boolean; message?: string }>();

    //     const isValid = this.formDesigner.prepareBeforeFormSaved();
    //     if (!isValid) {
    //         return of({ success: false });
    //     }

    //     const domJson = this.domService.getDomJson();
    //     this.metadaService.saveFormData(domJson).subscribe(() => {
    //         this.multiViewManageService.addVOActionsForCrosstabView();

    //         if (gsp.ide && gsp.ide.setDesignerStatus) {
    //             window.isFormMetadataChanged = false;

    //             const changes = window.isFormCodeViewChanged || window.isFormMetadataChanged;
    //             gsp.ide.setDesignerStatus(this.iFrameTabId, changes);
    //         }

    //         subject.next({ success: true, message: '' });
    //         if (loading) {
    //             loading.close();
    //         }

    //     }, error => {
    //         const errorMessageResult = this.handleErrorMessage(error, '表单保存失败！', showNotify);
    //         subject.next({ success: false, message: errorMessageResult.message });

    //         if (loading) {
    //             loading.close();
    //         }
    //     });

    //     return subject;
    // }

    // /**
    //  * 截取保存表单的错误提示
    //  */
    // private handleErrorMessage(error, notifyMsg: string, showNotify = true) {
    //     const extractedResult = this.extractErrorMessage(error, notifyMsg);
    //     if (showNotify) {
    //         if (extractedResult.useDefault) {
    //             this.notifyService.error(extractedResult.message);
    //         } else {
    //             const modalOptions = {
    //                 title: '错误提示',
    //                 showMaxButton: false,
    //                 safeHtml: false
    //             };
    //             this.msgService.show('error', extractedResult.message, modalOptions);
    //         }
    //     }
    //     return extractedResult;
    // }

    // /**
    //  * 提取error的参数信息
    //  */
    // private extractErrorMessage(error, defaultNotifyMsg): {
    //     message: string;
    //     useDefault: boolean;
    // } {
    //     if (error && error._body) {
    //         if (error._body) {
    //             const befErrorToken = '#GSPBefError#';
    //             const befMsg = JSON.parse(error._body).Message;
    //             if (befMsg && befMsg.includes(befErrorToken)) {
    //                 const message = befMsg.split(befErrorToken);
    //                 const errorMessage = message[1];
    //                 return {
    //                     message: errorMessage,
    //                     useDefault: false
    //                 };
    //             }
    //         }
    //         if (error.error) {
    //             return {
    //                 message: error.error.message,
    //                 useDefault: false
    //             };
    //         }
    //     }
    //     if (error && error.error && error.error.Message) {
    //         return {
    //             message: error.error.Message,
    //             useDefault: false
    //         };
    //     }
    //     return {
    //         message: defaultNotifyMsg,
    //         useDefault: true
    //     };

    // }

    // /**
    //  *  另存为
    //  */
    // saveFormAs() {
    //     if (this.domService.module.isComposedFrm) {
    //         this.notifyService.warning('暂不支持组合表单另存为');
    //         return;
    //     }
    //     const self = this;

    //     self.formSaveAsComponent.show();
    // }

    // /**
    //  * 配置外部模块
    //  */
    // manageExtraModule() {
    //     const compFactory = this.resolver.resolveComponentFactory(ItemCollectionEditorComponent);
    //     const compRef = compFactory.create(this.injector);
    //     compRef.instance.editorParams = {
    //         modalTitle: '外部模块',
    //         columns: [
    //             { field: 'name', title: '模块名称', editor: { type: EditorTypes.TEXTBOX } },
    //             { field: 'path', title: '模块路径', editor: { type: EditorTypes.TEXTBOX } }
    //         ],
    //         requiredFields: ['name', 'path'],
    //         uniqueFields: ['name', 'path'],
    //         canEmpty: true
    //     };
    //     const modalConfig = {
    //         title: '外部模块',
    //         width: 900,
    //         height: 500,
    //         showButtons: true,
    //         buttons: compRef.instance.modalFooter
    //     };
    //     compRef.instance.value = this.domService.extraImports || [];

    //     const modalPanel = this.modalService.show(compRef, modalConfig);
    //     compRef.instance.closeModal.subscribe(() => {
    //         modalPanel.close();
    //     });

    //     compRef.instance.submitModal.subscribe((data) => {
    //         if (data) {
    //             this.domService.extraImports = data.value;
    //             modalPanel.close();
    //         }
    //     });
    // }

    // /**
    //  * 运行：先保存再运行
    //  */
    // runForm() {
    //     const loading = this.loadingService.show({ container: 'body' });
    //     this.saveFormMetadata().subscribe(saveResult => {
    //         if (!saveResult.success) {
    //             loading.close();
    //             return;
    //         }
    //         this.metadaService.getServerInfo().subscribe(serverInfo => {
    //             const serverIP = this.metadaService.getServerIP(serverInfo);
    //             if (serverIP === null) {
    //                 loading.close();
    //                 return;
    //             }
    //             this.metadaService.getFormRoute().subscribe(data => {
    //                 if (data) {
    //                     const pageFlowRoute = data.slice(data.lastIndexOf('#') + 1, data.length);
    //                     if (!pageFlowRoute) {
    //                         this.notifyService.error('获取路由为空，请检查是否配置页面流！');
    //                     } else {
    //                         serverIP === '' ?
    //                             window.open(serverIP + '/platform/common/web/debug/index.html?url=' + data) :
    //                             window.open(serverIP + '&redUrl=' + encodeURIComponent(data));
    //                     }
    //                 } else {
    //                     this.notifyService.error('获取路由为空！');
    //                 }
    //                 loading.close();
    //             }, error => {
    //                 loading.close();
    //                 this.handleErrorMessage(error, '表单运行失败！');
    //             });
    //         }, error => {
    //             loading.close();
    //             this.handleErrorMessage(error, '获取运行环境失败！');
    //         });

    //     });

    // }

    /**
     * 解析型预览
     */
    // private dynamicPreview() {
    //     const isUnSupportedMsg = this.checkUnSupportedControlsForDynamicPreview();
    //     if (isUnSupportedMsg) {
    //         this.notifyService.warning(isUnSupportedMsg);
    //         return;
    //     }
    //     const loading = this.loadingService.show({ container: 'body', message: '解析中，请稍候...' });
    //     // 先保存
    //     this.saveFormMetadata().subscribe(saveResult => {
    //         if (!saveResult.success) {
    //             loading.close();
    //             return;
    //         }
    //         this.metadaService.interpretationPreviewGenerate().subscribe((data: any) => {

    //             if (data) {
    //                 const { code, message } = data;
    //                 if (code === 1) {
    //                     loading.close();

    //                     const metadataPath = gsp.ide.getParam('id');
    //                     window.open('/platform/common/web/farris-render-preview/index.html#/?ispreviewmode=true&url=' + metadataPath);
    //                 } else {
    //                     const modalOptions = {
    //                         title: '调试失败',
    //                         showMaxButton: false,
    //                         safeHtml: false
    //                     };
    //                     this.msgService.show('error', message || '', modalOptions);
    //                     loading.close();

    //                 }
    //             } else {
    //                 this.notifyService.error('调试失败');
    //                 loading.close();

    //             }

    //         }, () => {
    //             this.notifyService.error('调试失败');
    //             loading.close();
    //         });

    //     });

    // }

    // /**
    //  * 校验解析调试不支持的场景，并给出提示信息
    //  */
    // private checkUnSupportedControlsForDynamicPreview() {

    //     // 暂不支持qdp类模板
    //     if (this.domService.module.qdpInfo) {
    //         return `暂不支持调试查询类表单`;
    //     }
    //     // 暂不支持多视图、滚动监听、侧边栏、多语输入框
    //     const unSupportedControls = [
    //         DgControl.MultiViewContainer.type, DgControl.Scrollspy.type,
    //         DgControl.Sidebar.type, DgControl.LanguageTextBox.type
    //     ];

    //     for (const cmp of this.domService.components) {
    //         const control = this.domService.selectNode(cmp, item => unSupportedControls.includes(item.type));
    //         if (control) {
    //             return `暂不支持调试${DgControl[control.type].name}表单`;
    //         }

    //         if (cmp.componentType === 'dataGrid') {
    //             const grid = this.domService.selectNode(cmp, item => item.type === DgControl.DataGrid.type);

    //             if (grid && grid.fields) {
    //                 if (JSON.stringify(grid.fields).includes(DgControl.LanguageTextBox.type)) {
    //                     return `暂不支持调试${DgControl.LanguageTextBox.name}表单`;
    //                 }
    //             }
    //         }
    //     }

    // }

    // /**
    //  * 发布菜单
    //  */
    // publishMenu() {
    //     const loading = this.loadingService.show({ container: 'body' });
    //     // 先保存，再发布
    //     this.saveFormMetadata().subscribe(saveResult => {
    //         loading.close();

    //         if (!saveResult.success) {
    //             return;
    //         }

    //         const compFactory = this.resolver.resolveComponentFactory(PublishMenuComponent);
    //         const compRef = compFactory.create(this.injector);

    //         const modalConfig = {
    //             title: '发布菜单',
    //             width: 600,
    //             height: 520,
    //             showButtons: true,
    //             buttons: compRef.instance.modalFooter
    //         };
    //         this.metadaService.getFormRoute().pipe(
    //             map(str => str.split('#')[0] + 'index.html')
    //         ).subscribe(path => {
    //             compRef.instance.publishPath = path;

    //             const modalPanel = this.modalService.show(compRef, modalConfig);
    //             compRef.instance.closeModal.subscribe(() => {
    //                 modalPanel.close();
    //             });

    //             compRef.instance.submitModal.subscribe((data) => {
    //                 if (data) {
    //                     modalPanel.close();
    //                 }
    //             });
    //         }, () => {
    //             this.notifyService.error('获取路由信息失败！');
    //         });
    //     });

    // }

    // /**
    //  * 组件声明：添加导出事件、uistate
    //  */
    // addExport() {
    //     const compFactory = this.resolver.resolveComponentFactory(CmpExportComponent);
    //     const compRef = compFactory.create(this.injector);

    //     const modalConfig = {
    //         title: '组件声明',
    //         width: 950,
    //         height: 600,
    //         resizable: false,
    //         buttons: compRef.instance.btns
    //     };
    //     const modalPanel = this.modalService.show(compRef, modalConfig);
    //     compRef.instance.closeModal.subscribe(() => {
    //         modalPanel.close();
    //     });
    // }

    // /**
    //  * 事件通讯
    //  */
    // eventCommunication() {
    //     const compFactory = this.resolver.resolveComponentFactory(EventCommunicationComponent);
    //     const compRef = compFactory.create(this.injector);

    //     const modalConfig = {
    //         title: '组件通讯',
    //         width: 950,
    //         height: 600,
    //         showButtons: true,
    //         buttons: compRef.instance.btns
    //     };
    //     const modalPanel = this.modalService.show(compRef, modalConfig);
    //     compRef.instance.closeModal.subscribe(() => {
    //         modalPanel.close();
    //     });
    // }

    // /**
    //  * 发布工作流
    //  */
    // publishWorkflow() {
    //     const loading = this.loadingService.show({ container: 'body' });
    //     // 先保存，再发布
    //     this.saveFormMetadata().subscribe(saveResult => {
    //         loading.close();

    //         if (!saveResult.success) {
    //             return;
    //         }
    //         const wfService = new FormProcessService(this.injector);
    //         wfService.publish();
    //     });

    // }

    // /**
    //  * 切换到代码视图
    //  */
    // onClickCodeEditor() {
    //     this.switchView.emit();
    // }

    // /**
    //  * 引入外部模块
    //  */
    // showExtraModule() {
    //     const compFactory = this.resolver.resolveComponentFactory(ItemCollectionEditorComponent);
    //     const compRef = compFactory.create(this.injector);
    //     compRef.instance.editorParams = {
    //         modalTitle: '外部模块',
    //         columns: [
    //             { field: 'name', title: '模块名称', editor: { type: EditorTypes.TEXTBOX } },
    //             { field: 'path', title: '模块路径', editor: { type: EditorTypes.TEXTBOX } }
    //         ],
    //         requiredFields: ['name', 'path'],
    //         uniqueFields: ['name', 'path'],
    //         canEmpty: true
    //     };
    //     const modalConfig = {
    //         title: '外部模块',
    //         width: 900,
    //         height: 500,
    //         showButtons: true,
    //         buttons: compRef.instance.modalFooter
    //     };
    //     compRef.instance.value = this.domService.extraImports || [];

    //     const modalPanel = this.modalService.show(compRef, modalConfig);
    //     compRef.instance.closeModal.subscribe(() => {
    //         modalPanel.close();
    //     });

    //     compRef.instance.submitModal.subscribe((data) => {
    //         if (data) {
    //             this.domService.extraImports = data.value;
    //             modalPanel.close();
    //         }
    //     });
    // }
}
