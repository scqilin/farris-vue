import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { CacheService } from '@farris/ide-devkit';
import { FarrisDesignerDevkitModule } from '@farris/designer-devkit';
import { FarrisDesignerUIModule } from '@farris/designer-ui';
import { FormDesignerComponent } from './form-designer/form-designer.component';
import { PropertyPanelModule } from '@farris/ide-property-panel';
import { SplitterModule } from '@farris/ui-splitter';
import { ClassEditorComponent } from './form-designer/panels/class-editor/class-editor.component';
import { FormSettingsComponent } from './form-settings/form-settings.component';
import { LoadingService, LoadingModule } from '@farris/ui-loading';
import { SwitchModule } from '@farris/ui-switch';
import { FAreaResponseModule } from '@farris/ui-area-response';
import { AngularDraggableModule } from '@farris/ui-draggable';
import { TreeTableModule } from '@farris/ui-treetable';
// import { GSPMetadataServiceModule } from '@gsp-lcm/metadata-selector';
import { environment } from '../../environments/environment';
import { MessagerModule } from '@farris/ui-messager';
import { FormEditorComponent } from './form-editor.component';
import { FarrisTabsModule } from '@farris/ui-tabs';
import { DatagridModule } from '@farris/ui-datagrid';
// import { CmpExportModule } from './toolbar/cmp-export/cmp-export.module';
// import { EventCommunicationComponent } from './toolbar/event-communication/event-communication.component';
import { VMDesignerModule } from './vm-designer/vm-designer.module';
import { FResponseToolbarModule } from '@farris/ui-response-toolbar';
// import { IDEPublishMenuModule } from './toolbar/publish-menu/publish-menu.module';
import { ComboListModule } from '@farris/ui-combo-list';
import { FarrisFormsModule } from '@farris/ui-forms';
import { FarrisMetadataService } from '@farris/designer-services';
import { DesignerMetadataService } from './service/designer-metadata.service';

@NgModule({

    declarations: [
        FormEditorComponent,
        FormDesignerComponent,
        ClassEditorComponent,
        FormSettingsComponent,
        // EventCommunicationComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        PropertyPanelModule,
        AngularDraggableModule,
        SplitterModule,
        SwitchModule,
        LoadingModule.forRoot({
            message: '加载中，请稍候...'
        }),
        TreeTableModule,
        FAreaResponseModule,
        FarrisDesignerDevkitModule,
        FarrisDesignerUIModule,
        // GSPMetadataServiceModule.forRoot(environment.SERVER_IP),
        MessagerModule,
        FarrisTabsModule,
        DatagridModule,
        // CmpExportModule,
        VMDesignerModule,
        FResponseToolbarModule,
        ComboListModule,
        // IDEPublishMenuModule,
        FarrisFormsModule
    ],
    providers: [
        LoadingService,
        { provide: FarrisMetadataService, useClass: DesignerMetadataService }
        // { provide: CacheService, useValue: gsp.cache }
    ],
    bootstrap: [FormEditorComponent],
    exports: [FormEditorComponent],
    entryComponents: [
        // EventCommunicationComponent
    ],
})
export class FormEditorModule { }
