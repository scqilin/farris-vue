import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { CacheService, GSP } from '@farris/ide-devkit';
import { LoadingService, LoadingModule } from '@farris/ui-loading';
import { SwitchModule } from '@farris/ui-switch';
import { TreeTableModule } from '@farris/ui-treetable';
import { MessagerModule } from '@farris/ui-messager';
import { FarrisTabsModule } from '@farris/ui-tabs';
import { FarrisTooltipModule } from '@farris/ui-tooltip';
import { environment } from '../../../environments/environment';
import { CmdBasicEditorComponent } from './method-manager/cmd-basic-editor/cmd-basic-editor.component';
import { MethodManagerComponent } from './method-manager/method-manager.component';
import { VaribleManagerComponent } from './variable-manager/variable-manager.component';
import { VmDesignerComponent } from './vm-designer.component';
import { DatagridModule } from '@farris/ui-datagrid';
import { CommandSelectorComponent } from './method-manager/command-selector/command-selector.component';
// import { FarrisIDEParameterEditorModule } from '@farris/ide-parameter-editor';
import { SplitterModule } from '@farris/ui-splitter';
import { AngularDraggableModule } from '@farris/ui-draggable';
import { AppNavigationSelectorComponent, AppNavigationSelectorModule } from '@farris/app-navigation-selector';


@NgModule({

    declarations: [
        VmDesignerComponent,
        MethodManagerComponent,
        VaribleManagerComponent,
        CmdBasicEditorComponent,
        CommandSelectorComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        SwitchModule,
        LoadingModule.forRoot({
            message: '加载中，请稍候...'
        }),
        TreeTableModule,
        MessagerModule,
        FarrisTabsModule,
        FarrisTooltipModule,
        DatagridModule,
        // FarrisIDEParameterEditorModule,
        SplitterModule,
        AngularDraggableModule,
        AppNavigationSelectorModule
    ],
    providers: [
        LoadingService,
        // { provide: CacheService, useValue: gsp.cache }
    ],
    bootstrap: [VmDesignerComponent],
    exports: [VmDesignerComponent],
    entryComponents: [CmdBasicEditorComponent, CommandSelectorComponent, AppNavigationSelectorComponent],
})
export class VMDesignerModule { }
