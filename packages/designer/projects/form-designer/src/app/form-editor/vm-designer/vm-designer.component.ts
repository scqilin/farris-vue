import { Component, HostBinding, OnInit, ViewChild } from '@angular/core';
import { MethodManagerComponent } from './method-manager/method-manager.component';
import { VaribleManagerComponent } from './variable-manager/variable-manager.component';


@Component({
  selector: 'app-vm-designer',
  templateUrl: './vm-designer.component.html',
  styleUrls: ['./vm-designer.component.css']
})
export class VmDesignerComponent implements OnInit {

  @ViewChild('variable') private variableComponent: VaribleManagerComponent;
  @ViewChild('method') private methodComponent: MethodManagerComponent;


  /** 右侧显示类型：方法method|变量variable */
  showType = 'method';

  @HostBinding('class')
  class = 'f-utils-fill-flex-row w-100 h-100 bg-white';

  ngOnInit(): void {
  }

  refreshVmMetadata() {

    this.methodComponent.refreshVmMethod();

    // 初始右侧变量列表
    this.variableComponent.initVariableList();
  }

  onChangeLeftNode(type: string) {
    this.showType = type;
  }

  /**
   *  由设计切换到实体视图时，自动刷新变量列表。（目的是刷新组件变量）
   */
  refreshVmDesigner() {
    this.refreshVmMetadata();
  }
}
