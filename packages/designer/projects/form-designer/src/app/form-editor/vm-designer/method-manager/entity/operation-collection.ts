import { Extension } from './extension';
import { ITreeNode } from './tree-node';
import { IOperationNode } from './operation-node';
import { ExecuteNode } from './execute-node';
import { SwitchNode } from './switch-node';
// import { ICommandItem, CommandItemType, CmpMethodRefering, BranchCollectionCommandItem } from '@gsp-cmp/webcommand';
import { ServiceRef } from './service-ref';

export class OperationCollection extends Array<ITreeNode & IOperationNode> {

    constructor(handlersJson?: any) {
        super();
        if (handlersJson) {
            // this.parse(handlersJson);
            for (const node of handlersJson) {
                if (!node.type) {
                    continue; // 没有type节点，信息不完整，跳过处理
                }

                switch (node.type) {
                    case 'executeNode':
                        this./* operations. */push(new ExecuteNode(node));
                        break;
                    case 'switchNode':
                        this./* operations. */push(new SwitchNode(node));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    parse(handlersJson: any) {
        // this.operations = new Array();
        if (handlersJson) {
            for (const node of handlersJson) {
                if (!node.type) {
                    continue; // 没有type节点，信息不完整，跳过处理
                }

                switch (node.type) {
                    case 'executeNode':
                        this./* operations. */push(new ExecuteNode(node));
                        break;
                    case 'switchNode':
                        this./* operations. */push(new SwitchNode(node));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    // parseFromWebCmdMetadata(items: Array<ICommandItem>, serviceRefs: Map<string, ServiceRef>) {
    //   // this.operations = new Array();
    //   if (items) {
    //     for (const item of items) {
    //       if (item.GetItemType() === CommandItemType.MethodRefer && item instanceof CmpMethodRefering) {
    //         // executeNode
    //         const executeNode = new ExecuteNode();
    //         executeNode.parseFromWebCmdMetadata(item, serviceRefs);
    //         this./* operations. */push(executeNode);
    //       } else if (item.GetItemType() === CommandItemType.BranchCollection && item instanceof BranchCollectionCommandItem) {
    //         // switchNode
    //         const switchNode = new SwitchNode();
    //         switchNode.parseFromWebCmdMetadata(item, serviceRefs);
    //         this./* operations. */push(switchNode);
    //       }
    //     }
    //   }
    // }

    toJson() {
        const handlers = [];
        for (const node of this) {
            handlers.push(node.toJson());
        }
        return handlers;
    }
}
