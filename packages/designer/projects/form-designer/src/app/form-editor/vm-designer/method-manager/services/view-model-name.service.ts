import { DomService, FormComponentType } from '@farris/designer-services';
import { DgControl } from '@farris/designer-ui';

export class ViewModelNameService {


    constructor(private domService: DomService) { }

    getViewModelName(viewModelId: string, cmpName: string) {
        const cmp = this.domService.getComponentByVMId(viewModelId);
        if (cmp.fakeDel) {
            return;
        }
        switch (cmp.componentType) {
            case FormComponentType.Frame: {
                return '根组件';
            }
            case FormComponentType.dataGrid: {
                return this.checkDataGridComponentName(cmp);

            }
            case FormComponentType.attachmentPanel: case 'AttachmentPanel': {
                return '附件组件';
            }
            case FormComponentType.listView: case 'ListView': {
                return '列表视图组件';
            }
            case FormComponentType.appointmentCalendar: {
                return '预约日历组件';
            }
            case FormComponentType.modalFrame: {
                return '弹窗页面组件';
            }
            default: {
                // 卡片组件取内部section的标题
                if (cmp.componentType.startsWith('form')) {
                    const section = cmp.contents.find(c => c.type === DgControl.Section.type);
                    if (section && section.mainTitle) {
                        return section.mainTitle + '组件';
                    }

                }
            }
        }
        return cmpName + '组件';
    }

    private checkDataGridComponentName(cmp: any) {
        const treeGrid = this.domService.selectNode(cmp, (item) => item.type === DgControl.TreeGrid.type);
        if (treeGrid) {
            return '树表格组件';
        }

        const rootCmp = this.domService.components.find(c => c.componentType === 'Frame');
        const componentRefResult = this.domService.selectNodeAndParentNode(rootCmp, (item) => item.component === cmp.id, rootCmp);
        if (!componentRefResult) {
            return;
        }
        const componentRefParentContainer = componentRefResult.parentNode;

        // 列表组件取父层容器的标题:容器可能为标签页或者section
        if (componentRefParentContainer.type === DgControl.TabPage.type) {
            return componentRefParentContainer.title + '组件';
        }
        if (componentRefParentContainer.type === DgControl.Section.type && componentRefParentContainer.mainTitle) {
            return componentRefParentContainer.mainTitle + '组件';
        }

        return '表格组件';
    }
}
