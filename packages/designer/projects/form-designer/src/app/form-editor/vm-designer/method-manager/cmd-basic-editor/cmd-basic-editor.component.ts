import { Component, Input, ViewChild, TemplateRef, Output, EventEmitter, OnInit } from '@angular/core';
import { Command } from '../entity/command';
import { cloneDeep } from 'lodash-es';
import { NotifyService } from '@farris/ui-notify';
import { DomService } from '@farris/designer-services';

@Component({
    selector: 'app-cmd-basic-editor',
    templateUrl: './cmd-basic-editor.component.html'
})
export class CmdBasicEditorComponent implements OnInit {
    @Input() currentCommand: Command;
    @Input() viewModelId: string;
    @Input() commands: Command[];

    @Output() closeModal = new EventEmitter<any>();
    @Output() submitModal = new EventEmitter<any>();

    @ViewChild('modalFooter') modalFooter: TemplateRef<any>;

    value;

    constructor(
        private notifySer: NotifyService,
        private domServ: DomService) { }

    ngOnInit() {
        this.value = cloneDeep(this.currentCommand);
    }
    clickCancel() {
        this.closeModal.emit();
    }

    clickConfirm() {
        if (!this.value.name || !this.value.name.trim()) {
            this.notifySer.warning('请输入方法名称');
            return;
        }
        if (!this.value.code || !this.value.code.trim()) {
            this.notifySer.warning('请输入方法编号');
            return;
        }

        if (!this.validateCommand()) {
            return;
        }
        const newCode = this.value.code.trim();
        if (this.currentCommand.code !== newCode) {
            this.notifySer.warning('修改方法编号后请重新绑定控件交互事件！');
        }
        this.currentCommand.code = newCode;
        this.currentCommand.name = this.value.name.trim();


        this.submitModal.emit(this.currentCommand);
    }

    /**
     * 校验命令是否重复
     * 1、同一个VM下名称不重复
     * 2、当前VM下编号不重复，编号不区分大小写
     * 3、其他VM下编号不重复
     */
    validateCommand() {
        const names = this.commands.map(c => {
            if (c.id !== this.currentCommand.id) { return c.name; }
        });
        if (names.includes(this.value.name.trim())) {
            this.notifySer.warning('名称【' + this.value.name.trim() + '】已存在，请修改');
            return false;
        }
        const newCode = this.value.code.trim();
        for (const c of this.commands) {
            if (c.id === this.currentCommand.id) {
                continue;
            }
            if (c.code.toLowerCase() === newCode.toLowerCase()) {
                this.notifySer.warning('已存在编号【' + c.code + '】，请修改');
                return false;
            }
        }
        for (const vm of this.domServ.viewmodels) {
            if (vm.id === this.viewModelId) {
                continue;
            }
            for (const cmd of vm.commands) {
                if (cmd.code.toLowerCase() === newCode.toLowerCase()) {
                    // this.notifySer.warning('视图模型【' + vm.id + '】中已存在命令编号【' + newCode + '】，请修改');
                    this.notifySer.warning('已存在方法编号【' + newCode + '】，请修改');
                    return false;
                }
            }
        }

        return true;
    }
}
