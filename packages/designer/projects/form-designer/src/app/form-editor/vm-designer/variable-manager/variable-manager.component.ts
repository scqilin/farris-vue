import {
    Component,
    HostBinding,
    OnInit,
    TemplateRef,
    ViewChild
} from '@angular/core';
import { DomService, FormSchema, FormVariable, FormVariableCategory, FormVariableTypes, FormViewModel, SchemaService } from '@farris/designer-services';
import { NotifyService } from '@farris/ui-notify';
import { IdService } from '@farris/ui-common';
import { DataColumn, DatagridComponent } from '@farris/ui-datagrid';
import { of } from 'rxjs';
import { createVariableDataGridColumn } from './column';
import { ViewModelNameService } from '../method-manager/services/view-model-name.service';

const ROOT_VIEW_MODEL_ID = 'root-viewmodel';
@Component({
    selector: 'app-variable-manager',
    templateUrl: 'variable-manager.component.html',
    styleUrls: ['variable-manager.component.css']
})
export class VaribleManagerComponent implements OnInit {
    /** 列表列配置 */
    columns = [];
    /** 变量类别枚举 */
    private categoryTypes = [{ text: '组件变量', value: 'locale' }, { text: '表单变量', value: 'remote' }];

    /** 当前表单内所有的变量，不区分视图模型 */
    variableList = [];

    showAllCheckbox = true;
    @ViewChild('opCell') opCell: TemplateRef<any>;
    @ViewChild('dg') dg: DatagridComponent;

    constructor(
        private notifyService: NotifyService,
        private domService: DomService,
        private schemaService: SchemaService) { }

    ngOnInit(): void {
        this.columns = createVariableDataGridColumn(this.opCell);
    }
    initVariableList() {
        this.variableList = [];
        if (!this.domService.module) {
            return;
        }
        this.getRemoteVariables();
        this.getLocalVariables();
    }
    /**
     * 获取表单变量
     */
    private getRemoteVariables() {
        const rootVm = this.domService.viewmodels[0];
        if (rootVm && rootVm.states) {
            const remoteVars = rootVm.states.filter(s => s.category === FormVariableCategory.remote);
            if (remoteVars && remoteVars.length) {
                remoteVars.forEach(state => {
                    const nodeData = Object.assign({ hId: state.id, viewModelId: rootVm.id, sourceName: '视图对象' }, state);
                    this.variableList.push(nodeData);
                });
            }
        }

    }
    /**
     * 获取组件变量
     */
    private getLocalVariables() {
        let hasLocalVar = false;
        this.domService.viewmodels.forEach(vm => {
            if (!vm.states || !vm.states.length) {
                return;
            }
            vm.states.forEach(state => {
                if (state.category === FormVariableCategory.locale) {
                    const sourceName = this.getViewModelSourceName(vm, state);
                    const hId = new IdService().generate();
                    const nodeData = Object.assign({ hId, viewModelId: vm.id, sourceName }, state);
                    this.variableList.push(nodeData);
                    hasLocalVar = true;
                }

            });
        });

        this.showAllCheckbox = hasLocalVar;
    }
    /**
     * 格式化单元格显示文本
     * @param row 行数据
     */
    format(row: any) {
        const displayText = row.rowData[row.field];
        if (row.field === 'type') {
            const data = FormVariableTypes.find(c => c.value === displayText);
            return data ? data.text : displayText;
        }
        if (row.field === 'category') {
            const data = this.categoryTypes.find(c => c.value === displayText);
            return data ? data.text : displayText;
        }
        return displayText;
    }
    /**
     * 同步表单变量（vo->schema）
     */
    onRefresh() {
        const voId = this.domService.module.schemas[0].id;
        const sessionId = '';
        this.schemaService.converVO2Schema(voId, sessionId).subscribe((newSchema: FormSchema) => {
            if (!newSchema) {
                this.notifyService.error('更新失败！');
            } else {
                const newVariables = newSchema.variables || [];
                this.domService.updateRemoteVariables(newVariables);
                this.initVariableList();
                this.dg.loadData(this.variableList);
                this.notifyService.success('更新成功！');

                this.dg.clearAll();
                this.dg.clearCheckeds();
            }

        });

    }
    /**
     * 表单变量所在的行需要禁用
     */
    disableRowFn = (row) => {
        return row.category === FormVariableCategory.remote;
    }

    /**
     * 添加变量，默认添加到根组件
     */
    onAddElement() {
        const newData = new FormVariable();
        newData.id = new IdService().generate();
        newData['hId'] = new IdService().generate();
        newData.type = 'String';

        this.variableList.push(Object.assign({ viewModelId: ROOT_VIEW_MODEL_ID, sourceName: '根组件' }, newData));
        this.dg.loadData(this.variableList);

        this.showAllCheckbox = true;
    }


    onDeleteElement() {
        const checkeds = this.dg.checkeds;
        if (!checkeds || !checkeds.length) {
            this.notifyService.info('请勾选要删除的行！');
            return;
        }
        const remoteVariableIndex = checkeds.findIndex(c => c.data && c.data.category === FormVariableCategory.remote);
        if (remoteVariableIndex > -1) {
            this.notifyService.info('不可删除表单变量！');
        }
        checkeds.forEach(c => {
            if (c.data.category === FormVariableCategory.locale) {
                this.variableList = this.variableList.filter(e => c.data.hId !== e.hId);
                this.syncViewModelAfterDelete(c.data);

            }
        });

        this.dg.loadData(this.variableList);
        this.dg.clearAll();
        this.dg.clearCheckeds();

        const localIndex = this.variableList.findIndex(v => v.category === FormVariableCategory.locale);
        this.showAllCheckbox = localIndex >= 0;

    }


    afterGridEdit = (rowIndex: number, rowData: any, column?: DataColumn, editor?: any) => {
        // 校验编号唯一
        if (column.field === 'code') {
            if (!this.checkUniqueCode({ code: editor.formControl.value, id: rowData.id })) {
                return of(false);
            }
        }
        // 去除空格，不可为空
        if (column.field === 'code' && (!editor.formControl.value || !editor.formControl.value.trim())) {
            // this.notifyService.warning('变量编号不能为空');
            return of(false);
        }
        // 去除空格，不可为空
        if (column.field === 'name' && (!editor.formControl.value || !editor.formControl.value.trim())) {
            // this.notifyService.warning('变量名称不能为空');
            return of(false);
        }
        return of(true);

    }
    /**
     * 编辑结束后事件
     */
    endEdit(res: any) {
        this.syncViewModelAfterEdit(res.rowData, {
            field: res.column.field,
            value: res.value,
        });
    }

    private syncViewModelAfterEdit(rowData, changeObject) {
        // 校验不通过时，单元格重置为编辑态
        if (changeObject.field === 'code' && !this.checkUniqueCode(rowData)) {
            return;
        }
        const code = rowData.code && rowData.code.trim();
        const name = rowData.name && rowData.name.trim();
        if (!code || !name || !rowData.type) {
            return;
        }

        // 若当前行已填写完毕，则同步视图模型
        const viewModelId = rowData.viewModelId || ROOT_VIEW_MODEL_ID;
        const vm = this.domService.getViewModelById(viewModelId);
        if (vm) {
            const stateInVM = vm.states.find(state => state.id === rowData.id);
            if (stateInVM) {
                Object.assign(stateInVM, {
                    code,
                    name,
                    type: rowData.type
                });
            } else {
                vm.states.push({
                    id: rowData.id,
                    code,
                    name,
                    type: rowData.type,
                    category: rowData.category
                });
            }
        }


    }
    private syncViewModelAfterDelete(rowData) {
        const viewModelId = rowData.viewModelId || ROOT_VIEW_MODEL_ID;

        const vm = this.domService.getViewModelById(viewModelId);
        vm.states = vm.states.filter(s => s.id !== rowData.id);
    }
    private checkUniqueCode(newData: any) {
        if (!newData.code) {
            return true;
        }
        if (this.variableList.findIndex(d => d.code === newData.code && d.id !== newData.id) > -1) {
            this.notifyService.warning('变量编号已存在，请修改');
            return false;
        }
        return true;
    }

    private getViewModelSourceName(viewModel: FormViewModel, state: FormVariable) {
        // 表单变量来源于vo
        if (state.category === FormVariableCategory.remote) {
            return '视图对象';
        }

        const vmNameService = new ViewModelNameService(this.domService);

        return vmNameService.getViewModelName(viewModel.id, viewModel.name);
    }
}
