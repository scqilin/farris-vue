import { Extension } from './extension';
import { ITreeNode } from './tree-node';
import { IOperationNode } from './operation-node';
import { Case } from './case';
import { ServiceRef } from './service-ref';

export class SwitchNode implements ITreeNode, IOperationNode {
    get data(): { id: string; name: string; } {
        return { id: this.id, name: this.name };
    }
    expanded?: boolean;
    get children(): ITreeNode[] {
        return this.cases;
    }

    id: string;
    code: string;
    name: string;

    cases: Array<Case>;

    belongedExt: Extension;

    root: ITreeNode;

    constructor(switchNodeJson?: any, extension?: Extension) {
        if (switchNodeJson) {
            this.parse(switchNodeJson, extension);
        }
    }

    parse(switchNodeJson: any, extension?: Extension) {
        this.id = switchNodeJson.id;
        this.code = switchNodeJson.code;
        this.name = switchNodeJson.name;
        // this.cases = new SwitchCases(switchNodeJson.cases, serviceRefs);
        this.cases = new Array();
        for (const caseJson of switchNodeJson.cases) {
            const casee = new Case(caseJson, extension); // case 是关键字。。。
            this.cases.push(casee);
        }

        if (extension) {
            this.belongedExt = extension;
        }
    }

    // parseFromWebCmdMetadata(branchCollection: BranchCollectionCommandItem, serviceRefs: Map<string, ServiceRef>) {
    //   this.id = branchCollection.Id;
    //   this.name = branchCollection.Name;
    //   this.cases = new Array();
    //   for (const branch of branchCollection.Items) {
    //     const casee = new Case();
    //     casee.parseFromWebCmdMetadata(branch, serviceRefs);
    //     this.cases.push(casee);
    //   }
    // }

    toJson(): any {
        const cases = [];
        for (const casee of this.cases) {
            cases.push(casee.toJson());
        }
        return {
            'id': this.id,
            'type': 'switchNode',
            'code': this.code,
            'name': this.name,
            'cases': cases
        };
    }
}
