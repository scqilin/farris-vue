export class ServiceRef {
    name: string;
    path: string;
    cmpId: string;
    isCommon: string;
    alias: string;

    constructor(serviceRefJson?: any) {
        if (serviceRefJson) {
            this.parse(serviceRefJson);
        }
    }

    private parse(serviceRefJson: any) {
        this.name = serviceRefJson.name;
        this.path = serviceRefJson.path;
        this.cmpId = serviceRefJson.cmpId;
        this.isCommon = serviceRefJson.isCommon;
        this.alias = serviceRefJson.alias;
    }

    toJson() {
        return {
            'name': this.name,
            'path': this.path,
            'cmpId': this.cmpId,
            'isCommon': this.isCommon,
            'alias': this.alias
        };
    }
}
