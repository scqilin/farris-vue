import { ParamConfig } from './param';
export class ITreeNode {
  data: {
    id: string;
    name: string;
    code?: string;
  };
  expanded?: boolean;
  children: ITreeNode[];
  parent?: ITreeNode;
  params?: ParamConfig[];
  root: ITreeNode;
}
