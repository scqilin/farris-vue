import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormBasicService } from '@farris/designer-services';
import { Injector } from '@angular/core';


export class FrameProp {

    constructor(private injector: Injector) { }

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 常本信息属性
        const usualPropConfig = this.getUsualPropConfig(propertyData);
        propertyConfig.push(usualPropConfig);


        return propertyConfig;

    }

    private getUsualPropConfig(propertyData: any): ElementPropertyConfig {
        const formBasicServ = this.injector.get(FormBasicService);
        if (propertyData.module && !propertyData.module.metadataId) {
            propertyData.module.metadataId = formBasicServ.formMetaBasicInfo.id;
        }

        return {
            categoryId: 'Basic',
            categoryName: '基本信息',
            propertyData: propertyData.module,
            enableCascade: true,
            parentPropertyID: 'module',
            properties: [
                {
                    propertyID: 'metadataId',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '表单元数据的id',
                    readonly: true
                },
                {
                    propertyID: 'code',
                    propertyName: '编号',
                    propertyType: 'string',
                    description: '表单元数据的编号',
                    readonly: true
                },
                {
                    propertyID: 'name',
                    propertyName: '名称',
                    propertyType: 'string',
                    description: '表单元数据的名称',
                    readonly: true
                },
                {
                    propertyID: 'showType',
                    propertyName: '展示形式',
                    propertyType: 'select', // 目前只支持弹框类卡片模板切换展示形式
                    iterator: [
                        { key: 'modal', value: '弹出表单' },
                        { key: 'page', value: '标签页' },
                        { key: 'sidebar', value: '侧边栏' }
                    ],
                    visible: propertyData.module.templateId === 'modal-card-template',
                    readonly: true
                }
            ], setPropertyRelates(changeObject: any, data, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'showType': {
                        changeObject.needRefreshForm = true;
                    }
                }
            }
        };
    }
}
