
import { IDesignerHost } from '@farris/designer-element';

export class DesignerHost extends IDesignerHost {
    private desigenrHost = new Map<string, any>();

    addService(serviceType: string, service: any) {

        this.desigenrHost.set(serviceType, service);

    }

    getService(serviceType: string) {
        return this.desigenrHost.get(serviceType);
    }
}
