import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomService } from '@farris/designer-services';

export class TemplateOutlineSchemaService {

    constructor(
        private http: HttpClient,
        private domService: DomService
    ) {

    }

    /**
     * 获取当前模板schema
     * @param type 控件类型
     */
    getTemplateSchema() {
        const domModule = this.domService.module;
        try {
            this.http.get<any[]>('assets/form/template-schema/' + domModule.templateId + '.json?version=0.0.1').subscribe((data) => {
                if (data && data.length) {
                    this.domService.templateOutlineSchema = data;
                }
            }, () => {
                // console.log('获取模板大纲schema失败，模板ID：' + this.domService.module.templateId);
            });
        } catch (e) { }

    }
}
