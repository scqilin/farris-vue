

/**
 * 设计器左侧tab页签
 */
export enum LEFT_TAB_TYPE {

    /** 控件树节点--设计时 */
    controlTree = 'controlTree',

    /** 控件工具箱 */
    controllist = 'controllist',

    /** schema实体树 */
    schemaTree = 'schemaTree',

}

/**
 * 右侧为控件设计器时，区分三种子页面
 */
export enum BOTTOM_TAB_TYPE {

    /**
     * 可视化设计器
     */
    formDesigner = 'formDesigner',

    /**
     * 代码编辑器
     */
    codeEditor = 'codeEditor',
    /**
     * 样式编辑器
     */
    classEditor = 'classEditor',

    /**
     * 表单设置
     */
    formSetting = 'formSetting'
}

