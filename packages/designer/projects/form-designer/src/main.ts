import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

declare global {
    interface Window {
        /** 是否同时保存表单视图和代码视图 */
        saveFormAndCodeView: boolean;

        /** 是否挂起表单视图中DOM的变更检测 */
        suspendChangesOnForm: boolean;

        /** 表单视图是否有DOM的变更 */
        isFormMetadataChanged: boolean;

        /** 代码视图是否有变更 */
        isFormCodeViewChanged: boolean;
    }
}

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .catch((err) => console.error(err));
