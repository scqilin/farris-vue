
export class CmpMethodParamConfig{
    ParamCode:string;
    ParamName:string;
    ParamExpress:string;
}

export class CmpMethodParamConfigConvert{

    InitFromJobject(jsonObj: Object): CmpMethodParamConfig {
        let cmpMethodParamConfig = new CmpMethodParamConfig();
        cmpMethodParamConfig.ParamCode = jsonObj["ParamCode"];
        cmpMethodParamConfig.ParamName = jsonObj["ParamName"];
        cmpMethodParamConfig.ParamExpress = jsonObj["ParamExpress"];
        return cmpMethodParamConfig;
    }

    ConvertJObject(obj: CmpMethodParamConfig): Object {
        let cmpMethodParamConfig: CmpMethodParamConfig = obj as CmpMethodParamConfig;
        let jobj = new Object();
        jobj["ParamCode"] = cmpMethodParamConfig.ParamCode;
        jobj["ParamName"] = cmpMethodParamConfig.ParamName;
        jobj["ParamExpress"] = cmpMethodParamConfig.ParamExpress;
        return jobj;
    }
}
