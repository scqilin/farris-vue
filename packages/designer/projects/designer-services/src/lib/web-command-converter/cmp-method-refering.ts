import { CmpMethodParamConfig, CmpMethodParamConfigConvert } from "./cmp-method-param-config";
import { ICommandItem } from "./icommand-item";
import { CommandItemType } from "./command-item-type";

export class CmpMethodRefering implements ICommandItem {
    
    Id:string;
    Code:string;
    Name:string;
    ComponentId: string;
    ComponentCode: string;
    ComponentName: string;
    ComponentPath: string;
    MethodId: string;
    MethodCode: string;
    MethodName: string;
    IsReplaced:boolean;
    IsBeforeExpansion:boolean;
    IsAfterExpansion:boolean;
    ParamConfigs: Array<CmpMethodParamConfig>;

    GetItemType(): CommandItemType {
        return CommandItemType.MethodRefer;
    }
    GetItemCode(): string {
        return this.Code;
    }
    GetItemName(): string {
        return this.Name;
    }
    GetItemId(): string {
        return this.Id;
    }
}

export class CmpMethodReferingConvert{

    InitFromJobject(jsonObj: Object): CmpMethodRefering {
        let cmpMethodRefering = new CmpMethodRefering();
        cmpMethodRefering.Id = jsonObj["Id"];
        cmpMethodRefering.Code = jsonObj["Code"];
        cmpMethodRefering.Name = jsonObj["Name"];
        cmpMethodRefering.MethodId = jsonObj["MethodId"];
        cmpMethodRefering.MethodCode = jsonObj["MethodCode"];
        cmpMethodRefering.MethodName = jsonObj["MethodName"];
        cmpMethodRefering.ComponentId = jsonObj["ComponentId"];
        cmpMethodRefering.ComponentCode = jsonObj["ComponentCode"];
        cmpMethodRefering.ComponentName = jsonObj["ComponentName"];
        cmpMethodRefering.ComponentPath = jsonObj["ComponentPath"];
        cmpMethodRefering.IsReplaced = jsonObj["IsReplaced"];
        cmpMethodRefering.IsBeforeExpansion = jsonObj["IsBeforeExpansion"];
        cmpMethodRefering.IsAfterExpansion = jsonObj["IsAfterExpansion"];
        if (jsonObj["ParamConfigs"] != null) {
            cmpMethodRefering.ParamConfigs=new Array<CmpMethodParamConfig>();
            let convertor = new CmpMethodParamConfigConvert();
            jsonObj["ParamConfigs"].forEach(element => {
                cmpMethodRefering.ParamConfigs.push(convertor.InitFromJobject(element));
            });
        }
        return cmpMethodRefering;
    }

    ConvertJObject(obj: CmpMethodRefering): Object {
        let cmpMethodRefering: CmpMethodRefering = obj as CmpMethodRefering;
        let jobj = new Object();
        jobj["Id"] = cmpMethodRefering.Id;
        jobj["Code"] = cmpMethodRefering.Code;
        jobj["Name"] = cmpMethodRefering.Name;
        jobj["MethodId"] = cmpMethodRefering.MethodId;
        jobj["MethodCode"] = cmpMethodRefering.MethodCode;
        jobj["MethodName"] = cmpMethodRefering.MethodName;
        jobj["ComponentId"] = cmpMethodRefering.ComponentId;
        jobj["ComponentCode"] = cmpMethodRefering.ComponentCode;
        jobj["ComponentName"] = cmpMethodRefering.ComponentName;
        jobj["ComponentPath"] = cmpMethodRefering.ComponentPath;
        jobj["IsReplaced"] = cmpMethodRefering.IsReplaced;
        jobj["IsBeforeExpansion"] = cmpMethodRefering.IsBeforeExpansion;
        jobj["IsAfterExpansion"] = cmpMethodRefering.IsAfterExpansion;
        if (cmpMethodRefering.ParamConfigs != null) {
            let CommandsJArry = jobj["ParamConfigs"] = [];
            let convertor = new CmpMethodParamConfigConvert();
            cmpMethodRefering.ParamConfigs.forEach(paramConfig => {
                CommandsJArry.push(convertor.ConvertJObject(paramConfig));
            });
        }
        return jobj;
    }
}