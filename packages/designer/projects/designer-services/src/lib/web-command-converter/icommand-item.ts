import { CommandItemType } from "./command-item-type";
import { CmpMethodRefering, CmpMethodReferingConvert } from "./cmp-method-refering";
import { BranchCommandItemConvertor, BranchCommandItem } from "./branch-command-item";
import { BranchCollectionCommandItemConvertor, BranchCollectionCommandItem } from "./branch-collection-command-item";
export interface ICommandItem
{
     GetItemType():CommandItemType;

     GetItemCode():string;

     GetItemName():string;

     GetItemId():string;
}

export class CommandItemConvertor {
    ConvertJObject(obj: ICommandItem): Object {
        let commandItem = obj as ICommandItem;
        let itemType = commandItem.GetItemType();
        let jobj = new Object();
        jobj["Type"] = itemType;
        if (itemType == CommandItemType.MethodRefer) {
            jobj["Content"] = commandItem;
        }
        else if (itemType == CommandItemType.Branch) {
           let  branchConvertor=new BranchCommandItemConvertor();
           jobj["Content"] = branchConvertor.ConvertJObject(commandItem as BranchCommandItem);
        }
        else if (itemType == CommandItemType.BranchCollection) {
            let  branchCollectionConvertor=new BranchCollectionCommandItemConvertor();
            jobj["Content"] = branchCollectionConvertor.ConvertJObject(commandItem as BranchCollectionCommandItem);
        }
        return jobj;
    }

    InitFromJobject(jsonObj: Object): ICommandItem {
        let itemType=jsonObj["Type"] as CommandItemType;
        let content:ICommandItem=jsonObj["Content"]; 
        if (itemType == CommandItemType.MethodRefer) {
            return Object.assign(new CmpMethodRefering(),content as CmpMethodRefering);;
        }
        else if (itemType == CommandItemType.Branch) {
            let  branchConvertor=new BranchCommandItemConvertor();
            return branchConvertor.InitFromJobject(jsonObj["Content"]);
        }
        else if (itemType == CommandItemType.BranchCollection) {
            let  branchCollectionConvertor=new BranchCollectionCommandItemConvertor();
            return branchCollectionConvertor.InitFromJobject(jsonObj["Content"]);
        }
        return null;
    }
}