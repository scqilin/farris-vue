import { WebCommand, WebCommandConvertor } from "./command";
import { ExtendProperty, ExtendsConvert } from "./extend-property";

export class WebCommandMetadata {
    Id: string;
    Code: string;
    Name: string;
    Description: string;
    Extends:ExtendProperty;
    Commands: Array<WebCommand>;
}

export class WebCommandMetadataConvertor {
    ConvertJObject(obj: WebCommandMetadata): Object {
        let metadata: WebCommandMetadata = obj as WebCommandMetadata;
        let jobj = new Object();
        jobj["Id"] = metadata.Id;
        jobj["Code"] = metadata.Code;
        jobj["Name"] = metadata.Name;
        jobj["Description"] = metadata.Description;
        if (metadata.Commands != null) {
            let CommandsJArry = jobj["Commands"] = [];
            let convertor = new WebCommandConvertor();
            metadata.Commands.forEach(command => {
                CommandsJArry.push(convertor.ConvertJObject(command));
            });
        }
        if(metadata.Extends!=null){
            let convertor = new ExtendsConvert();
            jobj["Extends"] =convertor.ConvertJObject(metadata.Extends);
        }
        return jobj;
    }

    InitFromJobject(jsonObj: Object): WebCommandMetadata {
        let metadata = new WebCommandMetadata();
        metadata.Id = jsonObj["Id"];
        metadata.Code = jsonObj["Code"];
        metadata.Name = jsonObj["Name"];
        metadata.Description = jsonObj["Description"];
        let CommandsJArry = jsonObj["Commands"];
        if (CommandsJArry != null) {
            metadata.Commands = new Array<WebCommand>();
            CommandsJArry.forEach(element => {
                let cmpOpSerializer = new WebCommandConvertor();
                metadata.Commands.push(cmpOpSerializer.InitFromJobject(element) as WebCommand);
            });
        }
        let convertor = new ExtendsConvert();
        metadata.Extends=convertor.InitFromJobject(jsonObj["Extends"]);
        return metadata;
    }
}