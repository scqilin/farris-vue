
export class CmdParameter{
    Id:string;
    Code:string;
    Name:string;
    Description:string;
    ParameterType:string;
    IsRetVal:boolean;
    EditorType: string;
}
 