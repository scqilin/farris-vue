import { CommandItemType } from "./command-item-type";
import { ICommandItem, CommandItemConvertor } from "./icommand-item";
import { ConditionType } from "./condition-type";

export class BranchCommandItem implements ICommandItem {
    Id: string;
    Code:string;
    Name: string;
    ConditionType: ConditionType;
    Express: string;
    Items: Array<ICommandItem>;

    GetItemId(): string {
        return this.Id;
    }

    GetItemCode(): string {
        return this.Code;
    }

    GetItemName(): string {
        return this.Name;
    }

    GetItemType(): CommandItemType {
        return CommandItemType.Branch;
    }
}

export class BranchCommandItemConvertor {
    ConvertJObject(obj: BranchCommandItem): Object {
        let branchItem = obj as BranchCommandItem;
        let jobj = new Object();
        jobj["Id"] = branchItem.Id;
        jobj["Code"] = branchItem.Code;
        jobj["Name"] = branchItem.Name;
        jobj["ConditionType"] = branchItem.ConditionType;
        jobj["Express"] = branchItem.Express;
        if (branchItem.Items != null) {
            let itemArray = [];
            let itemConvertor = new CommandItemConvertor();
            branchItem.Items.forEach(element => {
                itemArray.push(itemConvertor.ConvertJObject(element));
            });
            jobj["Items"]=itemArray;
        }
        return jobj;
    }

    InitFromJobject(jsonObj: Object): BranchCommandItem {
        let branchItem = new BranchCommandItem();
        branchItem.Id = jsonObj["Id"];
        branchItem.Code = jsonObj["Code"];
        branchItem.Name = jsonObj["Name"];
        branchItem.ConditionType = jsonObj["ConditionType"];
        branchItem.Express = jsonObj["Express"];
        if (jsonObj["Items"] != null) {
            branchItem.Items=new Array<ICommandItem>();
            let itemConvertor = new CommandItemConvertor();
            jsonObj["Items"].forEach(element => {
                branchItem.Items.push(itemConvertor.InitFromJobject(element));
            });
        }
        return branchItem;
    }
}
