import {
    FormDeclaration, Subscription, FormExpression, FormComponent,
    FormViewModel, FormExternalComponent, FormWebCmd, FormStateMachine
} from './dom-entity';
import { FormSchema } from './schema';

/** 设计器运行环境 */
export enum DesignerEnvType {
    /**
     * 设计时
     */
    designer = 'designer',
    /**
     * 运行时定制环境
     */
    runtimeCustom = 'runtimeCustom',
    /**
     * 移动审批环境
     */
    mobileDesigner = 'mobileDesigner',
    /**
     * 移动审批环境
     */
    mobileApprove = 'mobileApprove',

    /**
     * 零代码设计环境
     */
    noCode = 'noCode',

}


export class FormMetaDataContent {
    Id: string;
    Code: string;
    Name: string;
    Contents: FormMetadaDataDom;
}

export class FormMetaDataContentForDB {
    Id: string;
    Code: string;
    Name: string;
    Contents: string;

    constructor(content: FormMetaDataContent) {
        this.Id = content.Id;
        this.Code = content.Code;
        this.Name = content.Name;
        this.Contents = JSON.stringify(content.Contents);
    }
}

export class FormMetadaDataDom {
    module: FormMetaDataModule;
    options?: FormOptions;
}

export class FormMetaDataModule {
    id: string;
    code: string;
    name: string;
    caption: string;
    type: string;
    creator: string;
    creationDate: Date;
    updateVersion: string;
    // 暂时不用
    showTitle: boolean;
    // 暂时不用
    bootstrap: string;
    // schema
    schemas: Array<FormSchema>;

    // 状态机
    stateMachines: Array<FormStateMachine>;

    // 视图模型
    viewmodels: Array<FormViewModel>;

    // 源组件-事件-命令-目标组件的映射关系
    actions: Array<any>;

    // 组件
    components: Array<FormComponent>;

    // 构件
    webcmds: Array<FormWebCmd>;

    serviceRefs: Array<any>;

    // 表单所属模板
    templateId: string;

    // 是否组合表单
    isComposedFrm: boolean;

    // 表单所在的工程名
    projectName: string;

    // 自定义样式
    customClass: any;

    // 外部模块声明
    extraImports: Array<{ name: string, path: string }>;

    // 外部组件
    externalComponents: Array<FormExternalComponent>;

    // 当前组件声明：事件、变量、命令
    declarations: FormDeclaration;

    // 订阅
    subscriptions: Array<Subscription>;

    /** 表达式配置 */
    expressions: FormExpression[];

    // 当前表单的展示形式：modal|page|sidebar
    showType?: string;

    // 页面级按钮配置（目前用于Header和ModalFooter组件内部的工具栏按钮）
    toolbar: {
        items: { [viewModelId: string]: any };
        configs: { modal?: any, page?: any, sidebar?: any },
    };

    qdpInfo: any;

    /** 表单元数据id */
    metadataId?: string;

    /** 用于零代码设计器 */
    oaVersion?: string;
}


export class FormOptions {
    /**
     * 启用静态文本
     */
    enableTextArea?: boolean;
    /**
     * 启用拖拽调整布局
     */
    enableDragAndDropToModifyLayout?: boolean;

    publishFormProcess?: boolean;

    /**
     * 界面渲染模式：编译（生成代码并编译），动态渲染（动态解析，本地不生成代码，不编译）
     */
    renderMode?: 'compile' | 'dynamic';

    /** 变更集提交策略 */
    changeSetPolicy?: 'entire' | 'valid';

    /** 启用服务器端变更检测：菜单或应用关闭前调用后端接口确认后端缓存中的数据是否已经保存并提示用户 */
    enableServerSideChangeDetection?: boolean;

    /** 生成表单代码时将之前的源码都删除 */
    enableDeleteSourceCode?: boolean;

    /** 表单是否可以被组合 */
    canBeComposed?: boolean;
}
