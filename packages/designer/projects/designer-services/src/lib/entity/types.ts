export interface Node {
  id: string;
  type: string;
  __parentId__?: string;
  contents?: Node[];
  /**
   * 组件类型
   */
  componentType?: string;
  [prop: string]: any;
}
export enum NodeType {
  /**
   * 隐藏区域
   */
  HiddenContainer = 'HiddenContainer',
  /**
   * 帮助控件
   */
  LookupEdit = 'LookupEdit',
  /**
   * 组件
   */
  Component = 'Component'
}