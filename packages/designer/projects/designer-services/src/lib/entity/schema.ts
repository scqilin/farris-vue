import { EnumData } from './dom-entity';
import { GSPElementDataType } from '@gsp-bef/gsp-cm-metadata';
/**
 * schema
 */
export class FormSchema {
    // public dataSource: string;
    public sourceUri: string;
    public id: string;
    public code: string;
    public name: string;
    public entities: FormSchemaEntity[];
    public variables: FormSchemaEntityField[];
    public eapiId: string;
    public extendProperties: { enableStdTimeFormat: boolean };

    public eapiCode?: string;
    public eapiName?: string;
    public eapiNameSpace?: string;
    public voPath?: string;
    public voNameSpace?: string;
}

/**
 * 实体
 */
export class FormSchemaEntity {
    public id: string;
    public code: string;
    public name: string;
    public label: string;
    public type: FormSchemaEntityType;
}

/**
 * 实体类型对象
 */
export class FormSchemaEntityType {
    public $type = 'EntityType';
    public name: string;
    public primary: string;
    public fields: FormSchemaEntityField[];
    public entities?: FormSchemaEntity[];
    public displayName?: string;
}

/**
 * 字段
 */
export class FormSchemaEntityField {
    public $type: FormSchemaEntityField$Type;
    public id: string;
    public originalId: string;
    public code: string;
    public label: string;
    public bindingField: string;
    public name: string;
    public defaultValue?: any;
    public require?: boolean;
    public readonly?: boolean;
    public type: FormSchemaEntityFieldType;
    public editor?: FormSchemaEntityFieldEditor;
    public path?: string;
    public bindingPath?: string;
    public multiLanguage?: boolean;

    // 表达式
    public expression?: any;
}

/**
 * 字段类型对象
 */
export class FormSchemaEntityFieldType {
    public $type: FormSchemaEntityFieldType$Type;
    public name: FormSchemaEntityFieldTypeName | any;
    public length?: number;
    public precision?: number;
    public valueType?: FormSchemaEntityFieldType;
    public enumValues?: EnumData[];
    public fields?: FormSchemaEntityField[];
    public displayName?: string;
    public primary?: string;
    public entities?: FormSchemaEntity[];
    // 用于区分日期/日期事件和整型/浮点型数据
    public elementType?: GSPElementDataType;


    // 扩展属性:  运行时定制用
    public extendProperty?: any;
}

/**
 * 字段编辑器对象
 */
export class FormSchemaEntityFieldEditor {
    public $type: string;
    [propName: string]: any;
}


/**
 * 字段类型枚举
 */
export enum FormSchemaEntityField$Type {
    /**
     * 简单类型字段
     */
    SimpleField = 'SimpleField',
    /**
     * 关联/UDT类型字段
     */
    ComplexField = 'ComplexField'
}

/**
 * 字段类型对象中的类型枚举
 */
export enum FormSchemaEntityFieldType$Type {

    /**
     * 字符串
     */
    StringType = 'StringType',
    /**
     * 备注
     */
    TextType = 'TextType',
    /**
     * 数字（整数、浮点数）
     */
    NumericType = 'NumericType',
    /**
     * 布尔
     */
    BooleanType = 'BooleanType',
    /**
     * 日期
     */
    DateType = 'DateType',

    /**
     * 日期时间
     */
    DateTimeType = 'DateTimeType',

    /**
     * 枚举
     */
    EnumType = 'EnumType',
    /**
     * 实体类
     */
    EntityType = 'EntityType',

    /**
     * 分级码
     */
    HierarchyType = 'HierarchyType',

    /**
     * 对象
     */
    ObjectType = 'ObjectType',

    /**
     * 数字（大数据）
     */
    BigNumericType = 'BigNumericType'
}


/**
 * 字段类型中的名称
 */
export enum FormSchemaEntityFieldTypeName {
    /**
     * 简单类型字段
     */
    String = 'String',
    /**
     * 日期时间
     */
    DateTime = 'DateTime',
    /**
     * 日期
     */
    Date = 'Date',
    /**
     * 枚举
     */
    Enum = 'Enum',
    /**
     * 布尔
     */
    Boolean = 'Boolean',

    /**
     * 数字
     */
    Number = 'Number',

    /**
     * 备注
     */
    Text = 'Text',

    /**
     * 大数字
     */
    BigNumber = 'BigNumber'
    /**
     * 人员
     */
}
