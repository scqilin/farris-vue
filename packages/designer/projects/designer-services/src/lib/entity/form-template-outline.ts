
export class FormTempateOutlineSchema {
    /** 节点id */
    id: string;

    /** 节点类型 */
    type: string;

    /**  节点展示名称 */
    name: string;

    /** 节点在dom中的路径 */
    path: string | string[];

    /** 图标，用于header区域 */
    icon?: string;

    /** 是否卡片容器（用于卡片模板like-card-container 容器） */
    hasLikeCardContainer?: boolean;

    /** 是否集合节点，用于section列表区域 */
    isGridCollection?: boolean;

    /** 子节点 */
    children?: Array<FormTempateOutlineSchema>;

    /** 多视图配置，用于列表区域 */
    multiView: { canEnable: boolean };

    /** 筛选条配置，用于列表区域 */
    listFilter: { canEnable: boolean };

    /** 筛选方案配置 */
    queryScheme: {

        canEnable: boolean,

        /** 筛选方案放置的位置 */
        position: string,

        /** 添加筛选方案后表单其他的节点需要修改class */
        relatedClass: [{

            /** 需要增加class 的节点路径 */
            path: string,
            /** 增加的class */
            class: string,
            /** 追加class或者移除class ---目前没有用到，都是增加class  */
            type: string
        }]
    };
}
