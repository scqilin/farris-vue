import { DesignViewModelField } from './design-viewmodel';
import { FormVariable } from './dom-entity';
import { FormSchemaEntityField, FormSchemaEntity } from './schema';

/**
 * 拖拽上下文
 */
export class ComponentResolveContext {
    /**
     * 拖拽来源
     * @summary
     * 控件工具箱control/实体树字段field/实体树实体entity/ 现有控件移动位置move
     */
    sourceType: 'control' | 'field' | 'entity' | 'move';

    /**
     * 拖拽控件类型
     */
    controlType: string;

    /**
     * 拖拽控件的中文类型名称
     */
    controlTypeName: string;
    /**
     * 拖拽控件分类
     */
    controlCategory: string;

    /**
     * 绑定对象类型（字段/实体/小部件）
     */
    bindingType: 'field' | 'entity' | 'widget';

    /**
     * 字段绑定路径
     */
    bindingPath: string;

    /**
     * 绑定字段标识
     */
    bindingTargetId: string;

    /**
     * 目标区域是否在侧边栏中
     */
    bindingTargetInSidebar?: boolean;

    /**
     * 当前移动的组件实例（用于组件移动位置）
     */
    componentInstance?: any;

    /**
     * 目标容器的组件实例
     */
    parentComponentInstance?: any;

    /**
     * 在父组件中的位置
     */
    positionInParent?: number;

    /**
     * 绑定信息
     */
    bindingSourceContext?: ComponentBindingSourceContext;

    /**
     * 要添加的控件Schema
     */
    componentSchema: any;

    /**
     * 允许选择的实体级别，不填则不限制。
     * @summary
     *  1：主表；2：从表；3从从表
     */
    allowedEntityLevel?: number;

    /**
     * 展示字段列表是否需要排除掉已添加的字段
     * @summary
     * 用于添加卡片组件时排除掉其他卡片中已有的字段
     */
    needExcludeDisplayedFields?: boolean;

    /**
     * 实体列表是否需要排除掉已使用的实体
     */
    needExcludeDisplayedEntity?: boolean;

    /** 工具箱中的控件，启用的特性 */
    controlFeature: any;

    /** 拖拽组件后需要追加到schema中的实体结构 */
    addedEntity?: FormSchemaEntity;

    /** 拖拽组件后需要追加到schema中的实体结构 */
    addedEntityField?: FormSchemaEntityField;

    /** 控件默认映射的字段类型 */
    defaultFieldType: string;

    /** 控件默认映射的udt */
    defaultUdtId: string;

    /** 控件工具箱是否启用严格的匹配模式。例如严格模式下日期控件只能选择日期字段，非严格模式下，日期控件可以选择日期字段或字符串字段  */
    useStrictMappingRule?: boolean;

    /** 工具箱中的模板类控件信息，目前应用于零代码设计器 */
    controlTemplate?: {

        /** 模板id */
        templateId: string;

        /** 模板类控件DOM 结构 */
        templateDom?: any;

        /** 模板类控件分类：input/container */
        templateCategory?: string;

        /** 控件需要绑定的字段信息 */
        bindingFields?: FormSchemaEntityField[]
    }
}


/**
 * 绑定上下文
 */
export class ComponentBindingSourceContext {
    bindingType: 'field' | 'entity';
    componentNode?: any;

    /**
     * 组件引用节点
     */
    componentRefNode?: any;

    /**
     * 视图模型节点
     */
    viewModelNode?: any;

    /** 控件绑定的实体schema字段 */
    entityFieldNode?: FormSchemaEntityField;

    /**
     * 实体schema字段对应的DesignViewModel结构
     */
    designViewModelField?: DesignViewModelField;

    /**
     * 变量字段节点
     */
    variableFieldNode?: DesignFormVariable;

    extendInfo?: { [key: string]: any };

    /**
     * 要绑定的实体
     */
    bindingEntity?: FormSchemaEntity;

    /**
     * 要绑定的字段集合
     */
    bindingEntityFields?: FormSchemaEntityField[];

}

/**
 * 表单变量
 */
class DesignFormVariable extends FormVariable {

    /**
     * 分组ID
     */
    groupId: string;

    /**
     * 分组名称
     */
    groupName: string;
}
