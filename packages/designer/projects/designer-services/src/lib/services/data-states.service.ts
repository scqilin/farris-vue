import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash-es';



/**
 * 数据状态服务
 */

@Injectable({
    providedIn: 'root'
})
export class DataStatesService {
    constructor() {
    }
    dataStates: any;
    dataStatesBindPath: any;
    getDomJson(domJson) {
        this.dataStates = [];
        domJson.module.schemas[0].entities[0].type.fields.forEach(item => {
            // 识别支持数据状态的情况; ComplexField:业务字段的类型
            if (item.$type == 'ComplexField') {
                if (item.type.displayName == '状态' && item.type.name.includes('BillState')) {
                    this.dataStates = cloneDeep(item.type.fields[0].type.enumValues);
                    const bindingPath = item.type.fields[0].bindingPath || '';
                    this.dataStatesBindPath = bindingPath.split('.')[0];
                    return this.dataStates, this.dataStatesBindPath;
                }
            }
        })
        return this.dataStates, this.dataStatesBindPath;
    }
}
