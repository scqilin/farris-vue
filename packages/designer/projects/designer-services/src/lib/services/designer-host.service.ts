import { Injectable } from '@angular/core';
import { IDesignerHost, IControlService } from '@farris/designer-element';

/** 控件列表 */
export let DgControl: any = {};

/**
 * 接收service
 */
@Injectable({
    providedIn: 'root'
})
export class DesignerHostSettingService {

    public designerHost: IDesignerHost;

    /** 控件相关服务 */
    public controlService: IControlService;

    /** 创建控件相关服务 */
    public controlCreatorService: any;


    setDesignerHost(designerHost: IDesignerHost) {
        this.designerHost = designerHost;

        this.controlService = this.designerHost.getService('ControlService');

        this.controlCreatorService = this.designerHost.getService('ControlCreatorService');

        DgControl = this.controlService.getDgControl();
    }
}
