import { Injectable } from '@angular/core';
import { DesignerEnvType } from '../entity';
import { FarrisMetadataDto } from './metadata-service';

/**
 * 存储表单设计器基本信息
 */
@Injectable({
    providedIn: 'root'
})
export class FormBasicService {
    /** 表单元数据信息基本信息 */
    formMetaBasicInfo: FarrisMetadataDto;

    /** 当前设计器环境 */
    envType: DesignerEnvType | any;

    /** 表单调试状态，零代码设计器专用 */
    debugModel?: boolean;

    /** 零代码设计器驱动模式：form:表单驱动；entity:实体驱动 */
    driveMode?: string;

}


