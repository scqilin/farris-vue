
export interface Actions {
    sourceComponent: {
        id: string,
        viewModelId: string,
        map: MapItem[],
    },
    path: string
};

export interface MapItem {
    event: {
        label: string,
        name: string,
    },
    command: {
        id: string,
        label: string,
        name: string,
        handlerName: string,
        params?: any,
        showTargetComponent?: boolean;
        targetComponentId?: string;
        isNewGenerated?: boolean,
        isInvalid: boolean,
        isRTCmd?: boolean,
    },
    controller: {
        id: string,
        label: string,
        name: string,
    },
    targetComponent: {
        id: string,
        viewModelId: string,
    },

}

export interface ControllerListItem {
    label: string,
    name: string,
    id: string,
    handlerName: string,
    /** 当前命令需要选择目标组件*/
    showTargetComponent: boolean,
    cmpId: string,
    componentLists: any,
    targetComponent: any,
    isNewGenerated: any,
    isInvalid: boolean,
    property: any,
    isRTCmd?: boolean,
}