import { FormBasicService } from '../services/form-basic.service';
import { Injectable } from '@angular/core';
// import { GSPMetadataService } from '@gsp-lcm/metadata-selector';
import { cloneDeep } from 'lodash-es';
import { Observable } from 'rxjs';
import { DomService } from '../services/dom.service';
import { DesignerEnvType } from '../entity/form-metadata';
import { NotifyService } from '@farris/ui-notify';
// import { GSPMetadataRTService } from '@gsp-lcm/metadatart-selector';
import { FormStateMachine } from '../entity/dom-entity';
import { FarrisMetadataService } from './metadata-service';
class StateMetadaDataDom {
    id: string;
    name: string;
    exist: string;
}


/**
 * 集成多值属性编辑器组件中的状态机
 */

@Injectable({
    providedIn: 'root'
})
export class StateMachineService {
    private _action: { [actionName: string]: { name: string, transitTo: string, description: string } };
    public get action() {
        return this._action;
    }
    stateMachineMiddleData: StateMetadaDataDom = {
        id: '',
        name: '',
        exist: ''
    };

    /** 状态机uri */
    stateMachineID: string;

    /* 状态机数组 */
    stateMachineMetaData: any;

    constructor(
        private formBasicService: FormBasicService,
        private domService: DomService,
        private notifyService: NotifyService,
        private metadataService: FarrisMetadataService
    ) { }


    /**
     * 获取状态机可选状态数据，用于属性面板配置
     */
    initStateMachineMetaData() {
        const stateMachines = this.domService.module.stateMachines;
        if (!stateMachines || !stateMachines.length) {
            return;
        }
        const stateMachineID = stateMachines[0].uri;

        this.getStateMachineMetadata(stateMachineID).subscribe(result => {
            this.getAllStates(result);
            this.buildActions(result);
            this.adaptOldStateMachine(result, stateMachines[0]);
        }, () => {
            this.notifyService.error('获取状态机元数据失败！');
        });
    }

    /**
     *  获取状态机元数据：区分运行时定制和设计时环境
     * @param stateMachineID 状态机id
     */
    private getStateMachineMetadata(stateMachineID: string): Observable<any> {
        return this.metadataService.getMetadata(stateMachineID);
    }

    /**
     * 根据状态机id获取状态机数组
     * @param result 状态机元数据
     */
    private getAllStates(result) {
        this.stateMachineMetaData = [];
        Object.keys(JSON.parse(result.content).renderState).forEach(item => {
            this.stateMachineMiddleData.id = item;
            this.stateMachineMiddleData.name = JSON.parse(result.content).renderState[item].name;
            this.stateMachineMiddleData.exist = '是';
            this.stateMachineMetaData.push(cloneDeep(this.stateMachineMiddleData));
            this.stateMachineMiddleData.id = '';
            this.stateMachineMiddleData.name = '';
        });
        return this.stateMachineMetaData;
    }
    private buildActions(result) {
        const object = JSON.parse(result.content);
        const actions = object && object.action || {};
        this._action = actions;
    }

    private adaptOldStateMachine(result: any, stateMachine: FormStateMachine) {
        if (this.formBasicService.envType !== DesignerEnvType.designer) {
            return;
        }
        // 设计器自动补充的属性，不记录变更
        window['suspendChangesOnForm'] = true;

        if (!stateMachine.nameSpace) {
            stateMachine.nameSpace = result.nameSpace;
        }
        if (!stateMachine.code) {
            stateMachine.code = result.code;
        }
        // if (!stateMachine.path) {
        //     stateMachine.nameSpace = result.code;
        // }

        window['suspendChangesOnForm'] = false;
    }
}
