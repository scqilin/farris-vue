import { NgModule } from '@angular/core';
import { GSP } from '@farris/ide-devkit';

@NgModule({
    declarations: [],
    imports: [
    ],
    exports: []
})
export class DesignerServicesModule { }


declare global {
    var gsp: GSP ;
}
