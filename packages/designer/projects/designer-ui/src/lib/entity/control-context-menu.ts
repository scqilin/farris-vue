import { ContextMenuItem } from '@farris/ui-context-menu';

export class ControlContextMenuItem implements ContextMenuItem {
    /** 菜单id */
    public id: string;
    /** 菜单标题 */
    public title: string;
    /** 菜单点击命令 */
    public handle?: (e?: any) => any;

    children?: ControlContextMenuItem[];

    /** 菜单是否可见 */
    visible?: (e: any) => boolean | boolean;

    /** 菜单是否禁用 */
    disable?: (e: any) => boolean | boolean;

    /**
     * 子菜单对应的控件分类，用于二级菜单是某一分类下的所有控件
     */
    public parentMenuId?: string;

    /** 菜单需要的额外参数 */
    [propName: string]: any;
}
