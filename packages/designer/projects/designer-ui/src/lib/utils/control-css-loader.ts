export class ControlCssLoaderUtils {
    /**
     * 加载控件样式文件
     * @param href 资源地址
     */
    static loadCss(href: string) {
        // 防止重复添加
        let cssLoaded = false;
        const links = document.body.getElementsByTagName('link');
        const assetsUrl = ControlCssLoaderUtils.getAssetsUrl();
        if (links && links.length) {
            const linkItem = document.location.origin + assetsUrl + '/style/' + href;
            for (const link of Array.from(links)) {
                if (link && link.href) {
                    const linkPath = link.href.slice(0, link.href.indexOf('?'));
                    if (linkItem === linkPath) {
                        cssLoaded = true;
                        break;
                    }

                }
            }
        }
        if (!cssLoaded) {
            const listFilterCssFile = document.createElement('link');
            listFilterCssFile.setAttribute('rel', 'stylesheet');
            listFilterCssFile.setAttribute('type', 'text/css');
            listFilterCssFile.setAttribute('href', `${assetsUrl}/style/${href}?v=${new Date().getTime()} `);

            document.body.appendChild(listFilterCssFile);
        }


    }

    static getAssetsUrl() {
        if (document.location.origin.includes('4200')) {
            return `/assets/farris-design-control`;
        } else {
            return `/platform/common/web/assets/farris-design-control`;
        }
    }
}



