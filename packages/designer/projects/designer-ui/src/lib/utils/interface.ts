
export interface actions {
    sourceComponent: {
        id: string,
        viewModelId: string,
        map: mapItem[],
    },
};

export interface mapItem {
    event: {
        label: string,
        name: string,
    },
    command: {
        id: string,
        label: string,
        name: string,
        handlerName: string,
        params?: any,
        showTargetComponent?: boolean;
        targetComponentId?: string;
        isNewGenerated?: boolean,
        isRTCmd?:true,
        isInvalid: Boolean;

    },
    controller: {
        id: string,
        label: string,
        name: string,
    },
    targetComponent: {
        id: string,
        viewModelId: string,
    },

}