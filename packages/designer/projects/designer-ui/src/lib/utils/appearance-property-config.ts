import { AdvancedStyleEditorComponent } from '@farris/designer-devkit';
import { ElementPropertyConfig } from '@farris/ide-property-panel';

/**
 * 内联样式属性面板配置
 */
export class ControlAppearancePropertyConfig {


    static getAppearanceStylePropConfigs(propertyData: any, showProperties?: string[]): ElementPropertyConfig[] {

        if (propertyData) {
            propertyData.appearance = propertyData.appearance ? propertyData.appearance : { class: '', style: '' };
        }
        return [
            {
                categoryId: 'appearance',
                categoryName: '外联样式',
                enableCascade: true,
                parentPropertyID: 'appearance',
                propertyData: propertyData.appearance,
                tabId: 'appearance',
                tabName: '样式',
                properties: [
                    {
                        propertyID: 'class',
                        propertyName: 'class样式',
                        propertyType: 'string',
                        description: '组件的CSS样式'
                    }
                ]
            },
            {
                categoryId: 'appearanceStyle',
                categoryName: '内联样式',
                tabId: 'appearance',
                tabName: '样式',
                properties: [
                    {
                        propertyID: 'inlineStyle',
                        propertyName: '',
                        propertyType: 'custom',
                        editor: AdvancedStyleEditorComponent,
                        editorParams: {
                            appearance: propertyData && propertyData.appearance
                        }
                    }
                ]
            }
        ];
    }
}
