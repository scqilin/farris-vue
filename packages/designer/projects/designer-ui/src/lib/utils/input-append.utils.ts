
/**
 * 启用扩展区域后的输入类控件模板
 */
export class InputAppendUtils {
    /**
     * 文本TextBox的扩展区域模板
     * @param ctx 控件schema
     * @param input 输入框部分的模板
     */
    static getTextBoxAppendElement(ctx: any, input: string) {
        const inputAppendType = ctx.component.inputAppendType;
        // 根据扩展类型区分样式
        const inputAppendCls = inputAppendType === 'button' ? 'input-append-button' : 'input-append-text';

        const inputWithAppendEle = `
            <div  class="f-cmp-text-input-append">
                ${input}
                <div  class="input-group-append input-append-wrapper ${inputAppendCls}">
                    <div class="input-group-text">${ctx.component.inputAppendText}</div>
                </div>
            </div>
        `;
        return inputWithAppendEle;
    }

    /**
     * 多行文本MultiTextBox的扩展区域模板
     * @param ctx 控件schema
     * @param input 输入框部分的模板
     */
    static getMultiTextBoxAppendElement(ctx: any, input: string) {
        const inputAppendType = ctx.component.inputAppendType;
        // 根据扩展类型区分样式
        const inputAppendCls = inputAppendType === 'button' ? 'input-append-button' : 'input-append-text';

        const inputWithAppendEle = `
            <div  class="f-cmp-textarea-input-append">
                ${input}
                <div  class="input-group-append input-append-wrapper ${inputAppendCls}">
                    <div class="input-group-text">${ctx.component.inputAppendText}</div>
                </div>
            </div>
        `;
        return inputWithAppendEle;
    }
    /**
     * 智能输入类（DateBox、InputGroup、Number、Select等）的扩展区域模板
     * @param ctx 控件schema
     * @param input 输入框部分的模板
     */
    static getInputGroupAppendElement(ctx: any) {
        const inputAppendType = ctx.component.inputAppendType;
        // 根据扩展类型区分样式
        const inputAppendCls = inputAppendType === 'button' ? 'input-append-button' : 'input-append-text';

        const inputWithAppendEle = `
            <div  class="input-group-append input-append-wrapper ${inputAppendCls}">
                <div class="input-group-text">${ctx.component.inputAppendText}</div>
            </div>
        `;
        return inputWithAppendEle;
    }
}
