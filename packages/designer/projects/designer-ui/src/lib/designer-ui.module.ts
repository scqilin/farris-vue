import { CommonModule } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FarrisDesignerDevkitModule } from '@farris/designer-devkit';
import { ColumnGroupEditorComponent, HeaderGroupEditorModule } from '@farris/header-group-editor';
import { PropertyPanelModule } from '@farris/ide-property-panel';
// import { CustomWidthEditorComponent, DefaultValueEditorComponent, NocodeDesignerPluginModule } from '@farris/nocode-designer-plugin';
import { ColumnWidthSettingComponent, ResponseLayoutEditorModule } from '@farris/response-layout-editor';
import { RichTextDesignerModule } from '@farris/rich-text-designer';
import { ComboListModule } from '@farris/ui-combo-list';
import { FarrisContextMenuModule } from '@farris/ui-context-menu';
import { DatagridModule } from '@farris/ui-datagrid';
import { DatagridEditorsModule, EditorProviders } from '@farris/ui-datagrid-editors';
import { FilterModule } from '@farris/ui-filter-editor';
import { FarrisFormsModule } from '@farris/ui-forms';
import { InputGroupModule } from '@farris/ui-input-group';
import { LoadingModule } from '@farris/ui-loading';
import { MultiSelectModule } from '@farris/ui-multi-select';
import { NotifyModule } from '@farris/ui-notify';
import { NumberSpinnerModule } from '@farris/ui-number-spinner';
import { ProgressStepModule } from '@farris/ui-progress-step';
import { FarrisTabsModule } from '@farris/ui-tabs';
import { TreeTableModule } from '@farris/ui-treetable';
// import { GSPMetadataServiceModule, MetadataSelectModule } from '@gsp-lcm/metadata-selector';
// import { GSPMetadataRTServiceModule, MetadataRTSelectModule } from '@gsp-lcm/metadatart-selector';
// import { ExpressionModule, ExpressionService } from '@gsp-svc/expression';
import { FieldStyleConfigComponent } from './components/collection/common/property/editor/field-style-config/field-style-config.component';
// import { EntityFilterConditionComponent } from './components/collection/common/property/editor/filter-condition/entity-filter-condition.component';
import { GridFieldEditorComponent } from './components/collection/common/property/editor/grid-field-editor/grid-field-editor.component';
import { GroupFieldEditorComponent } from './components/collection/common/property/editor/group-field-editor/group-field-editor.component';
import { PresetTemplateSelectorComponent } from './components/collection/list-view/property/editor/preset-template-selector/preset-template-selector.component';
import { TemplateBindingEditorComponent } from './components/collection/list-view/property/editor/template-binding-editor/template-binding-editor.component';
import { TemplateButtonEditorComponent } from './components/collection/list-view/property/editor/template-binding-editor/template-button-editor/template-button-editor.component';
import { VariableListItemComponent } from './components/collection/list-view/property/editor/template-binding-editor/variable-list-item/variable-list-item.component';
import { VariableListComponent } from './components/collection/list-view/property/editor/template-binding-editor/variable-list/variable-list.component';
import { SplitFormComponent } from './components/container/component/context-menu/editor/split-form-component/split-form-component.component';
import { CreateDynamicItemComponent } from './components/container/dynamic-area/component/editor/create-dynamic-item/create-dynamic-item.component';
import { CreateFieldSetComponent } from './components/container/form/context-menu/editor/create-field-set/create-field-set.component';
import { FormLayoutSettingComponent } from './components/container/form/property/editor/form-layout-setting/form-layout-setting.component';
import { PageToolbarEditorComponent } from './components/container/header/property/editor/page-toolbar-editor/page-toolbar-editor.component';
import { MultiViewEditorComponent } from './components/container/multi-view-container/property/editor/multi-view-editor/multi-view-editor.component';
import { ListFilterFieldsEditorComponent } from './components/filter/list-filter/property/editor/list-filter-fields-editor/list-filter-fields-editor.component';
import { InputgroupFormMappingEditorComponent } from './components/filter/queryScheme/property/editor/query-scheme-fields-editor/inputgroup-form-mapping-editor/inputgroup-form-mapping-editor';
import { QuerySchemeFieldsEditorComponent } from './components/filter/queryScheme/property/editor/query-scheme-fields-editor/query-scheme-fields-editor.component';
import { SelectHelpClearFieldsEditorComponent } from './components/filter/queryScheme/property/editor/query-scheme-fields-editor/select-help-clear-fields/select-help-clear-fields.component';
import { QuerySchemePresetFieldsEditorComponent } from './components/filter/queryScheme/property/editor/query-scheme-preset-fields-editor/query-scheme-preset-fields-editor.component';
// import { SelectHelpConditionComponent } from './components/input/lookup-edit/property/editors/select-help-condition/select-help-condition.component';
// import { SelectHelpDisplayFieldsComponent } from './components/input/lookup-edit/property/editors/select-help-display-fields/select-help-display-fields.component';
// import { SelectHelpMetadataComponent } from './components/input/lookup-edit/property/editors/select-help-metadata/select-help-metadata.component';
// import { SelectHelpTextFieldComponent } from './components/input/lookup-edit/property/editors/select-help-text-field/select-help-text-field.component';
import { ProgressStepsEditorComponent } from './components/wizard/property/editor/progress-steps-editor/progress-steps-editor.component';
// import { AppNavigationUdtComponent, AppNavigationUdtModule } from '@farris/app-navigation-udt';
import { AppNavigationBeComponent, AppNavigationBeModule } from '@farris/app-navigation-be';
import { FarrisColorpickerPlusComponent, FarrisColorpickerPlusModule } from '@farris/colorpicker-plus';
import { PaginationControlsComponent, PaginationModule } from '@farris/ui-pagination';
import { ResponseLayoutSplitterComponent } from './components/container/response-layout/property/editor/response-layout-splitter/response-layout-splitter.component';
import { CalendarFieldsEditorComponent } from './components/collection/appointment-calendar/property/editor/calendar-fields-editor/calendar-fields-editor.component';
import { PlaceDataSourceSelectorComponent } from './components/collection/appointment-calendar/property/editor/place-datasource-selector/place-datasource-selector.component';
// import { AppNavigationBeModule } from '@farris/app-navigation-be';
import { PlaceTemplateEditorComponent } from './components/collection/appointment-calendar/property/editor/place-template-editor/place-template-editor.component';
import { PlaceFieldSelectorComponent } from './components/collection/appointment-calendar/property/editor/place-field-selector/place-field-selector.component';
// import { NocodePlaceDataSourceSelectorComponent } from './components/collection/appointment-calendar/property/editor/nocode-place-datasource-selector/nocode-place-datasource-selector.component';
// import { SelectHelpLinkMappingComponent } from './components/input/lookup-edit/property/editors/select-help-link-mapping/select-help-link-mapping.component';
// import { SelectHelpLinkMappingItemComponent } from './components/input/lookup-edit/property/editors/select-help-link-mapping/mapping-item/mapping-item.component';
import { ComboLookupModule } from '@farris/ui-combo-lookup';

@NgModule({
  declarations: [
    // SelectHelpTextFieldComponent,
    // SelectHelpMetadataComponent,
    // SelectHelpConditionComponent,
    FormLayoutSettingComponent,
    CreateFieldSetComponent,
    SplitFormComponent,
    MultiViewEditorComponent,
    PageToolbarEditorComponent,
    GroupFieldEditorComponent,
    GridFieldEditorComponent,
    FieldStyleConfigComponent,
    ListFilterFieldsEditorComponent,
    SelectHelpClearFieldsEditorComponent,
    QuerySchemeFieldsEditorComponent,
    QuerySchemePresetFieldsEditorComponent,
    // SelectHelpDisplayFieldsComponent,
    InputgroupFormMappingEditorComponent,
    ProgressStepsEditorComponent,
    CreateDynamicItemComponent,
    PresetTemplateSelectorComponent,
    TemplateBindingEditorComponent,
    VariableListComponent,
    VariableListItemComponent,
    TemplateButtonEditorComponent,
    // EntityFilterConditionComponent,
    ResponseLayoutSplitterComponent,
    CalendarFieldsEditorComponent,
    PlaceDataSourceSelectorComponent,
    // NocodePlaceDataSourceSelectorComponent,
    PlaceTemplateEditorComponent,
    PlaceFieldSelectorComponent,
    // SelectHelpLinkMappingComponent,
    // SelectHelpLinkMappingItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NotifyModule.forRoot(),
    LoadingModule.forRoot({ message: '加载中，请稍候...' }),
    // ApprovalCommentsModule,
    // NocodeDesignerPluginModule,
    ResponseLayoutEditorModule,
    FarrisDesignerDevkitModule,
    FarrisFormsModule,
    // ExpressionModule,
    TreeTableModule,
    InputGroupModule,
    // MetadataRTSelectModule,
    // GSPMetadataRTServiceModule.forRoot(''),
    // GSPMetadataServiceModule.forRoot(''),
    // MetadataSelectModule,
    BrowserAnimationsModule,
    FilterModule,
    PropertyPanelModule,
    ComboListModule,
    HeaderGroupEditorModule,
    NumberSpinnerModule,
    MultiSelectModule,
    DatagridModule,
    DatagridEditorsModule,
    DatagridModule.forRoot([
      ...EditorProviders
    ]),
    ProgressStepModule,
    FarrisContextMenuModule,
   
    FarrisTabsModule,
    RichTextDesignerModule,
    // AppNavigationUdtModule,
    AppNavigationBeModule,
    PaginationModule,
    FarrisColorpickerPlusModule,
    ComboLookupModule
    // AppNavigationBeModule
  ],
  exports: [],
  entryComponents: [
    // ApprovalCommentEditorComponent,
    // CustomWidthEditorComponent,
    ColumnWidthSettingComponent,
    // DefaultValueEditorComponent,
    // SelectHelpTextFieldComponent,
    // SelectHelpMetadataComponent,
    // SelectHelpConditionComponent,
    FormLayoutSettingComponent,
    CreateFieldSetComponent,
    SplitFormComponent,
    MultiViewEditorComponent,
    PageToolbarEditorComponent,
    GroupFieldEditorComponent,
    GridFieldEditorComponent,
    ColumnGroupEditorComponent,
    FieldStyleConfigComponent,
    ListFilterFieldsEditorComponent,
    QuerySchemeFieldsEditorComponent,
    QuerySchemePresetFieldsEditorComponent,
    SelectHelpClearFieldsEditorComponent,
    // SelectHelpDisplayFieldsComponent,
    InputgroupFormMappingEditorComponent,
    ProgressStepsEditorComponent,
    CreateDynamicItemComponent,
    // JointSearchComponent,
    PresetTemplateSelectorComponent,
    TemplateBindingEditorComponent,
    // EntityFilterConditionComponent,
    // AppNavigationUdtComponent,
    AppNavigationBeComponent,
    PaginationControlsComponent,
    FarrisColorpickerPlusComponent,
    ResponseLayoutSplitterComponent,
    CalendarFieldsEditorComponent,
    PlaceDataSourceSelectorComponent,
    PlaceTemplateEditorComponent,
    PlaceFieldSelectorComponent,
    // NocodePlaceDataSourceSelectorComponent,
    // SelectHelpLinkMappingComponent,
    // SelectHelpLinkMappingItemComponent
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'zh-CHS' },
    // { provide: Server_Host, useValue: '' },
    // { provide: Load_Data_Uri, useValue: '' },
    // ExpressionService
  ]
})
export class FarrisDesignerUIModule {
}
