import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { CommandUsualProp } from '../../common/property/command-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class TagProp extends CommandUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {


        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonPops = this.getCommonAppearanceProperties();
        commonPops.push(
            {
                propertyID: 'label',
                propertyName: '标签文本',
                propertyType: 'string',
                description: '标签文本设置'
            },
            {
                propertyID: 'value',
                propertyName: '标签值',
                propertyType: 'string',
                description: '标签值设置'
            },
            {
                propertyID: 'colorType',
                propertyName: '标签类型',
                propertyType: 'select',
                description: '标签类型选择',
                iterator: [{ key: '', value: '无' }, { key: 'success', value: 'success' }, { key: 'primary', value: 'primary' },
                { key: 'danger', value: 'danger' }, { key: 'warning', value: 'warning' }, { key: 'info', value: 'info' },
                { key: 'secondary', value: 'secondary' }]
            }
        );

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonPops
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'closable',
                    propertyName: '可关闭',
                    propertyType: 'boolean',
                    description: '是否可关闭'
                }
            ]
        };
    }



    private getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            }
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            hideTitle: true,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService,domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);

            }
        };

    }
}
