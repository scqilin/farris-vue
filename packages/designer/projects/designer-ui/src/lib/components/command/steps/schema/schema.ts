export const StepsSchema = {
    id: '',
    type: 'Steps',
    stepsCls: '',
    stepStyle: '',
    extendTemplate: '',
    visible: true,
    fill: false,
    currentId: null,
    stepsMessage: null,
    appearance: {
        class: ''
    }
};
