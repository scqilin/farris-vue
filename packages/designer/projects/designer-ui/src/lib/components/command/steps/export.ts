import FdStepsComponent from './component/fd-steps';
import { StepsSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdStepsTemplates from './templates';


export const Steps: ComponentExportEntity = {
    type: 'Steps',
    component: FdStepsComponent,
    template: FdStepsTemplates,
    metadata: StepsSchema
};
