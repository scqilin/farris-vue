export default (ctx: any) => {
  let navItemStr = '';
  for (let k = 0; k < ctx.navData.length; k++) {
    let navItem = ctx.navData[k];
    let itemCls = navItem['id'] == ctx.activeNavId ? ' active' : '';
    itemCls += navItem.hasOwnProperty('disable') && navItem['disable'] ? navItem['disable'] : '';
    let numStr = '';
    if (navItem.hasOwnProperty('num')) {
      numStr = `<div class="farris-nav-item-tag">`;
      numStr += !ctx.component.maxNum || navItem['num'] <= ctx.component.maxNum ? `<span class="tag-text">${navItem.num}</span>` : `<span class="tag-text">${ctx.component.maxNum}+</span>`;
      numStr += `</div>`;
    }
    navItemStr += `<div class="farris-nav-item ${itemCls}" id="${navItem['id']}">
        <div class="farris-nav-item-link">
            <span class="farris-nav-item-link-text">${navItem.text}${numStr}</span>
        </div>
    </div>`;
  }
  return `<div class="farris-nav ide-cmp-nav-tab ${ctx.component.horizontal ? '' : 'farris-nav-vertical'}" ref="${ctx.NavTabKey}">
        ${navItemStr}
  </div>`;
};
