
export default (ctx: any) => {
  return `<div class="f-view-change" ref="${ctx.viewChangeKey}" viewChange="${ctx.component.viewGroupIdentify}">
    ${ctx.getViewChangeStr()}
  </div>`;
};

