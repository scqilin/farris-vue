
const ToolBarItemSchema: ToolBarItemType = {
    id: 'toolBarItem',
    type: 'ToolBarItem',
    appearance: null,
    disable: false,
    text: '按钮',
    items: [],
    visible: true,
    click: '',
    usageMode: 'button',
    modalConfig: null
};

interface ToolBarItemType {
    id: string;
    type: string;
    appearance: object | null;
    disable: boolean;
    text: string;
    items: Array<ToolBarItemType> | null;
    visible: boolean;
    click: string | null;
    usageMode: string;
    modalConfig: object | null;
}

export { ToolBarItemSchema, ToolBarItemType };
