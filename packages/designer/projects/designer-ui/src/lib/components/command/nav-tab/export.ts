import FdNavTabComponent from './component/fd-nav-tab';
import { NavTabSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdNavTabTemplates from './templates';


export const NavTab: ComponentExportEntity = {
    type: 'NavTab',
    component: FdNavTabComponent,
    template: FdNavTabTemplates,
    metadata: NavTabSchema
};
