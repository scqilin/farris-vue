export default (ctx: any) => {
    // 列表样式
    let listCls = '';
    if (ctx.direction == 'horizontal') {
        listCls = 'f-progress-step-list';
        if (ctx.component.fill == true) {
            listCls += ' f-progress-step-horizontal-fill';
        }
    } else {
        listCls = 'f-progress-step-list-block';
        if (ctx.component.fill == true) {
            listCls += ' f-progress-step-vertical-fill';
        }
    }
    if (ctx.component.extendTemplate) {
        listCls += ' f-progress-step-has-extend';
    }
    listCls += ' ' + ctx.component.stepsCls;
    // 列表结构
    let listStr = '';
    // li的style
    let liStyle = ctx.component.stepStyle ? 'style="' + ctx.component.stepStyle + '" ' : '';
    // 
    for(let k=0;k<ctx.stepsData.length;k++){
        let curStep=ctx.stepsData[k];
        let liCls= k<ctx.activeIndex?' finish':'';
        liCls+=curStep.hasOwnProperty('class')&&curStep['class']?' '+curStep['class']:'';
        liCls+=k==ctx.activeIndex?' active':'';
        liCls+=ctx.component.extendTemplate?' step-has-extend':'';  
        let stepRowCls= k<ctx.activeIndex?' step-finish': '';
        stepRowCls+=  k==ctx.activeIndex?' step-active':'';
        let iconStr='';
        if(curStep.hasOwnProperty('icon')&&curStep['icon']){
            iconStr=`<span class="step-icon ${curStep['icon']}"></span>`;
        }else{
            if(k >=ctx.activeIndex){
                iconStr= `<span class="step-icon">${k+1}</span>`;
            }else{
                iconStr=`<span class="step-icon step-success k-icon k-i-check"></span>`;
            }
        }
        let lineStr = '';
        if (k < ctx.stepsData.length - 1) {
            lineStr = `<div class="f-progress-step-line ${k == ctx.activeIndex ? 'f-progress-step-line-success' : ''}">
                <span class="triangle ${ctx.direction ? 'f-progress-step-vertical-triangle' : ''}"></span>
            </div>`;
        }
        listStr += `<li ${liStyle} class="step ${liCls}">
            <div class="f-progressstep-row ${stepRowCls}">
                <div class="f-progress-step-content">
                   ${iconStr}
                    <div class="f-progress-step-title">  
                       <p class="step-name ${k <= ctx.activeIndex ? 'step-name-success' : ''}" >${curStep.title}</p>
                    </div>   
                </div>${lineStr} 
            </div>             
            ${ctx.component.extendTemplate}
        </li>`;
    }

    // 空数据
    let emptyStr = '';
    if (ctx.stepsData.length == 0) {
        emptyStr = '<div class="f-progress-step-empty">暂无数据</div>';
    }
    return `<div class="f-progress-step">
        <ul class="${listCls}">
            ${listStr}
        </ul>
        ${emptyStr}
    </div>`;
}
