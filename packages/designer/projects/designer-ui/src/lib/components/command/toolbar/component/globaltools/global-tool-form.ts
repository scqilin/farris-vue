export const renderForm = (ctx) => {
    // document.body
    const modal = document.createElement('div');
    document.body.appendChild(modal);
    // 绝对定位  模态框样式
    modal.id = 'global-tool-modal';
    modal.classList.add('position-absolute', 'w-100', 'h-100', 'd-none');
    modal.style.backgroundColor = 'rgb(108, 108, 108, 0.5)';
    modal.style.zIndex = '1050';
    modal.style.top = '0px';
    // 组装按钮列表
    const buttonList = renderButtonList(ctx.items, ctx.disabledList);
    // 列表容器
    const buttonListContainer = `
    <div class="position-absolute" style="
    position: absolute;
    width:500px;
    height:300px;
    left: 50%;
    top: 50%;
    margin: -150px 0 0 -250px;
    background: #fff;
    border-radius: 10px;
    transform:translate3d(0px, 0px, 0px);
    user-select:none
    ">
        <div class="global-modal__header d-flex" style="padding:10px 20px;justify-content:space-between">
            <div ref="global-modal__title">${ctx.title || '弹出框'}</div>
            <div ref="global-modal__close" style="cursor:pointer">
                <span class="f-icon modal_close"></span>
            </div>
        </div>
        <div ref="global-modal__list" class="d-flex" style="flex-wrap:wrap;padding:10px">${buttonList.join('')}</div>
        <div style="
        position: absolute;
        bottom: 0;
        width: 100%;
        padding: 10px 20px;
        text-align: right;">
            <button class="btn btn-primary btn-lg" ref="global-modal__confirm">确定</button>
            <button class="btn btn-secondary btn-lg" ref="global-modal__cancel">取消</button>
        </div>
    </div>
    `;

    modal.innerHTML = buttonListContainer;
    return modal;
};

export const renderButtonList = (items, disabledList) => {
    return items.map(item => `<button id=${item.id} ref=${item.id} class="btn btn-lg btn-secondary
    ${disabledList.includes(item.id) ? 'disabled' : ''}"
     style="margin:5px 10px;box-sizing:border-box;${disabledList.includes(item.id) ? 'cursor:no-drop' : ''}">${item.text}</button>`);
};

