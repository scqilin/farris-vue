import ToolBarItemComponent from '../component/fd-toolbar-item';

const childHierarchy = `
  <div role="button"  class="btn component-settings-button"  title="新增子级" ref="appendChildButton" style="width:85px!important;padding:0 5px;position:static;">
      <i class="f-icon f-icon-plus-circle text-white mr-1" ></i>
      <span style="font-size:13px;margin:auto">新增子级</span>
  </div>
`;
const sameHierarchy = `
  <div role="button" class="btn component-settings-button" title="新增同级" ref="appendSame" style="width:85px!important;padding:0 5px;position:static;">
      <i class="f-icon f-icon-plus-circle text-white mr-1" ></i>
      <span style="font-size:13px;margin:auto">新增同级</span>
  </div>
`;
// 二级按钮支持上移下移，不支持拖拽
const moveUpAndDown = (ctx) => {
  if (!ctx.isChildOfButton || !ctx.instance.parent) {
    return '';
  }
  const siblingItems = ctx.instance.parent.items;
  const currentIndex = siblingItems.findIndex(item => item.id === ctx.component.id);
  const isFirstBtn = currentIndex === 0;
  const isLastBtn = currentIndex === siblingItems.length - 1;
  let moveTpl = '';

  if (!isLastBtn) {
    moveTpl += `
    <div role="button" class="btn component-settings-button" title="下移" ref="moveDown" style="position:static;">
        <i class="f-icon f-icon-arrow-down text-white"></i>
    </div>`;
  }

  if (!isFirstBtn) {
    moveTpl += `
    <div role="button" class="btn component-settings-button" title="上移" ref="moveUp" style="position:static;">
        <i class="f-icon f-icon-arrow-up text-white"></i>
    </div>`;
  }

  return moveTpl;
};

const buttonList = (ctx) => {
  const list = ctx.toolBarItemList.map((i: ToolBarItemComponent) => `
      ${i.render()}
  `);

  if (list && list.length) {
    const cascadeStyle = ctx.popDirection === 'top' ? 'top: auto;bottom: 100%;margin-top: 0;margin-bottom: 0.25rem;' : 'margin-top:3px;';
    return `
    <button
      class="btn dropdown-toggle dropdown-toggle-split ${ctx.component.appearance && ctx.component.appearance.class ? ctx.component.appearance.class : 'btn-secondary'}"
      ref="dropdown-button"
      style="margin-left:-10px;border-radius:0 3px 3px 0;border-left:1px solid #fff;"
    >
    </button>
    <div class="position-absolute d-none cascadeToolbar" style="right:0;z-index:800; ${cascadeStyle}">
      ${list.join('')}
    </div>
    `;
  }
  return '';
};

export default (ctx: any) => {
  const item = ctx.component;
  const icon = item.icon && item.icon.iconName ? `<span class='f-icon ${item.icon.iconName} ${item.icon.iconClass ? item.icon.iconClass : ''}'></span>` : '';

  let btnCls = 'farris-component position-relative btn f-rt-btn f-btn-ml ';
  btnCls += item.appearance && item.appearance.class ? item.appearance.class : ' btn-secondary ';
  btnCls += item.disable === true ? ' disabled' : '';
  btnCls += item.icon && item.icon.iconName ? ' btn-icontext' : '';
  const popDirectionCls = ctx.popDirection === 'top' ? ' dropup' : '';

  return `
  <div ref="${item.id}" class="d-inline-flex farris-component position-relative ${popDirectionCls}"
      style="${ctx.isChildOfButton ? 'display:block!important;margin:0;text-align:center;' : 'display:inline-block!important;'}">
    <div class="component-btn-group">
      ${ctx.showMenuInToolbarItem ? `
        <div>
          <div role="button" class="btn component-settings-button" title="删除" ref="removeButton" style="position:static;">
              <i class="f-icon f-icon-yxs_delete"></i>
          </div>
            <div role="button" class="btn component-settings-button" title="复制" ref="copyButton" style="position:static;" >
              <i class="f-icon f-icon-yxs_copy"></i>
          </div>
          ${ctx.isChildOfToolBar ? childHierarchy : ''}
          ${ctx.isChildOfButton ? `${moveUpAndDown(ctx)}${sameHierarchy}` : ''}
        </div>
      `:
      ''}
      
    </div>

    <button id = "${item.id}" class="${btnCls}" style= "${ctx.toolBarItemList && ctx.toolBarItemList.length ? 'border-radius:3px 0 0 3px;' : ''}
        ${ctx.isChildOfButton ? 'width:100%;margin:0!important' : ''} ">
        ${icon}
        ${item.text || item.title}
    </button>
    ${buttonList(ctx)}
  </div>
  `;
};
