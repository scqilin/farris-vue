import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { cloneDeep } from 'lodash-es';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DgControl } from '../../../../utils/dg-control';
import { ButtonProp } from '../property/property-config';
import { ButtonSchema } from '../schema/schema';

export default class ButtonComponent extends FarrisDesignBaseComponent {

    component;
    constructor(component: any, options: any) {
        super(component, options);
        this.applyClassToComponent = false;
        this.customToolbarConfigs = [
            {
                id: 'cloneItem',
                title: '复制按钮',
                icon: 'f-icon f-icon-yxs_copy text-white',
                click: (e) => {
                    e.stopPropagation();
                    this.cloneItem();
                }
            }
        ];
    }

    getDefaultSchema(): any {

        return ButtonSchema;
    }

    getStyles(): string {

        return 'display: inline-block;';
    }

    render(): any {

        return super.render(this.renderTemplate('Button', {
            component: this.component
        }));
    }


    attach(element: HTMLElement): Promise<any> {
        this.loadRefs(element, {
            button: 'single'
        });

        const superAttach = super.attach(element);
        return superAttach;
    }

    cloneItem() {
        const parent = this.parent;

        const random = Math.random().toString(36).slice(2, 6);

        // 点击事件不复制
        const clonedBtn = Object.assign(cloneDeep(this.component),
            {
                id: `${this.type}-cloned-${random}`,
                click: null
            });

        const index = parent.component.contents.findIndex(child => child.id === this.component.id);
        if (index > -1) {
            parent.component.contents.splice(index + 1, 0, clonedBtn);
            parent.rebuild().then(() => {
                // 触发父容器的重构，并刷新控件树
                this.emit('componentChanged', null);
            });
        }
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: ButtonProp = new ButtonProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        const dynamicPropertyIDs = ['appearance.class', 'appearance.style', 'size.width', 'size.height', 'text'];

        const propertyPath = changeObject.propertyPath ? changeObject.propertyPath + '.' : '';
        if (dynamicPropertyIDs.includes(propertyPath + changeObject.propertyID)) {
            this.triggerRedraw();
        }
    }

    /**
     * 控件可以向外拖拽的顶层容器
     */
    getDragScopeElement() {
        // 父级节点
        const parent = this.parent && this.parent.component;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 填报模板底部按钮不能拖拽到父容器外部，只能在内部移动位置
        if (parent.type === DgControl.ContentContainer.type && parentClassList.includes('f-btn-wrapper')) {
            return this.parent.element;
        }
        // OA的批量填报模板底部按钮不能拖拽到父容器外部，只能在内部移动位置
        if (parent.type === DgControl.ContentContainer.type && parentClassList.includes('f-subgrid-by-table-footer')) {
            return this.parent.element;
        }

    }
}



