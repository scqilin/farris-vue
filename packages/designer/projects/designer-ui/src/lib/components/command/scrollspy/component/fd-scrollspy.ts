import { ScrollspySchema } from '../schema/schema';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ScrollspyProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import { OperateUtils } from '../../../../utils/operate.utils';
import FdCommandBaseComponent from '../../common/commandBase/commandBase';

export default class FdScrollspyComponent extends FdCommandBaseComponent {
    // 跟随类型
    private followType = 'side';
    // 当前监听Id
    private currentSpyId = '';
    // 类型为side时，可以设置宽度
    private sidetabWidth = 145;
    // 列表标记
    get scrollSpyListKey(): string {
        return `scrollSpyList-${this.key}`;
    }

    constructor(component: any, options: any) {
        super(component, options);

        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('scrollspy.css');
    }

    getStyles(): string {
        return 'display: block;';
    }
    getDefaultSchema(): any {

        return ScrollspySchema;
    }
    // 不支持移动
    checkCanMoveComponent(): boolean {
        return false;
    }
    // 不支持删除（若要支持删除，是否需要修改配套的容器？）
    checkCanDeleteComponent(): boolean {
        return false;
    }
    // 渲染模板
    render(): any {
        this.initComponentParam();

        return super.render(this.renderTemplate('Scrollspy', {
            component: this.component,
            followType: this.followType,
            currentSpyId: this.currentSpyId,
            sidetabWidth: this.sidetabWidth,
            scrollSpyListKey: this.scrollSpyListKey
        }));
    }
    // 绑定事件
    attach(element: HTMLElement): Promise<any> {
        const parent = element.parentElement;
        if (!parent.style.position && parent.style.position !== 'relative' && this.followType === 'side') {
            parent.style.position = 'relative';
        }
        this.loadRefs(element, {
            [this.scrollSpyListKey]: 'single'
        });

        const superAttach = super.attach(element);
        const spyList = this.refs[this.scrollSpyListKey];
        const spyBtns = spyList.querySelectorAll('.f-scrollspy-monitor-btn');
        spyBtns.forEach(spyBtn => {
            this.addEventListener(spyBtn, 'click', (event) => {
                event.stopPropagation();
                if (this.currentSpyId !== spyBtn.id) {
                    const oldCurrentBtn = spyList.querySelector('#scrollspy-' + this.currentSpyId);
                    oldCurrentBtn && OperateUtils.removeClass(oldCurrentBtn, 'active');
                    OperateUtils.addClass(spyBtn, 'active');
                    this.currentSpyId = spyBtn.id.replace('scrollspy-', '');
                }
                // 此处不刷新界面
                // this.redraw();
            });
        });

        return superAttach;
    }
    /**
     * 获取当前生效的属性配置
     */
    private initComponentParam() {
        this.currentSpyId = this.component.currentSectionId;

        // 跟随样式属性若配置为动态值，设计器里统一展示到侧边
        if (this.component.followType) {
            this.followType = ['side', 'tab', 'fixedTab'].includes(this.component.followType) ? this.component.followType : 'side';
        }
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: ScrollspyProp = new ScrollspyProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        super.onPropertyChanged(changeObject, ['followType', 'currentSectionId', 'items']);
    }
}
