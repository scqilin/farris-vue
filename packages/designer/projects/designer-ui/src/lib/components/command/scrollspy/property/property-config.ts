import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { ItemCollectionConverter, ItemCollectionEditorComponent } from '@farris/designer-devkit';
import { CommandUsualProp } from '../../common/property/command-property-config';
import { EditorTypes } from '@farris/ui-datagrid-editors';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class ScrollspyProp extends CommandUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {


        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const bahaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(bahaviorPropConfig);

        return propertyConfig;
    }
    private getAppearancePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const props = this.getCommonAppearanceProperties();
        props.push(
            {
                propertyID: 'size',
                propertyName: '尺寸',
                propertyType: 'cascade',
                cascadeConfig: [
                    {
                        propertyID: 'width',
                        propertyName: '宽度（px）',
                        propertyType: 'number',
                        description: '组件的宽度',
                        min: 0,
                        decimals: 0
                    }
                ]
            },
            {
                propertyID: 'followType',
                propertyName: '跟随样式',
                propertyType: 'unity',
                description: '运行时组件显示位置',
                editorParams: {
                    controlName: UniformEditorDataUtil.getControlName(propertyData),
                    constType: 'enum',
                    editorOptions: {
                        types: ['const', 'variable', 'custom'],
                        enums: [{ key: 'side', value: '侧面' }, { key: 'tab', value: '顶部' }],
                        variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        newVariableType: 'String'
                    }
                }
            }
        );

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: props
        };
    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        const self = this;
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'itemsType',
                    propertyName: '滚动跟随项数组类型',
                    propertyType: 'select',
                    iterator: [{ key: 'static', value: '常量' }, { key: 'dynamic', value: '变量' }]
                },
                {
                    propertyID: 'items',
                    propertyName: '滚动跟随项数组',
                    propertyType: 'modal',
                    description: '滚动跟随项数据',
                    editor: ItemCollectionEditorComponent,
                    editorParams: {
                        columns: [
                            { field: 'id', title: '值', editor: { type: EditorTypes.TEXTBOX } },
                            { field: 'title', title: '名称', editor: { type: EditorTypes.TEXTBOX } }],
                        requiredFields: ['title', 'id'],
                        uniqueFields: ['title', 'id'],
                        modalTitle: '滚动跟随项编辑器',
                        canEmpty: false
                    },
                    converter: new ItemCollectionConverter(),
                    visible: propertyData.itemsType === 'static'
                },
                {
                    propertyID: 'dynamicItems',
                    propertyName: '滚动跟随项数组',
                    propertyType: 'unity',
                    description: '滚动跟随项数据',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    },
                    visible: propertyData.itemsType === 'dynamic'
                },
                {
                    propertyID: 'currentSectionId',
                    propertyName: '活动节点',
                    propertyType: 'unity',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable', 'custom'],
                            enums: this.getCurrentSectionIterators(propertyData),
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    }
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                switch (changeObject.propertyID) {
                    case 'itemsType': {
                        const items = this.properties.find(p => p.propertyID === 'items');
                        if (items) {
                            items.visible = changeObject.propertyValue === 'static';
                        }

                        const dynamicItems = this.properties.find(p => p.propertyID === 'dynamicItems');
                        if (dynamicItems) {
                            dynamicItems.visible = changeObject.propertyValue === 'dynamic';
                        }

                        propertyData.currentSectionId = null;
                        break;
                    }
                    case 'items': {
                        if (propertyData.currentSectionId && (typeof propertyData.currentSectionId === 'string')) {
                            const currentConfig = self.getCurrentSectionIterators(propertyData);
                            // 更新当前活动节点枚举选项
                            const currentSectionProp = this.properties.find(p => p.propertyID === 'currentSectionId');
                            if (currentSectionProp) {
                                currentSectionProp.editorParams.editorOptions.enums = currentConfig;
                            }


                            // 更新当前活动节点值
                            const sectionKeys = currentConfig.map(c => c.key);
                            if (!sectionKeys.includes(propertyData.currentSectionId)) {
                                propertyData.currentSectionId = null;
                            }
                        }

                    }
                }
            }
        };
    }

    private getCurrentSectionIterators(propertyData: any) {
        if (!propertyData.items || !propertyData.items.length) {
            return [];
        }
        const keyValues = [];
        propertyData.items.forEach(item => {
            keyValues.push({
                key: item.id,
                value: item.title
            });
        });
        return keyValues;
    }
}
