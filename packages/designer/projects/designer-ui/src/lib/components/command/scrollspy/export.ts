import FdScrollspyComponent from './component/fd-scrollspy';
import { ScrollspySchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdScrollspyTemplates from './templates';


export const Scrollspy: ComponentExportEntity = {
    type: 'Scrollspy',
    component: FdScrollspyComponent,
    template: FdScrollspyTemplates,
    metadata: ScrollspySchema
};
