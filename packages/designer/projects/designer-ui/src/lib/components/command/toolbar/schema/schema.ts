import { ToolBarItemType } from '../component/toolbaritem/schema/schema';

const ToolBarSchema = {
    id: 'toolBar',
    type: 'ToolBar',
    appearance: {
        class: 'f-toolbar'
    },
    size: null,
    items: [],
    visible: true,
    buttonSize: 'default',
    popDirection: 'default'
};

interface ToolBarType {
    id: string;
    type: string;
    appearance: Appearance;
    size: object | null;
    items?: ToolBarItemType[];
    contents?: any[];
    visible: boolean;
    buttonSize: string;
    popDirection: string;
}

interface Appearance {
    class?: string;
}

export { ToolBarSchema, ToolBarType };

