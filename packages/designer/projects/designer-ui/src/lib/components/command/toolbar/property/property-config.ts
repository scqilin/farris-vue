import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { CommandUsualProp } from '../../common/property/command-property-config';

export class ToolBarProp extends CommandUsualProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        return this.propertyConfig;
    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const baseAppearanceInfo = this.getCommonAppearanceProperties() as PropertyEntity[];
        const buttonInfo = [
            {
                propertyID: 'buttonSize',
                propertyName: '按钮标准',
                propertyType: 'select',
                iterator: [
                    { key: 'default', value: '标准' },
                    { key: 'lg', value: '大号' },
                ]
            },
            {
                propertyID: 'popDirection',
                propertyName: '弹出方向',
                propertyType: 'select',
                iterator: [
                    { key: 'default', value: '默认' },
                    { key: 'top', value: '向上' },
                    { key: 'bottom', value: '向下' },
                ]
            },
        ];
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: [...baseAppearanceInfo, ...buttonInfo],
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ]
        };
    }
}
