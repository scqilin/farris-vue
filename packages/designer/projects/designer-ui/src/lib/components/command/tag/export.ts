import FdTagComponent from '././component/fd-tag';
import { TagSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdTagTemplates from './templates';


export const Tag: ComponentExportEntity = {
    type: 'Tag',
    component: FdTagComponent,
    template: FdTagTemplates,
    metadata: TagSchema
};
