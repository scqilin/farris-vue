import { NavTabSchema } from '../schema/schema';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { NavTabProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import { OperateUtils } from '../../../../utils/operate.utils';
import FdCommandBaseComponent from '../../common/commandBase/commandBase';
import { cloneDeep } from 'lodash-es';

export default class FdNavTabComponent extends FdCommandBaseComponent {
    // 静态数据或者动态数据下的默认值
    private navData = [];
    // 绑定固定值，非变量
    private activeNavId = '';
    // 列表标记
    get NavTabKey(): string {
        return `NavTab-${this.key}`;
    }
    /**
     * 从工具箱拖拽生成的控件，默认三个标签配置
     */
    // static getMetadataInControlBox() {
    //     const navTabSchema = cloneDeep(NavTabSchema);
    //     navTabSchema.navData = [{ id: 'tab1', text: '标签一' }, { id: 'tab2', text: '标签二' }, { id: 'tab3', text: '标签三' }];

    //     return navTabSchema;
    // }

    constructor(component: any, options: any) {
        super(component, options);

        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('nav-tab.css');
    }

    getDefaultSchema(): any {
        return NavTabSchema;
    }

    private initNavTab() {
        if (this.component.dataSourceType === 'dynamic') {
            this.navData = [{ id: 'tab1', text: '标签一', num: 10 }, { id: 'tab2', text: '标签二', num: 20 }, { id: 'tab3', text: '标签三', num: 30 }];
            this.activeNavId = 'tab1';
        } else {
            this.navData = this.component.navData || [];
        }
        // 排除绑定变量
        if (this.component.activeNavId && typeof this.component.activeNavId == 'string') {
            this.activeNavId = this.component.activeNavId;
        } else {
            this.activeNavId = this.navData.length > 0 ? this.navData[0]['id'] : '';
        }
    }

    // 渲染模板
    render(): any {
        this.initNavTab();

        return super.render(this.renderTemplate('NavTab', {
            component: this.component,
            activeNavId: this.activeNavId,
            navData: this.navData,
            NavTabKey: this.NavTabKey
        }));
    }
    // 绑定事件
    attach(element: HTMLElement): Promise<any> {
        this.loadRefs(element, {
            [this.NavTabKey]: 'single'
        });
        const superAttach = super.attach(element);
        const navList = this.refs[this.NavTabKey];
        const navItems = navList.querySelectorAll('.farris-nav-item');
        navItems.forEach(navItem => {
            this.addEventListener(navItem, 'click', (event) => {
                event.stopPropagation();
                if (OperateUtils.hasClass(navItem, 'disable')) {
                    return;
                } else if (this.activeNavId != navItem.id) {
                    let oldActiveEl = navList.querySelector('#' + this.activeNavId);
                    oldActiveEl && OperateUtils.removeClass(oldActiveEl, 'active');
                    OperateUtils.addClass(navItem, 'active');
                    this.activeNavId = navItem.id;
                }
            });
        });

        return superAttach;
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: NavTabProp = new NavTabProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后事件
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        const propIds = ['horizontal', 'navData', 'dataSourceType', 'maxNum'];

        super.onPropertyChanged(changeObject, propIds);
    }
}
