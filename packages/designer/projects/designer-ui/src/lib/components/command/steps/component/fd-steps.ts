import { StepsSchema } from '../schema/schema';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { StepsProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import FdCommandBaseComponent from '../../common/commandBase/commandBase';

export default class FdStepsComponent extends FdCommandBaseComponent {
    // 步骤条数据,步骤条一般是绑定变量，此处用默认的数据
    // 后期数据来源可能会变
    private stepsData = [
        { id: 'plan', title: '发货计划' },
        { id: 'out', title: '出库' },
        { id: 'settlement', title: '结算' },
        { id: 'voucher', title: '凭证' }
    ];
    // 当前数据索引
    private activeIndex = 1;
    // 没有方向属性，假定
    private direction = 'horizontal';
    // fHeight 应用纵向时高度变量，未支持
    component;
    constructor(component: any, options: any) {
        super(component, options);

        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('progress-steps.css');
    }

    getStyles(): string {
        return 'display: block;';
    }
    getDefaultSchema(): any {

        return StepsSchema;
    }

    // 渲染模板
    render(): any {
        return super.render(this.renderTemplate('Steps', {
            component: this.component,
            stepsData: this.stepsData,
            activeIndex: this.activeIndex,
            direction: this.direction
        }));
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: StepsProp = new StepsProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }
    /**
     * 属性变更后事件
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        const notAppliedProps = ['visible', 'currentId', 'stepsMessage'];
        if (!notAppliedProps.includes(changeObject.propertyID)) {
            this.triggerRedraw();
        }

    }
}
