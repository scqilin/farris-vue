export default (ctx: any) => {
    // 不考虑模板的问题
    let scrollListStr = '';
    let scrollItemsStr = '';
    ctx.component.items.forEach(item => {
        let btnCls = item.id == ctx.currentSpyId ? 'f-scrollspy-monitor-btn active' : 'f-scrollspy-monitor-btn';
        scrollItemsStr += ` <li class="${btnCls}" id="scrollspy-${item.id}" title="${item.title}">
        <span class="f-scrollspy-monitor-title">${item.title}</span>
        </li>`;
    });
    scrollListStr = `<ul ref="${ctx.scrollSpyListKey}" class="f-scrollspy-monitor-list">${scrollItemsStr}</ul>`;

    if (ctx.followType == 'side') {
        let innerStyle = 'width:' + ctx.sidetabWidth / 16 + 'rem;transform:translateX(' + ctx.sidetabWidth / 16 + 'rem)';
        return `<div class="f-scrollspy f-scrollspy-sidetabs id-cmp-scrollspy">
        <div class="f-scrollspy-sidetabs-inner" style="${innerStyle}">
            ${scrollListStr}
        </div>
        <div class="f-scrollspy-monitor-tip">
        <div class="f-scrollspy-monitor-tip-inner">
            <span class="f-icon f-icon-navigation"></span>
            <span class="f-scrollspy-monitor-tip-text">导航</span>
            <span class="f-scrollspy-monitor-tip-img">
            </span>
        </div>
    </div>
        </div>`;
    }
    let scrollspyCls = ctx.followType == 'tab' ? ' f-scrollspy-tabs' : ' f-scrollspy-fixedTab';
    return `<div class="f-scrollspy ${scrollspyCls}">
    ${scrollListStr}
    </div>`;
}
