import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { DgControl } from '../../../../utils/dg-control';
import { PropertyEntity } from '@farris/ide-property-panel';
import { StyleEditorComponent } from '@farris/designer-devkit';
import { EventEditorService, UniformEditorDataUtil, DomService, FormBasicService, WebCmdService, StateMachineService } from '@farris/designer-services';
import { IDesignerHost } from '@farris/designer-element';


export class CommandUsualProp {

    public formBasicService: FormBasicService;
    public eventEditorService: EventEditorService;
    public domService: DomService;
    public webCmdService: WebCmdService;
    public viewModelId: string;
    public componentId: string;
    public stateMachineService: StateMachineService;

    constructor(serviceHost: IDesignerHost, viewModelId: string, componentId: string) {
        this.viewModelId = viewModelId;
        this.componentId = componentId;
        this.webCmdService = serviceHost.getService('WebCmdService');
        this.domService = serviceHost.getService('DomService');
        this.formBasicService = serviceHost.getService('FormBasicService');
        this.stateMachineService = serviceHost.getService('StateMachineService');
        this.eventEditorService = serviceHost.getService('EventEditorService');
    }
    public getBasicPropConfig(propertyData: any): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '组件的id',
                    readonly: true
                },
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件的类型',
                    iterator: [{
                        key: propertyData.type,
                        value: (DgControl[propertyData.type] && DgControl[propertyData.type].name) || propertyData.type
                    }],
                    readonly: true
                }
            ]
        };
    }


    public getCommonAppearanceProperties(): PropertyEntity[] {
        return [
            {
                propertyID: 'appearance',
                propertyName: '样式',
                propertyType: 'cascade',
                cascadeConfig: [
                    {
                        propertyID: 'class',
                        propertyName: 'class样式',
                        propertyType: 'string',
                        description: '组件的CSS样式'
                    },
                    {
                        propertyID: 'style',
                        propertyName: 'style样式',
                        propertyType: 'modal',
                        description: '组件的内容样式',
                        editor: StyleEditorComponent,
                        showClearButton: true
                    }
                ]
            },
            // {
            //     propertyID: 'size',
            //     propertyName: '尺寸',
            //     propertyType: 'cascade',
            //     cascadeConfig: [
            //         {
            //             propertyID: 'width',
            //             propertyName: '宽度（px）',
            //             propertyType: 'number',
            //             description: '组件的宽度',
            //             min: 0,
            //             decimals: 0
            //         },
            //         {
            //             propertyID: 'height',
            //             propertyName: '高度（px）',
            //             propertyType: 'number',
            //             description: '组件的高度',
            //             min: 0,
            //             decimals: 0
            //         }
            //     ]
            // }
        ];
    }

    getVisiblePropertyEntity(propertyData: any, viewModelId: string) {
        return {
            propertyID: 'visible',
            propertyName: '是否可见',
            propertyType: 'unity',
            description: '运行时组件是否可见',
            editorParams: {
                controlName: UniformEditorDataUtil.getControlName(propertyData),
                constType: 'enum',
                editorOptions: {
                    types: ['const', 'variable'],
                    enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                    variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                    getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                    newVariableType: 'Boolean',
                    newVariablePrefix: 'is'
                }
            }
        };
    }

}
