import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { CodeEditorComponent, StyleEditorComponent } from '@farris/designer-devkit';
import { CommandUsualProp } from '../../common/property/command-property-config';

export class StepsProp extends CommandUsualProp {


    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }

    private getAppearancePropConfig(): ElementPropertyConfig {
        const appearanceInfo = this.getCommonAppearanceProperties() as PropertyEntity[];
        appearanceInfo.push(
            {
                propertyID: 'fill',
                propertyName: '是否填充',
                propertyType: 'boolean'
            },
            {
                propertyID: 'extendTemplate',
                propertyName: '扩展区域模板',
                propertyType: 'modal',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
            {
                propertyID: 'stepsCls',
                propertyName: '单步骤Class样式',
                propertyType: 'string'
            },
            {
                propertyID: 'stepStyle',
                propertyName: '单步骤Style样式',
                propertyType: 'modal',
                editor: StyleEditorComponent,
                showClearButton: true
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceInfo,
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'currentId',
                    propertyName: '当前步骤变量',
                    propertyType: 'unity',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    },

                },
                {
                    propertyID: 'stepsMessage',
                    propertyName: '步骤信息变量',
                    propertyType: 'unity',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    },
                }
            ]
        };
    }
}
