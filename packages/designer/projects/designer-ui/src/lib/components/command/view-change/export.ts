import FdViewChangeComponent from './component/fd-view-change';
import { ViewChangeSchema } from './schema/schema';
import FdViewChangeTemplates from './templates';
import { ComponentExportEntity } from '@farris/designer-element';


export const ViewChange: ComponentExportEntity = {
    type: 'ViewChange',
    component: FdViewChangeComponent,
    template: FdViewChangeTemplates,
    metadata: ViewChangeSchema
};
