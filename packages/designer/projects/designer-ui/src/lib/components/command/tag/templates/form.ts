export default (ctx: any) => {
    let tagCls = 'btn-md ';
    // 没有size属性，跳过
    if (ctx.component.colorType) {
        tagCls += 'btn-' + ctx.component.colorType;
    }
    if (ctx.component.disabled) {
        tagCls += ' disabled farris-tag-disabled';
    }
    let closeStr = '';
    if (ctx.component.closable) {
        closeStr = `<i class="k-icon k-i-close"></i>`;
    }
    return `<div class="farris-tag ide-cmp-tag"><span class="btn ${tagCls}">${ctx.component.label}${closeStr}</span></div>`;
};
