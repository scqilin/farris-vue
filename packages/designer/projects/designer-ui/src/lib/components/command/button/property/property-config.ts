
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { UniformEditorDataUtil, DataStatesService } from '@farris/designer-services';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { IDesignerHost } from '@farris/designer-element';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { CommandUsualProp } from '../../common/property/command-property-config';

export class ButtonProp extends CommandUsualProp {

    dataStatesService: DataStatesService;

    constructor(serviceHost: IDesignerHost, viewModelId: string, componentId: string) {

        super(serviceHost, viewModelId, componentId);

        this.dataStatesService = serviceHost.getService('DataStatesService');

    }
    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const bahaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(bahaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }

    private getAppearancePropConfig(): ElementPropertyConfig {
        const props = this.getCommonAppearanceProperties();
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: props
        };
    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'disable',
                    propertyName: '禁用',
                    propertyType: 'unity',
                    description: '是否禁用',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: this.dataStatesService.dataStates.length != 0 ? ['const', 'stateMachine', 'dataStates'] : ['const', 'stateMachine'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            stateMachine: this.stateMachineService.stateMachineMetaData,
                            dataStates: this.dataStatesService.dataStates,
                            dataStatesBindPath: this.dataStatesService.dataStatesBindPath,
                        }
                    },
                },
                {
                    propertyID: 'dataSourceType',
                    propertyName: '文本数据源类型',
                    propertyType: 'select',
                    description: '文本数据源类型选择',
                    iterator: [{ key: 'static', value: '静态' }, { key: 'dynamic', value: '动态' }],
                    defaultValue: 'static'
                },
                {
                    propertyID: 'bindDataSource',
                    propertyName: '文本',
                    propertyType: 'unity',
                    description: '动态文本数据源文本设置',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService)
                        },

                    },
                    visible: propertyData.dataSourceType === 'dynamic',
                },
                {
                    propertyID: 'text',
                    propertyName: '文本',
                    propertyType: 'string',
                    description: '静态文本数据源文本设置',
                    visible: propertyData.dataSourceType === 'static'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'dataSourceType': {
                        const text = this.properties.find(p => p.propertyID === 'text');
                        const bindDataSource = this.properties.find(p => p.propertyID === 'bindDataSource');

                        if (text) {
                            text.visible = changeObject.propertyValue === 'static';
                        }
                        if (bindDataSource) {
                            bindDataSource.visible = changeObject.propertyValue === 'dynamic';
                        }

                        break;
                    }
                    case 'text': {
                        if (propertyData.titleChanged) {
                            propertyData.titleChanged.next(changeObject);
                        }
                        break;
                    }
                }
            }
        };
    }

    // 事件编辑器集成
    private getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            }
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            hideTitle: true,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService,domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
            }
        };
    }
}
