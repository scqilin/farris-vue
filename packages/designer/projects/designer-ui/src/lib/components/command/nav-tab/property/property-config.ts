import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { ItemCollectionConverter, ItemCollectionEditorComponent } from '@farris/designer-devkit';
import { CommandUsualProp } from '../../common/property/command-property-config';
import { EditorTypes } from '@farris/ui-datagrid-editors';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class NavTabProp extends CommandUsualProp {
    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;


    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonPops = this.getCommonAppearanceProperties();
        commonPops.push(
            {
                propertyID: 'horizontal',
                propertyName: '水平布局',
                propertyType: 'boolean',
                description: '是否水平布局'
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonPops
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const visbileProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visbileProp,
                {
                    propertyID: 'dataSourceType',
                    propertyName: '数据源类型',
                    propertyType: 'select',
                    iterator: [{ key: 'static', value: '静态' }, { key: 'dynamic', value: '动态' }],
                    description: '若要配置标签数值，请使用动态数据源。'
                },
                {
                    propertyID: 'navData',
                    propertyName: '数据源',
                    propertyType: 'modal',
                    description: '静态类型数据源选择',
                    editor: ItemCollectionEditorComponent,
                    converter: new ItemCollectionConverter(),
                    editorParams: {
                        columns: [
                            { field: 'id', title: '编号', editor: { type: EditorTypes.TEXTBOX } },
                            { field: 'text', title: '文本', editor: { type: EditorTypes.TEXTBOX } },
                        ],
                        requiredFields: ['id', 'text'],
                        uniqueFields: ['id', 'text'],
                        modalTitle: '数据源编辑器',
                        canEmpty: false
                    },
                    visible: propertyData.dataSourceType === 'static'
                },
                {
                    propertyID: 'bindDataSource',
                    propertyName: '绑定数据源',
                    propertyType: 'unity',
                    description: '动态类型绑定数据源选择',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    },
                    visible: propertyData.dataSourceType === 'dynamic'
                },
                {
                    propertyID: 'activeNavId',
                    propertyName: '默认选中标签页',
                    propertyType: 'unity',
                    description: '默认选中的标签页',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: this.getActiveNavEnums(propertyData),
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    }
                },
                {
                    propertyID: 'maxNum',
                    propertyName: '显示数字最大值',
                    propertyType: 'number',
                    visible: propertyData.dataSourceType === 'dynamic',
                    description: '超过最大值后以【maxNum+】显示，仅支持动态数据源场景。'
                }
            ],
            setPropertyRelates(changeObject, data, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'dataSourceType': {
                        const navData = this.properties.find(p => p.propertyID === 'navData');
                        const bindDataSource = this.properties.find(p => p.propertyID === 'bindDataSource');
                        const maxNum = this.properties.find(p => p.propertyID === 'maxNum');

                        if (navData) {
                            navData.visible = changeObject.propertyValue === 'static';
                        }
                        if (bindDataSource) {
                            bindDataSource.visible = changeObject.propertyValue === 'dynamic';
                        }
                        if (maxNum) {
                            maxNum.visible = changeObject.propertyValue === 'dynamic';
                        }
                        break;
                    }
                    case 'navData': {

                        const activeNavId = this.properties.find(p => p.propertyID === 'activeNavId');
                        if (activeNavId) {
                            activeNavId.editorParams.editorOptions.enums = self.getActiveNavEnums(propertyData);
                        }
                        // 重置默认标签页
                        const newEnumData = activeNavId.editorParams.editorOptions.enums;
                        const active = newEnumData.find(e => e.key === propertyData.activeNavId);
                        if (!active) {
                            propertyData.activeNavId = newEnumData.length > 0 ? newEnumData[0].key : null;
                        }
                        break;
                    }
                }
            }
        };
    }

    private getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'navPicking',
                name: '切换前事件'
            },
            {
                label: 'navChange',
                name: '切换事件'
            }
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            hideTitle: true,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService,domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);

            }
        };

    }

    /**
     * 将数据源数组转换为多值属性编辑器支持的key-value格式
     * @param propertyData 属性值
     */
    private getActiveNavEnums(propertyData: any) {
        if (!propertyData.navData || !propertyData.navData.length) {
            return [];
        }
        const enums = [];
        propertyData.navData.forEach(data => {
            enums.push({
                key: data.id,
                value: data.text
            });
        });
        return enums;
    }
}
