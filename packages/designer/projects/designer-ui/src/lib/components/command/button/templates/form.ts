
export default (ctx: any) => {
  let cls = '';
  if (ctx.component.appearance && ctx.component.appearance.class) {
    cls = ` class="${ctx.component.appearance.class}" `;
  }
  if (ctx.component.appearance && ctx.component.appearance.style) {
    cls += ` style="${ctx.component.appearance.style}" `;
  }
  return `
      <button id="${ctx.id}" ref="button" ${cls}>${ctx.component.text}</button>
      `;
};

