export const ScrollspySchema ={
    id: '',
    type: 'Scrollspy',
    appearance: {
        class: ''
    },
    currentSectionId: '',
    followType: 'side',
    items: [],
    itemsType: 'static',
    visible: true
};
