import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { CommandUsualProp } from '../../common/property/command-property-config';
import { ItemCollectionConverter, ItemCollectionEditorComponent } from '@farris/designer-devkit';
import { EditorTypes } from '@farris/ui-datagrid-editors';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class ViewChangeProp extends CommandUsualProp {


    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;

    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'changeMethod',
                    propertyName: '切换方式',
                    propertyType: 'select',
                    description: '切换方式选择',
                    iterator: [{ key: 'display', value: '显示隐藏' }, { key: 'route', value: '路由' }],
                    readonly: true
                },
                {
                    propertyID: 'viewType',
                    propertyName: '视图按钮排列方式',
                    propertyType: 'select',
                    description: '视图按钮排列方式设置',
                    iterator: [{ key: 'tile', value: '平铺' }, { key: 'dropdown', value: '下拉选择' }]
                },
                {
                    propertyID: 'toolbarData',
                    propertyName: '视图按钮组',
                    propertyType: 'modal',
                    description: '视图按钮组设置',
                    editor: ItemCollectionEditorComponent,
                    editorParams: {
                        columns: [
                            { field: 'type', title: '类型', editor: { type: EditorTypes.TEXTBOX } },
                            { field: 'title', title: '名称', editor: { type: EditorTypes.TEXTBOX } },
                            { field: 'iconName', title: '图标样式', editor: { type: EditorTypes.TEXTBOX } },
                        ],
                        requiredFields: ['type', 'title', 'iconName'],
                        uniqueFields: ['type', 'title'],
                        modalTitle: '视图按钮编辑器'
                    },
                    converter: new ItemCollectionConverter(),
                    visible: propertyData.changeMethod !== 'display'
                },
                {
                    propertyID: 'defaultType',
                    propertyName: '默认显示的视图类型',
                    propertyType: 'select',
                    description: '默认显示的视图类型设置',
                    iterator: this.getViewChangeDataTypes(propertyData)
                },],
            setPropertyRelates(changeObject, data, parameters) {
                if (!changeObject) {
                    return;
                }
                if (changeObject.propertyID === 'toolbarData') {
                    const defaultProp = this.properties.find(p => p.propertyID === 'defaultType');
                    if (defaultProp) {
                        defaultProp.iterator = self.getViewChangeDataTypes(data);
                    }
                }
            }
        };
    }

    private getEventPropertyConfig(propertyData, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        let eventList = [];
        eventList = self.switchEvents(propertyData, eventList);
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList, self.switchEvents),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, parameters['events'], parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, parameters['events'], self.switchEvents);
            }
        };
    }
    private switchEvents(propertyData, eventList) {
        if (propertyData.changeMethod !== 'display') {
            const eventListExist = eventList.find(eventListItem => eventListItem.label == 'toolTypeChange')
            if (eventListExist === undefined) {
                eventList.push(
                    {
                        label: 'toolTypeChange',
                        name: '视图类型切换事件'
                    }
                );
            }
            return eventList;
        }

        eventList = eventList.filter(eventListItem => eventListItem.label !== 'toolTypeChange');
        return eventList;
    }

    private getViewChangeDataTypes(propData: any) {

        if (!propData || !propData.toolbarData || propData.toolbarData.length === 0) {
            return [];
        }
        const iterators = [];
        propData.toolbarData.forEach(data => {
            iterators.push(
                {
                    key: data.type,
                    value: data.title
                }
            );
        });
        return iterators;
    }
}
