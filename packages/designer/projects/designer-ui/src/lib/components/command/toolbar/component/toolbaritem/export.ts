import FdToolBarItemComponent from './component/fd-toolbar-item';
import { ToolBarItemSchema } from './schema/schema';
import FdToolBarItemTemplates from './templates';
import { ComponentExportEntity } from '@farris/designer-element';
import { cloneDeep } from 'lodash-es';

export const ToolBarItem: ComponentExportEntity = {
    type: 'ToolBarItem',
    component: FdToolBarItemComponent,
    template: FdToolBarItemTemplates,
    metadata: cloneDeep(ToolBarItemSchema)
};
