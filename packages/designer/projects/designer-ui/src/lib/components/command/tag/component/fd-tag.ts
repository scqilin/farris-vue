import { TagSchema } from '../schema/schema';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { TagProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import FdCommandBaseComponent from '../../common/commandBase/commandBase';

export default class FdTagComponent extends FdCommandBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);
    }

    getStyles(): string {
        return 'display: inline-block;';
    }
    getDefaultSchema(): any {
        return TagSchema;
    }

    // 渲染
    render(): any {
        return super.render(this.renderTemplate('Tag', {
            component: this.component
        }));
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: TagProp = new TagProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后事件
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        const propIds = ['colorType', 'disabled', 'label'];

        super.onPropertyChanged(changeObject, propIds);
    }
}
