export default (ctx) => {
    const { top, left, right, bottom } = ctx;
    const container = document.createElement('div');
    container.classList.add('position-absolute', 'component-btn-group');
    container.id = 'dynamic-component__tool';
    const tool =
        `
        <div
          role="button"
          class="btn component-settings-button"
          title="删除"
          ref="removeButton"
        >
          <i class="f-icon f-icon-yxs_delete"></i>
        </div>
        ${ctx.isChildOfButton ? '' :
            `<div
          role="button"
          class="btn component-settings-button"
          title="移动"
          ref="moveButton"
        >
          <i class="f-icon f-icon-move"></i>
        </div>`
        }
        <div
          role="button"
          class="btn component-settings-button"
          title="复制"
          ref="copyButton"
        >
          <i class="f-icon f-icon-yxs_copy"></i>
        </div>
      `;
    container.innerHTML = tool;
    document.body.appendChild(container);
    return container;
};
