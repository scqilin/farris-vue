export const NavTabSchema = {
    id: '',
    type: 'NavTab',
    visible: true,
    dataSourceType: 'static',
    navData: [{ id: 'tab1', text: '标签一' }, { id: 'tab2', text: '标签二' }, { id: 'tab3', text: '标签三' }],
    horizontal: true,
    activeNavId: 'tab1',
    maxNum: null,
    navChange: null,
    navPicking: null
};
