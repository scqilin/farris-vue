import ToolBarItemComponent from '../component/toolbaritem/component/fd-toolbar-item';
export default (ctx: any) => {
  const buttons = ctx.toolBarItemList.map((toolBarItemInstance: ToolBarItemComponent) => toolBarItemInstance.render());
  const contentEndCls = ctx.justifyContentEnd ? 'justify-content-end' : '';
  return `
    <div class="flex-grow-1 w-100 f-response-toolbar-default f-response-toolbar position-relative">
      <div class="d-flex flex-nowrap ${contentEndCls}">
        <div class="toolbar-drag d-inline-block f-response-content">
          ${buttons.join('')}
        </div>
      </div>
    </div>`;
};

