import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { DesignerEnvType, FormBasicService } from '@farris/designer-services';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export default class FdCommandBaseComponent extends FarrisDesignBaseComponent {

    envType: DesignerEnvType;
    constructor(component: any, options: any) {
        super(component, options);

        // 组件所属分类为“命令”
        this.category = 'command';

        const serviceHost = this.options.designerHost;
        const formBasicService = serviceHost.getService('FormBasicService') as FormBasicService;
        if (formBasicService) {
            this.envType = formBasicService.envType;
        }
    }

    /**
     * 属性变更后事件：默认监听样式类属性变更，并触发模板重绘
     * @param changeObject 变更集
     * @param propertyIDs 需要额外监听的属性ID列表
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs: string[]): void {
        let dynamicPropertyIDs = ['appearance.class', 'appearance.style', 'size.width', 'size.height'];
        if (propertyIDs) {
            dynamicPropertyIDs = dynamicPropertyIDs.concat(propertyIDs);
        }

        const propertyPath = changeObject.propertyPath ? changeObject.propertyPath + '.' : '';
        if (dynamicPropertyIDs.includes(propertyPath + changeObject.propertyID)) {
            this.triggerRedraw();
        }

    }
}
