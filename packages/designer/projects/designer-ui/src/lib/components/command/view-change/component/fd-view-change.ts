import { ViewChangeSchema } from '../schema/schema';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ViewChangeProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { OperateUtils } from '../../../../utils/operate.utils';
import FdCommandBaseComponent from '../../common/commandBase/commandBase';
export default class ViewChangeComponent extends FdCommandBaseComponent {
    // 当前选中的项
    activeTypeItem;
    // 标识当前的legend
    get viewChangeKey(): string {
        return `viewChange-${this.key}`;
    }
    // 查找formParent
    private formParent = null;
    private viewItems = null;
    component;
    constructor(component: any, options: any) {
        super(component, options);
        this.applyClassToComponent = false;
    }



    getDefaultSchema(): any {

        return ViewChangeSchema;
    }

    getComponentAttributes() {
        return {
            viewchange: this.component.viewGroupIdentify
        };
    }

    // 初始化
    init(): void {
        this.initActiveItem(this.component.currentTypeInDesignerView || this.component.defaultType);
        super.init();
    }

    // 渲染
    render(): any {
        return super.render(this.renderTemplate('ViewChange', {
            component: this.component,
            getViewChangeStr: () => this.getViewChangeStr(),
            viewChangeKey: this.viewChangeKey
        }));
    }

    // 绑定事件
    attach(element: HTMLElement): Promise<any> {
        this.loadRefs(element, {
            [this.viewChangeKey]: 'single'
        });

        const superAttach = super.attach(element);
        // 平铺
        if (this.component.viewType == 'tile') {
            const tileBtns = this.refs[this.viewChangeKey].querySelectorAll('.f-view-change-tile-btn');
            tileBtns.forEach(tileBtn => {
                this.addEventListener(tileBtn, 'click', (event) => {
                    event.stopPropagation();
                    this.viewChangeBtnClick(tileBtn);
                    this.redraw();
                });
            });

        } else if (this.component.viewType == 'dropdown') {
            //下拉            
            // 展开收起下拉面板
            const viewDropdown = this.refs[this.viewChangeKey].querySelector('.f-view-change-dropdown');
            const viewDroppanel = this.refs[this.viewChangeKey].querySelector('.f-view-change-typelist');
            // 按钮
            const listItems = this.refs[this.viewChangeKey].querySelectorAll('.typelist-item');
            listItems.forEach(tileBtn => {
                this.addEventListener(tileBtn, 'click', (event) => {
                    event.stopPropagation();
                    this.viewChangeBtnClick(tileBtn);
                    viewDroppanel.style.display = 'none';
                    this.redraw();
                });
            });
            this.addEventListener(viewDropdown, 'mouseenter', (event) => {
                event.stopPropagation();
                viewDroppanel.style.display = 'block';
            });
            this.addEventListener(viewDropdown, 'mouseleave', (event) => {
                event.stopPropagation();
                viewDroppanel.style.display = 'none';
            });
        }
        // this.attachViewChange();
        return superAttach;
    }
    /**
     * 
     * @param tileBtn 
     */
    private viewChangeBtnClick(tileBtn) {
        const activeType = tileBtn.getAttribute('viewChange');
        if (activeType !== this.activeTypeItem.type) {
            this.changeRelatedViewItem(activeType);
            this.initActiveItem(activeType);
        }
    }

    /**
     * 点击按钮，触发view change操作
     */
    private changeRelatedViewItem(viewChangeType: string) {
        this.findFormParent();
        const multiViewContainerEle = this.formParent.querySelector('.farris-component-MultiViewContainer[viewchange=' + this.component.viewGroupIdentify + ']');
        if (!multiViewContainerEle) {
            return;
        }
        const multiViewContainerInstance = multiViewContainerEle.componentInstance;
        if (!multiViewContainerInstance || !multiViewContainerInstance.updateCurrentViewItem) {
            return;
        }
        multiViewContainerInstance.updateCurrentViewItem(viewChangeType);
    }


    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: ViewChangeProp = new ViewChangeProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        super.onPropertyChanged(changeObject, ['viewType', 'toolbarData', 'defaultType']);
    }

    /**
     * 查找form父元素
     */
    private findFormParent() {
        if (this.formParent) {
            return;
        }
        this.formParent = OperateUtils.getFormParent(this.element);
    }
    /**
     * 当前选中行设置
     */
    initActiveItem(activeType) {
        if (this.component.toolbarData && this.component.toolbarData.length > 0) {
            if (activeType) {
                const item = this.component.toolbarData.find((bar) => {
                    return bar['type'] === activeType;
                });
                this.activeTypeItem = item;
            }
            if (!this.activeTypeItem) {
                this.activeTypeItem = this.component.toolbarData[0];
            }

            this.component.currentTypeInDesignerView = activeType;
        }
    }
    /**
     * 获取view chagne的字符串
     */
    getViewChangeStr() {
        return OperateUtils.getViewChangeStr(this.component.toolbarData, this.component.viewType, this.activeTypeItem);
    }

    checkCanDeleteComponent(): boolean {
        return false;
    }

    checkCanMoveComponent(): boolean {
        return false;
    }
}
