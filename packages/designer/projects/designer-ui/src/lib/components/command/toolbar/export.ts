import FdToolBarComponent from './component/fd-toolbar';
import { ToolBarSchema } from './schema/schema';
import FdToolBarTemplates from './templates';
import { ComponentExportEntity } from '@farris/designer-element';
import { IdService } from '@farris/ui-common';


export const ToolBar: ComponentExportEntity = {
    type: 'ToolBar',
    component: FdToolBarComponent,
    template: FdToolBarTemplates,
    metadata: ToolBarSchema,
    uniqueMedataItems: (metadata) => {
        if (metadata.items && metadata.items.length) {
            metadata.items.forEach(m => m.id = 'toolbarItem-' + new IdService().generate().slice(0, 4));
        }
    }
};


