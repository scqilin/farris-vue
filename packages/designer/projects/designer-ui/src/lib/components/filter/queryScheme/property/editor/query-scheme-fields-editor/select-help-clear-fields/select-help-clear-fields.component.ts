import { Component, OnInit, Output, Input, EventEmitter, ViewChild, TemplateRef } from '@angular/core';

@Component({
    selector: 'app-select-help-clear-fields',
    templateUrl: './select-help-clear-fields.component.html'
})
export class SelectHelpClearFieldsEditorComponent implements OnInit {
    @Output() closeModal = new EventEmitter<any>();
    @Output() submitModal = new EventEmitter<any>();
    @Input() value;
    @Input() editorParams = {
        fieldConfigs: [],
        // 排除字段本身
        currentId: ''
    };
    @ViewChild('footer') modalFooter: TemplateRef<any>;

    modalConfig = {
        title: '字段选择器',
        width: 900,
        height: 500,
        showButtons: true,
        showMaxButton: false
    };

    // 左侧列表数据
    fieldConfigData = [];

    // 选中的字段label列表
    selectedFieldIds = [];

    // 选中的字段列表
    selectedFields = [];

    constructor() { }

    ngOnInit(): void {
        if (!this.editorParams) {
            return;
        }
        this.fieldConfigData = this.editorParams.fieldConfigs.filter(f => f.id !== this.editorParams.currentId);

        if (this.value) {
            this.selectedFields = this.selectedFieldIds = this.value.split(',');
        }
    }

    changeSelectField(e) {
        this.selectedFields = e.map(f => f.labelCode);
    }
    /**
     * 取消
     */
    clickCancel() {
        this.closeModal.emit();
    }
    /**
     * 确定
     */
    clickConfirm() {
        this.submitModal.emit({ value: this.selectedFields.join(',') });
    }


}
