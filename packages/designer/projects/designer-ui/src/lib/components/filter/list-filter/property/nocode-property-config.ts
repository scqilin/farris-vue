import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ListFilterProp } from './property-config';

export class NoCodeListFilterProp extends ListFilterProp {
    propertyConfig: ElementPropertyConfig[];


    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 常规属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        return this.propertyConfig;
    }

}
