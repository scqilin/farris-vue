export default (ctx: any) => {
  if (ctx.component.controlSource === 'normal') {
    return resolveNormalFilterTemplate(ctx);
  } else {
    return resolveLightFilterTemplate(ctx);
  }

};

/**
 * 渲染常规筛选条
 */
function resolveNormalFilterTemplate(ctx: any) {

  // 自动宽度相关样式
  const autoWidth = ctx.component.autoWidth ? 'list-filter-autoWidth' : '';
  const listPadding = ctx.showMoreIcon ? 'list-filter-padding' : '';

  // 父级div的样式
  const filterInputsCls = getFilterInputCls(ctx);

  // 实时搜索区域
  const liveSearch = getLiveSearch(ctx);

  // 主区域
  let mainDiv = '';
  if (autoWidth) {
    const alLeftCls = !ctx.component.alLeft ? 'justify-content-end' : '';


    mainDiv = `
    <div class="filter-inputs-main-basis-wrapper-inner">
      <div class="filter-inputs-main filter-inputs-main-basis farris-form d-flex farris-form-controls-inline ${alLeftCls}">
        ${ctx.filterControlTpls}
      </div>
    </div>

    ${liveSearch}
    `;
  } else {
    mainDiv = `
    ${ctx.filterControlTpls}
    ${liveSearch}
    `;
  }

  const moreIconPanel = getMoreIconPanel(ctx);

  return `
  <div class="f-list-filter position-relative ${autoWidth} ${listPadding}">
    <div class="${filterInputsCls}">
      ${mainDiv}
    </div>

    ${moreIconPanel}
  </div>
      `;

}

/**
 * 渲染轻量筛选条
 */
function resolveLightFilterTemplate(ctx: any) {
  // 控件区
  let controlTpls = '';
  if (ctx.showInPageFilterList && ctx.showInPageFilterList.length) {
    ctx.showInPageFilterList.forEach(filterConfig => {
      controlTpls += `
      <div class="f-filter-item" style="display: flex;">
        <div class="f-filter-item-inner">
          <span class="f-filter-item-text"> ${filterConfig.name} </span>
          <span class="f-filter-item-arrow f-icon f-icon-arrow-chevron-down"></span></div>
      </div>`
    })
  }
  // 高级筛选
  let sidebarBtn = '';
  if (ctx.component.showExtendInSidebar && ctx.showMoreIcon) {
    sidebarBtn = `
      <div class="f-filter-toolbars" *ngIf="options.showExtendInSidebar && hasExtendFields">
        <button class="btn btn-link">高级筛选</button>
      </div>`;
  }

  // 筛选图标
  let advancedBtn = '';
  if (!ctx.component.showExtendInSidebar && ctx.showMoreIcon) {
    advancedBtn = `
    <div class="f-filter-extend-btn-advanced" *ngIf="!options.showExtendInSidebar && hasExtendFields">
    <span class="f-icon f-icon-filtrate"></span>
  </div>`;
  }
  return `
<div class="f-filter-wrapper">
  <div class="f-filter-wrapper-inner f-utils-fill">
    <div class="f-filter-main f-utils-fill">
      <div class="f-filter-list-wrapper">
        <div class="f-filter-list">
          ${controlTpls}
        </div>
      </div>
      ${sidebarBtn}
    </div>
    ${advancedBtn}

  </div>

</div>
  `;
}


// 父级div样式是根据是否自动宽度区分的
function getFilterInputCls(ctx: any) {
  const autoWidth = ctx.component.autoWidth;
  let filterInputsCls = '';
  if (autoWidth) {
    filterInputsCls = 'filter-inputs-main-basis-wrapper';
    if (!ctx.component.liveSearch) {
      filterInputsCls += ' list-filter-wrapper-show-btn';
      if (!ctx.component.liveSearch && !ctx.showMoreIcon) {
        filterInputsCls += ' list-filter-wrapper-show-btn-remove';
      }
    }
  } else {
    filterInputsCls = 'filter-inputs-main f-form-layout farris-form farris-form-controls-inline';
    const showLength = ctx.showFilterListLength >= 6 ? 6 : ctx.showFilterListLength;
    filterInputsCls += ' list-filter-type-' + showLength;
    if (ctx.component.filterClass) {
      filterInputsCls += ' list-filter-custom-type';
    }
    if (!ctx.showMoreIcon) {
      filterInputsCls += ' list-filter-show-reset';
    }

    if (ctx.component.liveSearch) {
      filterInputsCls += ' list-filter-padding-none';
    }
  }

  return filterInputsCls;
}

// 实时搜索区域模板
function getLiveSearch(ctx) {
  let searchBtn = ` <button class="btn btn-primary disabled">筛选</button> `;
  if (!ctx.showMoreIcon) {
    searchBtn += ' <button class="btn btn-secondary filter-search-btn-reset "><span class="f-icon f-icon-remove"></span></button>';
  }
  if (ctx.component.liveSearch) {
    return '';
  }
  let cls = 'filter-search-btn';
  if (ctx.component.autoWidth) {
    if (!ctx.showMoreIcon) {
      cls += ' filter-search-btn-remove';
    }

  } else {
    const showLength = ctx.showFilterListLength >= 6 ? 6 : ctx.showFilterListLength;
    cls += ' filter-search-btn-type-' + showLength;

    if (ctx.component.filterClass) {
      cls += ' filter-search-custom-type';
    }
  }

  return `<div class="${cls}"> ${searchBtn}</div>`;
}

function getMoreIconPanel(ctx: any) {
  if (!ctx.showMoreIcon) {
    return '';
  }
  return `
  <div class="filter-expand position-absolute">
    <span class="f-icon f-icon-filtrate"></span>
  </div>`;
}
