import { KeyMap } from '@farris/ide-property-panel';
import { FormSchemaEntityFieldTypeName } from '@farris/designer-services';
import { GSPElementDataType } from '@gsp-bef/gsp-cm-metadata';

export class QuerySchemaFieldTypeEnums {


    static getControlTypeEnum(fieldType: string): KeyMap[] {
        const controlTypes = [];
        switch (fieldType) {
            case FormSchemaEntityFieldTypeName.String: {
                controlTypes.push({ key: 'text', value: '文本' });
                controlTypes.push({ key: 'help', value: '帮助' });
                controlTypes.push({ key: 'combolist-help', value: '下拉帮助' });
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });
                controlTypes.push({ key: 'single-year', value: '年度' });
                controlTypes.push({ key: 'single-month', value: '单月份' });
                controlTypes.push({ key: 'month', value: '月份区间' });
                controlTypes.push({ key: 'date', value: '日期区间' });
                controlTypes.push({ key: 'single-date', value: '日期' });
                controlTypes.push({ key: 'date-time', value: '日期时间区间' });
                controlTypes.push({ key: 'single-date-time', value: '单日期时间' });
                controlTypes.push({ key: 'input-group', value: '智能输入框' });
                break;
            }
            case FormSchemaEntityFieldTypeName.Number: case GSPElementDataType.Integer: case GSPElementDataType.Decimal: {
                controlTypes.push({ key: 'number', value: '数值区间' });
                controlTypes.push({ key: 'single-number', value: '数值' });
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });
                break;
            }
            case FormSchemaEntityFieldTypeName.BigNumber: {
                controlTypes.push({ key: 'number', value: '数值区间' });
                controlTypes.push({ key: 'single-number', value: '数值' });
                break;
            }
            case FormSchemaEntityFieldTypeName.Date: {
                controlTypes.push({ key: 'date', value: '日期区间' });
                controlTypes.push({ key: 'single-date', value: '日期' });
                controlTypes.push({ key: 'date-time', value: '日期时间区间' });
                break;
            }
            case FormSchemaEntityFieldTypeName.DateTime: {
                controlTypes.push({ key: 'date', value: '日期区间' });
                controlTypes.push({ key: 'single-date', value: '日期' });
                controlTypes.push({ key: 'date-time', value: '日期时间区间' });
                controlTypes.push({ key: 'single-date-time', value: '单日期时间' });
                break;
            }
            case FormSchemaEntityFieldTypeName.Enum: {
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });
                controlTypes.push({ key: 'radio', value: '单选组' });
                break;
            }
            case FormSchemaEntityFieldTypeName.Boolean: {
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });
                controlTypes.push({ key: 'bool-check', value: '复选框' });
                break;
            }
        }
        return controlTypes;
    }
}
