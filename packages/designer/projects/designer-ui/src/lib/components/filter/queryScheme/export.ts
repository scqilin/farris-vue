import QuerySchemeComponent from './component/fd-queryScheme';
import { QuerySchemeSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdQuerySchemeTemplates from './templates';
import FdQuerySchemeItemTemplates from './component/filter-bar-item/templates';

export const QueryScheme: ComponentExportEntity = {
    type: 'QueryScheme',
    component: QuerySchemeComponent,
    template: FdQuerySchemeTemplates,
    metadata: QuerySchemeSchema
};


export const QuerySchemeItem: ComponentExportEntity = {
    type: 'QuerySchemeItem',
    template: FdQuerySchemeItemTemplates,
    metadata: {}
};
