export const ListFilterControlMapping = {
    // 文本
    text: 'TextBox',

    // 搜索 
    search: 'Search',
    // 帮助
    help: 'LookupEdit',

    // 下拉选择
    dropdown: 'EnumField',

    // 复选组
    checkboxgroup: 'CheckGroup',

    // 数值区间
    number: 'NumberRange',

    // 单选组
    radio: 'RadioGroup',

    // 年度
    year: 'DateBox',

    // 年度范围
    yearRange: 'DateRange',

    // 年月
    month: 'DateBox',

    // 年月范围
    monthRange: 'DateRange',

    // 智能输入框
    'input-group': 'InputGroup',

    // 日期
    singleDate: 'DateBox',

    // 日期区间
    date: 'DateRange',

    // 日期时间区间
    datetime: 'DateRange'

};
