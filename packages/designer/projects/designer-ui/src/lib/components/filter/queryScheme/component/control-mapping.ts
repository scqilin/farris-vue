/**
 * 筛选方案控件类型与输入控件类型的映射
 */
export const QuerySchemeControlMapping = {
    // 文本
    text: 'TextBox',

    // 帮助
    help: 'LookupEdit',

    // 下拉帮助
    'combolist-help': 'LookupEdit',

    // 下拉选择
    dropdown: 'EnumField',

    // 年度
    'single-year': 'DateBox',

    // 单月份
    'single-month': 'DateBox',

    // 月份区间
    month: 'DateRange',

    // 日期区间
    date: 'DateRange',

    // 日期
    'single-date': 'DateBox',

    // 日期时间区间
    'date-time': 'DateRange',

    // 单日期时间
    'single-date-time': 'DateBox',

    // 智能输入框
    'input-group': 'InputGroup',

    // 数值区间
    number: 'NumberRange',

    // 数值
    'single-number': 'NumericBox',

    // 单选组
    radio: 'RadioGroup',

    // 复选框
    'bool-check': 'CheckBox'
};
