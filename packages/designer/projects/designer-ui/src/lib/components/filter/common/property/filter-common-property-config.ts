
import { EventEditorService, DomService, FormBasicService, RefreshFormService, WebCmdService, UniformEditorDataUtil, StateMachineService } from "@farris/designer-services";
import { IDesignerHost } from "@farris/designer-element";
import { MessagerService } from "@farris/ui-messager";
import { NotifyService } from "@farris/ui-notify";

export class FilterCommonProp {
  public formBasicService: FormBasicService;
  public eventEditorService: EventEditorService;
  public refreshFormService: RefreshFormService;
  public domService: DomService;
  public msgService: MessagerService;
  public notifyService: NotifyService;
  public webCmdService: WebCmdService;
  public viewModelId: string;
  public componentId: string;
  stateMachineService: StateMachineService;

  constructor(
    private serviceHost: IDesignerHost,
    viewModelId: string,
    componentId: string,
  ) {
    this.viewModelId = viewModelId;
    this.componentId = componentId;
    this.domService = serviceHost.getService('DomService');
    this.webCmdService = serviceHost.getService('WebCmdService');
    this.formBasicService = serviceHost.getService('FormBasicService');
    this.refreshFormService = serviceHost.getService('RefreshFormService');
    this.eventEditorService = serviceHost.getService('EventEditorService');
    const injector = serviceHost.getService('Injector');
    this.notifyService = injector.get(NotifyService);
    this.msgService = injector.get(MessagerService);
    this.stateMachineService = serviceHost.getService('StateMachineService');

  }


  getVisiblePropertyEntity(propertyData: any, viewModelId: string) {
    return {
      propertyID: 'visible',
      propertyName: '是否可见',
      propertyType: 'unity',
      description: '运行时组件是否可见',
      editorParams: {
        controlName: UniformEditorDataUtil.getControlName(propertyData),
        constType: 'enum',
        editorOptions: {
          types: ['const', 'variable'],
          enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
          variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
          getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
          newVariableType: 'Boolean',
          newVariablePrefix: 'is'
        }
      }
    };
  }
}
