export default (ctx: any) => {
  const require = ctx.filterConfig.control.require ? '<span class="farris-label-info text-danger">*</span>' : '';
  const className = ctx.component.className ? ctx.component.className : 'col-12 col-md-6 col-xl-3 col-el-2';

  const item = `

    <div class="farris-group-wrap">
        <div class="form-group farris-form-group common-group">
          <label class="col-form-label" title="${ctx.filterConfig.name}">
            ${require}
            <span class="farris-label-text">${ctx.filterConfig.name}</span>
          </label>
          ${ctx.input}
        </div>
    </div>
  `;
  return `
    <div class="${className}">
        ${item}
    </div>
    `;
};



