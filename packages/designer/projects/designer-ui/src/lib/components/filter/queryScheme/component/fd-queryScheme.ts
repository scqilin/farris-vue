import { QuerySchemeSchema } from '../schema/schema';
import { BuilderHTMLElement } from '@farris/designer-element';
import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { QuerySchemeItemComponent } from './filter-bar-item/component/fd-query-scheme-item';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { QuerySchemeProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { RowNode } from '@farris/ui-treetable';
import { QuerySchemeContextMenuManager } from '../context-menu/context-menu.manager';
import { DgControl } from '../../../../utils/dg-control';
import { DomService, FormSchemaEntityField$Type, SchemaService, WebCmdService } from '@farris/designer-services';
import { QuerySchemaFieldCreator } from '../property/editor/query-scheme-fields-editor/query-data/field-control-data';
import { QuerySchemaFieldTypeEnums } from '../property/editor/query-scheme-fields-editor/query-data/field-control-types';

export default class QuerySchemeComponent extends FarrisDesignBaseComponent {

    // 展示在界面上的控件模板字符串
    filterControlTpls = '';


    getClassName(): string {
        return super.getClassName() + ' query-solution';
    }
    getDefaultSchema(): any {
        return QuerySchemeSchema;
    }

    init() {
        this.checkFieldSchemaAndControl();
    }

    render(): any {
        this.assembleFilterControls();

        return super.render(this.renderTemplate('QueryScheme', {
            component: this.component,
            filterControlTpls: this.filterControlTpls,
        }));

    }

    attach(element: any): any {
        const superAttach: any = super.attach(element);

        const domService = this.options.designerHost.getService('DomService') as DomService;

        // 将各筛选项追加到domservice的map中，目的是为了在交互面板【已绑定命令】中展示路径信息
        const filterList = this.component.fieldConfigs || [];
        filterList.forEach(filterConfig => {
            if (!filterConfig.control || !filterConfig.control.controltype) {
                return;
            }
            domService.controlBasicInfoMap.set(filterConfig.control.id, {
                showName: filterConfig.name,
                parentPathName: `筛选方案 > ${filterConfig.name}`
            });
        });
        return superAttach;
    }


    /**
     * 判断是否可以接收拖拽新增的子级控件
     * @param data 新控件的类型、所属分类
     * @returns boolean
     */
    canAccepts(el: BuilderHTMLElement, target: BuilderHTMLElement) {
        return false;
    }

    /**
     * 构造筛选项
     */
    private assembleFilterControls(): void {
        const presetFieldConfigs = this.component.presetFieldConfigs || [];

        this.filterControlTpls = '';

        presetFieldConfigs.forEach(presetFieldConfigItem => {
            if (!presetFieldConfigItem.control || !presetFieldConfigItem.control.controltype) {
                return;
            }
            const itemTemplate = this.renderNormalItem(presetFieldConfigItem);
            this.filterControlTpls += ' ' + itemTemplate;
        });
    }

    /**
     * 常规筛选：渲染单个输入框
     * @param filterConfig 配置数据
     */
    private renderNormalItem(filterConfig: any): any {
        const inputProp = Object.assign({}, {
            controltype: filterConfig.control.controltype,
            require: filterConfig.control.require,
            enumData: filterConfig.control.enumValues,
            items: filterConfig.control.enumValues,
            placeHolder: filterConfig.placeHolder || '',
            beginPlaceHolder: filterConfig.beginPlaceHolder || '',
            endPlaceHolder: filterConfig.endPlaceHolder || '',
            isHorizontal: true,
            groupText: filterConfig.control.groupText,
            className: filterConfig.control.className
        });
        const itemComponent = new QuerySchemeItemComponent(inputProp, this.options, this);

        return itemComponent.renderItem(filterConfig);
    }

    /**
     * 属性面板构造数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: QuerySchemeProp = new QuerySchemeProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }


    /**
     * 属性变更后事件
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        const mappingPropIds = ['presetQuerySolutionName', 'fieldConfigs', 'presetFieldConfigs', 'isControlInline', 'showCompleteLabel'];

        if (mappingPropIds.includes(changeObject.propertyID)) {
            this.triggerRedraw();

            // 涉及到字段绑定事件的变更，故触发同步actions操作
            if (changeObject.propertyID === 'fieldConfigs') {
                const webCmdService = this.options.designerHost.getService('WebCmdService') as WebCmdService;
                webCmdService.syncActions();
            }
        }
    }


    /**
     * 组装右键菜单
     * @param rowNode 组件在控件树上对应的行数据
     */
    resolveContextMenuConfig(rowNode: RowNode) {
        const menuManager = new QuerySchemeContextMenuManager(this, rowNode);
        return menuManager.setContextMenuConfig();
    }

    /**
     * 从控件工具箱拖拽生成控件时获取控件必须的上下文容器
     */
    getMetadataContextInDrag() {

    }


    /**
     * 禁止移动
     */
    checkCanMoveComponent(): boolean {
        return false;
    }

    /**
     * 删除筛选方案时连带删除上层容器Section
     */
    onRemoveComponent() {
        const parentSchema = this.parent && this.parent.component;
        if (!parentSchema) {
            return;
        }
        const parentClass = parentSchema.appearance ? parentSchema.appearance.class : '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        const grandParent = this.parent.parent;
        const grandParentSchema = grandParent && grandParent.component;

        // 上层为筛选方案容器，则需要将容器一同移除
        if (grandParentSchema && parentSchema.type === DgControl.Section.type && parentClassList.includes('f-section-scheme')) {
            const sectionIndex = grandParentSchema.contents.findIndex(c => c.id === parentSchema.id);
            if (sectionIndex > -1) {
                grandParentSchema.contents.splice(sectionIndex, 1);


                // 移除根节点对应的样式
                this.removeSchemeClassInRootLayout();

                // 为了后续触发重绘的父级组件，返回筛选条容器的父级容器
                return { parentComponentInstance: grandParent };
            }

        }
    }

    /**
     * 移除根节点对应的筛选方案样式
     */
    private removeSchemeClassInRootLayout() {

        const domService = this.options.designerHost.getService('DomService') as DomService;
        const rootComponentJson = domService.getComponentById('root-component');
        const templateOutlineSchema = domService.templateOutlineSchema || [];
        const schemeConfig = templateOutlineSchema.find(schema => schema.queryScheme && schema.queryScheme.canEnable);
        if (schemeConfig && schemeConfig.queryScheme && schemeConfig.queryScheme.relatedClass && schemeConfig.queryScheme.relatedClass.length) {
            const relatedClass = schemeConfig.queryScheme.relatedClass[0];

            const relateDom = domService.getNodeByIdPath(rootComponentJson, relatedClass.path);
            if (relateDom) {
                if (relateDom.appearance && relateDom.appearance.class) {
                    relateDom.appearance.class = relateDom.appearance.class.replace(relatedClass.class, '');
                }
            }


        }
    }

    /**
     * 检查字段类型与控件类型的适配度
     */
    private checkFieldSchemaAndControl() {
        const schemService = this.options.designerHost.getService('SchemaService') as SchemaService;
        this.component.fieldConfigs.forEach(fieldConfig => {
            if (!fieldConfig.id || !fieldConfig.control) {
                return;
            }
            const schemaField = schemService.getFieldByID(fieldConfig.id);
            // 1、字段不存在：自动删除
            if (!schemaField) {
                fieldConfig.isInValid = true;
                const presetFieldConfig = this.component.presetFieldConfigs.find(c => c.id === fieldConfig.id);
                if (presetFieldConfig) {
                    presetFieldConfig.isInValid = true;
                }
                return;
            }
            // 2、字段由简单字段变更为复杂字段：自动删除
            if (schemaField.$type === FormSchemaEntityField$Type.ComplexField) {
                fieldConfig.isInValid = true;
                const presetFieldConfig = this.component.presetFieldConfigs.find(c => c.id === fieldConfig.id);
                if (presetFieldConfig) {
                    presetFieldConfig.isInValid = true;
                }
                return;
            }
            const schemaFieldType = schemaField.type.name;
            const allowedQueryControlTypes = QuerySchemaFieldTypeEnums.getControlTypeEnum(schemaFieldType);
            if (!allowedQueryControlTypes.find(type => type.key === fieldConfig.control.controltype)) {
                // 3、字段类型与控件类型不匹配：自动替换控件
                const newControl = QuerySchemaFieldCreator.getSolutionField(schemaField);
                Object.assign(fieldConfig, newControl);
                const presetFieldConfig = this.component.presetFieldConfigs.find(c => c.id === fieldConfig.id);
                if (presetFieldConfig) {
                    Object.assign(presetFieldConfig, fieldConfig);
                }

            }

        });
        this.component.fieldConfigs = this.component.fieldConfigs.filter(config => !config.isInValid);
        this.component.presetFieldConfigs = this.component.presetFieldConfigs.filter(config => !config.isInValid);
    }
}
