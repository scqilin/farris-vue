import { FormSchemaEntityField, FormSchemaEntityFieldTypeName } from '@farris/designer-services';
import { GSPElementDataType } from '@gsp-bef/gsp-cm-metadata';
import { DgControl } from '../../../../../../../utils/dg-control';

export class QuerySchemaFieldCreator {

    /** 占位符显示区间的控件 */
    static rangePlaceHolderControl = ['number', 'date', 'date-time', 'month'];

    /**
     * 根据指定的控件类型和schema信息生成筛选方案control节点
     * @param controlType 控件类型
     * @param schemaField 字段schema信息
     */
    static getControlDataByType(controlType: string, schemaField: FormSchemaEntityField): any {
        switch (controlType) {
            case 'dropdown': {
                if (schemaField.editor.$type === DgControl.EnumField.type) {
                    return {
                        id: schemaField.id,
                        controltype: 'dropdown',
                        require: false,
                        valueType: '1',
                        enumValues: schemaField.type.enumValues,
                        className: '',
                        multiSelect: false,
                        panelHeight: null,
                        idField: 'value',
                        textField: 'name',
                        uri: ''
                    };
                } else if (schemaField.editor.$type === DgControl.CheckBox.type) {
                    return {
                        id: schemaField.id,
                        controltype: 'dropdown',
                        require: false,
                        valueType: '2',
                        enumValues: [{ value: true, name: 'true' }, { value: false, name: 'false' }],
                        className: '',
                        multiSelect: false,
                        panelHeight: null,
                        idField: 'value',
                        textField: 'name',
                        uri: ''
                    };
                } else if (schemaField.editor.$type === DgControl.TextBox.type || schemaField.editor.$type === DgControl.NumericBox.type) {
                    return {
                        id: schemaField.id,
                        controltype: 'dropdown',
                        require: false,
                        valueType: '1',
                        enumValues: [],
                        className: '',
                        multiSelect: false,
                        panelHeight: null,
                        idField: 'value',
                        textField: 'name',
                        uri: ''
                    };
                }
                break;
            }
            case 'help': {
                return {
                    id: schemaField.id,
                    controltype: 'help',
                    require: false,
                    helpId: '',
                    uri: '',
                    idField: '',
                    valueField: '',
                    textField: '',
                    displayType: '',
                    enableExtendLoadMethod: true,
                    enableFullTree: false,
                    loadTreeDataType: 'default',
                    multiSelect: false,
                    enableCascade: false,
                    cascadeStatus: 'enable',
                    expandLevel: -1,
                    className: '',
                    nosearch: false,
                    displayFields: '',
                    clearFields: '',
                    editable: false,
                    pageList: '10,20,30,50,100',
                    pageSize: 20,
                    dialogTitle: '',
                    panelHeight: null,
                    panelWidth: null,
                    quickSelect: {
                        enable: false,
                        showItemsCount: 10,
                        formatter: null,
                        showMore: true
                    }
                };
            }
            case 'combolist-help': {
                return {
                    id: schemaField.id,
                    controltype: 'combolist-help',
                    require: false,
                    className: '',
                    helpId: '',
                    uri: '',
                    idField: '',
                    valueField: '',
                    textField: '',
                    displayType: '',
                    enableExtendLoadMethod: true,
                    enableFullTree: false,
                    loadTreeDataType: 'default',
                    multiSelect: false,
                    expandLevel: -1,
                    panelHeight: null,
                    panelWidth: null
                };
            }
            case 'number': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    textAlign: 'left',
                    precision: schemaField.type.precision || 0,
                    isBigNumber: schemaField.type.name === FormSchemaEntityFieldTypeName.BigNumber
                };
            }
            case 'single-number': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    textAlign: 'left',
                    precision: schemaField.type.precision || 0,
                    isBigNumber: schemaField.type.name === FormSchemaEntityFieldTypeName.BigNumber
                };
            }
            case 'date': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    format: 'yyyy-MM-dd',
                    returnFormat: 'yyyy-MM-dd',
                    weekSelect: false,
                    startFieldCode: [FormSchemaEntityFieldTypeName.Date, FormSchemaEntityFieldTypeName.DateTime].includes(schemaField.type.name) ? schemaField.path : undefined,
                    endFieldCode: [FormSchemaEntityFieldTypeName.Date, FormSchemaEntityFieldTypeName.DateTime].includes(schemaField.type.name) ? schemaField.path : undefined
                };
            }
            case 'single-date': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    format: 'yyyy-MM-dd',
                    returnFormat: 'yyyy-MM-dd',
                    isDynamicDate: false
                };
            }
            case 'date-time': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    enableCustomFormat: true,
                    format: 'yyyy-MM-dd HH:mm:ss',
                    returnFormat: 'yyyy-MM-dd HH:mm:ss',
                    // startFieldCode: [FormSchemaEntityFieldTypeName.Date, FormSchemaEntityFieldTypeName.DateTime].includes(schemaField.type.name) ? schemaField.path : undefined,
                    // endFieldCode: [FormSchemaEntityFieldTypeName.Date, FormSchemaEntityFieldTypeName.DateTime].includes(schemaField.type.name) ? schemaField.path : undefined
                };
            }
            case 'single-date-time': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    enableCustomFormat: true,
                    format: 'yyyy-MM-dd HH:mm:ss',
                    returnFormat: 'yyyy-MM-dd HH:mm:ss'
                };
            }
            case 'single-month': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    format: 'yyyy-MM',
                    returnFormat: 'yyyy-MM'
                };
            }
            case 'month': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    format: 'yyyy-MM',
                    returnFormat: 'yyyy-MM'
                };
            }
            case 'single-year': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false,
                    format: 'yyyy',
                    returnFormat: 'yyyy'
                };
            }
            case 'radio': {
                return {
                    id: schemaField.id,
                    controltype: 'radio',
                    require: false,
                    valueType: '1',
                    enumValues: schemaField.type.enumValues,
                    className: '',
                    horizontal: true,
                    showLabel: false
                };
            }
            case 'input-group': {
                return {
                    id: schemaField.id,
                    controltype: 'input-group',
                    require: false,
                    className: '',
                    editable: true,
                    groupText: '',
                    usageMode: 'text',
                    modalConfig: {
                        modalCmp: null,
                        mapFields: null,
                        showHeader: true,
                        title: '',
                        showCloseButton: true,
                        showMaxButton: true,
                        width: 800,
                        height: 600,
                        showFooterButtons: true,
                        footerButtons: []
                    }
                };
            }
            default: {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    require: false
                };
            }
        }
    }

    /**
     * 组装查询方案字段信息
     * @param schemaField 字段
     */
    static getSolutionField(schemaField: any) {
        if (schemaField.$type !== 'SimpleField') {
            // console.warn('字段类型错误！');
            return;
        }
        let control = {} as any;
        switch (schemaField.type.name) {
            case FormSchemaEntityFieldTypeName.String: {
                control = QuerySchemaFieldCreator.getControlDataByType('text', schemaField);
                break;
            }
            case FormSchemaEntityFieldTypeName.Number: case GSPElementDataType.Integer: case GSPElementDataType.Decimal:
            case FormSchemaEntityFieldTypeName.BigNumber: {
                control = QuerySchemaFieldCreator.getControlDataByType('number', schemaField);
                break;
            }
            case FormSchemaEntityFieldTypeName.Date: case FormSchemaEntityFieldTypeName.DateTime: {
                control = QuerySchemaFieldCreator.getControlDataByType('date', schemaField);
                break;
            }
            case FormSchemaEntityFieldTypeName.Enum: case FormSchemaEntityFieldTypeName.Boolean: {
                control = QuerySchemaFieldCreator.getControlDataByType('dropdown', schemaField);
                break;
            }
            default: {
                control = null;
            }
        }

        const fieldConfig: any = {
            id: schemaField.id,
            labelCode: schemaField.path,
            code: schemaField.code,
            name: schemaField.name,
            placeHolder: '',
            control
        };

        if (this.rangePlaceHolderControl.includes(fieldConfig.control.controltype)) {
            fieldConfig.beginPlaceHolder = '';
            fieldConfig.endPlaceHolder = '';
        } else {
            fieldConfig.placeHolder = '';
        }

        return fieldConfig;
    }

}

