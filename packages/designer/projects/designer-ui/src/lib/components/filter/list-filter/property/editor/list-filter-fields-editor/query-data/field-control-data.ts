import { FormSchemaEntityField, FormSchemaEntityFieldTypeName } from '@farris/designer-services';
import { GSPElementDataType } from '@gsp-bef/gsp-cm-metadata';

export class ListFilterFieldCreator {

    /** 占位符显示区间的控件 */
    static rangePlaceHolderControl = ['date', 'datetime', 'number', 'yearRange', 'monthRange'];

    /**
     * 根据指定的控件类型和schema信息生成筛选条control节点
     * @param controlType 控件类型
     * @param schemaField 字段schema信息
     */
    static getControlDataByType(controlType: string, schemaField: FormSchemaEntityField) {
        switch (controlType) {
            case 'dropdown': case 'radio': {
                const o = {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    valueType: '1',
                    enumValues: []
                };
                // 因为筛选条与列表在同一个视图模型下，一旦列表中为字段配置了特殊的编辑器类型，这里的editor.$type就会发生变化，所以只能用字段类型进行判断
                if (schemaField.type.name === FormSchemaEntityFieldTypeName.Enum) {
                    o.valueType = '1';
                    o.enumValues = schemaField.type.enumValues;
                } else if (schemaField.type.name === FormSchemaEntityFieldTypeName.Boolean) {
                    o.valueType = '2';
                    o.enumValues = [{ value: true, name: 'true' }, { value: false, name: 'false' }];
                } else if ([FormSchemaEntityFieldTypeName.String, FormSchemaEntityFieldTypeName.Number, FormSchemaEntityFieldTypeName.BigNumber].includes(schemaField.type.name)) {
                    o.valueType = '1';
                    o.enumValues = [];
                }
                return o;
            }
            case 'help': {
                return {
                    id: schemaField.id,
                    controltype: 'help',
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    helpId: '',
                    uri: '',
                    idField: '',
                    valueField: '',
                    textField: '',
                    displayType: '',
                    enableFullTree: false,
                    loadTreeDataType: 'default',
                    multiSelect: false,
                    enableCascade: false,
                    expandLevel: -1
                };
            }
            case 'checkboxgroup': {
                const o = {
                    id: schemaField.id,
                    controltype: 'checkboxgroup',
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    enumValues: []
                };
                if (schemaField.type.name === FormSchemaEntityFieldTypeName.Enum) {
                    o.enumValues = schemaField.type.enumValues;

                } else if (schemaField.type.name === FormSchemaEntityFieldTypeName.Boolean) {
                    o.enumValues = [{ value: true, name: 'true' }, { value: false, name: 'false' }];
                }
                return o;
            }
            case 'date': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    format: 'yyyy-MM-dd'
                };
            }
            case 'datetime': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    format: 'yyyy-MM-dd'
                };
            }
            case 'singleDate': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    format: 'yyyy-MM-dd',
                    compareType: 0
                };
            }
            case 'number': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    precision: schemaField.type.precision || 0,
                    bigNumber: schemaField.type.name === FormSchemaEntityFieldTypeName.BigNumber
                };
            }
            case 'year': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    format: 'yyyy',
                    compareType: 0
                };
            }
            case 'yearRange': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    format: 'yyyy'
                };
            }
            case 'month': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    format: 'yyyy-MM',
                    compareType: 0
                };
            }
            case 'monthRange': {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    format: 'yyyy-MM'
                };
            }
            case 'input-group': {
                return {
                    id: schemaField.id,
                    controltype: 'input-group',
                    isExtend: false,
                    required: false,
                    showLabel: true,
                    editable: true,
                    groupText: '',
                    usageMode: 'text',
                    modalConfig: {
                        modalCmp: null,
                        mapFields: null,
                        showHeader: true,
                        title: '',
                        showCloseButton: true,
                        showMaxButton: true,
                        width: 800,
                        height: 600,
                        showFooterButtons: true,
                        footerButtons: []
                    }
                };
            }
            case 'bool-check': {
                return {
                    id: schemaField.id,
                    controltype: 'bool-check',
                    isExtend: false,
                    required: false,
                    showLabel: true
                };
            }
            case 'flexibleNumber': {
                return {
                    id: schemaField.id,
                    controltype: 'flexibleNumber',
                    isExtend: false,
                    required: false,
                    precision: schemaField.type.precision || 0,
                    bigNumber: schemaField.type.name === FormSchemaEntityFieldTypeName.BigNumber,
                    single: true
                };
            }

            case 'flexibleDate': {
                return {
                    id: schemaField.id,
                    controltype: 'flexibleDate',
                    isExtend: false,
                    required: false,
                    showTime: false,
                    showType: 1,
                    format: 'yyyy-MM-dd',
                    single: true
                };
            }
            default: {
                return {
                    id: schemaField.id,
                    controltype: controlType,
                    isExtend: false,
                    required: false,
                    showLabel: true,
                };
            }
        }
    }

    /**
     * 组装筛选条字段信息
     * @param schemaField 字段
     */
    static getFilterField(schemaField: any) {
        if (schemaField.$type !== 'SimpleField') {
            // console.warn('字段类型错误！');
            return;
        }

        const fieldConfig: any = {
            id: schemaField.id,
            labelCode: schemaField.path,
            code: schemaField.code,
            name: schemaField.name,
            control: {}
        };

        switch (schemaField.type.name) {
            case FormSchemaEntityFieldTypeName.String: {
                fieldConfig.control = ListFilterFieldCreator.getControlDataByType('text', schemaField);
                break;
            }
            case FormSchemaEntityFieldTypeName.Number: case GSPElementDataType.Integer: case GSPElementDataType.Decimal:
            case FormSchemaEntityFieldTypeName.BigNumber: {
                fieldConfig.control = ListFilterFieldCreator.getControlDataByType('number', schemaField);
                break;
            }
            case FormSchemaEntityFieldTypeName.Date: case FormSchemaEntityFieldTypeName.DateTime: {
                fieldConfig.control = ListFilterFieldCreator.getControlDataByType('date', schemaField);
                break;
            }
            case FormSchemaEntityFieldTypeName.Enum: case FormSchemaEntityFieldTypeName.Boolean: {
                fieldConfig.control = ListFilterFieldCreator.getControlDataByType('dropdown', schemaField);
                break;
            }
            default: {
                fieldConfig.control = null;
            }
        }
        if (this.rangePlaceHolderControl.includes(fieldConfig.control.controltype)) {
            fieldConfig.beginPlaceHolder = '';
            fieldConfig.endPlaceHolder = '';
        } else {
            fieldConfig.placeHolder = '';
        }

        return fieldConfig;
    }

}

