export const contextMenu = [
  {
    title: '切换为筛选方案',
    id: 'changeToQueryScheme'
  },
  {
    title: '移除筛选条',
    id: 'removeListFilter'
  }
];
