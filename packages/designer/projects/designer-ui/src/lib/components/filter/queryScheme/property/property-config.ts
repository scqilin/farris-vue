import { BeforeOpenModalResult, ElementPropertyConfig } from '@farris/ide-property-panel';
import { DgControl } from '../../../../utils/dg-control';
import { FilterCommonProp } from '../../common/property/filter-common-property-config';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { QuerySchemeFieldsEditorComponent } from './editor/query-scheme-fields-editor/query-scheme-fields-editor.component';
import { QuerySchemeFieldConverter } from './editor/query-scheme-fields-editor/query-scheme-field-converter';
import { QuerySchemePresetFieldsEditorComponent } from './editor/query-scheme-preset-fields-editor/query-scheme-preset-fields-editor.component';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class QuerySchemeProp extends FilterCommonProp {
    propertyConfig: ElementPropertyConfig[];


    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 常规属性
        const usualPropConfig = this.getBasicPropConfig(propertyData);
        this.propertyConfig.push(usualPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }

    private getBasicPropConfig(propertyData: any): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '组件的id',
                    readonly: true
                },
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件的类型',
                    iterator: [{ key: propertyData.type, value: DgControl[propertyData.type].name }],
                    readonly: true
                }
            ]
        };
    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: [
                {
                    propertyID: 'isControlInline',
                    propertyName: '控件标签独占一列',
                    propertyType: 'boolean',
                    description: '控件标签是否独占一列',
                    defaultValue: true
                },
                {
                    propertyID: 'showCompleteLabel',
                    propertyName: '控件标签完全显示',
                    propertyType: 'unity',
                    description: '控件标签是否完全显示',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        },
                    },
                },
                {
                    propertyID: 'expanded',
                    propertyName: '展开',
                    propertyType: 'unity',
                    description: '是否展开',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        }
                    },
                },
            ]
        };
    }


    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const inputConfig = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                {
                    propertyID: 'presetQuerySolutionName',
                    propertyName: '系统预置查询方案名称',
                    propertyType: 'string',
                    description: '系统预置查询方案名称设置'
                },
                {
                    propertyID: 'fieldConfigs',
                    propertyName: '筛选方案字段',
                    propertyType: 'modal',
                    description: '筛选方案字段设置',
                    editor: QuerySchemeFieldsEditorComponent,
                    editorParams: { viewModelId, presetFieldConfigs: propertyData.presetFieldConfigs },
                    converter: new QuerySchemeFieldConverter(),
                    beforeOpenModal(): BeforeOpenModalResult {
                        // 取预置方案字段的最新值
                        this.editorParams.presetFieldConfigs = propertyData.presetFieldConfigs || [];
                        return { result: true, message: '' };
                    }
                },
                {
                    propertyID: 'presetFieldConfigs',
                    propertyName: '系统预置字段',
                    propertyType: 'modal',
                    description: '系统预置字段设置',
                    editor: QuerySchemePresetFieldsEditorComponent,
                    editorParams: { fieldConfigs: propertyData.fieldConfigs },
                    converter: new QuerySchemeFieldConverter(),
                    beforeOpenModal(): BeforeOpenModalResult {
                        // 取预置方案字段的最新值
                        this.editorParams.fieldConfigs = propertyData.fieldConfigs || [];
                        return { result: true, message: '' };
                    }
                },
                {
                    propertyID: 'binding',
                    propertyName: '默认值绑定',
                    propertyType: 'unity',
                    description: '默认值绑定设置',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    }
                },
                {
                    propertyID: 'enableInitQuery',
                    propertyName: '自动查询',
                    propertyType: 'boolean',
                    description: '是否自动查询',
                    defaultValue: false
                },
                {
                    propertyID: 'enableHistory',
                    propertyName: '支持历史记录',
                    propertyType: 'boolean',
                    description: '是否支持历史记录',
                    defaultValue: false
                },
                {
                    propertyID: 'resetFieldConfigs',
                    propertyName: '动态字段',
                    propertyType: 'unity',
                    description: '动态字段设置',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    },
                }
            ],
            setPropertyRelates(changeObject, data, parameters) {
                if (!changeObject) {
                    return;
                }
                if (changeObject.propertyID === 'fieldConfigs') {
                    propertyData.presetFieldConfigs = parameters || [];

                    changeObject.relateChangeProps = [{
                        propertyID: 'presetFieldConfigs',
                        propertyValue: parameters
                    }];
                }
            }
        };

        return inputConfig;
    }


    private getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'onQuery',
                name: '查询事件'
            },
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService,domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
            }
        };
    }
}

