import { TypeConverter } from '@farris/ide-property-panel';

export class ListFilterFieldsConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (data) {
            return '共 ' + data.length + ' 项';
        }
    }
}
