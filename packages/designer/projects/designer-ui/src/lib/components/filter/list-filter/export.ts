import FdListFilterComponent from './component/fd-list-filter';
import { ListFilterSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdListFilterTemplates from './templates';
import FdListFilterItemTemplates from './component/filter-item/templates';


export const ListFilter: ComponentExportEntity = {
    type: 'ListFilter',
    component: FdListFilterComponent,
    template: FdListFilterTemplates,
    metadata: ListFilterSchema
};

export const ListFilterItem: ComponentExportEntity = {
    type: 'ListFilterItem',
    template: FdListFilterItemTemplates,
    metadata: {}
};
