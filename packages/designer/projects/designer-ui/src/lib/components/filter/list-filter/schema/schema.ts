export const ListFilterSchema = {
    id: 'list-filter',
    type: 'ListFilter',
    appearance: null,
    controlSource: 'light',
    filterList: [],
    liveSearch: false,
    alLeft: true,
    autoWidth: false,
    filterClass: '',
    autoLabel: true,
    query: '',
    binding: null,
    disable: false,
    showReminder: false,
    showExtendInSidebar: true,
    visible: true,
    searchChange: null
};
