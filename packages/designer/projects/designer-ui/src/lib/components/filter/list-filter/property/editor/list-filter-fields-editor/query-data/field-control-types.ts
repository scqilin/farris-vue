import { KeyMap } from '@farris/ide-property-panel';
import { FormSchemaEntityFieldTypeName } from '@farris/designer-services';
import { GSPElementDataType } from '@gsp-bef/gsp-cm-metadata';

export class ListFilterFieldTypeEnums {


    static getControlTypeEnum(fieldType: string, controlSource: string): KeyMap[] {
        const controlTypes = [];
        switch (fieldType) {
            case FormSchemaEntityFieldTypeName.String: {
                controlTypes.push({ key: 'text', value: '文本' });
                controlTypes.push({ key: 'search', value: '搜索' });
                controlTypes.push({ key: 'help', value: '帮助' });
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });
                controlTypes.push({ key: 'checkboxgroup', value: '复选组' });
                controlTypes.push({ key: 'number', value: '数值区间' });
                controlTypes.push({ key: 'radio', value: '单选组' });
                controlTypes.push({ key: 'year', value: '年度' });
                controlTypes.push({ key: 'yearRange', value: '年度范围' });
                controlTypes.push({ key: 'month', value: '年月' });
                controlTypes.push({ key: 'monthRange', value: '年月范围' });
                controlTypes.push({ key: 'input-group', value: '智能输入框' });
                break;
            }
            case FormSchemaEntityFieldTypeName.Number: case GSPElementDataType.Integer: case GSPElementDataType.Decimal: {
                controlTypes.push({ key: 'number', value: '数值区间' });
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });

                // if (controlSource === 'light') {
                //     controlTypes.push({ key: 'flexibleNumber', value: '可变数值' });
                // }
                break;
            }
            case FormSchemaEntityFieldTypeName.BigNumber: {
                controlTypes.push({ key: 'number', value: '数值区间' });

                // if (controlSource === 'light') {
                //     controlTypes.push({ key: 'flexibleNumber', value: '可变数值' });
                // }
                break;
            }
            case FormSchemaEntityFieldTypeName.Date: case FormSchemaEntityFieldTypeName.DateTime: {
                controlTypes.push({ key: 'date', value: '日期区间' });
                controlTypes.push({ key: 'singleDate', value: '日期' });
                controlTypes.push({ key: 'datetime', value: '日期时间区间' });

                // if (controlSource === 'light') {
                //     controlTypes.push({ key: 'flexibleDate', value: '可变日期' });
                // }

                break;
            }
            case FormSchemaEntityFieldTypeName.Enum: {
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });
                controlTypes.push({ key: 'checkboxgroup', value: '复选组' });
                controlTypes.push({ key: 'radio', value: '单选组' });
                break;
            }
            case FormSchemaEntityFieldTypeName.Boolean: {
                controlTypes.push({ key: 'dropdown', value: '下拉选择' });
                controlTypes.push({ key: 'checkboxgroup', value: '复选组' });
                controlTypes.push({ key: 'radio', value: '单选组' });

                // if (controlSource === 'light') {
                //     controlTypes.push({ key: 'bool-check', value: '单选框' });
                // }
                break;
            }
        }
        return controlTypes;
    }
}
