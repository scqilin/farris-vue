// tslint:disable:max-line-length
import { ElementPropertyConfig, BeforeOpenModalResult, KeyMap } from '@farris/ide-property-panel';
import { EditorTypes } from '@farris/ui-datagrid-editors';
import { DgControl } from '../../../../../../utils/dg-control';
import { ItemCollectionEditorComponent, ItemCollectionConverter } from '@farris/designer-devkit';
import { SelectMappingComponent, MappingConverter, CodeEditorComponent, ImportCmpComponent, ImportCmpConverter } from '@farris/designer-devkit';
// import { SelectHelpMetadataComponent } from '../../../../../input/lookup-edit/property/editors/select-help-metadata/select-help-metadata.component';
// import { SelectHelpConverter } from '../../../../../input/lookup-edit/property/editors/select-help-metadata/select-help-converter';
// import { SelectHelpTextFieldComponent } from '../../../../../input/lookup-edit/property/editors/select-help-text-field/select-help-text-field.component';
import { EventEditorService, DomService, FormSchemaEntityFieldTypeName, WebCmdService, FormBasicService } from '@farris/designer-services';
import { ListFilterFieldCreator } from './query-data/field-control-data';
import { ListFilterFieldTypeEnums } from './query-data/field-control-types';
import { Injectable } from '@angular/core';
import { InputgroupFormMappingEditorComponent } from '../../../../queryScheme/property/editor/query-scheme-fields-editor/inputgroup-form-mapping-editor/inputgroup-form-mapping-editor';
import { SelectHelpClearFieldsEditorComponent } from '../../../../queryScheme/property/editor/query-scheme-fields-editor/select-help-clear-fields/select-help-clear-fields.component';
import { EventsEditorFuncUtils } from '../../../../../../utils/events-editor-func';
import { FormPropertyChangeObject } from '../../../../../../entity/property-change-entity';
import { MessagerService } from '@farris/ui-messager';
import { Subject } from 'rxjs';
/**
 * 筛选条相关特性
 */
@Injectable()
export class ListFilterService {

    /** 属性配置 */
    propertyConfig: ElementPropertyConfig[];

    /** 触发关闭编辑器，用于在交互面板中跳转到代码视图之前将编辑器弹窗关闭 */
    triggerSaveAndCloseEditor = new Subject<any>();

    constructor(
        private domService: DomService,
        private webCmdService: WebCmdService,
        private eventEditorService: EventEditorService,
        private formBasicService: FormBasicService,
        private msgService: MessagerService) { }

    /**
     * 筛选条支持的字段类型
     */
    allowedFieldType = [
        FormSchemaEntityFieldTypeName.String,
        FormSchemaEntityFieldTypeName.Number,
        FormSchemaEntityFieldTypeName.BigNumber,
        FormSchemaEntityFieldTypeName.Date,
        FormSchemaEntityFieldTypeName.DateTime,
        FormSchemaEntityFieldTypeName.Boolean,
        FormSchemaEntityFieldTypeName.Enum
    ];

    getPropertyConfig(schemaField: any, fieldObject: any, viewModelId: string, controlSource: string, fieldConfigs: any[]): ElementPropertyConfig[] {
        if (!fieldObject) {
            return [];
        }
        const controlTypes = ListFilterFieldTypeEnums.getControlTypeEnum(schemaField.type.name, controlSource);
        const self = this;
        this.propertyConfig = [];
        let openModalPropConfigs;
        if (schemaField.type.name === FormSchemaEntityFieldTypeName.String) {
            // 智能输入框----弹出表单框属性
            openModalPropConfigs = this.getOpenModalPropConfigs(fieldObject, viewModelId);
        }
        const usualConfig: ElementPropertyConfig = {
            categoryId: 'usual',
            categoryName: '常规',
            hideTitle: true,
            properties: [{
                propertyID: 'id',
                propertyName: '标识',
                propertyType: 'string',
                readonly: true
            }, {
                propertyID: 'code',
                propertyName: '编号',
                propertyType: 'string',
                readonly: true
            }, {
                propertyID: 'labelCode',
                propertyName: '标签',
                propertyType: 'string',
                readonly: true
            }, {
                propertyID: 'name',
                propertyName: '控件名称',
                propertyType: 'string'
            },
            {
                propertyID: 'beginPlaceHolder',
                propertyName: '开始占位符文本',
                propertyType: 'string',
                visible: ListFilterFieldCreator.rangePlaceHolderControl.includes(fieldObject.control.controltype)
            }, {
                propertyID: 'endPlaceHolder',
                propertyName: '结束占位符文本',
                propertyType: 'string',
                visible: ListFilterFieldCreator.rangePlaceHolderControl.includes(fieldObject.control.controltype)
            }, {
                propertyID: 'placeHolder',
                propertyName: '占位符文本',
                propertyType: 'string',
                visible: !ListFilterFieldCreator.rangePlaceHolderControl.includes(fieldObject.control.controltype)
            },
            {
                propertyID: 'control',
                propertyName: '控件',
                propertyType: 'cascade',
                isExpand: true,
                hideCascadeTitle: true,
                cascadeConfig: [
                    {
                        propertyID: 'controltype',
                        propertyName: '控件类型',
                        propertyType: 'select',
                        iterator: controlTypes
                    },
                    {
                        propertyID: 'isExtend',
                        propertyName: '是否默认收起到右侧',
                        propertyType: 'boolean',
                        defaultValue: false
                    }, {
                        propertyID: 'required',
                        propertyName: '是否必填',
                        propertyType: 'boolean',
                        defaultValue: false
                    }, {
                        propertyID: 'showLabel',
                        propertyName: '是否显示标签',
                        propertyType: 'boolean',
                        defaultValue: true,
                        visible: controlSource === 'normal'
                    },
                    // {
                    //     propertyID: 'helpId',
                    //     propertyName: '帮助元数据',
                    //     propertyType: 'modal',
                    //     visible: fieldObject.control.controltype === 'help',
                    //     category: 'help',
                    //     editor: SelectHelpMetadataComponent,
                    //     editorParams: {
                    //         viewModelId,
                    //         bindingField: schemaField.bindingField,
                    //         bindingType: 'Form',
                    //         relativePath: this.formBasicService.formMetaBasicInfo.relativePath,
                    //         boId: this.formBasicService.formMetaBasicInfo.bizobjectID,
                    //     },
                    //     converter: new SelectHelpConverter(),
                    //     showClearButton: true
                    // },
                    // {
                    //     propertyID: 'uri',
                    //     propertyName: 'uri',
                    //     propertyType: 'string',
                    //     visible: fieldObject.control.controltype === 'help',
                    //     category: 'help',
                    //     readonly: true
                    // },
                    // {
                    //     propertyID: 'textField',
                    //     propertyName: '文本字段',
                    //     propertyType: 'modal',
                    //     visible: fieldObject.control.controltype === 'help',
                    //     category: 'help',
                    //     editor: SelectHelpTextFieldComponent,
                    //     editorParams: {
                    //         helpId: fieldObject.control.helpId
                    //     },
                    //     beforeOpenModal(): BeforeOpenModalResult {
                    //         if (!fieldObject.control.helpId) {
                    //             return { result: false, message: '请先选择帮助元数据！' };
                    //         }
                    //         // 取帮助最新值
                    //         this.editorParams.helpId = fieldObject.control.helpId;
                    //         return { result: true, message: '' };

                    //     }
                    // },
                    // {
                    //     propertyID: 'valueField',
                    //     propertyName: '值字段',
                    //     propertyType: 'modal',
                    //     visible: fieldObject.control.controltype === 'help',
                    //     category: 'help',
                    //     editor: SelectHelpTextFieldComponent,
                    //     editorParams: {
                    //         helpId: fieldObject.control.helpId
                    //     },
                    //     beforeOpenModal(): BeforeOpenModalResult {
                    //         if (!fieldObject.control.helpId) {
                    //             return { result: false, message: '请先选择帮助元数据！' };
                    //         }
                    //         // 取帮助最新值
                    //         this.editorParams.helpId = fieldObject.control.helpId;
                    //         return { result: true, message: '' };

                    //     }
                    // },
                    {
                        propertyID: 'idField',
                        propertyName: '标识字段',
                        propertyType: 'string',
                        visible: fieldObject.control.controltype === 'help',
                        category: 'help',
                        readonly: true
                    }, {
                        propertyID: 'displayType',
                        propertyName: '显示类型',
                        propertyType: 'string',
                        visible: fieldObject.control.controltype === 'help',
                        category: 'help',
                        readonly: true
                    }, {
                        propertyID: 'mapFields',
                        propertyName: '映射',
                        propertyType: 'modal',
                        editor: SelectMappingComponent,
                        editorParams: {
                            viewModelId,
                            sourceMetadataId: fieldObject.control.helpId,
                            binding: {
                                type: 'Form',
                                field: fieldObject.id
                            }
                        },
                        converter: new MappingConverter(),
                        visible: fieldObject.control.controltype === 'help',
                        category: 'help'
                    },
                    {
                        propertyID: 'enableFullTree',
                        propertyName: '构造完整树',
                        propertyType: 'boolean',
                        defaultValue: false,
                        category: 'help',
                        visible: fieldObject.control.controltype === 'help' && fieldObject.control.displayType &&
                            (fieldObject.control.displayType.toLowerCase() === 'treelist' ||
                                fieldObject.control.displayType.toLowerCase() === 'navtreelist')
                    },
                    {
                        propertyID: 'loadTreeDataType',
                        propertyName: '数据加载方式',
                        propertyType: 'select',
                        defaultValue: 'default',
                        iterator:
                            [{ key: 'default', value: '默认' }, { key: 'loadall', value: '全部加载' }, { key: 'layerload', value: '分层加载' }],
                        visible: fieldObject.control.controltype === 'help' && fieldObject.control.displayType &&
                            fieldObject.control.displayType.toLowerCase() === 'treelist',
                        category: 'help'
                    },
                    {
                        propertyID: 'multiSelect',
                        propertyName: '是否多选',
                        propertyType: 'boolean',
                        defaultValue: false,
                        visible: fieldObject.control.controltype === 'help',
                    },
                    {
                        propertyID: 'enableCascade',
                        propertyName: '启用级联选择',
                        propertyType: 'boolean',
                        defaultValue: false,
                        category: 'help',
                        visible: fieldObject.control.controltype === 'help' && fieldObject.control.multiSelect
                            && fieldObject.control.displayType &&
                            (fieldObject.control.displayType.toLowerCase() === 'treelist' ||
                                fieldObject.control.displayType.toLowerCase() === 'navtreelist')
                    },
                    {
                        propertyID: 'expandLevel',
                        propertyName: '默认展开层级',
                        propertyType: 'number',
                        decimals: 0,
                        min: -1,
                        defaultValue: -1,
                        category: 'help',
                        visible: fieldObject.control.controltype === 'help' && fieldObject.control.displayType &&
                            (fieldObject.control.displayType.toLowerCase() === 'treelist' ||
                                fieldObject.control.displayType.toLowerCase() === 'navtreelist')
                    },
                    {
                        propertyID: 'clearFields',
                        propertyName: '值变化时清空字段',
                        propertyType: 'modal',
                        category: 'help',
                        visible: fieldObject.control.controltype === 'help' && controlSource === 'normal',
                        editor: SelectHelpClearFieldsEditorComponent,
                        editorParams: {
                            fieldConfigs,
                            currentId: fieldObject.id
                        },
                        showClearButton: true
                    },
                    {
                        propertyID: 'valueType',
                        propertyName: '值类型',
                        propertyType: 'select',
                        visible: fieldObject.control.controltype === 'dropdown',
                        category: 'dropdown',
                        iterator: [{ key: '1', value: '字符串' }, { key: '0', value: '整型' }, { key: '2', value: '布尔' }]
                    },
                    {
                        propertyID: 'enumValues',
                        propertyName: '枚举数组',
                        propertyType: 'modal',
                        visible: fieldObject.control.controltype === 'dropdown' || fieldObject.control.controltype === 'checkboxgroup'
                            || fieldObject.control.controltype === 'radio',
                        category: 'dropdown/checkboxgroup/radio',
                        editor: ItemCollectionEditorComponent,
                        converter: new ItemCollectionConverter(),
                        editorParams: {
                            columns: [
                                {
                                    field: 'value', title: '枚举值',
                                    editor: {
                                        type: schemaField.type.name === 'Boolean' ? EditorTypes.CHECKBOX : EditorTypes.TEXTBOX
                                    }
                                },
                                { field: 'name', title: '枚举名称', editor: { type: EditorTypes.TEXTBOX } },
                            ],
                            requiredFields: ['value', 'name'],
                            uniqueFields: ['value', 'name'],
                            modalTitle: '枚举编辑器'
                        }
                    },
                    {
                        propertyID: 'showTime',
                        propertyName: '是否显示时间',
                        propertyType: 'boolean',
                        visible: fieldObject.control.controltype === 'flexibleDate',
                        category: 'flexibleDate'
                    },
                    {
                        propertyID: 'showType',
                        propertyName: '显示类型',
                        propertyType: 'select',
                        iterator: [{ key: 1, value: '年月日' }, { key: 2, value: '年月' }, { key: 3, value: '年' }],
                        visible: fieldObject.control.controltype === 'flexibleDate' && !fieldObject.control.showTime,
                        category: 'flexibleDate'
                    },
                    {
                        propertyID: 'format',
                        propertyName: '格式',
                        propertyType: 'select',
                        iterator: this.getFormatIterators(fieldObject),
                        visible: 'date/singleDate/datetime/year/month/yearRange/monthRange/flexibleDate'.includes(
                            fieldObject.control.controltype),
                        category: 'date/singleDate/datetime/year/month/yearRange/monthRange/flexibleDate'
                    }, {
                        propertyID: 'compareType',
                        propertyName: '搜索的日期范围',
                        propertyType: 'select',
                        iterator: [
                            { key: 0, value: '等于' },
                            { key: 2, value: '大于' },
                            { key: 3, value: '大于等于' },
                            { key: 4, value: '小于' },
                            { key: 5, value: '小于等于' }
                        ],
                        visible: fieldObject.control.controltype === 'singleDate' || fieldObject.control.controltype === 'year'
                            || fieldObject.control.controltype === 'month',
                        category: 'singleDate/year/month'
                    }, {
                        propertyID: 'precision',
                        propertyName: '精度',
                        propertyType: 'number',
                        decimals: 0,
                        min: 0,
                        visible: fieldObject.control.controltype === 'number' || fieldObject.control.controltype === 'flexibleNumber',
                        category: 'number/flexibleNumber'
                    },
                    {
                        propertyID: 'single',
                        propertyName: '默认启用单值',
                        propertyType: 'boolean',
                        visible: fieldObject.control.controltype === 'flexibleNumber' ||
                            fieldObject.control.controltype === 'flexibleDate',
                        category: 'flexibleNumber/flexibleDate'
                    },
                    {
                        propertyID: 'editable',
                        propertyName: '是否允许输入',
                        propertyType: 'boolean',
                        category: 'input-group',
                        visible: fieldObject.control.controltype === 'input-group'
                    },
                    {
                        propertyID: 'groupText',
                        propertyName: '按钮显示文本',
                        propertyType: 'modal',
                        editor: CodeEditorComponent,
                        editorParams: {
                            language: 'html'
                        },
                        category: 'input-group',
                        visible: fieldObject.control.controltype === 'input-group'
                    },
                    {
                        propertyID: 'usageMode',
                        propertyName: '使用模式',
                        propertyType: 'select',
                        iterator: [
                            { key: 'text', value: '文本' },
                            { key: 'open-modal', value: '弹出表单' },
                            { key: 'open-remote-modal', value: '弹出远端表单' }],
                        refreshPanelAfterChanged: true,
                        category: 'input-group',
                        visible: fieldObject.control.controltype === 'input-group'
                    }
                ]
            }],
            setPropertyRelates(changeObject: any, propertyData: any, parameters: any) {
                switch (changeObject.propertyID) {
                    case 'controltype': {
                        const controlProp = this.properties.find(p => p.propertyID === 'control');
                        controlProp.cascadeConfig.map(p => {
                            if (p.category) {
                                p.visible = false;
                                if (p.category.includes(changeObject.propertyValue)) {
                                    p.visible = true;
                                }
                            }
                            if (p.propertyID === 'loadTreeDataType' || p.propertyID === 'enableFullTree'
                                || p.propertyID === 'enableCascade' || p.propertyID === 'expandLevel') {
                                p.visible = false;
                            }
                            if (p.propertyID === 'multiSelect') {
                                p.visible = changeObject.propertyValue === 'help';
                            }
                            if (p.propertyID === 'showType') {
                                p.visible = changeObject.propertyValue === 'flexibleDate' && !propertyData.control.showTime;
                            }
                            if (p.propertyID === 'clearFields') {
                                p.visible = changeObject.propertyValue === 'help' && controlSource === 'normal';
                            }

                        });

                        // 切换类型：重新设置control节点，以便清除与当前类型无关的属性;
                        const oldControl = propertyData.control;
                        propertyData.control = ListFilterFieldCreator.getControlDataByType(
                            changeObject.propertyValue, schemaField);
                        Object.assign(propertyData.control, {
                            isExtend: oldControl.isExtend,
                            required: oldControl.required,
                            showLabel: oldControl.showLabel,
                        });

                        // 占位符属性
                        const beginPlaceHolder = this.properties.find(p => p.propertyID === 'beginPlaceHolder');
                        const endPlaceHolder = this.properties.find(p => p.propertyID === 'endPlaceHolder');
                        const placeHolder = this.properties.find(p => p.propertyID === 'placeHolder');

                        if (ListFilterFieldCreator.rangePlaceHolderControl.includes(changeObject.propertyValue)) {
                            if (beginPlaceHolder) {
                                beginPlaceHolder.visible = true;
                            }
                            if (endPlaceHolder) {
                                endPlaceHolder.visible = true;
                            }
                            if (placeHolder) {
                                placeHolder.visible = false;
                            }
                            propertyData.placeHolder = undefined;
                        } else {
                            if (beginPlaceHolder) {
                                beginPlaceHolder.visible = false;
                            }
                            if (endPlaceHolder) {
                                endPlaceHolder.visible = false;
                            }
                            if (placeHolder) {
                                placeHolder.visible = true;
                            }
                            propertyData.beginPlaceHolder = undefined;
                            propertyData.endPlaceHolder = undefined;
                        }

                        // 格式化属性
                        const format = controlProp.cascadeConfig.find(p => p.propertyID === 'format');
                        if (format) {
                            format.iterator = self.getFormatIterators(propertyData);
                        }

                        self.setOpenModalConfigDisplay(propertyData, openModalPropConfigs);

                        // 重置事件分类
                        const newEventConfig = self.getEventPropertyConfig(propertyData, viewModelId);
                        const currentEventConfig = self.propertyConfig.find(cat => cat.categoryId === 'eventsEditor');
                        Object.assign(currentEventConfig, newEventConfig);

                        break;
                    }
                    case 'helpId': {
                        // 选择帮助元数据返回的是DataSource，需要拆分出uri和idField
                        if (!changeObject.propertyValue) {
                            Object.assign(propertyData.control,
                                {
                                    helpId: '', uri: '', idField: '', valueField: '', textField: '', displayType: '',
                                    enableFullTree: false, loadTreeDataType: 'default',
                                    // enableExtendLoadMethod: false,
                                    multiSelect: false, enableCascade: false, expandLevel: -1
                                });
                        } else {
                            const { uri, idField } = changeObject.propertyValue;
                            propertyData.control.uri = uri;
                            propertyData.control.idField = idField;
                            Object.assign(propertyData.control, parameters);


                            const controlProp = this.properties.find(p => p.propertyID === 'control');

                            controlProp.cascadeConfig.map(p => {
                                // 旧表单手动添加的displayType 大小写不一定，故转换为小写再对比
                                if (p.propertyID === 'enableFullTree' || p.propertyID === 'enableCascade'
                                    || p.propertyID === 'expandLevel') {
                                    p.visible = parameters.displayType.toLowerCase() === 'treelist' ||
                                        parameters.displayType.toLowerCase() === 'navtreelist';
                                }
                                if (p.propertyID === 'loadTreeDataType') {
                                    p.visible = parameters.displayType.toLowerCase() === 'treelist';
                                }
                                // 映射属性中配置帮助id
                                if (p.propertyID === 'mapFields') {
                                    p.editorParams.sourceMetadataId = parameters ? parameters.helpId : '';
                                }
                            });
                        }
                        break;
                    }
                    case 'multiSelect': {
                        if (propertyData.control.controltype === 'help') {
                            const controlProp = this.properties.find(p => p.propertyID === 'control');
                            const enableCascade = controlProp.cascadeConfig.find(p => p.propertyID === 'enableCascade');
                            enableCascade.visible = propertyData.control.multiSelect
                                && propertyData.control.displayType
                                && (propertyData.control.displayType.toLowerCase() === 'treelist' ||
                                    propertyData.control.displayType.toLowerCase() === 'navtreelist');

                        }
                        break;
                    }
                    case 'usageMode': {
                        self.setOpenModalConfigDisplay(propertyData, openModalPropConfigs);
                        if (propertyData.control.modalConfig) {
                            propertyData.control.modalConfig.mapFields = '';
                            propertyData.control.modalConfig.modalCmp = '';
                            propertyData.control.modalConfig.component = '';
                            propertyData.control.modalConfig.formId = '';
                        }
                        break;
                    }
                    case 'showTime': {
                        const controlProp = this.properties.find(p => p.propertyID === 'control');
                        const showType = controlProp.cascadeConfig.find(p => p.propertyID === 'showType');
                        if (showType) {
                            showType.visible = propertyData.control.controltype === 'flexibleDate' && !changeObject.propertyValue;
                        }
                        // 格式化属性
                        const format = controlProp.cascadeConfig.find(p => p.propertyID === 'format');
                        if (format) {
                            format.iterator = self.getFormatIterators(propertyData);
                            propertyData.control.format = format.iterator[0].key;
                        }
                        break;
                    }

                    case 'showType': {
                        const controlProp = this.properties.find(p => p.propertyID === 'control');

                        // 格式化属性
                        const format = controlProp.cascadeConfig.find(p => p.propertyID === 'format');
                        if (format) {
                            format.iterator = self.getFormatIterators(propertyData);
                            propertyData.control.format = format.iterator[0].key;
                        }
                        break;
                    }
                }
            }
        };
        this.propertyConfig.push(usualConfig);
        if (openModalPropConfigs) {
            this.propertyConfig = this.propertyConfig.concat(openModalPropConfigs);
        }

        const eventConfig = this.getEventPropertyConfig(fieldObject, viewModelId);
        this.propertyConfig.push(eventConfig);

        return this.propertyConfig;
    }
    /**
     * 获取交互面板配置
     * @param propertyData 属性值
     */
    private getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        let eventList = [];
        const controlData = propertyData.control;
        eventList = this.switchEvents(controlData, eventList);
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, controlData, viewModelId, eventList, this.switchEvents),
            tabId: 'commands',
            tabName: '交互',
            enableCascade: true,
            propertyData: controlData,
            parentPropertyID: 'control',
            setPropertyRelates(changeObject: FormPropertyChangeObject, data: any, parameters: any) {
                delete controlData[viewModelId];
                // 若需要跳转到代码视图，首先要求用户将当前弹出窗口关闭
                if (parameters.isAddControllerMethod) {
                    self.msgService.question('确定关闭当前编辑器并跳转到代码视图吗？', () => {
                        self.triggerSaveAndCloseEditor.next();
                        // 暂存控件信息，用于自动创建新方法的方法编号和名称
                        parameters.controlInfo = { type: propertyData.code, name: propertyData.name };

                        EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, controlData, viewModelId, parameters['events'], parameters);
                        this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, controlData, viewModelId, parameters['events'], self.switchEvents);
                    });
                } else {
                    EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, controlData, viewModelId, parameters['events'], parameters);
                    this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, controlData, viewModelId, parameters['events'], self.switchEvents);
                }
            }
        };
    }
    /**
     * 处理动态事件属性
     * @param fieldObject 属性值
     * @param eventList 事件列表
     */
    private switchEvents(fieldObjectControl: any, eventList: { label: string, name: string }[]) {
        const controlType = fieldObjectControl.controltype;
        // 帮助相关事件
        if (controlType === 'help') {
            if (!eventList.find(e => e.label === 'preEventCmd')) {
                eventList.push(
                    {
                        label: 'preEventCmd',
                        name: '帮助前事件'
                    },
                    {
                        label: 'postEventCmd',
                        name: '帮助后事件'
                    }
                );
            }

        } else {
            eventList = eventList.filter(e => e.label !== 'preEventCmd' && e.label !== 'postEventCmd');
        }

        // 智能输入框相关事件
        if (controlType === 'input-group') {
            if (!eventList.find(e => e.label === 'click')) {
                eventList.push(
                    {
                        label: 'click',
                        name: '点击事件'
                    }
                );
            }
        } else {
            eventList = eventList.filter(e => e.label !== 'click');
        }

        return eventList;
    }

    private setOpenModalConfigDisplay(propertyData: any, openModalPropConfigs: ElementPropertyConfig[]) {
        if (!openModalPropConfigs || !propertyData.control) {
            return;
        }
        if (propertyData.control.controltype !== 'input-group') {
            openModalPropConfigs.forEach(config => {
                config.hide = true;
            });
            return;
        }
        openModalPropConfigs.forEach(config => {
            if (config.categoryId === 'openModal') {
                config.hide = propertyData.control.usageMode !== 'open-modal';
            }
            if (config.categoryId === 'openRemoteModal') {
                config.hide = propertyData.control.usageMode !== 'open-remote-modal';
            }
            if (config.categoryId === 'modalConfig') {
                config.hide = propertyData.control.usageMode === 'text';
            }
        });
    }

    private getFormatIterators(fieldObject: any): KeyMap[] {
        switch (fieldObject.control.controltype) {
            case 'date': case 'singleDate': case 'datetime': {
                return [
                    { key: 'yyyy-MM-dd', value: 'yyyy-MM-dd' },
                    { key: 'yyyy年MM月dd日', value: 'yyyy年MM月dd日' },
                    { key: 'yyyy/MM/dd', value: 'yyyy/MM/dd' }
                ];
            }
            case 'year': case 'yearRange': {
                return [
                    { key: 'yyyy', value: 'yyyy' },
                    { key: 'yyyy年', value: 'yyyy年' }
                ];
            }
            case 'month': case 'monthRange': {
                return [
                    { key: 'yyyy-MM', value: 'yyyy-MM' },
                    { key: 'yyyy年MM月', value: 'yyyy年MM月' },
                    { key: 'yyyy/MM', value: 'yyyy/MM' }

                ];
            }
            case 'flexibleDate': {
                if (fieldObject.control.showTime) {
                    return [
                        { key: 'yyyy-MM-dd HH:mm:ss', value: 'yyyy-MM-dd HH:mm:ss' },
                        { key: 'yyyy/MM/dd HH:mm:ss', value: 'yyyy/MM/dd HH:mm:ss' },
                        { key: 'yyyy年MM月dd日 HH时mm分ss秒', value: 'yyyy年MM月dd日 HH时mm分ss秒' }
                    ];
                } else {
                    switch (fieldObject.control.showType) {
                        case 1: {
                            return [
                                { key: 'yyyy-MM-dd', value: 'yyyy-MM-dd' },
                                { key: 'yyyy年MM月dd日', value: 'yyyy年MM月dd日' },
                                { key: 'yyyy/MM/dd', value: 'yyyy/MM/dd' }
                            ];
                        }
                        case 2: {
                            return [
                                { key: 'yyyy-MM', value: 'yyyy-MM' },
                                { key: 'yyyy年MM月', value: 'yyyy年MM月' },
                                { key: 'yyyy/MM', value: 'yyyy/MM' }
                            ];
                        }
                        case 3: {
                            return [
                                { key: 'yyyy', value: 'yyyy' },
                                { key: 'yyyy年', value: 'yyyy年' }
                            ];
                        }
                    }
                }
            }
        }
    }

    private getOpenModalPropConfigs(propertyData: any, viewModelId: string): ElementPropertyConfig[] {
        const self = this;
        const control = propertyData.control;
        if (!control.modalConfig) {
            propertyData.control.modalConfig = {
                modalCmp: null,
                mapFields: '',
                showHeader: true,
                title: '',
                showCloseButton: true,
                showMaxButton: true,
                width: 800,
                height: 600,
                showFooterButtons: true,
                footerButtons: []
            };
        }
        return [{
            categoryId: 'openModal',
            categoryName: '表单配置',
            hide: control.usageMode !== 'open-modal',
            propertyData: control.modalConfig,
            enableCascade: true,
            parentPropertyID: 'control.modalConfig',
            properties: [
                {
                    propertyID: 'modalCmp',
                    propertyName: '弹出表单',
                    propertyType: 'modal',
                    editor: ImportCmpComponent,
                    converter: new ImportCmpConverter(),
                    editorParams: { containerType: DgControl.ModalContainer.type, containerId: propertyData.id, relativePath: this.formBasicService.formMetaBasicInfo.relativePath, name: this.formBasicService.formMetaBasicInfo.name },
                    showClearButton: true,
                    afterClickClearButton(oldValue: string) {
                        self.domService.deleteExternalComponent(oldValue); // 清空外部组件的声明节点
                    }
                },
                {
                    propertyID: 'mapFields',
                    propertyName: '字段映射',
                    propertyType: 'modal',
                    editor: SelectMappingComponent,
                    editorParams: {
                        sourceMetadataId: control.modalConfig.formId,
                        viewModelId,
                        binding: {
                            type: 'Form'
                        },
                        sourceMetadataType: 'form',
                        sourceMetadataName: '弹出表单'
                    },
                    converter: new MappingConverter(),
                    beforeOpenModal(): BeforeOpenModalResult {
                        if (!control.modalConfig || !control.modalConfig.formId) {
                            return { result: false, message: '请先选择弹出表单！' };
                        }
                        // 取弹出表单和绑定的最新值
                        this.editorParams.sourceMetadataId = control.modalConfig.formId;
                        return { result: true, message: '' };
                    }
                }

            ],
            setPropertyRelates(changeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'modalCmp': {
                        propData.component = parameters ? parameters.component : '';
                        propData.formId = parameters ? parameters.uri : '';
                        propData.mapFields = '';
                        break;
                    }
                }
                Object.assign(propertyData.control.modalConfig, propData);
            }
        },
        {
            categoryId: 'openRemoteModal',
            categoryName: '表单配置',
            hide: control.usageMode !== 'open-remote-modal',
            propertyData: control.modalConfig,
            enableCascade: true,
            parentPropertyID: 'control.modalConfig',
            properties: [
                {
                    propertyID: 'mapFields',
                    propertyName: '字段映射',
                    propertyType: 'modal',
                    editor: InputgroupFormMappingEditorComponent,
                    converter: new MappingConverter(),
                }

            ],
            setPropertyRelates(changeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                Object.assign(propertyData.control.modalConfig, propData);
            }
        },
        {
            categoryId: 'modalConfig',
            categoryName: '弹出窗口配置',
            hide: control.controltype !== 'input-group' || control.usageMode === 'text',
            propertyData: control.modalConfig,
            enableCascade: true,
            parentPropertyID: 'control.modalConfig',
            properties: [
                {
                    propertyID: 'showHeader',
                    propertyName: '显示标题栏',
                    propertyType: 'boolean'
                },
                {
                    propertyID: 'title',
                    propertyName: '标题',
                    propertyType: 'string',
                    visible: control.modalConfig && control.modalConfig.showHeader,
                    category: 'header'
                },
                {
                    propertyID: 'showCloseButton',
                    propertyName: '显示关闭按钮',
                    propertyType: 'boolean',
                    visible: control.modalConfig && control.modalConfig.showHeader,
                    category: 'header'
                },
                {
                    propertyID: 'showMaxButton',
                    propertyName: '显示最大化按钮',
                    propertyType: 'boolean',
                    visible: control.modalConfig && control.modalConfig.showHeader,
                    category: 'header'
                },
                {
                    propertyID: 'width',
                    propertyName: '宽度',
                    propertyType: 'number',
                    min: 0
                },
                {
                    propertyID: 'height',
                    propertyName: '高度',
                    propertyType: 'number',
                    min: 0
                }
                // {
                //     propertyID: 'showFooterButtons',
                //     propertyName: '显示底部按钮区',
                //     propertyType: 'boolean'
                // },
                // {
                //     propertyID: 'footerButtons',
                //     propertyName: '按钮组',
                //     propertyType: 'modal',
                //     editor: ToolbarEditorComponent,
                //     editorParams: { viewModelId, disableChildItem: true, defaultBtnClass: 'btn' },
                //     converter: new ToolBarConverter(),
                //     visible: control.modalConfig && control.modalConfig.showFooterButtons
                // }

            ],
            setPropertyRelates(changeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'showHeader': {
                        this.properties.map(p => {
                            if (p.category === 'header') {
                                p.visible = changeObject.propertyValue;
                            }
                        });

                        break;
                    }
                    case 'showFooterButtons': {
                        const footerButtons = this.properties.find(p => p.propertyID === 'footerButtons');
                        if (footerButtons) {
                            footerButtons.visible = changeObject.propertyValue;
                        }
                    }
                }

                Object.assign(propertyData.control.modalConfig, propData);
            }
        }];
    }

}
