export const contextMenu = [
  {
    title: '切换为筛选条',
    id: 'changeToListFilter'
  },
  {
    title: '移除筛选方案',
    id: 'removeQueryScheme'
  }
];
