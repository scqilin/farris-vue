export default (ctx: any) => {

    const controlsInlineCls = ctx.component.isControlInline ? ' farris-form-controls-inline' : '';
    const completeCls = ctx.component.showCompleteLabel ? 'f-form-label-xl condition-with-complete' : '';
    return `

        <div class="farris-panel position-relative" style="outline: none;" tabindex="1">
            <div class="solution-header" style="height: auto!important;margin-bottom: 10px!important;display: flex;align-items: center;justify-content: space-between;">
                <div class="btn-group mr-3 dropdown" fdropdown>
                    <div class="solution-header-title" fdropdowntoggle aria-haspopup="true" style="    padding: .25rem 6px .25rem 12px;border-radius: 3px;display: flex;flex-direction: row;align-items: center;color: #2A87FF;font-size: .8125rem;vertical-align: middle;background: #fff;cursor: pointer;">
                        <span style="max-width: 288px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap" title="默认筛选方案">
                        ${ctx.component.presetQuerySolutionName}
                        </span>
                        <span class="f-icon f-accordion-expand" style="color: #2A87FF;">
                        </span>
                    </div>
                </div>
                <div class="solution-action" style="display: flex;align-items: center;flex-shrink: 0;">
                    <div class="btn-group" style="position: relative;display: inline-flex;">
                        <button class="btn btn-primary " type="button">筛选</button>
                    </div>
                    <div class="icon-group" style="display: flex!important;align-items: center;">
                        <span class="icon-group-remove" title="清空" style="margin: 0 3px;">
                            <span class="f-icon f-icon-remove" style="margin: 0 3px;color: #2D2F33;background: #fff;border: 1px solid #EAECF3;border-radius: 3px;width: calc(1.5rem + .125rem);height: calc(1.5rem + .125rem);text-align: center;line-height: 20px;display: block;box-shadow: 0 2px 6px 0 rgb(31 35 41 / 6%);">
                            </span>
                        </span>
                        <span class="divide" style="color: #E4E7EF;margin: 0 3px;position: relative;font-size:12px;padding: 3px 0;">
                        </span>
                        <span class="icon-group-setup" title="配置" style="margin-right:0;margin-right:3px;margin-left:7px;height: 100%;width: 100%;color: #6c6c6c;background-color: #fff">
                            <span class="f-icon f-icon-home-setup"></span>
                        </span>
                        <span class="icon-group-packup" title="收起" style="margin-right:3px;margin-left: 7px;height: 100%;width: 100%;color: #6c6c6c;background-color: #fff">
                            <span class="f-icon f-icon-packup"></span>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row f-utils-flex-row-wrap farris-form ${controlsInlineCls} condition-with-fixed ${completeCls} condition-div">
                ${ctx.filterControlTpls}
            </div>
        </div>

    `;

};



