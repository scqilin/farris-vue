import { Component, OnInit, Output, Input, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { NotifyService } from '@farris/ui-notify';

@Component({
  selector: 'app-query-scheme-preset-fields-editor',
  templateUrl: './query-scheme-preset-fields-editor.component.html',
  styleUrls: ['./query-scheme-preset-fields-editor.component.css']
})
export class QuerySchemePresetFieldsEditorComponent implements OnInit {
  @Output() closeModal = new EventEmitter<any>();
  @Output() submitModal = new EventEmitter<any>();
  @Input() value;
  @Input() editorParams = { fieldConfigs: [] };
  @ViewChild('footer') modalFooter: TemplateRef<any>;

  modalConfig = {
    title: '筛选方案预置字段编辑器',
    width: 900,
    height: 500,
    showButtons: true,
    showMaxButton: false
  };

  // 左侧列表数据
  fieldConfigData = [];

  // 选中的字段ID列表
  selectedFieldIds = [];

  // 选中的字段列表
  selectedFields = [];

  constructor(
    private notifyService: NotifyService,
  ) { }

  ngOnInit(): void {
    if (!this.editorParams) {
      return;
    }
    this.fieldConfigData = this.editorParams.fieldConfigs;

    if (this.value && this.value.length > 0) {
      this.selectedFieldIds = this.value.map(v => v.id);
      this.selectedFields = this.value;
    }
  }

  changeSelectField(e) {
    this.selectedFields = e;
  }
  /**
   * 取消
   */
  clickCancel() {
    this.closeModal.emit();
  }
  /**
   * 确定
   */
  clickConfirm() {

    if (this.selectedFields.length === 0) {
      this.notifyService.warning('请选择预置字段');
      return;
    }
    this.submitModal.emit({ value: this.selectedFields });
  }


}
