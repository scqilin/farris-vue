import { NotifyService } from '@farris/ui-notify';
import { FarrisDesignBaseComponent } from '@farris/designer-element';

import { RefreshFormService, DomService } from '@farris/designer-services';
import { ComponentSchema } from '@farris/designer-element';
import { ControlContextMenuItem } from '../../../../../entity/control-context-menu';
import { DgControl } from '../../../../../utils/dg-control';

const MOVE_TO_TAB_COMMAND = 'moveToTab';

/**
 * 标签页移动至其他标签页
 */
export class TabMoveToTabService {

    /** tab组件实例 */
    private cmpInstance: FarrisDesignBaseComponent;

    // tab的祖父节点的父容器，一般指向主区域节点（卡片面板一般为like-card-container；列卡模板一般为SplitterPane）
    private grandParentParentContainer: any;

    // tab的祖父节点，一般为container
    private grandParentContainer: any;

    constructor(
        private notifyService: NotifyService,
        private refreshFormService: RefreshFormService,
        private domService: DomService
    ) { }

    /**
     * 组装是否支持移动至标签页的菜单
     * @param dgTabElement 当前标签页节点
     * @param menuConfig 右键菜单
     */
    assembleMoveToTabMenu(cmpInstance: FarrisDesignBaseComponent, menuConfig: ControlContextMenuItem[]) {
        this.cmpInstance = cmpInstance;

        const tabElement = cmpInstance.component;

        const moveToTabMenu = menuConfig.find(menu => menu.id === MOVE_TO_TAB_COMMAND);
        if (!moveToTabMenu) {
            return menuConfig;
        }

        const checkResult = this.checkCanMoveToTab(tabElement);
        if (checkResult) {
            menuConfig = menuConfig.filter(menu => menu.id !== MOVE_TO_TAB_COMMAND);
            return menuConfig;
        }

        // 校验祖父容器的同级中有没有Container--->Tab 的层级结构
        this.grandParentContainer = this.cmpInstance.parent.parent.component;
        this.grandParentParentContainer = this.cmpInstance.parent.parent.parent.component;

        // 组装子级菜单
        moveToTabMenu.children = [];
        this.grandParentParentContainer.contents.forEach(container => {
            if (container.type !== DgControl.ContentContainer.type) {
                return;
            }
            const tabNode = this.domService.selectNode(container, item => item.type === DgControl.Tab.type);
            if (tabNode && tabNode.id !== tabElement.id && tabNode.controlSource === tabElement.controlSource) {
                moveToTabMenu.children.push({
                    id: tabNode.id,
                    title: tabNode.id
                });
            }
        });

        if (moveToTabMenu.children.length === 0) {
            menuConfig = menuConfig.filter(menu => menu.id !== MOVE_TO_TAB_COMMAND);
        }

        return menuConfig;

    }

    private checkCanMoveToTab(tabElement: ComponentSchema) {

        // 1、不包含tabPage，不允许切换
        if (!tabElement.contents || tabElement.contents.length === 0) {
            return '空标签页不支持移动至标签页';
        }
        // 2、标准模板规定container-section-tab 是成对出现三层结构，转换后的section结构也是container-section
        const sectionElement = this.cmpInstance.parent.component;
        if (!sectionElement || sectionElement.type !== DgControl.Section.type) {
            return '标签页的父级不是分组面板，不支持移动至标签页';
        }
        const containerElement = this.cmpInstance.parent.parent.component;
        if (!containerElement || containerElement.type !== DgControl.ContentContainer.type) {
            return '标签页的祖父级不是ConentContainer容器，不支持移动至标签页';
        }

        // 3、限制三个结构的class样式
        if (!tabElement.appearance || !tabElement.appearance.class || !tabElement.appearance.class.includes('f-component-tabs')) {
            return '标签页的class样式不包含f-component-tabs，不支持移动至标签页';
        }
        if (!sectionElement.appearance || !sectionElement.appearance.class || !sectionElement.appearance.class.includes('f-section-tabs')) {
            return '分组面板的class样式不包含f-section-tabs，不支持移动至标签页';
        }
        if (!containerElement.appearance || !containerElement.appearance.class || !containerElement.appearance.class.includes('f-struct-wrapper')) {
            return 'ContentContainer容器的class样式不包含f-struct-wrapper，不支持移动至标签页';
        }

        return;
    }

    /**
     * tab移动至tab
     * Container-Section-ComponentRef 切换为TabPage-ComponentRef
     * @param sectionElement section元素
     * @param targetTabId 目标标签页id
     */
    moveToTab(targetTabId: string) {
        const targetTabElement = this.domService.domDgMap.get(targetTabId);
        const sourceTabElement = this.cmpInstance.component;

        targetTabElement.contents = targetTabElement.contents.concat(sourceTabElement.contents);


        // 移除tab的祖父容器
        this.grandParentParentContainer.contents = this.grandParentParentContainer.contents.filter(c => c.id !== this.grandParentContainer.id);

        this.notifyService.success('移动成功');

        this.cmpInstance.emit('clearPropertyPanel');
        this.refreshFormService.refreshFormDesigner.next();
    }
}
