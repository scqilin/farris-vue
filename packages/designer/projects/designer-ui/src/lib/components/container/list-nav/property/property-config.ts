import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class ListNavProp extends ContainerUsualProp {
    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];


        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const customPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(customPropConfig);

        // 拖拽属性
        const dragPropConfig = this.getDragPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(dragPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;

    }



    private getAppearancePropConfig(propertyData: any): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        commonProps.push(
            {
                propertyID: 'size',
                propertyName: '尺寸',
                propertyType: 'cascade',
                cascadeConfig: [
                    {
                        propertyID: 'width',
                        propertyName: '宽度（px）',
                        propertyType: 'number',
                        description: '组件的宽度',
                        min: 0,
                        decimals: 0
                    },
                    {
                        propertyID: 'height',
                        propertyName: '高度（px）',
                        propertyType: 'number',
                        description: '组件的高度',
                        min: 0,
                        decimals: 0
                    }
                ]
            },
            {
                propertyID: 'title',
                propertyName: '标签',
                propertyType: 'string'
            },
            {
                propertyID: 'hideNav',
                propertyName: '默认收起',
                propertyType: 'boolean',
                defaultValue: false
            },
            {
                propertyID: 'showEntry',
                propertyName: '是否显示收折按钮',
                propertyType: 'select',
                iterator: [{ key: true, value: '是' }, { key: false, value: '否' }]
            },
            {
                propertyID: 'position',
                propertyName: '收折按钮显示位置', // 因为控件里这个属性是反的，所以在设计器上枚举项也反着写
                propertyType: 'select',
                defaultValue: 'left',
                iterator: [{ key: 'left', value: '靠右' }, { key: 'right', value: '靠左' }, { key: 'bottom', value: '靠上' }, { key: 'top', value: '靠下' }],
                visible: propertyData.showEntry
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps,
            setPropertyRelates(changeObject: FormPropertyChangeObject) {
                switch (changeObject.propertyID) {
                    case 'showEntry': {
                        const position = this.properties.find(prop => prop.propertyID === 'position');
                        if (position) {
                            position.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                }
            }
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ]
        };
    }
    private getDragPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'drag',
            categoryName: '拖拽',
            properties: [
                {
                    propertyID: 'enableDraggable',
                    propertyName: '启用拖拽',
                    propertyType: 'boolean',
                    defaultValue: false
                },
                {
                    propertyID: 'rzHandles',
                    propertyName: '拖拽线条的位置',
                    propertyType: 'select',
                    iterator: [{ key: 'e', value: '靠右' }, { key: 'w', value: '靠左' }, { key: 's', value: '靠下' }, { key: 'n', value: '靠上' }],
                    visible: propertyData.enableDraggable
                },
                {
                    propertyID: 'relatedIframeParent',
                    propertyName: '右侧区域元素id',
                    description: '非必填，若导航右侧有iframe，此处填写iframe所在元素的父元素ID。例如#containerId',
                    propertyType: 'string',
                    visible: propertyData.enableDraggable
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'enableDraggable': {
                        this.properties.map(p => {
                            if (p.propertyID !== 'enableDraggable') {
                                p.visible = changeObject.propertyValue;
                            }
                        });
                        break;
                    }
                }
            }
        };
    }

    public getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;

        // 静态事件
        let eventList = [];

        // 动态事件
        this.switchEvents(propertyData, eventList);

        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList, this.switchEvents),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: FormPropertyChangeObject, data: any, parameters: any) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, parameters['events'], parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, parameters['events'], self.switchEvents);
            }
        };

    }
    /**
     * 处理交互面板中的动态事件
     * @param propertyData 属性值
     * @param eventList 事件列表
     */
    switchEvents(propertyData: any, eventList: any[]) {

        if (propertyData.enableDraggable) {
            const eventListExist = eventList.find(eventListItem => eventListItem.label === 'rzStart');
            if (!eventListExist) {
                eventList.push(
                    {
                        label: 'rzStart',
                        name: '拖拽开始时事件'
                    },
                    {
                        label: 'rzResizing',
                        name: '拖拽时事件'
                    },
                    {
                        label: 'rzStop',
                        name: '拖拽结束时事件'
                    }
                );
            }
        } else {
            if (eventList.length !== 0) {
                eventList = eventList.filter(eventListItem => !['rzStart', 'rzResizing', 'rzStop'].includes(eventListItem.label));
            }
        }

        return eventList;

    }
}
