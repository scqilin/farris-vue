export default (ctx: any) => {
    return ctx.shownHtml || `
    <div class="alert alert-info no-drag w-100" style="height: 50px;display: flex;justify-content: center;align-items: center;overflow: hidden;" role="alert" data-noattach="true" data-position="0">
      请在属性面板中配置模板
  </div>`;
};
