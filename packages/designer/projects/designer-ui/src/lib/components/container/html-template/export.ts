import { ComponentExportEntity } from '@farris/designer-element';
import FdHtmlTemplateComponent from './component/fd-html-template';
import { HtmlTemplateSchema } from './schema/schema';

import FdHtmlTemplates from './templates';


export const HtmlTemplate: ComponentExportEntity = {
    type: 'HtmlTemplate',
    component: FdHtmlTemplateComponent,
    template: FdHtmlTemplates,
    metadata: HtmlTemplateSchema
};
