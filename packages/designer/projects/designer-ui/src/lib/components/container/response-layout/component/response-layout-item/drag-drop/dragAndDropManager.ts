import { BuilderHTMLElement } from '@farris/designer-element';
import FdContainerBaseComponent from '../../../../common/containerBase/containerBase';
import { DgControl } from '../../../../../../utils/dg-control';
import { SchemaService, DomService, ComponentResolveContext, FormBasicService, DesignerEnvType } from '@farris/designer-services';
import { FormDragDropManager } from '../../../../form/drag-drop/dragAndDropManager';

export class ResponseLayoutItemDragDropManager {

    private cmpInstance: FdContainerBaseComponent;

    constructor(cmpInstance: FdContainerBaseComponent) {
        this.cmpInstance = cmpInstance;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     * @returns boolean
     */
    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {

        const serviceHost = this.cmpInstance.options.designerHost;
        const domService = serviceHost.getService('DomService') as DomService;
        const dragResolveService = serviceHost.getService('DragResolveService');
        const resolveContext = dragResolveService.getComponentResolveContext(sourceElement, this);

        // 布局容器组件不能嵌套
        if (resolveContext.controlType === DgControl.ResponseLayout.type) {
            return false;
        }

        // 每个布局项内只能放一个子级控件
        if (this.cmpInstance.component.contents && this.cmpInstance.component.contents.length) {
            return false;
        }

        // 可以接收控件工具箱的输入类控件
        if (resolveContext.sourceType === 'control' && resolveContext.controlCategory === 'input') {
            return true;
        }

        // 可以接收实体树中拖拽来的字段，需要判断是否为主表
        if (resolveContext.sourceType === 'field') {
            const schemaService = serviceHost.getService('SchemaService') as SchemaService;
            const fieldInfo = schemaService.getFieldByIDAndVMID(resolveContext.bindingTargetId, this.cmpInstance.viewModelId);
            if (!fieldInfo || !fieldInfo.schemaField) {
                return false;
            }
        }

        // 在现有设计器中拖拽控件：跨Component的移动输入控件，需要判断组件是否绑定同一实体。
        const sourceComponentInstance = sourceElement.componentInstance;
        if (sourceComponentInstance && sourceComponentInstance.viewModelId !== this.cmpInstance.viewModelId) {

            const sourceVM = domService.getViewModelById(sourceComponentInstance.viewModelId);
            const currentVM = domService.getViewModelById(this.cmpInstance.viewModelId);
            return sourceVM.bindTo === currentVM.bindTo;

        }


        return true;
    }
    /**
     * 单元格接收新控件后事件(来源为控件工具箱或者实体树)
     * @param componentResolveContext 拖拽上下文
     */
    afterAcceptNewChildElement(componentResolveContext: ComponentResolveContext) {
        const { controlCategory, controlTemplate } = componentResolveContext;

        // 接收输入控件以及输入类的模板控件：需要在布局容器上增加form样式
        if (controlCategory === 'input' ||
            (controlCategory === 'componentTemplate' && controlTemplate && controlTemplate.templateCategory === 'input')) {
            this.addFormClassToResponseLayoutItem();
        }
    }

    /**
     * 移动控件后事件：在可视化设计器中，将现有的控件移动到容器中
     * @param el 移动的源DOM结构
     */
    onAcceptMovedChildElement(sourceElement: BuilderHTMLElement) {
        if (!sourceElement) {
            return;
        }

        // 若拖拽的源组件是输入类控件，需要在当前布局项上增加form样式
        if (sourceElement.componentInstance && sourceElement.componentInstance.category === 'input') {
            this.addFormClassToResponseLayoutItem();
        }

        const dragManager = new FormDragDropManager(this.cmpInstance);
        dragManager.onAcceptMovedChildElement(sourceElement);
    }

    /**
     * 在布局项中追加form的样式
     * @param componentInstance 组件实例
     */
    private addFormClassToResponseLayoutItem() {
        if (!this.cmpInstance.component.appearance) {
            this.cmpInstance.component.appearance = { class: '' };
        }
        if (!this.cmpInstance.component.appearance.class) {
            this.cmpInstance.component.appearance.class = '';
        }
        // farris-form-auto是为了让控件长度不受最大长度限制
        if (!this.cmpInstance.component.appearance.class.includes('farris-form')) {
            this.cmpInstance.component.appearance.class += ' f-form-layout farris-form farris-form-controls-inline farris-form-auto';
        }
    }

    /**
     * 移除布局项中的form类样式
     */
    removeFormClassFromResponseLayoutItem() {
        if (!this.cmpInstance.component.appearance || !this.cmpInstance.component.appearance.class) {
            return;
        }
        this.cmpInstance.component.appearance.class = this.cmpInstance.component.appearance.class.replace('f-form-layout', '');
        this.cmpInstance.component.appearance.class = this.cmpInstance.component.appearance.class.replace('farris-form', '');
        this.cmpInstance.component.appearance.class = this.cmpInstance.component.appearance.class.replace('farris-form-controls-inline', '');
        this.cmpInstance.component.appearance.class = this.cmpInstance.component.appearance.class.replace('farris-form-auto', '');
        this.cmpInstance.component.appearance.class = this.cmpInstance.component.appearance.class.trim();
    }
}
