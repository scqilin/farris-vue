import FdComponentComponent from './component/fd-component';
import { ComponentSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdComponentTemplates from './templates';


export const Component: ComponentExportEntity = {
    type: 'Component',
    component: FdComponentComponent,
    template: FdComponentTemplates,
    metadata: ComponentSchema
};
