export default (ctx: any) => {

    // 顶层样式
    const sectionCls = getSectionClass(ctx);

    let template = `<div class="f-section ide-cmp-section f-utils-fill p-0 ${sectionCls}" >`;

    // 头部
    const headerStr = getHeaderTemplate(ctx);
    template += headerStr;

    // 扩展区域
    const extendStr = getExtendStr(ctx);
    template += extendStr;

    // 内容区按钮
    const contentBtnStr = getContentToolbarStr(ctx);

    if (contentBtnStr) {
        template += `
            <div class="f-section-content">
                ${contentBtnStr}
                <div class="drag-container"  ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container" id="${ctx.component.id}">
                    ${ctx.children}
                </div>
            </div>
        </div>
        `;
    } else {
        template += `
            <div class="f-section-content drag-container" ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container" id="${ctx.component.id}">
                ${ctx.children}
            </div>
        </div>
    `;
    }

    return template;
};

/**
 * 顶层样式
 */
function getSectionClass(ctx: any) {
    let sectionCls = ctx.component.fill ? ' f-section-fill' : '';
    // 处理収折
    if (ctx.component.enableAccordion && ctx.component.accordionMode) {
        sectionCls += ctx.component.accordionMode === 'default' ? ' f-section-accordion' : (ctx.component.accordionMode === 'custom' ? ' f-section-custom-accordion' : '');
        sectionCls += !ctx.expandStatus ? ' f-state-collapse' : '';
    }

    return sectionCls;
}
/**
 * 头部区域
 */
function getHeaderTemplate(ctx: any) {
    if (!ctx.component.showHeader) {
        return '';
    }

    // 启用头部模板
    if (ctx.component.headerTemplate) {
        return `<div class="f-section-header ${ctx.component.headerClass}">
                    ${ctx.component.headerTemplate}
                </div>`;
    }

    // 头部标题
    const htitleStr = getHeaderTitleStr(ctx);
    // 头部内容
    const hcontentStr = getHeaderContentStr(ctx);
    // 头部工具栏模板
    const htoolbarTmplStr = gethHeadertoolbarStr(ctx);
    // 头部最大化
    const maxAccordionStr = getMaxAccordionStr(ctx);
    // 视图切换
    const viewChangeStr = getViewChangeStr(ctx);

    return `
    <div class="f-section-header">
        ${htitleStr} ${hcontentStr} ${htoolbarTmplStr} ${viewChangeStr} ${maxAccordionStr}
    </div>`;
}

/**
 * 头部标题
 */
function getHeaderTitleStr(ctx: any) {
    if (ctx.component.titleTemplate) {
        return ctx.component.titleTemplate;
    }

    let htitleStr = `<div class="f-title"><h4 class="f-title-text">${ctx.component.mainTitle}</h4>`;

    if (ctx.component.subTitle) {
        htitleStr += `<span>${ctx.component.subTitle}</span>`;
    }
    htitleStr += '</div>';

    return htitleStr;
}

/**
 * 头部内容区域
 */
function getHeaderContentStr(ctx: any) {
    let hcontentStr = '';
    if (ctx.component.extendedHeaderAreaTemplate) {
        hcontentStr = `
        <div class="f-content ${ctx.component.extendedHeaderAreaClass}">
            ${ctx.component.extendedHeaderAreaTemplate}
        </div>`;
    }

    return hcontentStr;
}


/**
 * 头部工具栏模板
 */
function gethHeadertoolbarStr(ctx: any) {

    if (ctx.component.toolbarTemplate) {
        return ` <div class="f-toolbar ${ctx.component.toolbarClass} f-btn-mr">${ctx.component.toolbarTemplate}</div>`;
    }

    let htoolbarTmplStr = '';
    if (ctx.component.toolbar && ctx.component.toolbar.position === 'inHead' && ctx.component.toolbar.contents.length > 0) {
        htoolbarTmplStr = `<div  class="f-section-toolbar f-section-header--toolbar f-btn-mr" style="overflow:hidden">
             ${ctx.toolBarComponent.render()}
         </div>`;
    }

    return htoolbarTmplStr;
}

/**
 * 视图切换
 */
function getViewChangeStr(ctx: any) {
    if (ctx.component.multiViews && ctx.component.views) {
        return ` <div class="f-viewchange">
                    <div class="f-view-change" ref="${ctx.component.views.viewGroupIdentify}" viewChange="${ctx.component.views.viewGroupIdentify}">
                        ${ctx.getViewChangeStr()}
                     </div>
                </div>`;
    }

    return '';
}

/**
 * 收折、最大化
 */
function getMaxAccordionStr(ctx: any) {
    if (ctx.component.enableMaximize || (ctx.component.enableAccordion && ctx.component.accordionMode !== '')) {
        // 最大化
        const maxStr = ctx.component.enableMaximize ? ' <span class="f-icon f-icon-maximize"></span>' : '';

        // 收折
        let accordionStr = '';
        const accordionCls = ctx.expandStatus ? ' f-state-expand' : '';
        const accordionText = ctx.expandStatus ? '收起' : '展开';

        if (ctx.component.enableAccordion && ctx.component.accordionMode !== '') {
            accordionStr = `
            <button class="btn f-btn-collapse-expand f-btn-mx ${accordionCls}" ref="${ctx.collapseKey}">
                <span>${accordionText}</span>
            </button>
            `;
        }

        return ` <div class="f-max-accordion">${maxStr} ${accordionStr}</div>`;
    }

    return '';
}


/**
 * 内容区域按钮
 */
function getContentToolbarStr(ctx: any) {
    let contentBtnStr = '';
    if (!ctx.component.toolbarTemplate && ctx.component.toolbar && ctx.component.toolbar.contents.length > 0 && ctx.component.toolbar.position == 'inContent') {
        contentBtnStr = ` <div class="f-section-toolbar f-section-content--toolbar" style="overflow:hidden">
        ${ctx.toolBarComponent.render()}
        </div>`;
    }

    return contentBtnStr;
}


/**
 * 扩展区域 
 */
function getExtendStr(ctx: any) {
    let extendStr = '';
    if (ctx.component.extendedAreaTemplate) {
        extendStr = `<div class="f-section-extend ${ctx.component.extendedAreaClass}">
            ${ctx.component.extendedAreaTemplate}
        </div>`;
    }

    return extendStr;
}
