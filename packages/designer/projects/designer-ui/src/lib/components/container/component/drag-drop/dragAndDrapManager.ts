import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { DgControl } from '../../../../utils/dg-control';
import { ComponentResolveContext, FormComponentType } from '@farris/designer-services';

export class ComponentDragDropManager {

    private cmpInstance: FdContainerBaseComponent;


    private canAcceptChildContent = true;

    constructor(cmpInstance: FdContainerBaseComponent) {
        this.cmpInstance = cmpInstance;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    canAccepts(resolveContext: ComponentResolveContext): boolean {

        if (!this.canAcceptChildContent) {
            return false;
        }
        return true;
    }

    /**
     * 判断当前容器是否是固定的上下文的中间层级
     */
    checkIsInFixedContextRules() {
        const component = this.cmpInstance.component;

        // 根组件不能移动、删除
        if (component.id === 'root-component' && component.componentType === FormComponentType.Frame) {
            return true;
        }
        // 控件本身样式
        const cmpClass = component.appearance && component.appearance.class || '';
        const cmpClassList = cmpClass.split(' ');

        // 组件引用ComponentRef节点
        const componentRefEle = this.cmpInstance.parent;
        if (componentRefEle.type !== DgControl.ComponentRef.type) {
            return true;
        }
        // 组件引用节点的父级
        const componentRefParent = componentRefEle.parent;
        const componentRefParentEle = componentRefParent && componentRefParent.component;
        const componentRefParentEleCls = componentRefParentEle && componentRefParentEle.appearance ? componentRefParentEle.appearance.class : '';
        const componentRefParentEleClsList = componentRefParentEleCls ? componentRefParentEleCls.split(' ') : [];


        const componentRefGrandParentEle = componentRefParent && componentRefParent.parent && componentRefParent.parent.component;
        const componentRefGrandParentEleCls = componentRefGrandParentEle && componentRefGrandParentEle.appearance ? componentRefGrandParentEle.appearance.class : '';
        const componentRefGrandParentEleClsList = componentRefGrandParentEleCls ? componentRefGrandParentEleCls.split(' ') : [];

        const componentType = component.componentType;

        // 多视图下的组件不允许移动、删除
        if (componentRefEle.parent.type === DgControl.MultiViewContainer.type) {
            return true;
        }

        // 管理列表、带侧边栏的列表、带导航的列表中DataGrid组件禁止移动、删除
        if (componentType === FormComponentType.dataGrid) {
            if (componentRefParentEle.type === DgControl.ContentContainer.type && componentRefParentEleClsList.includes('f-page-main')) {
                return true;
            }
            // 在分栏面板中的组件不支持移动
            if (componentRefParentEle.type === DgControl.SplitterPane.type && componentRefParentEleCls.includes('f-page-')) {
                return true;
            }

            // 单树组件模板的列表
            if (componentRefParentEle.type === DgControl.ContentContainer.type && componentRefParentEleClsList.includes('f-page-module')) {
                return true;
            }

            // 7、section类子表：container-section-component
            if (cmpClassList.includes('f-struct-is-subgrid') && (componentRefParentEleClsList.includes('f-section-in-mainsubcard') || componentRefParentEleClsList.includes('f-section-in-main')) &&
                componentRefGrandParentEleClsList.includes('f-struct-wrapper')) {
                this.canAcceptChildContent = false;
                return true;
            }
        }

        return false;
    }
}
