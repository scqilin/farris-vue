export default (ctx: any) => {
    let splitterPaneBodys = '';

    ctx.component.contents.forEach((splitterPane, index) => {

        splitterPaneBodys += `
        <farris-splitter-pane
            class="f-component-splitter-pane ${splitterPane.appearance && splitterPane.appearance.class}" ref="${ctx.splitterPaneKey}"
            dragref="${ctx.splitterPaneKey}-container" id="${splitterPane.id}">
            ${ctx.splitterPaneComponents[index]}
        </farris-splitter-pane>
        `;

    });


    return `
    <div class="f-component-splitter ${ctx.component.appearance && ctx.component.appearance.class}"
        id="${ctx.component.id}">

        ${splitterPaneBodys}

    </div>
    `;
};
