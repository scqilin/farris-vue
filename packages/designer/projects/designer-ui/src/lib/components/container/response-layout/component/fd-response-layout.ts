import { ResponseLayoutItemSchema, ResponseLayoutSchema } from '../schema/schema';
import { FdResponseLayoutProp } from '../property/property-config';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { BuilderHTMLElement, FarrisDesignBaseComponent } from '@farris/designer-element';
import FdResponseLayoutItemComponent from './response-layout-item/component/fd-response-layout-item';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DgControl } from '../../../../utils/dg-control';
import { DesignerEnvType, DesignViewModelService, RefreshFormService } from '@farris/designer-services';
import { cloneDeep } from 'lodash-es';
import { SaveAsTemplateEditorComponent } from '@farris/designer-devkit';
import { ComponentFactoryResolver, Injector } from '@angular/core';
import { BsModalService } from '@farris/ui-modal';

export default class FdResponseLayoutComponent extends FdContainerBaseComponent {

    /** 标识每个布局项的key值 */
    get layoutItemKey(): string {
        return `item-${this.id}`;
    }

    /** 布局项实例 */
    layoutItemComponents: FdResponseLayoutItemComponent[];

    /** 当前选中布局项id */
    selectedLayoutItemId: string;

    constructor(component: any, options: any) {
        super(component, options);
    }

    /**
     * 从工具箱拖拽生成时需要预制布局项
     */
    static getMetadataInControlBox(targetComponentInstance: FarrisDesignBaseComponent, controlFeature: any) {
        // 判断是何种布局
        const feature = controlFeature ? JSON.parse(controlFeature) : null;
        const splitterClass = (feature && feature.splitter) || '6:6';
        const columnParts = splitterClass.split(':');
        const layoutItems = [];

        // tslint:disable-next-line: prefer-for-of
        for (let index = 0; index < columnParts.length; index++) {
            const radomNum = Math.random().toString().slice(2, 6);
            const newLayoutItem = cloneDeep(ResponseLayoutItemSchema);
            newLayoutItem.id = `layout_${radomNum}`;
            newLayoutItem.appearance.class += ` col-${columnParts[index]} px-0`;

            layoutItems.push(newLayoutItem);
        }

        const layoutSchema = cloneDeep(ResponseLayoutSchema);
        Object.assign(layoutSchema, {
            id: `response_layout_${Math.random().toString().slice(2, 6)}`,
            contents: layoutItems,
            appearance: {
                class: 'bg-white'
            }
        });

        return layoutSchema;
    }

    getDefaultSchema(): any {
        return ResponseLayoutSchema;
    }

    getTemplateName(): string {
        return 'ResponseLayout';
    }

    /** 设计时增加下边距，方便拖拽 */
    getStyles(): string {
        return ' margin-bottom:10px';
    }

    init(): void {
        this.components = [];
        this.layoutItemComponents = [];
        const options = Object.assign({}, this.options, { parent: this });
        this.component.contents.forEach((item, index) => {
            item.contents = item.contents || [];
            const layoutItemComponent: any = this.createComponent(item, options, null);

            this.layoutItemComponents[index] = layoutItemComponent;
        });
    }

    render(): any {

        this.addSaveAsCustomToolbarConfig();

        return super.render(this.renderTemplate('ResponseLayout', {
            layoutItemElements: this.layoutItemComponents.map((pane: FdResponseLayoutItemComponent) => pane.renderComponents()),
            layoutItemKey: this.layoutItemKey,
            selectedLayoutItemId: this.selectedLayoutItemId
        }));
    }



    attach(element: any): any {
        this.loadRefs(element, {
            [this.layoutItemKey]: 'multiple',
        });
        const superAttach: any = super.attach(element);

        this.refs[this.layoutItemKey].forEach((layoutItemEle, index) => {
            this.layoutItemComponents[index].attach(layoutItemEle);
        });

        return superAttach;
    }

    /**
     * 组装属性面板配置数据
     * @returns ElementPropertyConfig[]
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: FdResponseLayoutProp = new FdResponseLayoutProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
        super.onPropertyChanged(changeObject);

        if (changeObject.propertyID === 'layout') {

            // 若有删除的布局项，需要触发布局项的删除方法，从而触发布局项内部控件的删除方法
            this.afterLayoutItemCountChanged();

            this.rebuild();

            // 刷新页面，触发控件树更新
            const refreshFormService = this.options.designerHost.getService('RefreshFormService') as RefreshFormService;
            refreshFormService.refreshFormDesigner.next(this.component.id);
        }
    }
    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {
        return false;
    }

    /**
     * 零代码：支持输入控件另存为模板
     */
    private addSaveAsCustomToolbarConfig() {
        this.customToolbarConfigs = [];

        if (this.envType !== DesignerEnvType.noCode) {
            return;
        }
        // 只有带输入类控件的布局容器支持另存为，若布局容器中包含非输入类控件，则不允许另存为模板
        let hasControlExceptInput = false;
        const saveAsControls = [];
        this.component.contents.forEach(item => {
            if (item.contents && item.contents.length) {
                if (item.appearance && item.appearance.class && item.appearance.class.includes('farris-form')) {
                    saveAsControls.push(item.contents[0]);
                } else {
                    hasControlExceptInput = true;
                    return;
                }

            }
        })
        if (!hasControlExceptInput && saveAsControls.length) {
            this.customToolbarConfigs.push(
                {
                    id: 'saveAsTemplate',
                    title: '另存为模板',
                    icon: 'f-icon f-icon-save',
                    click: (e) => {
                        e.stopPropagation();
                        this.showSaveAsTemplateModal();
                    }
                });
        }

    }
    /**
     * 弹出另存为模板的窗口
     */
    private showSaveAsTemplateModal() {
        // 动态创建相关的服务需要从designerHost中获取
        const serviceHost = this.options.designerHost;
        const resolver = serviceHost.getService('ComponentFactoryResolver') as ComponentFactoryResolver;
        const injector = serviceHost.getService('Injector') as Injector;
        const modalService = serviceHost.getService('ModalService') as BsModalService;

        const compFactory = resolver.resolveComponentFactory(SaveAsTemplateEditorComponent);
        const compRef = compFactory.create(injector);
        const modalConfig = compRef.instance.modalConfig as any;
        modalConfig.buttons = compRef.instance.modalFooter;

        const layoutItemCount = this.component.contents ? this.component.contents.length || 3 : 3;
        const saveAsComponentDom = cloneDeep(this.component);
        compRef.instance.editorParams = {
            controlCategory: 'container',
            controlTitle: DgControl[this.component.type].name + '模板',
            controlDom: saveAsComponentDom,
            viewModelId: this.viewModelId,
            templateIconType: this.component.type + layoutItemCount
        }


        const modalPanel = modalService.show(compRef, modalConfig);
        compRef.instance.closeModal.subscribe(() => {
            modalPanel.close();
        });
        compRef.instance.submitModal.subscribe(() => {
            modalPanel.close();

            const refreshFormService = this.options.designerHost.getService('RefreshFormService') as RefreshFormService;

            if (refreshFormService && refreshFormService.refreshControlBox) {
                refreshFormService.refreshControlBox.next();
            }
        });

    }

    /**
     * 变更布局项后事件：若有删除的布局项，需要触发布局项的删除方法，从而触发布局项内部控件的删除方法
     */
    private afterLayoutItemCountChanged() {
        const layoutItemInstances = this.components || [];
        const layoutItemSchema = this.component.contents || [];
        const layoutItemIds = layoutItemSchema.map(item => item.id);

        layoutItemInstances.forEach(itemInstance => {

            if (!layoutItemIds.includes(itemInstance.id) && itemInstance.onRemoveComponent) {
                itemInstance.onRemoveComponent();
            }
        })
    }
}
