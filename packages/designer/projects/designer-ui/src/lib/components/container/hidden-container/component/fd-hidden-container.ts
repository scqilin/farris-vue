import { HiddenContainerSchema } from '../schema/schema';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { HiddenContainerProp } from '../property/property-config';

export default class FdHiddenContainerComponent extends FdContainerBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);
    }


    getDefaultSchema(): any {

        return HiddenContainerSchema;
    }

    getTemplateName(): string {
        return 'HiddenContainer';
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: HiddenContainerProp = new HiddenContainerProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

}
