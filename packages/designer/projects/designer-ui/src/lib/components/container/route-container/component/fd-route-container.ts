import { RouteContainerSchema } from '../schema/schema';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { RouteContainerProp } from '../property/property-config';
import { BuilderHTMLElement } from '@farris/designer-element';

export default class FdRouteContainerComponent extends FdContainerBaseComponent {
    constructor(component: any, options: any) {
        super(component, options);
    }

 
    getDefaultSchema(): any {

        return RouteContainerSchema;
    }

    getTemplateName(): string {
        return 'RouteContainer';
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: RouteContainerProp = new RouteContainerProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 不接收子组件
     */
    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {
        return false;
    }

}
