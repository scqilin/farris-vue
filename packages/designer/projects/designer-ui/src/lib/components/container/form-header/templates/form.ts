export default (ctx: any) => {
    return `
        <div class="f-title-pagination">
            <span class="f-icon f-icon-arrow-w" style="width: 1.25rem;height: 1.25rem;margin: 0 0.375rem 0 0;color: #878D99;line-height: 1.125rem;border-width: 1px;border-style: solid;border-color: #DEE1EA;border-radius: 3px;background: #fff;cursor: pointer;"></span>
            <span class="f-icon f-icon-arrow-e" style="width: 1.25rem;height: 1.25rem;margin: 0 0.375rem 0 0;color: #878D99;line-height: 1.125rem;border-width: 1px;border-style: solid;border-color: #DEE1EA;border-radius: 3px;background: #fff;cursor: pointer;"></span>
        </div>
      `;
};
