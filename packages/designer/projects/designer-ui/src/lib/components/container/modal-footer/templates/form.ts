export default (ctx: any) => {
  // 自定义模板
  const { upStr, leftStr, rightStr } = getCustomTemplate(ctx);

  // 工具栏区域
  const toolbarStr = ctx.footerToolBar.render() || '';

  return `<div class="fe-modal-footer-wrapper  ide-cmp-modal-footer showtype-${ctx.currentFormMode}">
      ${upStr}
      <div class="fe-modal-footer-base no-gutters">
      ${leftStr} ${toolbarStr} ${rightStr}
      </div>
  </div>`;
};


function getCustomTemplate(ctx: any) {
  // 上方扩展区域
  let upStr = '';
  if (ctx.component.upTemplate) {
    upStr = ` <div class="f-top ${ctx.component.upTemplateCls}">
     ${ctx.component.upTemplate}
    </div>`;
  }

  // 左侧扩展区域
  let leftStr = '';
  if (ctx.component.leftTemplate) {
    leftStr = ` <div class="f-left ${ctx.component.leftTemplateCls}">
     ${ctx.component.leftTemplate}
    </div>`;
  }

  // 右侧扩展区域
  let rightStr = '';
  if (ctx.component.rightTemplate) {
    rightStr = ` <div class="f-right ${ctx.component.rightTemplateCls}">
     ${ctx.component.rightTemplate}
    </div>`;
  }

  return { upStr, leftStr, rightStr };
}
