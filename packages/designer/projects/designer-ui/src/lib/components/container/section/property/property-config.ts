import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DgControl } from '../../../../utils/dg-control';
import { ContainerUsualProp } from '../../common/property/container-property-config';


export class SectionProp extends ContainerUsualProp {

  propertyConfig: ElementPropertyConfig[];

  getPropConfig(propertyData: any): ElementPropertyConfig[] {
    this.propertyConfig = [];

    // 基本信息属性
    const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
    this.propertyConfig.push(basicPropConfig);

    // 外观属性
    const appearancePropConfig = this.getAppearancePropConfig(propertyData);
    this.propertyConfig.push(appearancePropConfig);

    // 行为属性
    const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
    this.propertyConfig.push(behaviorPropConfig);

    // 模板属性
    const templatePropConfig = this.getTemplatePropConfig(propertyData, this.viewModelId);
    this.propertyConfig.push(templatePropConfig);

    // 扩展属性
    const extendPropConfig = this.getExtendPropConfig();
    this.propertyConfig.push(extendPropConfig);

    // 多视图属性
    const multiViewPropConfig = this.getMultiViewPropConfig(propertyData, this.viewModelId);
    this.propertyConfig.push(multiViewPropConfig);

    return this.propertyConfig;

  }


  private getAppearancePropConfig(propertyData: any): ElementPropertyConfig {

    // section下是Form节点，才允许启用自定义收折
    let hasFormElement = false;
    const sectionElement = this.domService.domDgMap.get(propertyData.id);
    hasFormElement = sectionElement && sectionElement.contents && sectionElement.contents.find(c => c.type === DgControl.Form.type);

    let advProperties: PropertyEntity[] = [];

    const commonProps = this.getCommonAppearanceProperties();
    advProperties = advProperties.concat(commonProps);
    advProperties.push(
      {
        propertyID: 'fill',
        propertyName: '填充',
        propertyType: 'boolean',
        description: 'flex布局下，填充满剩余部分',
        detail: 'https://igix.inspures.com/igixword/main.html#section'
      },
      {
        propertyID: 'expanded',
        propertyName: '展开',
        propertyType: 'boolean',
        description: '是否展开',
      },
      {
        propertyID: 'showHeader',
        propertyName: '显示头部区域',
        propertyType: 'boolean',
        description: '是否显示头部区域',
        detail: 'https://igix.inspures.com/igixword/main.html#section',
        refreshPanelAfterChanged: true
      },
      {
        propertyID: 'mainTitle',
        propertyName: '主标题',
        propertyType: 'string',
        description: '主标题名称',
        group: 'header'
      },
      {
        propertyID: 'subTitle',
        propertyName: '副标题',
        propertyType: 'string',
        description: '副标题名称',
        group: 'header'
      },
      {
        propertyID: 'enableMaximize',
        propertyName: '显示最大化',
        propertyType: 'boolean',
        description: '是否显示最大化',
        group: 'header'
      },
      {
        propertyID: 'enableAccordion',
        propertyName: '启用收折功能',
        propertyType: 'boolean',
        description: '是否启用收折功能',
        group: 'header'
      },
      {
        propertyID: 'accordionMode',
        propertyName: '收折模式',
        propertyType: 'select',
        description: '收折模式选择',
        iterator: [{ key: 'default', value: '默认收折' }, { key: 'custom', value: '自定义收折' }],
        group: 'header'
      });

    advProperties.map(p => {
      if (p.group === 'header') {
        p.visible = propertyData.showHeader;
      }
      if (p.propertyID === 'accordionMode') {
        p.visible = propertyData.showHeader && propertyData.enableAccordion === true && hasFormElement;
      }
    });
    const self = this;
    return {
      categoryId: 'appearance',
      categoryName: '外观',
      properties: advProperties,
      setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
        switch (changeObject.propertyID) {
          case 'showHeader': {
            this.properties.map(p => {
              if (p.group === 'header') {
                p.visible = changeObject.propertyValue;
              }
              if (p.propertyID === 'accordionMode') {
                p.visible = changeObject.propertyValue && propertyData.enableAccordion === true && hasFormElement;
              }
            });

            const templateCategory = self.propertyConfig.find(cat => cat.categoryId === 'template');
            if (templateCategory) {
              templateCategory.hide = !changeObject.propertyValue;
            }

            const multiViewCategory = self.propertyConfig.find(cat => cat.categoryId === 'multiView');
            if (multiViewCategory) {
              const multiViewToolbarData = propertyData.views && propertyData.views.toolbarData && propertyData.views.toolbarData;
              multiViewCategory.hide = !(changeObject.propertyValue && multiViewToolbarData && multiViewToolbarData.length > 0);
            }
            break;
          }
          case 'enableAccordion': {
            this.properties.map(p => {
              if (p.propertyID === 'accordionMode') {
                p.visible = changeObject.propertyValue && hasFormElement;
              }
            });
            break;
          }
          case 'mainTitle': {
            changeObject.needUpdateControlTreeNodeName = true;
          }
        }
      }
    };

  }

  private getTemplatePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
    return {
      categoryId: 'template',
      categoryName: '头部区域模板',
      hide: !propertyData.showHeader,
      properties: [
        {
          propertyID: 'titleTemplate',
          propertyName: '标题模板',
          propertyType: 'modal',
          description: '标题模板设置',
          editor: CodeEditorComponent,
          editorParams: {
            language: 'html'
          }
        },
        {
          propertyID: 'titleClass',
          propertyName: '标题样式',
          propertyType: 'string',
          description: '标题样式设置',
          visible: propertyData.titleTemplate
        },
        {
          propertyID: 'headerTemplate',
          propertyName: '头部模板',
          propertyType: 'modal',
          description: '头部模板设置',
          editor: CodeEditorComponent,
          editorParams: {
            language: 'html'
          }
        },
        {
          propertyID: 'headerClass',
          propertyName: '头部样式',
          propertyType: 'string',
          description: '头部样式设置',
          visible: propertyData.headerTemplate
        },
        {
          propertyID: 'extendedHeaderAreaTemplate',
          propertyName: '头部扩展区域模板',
          propertyType: 'modal',
          description: '头部扩展区域模板设置',
          editor: CodeEditorComponent,
          editorParams: {
            language: 'html'
          }
        },
        {
          propertyID: 'extendedHeaderAreaClass',
          propertyName: '头部扩展区域样式',
          propertyType: 'string',
          description: '头部扩展区域样式设置',
          visible: propertyData.extendedHeaderAreaTemplate
        },
        {
          propertyID: 'toolbarTemplate',
          propertyName: '工具栏模板',
          propertyType: 'modal',
          description: '工具栏模板设置',
          editor: CodeEditorComponent,
          editorParams: {
            language: 'html'
          }
        },
        {
          propertyID: 'toolbarClass',
          propertyName: '工具栏样式',
          propertyType: 'string',
          description: '工具栏样式设置',
          visible: propertyData.toolbarTemplate
        }
      ],
      setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
        switch (changeObject.propertyID) {
          case 'titleTemplate': {
            const titleClass = this.properties.find(p => p.propertyID === 'titleClass');
            if (titleClass) {
              titleClass.visible = changeObject.propertyValue;
            }
            break;
          }
          case 'headerTemplate': {
            const headerClass = this.properties.find(p => p.propertyID === 'headerClass');
            if (headerClass) {
              headerClass.visible = changeObject.propertyValue;
            }
            break;
          }
          case 'extendedHeaderAreaTemplate': {
            const extendedHeaderAreaClass = this.properties.find(p => p.propertyID === 'extendedHeaderAreaClass');
            if (extendedHeaderAreaClass) {
              extendedHeaderAreaClass.visible = changeObject.propertyValue;
            }
            break;
          }
          case 'toolbarTemplate': {
            const toolbarClass = this.properties.find(p => p.propertyID === 'toolbarClass');
            if (toolbarClass) {
              toolbarClass.visible = changeObject.propertyValue;
            }
            break;
          }
        }
      }
    };
  }

  private getMultiViewPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
    // section的多视图个数不是由section本身配置的，所以【显示多视图按钮】属性需要根据当前有没有视图来判断
    const multiViewToolbarData = propertyData.views && propertyData.views.toolbarData && propertyData.views.toolbarData;
    return {
      categoryId: 'multiView',
      categoryName: '多视图',
      hide: !(propertyData.showHeader && multiViewToolbarData && multiViewToolbarData.length > 0),
      properties: [
        {
          propertyID: 'multiViews',
          propertyName: '显示多视图按钮',
          propertyType: 'boolean',
          description: '是否显示多视图按钮',
          group: 'header',
          defaultValue: false
        },
        {
          propertyID: 'views',
          propertyName: '多视图配置',
          propertyType: 'cascade',
          visible: propertyData.multiViews,
          isExpand: true,
          hideCascadeTitle: true,
          cascadeConfig: [
            {
              propertyID: 'viewType',
              propertyName: '视图按钮排列方式',
              propertyType: 'select',
              description: '视图按钮排列方式设置',
              iterator: [{ key: 'tile', value: '平铺' }, { key: 'dropdown', value: '下拉选择' }]
            },
            {
              propertyID: 'defaultType',
              propertyName: '默认视图类型',
              propertyType: 'select',
              description: '默认显示的视图类型',
              iterator: this.getViewChangeDataTypes(propertyData.views)
            }
          ]
        }

      ],
      setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
        switch (changeObject.propertyID) {
          case 'multiViews': {

            const views = this.properties.find(p => p.propertyID === 'views');
            if (views) {
              views.visible = changeObject.propertyValue;
            }

          }
        }
      }

    };

  }

  private getViewChangeDataTypes(propData: any) {

    if (!propData || !propData.toolbarData || propData.toolbarData.length === 0) {
      return [];
    }
    const iterators = [];
    propData.toolbarData.forEach(data => {
      iterators.push(
        {
          key: data.type,
          value: data.title
        }
      );
    });
    return iterators;
  }

  private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
    const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

    return {
      categoryId: 'behavior',
      categoryName: '行为',
      properties: [
        visibleProp,
        {
          propertyID: 'isScrollSpyItem',
          propertyName: '被滚动跟随的节点',
          propertyType: 'boolean',
          description: '是否为被滚动监听跟随的节点',
          defaultValue: false
        }
      ]
    };
  }

  private getExtendPropConfig(): ElementPropertyConfig {

    return {
      categoryId: 'extend',
      categoryName: '扩展',
      properties: [
        {
          propertyID: 'extendedAreaTemplate',
          propertyName: '扩展区域模板',
          propertyType: 'modal',
          description: '扩展区域模板设置',
          editor: CodeEditorComponent,
          editorParams: {
            language: 'html'
          }
        },
        {
          propertyID: 'extendedAreaClass',
          propertyName: '扩展区域样式',
          propertyType: 'string',
          description: '扩展区域样式设置',
        },
        {
          propertyID: 'contentTemplateClass',
          propertyName: '内容区域样式',
          propertyType: 'string',
          description: '内容区域样式设置',
        }
      ]
    };
  }

}
