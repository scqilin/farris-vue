import { ContentContainerSchema } from '../schema/schema';
import { BuilderHTMLElement, FarrisDesignBaseComponent } from '@farris/designer-element';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerProp } from '../property/property-config';
import { ContainerContextMenuManager } from '../context-menu/context-menu.manager';
import { RowNode } from '@farris/ui-treetable';
import { ContainerDragDropManager } from '../drag-drop/dragAndDrapManager';
import { SplitLikeCardContainerContextMenuService } from '../context-menu/services/split-like-card-container';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeContainerProp } from '../property/nocode-property-config';
import { cloneDeep } from 'lodash-es';

export default class ContentContainerComponent extends FdContainerBaseComponent {

    /** 判断当前容器是否是固定的上下文的中间层级，这种容器不支持移动、不支持删除、并且隐藏间距 */
    private isInFixedContextRules = false;
    private dragManager: ContainerDragDropManager;

    constructor(component: any, options: any) {
        super(component, options);
        this.customToolbarConfigs = [];

        this.dragManager = new ContainerDragDropManager(this);
        this.isInFixedContextRules = this.dragManager.checkIsInFixedContextRules();
    }

    /**
     * 从工具箱拖拽生成时需要判断是何种类型的容器
     */
    static getMetadataInControlBox(targetComponentInstance: FarrisDesignBaseComponent, controlFeature: string) {
        const metadata = cloneDeep(ContentContainerSchema);

        // 区块容器，需要增加样式
        const feature = controlFeature ? JSON.parse(controlFeature) : null;
        if (feature && feature.isLikeCardContainer) {
            metadata.appearance = {
                class: 'f-struct-like-card',
                style: 'border-radius:16px;'
            };
            metadata.isLikeCardContainer = true;
        }

        return metadata;
    }
    init(): void {
        super.init();
        this.getCustomToolbarConfig();
    }

    getDefaultSchema(): any {

        return ContentContainerSchema;
    }

    getTemplateName(): string {
        return 'ContentContainer';
    }


    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {

        const serviceHost = this.options.designerHost;

        if (this.envType === DesignerEnvType.noCode) {
            const prop: NoCodeContainerProp = new NoCodeContainerProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;

        } else {
            const prop: ContainerProp = new ContainerProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        }
    }

    /**
     * 组装右键菜单
     * @param rowNode 组件在控件树上对应的行数据
     */
    resolveContextMenuConfig(rowNode: RowNode) {
        const menuManager = new ContainerContextMenuManager(this, rowNode);
        return menuManager.setContextMenuConfig();
    }

    /**
     * 判断在可视化区域中是否隐藏容器间距和线条
     */
    hideNestedPaddingInDesginerView() {

        if (this.isInFixedContextRules) {
            return true;
        }
        return false;

    }


    checkCanMoveComponent(): boolean {
        const canMove: boolean = super.checkCanMoveComponent();
        if (!canMove) {
            return false;
        }
        if (this.isInFixedContextRules) {
            return false;
        }
        return true;
    }

    checkCanDeleteComponent(): boolean {
        const canDelete: boolean = super.checkCanDeleteComponent();
        if (!canDelete) {
            return false;
        }
        if (this.isInFixedContextRules) {
            return false;
        }

        return true;
    }

    /**
     * 判断是否可以接收拖拽新增的子级控件
     * @param data 新控件的类型、所属分类
     * @returns boolean
     */
    canAccepts(el: BuilderHTMLElement, target: BuilderHTMLElement) {

        const result = super.canAccept(el, target);
        if (!result.canAccepts) {
            return false;
        }
        const resolveContext = result.resolveContext;

        return this.dragManager.canAccepts(resolveContext);
    }

    private getCustomToolbarConfig() {
        this.customToolbarConfigs = [];

        const injector = this.options.designerHost.getService('Injector');
        const serv = new SplitLikeCardContainerContextMenuService(injector, this);

        const show = serv.checkCanSplitLikeCardContainer();
        if (show) {
            this.customToolbarConfigs.push(
                {
                    id: 'splitLikeCardContentainer',
                    title: '拆分区块',
                    icon: 'f-icon f-icon-yxs_level',
                    click: (e) => {
                        e.stopPropagation();
                        this.splitLikeCardContainer();
                    }
                });
        }
    }

    private splitLikeCardContainer() {
        const injector = this.options.designerHost.getService('Injector');
        const refreshFormService = this.options.designerHost.getService('RefreshFormService');

        const serv = new SplitLikeCardContainerContextMenuService(injector, this);
        serv.splitLikeCardContainer();

        refreshFormService.refreshFormDesigner.next();
    }
}

