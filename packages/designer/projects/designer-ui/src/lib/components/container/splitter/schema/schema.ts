export const SplitterSchema = {
    id: 'splitter',
    type: 'Splitter',
    visible: true,
    appearance: null,
    orientation: 'horizontal',
    contents: []
};



export const SplitterPaneSchema = {
    id: 'splitter-pane',
    type: 'SplitterPane',
    visible: true,
    appearance: null,
    resizable: false,
    resizeHandlers: '',
    contents: []
};
