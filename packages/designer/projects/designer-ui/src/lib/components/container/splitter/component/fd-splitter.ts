import { SplitterSchema } from '../schema/schema';
import { forEach } from 'lodash-es';
import { SplitterProp } from '../property/property-config';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { SplitterContextMenuManager } from '../context-menu/context-menu.manager';
import { RowNode } from '@farris/ui-treetable';
import { BuilderHTMLElement } from '@farris/designer-element';
import { DgControl } from '../../../../utils/dg-control';
import FdSplitterPaneComponent from './fd-splitter-pane';

export default class FdSplitterComponent extends FdContainerBaseComponent {

    /** 标识每个分栏项的key值 */
    get splitterPaneKey(): string {
        return `pane-${this.id}`;
    }

    /** 分栏项实例 */
    splitterPaneComponents: FdSplitterPaneComponent[];

    constructor(component: any, options: any) {
        super(component, options);
    }

    getDefaultSchema(): any {
        return SplitterSchema;
    }

    getTemplateName(): string {
        return 'Splitter';
    }

    init(): void {
        this.components = [];
        this.splitterPaneComponents = [];
        const options = Object.assign({}, this.options, { parent: this });
        forEach(this.component.contents, (pane, index) => {
            pane.contents = pane.contents || [];
            const splitterPageComponent: any = this.createComponent(pane, options, null);

            this.splitterPaneComponents[index] = splitterPageComponent;
        });
    }

    render(): any {
        return super.render(this.renderTemplate('Splitter', {
            splitterPaneComponents: this.splitterPaneComponents.map(pane => pane.renderComponents()),
            splitterPaneKey: this.splitterPaneKey

        }));
    }



    attach(element: any): any {
        this.loadRefs(element, {
            [this.splitterPaneKey]: 'multiple',
        });
        const superAttach: any = super.attach(element);

        this.refs[this.splitterPaneKey].forEach((splitterPaneEle, index) => {
            this.attachComponents(splitterPaneEle, this.splitterPaneComponents[index].components, this.component.contents[index].contents);
            this.bindingScrollEvent(splitterPaneEle);
        });

        return superAttach;
    }

    /**
     * 组装属性面板配置数据
     * @returns ElementPropertyConfig[]
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: SplitterProp = new SplitterProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 组装右键菜单
     * @param rowNode 组件在控件树上对应的行数据
     */
    resolveContextMenuConfig(rowNode: RowNode, parentRowNode: RowNode) {
        const menuManager = new SplitterContextMenuManager(this, rowNode, parentRowNode);
        return menuManager.setContextMenuConfig();
    }

    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {
        // splitter 不接收子级节点；splitterPane可接收
        const targetId = targetElement.getAttribute('id');
        if (this.component.contents && this.component.contents.find(pane => pane.id === targetId)) {
            const paneCmp = this.splitterPaneComponents.find(c => c.id === targetId);
            return paneCmp.canAccepts(sourceElement, targetElement);
            // return super.canAccepts(sourceElement, targetElement);
        } else {
            return false;
        }
    }

    checkCanMoveComponent(): boolean {
        // 控件本身样式
        const cmpClass = this.component.appearance && this.component.appearance.class || '';
        const cmpClassList = cmpClass.split(' ');

        // 父级节点
        const parent = this.parent && this.parent.component;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];


        // 主区域下的Splitter不能移动
        if (cmpClassList.includes('f-page-content') && parentClassList.includes('f-page-main') && parent.type === DgControl.ContentContainer.type) {
            return false;
        }

        return true;
    }
    checkCanDeleteComponent(): boolean {
        return false;
    }

    /**
     * 判断在可视化区域中是否隐藏容器间距和线条
     */
    hideNestedPaddingInDesginerView() {
        return true;
    }
}
