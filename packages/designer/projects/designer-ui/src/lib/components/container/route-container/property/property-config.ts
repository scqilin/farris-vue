import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { ItemCollectionConverter, ItemCollectionEditorComponent } from '@farris/designer-devkit';
import { EditorTypes } from '@farris/ui-datagrid-editors';

export class RouteContainerProp extends ContainerUsualProp {


    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        // 获取可选的子组件
        const enumData = [];
        this.domService.components.map(c => {
            if (c.componentType !== 'Frame') {
                enumData.push({
                    id: c.id
                });
            }
        });
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'childrenRoutes',
                    propertyName: '子路由',
                    propertyType: 'modal',
                    editor: ItemCollectionEditorComponent,
                    editorParams: {
                        columns: [
                            {
                                field: 'childComponentId',
                                title: '子组件',
                                editor: {
                                    type: EditorTypes.SELECT,
                                    options: { valueField: 'id', textField: 'id', data: enumData }
                                }
                            },
                            {
                                field: 'routeUri',
                                title: '路由标识',
                                editor: {
                                    type: EditorTypes.TEXTBOX
                                }
                            },
                        ],
                        requiredFields: ['childComponentId', 'routeUri'],
                        uniqueFields: ['childComponentId', 'routeUri'],
                        modalTitle: '子路由编辑器'
                    },
                    converter: new ItemCollectionConverter()
                }
            ]
        };
    }
}
