import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { DgControl } from '../../../../utils/dg-control';
import { ContainerUsualProp } from '../../../container/common/property/container-property-config';

export class TabToolBarProp extends ContainerUsualProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // // 外观属性
        // const appearancePropConfig = this.getAppearancePropConfig(propertyData);
        // this.propertyConfig.push(appearancePropConfig);

        // // 行为属性
        // const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        // this.propertyConfig.push(behaviorPropConfig);

        return this.propertyConfig;

    }


    // private getAppearancePropConfig(propertyData: any): ElementPropertyConfig {
    //     const baseAppearanceInfo = this.getCommonAppearanceProperties();
    //     const buttonInfo = [
    //         {
    //             propertyID: 'position',
    //             propertyName: '位置',
    //             propertyType: 'select',
    //             iterator: [{ key: 'inHead', value: '标题区域' }, { key: 'inContent', value: '内容区域' }]
    //         }
    //     ];
    //     return {
    //         categoryId: 'appearance',
    //         categoryName: '外观',
    //         properties: [...baseAppearanceInfo, ...buttonInfo],
    //     };
    // }

    // private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

    //     return {
    //         categoryId: 'behavior',
    //         categoryName: '行为',
    //         properties: [
    //             {
    //                 propertyID: 'visible',
    //                 propertyName: '是否可见',
    //                 propertyType: 'unity',
    //                 description: '运行时组件是否可见',
    //                 editorParams: {
    //                     controlName: UniformEditorDataUtil.getControlName(propertyData),
    //                     constType: 'enum',
    //                     editorOptions: {
    //                         types: ['const', 'variable'],
    //                         enums: [{ key: true, value: 'true' }, { key: false, value: 'false' }],
    //                         variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
    //                         getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
    //                         newVariableType: 'Boolean',
    //                         newVariablePrefix: 'is'
    //                     }
    //                 }
    //             },
    //         ]
    //     };
    // }
}
