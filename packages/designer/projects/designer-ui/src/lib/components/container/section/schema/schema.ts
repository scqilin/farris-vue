export const SectionSchema = {
    id: 'section',
    type: 'Section',
    appearance: null,
    visible: true,
    mainTitle: '主标题',
    subTitle: '',
    headerClass: '',
    titleClass: '',
    extendedHeaderAreaClass: '',
    toolbarClass: '',
    extendedAreaClass: '',
    contentTemplateClass: '',
    fill: false,
    expanded: true,
    enableMaximize: false,
    enableAccordion: true,
    accordionMode: 'default',
    showHeader: true,
    headerTemplate: '',
    titleTemplate: '',
    extendedHeaderAreaTemplate: '',
    toolbarTemplate: '',
    extendedAreaTemplate: '',
    contents: [],
    isScrollSpyItem: false,
    toolbar: {
        type: 'SectionToolbar',
        position: 'inHead',
        contents: []
    }
};

export const SectionToolbarSchema = {
    type: 'SectionToolbar',
    position: 'inHead',
    contents: []
};

export const SectionToolbarItemSchema = {
    id: 'sectionToolbarItem',
    type: 'SectionToolbarItem',
    title: '按钮',
    disable: false,
    appearance: {
        class: 'btn btn-secondary f-btn-ml'
    },
    visible: true,
    click: null
};


