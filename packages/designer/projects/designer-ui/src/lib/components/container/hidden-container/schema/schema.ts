export const HiddenContainerSchema = {
    id: '',
    type: 'HiddenContainer',
    appearance: null,
    contents: [],
    controlsInline: true,
    formAutoIntl: true,
    draggable: false
};

