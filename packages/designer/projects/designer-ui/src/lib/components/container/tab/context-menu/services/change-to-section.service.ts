import { NotifyService } from '@farris/ui-notify';

import { cloneDeep } from 'lodash-es';
import { MessagerService } from '@farris/ui-messager';
import { ControlService } from '../../../../../service/control.service';
import { DomService, RefreshFormService } from '@farris/designer-services';
import { ComponentSchema } from '@farris/designer-element';
import { ControlContextMenuItem } from '../../../../../entity/control-context-menu';
import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { DgControl } from '../../../../../utils/dg-control';

const CHANGE_TO_SECTION_COMMAND = 'changeToSection';

/**
 * 标签页切换为分组面板
 */
export class TabChangeToSectionService {

    /** tab组件实例 */
    private cmpInstance: FarrisDesignBaseComponent;

    constructor(
        private notifyService: NotifyService,
        private controlService: ControlService,
        private msgService: MessagerService,
        private refreshFormService: RefreshFormService
    ) { }

    /**
     * 组装是否支持切换为分组面板Section的菜单
     * @param tabElement 当前标签页节点
     * @param menuConfig 右键菜单
     */
    assembleChangeToSectionMenu(cmp: FarrisDesignBaseComponent, menuConfig: ControlContextMenuItem[]) {
        this.cmpInstance = cmp;

        const tabElement = cmp.component;
        const sectionMenu = menuConfig.find(menu => menu.id === CHANGE_TO_SECTION_COMMAND);
        if (!sectionMenu) {
            return menuConfig;
        }

        const checkResult = this.checkCanChangeToSection(tabElement);
        if (checkResult) {
            menuConfig = menuConfig.filter(menu => menu.id !== CHANGE_TO_SECTION_COMMAND);
        }
        return menuConfig;

    }

    private checkCanChangeToSection(tabElement: ComponentSchema) {

        // 1、不包含tabPage，不允许切换
        if (!tabElement.contents || tabElement.contents.length === 0) {
            return '空标签页不支持切换分组面板';
        }
        // 2、标准模板规定container-section-tab 是成对出现三层结构，转换后的section结构也是container-section
        const sectionElement = this.cmpInstance.parent.component;
        if (!sectionElement || sectionElement.type !== DgControl.Section.type) {
            return '标签页的父级不是分组面板，不支持切换分组面板';
        }
        const containerElement = this.cmpInstance.parent.parent.component;
        if (!containerElement || containerElement.type !== DgControl.ContentContainer.type) {
            return '标签页的祖父级不是ConentContainer容器，不支持切换分组面板';
        }

        // 3、限制三个结构的class样式
        if (!tabElement.appearance || !tabElement.appearance.class || !tabElement.appearance.class.includes('f-component-tabs')) {
            return '标签页的class样式不包含f-component-tabs，不支持切换分组面板';
        }
        if (!sectionElement.appearance || !sectionElement.appearance.class || !sectionElement.appearance.class.includes('f-section-tabs')) {
            return '分组面板的class样式不包含f-section-tabs，不支持切换分组面板';
        }
        if (!containerElement.appearance || !containerElement.appearance.class || !containerElement.appearance.class.includes('f-struct-wrapper')) {
            return 'ContentContainer容器的class样式不包含f-struct-wrapper，不支持切换分组面板';
        }

        // 双列表标签页模板中各层级有特定的class填充样式，若在标签页和分组之间来回切换，会造成样式丢失。
        const domService = this.cmpInstance.options.designerHost.getService('DomService') as DomService;
        if (domService && domService.module && domService.module.templateId === 'double-list-in-tab-template') {
            return '双列表标签页模板，不支持切换分组面板';
        }

        return;
    }

    /**
     * Tab切换为Section
     * Contaienr-Section-Tab-TabPage-ComponentRef 切换为多个[Container-Section-ComponentRef]
     */
    changeToSection() {
        const tabElement = this.cmpInstance.component;

        this.msgService.question('标签页切换为分组面板后，将仅保留标题、按钮属性，确定切换？', () => {

            const originSectionElement = this.cmpInstance.parent.component;
            const originContainerElement = this.cmpInstance.parent.parent.component;
            const originContainerParentElement = this.cmpInstance.parent.parent.parent.component;

            if (!originContainerParentElement) {
                this.notifyService.warning('无法定位容器父节点');
                return;
            }

            // 组装container-section-component 三层结构
            const newContainerList = [];
            tabElement.contents.forEach(tabPage => {
                const title = tabPage.title;
                const idPostfix = this.getControlIdPostfix(tabPage);

                const container = cloneDeep(originContainerElement);
                container.id = container.id + '-' + idPostfix;

                const section = cloneDeep(originSectionElement);
                section.id = section.id + '-' + idPostfix;
                // 配置页头
                section.showHeader = true;
                section.mainTitle = title;
                section.enableMaximize = false;
                section.enableAccordion = false;
                section.appearance.class = section.appearance.class.replace('f-section-tabs', '');
                section.multiViews = tabPage.multiViews;
                section.views = tabPage.views;

                // 配置内容
                section.contents = tabPage.contents;

                // 配置按钮
                if (tabPage.toolbar && tabPage.toolbar.contents && tabPage.toolbar.contents.length) {
                    section.toolbar = this.controlService.getControlMetaData(DgControl.SectionToolbar.type);
                    section.toolbar.position = tabPage.toolbar.position;

                    tabPage.toolbar.contents.forEach(tabToolbarItem => {
                        tabToolbarItem.type = DgControl.SectionToolbarItem.type;
                        section.toolbar.contents.push(tabToolbarItem);
                    });
                }

                container.contents = [section];
                newContainerList.push(container);
            });


            const containerIndex = originContainerParentElement.contents.findIndex(c => c.id === originContainerElement.id);
            originContainerParentElement.contents.splice(containerIndex, 1, ...newContainerList);


            this.notifyService.success('切换成功');
            this.cmpInstance.emit('clearPropertyPanel');
            this.refreshFormService.refreshFormDesigner.next();
        });



    }


    /**
     * 根据tab的上下文获取新建控件的id后缀
     * @param sectionElement section元素
     */
    private getControlIdPostfix(tabPageElement: any) {
        let idPostfix = Math.random().toString(36).slice(2, 6);
        const componentRef = tabPageElement.contents.find(c => c.type === DgControl.ComponentRef.type);
        if (componentRef) {
            idPostfix = componentRef.id.replace('-component-ref', '');
        }
        const multiViewContainer = tabPageElement.contents.find(c => c.type === DgControl.MultiViewContainer.type);
        if (multiViewContainer) {
            idPostfix = multiViewContainer.dataSource + '-' + idPostfix;
        }

        return idPostfix;
    }
}
