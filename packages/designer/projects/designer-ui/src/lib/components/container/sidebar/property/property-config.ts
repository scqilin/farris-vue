import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { CodeEditorComponent, CollectionWithPropertyConverter, CollectionWithPropertyEditorComponent } from '@farris/designer-devkit';
import { DgControl } from '../../.././../utils/dg-control';
import { SidebarToolbarItemProp } from './sidebar-toolbar-item.config';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class SidebarProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 入口属性
        const entryPropConfig = this.getEntryPropConfig(propertyData);
        propertyConfig.push(entryPropConfig);

        // 页头属性
        const headerPropConfig = this.getHeaderPropConfig(propertyData);
        propertyConfig.push(headerPropConfig);

        // 页脚属性
        const footerPropConfig = this.getFooterPropConfig(propertyData);
        propertyConfig.push(footerPropConfig);


        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }



    private getAppearancePropConfig(propertyData: any): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        commonProps.push(
            {
                propertyID: 'position',
                propertyName: '显示位置',
                propertyType: 'select',
                iterator: [{ key: 'left', value: '左侧' }, { key: 'right', value: '右侧' }]
            },
            {
                propertyID: 'width',
                propertyName: '宽度',
                propertyType: 'number'
            },
            {
                propertyID: 'contentTemplateClass',
                propertyName: '内容模板样式',
                propertyType: 'string',
                description: '内容模板样式设置',
            },
            {
                propertyID: 'maskable',
                propertyName: '显示遮罩',
                propertyType: 'boolean',
                description: '是否显示遮罩',
                defaultValue: false
            },
            {
                propertyID: 'maskClosable',
                propertyName: '点击遮罩层关闭侧边栏',
                propertyType: 'boolean',
                description: '点击遮罩层是否可关闭侧边栏',
                visible: propertyData.maskable
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps,
            setPropertyRelates(changeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'maskable': {
                        const maskClosable = this.properties.find(p => p.propertyID === 'maskClosable');
                        if (maskClosable) {
                            maskClosable.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                }
            }
        };
    }

    private getEntryPropConfig(propertyData: any): ElementPropertyConfig {
        return {
            categoryId: 'enty',
            categoryName: '入口区域',
            properties: [
                {
                    propertyID: 'showEntry',
                    propertyName: '显示入口区域',
                    propertyType: 'boolean',
                    description: '是否显示入口区域',
                    defaultValue: false
                },
                {
                    propertyID: 'customEntryTemplate',
                    propertyName: '自定义入口模板',
                    propertyType: 'modal',
                    description: '自定义入口模板设置',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    },
                    visible: propertyData.showEntry,
                    category: 'entry'
                },
                {
                    propertyID: 'customEntryTemplateClass',
                    propertyName: '自定义入口模板样式',
                    propertyType: 'string',
                    description: '自定义入口模板样式设置',
                    visible: propertyData.showEntry,
                    category: 'entry'
                }
            ],
            setPropertyRelates(changeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'showEntry': {
                        this.properties.map(p => {
                            if (p.category === 'entry') {
                                p.visible = changeObject.propertyValue;
                            }
                        });
                        break;
                    }
                }
            }

        };
    }

    private getHeaderPropConfig(propertyData: any): ElementPropertyConfig {
        const sidebarToolbarItemProp = new SidebarToolbarItemProp(this.formBasicService, this.serviceHost);

        const headerProps: PropertyEntity[] = [
            {
                propertyID: 'showHeader',
                propertyName: '显示页头',
                propertyType: 'boolean',
                description: '是否显示页头',
                defaultValue: true
            },
            {
                propertyID: 'showClose',
                propertyName: '显示关闭按钮',
                propertyType: 'boolean',
                description: '是否显示关闭按钮'
            },
            {
                propertyID: 'toolbar',
                propertyName: '工具栏',
                propertyType: 'cascade',
                cascadeConfig: [
                    {
                        propertyID: 'items',
                        propertyName: '工具栏项',
                        propertyType: 'modal',
                        description: '工具栏项设置',
                        editor: CollectionWithPropertyEditorComponent,
                        converter: new CollectionWithPropertyConverter(),
                        editorParams: {
                            modalTitle: '工具栏编辑器',
                            viewModelId: this.viewModelId,
                            idKey: 'id',
                            textKey: 'text',
                            childrenKey: 'items',
                            controlType: DgControl.ToolBarItem.type,
                            defaultControlValue: {
                                id: 'toolBarItem',
                                text: '按钮'
                            },
                            parentNodeId: propertyData.id,
                            getPropertyConfig: (selectedNode, triggerModalSave: any) => sidebarToolbarItemProp.getPropertyConfig(this.viewModelId, selectedNode, triggerModalSave)
                        }
                    }
                ]
            },
            {
                propertyID: 'toolbarTemplateClass',
                propertyName: '工具栏样式',
                propertyType: 'string',
                description: '工具栏样式设置'
            },
            {
                propertyID: 'headerTemplate',
                propertyName: '页头模板',
                propertyType: 'modal',
                description: '页头模板设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
            {
                propertyID: 'headerTemplateClass',
                propertyName: '页头模板样式',
                propertyType: 'string',
                description: '页头模板样式设置'
            },
            {
                propertyID: 'headerTitleTemplate',
                propertyName: '页头标题模板',
                propertyType: 'modal',
                description: '页头标题模板设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
            {
                propertyID: 'headerTitleTemplateClass',
                propertyName: '页头标题模板样式',
                propertyType: 'string',
                description: '页头标题模板样式设置'
            },
            {
                propertyID: 'headerContentTemplate',
                propertyName: '页头内容模板',
                propertyType: 'modal',
                description: '页头内容模板设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
            {
                propertyID: 'headerContentTemplateClass',
                propertyName: '页头内容模板样式',
                propertyType: 'string',
                description: '页头内容模板样式设置'
            }
        ];

        headerProps.map(p => {
            if (p.propertyID !== 'showHeader') {
                p.visible = propertyData.showHeader;
            }
        });

        return {
            categoryId: 'header',
            categoryName: '页头区域',
            properties: headerProps,
            setPropertyRelates(changeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'showHeader': {
                        this.properties.map(p => {
                            if (p.propertyID !== 'showHeader') {
                                p.visible = changeObject.propertyValue;
                            }
                        });
                        break;
                    }
                }
            }

        };
    }

    private getFooterPropConfig(propertyData: any): ElementPropertyConfig {
        const footerProps: PropertyEntity[] = [
            {
                propertyID: 'showFooter',
                propertyName: '显示页脚',
                propertyType: 'boolean',
                description: '是否显示页脚',
                defaultValue: false
            },
            {
                propertyID: 'footerContentTemplate',
                propertyName: '页脚内容模板',
                propertyType: 'modal',
                description: '页脚内容模板设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
            {
                propertyID: 'footerContentTemplateClass',
                propertyName: '页脚内容模板样式',
                propertyType: 'string',
                description: '页脚内容模板样式设置'
            }
        ];

        footerProps.map(p => {
            if (p.propertyID !== 'showFooter') {
                p.visible = propertyData.showFooter;
            }
        });

        return {
            categoryId: 'footer',
            categoryName: '页脚区域',
            properties: footerProps,
            setPropertyRelates(changeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'showFooter': {
                        this.properties.map(p => {
                            if (p.propertyID !== 'showFooter') {
                                p.visible = changeObject.propertyValue;
                            }
                        });
                        break;
                    }
                }
            }

        };
    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [visibleProp]
        };
    }


    private getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        let eventList = [
            {
                label: 'beforeCloseSidebar',
                name: '侧边栏关闭前事件'
            },
            {
                label: 'changeState',
                name: '侧边栏状态切换后事件'
            },
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
            }
        };
    }
}
