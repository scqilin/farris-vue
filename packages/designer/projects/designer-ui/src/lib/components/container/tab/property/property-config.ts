
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { DgControl } from '../../../../utils/dg-control';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class TabProp extends ContainerUsualProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }


    getBasicPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '组件的id',
                    readonly: true
                },
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件的类型',
                    iterator: [{ key: propertyData.type, value: DgControl[propertyData.type].name }],
                    readonly: true
                },
                {
                    propertyID: 'controlSource',
                    propertyName: '控件来源',
                    propertyType: 'select',
                    description: '组件的控件来源',
                    iterator: [{ key: 'Farris', value: 'Farris' }, { key: 'Kendo', value: 'Kendo' }],
                    refreshPanelAfterChanged: true,
                    visible: propertyData.controlSource !== 'Farris'
                }
            ],

            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                switch (changeObject && changeObject.propertyID) {
                    case 'controlSource': {
                        // 变更【内容】的显隐
                        const contentConfig = self.propertyConfig.find(cat => cat.categoryId === 'content');
                        if (contentConfig) { contentConfig.hide = changeObject.propertyValue !== 'Farris'; }
                    }
                }
            }
        };
    }

    private getAppearancePropConfig(): ElementPropertyConfig {
        const props = this.getCommonAppearanceProperties();

        props.push(
            {
                propertyID: 'size',
                propertyName: '尺寸',
                propertyType: 'cascade',
                cascadeConfig: [
                    {
                        propertyID: 'width',
                        propertyName: '宽度（px）',
                        propertyType: 'number',
                        description: '组件的宽度',
                        min: 0,
                        decimals: 0
                    },
                    {
                        propertyID: 'height',
                        propertyName: '高度（px）',
                        propertyType: 'number',
                        description: '组件的高度',
                        min: 0,
                        decimals: 0
                    }
                ]
            },
            {
                propertyID: 'titleWidth',
                propertyName: '标题区域宽度（%）',
                propertyType: 'number',
                description: '标题区域宽度占页面比',
                decimals: 0,
                min: 0,
                max: 100
            },
            {
                propertyID: 'autoTitleWidth',
                propertyName: '标题自适应宽度',
                propertyType: 'select',
                iterator: [
                  {key: true, value: '是'},
                  {key: false, value: '否'}
                ],
                description: '开启，则显示全部字符；关闭，最多显示7个字符',
                defaultValue: false
            },
            {
                propertyID: 'contentFill',
                propertyName: '填充',
                propertyType: 'select',
                iterator: [
                  {key: true, value: '是'},
                  {key: false, value: '否'}
                ],
                description: 'flex布局下，填充满剩余部分',
                defaultValue: false
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: props
        };
    }


    private getCurrentTabPageKeyValueMap(propertyData: any) {
        const contents = propertyData.contents || [];
        const keyMaps = [];
        contents.forEach(tabPage => {
            keyMaps.push({
                key: tabPage.id,
                value: tabPage.title
            });
        });
        return keyMaps;

    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        const tabPageKeyValueMap = this.getCurrentTabPageKeyValueMap(propertyData);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'selected',
                    propertyName: '默认选中标签页',
                    propertyType: 'unity',
                    description: '运行时默认选中的标签页',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: tabPageKeyValueMap,
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    }
                }
            ]
        };
    }


    // 事件编辑器集成
    private getEventPropertyConfig(propertyData: any, viewModelId: string) {
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'beforeSelect',
                name: '标签页选中前事件'
            },
            {
                label: 'tabChange',
                name: '切换标签页事件'
            },
            {
                label: 'tabRemove',
                name: '移除标签页事件'
            }
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService,formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService,domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService,formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
            }
        };
    }
}

