export const ResponseLayoutSchema = {
    id: 'responseLayout',
    type: 'ResponseLayout',
    appearance: null,
    visible: true,
    contents: []
};

export const ResponseLayoutItemSchema = {
    id: 'responseLayoutItem',
    type: 'ResponseLayoutItem',
    appearance: {
        class: 'h-100'
    },
    visible: true,
    contents: [],
    // hasInputControl: false
};


