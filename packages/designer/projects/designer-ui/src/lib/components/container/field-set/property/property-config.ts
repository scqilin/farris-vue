import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class FieldSetProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }


    private getAppearancePropConfig(propertyData: any): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        commonProps.push(
            {
                propertyID: 'title',
                propertyName: '标签',
                propertyType: 'string',
                description: '组件的标签'
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'title': {
                        changeObject.needUpdateControlTreeNodeName = true;
                        break;
                    }
                }
            }
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'collapse',
                    propertyName: '默认收折',
                    propertyType: 'boolean',
                    description: '运行时是否默认收折分组区域'
                },
                {
                    propertyID: 'expandText',
                    propertyName: '展开文本',
                    propertyType: 'string',
                    description: '展开文本设置'
                },
                {
                    propertyID: 'collapseText',
                    propertyName: '折起文本',
                    propertyType: 'string',
                    description: '折起文本设置'
                },
                {
                    propertyID: 'isScrollSpyItem',
                    propertyName: '被滚动跟随的节点',
                    propertyType: 'boolean',
                    description: '是否为被滚动监听跟随的节点',
                    defaultValue: false
                },
                {
                    propertyID: 'sectionCollapseVisible',
                    propertyName: '分组面板收折时显示',
                    propertyType: 'boolean',
                    description: '收折分组面时是否显示当前分组内的控件'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop: any, parameters?) {
                switch (changeObject.propertyID) {

                    case 'sectionCollapseVisible': {
                        const childControls = propertyData.contents || [];
                        childControls.forEach((control: any) => {
                            if (!control.appearance) {
                                control.appearance = { class: '' };
                            }
                            // 启用：子级控件的样式都追加f-state-visible
                            if (changeObject.propertyValue) {
                                if (!control.appearance.class || !control.appearance.class.includes('f-state-visible')) {
                                    control.appearance.class = (control.appearance.class || '') + ' f-state-visible';
                                }
                            } else {
                                // 关闭：子级控件的样式都移除f-state-visible
                                if (control.appearance.class && control.appearance.class.includes('f-state-visible')) {
                                    control.appearance.class = control.appearance.class.replace('f-state-visible', '').trim();
                                }
                            }

                        });
                    }
                }
            }
        };
    }
}
