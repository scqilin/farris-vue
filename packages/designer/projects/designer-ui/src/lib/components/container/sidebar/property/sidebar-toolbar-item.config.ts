import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { TreeNode } from '@farris/ui-treetable';
import { EventEditorService, DomService, FormBasicService, WebCmdService, StateMachineService, UniformEditorDataUtil } from '@farris/designer-services';
import { CodeEditorComponent, IconSelectEditorComponent } from '@farris/designer-devkit';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { MessagerService } from '@farris/ui-messager';
import { IDesignerHost } from '@farris/designer-element';


export class SidebarToolbarItemProp {

    /** 在弹窗中使用按钮配置时，交互面板若要跳转到代码视图，需要先关闭弹窗 */
    triggerModalSave?: any;

    private stateMachineService: StateMachineService;
    private eventEditorService: EventEditorService;
    public webCmdService: WebCmdService;
    private domService: DomService;
    private msgService: MessagerService;

    constructor(public formBasicService: FormBasicService, serviceHost: IDesignerHost) {

        this.stateMachineService = serviceHost.getService('StateMachineService') as StateMachineService;
        this.eventEditorService = serviceHost.getService('EventEditorService') as EventEditorService;
        this.webCmdService = serviceHost.getService('WebCmdService') as WebCmdService;
        this.domService = serviceHost.getService('DomService') as DomService;
        this.msgService = serviceHost.getService('MessagerService') as MessagerService;
    }

    /**
     * sidebar toolbarItem 属性配置
     * @param viewModelId 按钮所在视图模型ID
     * @param selectedNode 当前选中行数据
     */
    public getPropertyConfig(viewModelId: string, selectedNode: TreeNode, triggerModalSave?: any)
        : ElementPropertyConfig[] {

        this.triggerModalSave = triggerModalSave;

        const propertyData = selectedNode.data;
        const propertyConfig: ElementPropertyConfig[] = [];

        const isFirstLevel = !!!selectedNode.parent;

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig();
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearanceItemPropConfig(propertyData, false, isFirstLevel);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorItemPropConfig(propertyData, viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }

    private getBasicPropConfig(): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '工具栏按钮的标识'
                },
                {
                    propertyID: 'text',
                    propertyName: '标签',
                    propertyType: 'string',
                    description: '工具栏按钮的标签',
                    readonly: false
                }
            ]
        };
    }

    private getAppearanceItemPropConfig(propertyData: any, disableChildItem: boolean, isFirstLevel: boolean): ElementPropertyConfig {

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: [
                {
                    propertyID: 'appearance',
                    propertyName: '样式',
                    propertyType: 'cascade',
                    cascadeConfig: [
                        {
                            propertyID: 'class',
                            propertyName: '按钮样式',
                            propertyType: 'string',
                            description: '工具栏按钮样式'
                        },
                        {
                            propertyID: 'dropdownCls',
                            propertyName: '下拉框样式',
                            propertyType: 'string',
                            description: '工具栏按钮下拉框样式',
                            visible: !disableChildItem
                        }
                    ]
                },
                {
                    propertyID: 'split',
                    propertyName: '下拉按钮分离',
                    propertyType: 'boolean',
                    defaultValue: false,
                    description: '工具栏下拉按钮是否分离',
                    visible: !disableChildItem
                },
                {
                    propertyID: 'icon',
                    propertyName: '图标',
                    propertyType: 'modal',
                    description: '图标设置',
                    editor: IconSelectEditorComponent,
                    editorParams: { needIconClass: true },
                    visible: isFirstLevel
                },
                {
                    propertyID: 'tipsEnable',
                    propertyName: '启用提示',
                    propertyType: 'boolean',
                    description: '是否启用提示信息',
                    defaultValue: false
                },
                {
                    propertyID: 'tipsText',
                    propertyName: '提示消息内容',
                    propertyType: 'modal',
                    description: '提示消息内容设置',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    },
                    visible: propertyData.tipsEnable
                }
            ],
            setPropertyRelates(changeObject: PropertyChangeObject, data, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'tipsEnable': {
                        const tipsText = this.properties.find(p => p.propertyID === 'tipsText');
                        if (tipsText) {
                            tipsText.visible = changeObject.propertyValue;
                        }
                    }
                }
            }
        };
    }

    private getBehaviorItemPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                {
                    propertyID: 'visible',
                    propertyName: '是否可见',
                    propertyType: 'unity',
                    description: '运行时组件是否可见',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        }
                    }
                },
                {
                    propertyID: 'disable',
                    propertyName: '是否禁用',
                    propertyType: 'unity',
                    description: '运行时组件是否禁用',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'stateMachine'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            stateMachine: this.stateMachineService.stateMachineMetaData
                        }
                    }
                }
            ]
        };
    }


    private getEventPropertyConfig(propertyData, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            },
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject, data, parameters) {
                delete propertyData[viewModelId];

                // 若需要跳转到代码视图，首先要求用户将当前弹出窗口关闭
                if (parameters.isAddControllerMethod && self.triggerModalSave) {
                    self.msgService.question('确定关闭当前编辑器并跳转到代码视图吗？', () => {
                        self.triggerModalSave();

                        EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                        this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
                    });
                } else {
                    EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                    this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
                }


            }
        };
    }


}

