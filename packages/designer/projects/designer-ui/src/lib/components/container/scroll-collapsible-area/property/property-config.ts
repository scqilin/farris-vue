import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { ContainerUsualProp } from '../../common/property/container-property-config';

export class ScrollCollapsibleAreaProp extends ContainerUsualProp {
  getPropConfig(propertyData: any): ElementPropertyConfig[] {
    const propertyConfig = [];

    // 基本信息属性
    const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
    propertyConfig.push(basicPropConfig);

    // 行为属性
    const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
    propertyConfig.push(behaviorPropConfig);

    return propertyConfig;
  }
  private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
    const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

    return {
      categoryId: 'behavior',
      categoryName: '行为',
      properties: [
        visibleProp,
        {
          propertyID: 'expanded',
          propertyName: '是否展开',
          propertyType: 'unity',
          description: '运行时组件是否默认展开',
          editorParams: {
            controlName: UniformEditorDataUtil.getControlName(propertyData),
            constType: 'enum',
            editorOptions: {
              types: ['const', 'custom', 'variable'],
              enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
              variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
              getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
              newVariableType: 'Boolean',
              newVariablePrefix: 'is'
            }
          }
        },
        {
          propertyID: 'contentTemplate',
          propertyName: '内容区域模板',
          propertyType: 'modal',
          editor: CodeEditorComponent,
          editorParams: {
            language: 'html'
          },
          description: '配置内容区域模板后，将隐藏子级控件。'
        }
      ]
    };
  }
}
