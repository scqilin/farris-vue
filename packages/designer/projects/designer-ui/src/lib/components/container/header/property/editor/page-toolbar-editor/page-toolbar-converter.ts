import { TypeConverter } from '@farris/ide-property-panel';
import { DomService } from '@farris/designer-services';

export class PageToolbarConverter implements TypeConverter {

    controlId: string;
    showType: string;
    constructor(private domService: DomService, controlId: string, showType: string) {
        this.controlId = controlId;
        this.showType = showType;
    }
    convertTo(data): string {
        const { toolbar } = this.domService.module;
        if (!toolbar || !toolbar.items || !toolbar.configs || !toolbar.configs[this.showType]) {
            return;
        }

        const currentButtonConfig = toolbar.configs[this.showType][this.controlId];
        if (!currentButtonConfig) {
            return;
        }
        return '共' + currentButtonConfig.length + '项';

    }
}
