import { Injectable } from '@angular/core';
import { DgControl } from '../../../../../../utils/dg-control';
import { FormResponseLayoutContext } from '../../../../../input/common/property/response-layout-editor';
import { ResponseLayoutEditorService } from '../../../../../input/common/property/response-layout-editor';
import { FormUnifiedColumnLayout } from './form-layout-setting.component';
import { DomService } from '@farris/designer-services';

export class FormLayoutSettingService {

    constructor(
        public domService: DomService,
        public componentId: string) {

    }

    /**
     * 组装每种屏幕下的宽度样式值，例如col-md-6，则uniqueColClassInMD为6
     */
    assembleUnifiedLayoutContext(propertyData: any): { result: boolean, parameters: FormUnifiedColumnLayout } {

        const formNode = this.domService.domDgMap.get(propertyData.id);

        const responseLayoutService = new ResponseLayoutEditorService(this.domService, this.componentId);
        const responseLayoutConfig: FormResponseLayoutContext[] = [];
        responseLayoutService.getFormResonseLayoutConfig(formNode, responseLayoutConfig, 1);


        // 收集每种屏幕下的列数
        const columnInSMArray = responseLayoutConfig.map(config => config.columnInSM);
        const columnInMDArray = responseLayoutConfig.map(config => config.columnInMD);
        const columnInLGArray = responseLayoutConfig.map(config => config.columnInLG);
        const columnInELArray = responseLayoutConfig.map(config => config.columnInEL);

        // 只有每个控件的宽度都一样时，才认为form上有统一宽度，否则认为是自定义的控件宽度，此处传递null
        const uniqueColClassInSM = this.checkIsUniqueColumn(columnInSMArray) ? columnInSMArray[0] : null;
        const uniqueColClassInMD = this.checkIsUniqueColumn(columnInMDArray) ? columnInMDArray[0] : null;
        const uniqueColClassInLG = this.checkIsUniqueColumn(columnInLGArray) ? columnInLGArray[0] : null;
        const uniqueColClassInEL = this.checkIsUniqueColumn(columnInELArray) ? columnInELArray[0] : null;


        return {
            result: true,
            parameters: {
                uniqueColClassInSM,
                uniqueColClassInMD,
                uniqueColClassInLG,
                uniqueColClassInEL
            }
        };
    }

    /**
     * 校验宽度样式值是否一致
     */
    private checkIsUniqueColumn(columnsInScreen: number[]) {
        const keySet = new Set(columnsInScreen);
        const exclusiveKeys = Array.from(keySet);

        if (exclusiveKeys.length === 1) {
            return true;
        }
        return false;
    }

    /**
     * 根据统一配置值，修改卡片区域内所有控件的class样式
     * @param formNode dom节点
     * @param unifiedLayout 统一配置值
     */
    changeFormControlsByUnifiedLayoutConfig(formNode: any, unifiedLayout: FormUnifiedColumnLayout) {

        formNode.contents.forEach(control => {
            if (control.type === DgControl.FieldSet.type) {
                this.changeFormControlsByUnifiedLayoutConfig(control, unifiedLayout);
                return;
            }
            if (!control.appearance) {
                control.appearance = {};
            }
            const controlClass = control.appearance.class;
            control.appearance.class = this.changeControlClassInByColumn(controlClass, unifiedLayout);

        });
    }

    /**
     * 根据统一配置值，修改某一个控件的class样式
     */
    private changeControlClassInByColumn(controlClass: string, unifiedLayout: FormUnifiedColumnLayout) {
        let originColClass;
        let originColMDClass;
        let originColXLClass;
        let originColELClass;

        let otherClassItems = [];

        if (controlClass) {
            const controlClassArray = controlClass.split(' ');
            const colClassItems = controlClassArray.filter(classItem => classItem.startsWith('col-'));

            originColClass = colClassItems.find(item => /^col-([1-9]|10|11|12)$/.test(item));
            originColMDClass = colClassItems.find(item => /^col-md-([1-9]|10|11|12)$/.test(item));
            originColXLClass = colClassItems.find(item => /^col-xl-([1-9]|10|11|12)$/.test(item));
            originColELClass = colClassItems.find(item => /^col-el-([1-9]|10|11|12)$/.test(item));

            otherClassItems = controlClassArray.filter(classItem => !classItem.startsWith('col-'));
        }



        const colClass = unifiedLayout.uniqueColClassInSM ? 'col-' + unifiedLayout.uniqueColClassInSM : originColClass;
        const colMDClass = unifiedLayout.uniqueColClassInMD ? 'col-md-' + unifiedLayout.uniqueColClassInMD : originColMDClass;
        const colXLClass = unifiedLayout.uniqueColClassInLG ? 'col-xl-' + unifiedLayout.uniqueColClassInLG : originColXLClass;
        const colELClass = unifiedLayout.uniqueColClassInEL ? 'col-el-' + unifiedLayout.uniqueColClassInEL : originColELClass;

        const newClassItems = [colClass, colMDClass, colXLClass, colELClass].concat(otherClassItems);

        return newClassItems.join(' ');
    }
}
