import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';

export class HeaderToolBarProp extends ContainerUsualProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);


        const appearance = this.getToolbarAppearanceConfig();
        this.propertyConfig.push(appearance);

        return this.propertyConfig;

    }

    /**
     * 将header上的工具栏相关属性放到toolbar上
     */
    private getToolbarAppearanceConfig() {
        return {
            categoryId: 'toolbarPropsOnHeader',
            categoryName: '外观',
            properties: [
                {
                    propertyID: 'toolbarCls',
                    propertyName: '工具栏样式',
                    propertyType: 'string'
                },
                // {
                //     propertyID: 'toolbarDatas',
                //     propertyName: '按钮组',
                //     propertyType: 'modal',
                //     editor: PageToolbarEditorComponent,
                //     editorParams: { controlId: propertyData.id, viewModelId },
                //     converter: new PageToolbarConverter(this.domService, propertyData.id, showType)
                // },
                {
                    propertyID: 'toolbarBtnSize',
                    propertyName: '按钮尺寸',
                    propertyType: 'select',
                    iterator: [
                        { key: 'default', value: '标准' },
                        { key: 'lg', value: '大号' }
                    ]
                },
                {
                    propertyID: 'toolbarPopDirection',
                    propertyName: '弹出方向',
                    propertyType: 'select',
                    iterator: [
                        { key: 'default', value: '自动' },
                        { key: 'top', value: '向上' },
                        { key: 'bottom', value: '向下' }
                    ]
                }
            ]
        };
    }
}
