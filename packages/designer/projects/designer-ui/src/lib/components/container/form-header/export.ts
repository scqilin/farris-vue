import { ComponentExportEntity } from '@farris/designer-element';
import FdFormHeaderComponent from './component/fd-form-header';
import { FormHeaderSchema } from './schema/schema';

import FdFormHeaderTemplates from './templates';

/**
 * 页面表头翻页图标。目前已废弃
 */
export const FormHeader: ComponentExportEntity = {
    type: 'FormHeader',
    component: FdFormHeaderComponent,
    template: FdFormHeaderTemplates,
    metadata: FormHeaderSchema
};
