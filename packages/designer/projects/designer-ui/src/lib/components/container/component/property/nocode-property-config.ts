import { ContainerUsualProp } from '../../common/property/container-property-config';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { cloneDeep, set } from 'lodash-es';

export class NoComponentProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;
    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const props = this.getCommonAppearanceProperties();

        const propData = cloneDeep(propertyData);
        // 【模型名称】是保存到ViewModel上的，不是保存到Component上，所以这里复制一份属性值
        const vm = this.domService.getViewModelById(viewModelId);
        propData.name = vm ? vm.name : null;

        props.push(
            {
                propertyID: 'name',
                propertyName: '组件名称',
                propertyType: 'string'
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            propertyData: propData,
            enableCascade: true,
            properties: props,
            setPropertyRelates(changeObject: FormPropertyChangeObject) {

                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'name': {
                        self.syncChangesToViewModel(viewModelId, { name: changeObject.propertyValue });
                        break;
                    }

                    default: {
                        // 将属性值同步到Component属性上
                        const propPath = changeObject.parentPropertyID ? changeObject.parentPropertyID + '.' + [changeObject.propertyID] : [changeObject.propertyID];
                        set(propertyData, propPath, changeObject.propertyValue);
                        break;
                    }
                }
            }
        };
    }


    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const propData = cloneDeep(propertyData);
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        // 是否启用校验是保存到ViewModel上的，不是保存到Component上，所以这里复制一份属性值
        const vm = this.domService.getViewModelById(viewModelId);
        propData.enableValidation = vm ? vm.enableValidation : null;

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            propertyData: propData,
            enableCascade: true,
            properties: [
                visibleProp
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject) {

                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'visible': {
                        // 将属性值同步到Component属性上
                        propertyData.visible = changeObject.propertyValue;
                        break;
                    }
                }
            }
        };
    }

    /**
     * 将变更的属性值同步到ViewModel上
     */
    private syncChangesToViewModel(viewModelId: string, changeObject: any) {
        const vm = this.domService.getViewModelById(viewModelId);
        if (!vm) {
            return;
        }
        Object.assign(vm, changeObject)
    }
}
