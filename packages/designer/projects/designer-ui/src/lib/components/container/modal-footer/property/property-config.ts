import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { PageToolbarEditorComponent } from '../../header/property/editor/page-toolbar-editor/page-toolbar-editor.component';
import { PageToolbarConverter } from '../../header/property/editor/page-toolbar-editor/page-toolbar-converter';

export class ModalFooterProp extends ContainerUsualProp {


    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];


        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 按钮属性
        // const toolbarPropConfig = this.getToolbarPropConfig(propertyData,  this.viewModelId);
        // propertyConfig.push(toolbarPropConfig);

        // 自定义模板属性
        const customPropConfig = this.getCustomTemplatePropConfig(propertyData,  this.viewModelId);
        propertyConfig.push(customPropConfig);

        return propertyConfig;
    }

    private getAppearancePropConfig(): ElementPropertyConfig {

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: this.getCommonAppearanceProperties()
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [visibleProp],
        };
    }

    private getToolbarPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        /** 展示模式：弹出类modal/独立页签类page/侧边滑出类slide 。
         * 优先取控件所属组件的展示类型，若没有配置，则取当前表单的展示类型
         */
        let showType = this.domService.module.showType;
        const comp = this.domService.getComponentByVMId(viewModelId);
        if (comp && comp.showType) {
            showType = comp.showType;
        }
        return {
            categoryId: 'toolbar',
            categoryName: '工具栏',
            properties: [
                {
                    propertyID: 'toolbarCls',
                    propertyName: '工具栏样式',
                    propertyType: 'string'
                },
                {
                    propertyID: 'toolbarDatas',
                    propertyName: '按钮组',
                    propertyType: 'modal',
                    editor: PageToolbarEditorComponent,
                    editorParams: { controlId: propertyData.id, viewModelId },
                    converter: new PageToolbarConverter(this.domService, propertyData.id, showType)
                },
                {
                    propertyID: 'toolbarBtnSize',
                    propertyName: '按钮尺寸',
                    propertyType: 'select',
                    iterator: [
                        { key: 'default', value: '标准' },
                        { key: 'lg', value: '大号' }
                    ]
                },
                {
                    propertyID: 'toolbarPopDirection',
                    propertyName: '弹出方向',
                    propertyType: 'select',
                    iterator: [
                        { key: 'default', value: '自动' },
                        { key: 'top', value: '向上' },
                        { key: 'bottom', value: '向下' }
                    ]
                }
            ]
        };
    }


    private getCustomTemplatePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'custom',
            categoryName: '自定义模板',
            properties: [
                {
                    propertyID: 'upTemplate',
                    propertyName: '上方扩展区域模板',
                    propertyType: 'modal',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    }
                },
                {
                    propertyID: 'upTemplateCls',
                    propertyName: '上方扩展区域模板样式',
                    propertyType: 'string'
                },
                {
                    propertyID: 'leftTemplate',
                    propertyName: '左侧内容区域模板',
                    propertyType: 'modal',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    }
                },
                {
                    propertyID: 'leftTemplateCls',
                    propertyName: '左侧内容区域模板样式',
                    propertyType: 'string'
                },
                {
                    propertyID: 'rightTemplate',
                    propertyName: '右侧扩展区域模板',
                    propertyType: 'modal',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    }
                },
                {
                    propertyID: 'rightTemplateCls',
                    propertyName: '右侧扩展区域模板样式',
                    propertyType: 'string'
                }
            ]
        };
    }
}
