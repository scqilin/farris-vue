import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../../../common/property/container-property-config';
import { ControlAppearancePropertyConfig } from '../../../../../../utils/appearance-property-config';

export class FdResponseLayoutItemProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        let propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = ControlAppearancePropertyConfig.getAppearanceStylePropConfigs(propertyData);
        propertyConfig = propertyConfig.concat(appearancePropConfig);

        return propertyConfig;

    }

}
