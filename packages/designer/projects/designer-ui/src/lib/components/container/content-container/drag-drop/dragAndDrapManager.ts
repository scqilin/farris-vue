import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { DgControl } from '../../../../utils/dg-control';
import { ComponentResolveContext, FormComponentType } from '@farris/designer-services';

export class ContainerDragDropManager {

    private cmpInstance: FdContainerBaseComponent;


    private canAcceptChildContent = true;

    constructor(cmpInstance: FdContainerBaseComponent) {
        this.cmpInstance = cmpInstance;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    canAccepts(resolveContext: ComponentResolveContext): boolean {

        if (!this.canAcceptChildContent) {
            return false;
        }
        return true;
    }

    /**
     * 判断当前容器是否是固定的上下文的中间层级
     */
    checkIsInFixedContextRules() {
        const component = this.cmpInstance.component;

        // 控件本身样式
        const cmpClass = component.appearance && component.appearance.class || '';
        const cmpClassList = cmpClass.split(' ');

        // 子级节点
        const childContents = component.contents || [];
        const firstChildContent = childContents.length ? childContents[0] : null;
        const firstChildClass = firstChildContent && firstChildContent.appearance ? firstChildContent.appearance.class : '';
        const firstChildClassList = firstChildClass ? firstChildClass.split(' ') : [];

        // 父级节点
        const parent = this.cmpInstance.parent && this.cmpInstance.parent.component;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 1、顶层节点
        if (cmpClassList.includes('f-page')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 2、页头
        if (cmpClassList.includes('f-page-header')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 2、header标题模板的父级容器
        if (cmpClassList.includes('f-title') && parentClassList.includes('f-page-header-base') && firstChildContent && firstChildContent.type === DgControl.HtmlTemplate.type) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 3、header标题模板的祖父级容器
        if (cmpClassList.includes('f-page-header-base') && parentClassList.includes('f-page-header') && firstChildClassList.includes('f-title')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 3、页面主区域
        if (cmpClassList.includes('f-page-main') && parentClassList.includes('f-page')) {
            // 卡片类模板
            if (childContents.length === 1 && firstChildClassList.includes('f-struct-like-card')) {
                this.canAcceptChildContent = false;
            }
            // 列卡类
            if (firstChildClassList.includes('f-page-content')) {
                this.canAcceptChildContent = false;
            }
            // 滚动监听类
            if (firstChildContent && firstChildContent.type === DgControl.Scrollspy.type) {
                this.canAcceptChildContent = false;
            }
            return true;
        }
        // 3、页面主区域like-card，若只有一个，那么不可移动、删除，但可以接收控件
        if (cmpClassList.includes('f-struct-like-card') && parent && parent.contents.length === 1 && parentClassList.includes('f-page-main')) {
            return true;
        }
        // 4、隐藏子表DataGrid父级间距
        if (cmpClassList.includes('f-grid-is-sub') && parentClassList.includes('f-struct-is-subgrid') && firstChildClassList.includes('f-component-grid')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 4、子表三层父容器 container---section-tab 中container不接收新控件
        if (cmpClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-section-tabs')) {
            if (firstChildContent.contents && firstChildContent.contents.length === 1) {
                const tabEle = firstChildContent.contents[0];
                if (tabEle.type === DgControl.Tab.type) {
                    this.canAcceptChildContent = false;
                }
            }
        }

        // 5、子表分组容器：container-section-componentRef
        if (cmpClassList.includes('f-struct-wrapper') && (firstChildClassList.includes('f-section-in-main') || firstChildClassList.includes('f-section-in-mainsubcard'))) {
            if (firstChildContent.type === DgControl.Section.type && firstChildContent.contents && firstChildContent.contents.length === 1) {
                const cmpRefEle = firstChildContent.contents[0];
                if (cmpRefEle.type === DgControl.ComponentRef.type) {
                    this.canAcceptChildContent = false;
                }
            }
        }
        // 5、滚动监听容器
        if (parent && parent.contents.length > 1) {
            const preSibling = parent.contents[0];
            const preSiblingType = preSibling && preSibling.type || '';
            if (cmpClassList.includes('f-scrollspy-content') && parentClassList.includes('f-page-main') && preSiblingType === DgControl.Scrollspy.type) {
                this.canAcceptChildContent = false;
                return true;
            }

        }
        // 5、填报模板的框架容器
        if (cmpClassList.includes('f-page-layout') && parentClassList.includes('f-page')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 5、填报模板的框架容器
        if (cmpClassList.includes('f-page-container') && parentClassList.includes('f-page-layout')) {
            // this.canAcceptChildContent = false;
            return true;
        }
        // 5、带导航的模板的框架容器
        if (cmpClassList.includes('f-page-content') && parentClassList.includes('f-page-main')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 5、带导航的模板的框架容器
        if (cmpClassList.includes('f-page-content-main') && parentClassList.includes('f-page-content')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 5、筛选条容器
        if (cmpClassList.includes('f-filter-container') && firstChildContent && firstChildContent.type === DgControl.ListFilter.type) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 树组件模板、列表组件模板框架容器
        if (cmpClassList.includes('f-page-module')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 树组件模板、列表组件模板TreeGrid父级容器间距
        if (cmpClassList.includes('f-utils-fill-flex-column') && parent && parent.type === DgControl.Component.type && parentClassList.includes('f-utils-fill-flex-column')) {
            if (firstChildContent && (firstChildContent.type === DgControl.TreeGrid.type || firstChildContent.type === DgControl.DataGrid.type)) {
                this.canAcceptChildContent = false;
                return true;
            }
        }
        // ListView模板的框架容器
        if (cmpClassList.includes('farris-main-area') && parent && parent.type === DgControl.Component.type && parent.componentType === FormComponentType.Frame) {
            this.canAcceptChildContent = false;
            return true;

        }

        // ListView模板中ListView组件---ContentContainer---ListView 的三层结构
        if (parent && parent.type === DgControl.Component.type && (parent.componentType === FormComponentType.listView || parent.componentType === 'ListView') && firstChildContent && firstChildContent.type === DgControl.ListView.type) {
            this.canAcceptChildContent = false;
            return true;

        }

        // 多视图下的组件不允许移动、删除
        if (parent && parent.type === DgControl.MultiViewContainer.type) {
            return true;
        }

    }
}
