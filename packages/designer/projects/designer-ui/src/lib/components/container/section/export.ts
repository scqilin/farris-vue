import { ComponentExportEntity } from '@farris/designer-element';
import FdSectionComponent from './component/fd-section';
import { SectionSchema, SectionToolbarItemSchema, SectionToolbarSchema } from './schema/schema';

import FdSectionTemplates from './templates';
import FdSectionToolBarTemplate from '../../command/toolbar/templates/form';
import FdSectionToolBarItemTemplate from '../../command/toolbar/component/toolbaritem/templates/form';

export const Section: ComponentExportEntity = {
    type: 'Section',
    component: FdSectionComponent,
    template: FdSectionTemplates,
    metadata: SectionSchema
};

export const SectionToolbar: ComponentExportEntity = {
    type: 'SectionToolbar',
    metadata: SectionToolbarSchema,
    template: { form: FdSectionToolBarTemplate }
};

export const SectionToolbarItem: ComponentExportEntity = {
    type: 'SectionToolbarItem',
    metadata: SectionToolbarItemSchema,
    template: { form: FdSectionToolBarItemTemplate }
};
