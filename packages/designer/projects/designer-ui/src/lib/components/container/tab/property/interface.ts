
export interface actions {
    sourceComponent:{
        id:string,
        viewModelId:string,
        map:mapItem[],
    },  
};

export interface mapItem {
    event:{
        label:string,
        name:string,
    },
    command:{
        id:string,
        label:string,
        name:string,
        handlerName:string,
        params?:any,
    },
    controller:{
        id:string,
        label:string,
        name:string,
    },
    targetComponent:{
        id:string,
        viewModelId:string,
    },

}

// export interface paramsItem {

// }