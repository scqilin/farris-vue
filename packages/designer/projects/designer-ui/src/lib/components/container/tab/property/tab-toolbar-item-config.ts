import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { EventEditorService, DomService, FormBasicService, StateMachineService, UniformEditorDataUtil, WebCmdService } from '@farris/designer-services';
import { Injector } from '@angular/core';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export default class TabToolBarItemProp {

    private domService: DomService;
    private stateMachineService: StateMachineService;
    public viewModelId: string;
    public webCmdService: WebCmdService;
    public formBasicService: FormBasicService;
    public eventEditorService: EventEditorService;

    constructor(private injector: Injector, viewModelId: string) {
        this.viewModelId = viewModelId;
        this.domService = this.injector.get(DomService);
        this.formBasicService = this.injector.get(FormBasicService);
        this.webCmdService = this.injector.get(WebCmdService);
        this.stateMachineService = this.injector.get(StateMachineService);
        this.eventEditorService = this.injector.get(EventEditorService);
    }
    /**
     * toolbarItem 属性配置
     * @param viewModelId 按钮所在视图模型ID
     * @param propertyData 按钮属性
     * @param disableChildItem 是否禁用子级按钮
     * @param isFirstLevel 是否为一级按钮
     */
    public getPropertyConfig(propertyData: any) {

        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig();
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearanceItemPropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorItemPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }

    private getBasicPropConfig(): ElementPropertyConfig {
        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '工具栏按钮的标识',
                    readonly: true
                },
                {
                    propertyID: 'title',
                    propertyName: '标签',
                    propertyType: 'string',
                    description: '工具栏按钮的标签',
                    readonly: false
                }
            ]
        };
    }


    private getAppearanceItemPropConfig(): ElementPropertyConfig {

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: [
                {
                    propertyID: 'appearance',
                    propertyName: '样式',
                    propertyType: 'cascade',
                    cascadeConfig: [
                        {
                            propertyID: 'class',
                            propertyName: '按钮样式',
                            propertyType: 'string',
                            description: '工具栏按钮样式'
                        }
                    ]
                }
            ]
        };
    }

    private getBehaviorItemPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        const self = this;
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                {
                    propertyID: 'visible',
                    propertyName: '是否可见',
                    propertyType: 'unity',
                    description: '运行时组件是否可见',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        }
                    }
                },
                {
                    propertyID: 'disable',
                    propertyName: '禁用',
                    propertyType: 'unity',
                    description: '是否禁用',
                    editorParams: {
                        controlName: undefined,
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'stateMachine'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            stateMachine: this.stateMachineService.stateMachineMetaData
                        }
                    }
                }
            ]
        };
    }

    // 事件编辑器集成
    private getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            }
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            hideTitle: true,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
            }
        };
    }
}
