import FdMultiViewContainerComponent from './component/fd-multi-view-container';
import { MultiViewContainerSchema,MultiViewItemSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdMultiViewContainerTemplates from './templates';


export const MultiViewContainer: ComponentExportEntity = {
    type: 'MultiViewContainer',
    component: FdMultiViewContainerComponent,
    template: FdMultiViewContainerTemplates,
    metadata: MultiViewContainerSchema
};


export const MultiViewItem: ComponentExportEntity = {
    type: 'MultiViewItem',
    metadata: MultiViewItemSchema
};
