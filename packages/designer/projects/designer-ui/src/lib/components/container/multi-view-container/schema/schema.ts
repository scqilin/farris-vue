export const MultiViewContainerSchema = {
    id: '',
    type: 'MultiViewContainer',
    appearance: {
        class: ''
    },
    contents: [],
    visible: true,
    dataSource: null
};

export const MultiViewItemSchema = {
    id: '',
    type: 'MultiViewItem',
    appearance: {
        class: ''
    },
    title: '',
    icon: 'f-icon f-icon-group',
    contents: []
};