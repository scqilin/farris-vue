import { TypeConverter } from '@farris/ide-property-panel';
export class MultiViewConverter implements TypeConverter {

    constructor() { }

    convertTo(data: any): string {
        if (!data) {
            return '';
        }
        return '共 ' + data.length + ' 个';
    }
}
