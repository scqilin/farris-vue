import { ComponentExportEntity } from '@farris/designer-element';
import FdFormComponent from './component/fd-form';
import { FormSchema } from './schema/schema';

import FdFormTemplates from './templates';


export const Form: ComponentExportEntity = {
    type: 'Form',
    component: FdFormComponent,
    template: FdFormTemplates,
    metadata: FormSchema
};
