
import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { EventEditorService, DomService, FormBasicService, RefreshFormService, WebCmdService, StateMachineService, UniformEditorDataUtil } from '@farris/designer-services';
import { IDesignerHost } from '@farris/designer-element';
import { IdService } from '@farris/ui-common';
import { DgControl } from '../../../../utils/dg-control';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class NoCodeContainerUsualProp {
  public formBasicService: FormBasicService;
  public refreshFormService: RefreshFormService;
  public domService: DomService;
  public webCmdService: WebCmdService;
  public viewModelId: string;
  public componentId: string;
  public idService: IdService;
  public stateMachineService: StateMachineService;
  public eventEditorService: EventEditorService;

  constructor(public serviceHost: IDesignerHost, viewModelId: string, componentId: string) {
    this.viewModelId = viewModelId;
    this.componentId = componentId;
    this.domService = serviceHost.getService('DomService');
    this.formBasicService = serviceHost.getService('FormBasicService');
    this.refreshFormService = serviceHost.getService('RefreshFormService');
    this.eventEditorService = serviceHost.getService('EventEditorService');
    this.webCmdService = serviceHost.getService('WebCmdService');
    this.idService = new IdService();

    this.stateMachineService = serviceHost.getService('StateMachineService');
  }

  getBasicPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

    return {
      categoryId: 'basic',
      categoryName: '基本信息',
      properties: [
        // {
        //   propertyID: 'id',
        //   propertyName: '标识',
        //   propertyType: 'string',
        //   description: '组件的id',
        //   readonly: true
        // },
        {
          propertyID: 'type',
          propertyName: '控件类型',
          propertyType: 'select',
          description: '组件的类型',
          iterator: [{ key: propertyData.type, value: DgControl[propertyData.type].name }],
        }
      ]
    };
  }


  getCommonAppearanceProperties(): PropertyEntity[] {

    return [
      {
        propertyID: 'size',
        propertyName: '尺寸',
        propertyType: 'cascade',
        cascadeConfig: [
          {
            propertyID: 'width',
            propertyName: '宽度（px）',
            propertyType: 'number',
            description: '组件的宽度',
            min: 0,
            decimals: 0
          },
          {
            propertyID: 'height',
            propertyName: '高度（px）',
            propertyType: 'number',
            description: '组件的高度',
            min: 0,
            decimals: 0
          }
        ]
      }
    ];

  }

  getVisiblePropertyEntity(propertyData: any, viewModelId: string) {
    return {
      propertyID: 'visible',
      propertyName: '是否可见',
      propertyType: 'unity',
      description: '运行时组件是否可见',
      editorParams: {
        controlName: UniformEditorDataUtil.getControlName(propertyData),
        constType: 'enum',
        editorOptions: {
          types: ['const', 'variable'],
          enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
          variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
          getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
          newVariableType: 'Boolean',
          newVariablePrefix: 'is'
        },

      }
    };
  }

  /**
   * 新版属性编辑器，在编辑过程中可能会新增变量，此处需要将新增的变量追加到ViewModel中
   */
  addNewVariableToViewModelAfterPropertyChanged(changeObject: FormPropertyChangeObject, viewModelId: string) {
    const newPropertyValue = changeObject.propertyValue;
    // tslint:disable-next-line:max-line-length
    if (newPropertyValue && newPropertyValue.isNewVariable && typeof newPropertyValue === 'object' && newPropertyValue.type === 'Variable') {
      // 如果有则加入新变量
      delete newPropertyValue.isNewVariable;
      const newVar = {
        id: newPropertyValue.field,
        category: 'locale',
        code: newPropertyValue.path,
        name: newPropertyValue.path,
        type: newPropertyValue.newVariableType || 'String'
      };
      delete newPropertyValue.newVariableType;
      const viewModel = this.domService.getViewModelById(viewModelId);
      if (!viewModel.states.find(s => s.id !== newVar.id)) {
        viewModel.states.push(newVar);
      }
    }

  }


}
