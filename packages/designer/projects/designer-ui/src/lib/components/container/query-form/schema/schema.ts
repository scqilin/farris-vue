export const QueryFormSchema = {
    id: 'form',
    type: 'QueryForm',
    appearance: {
        class: 'f-form-layout farris-form farris-form-controls-inline'
    },
    visible: true,
    controlsInline: true,
    formAutoIntl: true,
    contents: []
};

