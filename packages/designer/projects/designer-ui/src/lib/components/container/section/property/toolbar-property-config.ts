import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { DgControl } from '../../../../utils/dg-control';

export class SectionToolBarProp extends ContainerUsualProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 按钮属性
        const toolbarPropConfig = this.getToolbarPropConfig();
        this.propertyConfig.push(toolbarPropConfig);

        return this.propertyConfig;

    }
    getBasicPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件的类型',
                    iterator: [{ key: propertyData.type, value: DgControl[propertyData.type].name }],
                }
            ]
        };
    }
    private getToolbarPropConfig(): ElementPropertyConfig {

        // 若表单有状态机，section才支持启用按钮；若没有状态机，不支持启用按钮
        let showToolbar = false;
        const stateMachines = this.domService.module.stateMachines;
        if (stateMachines && stateMachines.length > 0) {
            showToolbar = true;
        }

        return {
            categoryId: 'toolbar',
            categoryName: '工具栏',
            hide: !showToolbar,
            properties: [
                {
                    propertyID: 'position',
                    propertyName: '显示位置',
                    propertyType: 'select',
                    iterator: [{ key: 'inHead', value: '标题区域' }, { key: 'inContent', value: '内容区域' }]
                }
            ]
        };
    }
}
