export default (ctx: any) => {

    // Header样式
    let headerCls = ' farris-tabs-inContent';

    // Header中Tab标题所占宽度比例
    let headerTitleStyle = '';
    if (hasInHeadCl(ctx)) {
        headerCls = ' farris-tabs-inHead';
        headerTitleStyle = ctx.component.titleWidth ? `width:${ctx.component.titleWidth}%` : '';
    }

    // fill属性没有
    let navTabsCls = '';
    navTabsCls += ctx.component.position === 'top' || ctx.component.position === 'bottom' ? ' flex-row' : '';
    navTabsCls += ctx.component.position === 'left' || ctx.component.position === 'right' ? ' flex-column' : '';

    // 遍历标签页，获取头部和内容区域
    const { tabPageNavs, tabPageBodys } = getTabPageTemplate(ctx);

    // 判断按钮
    let toolbarStr = '';
    if (ctx.toolbarSettings) {
        toolbarStr = `<div class="farris-tabs-inline-flex" style="overflow: hidden;flex:1">
            ${ctx.toolBarComponent.render()}
        </div>`;
        headerCls += ' hasToolbar';
    }

    // 判断viewchange的扩展区域
    let headerExtendStr = '';
    if (ctx.headerExtendSettings) {
        headerExtendStr = `<div class="farris-tabs-header-extend ${toolbarStr ? 'margin-right-14' : ''}">
                <div class="f-view-change f-btn-ml" ref="${ctx.viewChangeKey}" viewChange="${ctx.headerExtendSettings.viewGroupIdentify}">
                ${ctx.getViewChangeStr()}
            </div>
        </div>`;
    }

    return `
        <farris-tabs class="farris-tabs ide-cmp-tabs flex-column ${ctx.component.contentFill ? ' f-tabs-content-fill' : ''}" id="${ctx.component.id}">
            <div class="farris-tabs-header ${headerCls}">
                <div class="farris-tabs-title scroll-tabs" style="${headerTitleStyle}">
                    <div class="spacer f-utils-fill" style="width:100%;">
                        <ul class="nav farris-nav-tabs flex-nowrap">
                            ${tabPageNavs}
                        </ul>
                    </div>
                </div>
                <div class="farris-tabs-toolbar">
                    ${toolbarStr}
                    ${headerExtendStr}
                </div>
            </div>
            ${tabPageBodys}
        </farris-tabs>
    `;
};

/**
 * 判断Header的样式
 */
function hasInHeadCl(ctx: any) {
    if (ctx.toolbarSettings && ctx.toolbarSettings.hasOwnProperty('position') && ctx.toolbarSettings.hasOwnProperty('contents') && ctx.toolbarSettings.position === 'inHead' && ctx.toolbarSettings.contents.length > 0) {
        return true;
    }
    if (ctx.headerExtendSettings && !(ctx.toolbarSettings && ctx.toolbarSettings.position === 'inContent' && ctx.toolbarSettings.contents.length)) {
        return true;
    }
    return false;
}


function getTabPageTemplate(ctx: any) {

    let tabPageNavs = '';
    let tabPageBodys = '';


    ctx.component.contents.forEach((tab, index) => {
        let tabNavCls = ctx.selectedTabPageId === tab.id ? ' f-state-active farris-component' : '';
        tabNavCls += tab.disabled ? ' f-state-disabled' : '';

        const tabBodyCls = ctx.selectedTabPageId === tab.id ? ' f-tab-active' : ' f-tab-d-none';

        const tabStyle = tab.tabWidth ? `width:${tab.tabWidth}px` : '';
        let navLinkCls = tab.id === ctx.selectedTabPageId ? ' active' : '';
        navLinkCls += tab.disabled ? ' disabled' : '';
        let navTextStr = '';
        // 有模板
        if (tab.headerTemplate) {
            navTextStr = ` <span class="st-tab-text ${ctx.component.autoTitleWidth ? ' farris-title-auto' : ''}">${tab.headerTemplate}</span>`;
        } else {
            //  farris-title-text-custom 需要计算
            navTextStr = ` <span class="st-tab-text ${ctx.component.autoTitleWidth ? ' farris-title-auto' : ''}">${tab.title}</span>`;
        }
        // 若没有工具栏，则增加图标
        let addToolbarStr = '';
        if (!tab.toolbar || !tab.toolbar.contents || !tab.toolbar.contents.length) {
            addToolbarStr = `
                <div role="button" class="btn component-settings-button" title="新增工具栏" ref="addToolbar">
                    <i class="f-icon f-icon-plus-circle text-white"></i>
                </div>
            `;
        }
        tabPageNavs += `
        <li class="nav-item position-relative ${tabNavCls}" ref="${ctx.tabPageLinkKey}" style="${tabStyle}" id="${tab.id}">
        <div class="component-btn-group" data-noattach="true">
            <div>
                <div role="button" class="btn component-settings-button" title="删除" ref="removeTabPage" tabPageTitle="${tab.title}" tabPageId="${tab.id}">
                    <i class="f-icon f-icon-yxs_delete"></i>
                </div>
               ${addToolbarStr}
            </div>
        </div>
        <a class="nav-link tabs-text-truncate ${navLinkCls}">
            ${navTextStr}
        </a>
        </li>
        `;

        tabPageBodys += `
        <div class="farris-tabs-body ${tabBodyCls} drag-container"
             ref="${ctx.tabPageBodyKey}" dragref="${ctx.tabPageBodyKey}-container" id="${tab.id}-body">
            ${ctx.tabComponents[index]}
        </div>
        `;
    });

    return { tabPageNavs, tabPageBodys };
}
