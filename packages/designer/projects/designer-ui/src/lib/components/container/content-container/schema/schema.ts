export const ContentContainerSchema = {
    id: 'contentContainer',
    type: 'ContentContainer',
    appearance: null,
    visible: true,
    contents: [],
    isScrollspyContainer: false,
    isLikeCardContainer: false
};

