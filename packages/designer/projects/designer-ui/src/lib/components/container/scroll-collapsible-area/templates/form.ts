export default (ctx: any) => {
    // 如果有模板，就优先显示，如果没有就显示拖拽容器
    let contentStr = '';
    if (ctx.component.contentTemplate) {
        contentStr = ctx.component.contentTemplate;
    } else {
        contentStr = `<div class="drag-container" id = "${ctx.component.id}" ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container">
        ${ctx.children}
    </div>`;
    }
    // 样式
    let collapseCls = ctx.collapseState ? 'f-state-collapse' : 'f-state-expand';
    return `<div class="fe-cmp-collapsible-area ide-cmp-collapsible-area ${collapseCls}" ref="${ctx.ScrollCollapsibleAreaKey}">
    <div class="fe-collapsible-container animated-none">
        <div class="fe-collapsible-area">
            ${contentStr}
        </div>
    </div>
    <div class="fe-collapsible-icon-container">
        <span class="f-icon-container"><i class="f-icon"></i></span>
    </div>
</div>`;
};
