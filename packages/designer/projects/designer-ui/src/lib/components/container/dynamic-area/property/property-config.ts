import { CollectionWithPropertyConverter, CollectionWithPropertyEditorComponent } from '@farris/designer-devkit';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { DynamicAreaItemProp } from './item-property-config';

export class DynamicAreaProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getCustomPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);


        return propertyConfig;

    }

    private getCustomPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        const itemProp = new DynamicAreaItemProp(this.serviceHost, this.viewModelId, this.componentId)
        return {
            categoryId: 'custom',
            categoryName: '面板显示配置',
            properties: [
                {
                    propertyID: 'contents',
                    propertyName: '面板配置',
                    propertyType: 'modal',
                    editor: CollectionWithPropertyEditorComponent,
                    converter: new CollectionWithPropertyConverter(),
                    editorParams: {
                        modalTitle: '面板配置',
                        viewModelId,
                        idKey: 'id',
                        textKey: 'title',
                        maxCascadeLevel: 1,
                        disableToolbar: true,
                        getPropertyConfig: (selectedNode) => itemProp.getPropConfig(selectedNode.data)
                    }
                }
            ]
        };
    }
}
