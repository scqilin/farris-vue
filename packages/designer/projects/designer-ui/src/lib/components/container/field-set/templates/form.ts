export default (ctx: any) => {
  const formGroupCls = ctx.collapseState ? ' f-state-collapse' : '';
  const buttonCls = ctx.collapseState ? '' : ' f-state-expand';
  const buttonText = ctx.collapseState ? ctx.component.expandText : ctx.component.collapseText;
  return `
    <fieldset class="ide-cmp-fieldSet f-section-formgroup ${formGroupCls}" id="${ctx.component.id}">
        <legend class="f-section-formgroup-legend" ref="${ctx.fieldSetLegendKey}">
            <div class="f-header">
              <div class="d-flex f-header-text" style="align-items: center;" >
                <div class="f-title">${ctx.component.title}</div>
                <div class="f-toolbar">
                    <button class="btn f-btn-collapse-expand f-btn-mx ${buttonCls}">
                        <span>${buttonText}</span>
                    </button>
                </div>
              </div>
            </div>
        </legend>

        <div class="f-section-formgroup-inputs mb-2 drag-container" ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container">
                ${ctx.children}
        </div>
    </fieldset>
      `;
};
