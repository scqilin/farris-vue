import { FieldSetSchema } from '../schema/schema';
import { BuilderHTMLElement } from '@farris/designer-element';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FieldSetProp } from '../property/property-config';
import { RowNode } from '@farris/ui-treetable';
import { FieldSetContextMenuManager } from '../context-menu/context-menu.manager';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { FieldSetDragDropManager } from '../drag-drop/dragAndDropManager';

export default class FdFieldSetComponent extends FdContainerBaseComponent {
    /** 在运行态的収折的状态 */
    collapseState: boolean;

    // 标识当前的legend
    get fieldSetLegendKey(): string {
        return `fieldSetLegend-${this.key}`;
    }

    constructor(component: any, options: any) {
        super(component, options);
    }

    getStyles(): string {
        return ' display: block;';
    }
    getDefaultSchema(): any {
        return FieldSetSchema;
    }

    getTemplateName(): string {
        return 'FieldSet';
    }
    // 初始化
    init(): void {
        // 维护分组在设计时的收折状态
        this.component.collapseStateInDesignerView = this.component.collapseStateInDesignerView === undefined ? this.component.collapse : this.component.collapseStateInDesignerView;

        super.init();
    }
    // 渲染模板
    render(children): any {
        return super.render(children || this.renderTemplate('FieldSet', {
            collapseState: this.component.collapseStateInDesignerView,
            fieldSetLegendKey: this.fieldSetLegendKey,
            children: this.renderComponents(),
            nestedKey: this.nestedKey
        }));
    }
    // 绑定事件
    attach(element: any): any {
        // key 如果有多个用 multi，单个用single
        this.loadRefs(element, {
            [this.fieldSetLegendKey]: 'single'
        });
        // 必写
        const superAttach: any = super.attach(element);

        // 注册收折按钮点击事件
        const btnEl = this.refs[this.fieldSetLegendKey].querySelector('.f-header-text');
        this.addEventListener(btnEl, 'click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this.component.collapseStateInDesignerView = !this.component.collapseStateInDesignerView;
            this.redraw();

            // 收折后涉及到了界面高度的变化，所以需要重新计算页面上控件操作图标的位置
            this.setPositionOfSelectedComponentBtnGroup();
        });
        // 必写
        return superAttach;
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: FieldSetProp = new FieldSetProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }
    onPropertyChanged(changeObject: FormPropertyChangeObject) {
        super.onPropertyChanged(changeObject, ['title', 'collapseText', 'expandText']);
    }

    /**
     * 判断是否可以接收拖拽新增的子级控件
     * @param data 新控件的类型、所属分类
     * @returns boolean
     */
    canAccepts(sourceElement: BuilderHTMLElement) {
        const dragManager = new FieldSetDragDropManager(this);
        return dragManager.canAccepts(sourceElement);
    }

    /**
     * 移动控件后事件：在可视化设计器中，将现有的控件移动到容器中
     * @param el 移动的源DOM结构
     */
    onAcceptMovedChildElement(sourceElement: BuilderHTMLElement) {
        if (!sourceElement) {
            return;
        }
        const dragManager = new FieldSetDragDropManager(this);
        dragManager.onAcceptMovedChildElement(sourceElement);
    }

    /**
     * 组装右键菜单
     * @param rowNode 组件在控件树上对应的行数据
     */
    resolveContextMenuConfig(rowNode: RowNode) {
        const menuManager = new FieldSetContextMenuManager(this, rowNode);
        return menuManager.setContextMenuConfig();
    }

}
