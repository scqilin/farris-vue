import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class FormProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;

        const commonProps = this.getCommonAppearanceProperties();

        let properties = commonProps;


        properties = properties.concat([
            {
                propertyID: 'controlsInline',
                propertyName: '控件标签独占一列',
                propertyType: 'boolean',
                description: '控件标签是否独占一列',
            }]);

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                switch (changeObject.propertyID) {
                    case 'controlsInline': {
                        const appearance = Object.assign({}, propertyData.appearance);
                        if (changeObject.propertyValue) {
                            if (!appearance.class.includes('farris-form-controls-inline')) {
                                appearance.class = appearance.class + ' ' + 'farris-form-controls-inline';
                            }
                        } else {
                            appearance.class = appearance.class.replace('farris-form-controls-inline', '');
                        }
                        propData.appearance = appearance;

                        break;
                    }
                }
            }
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ],
        };
    }

}
