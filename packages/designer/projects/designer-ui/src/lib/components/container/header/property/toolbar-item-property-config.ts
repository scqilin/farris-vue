import { EventsEditorFuncUtils } from '../../..../../../../utils/events-editor-func';
import { ElementPropertyConfig } from '@farris/ide-property-panel';

import { ToolBarItemProp } from '../../../command/toolbar/component/toolbaritem/property/property-config';
import { FormPropertyChangeObject } from '../../..../../../../entity/property-change-entity';

/**
 * Header单个按钮属性配置
 */
export class HeaderToolBarItemProp extends ToolBarItemProp {

    /** 是否为二级按钮 */
    isChildOfButton: boolean;
    /** 父级按钮id */
    parentButtonId: string;

    // 事件编辑器集成
    getEventPropertyConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            }
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            hideTitle: true,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];

                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates; // 添加自定义方法后，调用此回调方法，用于处理联动属性

                    // 若需要跳转到代码视图，首先要求用户将当前弹出窗口关闭
                    if (parameters.isAddControllerMethod && self.triggerModalSave) {
                        self.msgService.question('确定关闭当前编辑器并跳转到代码视图吗？', () => {
                            self.triggerModalSave();

                            EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                            this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
                        });
                    } else {
                        EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                        this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
                    }
                } else {
                    self.syncGlobalButtonEvent(propertyData);
                }

            }
        };
    }
    /**
     * 添加自定义方法（跳转到代码视图的场景）后，同步全局配置
     */
    private syncGlobalButtonEvent(propertyData: any) {
        if (this.domService.module.toolbar && this.domService.module.toolbar.items) {
            const buttonsInGlobal = this.domService.module.toolbar.items[this.viewModelId];
            if (!buttonsInGlobal) {
                return;
            }
            let buttonInGlobal;
            if (this.isChildOfButton && this.parentButtonId) {
                const firstLevelItem = buttonsInGlobal.find(button => button.id === this.parentButtonId);
                if (firstLevelItem && firstLevelItem.items && firstLevelItem.items.length) {
                    buttonInGlobal = firstLevelItem.items.find(b => b.id === propertyData.id);
                }
            } else {
                buttonInGlobal = buttonsInGlobal.find(b => b.id === propertyData.id);
            }

            if (!buttonInGlobal) {
                return;
            }
            // 变更事件属性
            buttonInGlobal['click'] = propertyData['click'];

        }


    }

}

