export const TabSchema = {
    id: 'tab',
    type: 'Tab',
    controlSource: 'Farris',
    appearance: null,
    selected: null,
    size: null,
    position: 'top',
    contentFill: false,
    autoTitleWidth: true,
    titleLength: 7,
    contents: [],
    tabChange: null,
    tabRemove: null,
    visible: true,
    beforeSelect: null
};

export const TabPageSchema = {
    id: 'tabPage',
    type: 'TabPage',
    controlSource: 'Farris',
    title: '标题',
    appearance: null,
    size: null,
    removeable: false,
    headerTemplate: null,
    contents: [],
    toolbar: {
        id: 'TabToolbar',
        type: 'TabToolbar',
        position: 'inHead',
        contents: []
    },
    enableExtend: false,
    extendTemplate: '',
    visible: true
};

export const TabToolbarSchema = {
    id: 'TabToolbar',
    type: 'TabToolbar',
    position: 'inHead',
    contents: []

};

export const TabToolbarItemSchema = {
    id: 'tabToolbarItem',
    type: 'TabToolbarItem',
    title: '按钮',
    disable: false,
    appearance: {
        class: 'btn btn-secondary f-btn-ml'
    },
    visible: true,
    click: null
};


