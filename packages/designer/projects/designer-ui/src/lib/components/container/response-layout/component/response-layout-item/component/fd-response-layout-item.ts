import { BuilderHTMLElement } from '@farris/designer-element';
import FdContainerBaseComponent from '../../../../common/containerBase/containerBase';
import { ComponentResolveContext } from '@farris/designer-services';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FdResponseLayoutItemProp } from '../property/property-config';
import { ResponseLayoutItemDragDropManager } from '../drag-drop/dragAndDropManager';

/**
 * 布局容器项
 */
export default class FdResponseLayoutItemComponent extends FdContainerBaseComponent {


    constructor(component: any, options: any) {
        super(component, options);
        this.viewModelId = this.parent.viewModelId;
        this.componentId = this.parent.componentId;

        this.components = [];
    }


    attach(element: HTMLElement): Promise<any> {
        const superAttach: any = super.attach(element);

        return superAttach;
    }

    afterComponentClicked(e?: PointerEvent): void {
        this.parent.selectedLayoutItemId = this.id;
    }

    afterComponentCancelClicked(e?: PointerEvent): void {
        this.parent.selectedLayoutItemId = null;
    }

    /**
     * 组装属性面板配置数据
     * @returns ElementPropertyConfig[]
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: FdResponseLayoutItemProp = new FdResponseLayoutItemProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {

        const result = super.canAccept(sourceElement, targetElement);
        if (!result.canAccepts) {
            return false;

        }
        const dragManager = new ResponseLayoutItemDragDropManager(this);
        return dragManager.canAccepts(sourceElement, targetElement);

    }

    /**
     * 接收新控件后事件(来源为控件工具箱或者实体树)
     * @param componentResolveContext 新控件构造信息
     */
    afterAcceptNewChildElement(componentResolveContext: ComponentResolveContext) {
        if (!componentResolveContext.bindingSourceContext) {
            return;
        }

        const dragManager = new ResponseLayoutItemDragDropManager(this);
        return dragManager.afterAcceptNewChildElement(componentResolveContext);

    }

    /**
     * 移动内部控件后事件：在可视化设计器中，容器接收控件后事件
     * @param el 移动的源DOM结构
     */
    onAcceptMovedChildElement(sourceElement: BuilderHTMLElement) {
        if (!sourceElement) {
            return;
        }

        const dragManager = new ResponseLayoutItemDragDropManager(this);
        dragManager.onAcceptMovedChildElement(sourceElement);
    }

    /**
     * 移动内部控件后事件：在可视化设计器中，容器中的控件被移出后事件
     * @param el 移动的源DOM结构
     */
    onChildElementMovedOut(sourceElement: BuilderHTMLElement) {
        if (!sourceElement) {
            return;
        }

        // 若被移出的源组件是输入类控件，需要将当前布局项上的form样式删除掉
        if (sourceElement.componentInstance && sourceElement.componentInstance.category === 'input') {
            this.removeFormClassFromResponseLayoutItem();
        }
    }

    /**
     *  移除布局项中的form类样式
     */
    removeFormClassFromResponseLayoutItem() {
        const dragManager = new ResponseLayoutItemDragDropManager(this);
        dragManager.removeFormClassFromResponseLayoutItem();
    }

    redraw(): Promise<any> {
        return this.parent.redraw();
    }
    rebuild(): Promise<any> {
        return this.parent.rebuild();
    }
}
