export default (ctx: any) => {
    // 侧边栏样式
    let sidebarCls = ` f-sidebar-pos-${ctx.component.position}`;
    sidebarCls += ctx.isOpen ? ' f-sidebar-show' : ' f-sidebar-collapse';

    // 侧边栏Main样式
    const { mainStyle, mainCls } = getMainClassAndStyle(ctx);

    // 头部
    const headerStr = getHeaderStr(ctx);

    // 底部
    const footerStr = getFooterStr(ctx);

    // 入口按钮 ---此处不处理属性，始终显示
    const entryStr = getEntryStr(ctx);

    // 遮罩层
    const overlayCls = ctx.isOpen ? ' f-sidebar-show' : ' f-sidebar-collapse';
    const overlayStr = `<div class="f-sidebar-overlay ${overlayCls}" ref="${ctx.overlayKey}"></div>`;

    return `
    <div class="f-cmp-sidebar f-sidebar-slide ide-cmp-sidebar ${sidebarCls}" ref="${ctx.sidebarKey}">
        <div class="f-sidebar-main ${mainCls}" style="${mainStyle}">
            ${headerStr}
            <div class="f-sidebar-content ${ctx.component.contentTemplateClass} drag-container" ref="${ctx.nestedKey}" id=${ctx.component.id}
                dragref="${ctx.component.id}-container">
                ${ctx.children}
            </div>
            ${footerStr}
        </div>
        ${entryStr}
    </div>
    ${overlayStr}
    `;
};


/**
 * 获取主区域样式
 */
function getMainClassAndStyle(ctx: any) {
    // 宽度
    let width = 'auto';
    if ('leftright'.indexOf(ctx.component.position) > -1) {
        width = ctx.component.width ? ctx.component.width + 'px' : '350px';
    }
    let mainStyle = `width:${width}`;

    // 高度
    let height = 'auto';
    if ('topbottom'.indexOf(ctx.component.position) > -1) {
        height = ctx.component.height ? ctx.component.height : '30%';
    }
    mainStyle += `;height:${height}`;

    // 样式
    const mainCls = ctx.canRemoveInitNoAnimateCls ? ' f-sidebar-slideinit' : '';

    return { mainStyle, mainCls };
}

/**
 * 组装头部标题区域
 */
function getTitleStr(ctx: any) {
    const titleCls =
        ctx.component.headerTitleTemplate && ctx.component.headerTitleTemplateClass
            ? ' ' + ctx.component.headerTitleTemplateClass
            : '';
    let titleStr = ` <div class="f-title f-tmpl-for-title-withline ${titleCls}">`;

    if (ctx.component.headerTitleTemplate) {
        titleStr += ctx.component.headerTitleTemplate;
    } else {
        titleStr += '<h5 class="f-title-text">' + (ctx.component.title ? ctx.component.title : '详情') + '</h5>';
    }
    titleStr += '</div>';

    return titleStr;
}

/**
 * 组装头部区域
 */
function getHeaderStr(ctx: any) {
    // 按钮区
    const toolCls = ctx.component.toolbar.items.length > 0 ? ' ' + ctx.component.toolbarTemplateClass : '';
    const toolBtnsStr = ctx.getToolbarStr();
    const toolCloseStr = !toolBtnsStr || ctx.component.showClose ? ` <span class="f-icon f-sidebar-close" ref="${ctx.closeBtnKey}"></span>` : '';
    const toolStr = `<div class="f-toolbar ${toolCls}">
                        ${toolBtnsStr} ${toolCloseStr}
                    </div>
                    `;

    // 头部标题
    const titleStr = getTitleStr(ctx);

    // 头部-中间区域
    let hContentStr = '';
    if (ctx.component.headerContentTemplate) {
        hContentStr += `<div class="f-content ${ctx.component.headerContentTemplateClass}">${ctx.component.headerContentTemplate}</div>`;
    }

    // 头部
    let headerStr = '';
    if (ctx.component.showHeader) {
        if (ctx.component.headerTemplate) {
            headerStr = `<div class="f-sidebar-header ${ctx.component.headerTemplateClass}">
                            ${ctx.component.headerTemplate}
                        </div>`;
        } else {
            headerStr = `<div class="f-sidebar-header">
                             ${titleStr} ${hContentStr} ${toolStr}
                         </div>`;
        }
    }

    return headerStr;
}

/**
 * 底部区域
 */
function getFooterStr(ctx: any) {
    let footerStr = '';
    const footerCls = ctx.component.footerContentTemplate ? ' ' + ctx.component.footerContentTemplateClass : '';

    if (ctx.component.showFooter) {
        footerStr = `<div class="f-sidebar-footer ${footerCls}">
                        ${ctx.component.footerContentTemplate}
                    </div>`;
    }

    return footerStr;
}

/**
 * 入口按钮 ---此处不处理属性，始终显示
 */
function getEntryStr(ctx: any) {
    let entryStr = '';
    let entryIconStr = '';
    let entryCls = '';
    if (ctx.component.customEntryTemplate) {
        entryCls = ctx.component.customEntryTemplateClass;
    } else {
        entryIconStr = `<div class="f-sidebar-entry-icon"><span class="f-icon f-sidebar-entry"></span></div>`;
    }
    entryStr = `<div class="f-sidebar-entry-ctr ${entryCls}">
                    ${ctx.component.customEntryTemplate}
                    ${entryIconStr}
                </div> `;

    return entryStr;
}
