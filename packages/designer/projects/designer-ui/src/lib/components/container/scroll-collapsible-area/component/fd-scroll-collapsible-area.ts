import { ScrollCollapsibleAreaSchema } from '../schema/schema';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ScrollCollapsibleAreaProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { OperateUtils } from '../../../../utils/operate.utils';

export default class FdScrollCollapsibleAreaComponent extends FdContainerBaseComponent {
    // 収折状态，默认是展开
    collapseState: boolean = false;
    // 标识当前的legend
    get ScrollCollapsibleAreaKey(): string {
        return `ScrollCollapsibleArea-${this.key}`;
    }
    constructor(component: any, options: any) {
        super(component, options);
    }

    getDefaultSchema(): any {

        return ScrollCollapsibleAreaSchema;
    }

    getTemplateName(): string {
        return 'ScrollCollapsibleArea';
    }

    // 渲染模板
    render(children): any {
        this.collapseState = this.component.expanded === false;

        return super.render(children || this.renderTemplate('ScrollCollapsibleArea', {
            collapseState: this.collapseState,
            ScrollCollapsibleAreaKey: this.ScrollCollapsibleAreaKey,
            children: this.renderComponents(),
            nestedKey: this.nestedKey
        }));
    }
    // 绑定事件
    attach(element: any): any {
        // key 如果有多个用 multi，单个用single
        this.loadRefs(element, {
            [this.ScrollCollapsibleAreaKey]: 'single'
        });
        // 必写
        const superAttach: any = super.attach(element);
        //
        // 収折样式变动容器
        const collapseContainer = this.refs[this.ScrollCollapsibleAreaKey];
        // 初始禁用动画容器
        const collapseInner = collapseContainer.querySelector('.fe-collapsible-container');
        // 注册标签页切换事件
        const collapseBtn = collapseContainer.querySelector('.fe-collapsible-icon-container');
        OperateUtils.removeClass(collapseInner, 'animated-none');
        if (collapseBtn) {
            this.addEventListener(collapseBtn, 'click', (event) => {
                event.stopPropagation();
                this.collapseState = !this.collapseState;
                if (this.collapseState) {
                    OperateUtils.removeClass(collapseContainer, 'f-state-expand');
                    OperateUtils.addClass(collapseContainer, 'f-state-collapse');
                } else {
                    OperateUtils.removeClass(collapseContainer, 'f-state-collapse');
                    OperateUtils.addClass(collapseContainer, 'f-state-expand');
                }
            });
        }
        // 必写
        return superAttach;
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: ScrollCollapsibleAreaProp = new ScrollCollapsibleAreaProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后事件：默认监听样式类属性变更，并触发模板重绘
     * @param changeObject 变更集
     * @param propertyIDs 需要额外监听的属性ID列表
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        if (['expanded', 'contentTemplate'].includes(changeObject.propertyID)) {
            this.triggerRedraw();
        }
    }
}
