import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { ResponseLayoutSplitterComponent } from './editor/response-layout-splitter/response-layout-splitter.component';
import { ControlAppearancePropertyConfig } from '../../../../utils/appearance-property-config';
import { DesignerEnvType } from '@farris/designer-services';

export class FdResponseLayoutProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        let propertyConfig = [];

        // 基本信息属性
        if (this.formBasicService.envType === DesignerEnvType.designer) {
            const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
            propertyConfig.push(basicPropConfig);
        }

        // 外观属性
        const appearancePropConfig = ControlAppearancePropertyConfig.getAppearanceStylePropConfigs(propertyData);
        propertyConfig = propertyConfig.concat(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);


        // 布局属性
        const layoutPropConfig = this.getLayoutPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(layoutPropConfig);


        return propertyConfig;

    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ]
        };
    }

    private getLayoutPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'layout',
            categoryName: '布局',
            properties: [
                {
                    propertyID: 'layout',
                    propertyType: 'custom',
                    description: '配置布局容器内区块个数及比例。',
                    editor: ResponseLayoutSplitterComponent,
                    editorParams: {
                        layout: propertyData.contents || []
                    }
                }
            ]
        };
    }

}
