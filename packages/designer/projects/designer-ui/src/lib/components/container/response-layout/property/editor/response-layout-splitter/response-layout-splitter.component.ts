import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PropertyEntity } from '@farris/ide-property-panel';
import { MessagerService } from '@farris/ui-messager';
import { ResponseLayoutItemSchema } from '../../../schema/schema';
import { cloneDeep } from 'lodash-es';

@Component({
    selector: 'app-response-layout-splitter',
    templateUrl: './response-layout-splitter.component.html',
    styleUrls: ['./response-layout-splitter.component.css']

})
export class ResponseLayoutSplitterComponent implements OnInit {
    @Output() valueChanged = new EventEmitter<any>();

    private _elementConfig: PropertyEntity;

    get elementConfig() {
        return this._elementConfig;
    }

    set elementConfig(value) {
        this._elementConfig = value;
    }
    /** 内置布局 */
    standardLayouts = [
        '12',
        '6:6',
        '3:9',
        '9:3',
        '4:4:4',
        '3:6:3',
        '3:3:6',
        '3:3:3:3',
    ];

    standardLayoutClass = [];
    /** 当前选中布局 */
    selectedLayout = '';

    /** 当前布局容器实际的列数据 */
    currentLayoutItems = [];

    /** 是否标准布局 */
    isStandardLayout = true;

    constructor(private msgService: MessagerService) { }

    ngOnInit() {
        this.initStandardLayoutData();
        this.getCurrentLayoutData();
    }

    private initStandardLayoutData() {
        this.standardLayoutClass = [];
        this.standardLayouts.forEach(item => {
            this.standardLayoutClass.push(item.split(':').map(pro => `col-${pro}`));
        });
    }
    private getCurrentLayoutData() {
        if (!this.elementConfig || !this.elementConfig.editorParams || !this.elementConfig.editorParams.layout) {
            return;
        }
        this.currentLayoutItems = this.elementConfig.editorParams.layout || [];

        if (!this.currentLayoutItems.length) {
            this.selectedLayout = '';
            return;
        }
        this.isStandardLayout = true;
        const proportionList = [];
        this.currentLayoutItems.forEach(layoutItem => {
            if (!layoutItem.appearance) {
                layoutItem.appearance = { class: '' };
            }
            const itemClass = layoutItem.appearance.class || '';

            const colClass = itemClass.split(' ').find(classItem => /^col-([1-9]|10|11|12)$/.test(classItem));
            if (colClass) {
                proportionList.push(parseInt(colClass.replace('col-', ''), 10));
            } else {
                proportionList.push(0);
                this.isStandardLayout = false;
            }
        });

        if (this.isStandardLayout) {
            this.selectedLayout = proportionList.join(':');
        }

    }
    onSelectColumnLayout(column: string) {
        const columnParts = column.split(':');

        if (columnParts.length < this.currentLayoutItems.length && this.hasContentsInRemovingLayouts(columnParts)) {
            this.msgService.question(`应用${columnParts.length}列布局将删除右侧区域内的控件，此操作不可撤销，确定应用？`, () => {
                this.applySelectedColumnLayout(column, columnParts);


            }, () => {
            });
        } else {
            this.applySelectedColumnLayout(column, columnParts);
        }
    }

    private applySelectedColumnLayout(column: string, columnParts: string[]) {
        this.isStandardLayout = true;
        this.selectedLayout = column;

        // tslint:disable-next-line: prefer-for-of
        for (let index = 0; index < columnParts.length; index++) {
            const layoutItem = this.currentLayoutItems[index];
            // 已有列
            if (layoutItem) {
                let itemClass = layoutItem.appearance && layoutItem.appearance.class || '';
                const colClass = itemClass.split(' ').find(classItem => /^col-([1-9]|10|11|12)$/.test(classItem));
                if (colClass) {
                    itemClass = itemClass.replace(colClass, `col-${columnParts[index]}`);
                    layoutItem.appearance.class = itemClass;
                } else {
                    layoutItem.appearance.class += ` col-${columnParts[index]}`;
                }
            } else {
                // 新增列
                const newLayoutItem = cloneDeep(ResponseLayoutItemSchema);
                newLayoutItem.id = `layout_${Math.random().toString().slice(2, 6)}`;
                newLayoutItem.appearance.class += ` col-${columnParts[index]} px-0`;
                this.currentLayoutItems.push(newLayoutItem);
            }

        }
        // 移除后面的列
        this.currentLayoutItems.length = columnParts.length;

        this.valueChanged.next({ $event: null, elementValue: null });
    }

    /**
     * 检测被移除的布局列中是否包含控件
     * @param columnParts 新的布局列配置
     */
    private hasContentsInRemovingLayouts(columnParts: string[]) {

        for (let index = columnParts.length; index < this.currentLayoutItems.length; index++) {
            const layout = this.currentLayoutItems[index];
            if (layout && layout.contents && layout.contents.length) {
                return true;
            }

        }
        return false;

    }
}
