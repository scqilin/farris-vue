import { UniformEditorDataUtil } from '@farris/designer-services';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';

export class LoopContainerProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }

    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                {
                    propertyID: 'dataSource',
                    propertyName: '绑定数据源',
                    propertyType: 'unity',
                    description: '循环绑定数据源',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService)
                        }
                    },
                }
            ]
        };
    }

}
