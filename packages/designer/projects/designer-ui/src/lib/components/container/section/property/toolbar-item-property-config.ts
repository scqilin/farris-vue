import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class SectionToolbarItemProp extends ContainerUsualProp {

    getPropConfig(viewModelId: string, propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig();
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearanceItemPropConfig(propertyData);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorItemPropConfig(propertyData, viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;

    }

    getBasicPropConfig(): ElementPropertyConfig {
        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    readonly: true
                },
                {
                    propertyID: 'title',
                    propertyName: '文本',
                    propertyType: 'string'
                }
            ]
        };
    }

    private getAppearanceItemPropConfig(propertyData: any): ElementPropertyConfig {

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            enableCascade: true,
            parentPropertyID: 'appearance',
            propertyData: propertyData.appearance,
            properties: [
                {
                    propertyID: 'class',
                    propertyName: 'class样式',
                    propertyType: 'string'
                }
            ]
        };
    }

    private getBehaviorItemPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                {
                    propertyID: 'disable',
                    propertyName: '禁用',
                    propertyType: 'unity',
                    description: '是否禁用',
                    editorParams: {
                        controlName: undefined,
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'stateMachine'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            stateMachine: this.stateMachineService.stateMachineMetaData
                        }
                    }
                },
                visibleProp
            ]
        };
    }

    private getEventPropertyConfig(propertyData, viewModelId: string): ElementPropertyConfig {
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            },
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
            }
        };
    }
}
