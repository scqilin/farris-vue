import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PropertyEntity } from '@farris/ide-property-panel';
import { MessagerService } from '@farris/ui-messager';

@Component({
  selector: 'app-form-layout-setting',
  templateUrl: './form-layout-setting.component.html',
  styleUrls: ['./form-layout-setting.component.css']
})
export class FormLayoutSettingComponent implements OnInit {
  @Output() valueChanged = new EventEmitter<any>();

  private _elementConfig: PropertyEntity;

  get elementConfig() {
    return this._elementConfig;
  }

  set elementConfig(value) {
    this._elementConfig = value;
  }

  /** 小屏幕：列数只支持1 */
  allowedSMColumns = [1];

  /** 中等屏幕，列数支持1,2 */
  allowedMDColumns = [1, 2];

  /** 大屏幕，列数支持1,2, 3, 4 */
  allowedLGColumns = [1, 2, 3, 4];

  /** 超大等屏幕，列数支持1,2, 3, 4, 6 */
  allowedELColumns = [1, 2, 3, 4, 6];

  // 各屏幕尺寸的枚举项
  smColumnEnumData = [];
  mdColumnEnumData = [];
  lgColumnEnumData = [];
  elColumnEnumData = [];

  /** 当前卡片控件的column样式值 ，例如col-md-6，则uniqueColumnClass为6 */
  uniqueColumnClass: FormUnifiedColumnLayout;

  /** 当前卡片控件每行控件个数 */
  controlCountInRow = {
    sm: null,
    md: null,
    lg: null,
    el: null
  };

  constructor(private msgService: MessagerService) { }

  ngOnInit() {
    this.initStandardColumEnumData();
    this.initLayoutSetting();
  }
  initLayoutSetting() {

    if (!this.elementConfig.beforeOpenModal) {
      return;
    }
    const result = this.elementConfig.beforeOpenModal();
    this.uniqueColumnClass = result.parameters;

    // 小屏幕：列数只支持1
    const controlCountInSMRow = 12 / this.uniqueColumnClass.uniqueColClassInSM;
    if (!this.allowedSMColumns.includes(controlCountInSMRow)) {
      this.smColumnEnumData.push({ value: 0, text: '自定义' });
      this.controlCountInRow.sm = 0;
    } else {
      this.controlCountInRow.sm = controlCountInSMRow;
    }

    // 中等屏幕，列数支持1,2
    const controlCountInMDRow = 12 / this.uniqueColumnClass.uniqueColClassInMD;
    if (!this.allowedMDColumns.includes(controlCountInMDRow)) {
      this.mdColumnEnumData.push({ value: 0, text: '自定义' });
      this.controlCountInRow.md = 0;
    } else {
      this.controlCountInRow.md = controlCountInMDRow;
    }

    // 大屏幕，列数支持1,2, 3, 4
    const controlCountInLGRow = 12 / this.uniqueColumnClass.uniqueColClassInLG;
    if (!this.allowedLGColumns.includes(controlCountInLGRow)) {
      this.lgColumnEnumData.push({ value: 0, text: '自定义' });
      this.controlCountInRow.lg = 0;
    } else {
      this.controlCountInRow.lg = controlCountInLGRow;
    }

    // 超大等屏幕，列数支持1,2, 3, 4, 6
    const controlCountInELRow = 12 / this.uniqueColumnClass.uniqueColClassInEL;
    if (!this.allowedELColumns.includes(controlCountInELRow)) {
      this.elColumnEnumData.push({ value: 0, text: '自定义' });
      this.controlCountInRow.el = 0;
    } else {
      this.controlCountInRow.el = controlCountInELRow;
    }


  }

  initStandardColumEnumData() {
    this.allowedSMColumns.forEach(c => {
      this.smColumnEnumData.push({ value: c, text: c + '' });
    });
    this.allowedMDColumns.forEach(c => {
      this.mdColumnEnumData.push({ value: c, text: c + '' });
    });
    this.allowedLGColumns.forEach(c => {
      this.lgColumnEnumData.push({ value: c, text: c + '' });
    });
    this.allowedELColumns.forEach(c => {
      this.elColumnEnumData.push({ value: c, text: c + '' });
    });
  }

  changeValue() {

    this.msgService.question('应用统一布局，将重置区域内部所有控件的宽度样式，确定应用？', () => {
      this.uniqueColumnClass.uniqueColClassInSM = this.controlCountInRow.sm === 0 ? null : 12 / this.controlCountInRow.sm;
      this.uniqueColumnClass.uniqueColClassInMD = this.controlCountInRow.md === 0 ? null : 12 / this.controlCountInRow.md;
      this.uniqueColumnClass.uniqueColClassInLG = this.controlCountInRow.lg === 0 ? null : 12 / this.controlCountInRow.lg;
      this.uniqueColumnClass.uniqueColClassInEL = this.controlCountInRow.el === 0 ? null : 12 / this.controlCountInRow.el;

      this.valueChanged.next({ $event: null, elementValue: this.uniqueColumnClass });
    });

  }
}

export class FormUnifiedColumnLayout {
  uniqueColClassInSM: number;
  uniqueColClassInMD: number;
  uniqueColClassInLG: number;
  uniqueColClassInEL: number;
}
