import { DesignerEnvType, FormBasicService } from '@farris/designer-services';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import FdTabToolBarComponent from '../../tab/component/fd-tab-toolbar';
// import { NoCodeSectionToolBarProp } from '../property/nocode-toolbar-property-config';
import { SectionToolBarProp } from '../property/toolbar-property-config';
import { SectionToolbarItemSchema, SectionToolbarSchema } from '../schema/schema';
import FdSectionToolBarItemComponent from './fd-section-toolbar-item';
export default class FdSectionToolBarComponent extends FdTabToolBarComponent {
    /** 按钮对齐方式 */
    justifyContentEnd = true;
    envType: DesignerEnvType;

    constructor(component: any, options: any) {
        super(component, options);
        const serviceHost = this.options.designerHost;
        const formBasicService = serviceHost.getService('FormBasicService') as FormBasicService;
        if (formBasicService) {
            this.envType = formBasicService.envType;
        }
        if (this.envType !== DesignerEnvType.noCode) {
            this.customToolbarConfigs = [
                {
                    id: 'appendItem',
                    title: '新增按钮',
                    icon: 'f-icon f-icon-plus-circle text-white',
                    click: (e) => {
                        e.stopPropagation();
                        this.appendItem(SectionToolbarItemSchema);
                    }
                }
            ];
        }
    }

    getDefaultSchema(): any {
        return SectionToolbarSchema;
    }

    getStyles(): string {
        return 'flex-grow: 1; width: 100%; position: relative';
    }

    /**
     * Section内部的toolbar不能移动
     */
    checkCanMoveComponent(): boolean {
        return false;
    }
    /**
     * Section内部的toolbar不能删除
     */
    checkCanDeleteComponent(): boolean {
        return false;
    }
    init(): void {
        // 初始化方法
        const options = Object.assign({}, this.options, { childComponents: [], parent: this });
        this.toolBarItemList = this.items.map(item => {
            // this.createComponent(item, this.options, null);
            this.toolBarItem = new FdSectionToolBarItemComponent(item, options);
            return this.toolBarItem;
        });

        this.justifyContentEnd = this.component['position'] === 'inHead';
    }
    attach(element: HTMLElement): Promise<any> {
        const superAttach = super.attach(element);
        this.addEventListener(this.element, 'click', (event) => {
            event.stopPropagation();
            // 手动触发点击组件事件  展示属性面板
            this.parent.emit('componentClicked', {
                componentInstance: this
            });
            if (this.type === 'SectionToolbar') {
                this.parent.isToolbarSelected = true;
            }
        });

        setTimeout(() => {
            // 增加判断条件：当前页面上没有选中toolbar外的控件时，才触发按钮的点击。场景是先选中子表弹出编辑区域内的headertoolbar按钮，再选中父级tabPage，若没有这个判断，则焦点不能转移到TabPage上
            if (document.getElementsByClassName('dgComponentSelected').length === 0 && this.parent.isToolbarSelected) {
                this.element.click();
            }
        });
        return superAttach;
    }
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options && this.options.designerHost;
        // if (this.envType === DesignerEnvType.noCode) {
        //     const prop = new NoCodeSectionToolBarProp(serviceHost, this.viewModelId, this.componentId);
        //     const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        //     return propertyConfig;
        // } else {
        const prop = new SectionToolBarProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
        // }

    }

    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        switch (changeObject.propertyID) {
            case 'title': case 'class': case 'disable': {
                this.triggerRedraw();
                break;
            }
            case 'position': case 'contents': {
                this.parent.triggerRedraw();
            }
        }
    }

    confirmRemoveToolbarItem(id: string) {
        super.confirmRemoveToolbarItem(id);

        // 触发section的重绘，因为需要显示section添加按钮的图标
        if (this.items.length === 0) {
            this.parent.redraw();
        }
    }

    /**
     * 取消点击输入控件后，移除标签区域的编辑特性
     */
    afterComponentCancelClicked(e?: PointerEvent): void {
        this.parent.isToolbarSelected = false;
    }
}
