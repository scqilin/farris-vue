export const DynamicAreaSchema = {
    id: 'dynamicArea',
    type: 'DynamicArea',
    appearance: null,
    contents: [],
    visible: true
};


export const DynamicAreaItemSchema = {
    id: '',
    type: 'DynamicAreaItem',
    title: '',
    contents: [],
    visible: true
};

