import { contextMenu } from './context-menu-config';
import { cloneDeep } from 'lodash-es';
import { RowNode } from '@farris/ui-treetable';
import { DgControl } from '../../../../utils/dg-control';
import { ContextMenuManager } from '../../../../service/context-menu.manager';
import { DesignViewModelField, FormBinding, FormVariable } from '@farris/designer-services';
import { ControlContextMenuItem } from '../../../../entity/control-context-menu';
import { FormContextMenuManager } from '../../form/context-menu/context-menu.manager';
import { FormBasicService } from '@farris/designer-services';

const REMOVE_FIELDSET = 'removeFieldSet';
const ADD_CHILD_CONTENT_COMMAND = 'addChildContent';

class ControlCreationContextByContextMenu {
    bindingValue: FormBinding;
    bindingField: DesignViewModelField | FormVariable;
    componentId: string;
    controlType: string;
    controlParentElement: any;
}

export class FieldSetContextMenuManager extends ContextMenuManager {

    /**
     * 过滤、修改控件树右键菜单
     */
    setContextMenuConfig() {


        let menuConfig = cloneDeep(contextMenu) as ControlContextMenuItem[];
        if (!menuConfig) {
            return [];
        }


        menuConfig = this.assembleCreateChildContainerMenu(menuConfig);

        // 配置菜单点击事件
        this.addContextMenuHandle(menuConfig);

        return menuConfig;
    }
    /**
     * 点击控件树右键菜单
     */
    contextMenuClicked(e: { data: RowNode, menu: ControlContextMenuItem }) {

        const menu = e.menu;

        if (menu.id === REMOVE_FIELDSET) {
            this.removeFieldSet(this.cmpInstance.componentId, this.cmpInstance.id);
            return;
        }
        if (menu.parentMenuId === ADD_CHILD_CONTENT_COMMAND) {
            const formComponentInstance = this.cmpInstance.parent;
            const menuManager = new FormContextMenuManager(formComponentInstance, e.data.parentRowNode);
            menuManager.addChildControl(menu, this.cmpInstance.componentId, this.cmpInstance.component)
            return;
        }
        this.notifyService.warning('暂不支持');
    }

    /**
     * 组装添加子级的菜单
     */
    private assembleCreateChildContainerMenu(menuConfig: ControlContextMenuItem[]) {

        const addChildMenu = menuConfig.find(menu => menu.id == ADD_CHILD_CONTENT_COMMAND);
        if (!addChildMenu) {
            return menuConfig;
        }
        // 根据设计器环境过滤菜单
        const formBasicService = this.serviceHost.getService('FormBasicService') as FormBasicService;
        const envType = formBasicService && formBasicService.envType;
        addChildMenu.children = addChildMenu.children.filter(item => !item.supportedEnvType || item.supportedEnvType === envType);

        return menuConfig;
    }
    /**
     * 解除分组
     * @param componentId 控件所在组件ID
     * @param fieldSetId 分组控件id
     */
    private removeFieldSet(componentId: string, fieldSetId: string) {
        const formComponent = this.domService.getComponentById(componentId);
        const formNode = this.domService.selectNode(formComponent, item => item.type === DgControl.Form.type);
        if (!formNode) {
            // console.log('未检测到Form控件');
            return;
        }
        // 1、 将分组下的控件移动到form中
        const fieldSetNodeIndex = formNode.contents.findIndex(c => c.id === fieldSetId);
        if (fieldSetNodeIndex < 0) {
            // console.log('未在Form控件中检检测到分组控件');
            return;
        }
        const fieldSetNode = formNode.contents[fieldSetNodeIndex];
        formNode.contents.splice(fieldSetNodeIndex, 1, ...fieldSetNode.contents);

        // 2、移除viewModel group信息
        const viewModel = this.domService.getViewModelById(formComponent.viewModel);

        const groupFields = viewModel.fields.filter(field => field.groupId === fieldSetId);
        groupFields.map(field => {
            field.groupId = null;
            field.groupName = null;
        });

        this.notifyService.success('已解除分组');
        this.refreshFormService.refreshFormDesigner.next();
    }
}

