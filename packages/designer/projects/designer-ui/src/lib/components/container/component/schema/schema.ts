
export const ComponentSchema = {
    id: 'component',
    type: 'Component',
    viewModel: '',
    componentType: '',
    appearance: null,
    visible: true,
    onInit: null,
    afterViewInit: null,
    contents: []
};

