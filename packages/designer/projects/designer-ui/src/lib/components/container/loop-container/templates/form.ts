export default (ctx: any) => {
    return `
    <div class="drag-container" id = "${ctx.component.id}" ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container">
        ${ctx.children}
    </div>
      `;
};
