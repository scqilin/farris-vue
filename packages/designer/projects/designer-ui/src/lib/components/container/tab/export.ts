import FdTabComponent from './component/fd-tab';
import FdTabToolBarTemplate from '../../command/toolbar/templates/form';
import FdTabTemplates from './templates';

import { TabSchema, TabPageSchema, TabToolbarSchema, TabToolbarItemSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdTabToolBarComponent from './component/fd-tab-toolbar';
import FdTabToolBarItemComponent from './component/fd-tab-toolbar-item';
import FdTabToolBarItemTemplate from '../../command/toolbar/component/toolbaritem/templates/form';

export const Tab: ComponentExportEntity = {
    type: 'Tab',
    component: FdTabComponent,
    template: FdTabTemplates,
    metadata: TabSchema
};

export const TabPage: ComponentExportEntity = {
    type: 'TabPage',
    metadata: TabPageSchema
};

export const TabToolbar: ComponentExportEntity = {
    type: 'TabToolbar',
    metadata: TabToolbarSchema,
    template: { form: FdTabToolBarTemplate },
    component: FdTabToolBarComponent
};

export const TabToolbarItem: ComponentExportEntity = {
    type: 'TabToolbarItem',
    metadata: TabToolbarItemSchema,
    component: FdTabToolBarItemComponent,
    template: { form: FdTabToolBarItemTemplate }
};
