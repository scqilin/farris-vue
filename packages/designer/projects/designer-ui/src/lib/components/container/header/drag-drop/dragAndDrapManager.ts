import { FarrisDesignBaseNestedComponent } from '@farris/designer-element';

export class HeaderDragDropManager {

    private cmpInstance: FarrisDesignBaseNestedComponent;


    constructor(cmpInstance: FarrisDesignBaseNestedComponent) {
        this.cmpInstance = cmpInstance;
    }

    /**
     * 判断当前容器是否是固定的上下文的中间层级
     */
    checkIsInFixedContextRules() {

        // 父级节点
        const parent = this.cmpInstance.parent && this.cmpInstance.parent.component;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 放在根组件下面的页头、页脚，不支持移除
        if (parentClassList.includes('f-page')) {
            return true;
        }

    }
}
