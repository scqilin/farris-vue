import { BeforeOpenModalResult, ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ImportCmpComponent, ImportCmpConverter } from '@farris/designer-devkit';

export class ExternalContainerProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const customPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(customPropConfig);

        // 内容属性
        const contentPropConfig = this.getContentPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(contentPropConfig);

        return propertyConfig;

    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ]
        };
    }

    private getContentPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;

        return {
            categoryId: 'content',
            categoryName: '内容',
            properties: [
                {
                    propertyID: 'externalCmp',
                    propertyName: '导入组件',
                    propertyType: 'modal',
                    editor: ImportCmpComponent,
                    converter: new ImportCmpConverter(),
                    editorParams: {
                        containerType: propertyData.type,
                        containerId: propertyData.id,
                        useIsolateJs: propertyData.useIsolateJs,
                        relativePath: this.formBasicService.formMetaBasicInfo.relativePath,
                        name: this.formBasicService.formMetaBasicInfo.name
                    },
                    showClearButton: true,
                    afterClickClearButton(oldValue: string) {
                        // 清空外部组件的声明节点
                        self.domService.deleteExternalComponent(oldValue);
                    },
                    beforeOpenModal(): BeforeOpenModalResult {
                        // 取'组件使用独立脚本加载'属性的最新值
                        this.editorParams.useIsolateJs = propertyData.useIsolateJs;
                        return { result: true, message: '' };
                    }
                },
                {
                    propertyID: 'useIsolateJs',
                    propertyName: '组件使用独立脚本加载',
                    propertyType: 'boolean',
                    defaultValue: false,
                    description: '启用独立加载后只允许选择根组件下的命令！'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {

                switch (changeObject.propertyID) {
                    case 'externalCmp': {
                        propData.component = parameters && parameters.component;
                        changeObject.relateChangeProps = [{
                            propertyID: 'component',
                            propertyValue: propertyData.component
                        }];
                        break;
                    }
                    case 'useIsolateJs': {
                        // 同步外部组件节点的useIsolateJs属性
                        self.syncExternalComponent(changeObject.propertyValue, propertyData.externalCmp);

                        if (changeObject.propertyValue) {
                            self.notifyService.warning('启用独立加载后只允许选择根组件下的命令！');
                        }
                        break;
                    }
                }

            }
        };
    }
    /**
     * 同步外部组件节点的useIsolateJs属性
     * @param useIsolateJs 组件是否使用独立脚本加载
     * @param externalCmpId 导入组件的id
     */
    private syncExternalComponent(useIsolateJs: boolean, externalCmpId: string) {
        if (!externalCmpId) {
            return;
        }
        const externalComponnent = this.domService.getExternalComponent(externalCmpId);
        if (externalComponnent && externalComponnent.length) {
            externalComponnent[0].useIsolateJs = useIsolateJs;

            // 截取su路径--用于独立加载js脚本
            if (useIsolateJs && !externalComponnent[0].serviceUnitPath && externalComponnent[0].filePath) {
                const filePathArray = externalComponnent[0].filePath.split('/');
                if (filePathArray.length > 2) {
                    externalComponnent[0].serviceUnitPath = filePathArray[0] + '/' + filePathArray[1];
                }
            }
        }
    }
}
