import { QueryFormSchema } from '../schema/schema';
import { BuilderHTMLElement } from '@farris/designer-element';
import { FormProp } from '../property/property-config';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

/**
 * 查询区域，内部放置输入类控件。目前推荐使用ListFilter和QueryScheme替代。
 */
export default class FdQueryFormComponent extends FdContainerBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);
    }

    getDefaultSchema(): any {
        return QueryFormSchema;
    }

    getTemplateName(): string {
        return 'QueryForm';
    }

    /**
     * 不支持删除
     */
    checkCanDeleteComponent(): boolean {
        return false;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    canAccepts(sourceElement: BuilderHTMLElement): boolean {
        return false;
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: FormProp = new FormProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        super.onPropertyChanged(changeObject, ['controlsInline']);
    }
}
