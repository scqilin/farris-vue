import { ListNavSchema } from '../schema/schema';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ListNavProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import { BuilderHTMLElement } from '@farris/designer-element';
import { DgControl } from '../../../../utils/dg-control';

export default class FdListNavComponent extends FdContainerBaseComponent {
    /** 默认収折的状态 */
    hiddenState: boolean;

    /** 设计器中控件当前是否收折状态 */
    hiddenInDesigner: boolean;

    // 标识当前的legend
    get listNavKey(): string {
        return `listNav-${this.key}`;
    }
    /** 判断当前组件是否是固定的上下文层级，这种组件不支持移动 */
    private isInFixedContextRules = false;

    constructor(component: any, options: any) {
        super(component, options);
        this.checkIsInFixedContextRules();

        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('list-nav.css');
    }

    getStyles(): string {
        return 'display: flex;flex-direction:column;height:100%';
    }
    getDefaultSchema(): any {

        return ListNavSchema;
    }

    getTemplateName(): string {
        return 'ListNav';
    }
    // 初始化
    init(): void {
        this.hiddenState = this.component.hideNav;

        super.init();
    }
    // 渲染模板
    render(children): any {
        return super.render(children || this.renderTemplate('ListNav', {
            hiddenState: this.hiddenState,
            listNavKey: this.listNavKey,
            children: this.renderComponents(),
            nestedKey: this.nestedKey
        }));
    }
    // 绑定事件
    attach(element: any): any {
        // key 如果有多个用 multi，单个用single
        this.loadRefs(element, {
            [this.listNavKey]: 'single'
        });
        // 必写
        const superAttach: any = super.attach(element);

        // 注册收折按钮点击事件
        const btnEl = this.refs[this.listNavKey].querySelector('.f-list-nav-toggle-sidebar');
        if (btnEl) {
            this.addEventListener(btnEl, 'click', (event) => {
                event.preventDefault();
                event.stopPropagation();
                this.hiddenState = !this.hiddenState;

                this.redraw();
            });
        }

        if (this.hiddenState) {
            this.element.style.width = '0px';
        }
        // 必写
        return superAttach;
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: ListNavProp = new ListNavProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后事件：默认监听样式类属性变更，并触发模板重绘
     * @param changeObject 变更集
     * @param propertyIDs 需要额外监听的属性ID列表
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        super.onPropertyChanged(changeObject, ['title', 'showEntry', 'position']);
    }

    private checkIsInFixedContextRules() {
        // 控件本身样式
        const cmpClass = this.component.appearance && this.component.appearance.class || '';
        const cmpClassList = cmpClass.split(' ');

        // 父级节点
        const parent = this.parent && this.parent.component;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 带导航的列表和带导航的卡片模板中，ListNav不允许移动
        if (cmpClassList.includes('f-page-content-nav') && parentClassList.includes('f-page-content')) {
            this.isInFixedContextRules = true;
        }

    }
    checkCanMoveComponent(): boolean {
        return !this.isInFixedContextRules;
    }

    checkCanDeleteComponent(): boolean {
        return !this.isInFixedContextRules;
    }

    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {
        const result = super.canAccepts(sourceElement, targetElement);
        if (!result) {
            return false;
        }

        // 子级控件唯一且为外部容器，说明是模板预制的结构，那么不再接收其他控件
        const hasOnlyChild = (this.component.contents || []).length === 1;
        if (!hasOnlyChild) {
            return true;
        }
        const externalContainer = this.component.contents[0];
        if (externalContainer.type === DgControl.ExternalContainer.type) {
            return false;
        }
        return true;
    }
}
