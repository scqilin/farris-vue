export default (ctx: any) => {
  let viewItemsStr = '';

  ctx.component.contents.forEach((viewContainerItem, index) => {
    const viewItemCls = ctx.currentItemType === viewContainerItem.id ? '' : ' f-viewchange-view-none';
    viewItemsStr += `
    <div viewItem ="${viewContainerItem.id}"
        viewGroupId="${ctx.component.viewGroupIdentify}"
        class="f-multiview-item ${viewItemCls}"
        id="${viewContainerItem.id}"
        style="display: inherit;flex: 1; flex-direction: inherit;">

          <div class="drag-container" id="dragcontainer-${viewContainerItem.id}" ref="${ctx.viewItemKey}" dragref="${viewContainerItem.id}-container">
            ${ctx.viewContainerItems[index]}
          </div>

    </div>`;
  });

  return viewItemsStr;
};
