import { BeforeOpenModalResult, ElementPropertyConfig } from '@farris/ide-property-panel';
import { ImportCmpComponent, ImportCmpConverter } from '@farris/designer-devkit';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class ModalContainerProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 内容属性
        const contentPropConfig = this.getContentPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(contentPropConfig);

        return propertyConfig;

    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ]
        };
    }

    private getContentPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        return {
            categoryId: 'content',
            categoryName: '内容',
            properties: [
                {
                    propertyID: 'contentType',
                    propertyName: '内容类型',
                    propertyType: 'select',
                    iterator: [{ key: 'form', value: '内嵌表单' }, { key: 'url', value: 'URL' }],
                    defaultValue: 'form'
                },
                {
                    propertyID: 'externalCmp',
                    propertyName: '导入组件',
                    propertyType: 'modal',
                    editor: ImportCmpComponent,
                    converter: new ImportCmpConverter(),
                    editorParams: {
                        containerType: propertyData.type,
                        containerId: propertyData.id,
                        useIsolateJs: propertyData.useIsolateJs,
                        relativePath: this.formBasicService.formMetaBasicInfo.relativePath,
                        name: this.formBasicService.formMetaBasicInfo.name
                    },
                    showClearButton: true,
                    afterClickClearButton(oldValue: string) {
                        // 清空外部组件的声明节点
                        self.domService.deleteExternalComponent(oldValue);
                    },
                    beforeOpenModal(): BeforeOpenModalResult {
                        // 取"组件使用独立脚本加载"属性的最新值
                        this.editorParams.useIsolateJs = propertyData.useIsolateJs;
                        return { result: true, message: '' };
                    },
                    visible: propertyData.createType === 'form' || propertyData.contentType !== 'url'
                },
                {
                    propertyID: 'url',
                    propertyName: 'URL',
                    propertyType: 'string',
                    visible: propertyData.createType === 'url' || propertyData.contentType === 'url'

                },
                {
                    propertyID: 'useIsolateJs',
                    propertyName: '组件使用独立脚本加载',
                    propertyType: 'boolean',
                    defaultValue: false,
                    visible: propertyData.contentType !== 'url',
                    description: '启用独立加载后只允许选择根组件下的命令！'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'externalCmp': {
                        propData.component = parameters && parameters.component;

                        const dstExternalComponent = self.domService.getExternalComponentByContainerId(propertyData.id);
                        if (dstExternalComponent) {
                            dstExternalComponent.contentType = 'form'; // 同步contentType
                        }
                        changeObject.relateChangeProps = [{
                            propertyID: 'component',
                            propertyValue: propertyData.component
                        }];
                        break;
                    }
                    case 'contentType': {
                        this.properties.map(p => {
                            switch (p.propertyID) {
                                case 'externalCmp': {
                                    p.visible = changeObject.propertyValue === 'form';
                                    break;
                                }
                                case 'url': {
                                    p.visible = changeObject.propertyValue === 'url';
                                    break;
                                }
                                case 'useIsolateJs': {
                                    p.visible = changeObject.propertyValue === 'form';
                                    break;
                                }
                            }
                        });


                        const dstExternalComponent = self.domService.getExternalComponentByContainerId(propertyData.id);
                        if (dstExternalComponent) {
                            dstExternalComponent.contentType = changeObject.propertyValue; // 同步contentType
                            if (changeObject.propertyValue === 'url' && propertyData.url) {
                                dstExternalComponent.url = propertyData.url;  // 同步 url(解决切换contentType到url, 但不手动设置url属性值)
                            }
                        }

                        // 内容类型为url时，不支持独立脚本加载
                        if (changeObject.propertyValue === 'url') {
                            propertyData.useIsolateJs = false;
                            changeObject.relateChangeProps = [{
                                propertyID: 'useIsolateJs',
                                propertyValue: propertyData.useIsolateJs
                            }];

                            self.syncExternalComponent(false, propertyData.externalCmp);
                        }
                        break;
                    }
                    case 'url': {
                        self.setExternalComponentUrl(propertyData);
                        break;
                    }
                    case 'useIsolateJs': {
                        self.syncExternalComponent(changeObject.propertyValue, propertyData.externalCmp);
                        if (changeObject.propertyValue) {
                            self.notifyService.warning('启用独立加载后只允许选择根组件下的命令！');
                        }
                        break;
                    }
                }

            }
        };
    }


    /**
     * 同步外部组件节点的useIsolateJs属性
     * @param useIsolateJs 组件是否使用独立脚本加载
     * @param externalCmpId 导入组件的id
     */
    private syncExternalComponent(useIsolateJs: boolean, externalCmpId: string) {
        if (!externalCmpId) {
            return;
        }
        const externalComponnent = this.domService.getExternalComponent(externalCmpId);
        if (externalComponnent && externalComponnent.length) {
            externalComponnent[0].useIsolateJs = useIsolateJs;

            // 截取su路径--用于独立加载js脚本
            if (useIsolateJs && !externalComponnent[0].serviceUnitPath && externalComponnent[0].filePath) {
                const filePathArray = externalComponnent[0].filePath.split('/');
                if (filePathArray.length > 2) {
                    externalComponnent[0].serviceUnitPath = filePathArray[0] + '/' + filePathArray[1];
                }
            }
        }
    }

    /**
     * 在表单 DOM 上创建 externalComponent元素
     */
    private setExternalComponentUrl(propertyData: any) {
        const externalComponent = this.domService.getExternalComponentByContainerId(propertyData.id);
        if (externalComponent) {
            Object.assign(externalComponent, {
                type: propertyData.type,
                containerId: propertyData.id,
                contentType: propertyData.contentType,
                url: propertyData.url || ''
            });
        } else {
            const externalInfo = {
                type: propertyData.type,
                id: this.idService.generate(),
                containerId: propertyData.id,
                contentType: propertyData.contentType,
                url: propertyData.url || ''
            };

            this.domService.setExternalComponent(externalInfo, null);
        }
    }
}
