import { ElementPropertyConfig } from "@farris/ide-property-panel";
import { DgControl } from "../../../../utils/dg-control";
import { ContainerUsualProp } from "../../common/property/container-property-config";
import { FormLayoutSettingComponent } from "./editor/form-layout-setting/form-layout-setting.component";
import { FormLayoutSettingService } from "./editor/form-layout-setting/form-layout-setting.service";
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class FormProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const formLayoutService = new FormLayoutSettingService(this.domService, this.componentId);

        const commonProps = this.getCommonAppearanceProperties();

        let properties = commonProps;

        // 下级节点中包含table控件，则不允许配置标签独占和布局属性
        const formElement = this.domService.domDgMap.get(propertyData.id);
        const table = formElement.contents.find(c => c.type === DgControl.Table.type);
        if (!table) {
            properties = properties.concat([
                {
                    propertyID: 'formAutoIntl',
                    propertyName: '控件布局响应国际化',
                    propertyType: 'boolean',
                    description: '响应国际化：中文环境下控件标签与输入框在一行展示，其他语言环境下控件标签与输入框上下排列。'
                },
                {
                    propertyID: 'controlsInline',
                    propertyName: '控件标签与输入框在一行展示',
                    propertyType: 'boolean',
                    description: '控件标签与输入框是否在一行展示。控件布局不启用响应国际化时，此属性有效。',
                    refreshPanelAfterChanged: true,
                    visible: !propertyData.formAutoIntl
                },
                {
                    propertyID: 'labelAutoOverflow',
                    propertyName: '控件标签换行',
                    propertyType: 'boolean',
                    description: '控件标签字数超长时，换行显示。控件标签与输入框在一行展示时，此属性有效。',
                    visible: propertyData.formAutoIntl || propertyData.controlsInline
                },
                {
                    propertyID: 'unifiedLayout',
                    propertyName: '统一布局配置',
                    propertyType: 'custom',
                    description: '统一配置卡片区域内所有控件的宽度，只支持标准模式',
                    editor: FormLayoutSettingComponent,
                    beforeOpenModal: () => formLayoutService.assembleUnifiedLayoutContext(propertyData)
                }
            ]);
        }

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                switch (changeObject.propertyID) {
                    case 'formAutoIntl': {
                        const controlsInline = this.properties.find(p => p.propertyID === 'controlsInline');
                        if (controlsInline) {
                            controlsInline.visible = !changeObject.propertyValue;
                        }
                        const labelAutoOverflow = this.properties.find(p => p.propertyID === 'labelAutoOverflow');
                        if (labelAutoOverflow) {
                            labelAutoOverflow.visible = changeObject.propertyValue || propertyData.controlsInline;
                        }
                        self.setClassAfterAppearanceChanged(propertyData);
                        break;
                    }
                    case 'controlsInline': {
                        const labelAutoOverflow = this.properties.find(p => p.propertyID === 'labelAutoOverflow');
                        if (labelAutoOverflow) {
                            labelAutoOverflow.visible = propertyData.formAutoIntl || changeObject.propertyValue;
                        }
                        self.setClassAfterAppearanceChanged(propertyData);

                        break;
                    }
                    case 'unifiedLayout': {
                        const formNode = self.domService.domDgMap.get(propertyData.id);
                        formLayoutService.changeFormControlsByUnifiedLayoutConfig(formNode, changeObject.propertyValue);

                        self.refreshFormService.refreshFormDesigner.next(propertyData.id);
                        break;
                    }

                    case 'labelAutoOverflow': {
                        self.setLabelAutoOverflow(propertyData, changeObject.propertyValue);

                    }
                }
            }
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ],
        };
    }


    private setLabelAutoOverflow(propertyData: any, enableLableAutoOverflow: boolean) {
        const formNode = this.domService.domDgMap.get(propertyData.id);
        if (formNode && formNode.contents && formNode.contents.length) {
            formNode.contents.forEach(control => {
                if (!control.appearance) {
                    control.appearance = {
                        class: ''
                    };
                }
                if (!control.appearance.class) {
                    control.appearance.class = '';
                }
                if (enableLableAutoOverflow) {
                    if (!control.appearance.class.includes('farris-group-multi-label')) {
                        control.appearance.class = control.appearance.class + ' farris-group-multi-label';
                    }
                } else {
                    control.appearance.class = control.appearance.class.replace('farris-group-multi-label', '');
                }
            });
        }

        this.refreshFormService.refreshFormDesigner.next(propertyData.id);
    }

    /**
     * 改变【控件布局响应国际化】【控件标签与输入框在一行展示】 两个属性后，重新设置控件class样式
     */
    private setClassAfterAppearanceChanged(propertyData: any) {
        const appearance = Object.assign({}, propertyData.appearance);
        if (!appearance.class) {
            appearance.class = '';
        }
        appearance.class = appearance.class.replace('farris-form-controls-inline', '');

        // 启用国际化适配：根据当前语言来判断
        if (propertyData.formAutoIntl) {
            const isZhCHSLanguage = localStorage && localStorage.getItem('languageCode') || 'zh-CHS';
            if (isZhCHSLanguage === 'zh-CHS') {
                appearance.class = appearance.class + ' farris-form-controls-inline';
            }
        } else {
            // 未启用国际化适配：根据controlsInline属性来判断
            if (propertyData.controlsInline) {
                appearance.class = appearance.class + ' farris-form-controls-inline';
            }
        }
        propertyData.appearance = appearance;

    }
}
