import { BeforeOpenModalResult, ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { MultiViewEditorComponent } from './editor/multi-view-editor/multi-view-editor.component';
import { MultiViewConverter } from './editor/multi-view-editor/multi-view-converter';

export class MultiViewContainerProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;
    }

    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'dataSource',
                    propertyName: '绑定数据源',
                    propertyType: 'string',
                    readonly: propertyData.contents && propertyData.contents.length
                },
                {
                    propertyID: 'contents',
                    propertyName: '视图',
                    propertyType: 'modal',
                    editor: MultiViewEditorComponent,
                    editorParams: {
                        dataSource: propertyData.dataSource,
                        multiContainerViewModelId: viewModelId
                    },
                    converter: new MultiViewConverter(),
                    beforeOpenModal(): BeforeOpenModalResult {
                        if (!propertyData.dataSource) {
                            return { result: false, message: '请先选择绑定数据源！' };
                        }
                        // 参数取最新值
                        this.editorParams.dataSource = propertyData.dataSource;
                        return { result: true, message: '' };

                    }
                },
                {
                    propertyID: 'defaultType',
                    propertyName: '默认显示的视图', // 屏蔽此属性，因为运行时是由viewChange组件上的指令控制视图的显示。
                    propertyType: 'select',
                    iterator: this.getViewChangeDataTypes(propertyData),
                    visible: false
                },
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters: any) {
                if (!changeObject) {
                    return;
                }
                if (changeObject.propertyID === 'contents') {
                    self.syncDefaultTypeAfterContentsChanged(changeObject, data, this.properties);

                    // 属性变更后，触发刷新页面
                    changeObject.needRefreshForm = true;
                }
            }
        };
    }


    private getViewChangeDataTypes(propData: any) {

        if (!propData || !propData.contents || propData.contents.length === 0) {
            return [];
        }
        const iterators = [];
        propData.contents.forEach(data => {
            iterators.push(
                {
                    key: data.id,
                    value: data.title
                }
            );
        });
        return iterators;
    }

    /**
     * 变更视图后更新默认选中视图
     * @param changeObject 视图变更集
     * @param data 当前控件dom结构
     * @param properties 属性配置集合
     */
    private syncDefaultTypeAfterContentsChanged(changeObject: FormPropertyChangeObject, data: any, properties: PropertyEntity[]) {

        const defaultProp = properties.find(p => p.propertyID === 'defaultType');
        if (defaultProp) {
            defaultProp.iterator = this.getViewChangeDataTypes(data);
            if (defaultProp.iterator.findIndex(viewItem => viewItem.key === data.defaultType) < 0) {
                if (defaultProp.iterator.length) {
                    data.defaultType = defaultProp.iterator[0].key;
                } else {
                    data.defaultType = '';
                }
            }
            if (data.currentTypeInDesignerView && defaultProp.iterator.findIndex(viewItem => viewItem.key === data.currentTypeInDesignerView) < 0) {
                if (defaultProp.iterator.length) {
                    data.currentTypeInDesignerView = defaultProp.iterator[0].key;
                } else {
                    data.currentTypeInDesignerView = '';
                }
            }
        }



    }
}
