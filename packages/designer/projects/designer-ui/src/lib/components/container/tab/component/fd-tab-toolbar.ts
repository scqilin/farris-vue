import { DesignerEnvType, FormBasicService } from '@farris/designer-services';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import FdToolBarComponent from '../../../command/toolbar/component/fd-toolbar';
// import { NocodeTabToolBarProp } from '../property/nocode-tab-toolbar-property-config';
import { TabToolBarProp } from '../property/tab-toolbar-property-config';
import { TabToolbarItemSchema, TabToolbarSchema } from '../schema/schema';
import FdTabToolBarItemComponent from './fd-tab-toolbar-item';

export default class FdTabToolBarComponent extends FdToolBarComponent {

    /** 按钮对齐方式 */
    justifyContentEnd = true;
    envType: DesignerEnvType;

    constructor(component: any, options: any) {
        super(component, options);
        this.items = this.component.contents;
        this.viewModelId = this.parent.viewModelId;
        this.componentId = this.parent.componnentId;
        const serviceHost = this.options.designerHost;
        const formBasicService = serviceHost.getService('FormBasicService') as FormBasicService;
        if (formBasicService) {
            this.envType = formBasicService.envType;
        }
        if (this.envType !== DesignerEnvType.noCode) {
            this.customToolbarConfigs = [
                {
                    id: 'appendItem',
                    title: '新增按钮',
                    icon: 'f-icon f-icon-plus-circle text-white',
                    click: (e) => {
                        e.stopPropagation();
                        this.appendItem(TabToolbarItemSchema);
                    }
                }
            ];
        }
    }

    getDefaultSchema(): any {
        return TabToolbarSchema;
    }

    getStyles(): string {
        return 'flex-grow: 1; width: 100%; position: relative';
    }
    /**
     * Tab内部的toolbar不能移动
     */
    checkCanMoveComponent(): boolean {
        return false;
    }
    /**
     * Tab内部的toolbar不能删除
     */
    checkCanDeleteComponent(): boolean {
        return false;
    }
    init(): void {

        this.justifyContentEnd = this.component['position'] === 'inHead';

        // 初始化方法
        const options = Object.assign({}, this.options, { childComponents: [], parent: this });
        this.toolBarItemList = this.items.map(item => {
            // this.createComponent(item, this.options, null);
            this.toolBarItem = new FdTabToolBarItemComponent(item, options);
            return this.toolBarItem;
        });
    }

    attach(element: HTMLElement): Promise<any> {
        const superAttach = super.attach(element);
        this.addEventListener(this.element, 'click', (event) => {
            event.stopPropagation();
            // 手动触发点击组件事件  展示属性面板
            this.parent.emit('componentClicked', {
                componentInstance: this
            });
        });
        return superAttach;
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options && this.options.designerHost;
        // if (this.envType === DesignerEnvType.noCode) {
        //     const prop = new NocodeTabToolBarProp(serviceHost, this.viewModelId, this.componentId);
        //     const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        //     return propertyConfig;
        // } else {
        const prop = new TabToolBarProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
        // }
    }

    emit(eventName: string, fn: any) {
        return this.parent.emit(eventName, fn);
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        if (['title', 'class', 'disable', 'contents'].includes(changeObject.propertyID)) {
            this.triggerRedraw();
        }
    }
}
