import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { RichTextPropEditor } from '@farris/designer-devkit'

export class NoCodeHTMLProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 模板属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData);
        propertyConfig.push(appearancePropConfig);

        return propertyConfig;

    }

    getAppearancePropConfig(propertyData: any): ElementPropertyConfig {
        return {
            categoryId: 'html',
            categoryName: '模板',
            properties: [
                {
                    propertyID: 'html',
                    propertyName: 'html',
                    propertyType: 'modal',
                    description: 'html编辑器',
                    editor: RichTextPropEditor,
                    editorParams: {
                        value: propertyData.html,
                        language: 'html'
                    }
                }
            ]
        };
    }

}
