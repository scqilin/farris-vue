import FdComponentRefComponent from './component/fd-component-ref';
import { ComponentRefSchema } from './schema/schema';

import FdComponentRefTemplates from './templates';
import { ComponentExportEntity } from '@farris/designer-element';


export const ComponentRef: ComponentExportEntity = {
    type: 'ComponentRef',
    component: FdComponentRefComponent,
    template: FdComponentRefTemplates,
    metadata: ComponentRefSchema
};
