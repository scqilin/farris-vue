import FdToolBarItemComponent, { ToolBarItemOptions } from '../../../command/toolbar/component/toolbaritem/component/fd-toolbar-item';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import FdHeaderToolBarComponent from './fd-header-toolbar';
import { ToolBarItemSchema } from '../../../command/toolbar/component/toolbaritem/schema/schema';
import { cloneDeep } from 'lodash-es';
import { HeaderToolBarItemProp } from '../property/toolbar-item-property-config';

export default class FdHeaderToolBarItemComponent extends FdToolBarItemComponent {
    root: FdHeaderToolBarComponent;

    constructor(component: any, options: any, extendOptions?: ToolBarItemOptions) {
        super(component, options, extendOptions);

        this.root = this.getParent();
    }

    init(): void {
        // 新建子级组件(二级按钮)实例
        const options = Object.assign({}, this.options, { childComponents: [], parent: this });
        this.toolBarItemList = (this.items || []).map(item => {
            const toolBarItem = new FdHeaderToolBarItemComponent(item, options, { isChildOfButton: true, viewModelId: this.viewModelId, componentId: this.componentId });
            return toolBarItem;
        });
    }

    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options && this.options.designerHost;
        const prop: HeaderToolBarItemProp = new HeaderToolBarItemProp(serviceHost.getService('Injector'), this.viewModelId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropertyConfig(this.component, !!this.isChildOfButton, !!this.isChildOfToolBar);
        prop.parentButtonId = this.parent.id;
        prop.isChildOfButton = this.isChildOfButton;
        return propertyConfig;
    }


    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        if (this.isChildOfButton) {
            this.root.onButtonPropertyChanged(changeObject, this.component, this.parent.id);
        } else {
            this.root.onButtonPropertyChanged(changeObject, this.component);
        }

    }

    getParent(): FdHeaderToolBarComponent {
        let parent = this.options.parent;
        while (!(parent instanceof FdHeaderToolBarComponent)) {
            parent = parent.options.parent;
        }
        return parent;
    }
    triggerComponentClicked(e: PointerEvent) {
        // 使用父组件的componentClicked事件  传递不同的参数
        const parent = this.getParent();
        parent.selectedToolbarItem = this;

        parent.emit('componentClicked', { componentInstance: this, e, needUpdatePropertyPanel: true });

    }
    /**
     * 新增按钮（新增同级、新增子级）
     */
    appendItem() {
        const newItemMetadata = cloneDeep(ToolBarItemSchema);
        this.root.appendItem(newItemMetadata, this.component.id);
    }
    /**
     * 复制按钮（用于二级按钮的复制）
     * @param id 被复制的按钮id
     */
    cloneToolBarItem(id: string) {
        this.root.cloneToolBarItem(id, this.component.id);
    }
    /**
     * 删除按钮（用于二级按钮的删除）
     * @param id 被删除的按钮id
     */
    removeToolBarItem(id: string) {
        this.root.removeToolBarItem(id, this.component.id);
    }
    /**
     * 二级按钮上移下移
     * @param targetBtnId 当前移动的按钮id
     * @param action 上移/下移
     */
    moveUpAndDown(targetBtnId: string, action: string) {
        // const index = this.items.findIndex(i => i.id === targetBtnId);
        // if (index < 0) {
        //     return;
        // }
        // const targetBtn = this.items[index];
        // if (action === 'moveUp' && index > 0) {
        //     this.items.splice(index, 1);
        //     this.items.splice(index - 1, 0, targetBtn);


        // }

        // if (action === 'moveDown' && index < this.items.length - 1) {
        //     this.items.splice(index, 1);
        //     this.items.splice(index + 1, 0, targetBtn);


        // }
        this.root.moveUpAndDown(targetBtnId, this.component.id, action);
    }
}
