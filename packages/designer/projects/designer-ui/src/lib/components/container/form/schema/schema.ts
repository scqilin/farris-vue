export const FormSchema = {
    id: 'form',
    type: 'Form',
    appearance: {
        class: 'f-form-layout farris-form farris-form-controls-inline'
    },
    visible: true,
    controlsInline: true,
    formAutoIntl: true,
    contents: [],
    labelAutoOverflow: false
};

