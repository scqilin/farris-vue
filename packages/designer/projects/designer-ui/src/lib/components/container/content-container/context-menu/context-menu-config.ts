export const contextMenu = [
    {
        title: '添加从表区域',
        id: 'createDetailContainer'
    },
    {
        title: '添加从从表区域',
        id: 'createGrandContainer'
    },
    {
        title: '添加子级',
        id: 'addChildContent',
        children: [
            {
                title: '模板容器',
                id: 'HtmlTemplate'
            },
            {
                title: '前置任务',
                id: 'ListFilter'
            }
        ]
    },
    {
        title: '拆分区块',
        id: 'splitLikeCardContentainer'
    },
    {
        title: '从表填充',
        id: 'childContainerFill'
    }
];
