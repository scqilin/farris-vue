import FdResponseLayoutComponent from './component/fd-response-layout';
import { ResponseLayoutSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdResponseLayoutTemplates from './templates';
import FdResponseLayoutItemComponent from './component/response-layout-item/component/fd-response-layout-item';


export const ResponseLayout: ComponentExportEntity = {
    type: 'ResponseLayout',
    component: FdResponseLayoutComponent,
    template: FdResponseLayoutTemplates,
    metadata: ResponseLayoutSchema
};

export const ResponseLayoutItem: ComponentExportEntity = {
    type: 'ResponseLayoutItem',
    component: FdResponseLayoutItemComponent,
    // template: FdResponseLayoutTemplates,
    metadata: ResponseLayoutSchema
};
