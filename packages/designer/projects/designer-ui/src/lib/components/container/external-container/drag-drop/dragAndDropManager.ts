import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { DgControl } from '../../../../utils/dg-control';


export class ExternalContainerDragDropManager {

    private cmpInstance: FdContainerBaseComponent;

    constructor(cmpInstance: FdContainerBaseComponent) {
        this.cmpInstance = cmpInstance;
    }

    /**
     * 判断当前容器是否是固定的上下文的中间层级
     */
    checkIsInFixedContextRules() {

        // 父级节点
        const parent = this.cmpInstance.parent && this.cmpInstance.parent.component;

        // 向导详情页下的外部容器
        if (parent.type === DgControl.WizardDetail.type) {
            return true;
        }

        return false;
    }
}
