export const ModalFooterSchema = {
    id: '',
    type: 'ModalFooter',
    visible: true,
    appearance: null,
    toolbarCls: 'col-6',
    toolbarDatas: null,
    toolbarBtnSize: 'lg',
    toolbarPopDirection: 'top',
    upTemplate: '',
    upTemplateCls: '',
    leftTemplate: '',
    leftTemplateCls: '',
    rightTemplate: '',
    rightTemplateCls: ''
};
