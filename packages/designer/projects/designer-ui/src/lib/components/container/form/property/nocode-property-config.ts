import { ElementPropertyConfig } from "@farris/ide-property-panel";
import { ContainerUsualProp } from "../../common/property/container-property-config";
// import { CustomWidthEditorComponent } from '@farris/nocode-designer-plugin';
import { FormLayoutSettingComponent } from "./editor/form-layout-setting/form-layout-setting.component";
import { FormLayoutSettingService } from "./editor/form-layout-setting/form-layout-setting.service";
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DgControl } from "../../../../utils/dg-control";

export class NoCodeFormProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const formElement = this.domService.domDgMap.get(propertyData.id);
        const table = formElement.contents.find(c => c.type === DgControl.Table.type);
        const formLayoutService = new FormLayoutSettingService(this.domService, this.componentId);

        let properties = [
            {
                propertyID: 'id',
                propertyName: '标识',
                propertyType: 'string',
                readonly: true,
                visible: false
            },
            {
                propertyID: 'size',
                propertyName: '尺寸',
                propertyType: 'cascade',
                cascadeConfig: [
                    {
                        propertyID: 'width',
                        propertyName: '宽度（px）',
                        propertyType: 'number',
                        min: 0,
                        decimals: 0
                    },
                    {
                        propertyID: 'height',
                        propertyName: '高度（px）',
                        propertyType: 'number',
                        min: 0,
                        decimals: 0
                    }
                ],
                // cascadeConverter: new SummaryPropConverter()
            },
            // {
            //     propertyID: 'labelSize',
            //     propertyName: '标签宽度（字符）',
            //     propertyType: 'custom',
            //     editor: CustomWidthEditorComponent,
            //     beforeOpenModal: () => this.setInitDefaultValue(propertyData),
            //     visible: !!!table
            // },
            {
                propertyID: 'controlsInline',
                propertyName: '控件标签独占一列',
                propertyType: 'boolean',
                description: '控件标签是否独占一列',
                visible: !!!table
            },
            {
                propertyID: 'unifiedLayout',
                propertyName: '统一布局配置',
                propertyType: 'custom',
                description: '统一配置卡片区域内所有控件的宽度，只支持标准模式',
                editor: FormLayoutSettingComponent,
                beforeOpenModal: () => formLayoutService.assembleUnifiedLayoutContext(propertyData),
                visible: !!!table
            }
        ];

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                let classTemp = propertyData.appearance.class;
                switch (changeObject.propertyID) {
                    case 'labelSize': {
                        if (classTemp.includes('f-form-label')) {
                            propertyData.appearance.class = classTemp.replace(/(f-form-label-)[^\s]+/, changeObject.propertyValue);
                        } else {
                            propertyData.appearance.class = classTemp + ' ' + changeObject.propertyValue;
                        }
                        break;
                    }
                    case 'controlsInline': {
                        const appearance = Object.assign({}, propertyData.appearance);
                        if (changeObject.propertyValue) {
                            if (!appearance.class.includes('farris-form-controls-inline')) {
                                appearance.class = appearance.class + ' ' + 'farris-form-controls-inline';
                            }
                        } else {
                            appearance.class = appearance.class.replace('farris-form-controls-inline', '');
                        }
                        propData.appearance = appearance;
                        break;
                    }
                    case 'unifiedLayout': {
                        const formNode = self.domService.domDgMap.get(propertyData.id);
                        formLayoutService.changeFormControlsByUnifiedLayoutConfig(formNode, changeObject.propertyValue);
                        self.refreshFormService.refreshFormDesigner.next(propertyData.id);
                        break;
                    }
                }
            }
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                {
                    propertyID: 'visible',
                    propertyName: '是否可见',
                    propertyType: 'editableSelect',
                    iterator: [
                        {
                            key: true, value: '是'
                        },
                        {
                            key: false, value: '否'
                        }
                    ]
                }
            ],
        };
    }

    /**
     * 设置标签宽度（字符）控件初始默认值
     * @param propertyData 属性值
     */
    setInitDefaultValue(propertyData: any) {
        const iterator = [
            {
                label: 'f-form-label-el', value: '十四'
            },
            {
                label: 'f-form-label-xl', value: '十'
            },
            {
                label: 'f-form-label-lg', value: '八'
            },
            {
                label: 'f-form-label-ml', value: '六'
            },
            {
                label: 'f-form-label-sm', value: '四'
            }
        ];
        const result = {
            result: true,
            parameters: {
                customOptions: iterator,
                defaultValue: ''
            }
        };
        const value = propertyData.labelSize;
        result.parameters.defaultValue = value ? value : 'f-form-label-ml';
        return result;
    }
}
