import FdHeaderComponent from './component/fd-header';
import { HeaderSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdHeaderTemplates from './templates';
import FdHeaderToolBarComponent from './component/fd-header-toolbar';
import FdHeaderToolBarItemComponent from './component/fd-header-toolbar-item';
import FdHeaderToolBarTemplate from '../../command/toolbar/templates/form';
import FdHeaderToolBarItemTemplate from '../../command/toolbar/component/toolbaritem/templates/form';

export const Header: ComponentExportEntity = {
    type: 'Header',
    component: FdHeaderComponent,
    template: FdHeaderTemplates,
    metadata: HeaderSchema
};

export const HeaderToolBar = {
    type: 'HeaderToolBar',
    component: FdHeaderToolBarComponent,
    template: { form: FdHeaderToolBarTemplate }
};

export const HeaderToolBarItem = {
    type: 'HeaderToolBarItem',
    component: FdHeaderToolBarItemComponent,
    template: { form: FdHeaderToolBarItemTemplate }
};

