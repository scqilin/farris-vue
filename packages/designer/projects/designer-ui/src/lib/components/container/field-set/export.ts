import FdFieldSetComponent from './component/fd-field-set';
import { FieldSetSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdFieldSetTemplates from './templates';


export const FieldSet: ComponentExportEntity = {
    type: 'FieldSet',
    component: FdFieldSetComponent,
    template: FdFieldSetTemplates,
    metadata: FieldSetSchema
};
