import { TabToolbarItemSchema } from './../schema/schema';
import FdToolBarItemComponent from '../../../command/toolbar/component/toolbaritem/component/fd-toolbar-item';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import TabToolBarItemProp from '../property/tab-toolbar-item-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DomService } from '@farris/designer-services';

export default class FdTabToolBarItemComponent extends FdToolBarItemComponent {
    constructor(component: any, options: any) {
        super(component, options);
        this.viewModelId = '';
        this.viewModelId = this.parent.viewModelId;
        this.componentId = this.parent.componnentId;
    }
    getDefaultSchema() {
        return TabToolbarItemSchema;
    }

    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options && this.options.designerHost;
        const prop: TabToolBarItemProp = new TabToolBarItemProp(serviceHost.getService('Injector'), this.viewModelId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropertyConfig(this.component);
        return propertyConfig;
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        this.parent.onPropertyChanged(changeObject);
    }

    /**
     * 设置标签页按钮的展示名称和路径，用于交互面板已绑定事件窗口
     */
    setComponentBasicInfoMap() {
        if (!this.component) {
            return;
        }
        const title = this.component['title'];
        const domService = this.options.designerHost.getService('DomService') as DomService;
        domService.controlBasicInfoMap.set(this.component.id, {
            showName: title,
            parentPathName: `${this.options['parentTabPge'].title} > ${title}`
        });


    }
}
