import FdHiddenContainerComponent from './component/fd-hidden-container';
import { HiddenContainerSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdHiddenContainerTemplates from './templates';


export const HiddenContainer: ComponentExportEntity = {
    type: 'HiddenContainer',
    component: FdHiddenContainerComponent,
    template: FdHiddenContainerTemplates,
    metadata: HiddenContainerSchema
};
