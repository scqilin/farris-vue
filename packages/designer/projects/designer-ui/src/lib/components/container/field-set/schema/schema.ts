export const FieldSetSchema = {
    id: 'fieldSet',
    type: 'FieldSet',
    title: '分组标题',
    appearance: {
        class: 'col-12'
    },
    collapse: false,
    expandText: '',
    collapseText: '',
    contentTemplate: null,
    headerTemplate: null,
    contents: [],
    sectionCollapseVisible: false,
    isScrollSpyItem: false,
    visible: true
};

