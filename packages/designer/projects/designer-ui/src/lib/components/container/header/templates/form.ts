export default (ctx: any) => {

    // 标题区域
    const titleStr = getTitleTemplate(ctx);

    // 中间区域
    const contentStr = getContentTemplate(ctx);

    // 工具栏区域
    const { toolbarStr, viewChangeStr } = getToolbarTemplate(ctx);

    // 下方扩展区域
    const downExtendStr = getDownTemplate(ctx);

    return `
    <div class="f-page-header ide-cmp-header showtype-${ctx.currentFormMode}">
        <nav class="f-page-header-base">
            ${titleStr} ${contentStr} ${toolbarStr} ${viewChangeStr}
        </nav>
        ${downExtendStr}
    </div>`;
};

/**
 * 左侧标题区域
 * @param ctx 上下文构造信息
 */
function getTitleTemplate(ctx: any) {
    const headerOpts = ctx.component;

    // 自定义模板
    if (headerOpts.titleTemplate) {
        return `
        <div class="f-title ${headerOpts.titleTemplateCls}">
            ${headerOpts.titleTemplate}
        </div>`;
    }

    let titleStr = '';
    titleStr = `<div class="f-title">`;

    // 是否显示图标
    if (headerOpts.showIcon) {
        titleStr += `<span  class="f-title-icon ${headerOpts.iconCls}">
                <i class="f-icon ${headerOpts.iconName}"></i>
            </span>`;
    }

    // 主标题
    titleStr += `<h4 class="f-title-text">${ctx.mainTitle}</h4>`;

    // 子标题
    if (headerOpts.subTitle) {
        titleStr += ` <h5 class="f-title-subtitle">${headerOpts.subTitle}</h5>`;
    }

    // 显示分页信息
    if (headerOpts.pgShow) {
        titleStr += `<div class="f-title-pagination">
            <span class="f-icon f-icon-arrow-w ${headerOpts.pgPreDisabled == true ? 'f-state-disabled' : ''}"></span>
            <span class="f-icon f-icon-arrow-e ${headerOpts.pgNextDisabled == true ? 'f-state-disabled' : ''}"></span>
            </div>`;
    }
    titleStr += `</div>`;

    return titleStr;
}

/**
 * 中间区域模板
 * @param ctx 上下文构造信息
 */
function getContentTemplate(ctx: any) {
    const headerOpts = ctx.component;
    let contentStr = '';
    if (headerOpts.contentTemplate) {
        contentStr = `<div class="f-content ${headerOpts.contentTemplateCls}">
            ${headerOpts.contentTemplate}
        </div>`;
    }

    return contentStr;
}

/**
 * 工具栏区域模板
 * @param ctx 上下文构造信息
 */
function getToolbarTemplate(ctx: any) {
    const headerOpts = ctx.component;
    let toolbarStr = '';
    toolbarStr = ctx.headerToolBar.render();

    // 视图切换按钮
    let viewChangeStr = '';
    if (headerOpts.showViewChange && headerOpts.viewDatas.length > 0) {
        viewChangeStr = `<div class="f-view-change" ref="${ctx.viewChangeKey}" viewChange="${headerOpts.viewGroupId}">
            ${ctx.getViewChangeStr()}
        </div>`;
    }

    return { toolbarStr, viewChangeStr };
}

/**
 * 下方扩展区域模板
 * @param ctx 上下文构造信息
 */
function getDownTemplate(ctx: any) {
    const headerOpts = ctx.component;
    let downExtendStr = '';
    if (headerOpts.downTemplate) {
        downExtendStr = `<div class="f-content ${headerOpts.downTemplateCls}">${headerOpts.downTemplate} </div> `;
    }

    return downExtendStr;
}
