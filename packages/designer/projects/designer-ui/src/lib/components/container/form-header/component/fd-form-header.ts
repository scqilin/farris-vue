import { FormHeaderSchema } from '../schema/schema';
import { BuilderHTMLElement } from '@farris/designer-element';
import { FormHeaderProp } from '../property/property-config';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';

/**
 * 页面表头翻页图标。目前已废弃
 */
export default class FdFormHeaderComponent extends FdContainerBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);
    }

    getDefaultSchema(): any {
        return FormHeaderSchema;
    }

    getTemplateName(): string {
        return 'FormHeader';
    }
    hideNestedPaddingInDesginerView(): boolean {
        return true;
    }
    checkCanMoveComponent(): boolean {
        return false;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    canAccepts(sourceElement: BuilderHTMLElement): boolean {
        return false;
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: FormHeaderProp = new FormHeaderProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

}
