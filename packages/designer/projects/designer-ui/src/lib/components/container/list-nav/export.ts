import FdListNavComponent from './component/fd-list-nav';
import { ListNavSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdListNavTemplates from './templates';


export const ListNav: ComponentExportEntity = {
    type: 'ListNav',
    component: FdListNavComponent,
    template: FdListNavTemplates,
    metadata: ListNavSchema
};
