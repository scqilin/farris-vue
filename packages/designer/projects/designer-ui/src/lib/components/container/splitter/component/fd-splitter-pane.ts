import { SplitterPaneSchema } from '../schema/schema';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { BuilderHTMLElement } from '@farris/designer-element';
import { DgControl } from '../../../../utils/dg-control';

export default class FdSplitterPaneComponent extends FdContainerBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);
    }

    getDefaultSchema(): any {
        return SplitterPaneSchema;
    }

    getTemplateName(): string {
        return 'SplitterPane';
    }


    checkCanMoveComponent(): boolean {
        return false;
    }
    checkCanDeleteComponent(): boolean {
        return false;
    }

    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {
        const result = super.canAccept(sourceElement, targetElement);
        if (!result.canAccepts) {
            return false;
        }
        const paneCls = this.component.appearance && this.component.appearance.class || '';
        const paneClsList = paneCls.split(' ') || [];

        // 若SplitterPane是左侧区域（f-page-content-nav， 表单模板中一般放置单个列表或树列表），且已有子级节点，则不接收控件
        if (paneClsList.includes('f-page-content-nav')) {
            const firstChild = this.component.contents && this.component.contents[0];
            if (firstChild && firstChild.type === DgControl.ComponentRef.type) {
                return false;
            } else {
                return true;
            }
        }

        // SplitterPane是右侧区域：不接收DataGrid、TreeGrid、ListView。
        const restrictCollection = [DgControl.DataGrid.type, DgControl.TreeGrid.type, DgControl.ListView.type];
        if (restrictCollection.includes(result.resolveContext.controlType)) {
            return false;
        }

        return true;

    }

    /**
     * 触发整个面板的刷新
     */
    rebuild(): Promise<any> {
        return this.parent.rebuild();
    }
}
