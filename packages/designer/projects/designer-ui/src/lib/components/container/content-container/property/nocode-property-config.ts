import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { NoCodeContainerUsualProp } from '../../common/property/nocode-container-property-config';
import { ControlAppearancePropertyConfig } from '../../../../utils/appearance-property-config';

export class NoCodeContainerProp extends NoCodeContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        let propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 样式属性
        const stylePropConfigs = ControlAppearancePropertyConfig.getAppearanceStylePropConfigs(propertyData);
        propertyConfig = propertyConfig.concat(stylePropConfigs);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }


    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp
            ]
        };
    }
}
