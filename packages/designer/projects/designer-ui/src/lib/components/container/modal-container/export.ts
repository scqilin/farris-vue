import FdModalContainerComponent from './component/fd-modal-container';
import { ModalContainerSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdModalContainerTemplates from './templates';


export const ModalContainer: ComponentExportEntity = {
    type: 'ModalContainer',
    component: FdModalContainerComponent,
    template: FdModalContainerTemplates,
    metadata: ModalContainerSchema
};
