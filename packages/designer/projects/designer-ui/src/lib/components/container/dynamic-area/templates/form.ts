export default (ctx: any) => {
    let headersLi = '';
    let bodys = '';
    ctx.component.contents.forEach((item, index) => {
        const selectedLiCls = ctx.selectedTabPageId === item.id ? 'f-state-active' : '';
        const selectedACls = ctx.selectedTabPageId === item.id ? 'active' : '';
        const selectedBodyCls = ctx.selectedTabPageId === item.id ? 'f-tab-active' : 'd-none';

        // 头部标签页
        headersLi += `
            <li class="nav-item ${selectedLiCls}" ref=${ctx.tabPageLinkKey}>
                <a class="nav-link tabs-text-truncate ${selectedACls}">
                <span class="st-tab-text">${item.title}</span>
                <span class="st-drop-close ml-1" title="删除面板" index="${index}"ref=${ctx.tabPageRemoveLinkKey}>
                    <i class="f-icon f-icon-close"></i>
                </span>
                </a>
            </li>`;

        // 内容区
        bodys += `
            <div id=${item.id} class="farris-tabs-body ${selectedBodyCls}"  ref="${ctx.tabPageBodyKey}">
                ${ctx.tabComponents[index]}
            </div>
        `;
    });

    // 空数据图标
    let emptyPanel = '';
    if (ctx.component.contents.length === 0) {
        emptyPanel = `
            <div class="ide-external-container add-dynamic" ref="${ctx.addIconLinkKey}" title="新增面板"> </div>
        `;
    }
    return `
<div class="f-component-tabs farris-tabs flex-column ide-cmp-dynamic-area">
    <div class="farris-tabs-header farris-tabs-inHead">
        <div class="farris-tabs-title scroll-tabs w-100 d-flex">
            <div class="spacer f-utils-fill" style="width:100%;">
                <ul class="nav farris-nav-tabs flex-nowrap" style="white-space: nowrap;overflow: auto;position: relative;">
                    ${headersLi}
                </ul>
            </div>
        </div>
    </div>

    ${bodys}

    ${emptyPanel}
</div>
      `;
};
