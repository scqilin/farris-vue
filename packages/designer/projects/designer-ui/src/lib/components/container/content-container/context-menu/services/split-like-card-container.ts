import { Injector } from '@angular/core';
import { FarrisDesignBaseComponent, IControlService } from '@farris/designer-element';
import { DesignerEnvType, DgControl, DomService, FormBasicService, RefreshFormService } from '@farris/designer-services';
import { ControlService } from '../../../../../service/control.service';


/**
 * 将一个like-card-container区块拆分为多个区块
 */
export class SplitLikeCardContainerContextMenuService {
    private domService: DomService;
    public formBasicServ: FormBasicService;
    private controlService: IControlService;

    private cmpInstance: FarrisDesignBaseComponent;

    constructor(private injector: Injector, cmpInstance: FarrisDesignBaseComponent) {

        this.domService = this.injector.get(DomService);
        // this.refreshFormService = this.injector.get(RefreshFormService);
        // this.messagerService = this.injector.get(MessagerService);
        this.controlService = this.injector.get(ControlService);
        this.formBasicServ = this.injector.get(FormBasicService);

        this.cmpInstance = cmpInstance;
    }

    /**
     * 校验是否支持拆分区块
     */
    checkCanSplitLikeCardContainer(): boolean {
        // 暂时只在零代码提供
        if (this.formBasicServ.envType !== DesignerEnvType.noCode) {
            return false;
        }
        const containerSchema = this.cmpInstance.component || {};
        const parentSchema = this.cmpInstance.parent && this.cmpInstance.parent.component;

        const containerCls = containerSchema && containerSchema.appearance && containerSchema.appearance.class || '';
        const parentContainerCls = parentSchema && parentSchema.appearance && parentSchema.appearance.class || '';

        // 1、容器样式必须包含f-struct-like-card，属性isLikeCardContainer=true;
        if (!containerCls || !containerCls.includes('f-struct-like-card') || !containerSchema.isLikeCardContainer) {
            return false;
        }
        // 2、区块内要包含多个子级
        if (!containerSchema.contents || !containerSchema.contents.length || containerSchema.contents.length < 2) {
            return false;
        }

        // 3、父级容器为ContentContainer且样式中要包含f-page-main
        if (!parentSchema || !parentContainerCls || !parentContainerCls.includes('f-page-main')) {
            return false;
        }

        return true;

    }

    splitLikeCardContainer() {

        const containerSchema = this.cmpInstance.component || {};
        const parentSchema = this.cmpInstance.parent && this.cmpInstance.parent.component;

        let orignIndex = parentSchema.contents.findIndex(co => co.id === containerSchema.id);

        // 将卡片区块内除第一个外的子级，都拆分出去，并在外层包裹一个新的区块
        const contensShouldBeSplitted = containerSchema.contents.splice(1);
        contensShouldBeSplitted.forEach(co => {
            const container = this.controlService.getControlMetaData(DgControl.ContentContainer.type);

            Object.assign(container, {
                id: co.id + '-container',
                appearance: {
                    class: 'f-struct-like-card'
                },
                isLikeCardContainer: true,
                contents: [co]
            });
            parentSchema.contents.splice(orignIndex + 1, 0, container);
            orignIndex++;
        });


        // 若是零代码环境，还需要追加样式farris-multi-card-page
        if (this.formBasicServ.envType === DesignerEnvType.noCode) {
            const pageContainer = this.domService.selectNode(this.domService.components[0], item => {
                if (item.id !== 'root-layout') {
                    return;
                }
                if (item.appearance && item.appearance.class && item.appearance.class.includes('f-page') && item.appearance.class.includes('farris-oa-page')) {
                    return true;
                }
            });

            const cls = pageContainer && pageContainer.appearance && pageContainer.appearance.class;
            if (pageContainer) {
                if (!cls.includes('farris-multi-card-page')) {
                    pageContainer.appearance.class += ' farris-multi-card-page';

                }
            }
        }
    }

}
