import { DgControl } from '../../../../utils/dg-control';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';

export class SectionDragDropManager {

    private cmpInstance: FdContainerBaseComponent;


    private canAcceptChildContent = true;

    constructor(cmpInstance: FdContainerBaseComponent) {
        this.cmpInstance = cmpInstance;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    canAccepts(): boolean {

        if (!this.canAcceptChildContent) {
            return false;
        }
        return true;
    }

    /**
     * 判断当前容器是否是固定的上下文的中间层级
     */
    checkIsInFixedContextRules() {
        const component = this.cmpInstance.component;
        // 控件本身样式
        const cmpClass = component.appearance && component.appearance.class || '';
        const cmpClassList = cmpClass ? cmpClass.split(' ') : [];

        // 子级节点
        const childContents = component.contents || [];
        const firstChildContent = childContents.length ? childContents[0] : null;
        const firstChildClass = (firstChildContent && firstChildContent.appearance && firstChildContent.appearance.class) || '';
        const firstChildClassList = firstChildClass ? firstChildClass.split(' ') : [];

        // 父级节点
        const parent = this.cmpInstance.parent && this.cmpInstance.parent.component;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 1、卡片区域的section:Component-Section-Form三层结构
        if (cmpClassList.includes('f-section-form') && parent.type === DgControl.Component.type && firstChildClassList.includes('f-form-layout')) {
            this.canAcceptChildContent = false;
            return true;
        }

        // 2、子表区域tab外的section间距
        if (cmpClassList.includes('f-section-tabs') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-component-tabs')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 3、隐藏DataGrid父级section间距、隐藏带筛选条的DataGrid父级Section间距
        if (cmpClassList.includes('f-section-grid') && parentClassList.includes('f-struct-wrapper') && (firstChildClassList.includes('f-component-grid') || firstChildClassList.includes('f-filter-container'))) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 4、隐藏TreeGrid父级section间距
        if (cmpClassList.includes('f-section-treegrid') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-component-treetable')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 5、带导航的表格类填报模板：内部为form-table 时，隐藏间距
        if (cmpClassList.includes('f-section-oa-table') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-form-is-table')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 6、卡片类表格模板：内部为form-table 时，隐藏间距
        if (cmpClassList.includes('f-section-card-table') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-form-is-table')) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 7、筛选方案的外层Section
        if (cmpClassList.includes('f-section-scheme') && firstChildContent && firstChildContent.type === DgControl.QueryScheme.type) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 8、section类子表：container-section-component
        if ((cmpClassList.includes('f-section-in-main') || cmpClassList.includes('f-section-in-mainsubcard')) &&
            parentClassList.includes('f-struct-wrapper') && firstChildContent && firstChildContent.type === DgControl.ComponentRef.type) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 7、section类附件：component-section-FileUploadPreview
        if ((cmpClassList.includes('f-section-in-main') || cmpClassList.includes('f-section-in-mainsubcard')) &&
            parentClassList.includes('f-struct-wrapper') && firstChildContent && firstChildContent.type === DgControl.FileUploadPreview.type) {
            this.canAcceptChildContent = false;
            return true;
        }
        // 审批类
        if (firstChildContent && (firstChildContent.type === DgControl.ApprovalComments.type || firstChildContent.type === DgControl.ApprovalLogs.type)) {
            this.canAcceptChildContent = false;
            return false;
        }
        // 3、隐藏预约日历父级section间距
        if (cmpClassList.includes('f-section-grid') && parentClassList.includes('f-struct-wrapper') && firstChildClassList.includes('f-component-appointment-calendar')) {
            this.canAcceptChildContent = false;
            return true;
        }

    }
}
