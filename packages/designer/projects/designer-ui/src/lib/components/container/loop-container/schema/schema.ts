export const LoopContainerSchema = {
    id: 'loopContainer',
    type: 'LoopContainer',
    appearance: null,
    dataSource: null,
    contents: []
};

