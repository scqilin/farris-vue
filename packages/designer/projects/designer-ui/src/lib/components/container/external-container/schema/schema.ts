export const ExternalContainerSchema = {
    id: 'external-container',
    type: 'ExternalContainer',
    appearance: {
        class: 'position-relative'
    },
    visible: true,
    useIsolateJs: false
};

