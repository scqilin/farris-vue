import { ComponentExportEntity } from '@farris/designer-element';
import FdLoopContainerComponent from './component/fd-loop-container';
import { LoopContainerSchema } from './schema/schema';

import FdLoopContainerTemplates from './templates';

/**
 * 循环容器
 */
export const LoopContainer: ComponentExportEntity = {
    type: 'LoopContainer',
    component: FdLoopContainerComponent,
    template: FdLoopContainerTemplates,
    metadata: LoopContainerSchema
};
