import { ExternalContainerSchema } from '../schema/schema';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ExternalContainerProp } from '../property/property-config';
import { BuilderHTMLElement } from '@farris/designer-element';
import { ExternalContainerDragDropManager } from '../drag-drop/dragAndDropManager';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeExternalContainerProp } from '../property/nocode-property-config';

export default class FdExternalContainerComponent extends FdContainerBaseComponent {

    /** 判断当前容器是否是固定的上下文的中间层级，这种容器不支持移动、不支持删除、并且隐藏间距 */
    private isInFixedContextRules = false;
    private dragManager: ExternalContainerDragDropManager;


    constructor(component: any, options: any) {
        super(component, options);

        this.dragManager = new ExternalContainerDragDropManager(this);
        this.isInFixedContextRules = this.dragManager.checkIsInFixedContextRules();
    }

    getStyles(): string {
        return ' position:relative; padding:0 !important;border:0;';
    }
    getDefaultSchema(): any {

        return ExternalContainerSchema;
    }

    getTemplateName(): string {
        return 'ExternalContainer';
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;

        if (this.envType === DesignerEnvType.noCode) {
            const prop: NoCodeExternalContainerProp = new NoCodeExternalContainerProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        } else {
            const prop: ExternalContainerProp = new ExternalContainerProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        }
    }

    /**
     * 不接收子组件
     */
    canAccepts(sourceElement: BuilderHTMLElement, targetElement: BuilderHTMLElement): boolean {
        return false;
    }


    checkCanMoveComponent(): boolean {
        const canMove: boolean = super.checkCanMoveComponent();
        if (!canMove) {
            return false;
        }
        if (this.isInFixedContextRules) {
            return false;
        }
        return true;
    }
    /**
     * 删除组件时需要同步清空外部组件的声明节点
     */
    onRemoveComponent(): void {
        if (!this.component.externalCmp) {
            return;
        }

        const domService = this.options.designerHost.getService('DomService');

        domService.deleteExternalComponent(this.component.externalCmp);
    }
}
