export const SidebarSchema = {
    id: 'sidebar',
    type: 'Sidebar',
    appearance: null,
    position: 'right',
    showHeader: true,
    showEntry: false,
    maskable: false,
    maskClosable: true,
    iconCls: null,
    width: null,
    height: null,
    headerTemplate: '',
    headerTemplateClass: '',
    headerTitleTemplate: '',
    headerTitleTemplateClass: '',
    headerContentTemplate: '',
    headerContentTemplateClass: '',
    toolbar: {
        id: 'toolbar',
        type: 'ToolBar',
        appearance: {
            class: ''
        },
        items: []
    },
    toolbarTemplateClass: 'col-7',
    customEntryTemplate: '',
    customEntryTemplateClass: '',
    contents: [],
    visible: true,
    showClose: true,
    showFooter: true,
    footerContentTemplate: '',
    footerContentTemplateClass: '',
    changeState: null,
    contentTemplateClass: '',
    beforeCloseSidebar: null
};

