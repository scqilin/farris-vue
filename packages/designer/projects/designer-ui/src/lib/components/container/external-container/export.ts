import FdExternalContainerComponent from './component/fd-external-container';
import { ExternalContainerSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdExternalContainerTemplates from './templates';


export const ExternalContainer: ComponentExportEntity = {
    type: 'ExternalContainer',
    component: FdExternalContainerComponent,
    template: FdExternalContainerTemplates,
    metadata: ExternalContainerSchema
};
