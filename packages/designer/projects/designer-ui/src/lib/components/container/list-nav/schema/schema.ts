export const ListNavSchema =  {
    id: 'list-nav',
    type: 'ListNav',
    appearance: null,
    title: '导航',
    size: {
        width: 240
    },
    contents: [],
    hideNav: false,
    position: 'left',
    showEntry: true,
    visible: true,
    enableDraggable: false,
    rzHandles: 'e',
    relatedIframeParent: null,
    rzStart: null,
    rzResizing: null,
    rzStop: null
};
