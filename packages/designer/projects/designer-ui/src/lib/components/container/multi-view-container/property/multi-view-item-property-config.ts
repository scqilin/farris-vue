import { ElementPropertyConfig } from '@farris/ide-property-panel';
/**
 * 多视图项的属性配置
 */
export class MultiViewItemProp {

    getPropertyConfig(): ElementPropertyConfig[] {
        return [
            {
                categoryId: 'usual',
                categoryName: '常规',
                hideTitle: true,
                properties: [{
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string'
                }, {
                    propertyID: 'title',
                    propertyName: '文本',
                    propertyType: 'string'
                },
                {
                    propertyID: 'icon',
                    propertyName: '图标',
                    propertyType: 'string'
                }]
            }];
    }
}
