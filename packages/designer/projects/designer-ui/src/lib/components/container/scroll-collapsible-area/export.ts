import FdScrollCollapsibleAreaComponent from './component/fd-scroll-collapsible-area';
import { ScrollCollapsibleAreaSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdScrollCollapsibleAreaTemplates from './templates';


export const ScrollCollapsibleArea: ComponentExportEntity = {
    type: 'ScrollCollapsibleArea',
    component: FdScrollCollapsibleAreaComponent,
    template: FdScrollCollapsibleAreaTemplates,
    metadata: ScrollCollapsibleAreaSchema
};
