import FdSplitterComponent from './component/fd-splitter';
import { SplitterPaneSchema, SplitterSchema } from './schema/schema';
import FdSplitterTemplates from './templates';
import { ComponentExportEntity } from '@farris/designer-element';
import FdSplitterPaneComponent from './component/fd-splitter-pane';


export const Splitter: ComponentExportEntity = {
    type: 'Splitter',
    component: FdSplitterComponent,
    template: FdSplitterTemplates,
    metadata: SplitterSchema
};

export const SplitterPane: ComponentExportEntity = {
    type: 'SplitterPane',
    component: FdSplitterPaneComponent,
    metadata: SplitterPaneSchema
};
