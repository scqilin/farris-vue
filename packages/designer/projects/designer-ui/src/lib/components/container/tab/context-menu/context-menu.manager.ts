import { contextMenu, tabPageContextMenu } from './context-menu-config';
import { cloneDeep } from 'lodash-es';
import { RowNode } from '@farris/ui-treetable';
import { ContextMenuManager } from '../../../../service/context-menu.manager';
import { ControlContextMenuItem } from '../../../../entity/control-context-menu';
import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { TabChangeToSectionService } from './services/change-to-section.service';
import { ControlService } from '../../../../service/control.service';
import { MessagerService } from '@farris/ui-messager';
import { TabMoveToTabService } from './services/move-to-tab.service';
import { TabChildContentContextMenuService } from './services/child-content.service';
import { DgControl } from '../../../../utils/dg-control';
import { ControlTreeNode } from '@farris/designer-devkit';



export class TabContextMenuManager extends ContextMenuManager {

    private changeToSectionService: TabChangeToSectionService;
    private moveToTabService: TabMoveToTabService;
    private childContentService: TabChildContentContextMenuService;
    private parentRowNode: RowNode;

    constructor(cmp: FarrisDesignBaseComponent, rowNode: RowNode, parentRowNode: RowNode) {
        super(cmp, rowNode);
        this.parentRowNode = parentRowNode;

        const controlService = this.injector.get(ControlService);
        const msgService = this.injector.get(MessagerService);

        this.changeToSectionService = new TabChangeToSectionService(this.notifyService, controlService, msgService, this.refreshFormService);
        this.moveToTabService = new TabMoveToTabService(this.notifyService, this.refreshFormService, this.domService);
        this.childContentService = new TabChildContentContextMenuService(this.notifyService, controlService, this.refreshFormService, this.domService, this.injector, parentRowNode);

    }
    /**
     * 过滤、修改控件树右键菜单
     */
    setContextMenuConfig() {
        let menuConfig = cloneDeep(contextMenu) as ControlContextMenuItem[];

        const parentTreeNode = this.parentRowNode && this.parentRowNode.node as ControlTreeNode;
        if (parentTreeNode && parentTreeNode.rawElement.type === DgControl.Tab.type) {
            menuConfig = cloneDeep(tabPageContextMenu) as ControlContextMenuItem[];
        }
        if (!menuConfig) {
            return [];
        }

        menuConfig = this.changeToSectionService.assembleChangeToSectionMenu(this.cmpInstance, menuConfig);

        menuConfig = this.moveToTabService.assembleMoveToTabMenu(this.cmpInstance, menuConfig);


        // 配置菜单点击事件
        this.addContextMenuHandle(menuConfig);

        return menuConfig;
    }
    /**
     * 点击控件树右键菜单
     */
    contextMenuClicked(e: { data: RowNode, menu: ControlContextMenuItem }) {

        const menu = e.menu;

        switch (menu.parentMenuId) {
            // 移动至其他标签页
            case 'moveToTab': {
                this.moveToTabService.moveToTab(menu.id);
                break;
            }
            // 添加子级
            case 'addChildContent': {
                this.childContentService.addChildContent(this.cmpInstance, menu, this.rowNode);
                break;
            }
            default: {
                // 切换为分组面板
                if (menu.id === 'changeToSection') {
                    this.changeToSectionService.changeToSection();
                } else {
                    this.notifyService.warning('暂不支持');
                }
            }
        }

    }

}

