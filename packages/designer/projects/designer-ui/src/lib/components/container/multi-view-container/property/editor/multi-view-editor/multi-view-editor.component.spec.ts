import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiViewEditorComponent } from './multi-view-editor.component';

describe('MultiViewEditorComponent', () => {
  let component: MultiViewEditorComponent;
  let fixture: ComponentFixture<MultiViewEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiViewEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiViewEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
