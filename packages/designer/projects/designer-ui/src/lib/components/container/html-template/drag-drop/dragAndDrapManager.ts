import { DgControl } from '../../../../utils/dg-control';
import { FarrisDesignBaseNestedComponent } from '@farris/designer-element';

export class HtmlTemplateDragDropManager {

    private cmpInstance: FarrisDesignBaseNestedComponent;


    canDelete = true;
    canMove = true;

    constructor(cmpInstance: FarrisDesignBaseNestedComponent) {
        this.cmpInstance = cmpInstance;
    }


    /**
     * 判断当前容器是否是固定的上下文的中间层级
     */
    checkIsInFixedContextRules() {
        const component = this.cmpInstance.component;

        // 控件本身样式
        const cmpClass = component.appearance && component.appearance.class || '';
        const cmpClassList = cmpClass.split(' ');

        // 父级节点
        const parent = this.cmpInstance.parent && this.cmpInstance.parent.component;
        const parentClass = parent && parent.appearance && parent.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        // 1、header标题模板
        if (parent.type === DgControl.ContentContainer.type && parentClassList.includes('f-title')) {
            this.canDelete = false;
            this.canMove = false;
        }

        // 标题区域的下模板，都禁止拖拽
        if (parent.type === DgControl.ContentContainer.type && parentClassList.includes('f-page-header-base')) {
            this.canMove = false;
        }
        // 标题区域的下模板，都禁止拖拽
        if (parent.type === DgControl.ContentContainer.type && parentClassList.includes('f-page-header')) {
            this.canMove = false;
        }
    }
}
