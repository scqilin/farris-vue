import { ModalContainerSchema } from '../schema/schema';
import FdContainerBaseComponent from '../../common/containerBase/containerBase';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ModalContainerProp } from '../property/property-config';

export default class FdModalContainerComponent extends FdContainerBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);
    }

    getDefaultSchema(): any {

        return ModalContainerSchema;
    }

    getTemplateName(): string {
        return 'ModalContainer';
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: ModalContainerProp = new ModalContainerProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }
    /**
     * 清空外部组件的声明节点
     */
    onRemoveComponent(): void {
        if (!this.component.externalCmp) {
            return;
        }

        const domService = this.options.designerHost.getService('DomService');

        domService.deleteExternalComponent(this.component.externalCmp);
    }

    hideNestedPaddingInDesginerView(): boolean {
        return true;
    }
}
