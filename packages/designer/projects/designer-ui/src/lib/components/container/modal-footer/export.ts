import FdModalFooterComponent from './component/fd-modal-footer';
import { ModalFooterSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdModalFooterTemplates from './templates';
import FdModalFooterToolBarComponent from '../header/component/fd-header-toolbar';
import FdModalFooterToolBarItemComponent from '../header/component/fd-header-toolbar-item';
import FdModalFooterToolBarTemplate from '../../command/toolbar/templates/form';
import FdModalFooterToolBarItemTemplate from '../../command/toolbar/component/toolbaritem/templates/form';

export const ModalFooter: ComponentExportEntity = {
    type: 'ModalFooter',
    component: FdModalFooterComponent,
    template: FdModalFooterTemplates,
    metadata: ModalFooterSchema
};
export const ModalFooterToolBar = {
    type: 'ModalFooterToolBar',
    component: FdModalFooterToolBarComponent,
    template: { form: FdModalFooterToolBarTemplate }
};

export const ModalFooterToolBarItem = {
    type: 'ModalFooterToolBarItem',
    component: FdModalFooterToolBarItemComponent,
    template: { form: FdModalFooterToolBarItemTemplate }
};
