import { ComponentExportEntity } from '@farris/designer-element';
import FdQueryFormComponent from './component/fd-query-form';
import { QueryFormSchema } from './schema/schema';

import FdQueryFormTemplates from './templates';


export const QueryForm: ComponentExportEntity = {
    type: 'QueryForm',
    component: FdQueryFormComponent,
    template: FdQueryFormTemplates,
    metadata: QueryFormSchema
};
