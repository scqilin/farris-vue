import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ControlAppearancePropertyConfig } from '../../../../utils/appearance-property-config';

export class ContainerProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        let propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        // const appearancePropConfig = this.getAppearancePropConfig(propertyData);
        // propertyConfig.push(appearancePropConfig);

        // 样式属性
        const stylePropConfigs = ControlAppearancePropertyConfig.getAppearanceStylePropConfigs(propertyData);
        propertyConfig = propertyConfig.concat(stylePropConfigs);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }


    private getAppearancePropConfig(propertyData: any): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties(propertyData);

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'isScrollspyContainer',
                    propertyName: '被监听滚动的容器',
                    propertyType: 'boolean',
                    description: '是否为被监听页面滚动的容器',
                    defaultValue: false
                },
                {
                    propertyID: 'scrollOffset',
                    propertyName: '节点偏移量（px）',
                    propertyType: 'number',
                    description: '滚动时每个节点的偏移量',
                    decimals: 0,
                    min: 0,
                    defaultValue: 0,
                    visible: propertyData.isScrollspyContainer
                },

                {
                    propertyID: 'isScrollCollapsibleArea',
                    propertyName: '滚动收折父容器',
                    propertyType: 'boolean',
                    description: '是否为滚动收折父容器',
                    defaultValue: false,
                    readonly: propertyData.isScrollArea
                },
                {
                    propertyID: 'isScrollArea',
                    propertyName: '滚动收折父容器下的滚动区域',
                    propertyType: 'boolean',
                    description: '是否为滚动收折父容器下的滚动区域',
                    defaultValue: false,
                    readonly: propertyData.isScrollCollapsibleArea
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                switch (changeObject.propertyID) {
                    case 'isScrollspyContainer': {
                        const scrollOffset = this.properties.find(p => p.propertyID === 'scrollOffset');
                        if (scrollOffset) {
                            scrollOffset.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    case 'isScrollCollapsibleArea': {
                        const isScrollArea = this.properties.find(p => p.propertyID === 'isScrollArea');
                        if (isScrollArea) {
                            isScrollArea.readonly = changeObject.propertyValue;

                            propertyData.isScrollArea = changeObject.propertyValue ? false : propertyData.isScrollArea;
                            changeObject.relateChangeProps = [{
                                propertyID: 'isScrollArea',
                                propertyValue: propertyData.isScrollArea
                            }];
                        }

                        break;
                    }
                    case 'isScrollArea': {
                        const isScrollCollapsibleArea = this.properties.find(p => p.propertyID === 'isScrollCollapsibleArea');
                        if (isScrollCollapsibleArea) {
                            isScrollCollapsibleArea.readonly = changeObject.propertyValue;

                            propertyData.isScrollCollapsibleArea = changeObject.propertyValue ? false : propertyData.isScrollCollapsibleArea;
                            changeObject.relateChangeProps = [{
                                propertyID: 'isScrollCollapsibleArea',
                                propertyValue: propertyData.isScrollCollapsibleArea
                            }];
                        }
                        break;
                    }
                }


            }
        };
    }
}
