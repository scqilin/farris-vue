import FdContainerComponent from './component/fd-container';
import { ContentContainerSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdContainerTemplates from './templates';


export const ContentContainer: ComponentExportEntity = {
    type: 'ContentContainer',
    component: FdContainerComponent,
    template: FdContainerTemplates,
    metadata: ContentContainerSchema
};
