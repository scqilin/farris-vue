import { ElementPropertyConfig } from '@farris/ide-property-panel'; 
import { CodeEditorComponent } from '@farris/designer-devkit';
import { ContainerUsualProp } from '../../common/property/container-property-config';

export class HTMLProp extends ContainerUsualProp {

    getPropConfig(propertyData: any ): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 模板属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData);
        propertyConfig.push(appearancePropConfig);

        return propertyConfig;

    }

    getAppearancePropConfig(propertyData: any): ElementPropertyConfig {
        return {
            categoryId: 'html',
            categoryName: '模板',
            properties: [
                {
                    propertyID: 'html',
                    propertyName: 'html',
                    propertyType: 'modal',
                    description: 'html编辑器',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    }
                }
            ]
        };
    }

}
