import { Injectable } from '@angular/core';
import { DomService } from '@farris/designer-services';
import { DgControl } from '../../../../../utils/dg-control';
import { ControlService } from '../../../../../service/control.service';

@Injectable()
export class CreateDetailContainerService {

    constructor(
        private domService: DomService,
        private controlService: ControlService) { }

    /**
     * 没有从表区域的场景下，创建从表区域。
     * containerType:区域内实体的层级。child-grid：从表区域，grandson-grid：从从表区域
     * 容器层级为：detail-container-----detail-setction-----detail-tab
     */
    createDetailContainer(templateSchema: any, containerCascadeType: string) {


        const containerSchema = templateSchema.children.find(item => item.id === containerCascadeType);
        if (!containerSchema || !containerSchema.path) {
            return;
        }

        const settings = this.getSettingsByParentContainerType(templateSchema.type, containerSchema.path);
        if (!settings) {
            return;
        }
        const { detailContainer, detailSection, detailTab } = settings;
        const rootCmp = this.domService.getComponentById('root-component');

        let detailContainerParentElement;
        const mainContainer = this.domService.getNodeByIdPath(rootCmp, templateSchema.path);
        const likeCardCotainer = mainContainer.contents.find(co => co.type === DgControl.ContentContainer.type && co.isLikeCardContainer);
        if (likeCardCotainer) {
            detailContainerParentElement = likeCardCotainer;
        } else {
            detailContainerParentElement = mainContainer;
        }

        const radomNum = Math.random().toString().slice(2, 6);
        const tabPageSchema = this.controlService.getControlMetaData(DgControl.TabPage.type);
        tabPageSchema.id = `tabpage_${radomNum}`;
        tabPageSchema.toolbar.id = `tab_toolbar_${radomNum}`;


        const detailTabElement = this.controlService.getControlMetaData(DgControl.Tab.type);
        Object.assign(detailTabElement, {
            id: detailTab.id,
            appearance: {
                class: detailTab.class
            },
            contents: [tabPageSchema],
            selected: tabPageSchema.id
        });


        const detailSectionElement = this.controlService.getControlMetaData(DgControl.Section.type);
        Object.assign(detailSectionElement, {
            id: detailSection.id,
            appearance: {
                class: detailSection.class
            },
            fill: false,
            showHeader: false,
            contents: [detailTabElement]
        });

        const detailContainerElement = this.controlService.getControlMetaData(DgControl.ContentContainer.type);
        Object.assign(detailContainerElement, {
            id: detailContainer.id,
            appearance: {
                class: detailContainer.class
            },
            contents: [detailSectionElement]
        });

        this.adaptChildFill(detailContainerElement, mainContainer);
        detailContainerParentElement.contents.push(detailContainerElement);


    }

    /**
     * 根据父区域不同，获取container、section、tab组件的不同配置
     */
    private getSettingsByParentContainerType(parentContainerType: string, containerSchemaPath: string) {
        const detailContainer = { id: '-container', class: '' };
        const detailSection = { id: '-section', class: '' };
        const detailTab = { id: '-tab', class: '' };

        // 设置容器id
        const containerPathArray = containerSchemaPath.split('.');
        if (containerPathArray.length < 2) {
            return;
        }
        detailSection.id = containerPathArray.pop();
        detailContainer.id = containerPathArray.pop();
        detailTab.id = detailSection.id.replace('-section', '-tab');


        // 设置容器样式：卡片模板和列表模板在样式上不同
        switch (parentContainerType) {
            case 'MainContainer': {
                detailContainer.class = 'f-struct-wrapper';
                detailSection.class = 'f-section-tabs f-section-in-mainsubcard';
                detailTab.class = 'f-component-tabs f-tabs-has-grid';
                break;
            }
            case 'RightMainContainer': {
                detailContainer.class = 'f-struct-wrapper';
                detailSection.class = 'f-section-tabs f-section-in-main';
                detailTab.class = 'f-component-tabs';

            }
        }

        return { detailContainer, detailSection, detailTab };
    }

    /**
     * 从表区域启用填充
     * @param detailContainer 从表container
     * @param mainContainer 从表所在父容器
     */
    private adaptChildFill(detailContainer: any, mainContainer: any) {
        const mainContainerCls = mainContainer.appearance && mainContainer.appearance.class || '';
        if (mainContainerCls.includes('f-page-child-fill')) {
            detailContainer.appearance.class += ' f-struct-wrapper-child';

            const section = detailContainer.contents[0];
            section.fill = true;

            const tab = section.contents[0];
            tab.contentFill = true;
        }
    }
}
