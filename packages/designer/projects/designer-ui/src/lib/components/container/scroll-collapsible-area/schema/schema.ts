export const ScrollCollapsibleAreaSchema = {
    id: 'scroll-collapsible-area',
    type: 'ScrollCollapsibleArea',
    appearance: null,
    visible: true,
    contentTemplate: null
};
