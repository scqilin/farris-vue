export default (ctx: any) => {
    let navCls = ctx.component.appearance ? ' ' + ctx.component.appearance.class : '';
    navCls += ctx.component.position === 'left' ? ' f-list-nav-left' : (ctx.component.position === 'right' ? ' f-list-nav-right' : '');

    const navMainCls = ctx.hiddenState ? ' d-none' : '';

    let navInStr = `transition:width 0.5s;`;
    navInStr += ctx.hiddenState ? `width:0;` : ``;

    // 标题
    const headerStr = getTitleTemplate(ctx);

    // 入口
    const entryStr = getEntryTemplate(ctx);

    return `<div class="f-list-nav ide-list-nav ${navCls}">
            <div class="f-list-nav-in" style="${navInStr}" ref="${ctx.listNavKey}">
                <div class="f-list-nav-main ${navMainCls}" style="min-height:200px;">
                    ${headerStr}
                    <div class="f-list-nav-content drag-container" id = "${ctx.component.id}" ref="${ctx.nestedKey}" dragref="${ctx.component.id}-container">
                        ${ctx.children}
                    </div>
                </div>
                ${entryStr}
            </div>
        </div>`;
};

/**
 * 获取入口模板
 * @param ctx 上下文
 */
function getEntryTemplate(ctx: any) {
    if (!ctx.component.showEntry) {
        return '';
    }

    let entryCls = ctx.component.disabled ? ' f-list-nav-toggle-disabled' : '';
    entryCls += ctx.hiddenState ? ' active' : '';
    return `
    <div class="f-list-nav-toggle-sidebar ${entryCls}">
        <span class="triangle"></span>
    </div>` ;
}


/**
 * 标题模板
 * @param ctx 上下文
 */
function getTitleTemplate(ctx: any) {
    if (!ctx.component.title) {
        return '';
    }
    return `
    <div class="f-list-nav-header">
        <div class="f-list-nav-title">
            ${ctx.component.title}
        </div>
    </div>` ;
}
