import FdSidebarComponent from './component/fd-sidebar';
import { SidebarSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdSidebarTemplates from './templates';


export const Sidebar: ComponentExportEntity = {
    type: 'Sidebar',
    component: FdSidebarComponent,
    template: FdSidebarTemplates,
    metadata: SidebarSchema
};
