export const contextMenu = [
  {
    title: '添加子级',
    id: 'addChildContent',
    children: [
      {
        title: '标签页项',
        id: 'TabPage'
      },
      {
        title: '表格',
        id: 'DataGrid'
      },
      {
        title: '附件上传预览',
        id: 'FileUploadPreview'
      }
    ]
  },
  {
    title: '切换为分组面板',
    id: 'changeToSection'
  },
  {
    title: '移动至标签页',
    id: 'moveToTab'
  }
];


export const tabPageContextMenu = [
  {
    title: '添加子级',
    id: 'addChildContent',
    children: [
      {
        title: '表格',
        id: 'DataGrid'
      },
      {
        title: '附件上传预览',
        id: 'FileUploadPreview'
      }
    ]
  }
];
