import { DesignerEnvType } from "@farris/designer-services";

export const contextMenu = [
    {
        id: 'addChildContent',
        title: '添加控件',
        children: [
            {
                id: 'TextBox',
                title: '文本'
            },
            {

                id: 'MultiTextBox',
                title: '多行文本'
            },
            {

                id: 'DateBox',
                title: '日期'
            },
            {
                id: 'TimePicker',
                title: '时间选择'
            },
            {

                id: 'EnumField',
                title: '下拉列表'
            },
            {

                id: 'NumericBox',
                title: '数值'
            },
            {
                id: 'CheckBox',
                title: '复选框'
            },
            {

                id: 'CheckGroup',
                title: '复选框组'
            },
            {

                id: 'RadioGroup',
                title: '单选组'
            },
            {
                id: 'SwitchField',
                title: '开关'
            },
            {

                id: 'Image',
                title: '图像'
            },
            {

                id: 'LookupEdit',
                title: '帮助'
            },
            {
                id: 'RichTextBox',
                title: '富文本'
            },
            {

                id: 'InputGroup',
                title: '智能输入框'
            },
            {

                id: 'LanguageTextBox',
                title: '多语言输入框'
            },
            {
                id: 'Tags',
                title: '标记组'
            },
            {

                id: 'Avatar',
                title: '头像'
            },
            {

                id: 'PersonnelSelector',
                title: '人员选择',
                supportedEnvType: DesignerEnvType.designer
            },
            {

                id: 'OrganizationSelector',
                title: '组织选择',
                supportedEnvType: DesignerEnvType.designer
            },
            {
                id: 'AdminOrganizationSelector',
                title: '组织选择',
                supportedEnvType: DesignerEnvType.noCode
            },
            {
                id: 'EmployeeSelector',
                title: '人员选择',
                supportedEnvType: DesignerEnvType.noCode
            },
            {
                id: 'ExtIntergration',
                title: '外部服务集成',
                supportedEnvType: DesignerEnvType.noCode
            },
            {
                id: 'OaRelation',
                title: '关联行政审批',
                supportedEnvType: DesignerEnvType.noCode
            }
        ]
    },
    {
        id: 'addFieldSet',
        title: '添加分组',
    },
    {
        id: 'enableTabIndex',
        title: '启用Tab索引'
    },
    {
        id: 'closeTabIndex',
        title: '关闭Tab索引'
    }
];
