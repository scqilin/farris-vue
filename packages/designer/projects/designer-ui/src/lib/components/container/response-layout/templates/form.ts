export default (ctx: any) => {

    let layoutItemBodys = '';
    ctx.component.contents.forEach((layoutItem, index) => {

        const itemClass = getLayoutItemClass(ctx.selectedLayoutItemId, layoutItem);

        layoutItemBodys += `
        <div
            class="response-layout-item ${itemClass}" ref="${ctx.layoutItemKey}" id="${layoutItem.id}">
                <div class="drag-container" ref="nested-${layoutItem.id}"  dragref="${layoutItem.id}-container" >
                    ${ctx.layoutItemElements[index]}
                </div>
        </div>
        `;
    });

    return `
        <div class="d-flex response-layout"  style="height:inherit;" id = "${ctx.component.id}">
            ${layoutItemBodys}
        </div>
      `;
};

/**
 * 获取布局项的样式
 */
function getLayoutItemClass(selectedLayoutItemId: string, layoutItem) {
    let itemClass = layoutItem.appearance && layoutItem.appearance.class;
    if (layoutItem.id === selectedLayoutItemId) {
        itemClass += ' dgComponentSelected';
    }

    return itemClass || '';
}
