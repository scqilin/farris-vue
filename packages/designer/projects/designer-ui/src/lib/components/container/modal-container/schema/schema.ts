export const ModalContainerSchema = {
    id: 'modal-container',
    type: 'ModalContainer',
    appearance: null,
    visible: true,
    contentType: 'form',
    externalCmp: null,
    url: null,
    useIsolateJs: false
};

