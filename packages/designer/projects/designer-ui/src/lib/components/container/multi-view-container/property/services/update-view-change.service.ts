import { DgControl, DomService } from '@farris/designer-services';

export class MultiViewUpdateViewChangeService {

    /**
     * 视图变更后，更新viewChange图标组件
     * @param latestViews 变更后的视图组
     * （TODO：页头有header容器和header组件两种可能）
     */
    updateViewChangeButtons(latestViews: any[], viewChangeBar: any, parentContainer: any, domService: DomService, multiViewContainerId?: string) {

        // let viewChangeElement;
        // // 子表启用多视图，图标组件放在了TabPage/Section上
        // if (parentContainer && parentContainer.multiViews && parentContainer.views) {
        //     viewChangeElement = parentContainer.views;
        // }
        // // 主表启用多视图，在多视图组件上记录了图标组件的路径
        // if (viewChangeBar) {
        //     const rootComponentJson = domService.getComponentById('root-component');
        //     viewChangeElement = domService.getNodeByIdPath(rootComponentJson, viewChangeBar);
        // }

        const viewChangeElement = this.getViewChangeElement(viewChangeBar, parentContainer, domService, multiViewContainerId);

        if (viewChangeElement) {
            const viewChangeItems = viewChangeElement.toolbarData as any[];
            const updatedViewChangeItems = [];
            latestViews.forEach((view) => {
                const matchedViewItem = viewChangeItems.find(item => item.id === view.id);
                if (matchedViewItem) {
                    matchedViewItem.title = view.title;
                    matchedViewItem.iconName = view.icon;
                    updatedViewChangeItems.push(matchedViewItem);
                } else {
                    updatedViewChangeItems.push({
                        type: view.id,
                        title: view.title,
                        iconName: view.icon
                    });
                }
            });
            viewChangeElement.toolbarData = updatedViewChangeItems;

            // 变更默认视图
            if (updatedViewChangeItems.findIndex(viewItem => viewItem.type === viewChangeElement.defaultType) < 0) {
                if (updatedViewChangeItems.length) {
                    viewChangeElement.defaultType = updatedViewChangeItems[0].type;
                } else {
                    viewChangeElement.defaultType = '';
                }
            }
            // 变更设计时展示视图
            if (viewChangeElement.currentTypeInDesignerView && updatedViewChangeItems.findIndex(viewItem => viewItem.type === viewChangeElement.currentTypeInDesignerView) < 0) {
                if (updatedViewChangeItems.length) {
                    viewChangeElement.currentTypeInDesignerView = updatedViewChangeItems[0].type;
                } else {
                    viewChangeElement.currentTypeInDesignerView = '';
                }
            }
        }
    }


    private getViewChangeElement(viewChangeBar: any, parentContainer: any, domService: DomService, multiViewContainerId: string) {
        let viewChangeElement;

        // 主表启用多视图，在多视图组件上记录了图标组件的路径
        if (viewChangeBar) {
            const rootComponentJson = domService.getComponentById('root-component');
            viewChangeElement = domService.getNodeByIdPath(rootComponentJson, viewChangeBar);

            return viewChangeElement;
        }

        // 子表启用多视图，图标组件放在了Section上
        if (parentContainer && parentContainer.component && parentContainer.component.multiViews && parentContainer.component.views) {
            viewChangeElement = parentContainer.component.views;
            return viewChangeElement;
        }

        // 子表启用多视图，图标组件放在了TabPage上
        if (parentContainer.type === DgControl.Tab.type && parentContainer.component && parentContainer.component.contents) {
            const tabPage = parentContainer.component.contents.find(item => {
                if (item.contents && item.contents.find(co => co.id === multiViewContainerId)) {
                    return true;
                }
            });
            if (tabPage && tabPage.multiViews && tabPage.views) {
                viewChangeElement = tabPage.views;
                return viewChangeElement;
            }
        }
    }
}
