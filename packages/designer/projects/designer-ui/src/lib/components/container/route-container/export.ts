import FdRouteContainerComponent from './component/fd-route-container';
import { RouteContainerSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdRouteContainerTemplates from './templates';


export const RouteContainer: ComponentExportEntity = {
    type: 'RouteContainer',
    component: FdRouteContainerComponent,
    template: FdRouteContainerTemplates,
    metadata: RouteContainerSchema
};
