export default (ctx: any) => {  
  return  `<div class="row f-multi-select f-utils-fill ide-cmp-multi-select">
    <div class="f-utils-fill f-multi-select-list d-flex flex-column">
        <div class="f-utils-flex-column f-multi-select-list-content f-utils-fill">
            <legend class="f-multi-select-list-title"> 未选择 ( 0 / 0 ) </legend>
            <div class="input-group" style="margin: .75rem .875rem 0; width: auto">
                <input class="form-control" type="text" disabled placeholder="输入筛选项名称搜索" />
                <div class="input-group-append" style="background: transparent;">
                    <span class="fa f-icon-search" style="padding: 2px"></span>
                </div>
            </div>
            <div class="container columns-box f-utils-fill">
                <ul class="f-multi-select-list-group multi-select-list-group-flush f-multi-select-list-group--left f-multi-select-norecords">
                    <li class="f-multi-select-norecords-content"> 暂无数据 </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="f-multi-select-list-btns">
        <button class="btn btn-secondary f-btn-icon" type="button" title="右移">
            <span class="f-icon f-icon-arrow-chevron-right"></span></button>
        <button class="btn btn-secondary f-btn-icon right-all" type="button" title="全部右移">
            <span class="f-icon f-icon-arrow-seek-right"></span></button>
        <button class="btn btn-secondary f-btn-icon" type="button" title="左移">
            <span class="f-icon f-icon-arrow-chevron-left"></span></button>
        <button  class="btn btn-secondary f-btn-icon" type="button" title="全部左移">
            <span class="f-icon f-icon-arrow-seek-left"></span></button>
    </div>
    <div class="f-utils-fill f-multi-select-list d-flex flex-column">
        <div class="f-utils-flex-column f-multi-select-list-content f-utils-fill">
            <legend class="f-multi-select-list-title"> 已选择 (0) </legend>
            <div class="container columns-box f-utils-fill">
                <ul class="f-multi-select-list-group multi-select-list-group-flush f-multi-select-list-group--right">

                </ul>
            </div>
        </div>
    </div>

    <div class="f-multi-select-list-btns">
        <button class="btn btn-secondary f-btn-icon" type="button" title="置顶">
            <span class="f-icon f-icon-roofing"></span></button>
        <button class="btn btn-secondary f-btn-icon right-all" type="button" title="上移">
            <span class="f-icon f-icon-arrow-double-60-up"></span></button>
        <button class="btn btn-secondary f-btn-icon" type="button" title="下移"><span
                class="f-icon f-icon-arrow-double-60-down"></span></button>
        <button class="btn btn-secondary f-btn-icon"
            type="button" title="置底"><span class="f-icon f-icon-bottomsetting"></span></button>
    </div>
</div>`
}
