import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { SchemaService } from '@farris/designer-services';
import { BindingEditorComponent, BindingEditorConverter, SelectSchemaFieldEditorComponent } from '@farris/designer-devkit';
import { ContainerUsualProp } from '../../../container/common/property/container-property-config';
import { IDesignerHost } from '@farris/designer-element';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class MultiSelectProp extends ContainerUsualProp {

    private schemaService: SchemaService;
    constructor(serviceHost: IDesignerHost, viewModelId: string, componentId: string) {
        super(serviceHost, viewModelId, componentId);

        this.schemaService = serviceHost.getService('SchemaService');
    }

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 数据分配属性
        const customPropConfig = this.getDataPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(customPropConfig);


        return propertyConfig;
    }

    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId)
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [visibleProp]
        };
    }


    private getDataPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const fjmFields = this.schemaService.getTreeGridUdtFields(viewModelId);

        const properties: PropertyEntity[] = [
            {
                propertyID: 'dataSource',
                propertyName: '数据源',
                propertyType: 'string'
            },
            {
                propertyID: 'idField',
                propertyName: 'id字段',
                propertyType: 'modal',
                editor: SelectSchemaFieldEditorComponent,
                editorParams: {
                    viewModelId,
                    isMultiSelect: false
                }
            },
            {
                propertyID: 'valueField',
                propertyName: '值字段',
                propertyType: 'modal',
                editor: SelectSchemaFieldEditorComponent,
                editorParams: {
                    viewModelId,
                    isMultiSelect: false
                },
                showClearButton: true
            },
            {
                propertyID: 'textField',
                propertyName: '文本字段',
                propertyType: 'modal',
                editor: SelectSchemaFieldEditorComponent,
                editorParams: {
                    viewModelId,
                    isMultiSelect: false
                },
                showClearButton: true
            },
            {
                propertyID: 'binding',
                propertyName: '已选择数据绑定',
                propertyType: 'modal',
                editor: BindingEditorComponent,
                editorParams: { viewModelId, componentId: propertyData.componentId, controlType: propertyData.type },
                converter: new BindingEditorConverter()
            },
            {
                propertyID: 'displayType',
                propertyName: '显示类型',
                propertyType: 'select',
                visible: true,
                iterator: [{ key: 'List', value: '列表' }, { key: 'Tree', value: '树型' }]
            },
            {
                propertyID: 'showCascadeControl',
                propertyName: '启用级联控制',
                propertyType: 'boolean',
                visible: propertyData.displayType === 'Tree',
                defaultValue: false
            },
            {
                propertyID: 'fjmType',
                propertyName: '分级类型',
                propertyType: 'select',
                visible: propertyData.displayType === 'Tree',
                iterator: [{ key: 'fjmField', value: '分级码字段' }, { key: 'fjdField', value: '父节点分级码字段' }]
            },
            {
                propertyID: 'fjmField',
                propertyName: '分级码字段',
                propertyType: 'select',
                iterator: fjmFields,
                visible: propertyData.displayType === 'Tree' && propertyData.fjmType === 'fjmField'
            },
            {
                propertyID: 'fjdField',
                propertyName: '父节点分级码字段',
                propertyType: 'select',
                iterator: fjmFields,
                visible: propertyData.displayType === 'Tree' && propertyData.fjmType === 'fjdField'
            },
            {
                propertyID: 'enableTargetSort',
                propertyName: '启用调整顺序',
                propertyType: 'boolean'
            }
        ];

        return {
            categoryId: 'multiSelect',
            categoryName: '数据分配',
            properties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'displayType': {

                        this.properties.map(p => {
                            if (p.propertyID === 'fjmType' || p.propertyID === 'showCascadeControl') {
                                p.visible = changeObject.propertyValue === 'Tree';
                            } else if (p.propertyID === 'fjmField') {
                                p.visible = changeObject.propertyValue === 'Tree' && propertyData.fjmType === 'fjmField';
                            } else if (p.propertyID === 'fjdField') {
                                p.visible = changeObject.propertyValue === 'Tree' && propertyData.fjmType === 'fjdField';
                            }
                        });

                        if (changeObject.propertyValue !== 'Tree') {
                            propertyData.showCascadeControl = false;
                        }
                        break;
                    }
                    case 'fjmType': {
                        const fjmField = this.properties.find(p => p.propertyID === 'fjmField');
                        const fjdField = this.properties.find(p => p.propertyID === 'fjdField');
                        if (fjmField) {
                            fjmField.visible = propertyData.fjmType === 'fjmField';
                        }
                        if (fjdField) {
                            fjdField.visible = propertyData.fjmType === 'fjdField';
                        }

                    }
                }
            }
        };
    }

}
