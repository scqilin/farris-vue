import FdMultiSelectComponent from './component/fd-multi-select';
import { MultiSelectSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdMultiSelectTemplates from './templates';


export const MultiSelect: ComponentExportEntity = {
    type: 'MultiSelect',
    component: FdMultiSelectComponent,
    template: FdMultiSelectTemplates,
    metadata: MultiSelectSchema
};
