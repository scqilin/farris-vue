import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../../container/common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class DisplayFieldProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;

    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();

        commonProps.push(
            {
                propertyID: 'title',
                propertyName: '标签',
                propertyType: 'string',
                description: '组件的标签'
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'title': {
                        changeObject.needUpdateControlTreeNodeName = true;
                        break;
                    }
                }
            }
        };
    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId)
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [visibleProp]
        };
    }

}
