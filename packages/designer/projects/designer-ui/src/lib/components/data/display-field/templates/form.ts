export default (ctx: any) => {
  return `<label>${ctx.component.title || ''}</label>`
};
