export const MultiSelectSchema = {
    id: 'multi-select',
    type: 'MultiSelect',
    visible: true,
    appearance: null,
    binding: null,
    dataSource: 'viewModel.bindingData',
    idField: '',
    valueField: '',
    textField: '',
    displayType: 'List',
    showCascadeControl: false,
    enableTargetSort: true
};
