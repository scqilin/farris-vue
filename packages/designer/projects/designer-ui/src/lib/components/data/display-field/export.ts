import FdDisplayFieldComponent from './component/fd-display-field';
import { DisplayFieldSchema } from './schema/schema';
// import { ComponentExportEntity } from '@farris/designer-element';
import FdDisplayFieldTemplates from './templates';


export const DisplayField = {
    type: 'DisplayField',
    component: FdDisplayFieldComponent,
    template: FdDisplayFieldTemplates
};
