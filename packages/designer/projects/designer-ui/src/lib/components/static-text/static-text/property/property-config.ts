import { BindingEditorComponent, BindingEditorConverter, CodeEditorComponent } from '@farris/designer-devkit';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { StaticUsualProp } from '../../common/property/static-property-config';

export class FilePreviewProp extends StaticUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {


        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 附件属性
        const customPropConfig = this.getFilePreviewPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(customPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string) {
        const visibleProp = this.getVisiblePropEntity(propertyData, viewModelId);
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'readonly',
                    propertyName: '是否只读',
                    propertyType: 'unity',
                    description: '运行时组件是否只读',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'custom'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                        }
                    }
                }
            ]
        };

    }
    private getFilePreviewPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'file',
            categoryName: '附件属性',
            properties: [
                {
                    propertyID: 'previewAreaTemplate',
                    propertyName: '预览区域模板',
                    propertyType: 'modal',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    }
                },
                {
                    propertyID: 'rootId',
                    propertyName: '附件服务器根目录',
                    propertyType: 'string'
                },
                {
                    propertyID: 'formId',
                    propertyName: '二级目录',
                    propertyType: 'unity',
                    description: '默认为主表ID',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        editorOptions: {
                            types: ['custom', 'variable'],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'String'
                        }
                    },
                },
                {
                    propertyID: 'fieldIdKey',
                    propertyName: '附件ID字段',
                    propertyType: 'modal',
                    editor: BindingEditorComponent,
                    editorParams: { viewModelId, componentId: propertyData.componentId, controlType: propertyData.type },
                    converter: new BindingEditorConverter()
                },
                {
                    propertyID: 'fileNameKey',
                    propertyName: '附件名称字段',
                    propertyType: 'modal',
                    editor: BindingEditorComponent,
                    editorParams: { viewModelId, componentId: propertyData.componentId, controlType: propertyData.type },
                    converter: new BindingEditorConverter()
                },
                {
                    propertyID: 'enableBatchDownload',
                    propertyName: '是否启用批量下载',
                    propertyType: 'boolean'
                },
                {
                    propertyID: 'filePreviewToolbarTemplate',
                    propertyName: '批量下载区域模板',
                    propertyType: 'modal',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    },
                    visible: propertyData.enableBatchDownload
                },
                {
                    propertyID: 'showType',
                    propertyName: '附件展示类型',
                    propertyType: 'select',
                    iterator: [{ key: 'card', value: '卡片' }, { key: 'list', value: '列表' }]
                },
                {
                    propertyID: 'itemCls',
                    propertyName: '附件预览项样式',
                    propertyType: 'string'
                },
                {
                    propertyID: 'canRename',
                    propertyName: '是否启用重命名',
                    propertyType: 'boolean',
                    defaultValue: false
                }
            ],
            setPropertyRelates(changeObject, data, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'enableBatchDownload': {
                        const filePreviewToolbarTemplate = this.properties.find(p => p.propertyID === 'filePreviewToolbarTemplate');
                        if (filePreviewToolbarTemplate) {
                            filePreviewToolbarTemplate.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                }
            }
        };
    }

    private getEventPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'removeFile',
                name: '预览附件删除事件'
            },
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
            }
        };
    }
}
