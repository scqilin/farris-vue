import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { DesignerEnvType, FormBasicService } from '@farris/designer-services';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export default class FdStaticBaseComponent extends FarrisDesignBaseComponent {
    envType: DesignerEnvType;

    triggerBelongedComponentToMoveWhenMoved = true;

    constructor(component: any, options: any) {
        super(component, options);

        // 组件所属分类为“附件”
        this.category = 'file';

        const serviceHost = this.options.designerHost;
        const formBasicService = serviceHost.getService('FormBasicService') as FormBasicService;
        if (formBasicService) {
            this.envType = formBasicService.envType;
        }
    }


    /**
     * 属性变更后事件：默认监听样式类属性变更，并触发模板重绘
     * @param changeObject 变更集
     * @param propertyIDs 需要额外监听的属性ID列表
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
        let dynamicPropertyIDs = ['appearance.class', 'appearance.style'];
        if (propertyIDs) {
            dynamicPropertyIDs = dynamicPropertyIDs.concat(propertyIDs);
        }

        const propertyPath = changeObject.propertyPath ? changeObject.propertyPath + '.' : '';
        if (dynamicPropertyIDs.includes(propertyPath + changeObject.propertyID)) {
            this.triggerRedraw();
        }

    }


    /**
     * 不允许单独删除附件控件，若要删除需要定位到所属Component去删除
     */
    checkCanDeleteComponent(): boolean {
        return false;
    }

    /**
     * 是否支持移动取决于所属Component是否支持移动
     */
    checkCanMoveComponent(): boolean {
        const belongedCmpInstance = this.getBelongedComponentInstance(this);
        return belongedCmpInstance.checkCanMoveComponent();
    }
}
