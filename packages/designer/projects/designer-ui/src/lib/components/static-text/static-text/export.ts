import { ComponentExportEntity } from '@farris/designer-element';
import FdStaticTextComponent from './component/static-text';
import { StaticTextSchema } from './schema/schema';
import FdStaticTextTemplates from './templates';


export const StaticText: ComponentExportEntity = {
    type: 'StaticText',
    component: FdStaticTextComponent,
    template: FdStaticTextTemplates,
    metadata: StaticTextSchema
};
