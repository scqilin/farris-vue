import { StyleEditorComponent } from '@farris/designer-devkit';
import { IDesignerHost } from '@farris/designer-element';
import { EventEditorService, DomService, FormBasicService, SchemaService, UniformEditorDataUtil, WebCmdService } from '@farris/designer-services';
import { ElementPropertyConfig, PropertyEntity } from "@farris/ide-property-panel";
import { DgControl } from '../../../../utils/dg-control';

export class NoCodeStaticUsualProp {

    public domService: DomService;
    public formBasicService: FormBasicService;
    public schemaService: SchemaService;
    //事件编辑器集成
    public webCmdService: WebCmdService;
    public viewModelId: string;
    public componentId: string;
    public eventEditorService: EventEditorService;

    constructor(private serviceHost: IDesignerHost, viewModelId: string, componentId: string) {
        this.viewModelId = viewModelId;
        this.componentId = componentId;
        this.eventEditorService = serviceHost.getService('EventEditorService');
        this.domService = serviceHost.getService('DomService');
        this.webCmdService = serviceHost.getService('WebCmdService');
        this.formBasicService = serviceHost.getService('FormBasicService');
    }

    getBasicPropConfig(propertyData: any): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                // {
                //     propertyID: 'id',
                //     propertyName: '标识',
                //     propertyType: 'string',
                //     readonly: true
                // },
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件的类型',
                    iterator: [{ key: propertyData.type, value: DgControl[propertyData.type].name }],
                    readonly: true
                }
            ]
        };
    }

    getAppearancePropConfig(): ElementPropertyConfig {

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: [
                // {
                //     propertyID: 'appearance',
                //     propertyName: '样式',
                //     propertyType: 'cascade',
                //     cascadeConfig: [
                //         {
                //             propertyID: 'class',
                //             propertyName: 'class样式',
                //             propertyType: 'string'
                //         },
                //         {
                //             propertyID: 'style',
                //             propertyName: 'style样式',
                //             propertyType: 'modal',
                //             editor: StyleEditorComponent,
                //             showClearButton: true
                //         }
                //     ]
                // }
            ]
        };
    }

    getVisiblePropEntity(propertyData: any, viewModelId: string): PropertyEntity {

        return {
            propertyID: 'visible',
            propertyName: '是否可见',
            propertyType: 'unity',
            description: '运行时组件是否可见',
            editorParams: {
                controlName: UniformEditorDataUtil.getControlName(propertyData),
                constType: 'enum',
                editorOptions: {
                    types: ['const', 'variable'],
                    enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                    variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                    getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                    newVariableType: 'Boolean',
                    newVariablePrefix: 'is'
                }
            }
        };

    }
}
