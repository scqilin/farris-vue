import { DesignerEnvType } from '@farris/designer-services';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import { NoCodeFilePreviewProp } from '../../static-text/property/nocode-property-config';
import FdStaticBaseComponent from '../../common/component/base.component';
import { FilePreviewProp } from '../property/property-config';
import { StaticTextSchema } from '../schema/schema';
import { cloneDeep } from 'lodash-es';
import { SectionSchema } from '../../../container/section/schema/schema';

export default class FdStaticTextComponent extends FdStaticBaseComponent {
    
    constructor(component: any, options: any) {
        super(component, options);

        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('file-upload-preview.css');
    }
    static getMetadataInControlBox() {
        const staticTextCommentsSchema = cloneDeep(StaticTextSchema);
        Object.assign(staticTextCommentsSchema, {
            id: staticTextCommentsSchema.id + '-' + Math.random().toString(36).slice(2, 6),
        });

        // const sectionSchema = cloneDeep(SectionSchema);
        // Object.assign(sectionSchema, {
        //     id: 'static-text-' + Math.random().toString(36).slice(2, 6),
        //     mainTitle: '静态文本',
        //     contents: [staticTextCommentsSchema],
        //     visible: true,
        // });

        return staticTextCommentsSchema;
    }

    getStyles(): string {
        return 'display: block;';
    }
    getDefaultSchema(): any {

        return StaticTextSchema;
    }

    // 渲染模板
    render(): any {

        return super.render(this.renderTemplate('FilePreview', {
            component: this.component
        }));
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;

        if (this.envType === DesignerEnvType.noCode){
            const prop: NoCodeFilePreviewProp = new NoCodeFilePreviewProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        } else {
            const prop: FilePreviewProp = new FilePreviewProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        }
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
        super.onPropertyChanged(changeObject, ['enableBatchDownload', 'showType']);
    }
}
