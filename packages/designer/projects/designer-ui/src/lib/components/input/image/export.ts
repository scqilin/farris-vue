import FdImageComponent from './component/fd-image';
import { ImageSchema } from './schema/schema';

import FdImageTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const Image: ComponentExportEntity = {
    type: 'Image',
    component: FdImageComponent,
    template: FdImageTemplates,
    metadata: cloneDeep(ImageSchema)
};
