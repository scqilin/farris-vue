import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { DomService, FormBindingType } from '@farris/designer-services';
import { DgControl } from '../../../../../utils/dg-control';
import { ExpressionEditorComponent } from '@farris/designer-devkit';

export class ExpressionProp {

    constructor(public domService: DomService) { }

    getExpressionPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        if (!propertyData || !propertyData.binding || propertyData.binding.type !== FormBindingType.Form || !propertyData.binding.field) {
            return;
        }
        const fieldId = propertyData.binding.field;

        // 将表达式数组转换为属性面板支持的对象式结构
        const expressionData: any = {};
        if (this.domService.expressions) {
            const expField = this.domService.expressions.find(e => e.fieldId === fieldId);
            if (expField && expField.expression && expField.expression.length) {
                expField.expression.forEach(expr => {
                    expressionData[expr.type] = expr.value;
                    if (expr.type === 'validate') {
                        expressionData.validateMessage = expr.message;
                    }
                    if (expr.type === 'dataPicking') {
                        expressionData.dataPickingMessage = expr.message;
                        expressionData.dataPickingMessageType = expr.messageType;
                    }
                });
            }
        }

        // 为了发版，暂时将申报模板子表中的只读、必填属性的表达式选项屏蔽掉
        let showExpInTableTemplate = true;
        const vmBindTo = this.domService.getViewModelById(viewModelId).bindTo;
        if (this.domService.module.templateId === 'nav-table-fillin-form-template' && vmBindTo !== '/') {
            showExpInTableTemplate = false;
        }
        const properties = this.getExpressionProperties(fieldId, viewModelId, expressionData, propertyData, showExpInTableTemplate);

        const config: ElementPropertyConfig = {
            categoryId: 'expressionData',
            categoryName: '表达式',
            properties,
            propertyData: expressionData,
            enableCascade: true,
            parentPropertyID: (showPosition === 'gridFieldEditor' || showPosition === 'tableTdEditor') ? 'editor' : ''
        };

        if (showPosition === 'gridFieldEditor' || showPosition === 'tableTdEditor') {
            config.tabId = showPosition;
            config.tabName = '编辑器';
        }

        return config;
    }

    protected getExpressionProperties(fieldId, viewModelId: string, expressionData: any, propertyData: any, showExpInTableTemplate: boolean) {
        return [
            {
                propertyID: 'compute',
                propertyName: '计算表达式',
                propertyType: 'modal',
                editor: ExpressionEditorComponent,
                editorParams: {fieldId, viewModelId, expType: 'compute', modalTitle: '计算表达式编辑器'},
                showClearButton: true,
                afterClickClearButton: () => {
                    this.clearExpressionData(fieldId, 'compute');
                }
            },
            {
                propertyID: 'dependency',
                propertyName: '依赖表达式',
                propertyType: 'modal',
                editor: ExpressionEditorComponent,
                editorParams: {fieldId, viewModelId, expType: 'dependency', modalTitle: '依赖表达式编辑器'},
                showClearButton: true,
                afterClickClearButton: () => {
                    this.clearExpressionData(fieldId, 'dependency');
                }
            },
            {
                propertyID: 'validate',
                propertyName: '校验表达式',
                propertyType: 'modal',
                editor: ExpressionEditorComponent,
                editorParams: {
                    fieldId,
                    viewModelId,
                    expType: 'validate',
                    message: expressionData.validateMessage,
                    modalTitle: '校验表达式编辑器'
                },
                showClearButton: true,
                afterClickClearButton: () => {
                    this.clearExpressionData(fieldId, 'validate');
                }
            },
            {
                propertyID: 'dataPicking',
                propertyName: '帮助前表达式',
                propertyType: 'modal',
                editor: ExpressionEditorComponent,
                editorParams: {
                    fieldId,
                    viewModelId,
                    expType: 'dataPicking',
                    message: expressionData.dataPickingMessage,
                    messageType: expressionData.dataPickingMessageType,
                    modalTitle: '帮助前表达式编辑器'
                },
                showClearButton: true,
                afterClickClearButton: () => {
                    this.clearExpressionData(fieldId, 'dataPicking');
                },
                visible: propertyData.type === DgControl.LookupEdit.type && showExpInTableTemplate
            }
        ];
    }

    protected clearExpressionData(fieldId: string, expType: string) {
        if (this.domService.expressions) {
            const exp = this.domService.expressions.find(e => e.fieldId === fieldId);
            if (!exp) {
                return;
            }

            exp.expression = exp.expression.filter(e => e.type !== expType);
        }

    }
}
