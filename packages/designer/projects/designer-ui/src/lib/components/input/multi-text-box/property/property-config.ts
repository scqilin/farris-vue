import { ElementPropertyConfig, PropertyChangeObject, PropertyEntity } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { InputProps } from '../../common/property/input-property-config';

export class MultiTextBoxProp extends InputProps {


    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 字数属性
        const wordCountPropConfig = this.getWordCountPropConfig(propertyData);
        this.propertyConfig.push(wordCountPropConfig);

        // 扩展区域属性
        // const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, this.viewModelId);
        // this.propertyConfig.push(appendPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, this.viewModelId);
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }
        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }


    /**
     * 外观属性
     */
    private getAppearancePropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
        const self = this;
        let appearanceProperties: PropertyEntity[] = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {

            const appearanceCommonProps = this.getAppearanceCommonPropConfig(propertyData, viewModelId, showPosition);
            appearanceProperties = appearanceProperties.concat(appearanceCommonProps);
        }

        const config = {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);
            }

        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    /**
     * 行为属性
     */
    private getBehaviorPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
        let behaviorProperties: PropertyEntity[] = [];

        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
        }
        let maxLength = 0;
        if (propertyData.binding && propertyData.binding.type === 'Form') {
            const fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, viewModelId);
            if (fieldInfo && fieldInfo.schemaField) {
                maxLength = fieldInfo.schemaField.type.length;
            }
        }
        behaviorProperties.push(
            {
                propertyID: 'editType',
                propertyName: '允许弹出编辑',
                propertyType: 'select',
                description: '是否允许弹出编辑',
                iterator: [{ key: 'default', value: '不允许' }, { key: 'dialog', value: '允许' }],
                defaultValue: 'default',
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'dialogWidth',
                propertyName: '窗口宽度',
                propertyType: 'number',
                description: '弹出编辑窗口宽度',
                decimals: 0,
                min: 1,
                visible: propertyData.editType === 'dialog'
            },
            {
                propertyID: 'dialogHeight',
                propertyName: '窗口高度',
                propertyType: 'number',
                description: '弹出编辑窗口高度',
                decimals: 0,
                min: 1,
                visible: propertyData.editType === 'dialog'
            },
            {
                propertyID: 'maxLength',
                propertyName: '最大长度',
                propertyType: 'number',
                numberFormat: 'n0',
                decimals: 0,
                min: 0,
                max: maxLength > 0 ? maxLength : undefined,
                description: '文本框允许输入的最大字数'
            },
            {
                propertyID: 'useComments',
                propertyName: '启用常用记录',
                propertyType: 'select',
                description: '是否启用常用输入项',
                iterator: [{ key: true, value: '启用' }, { key: false, value: '不启用' }],
                visible: showPosition === 'card'
            }

        );

        const self = this;
        const config = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);

                switch (changeObject.propertyID) {
                    case 'editType': {
                        const dialogWidth = this.properties.find(p => p.propertyID === 'dialogWidth');
                        if (dialogWidth) {
                            dialogWidth.visible = changeObject.propertyValue === 'dialog';
                        }
                        const dialogHeight = this.properties.find(p => p.propertyID === 'dialogHeight');
                        if (dialogHeight) {
                            dialogHeight.visible = changeObject.propertyValue === 'dialog';
                        }

                        // 变更【字数分类】下的【字数显示位置】属性
                        const wordCountConfig = self.propertyConfig.find(cat => cat.categoryId.includes('wordCount'));
                        if (wordCountConfig && wordCountConfig.properties) {
                            const onlyShowInDialog = wordCountConfig.properties.find(p => p.propertyID === 'onlyShowInDialog');
                            if (onlyShowInDialog) {
                                onlyShowInDialog.visible = changeObject.propertyValue === 'dialog' && propertyData.enableWordCount;
                            }
                        }
                        if (changeObject.propertyValue !== 'dialog') {
                            propertyData.onlyShowInDialog = false;
                            changeObject.relateChangeProps = [
                                {
                                    propertyID: 'onlyShowInDialog',
                                    propertyValue: propertyData.onlyShowInDialog
                                }
                            ];

                        }

                        break;
                    }
                }
            }
        };


        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }

        return config;
    }

    /**
     * 字数属性
     */
    private getWordCountPropConfig(propertyData: any, showPosition = 'card'): ElementPropertyConfig {

        const countProperties = [
            {
                propertyID: 'enableWordCount',
                propertyName: '启用字数统计',
                propertyType: 'select',
                iterator: [{ key: true, value: '启用' }, { key: false, value: '不启用' }],
                description: '是否启用字数统计'
            },
            {
                propertyID: 'countType',
                propertyName: '统计方式',
                propertyType: 'select',
                description: '字数统计方式',
                iterator: [{ key: 'surplus', value: '显示剩余字数' }, { key: 'length', value: '显示已输入字数' }],
                visible: propertyData.enableWordCount
            },
            {
                propertyID: 'onlyShowInDialog',
                propertyName: '字数显示位置',
                propertyType: 'select',
                description: '字数显示位置',
                iterator: [{ key: true, value: '仅在弹窗中显示' }, { key: false, value: '在组件本身与弹窗中同时显示' }],
                visible: propertyData.enableWordCount && propertyData.editType === 'dialog'
            }
        ];

        const config = {
            categoryId: 'wordCount',
            categoryName: '字数统计',
            properties: countProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'enableWordCount': {
                        const countType = this.properties.find(p => p.propertyID === 'countType');
                        if (countType) {
                            countType.visible = changeObject.propertyValue;
                        }
                        const onlyShowInDialog = this.properties.find(p => p.propertyID === 'onlyShowInDialog');
                        if (onlyShowInDialog) {
                            onlyShowInDialog.visible = changeObject.propertyValue && propertyData.editType === 'dialog';
                        }
                        break;
                    }
                }
            }
        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }

        return config;
    }

    /**
     * 列编辑器属性
     * @param gridFieldData 列数据
     */

    getGridFieldEdtiorPropConfig(gridFieldData: any, viewModelId: string): ElementPropertyConfig[] {
        const self = this;

        const propertyData = gridFieldData.editor;
        this.propertyConfig = [];

        // 编辑器类型属性
        const editorTypeConfig = this.getGridFieldEditorTypePropertyConfig(gridFieldData, viewModelId);
        if (editorTypeConfig) {
            this.propertyConfig.push(editorTypeConfig);
        }

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.appendBehaviorPropsForGridFieldEditor(behaviorConfig, propertyData, viewModelId);

        this.propertyConfig.push(behaviorConfig);

        // 字数统计
        const wordCountConfig = this.getWordCountPropConfig(propertyData, 'gridFieldEditor');
        if (wordCountConfig) {
            this.propertyConfig.push(wordCountConfig);
        }

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId, 'gridFieldEditor');
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }
    /**
     * table单元格编辑器属性
     * @param tdData 单元格数据
     * @param viewModelId viewModelId
     * @returns 属性配置
     */
    getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
        const propertyData = tdData.editor;

        this.propertyConfig = [];

        // 外观属性
        const appearanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appearanceConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
        behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
        this.propertyConfig.push(behaviorConfig);

        // 字数属性
        const wordCountPropConfig = this.getWordCountPropConfig(propertyData, 'tableTdEditor');
        this.propertyConfig.push(wordCountPropConfig);

        // 扩展区域属性
        const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appendPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, viewModelId, 'tableTdEditor');
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId, 'tableTdEditor');
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }

}
