import FdFieldComponent from './component/field';

import FdFieldTemplates from './templates';


export const Field = {
    component: FdFieldComponent,
    template: FdFieldTemplates
};
