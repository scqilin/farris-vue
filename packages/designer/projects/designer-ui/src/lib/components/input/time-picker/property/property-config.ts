import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { InputProps } from '../../common/property/input-property-config';

export class TimePickerProp extends InputProps {

    propertyConfig: ElementPropertyConfig[];


    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorProperties = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorProperties);

        // 扩展区域属性
        // const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, this.viewModelId);
        // this.propertyConfig.push(appendPropConfig);

        // 事件属性
        const eventPropConfig = this.getTimeEventPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(eventPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, this.viewModelId);
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }

        return this.propertyConfig;
    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
        let appearanceProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            const appearanceCommonProps = this.getAppearanceCommonPropConfig(propertyData, viewModelId, showPosition);
            appearanceProperties = appearanceProperties.concat(appearanceCommonProps);
        }

        appearanceProperties.push(
            {
                propertyID: 'format',
                propertyName: '格式',
                propertyType: 'string',
                description: '格式设置'
            },
            {
                propertyID: 'showHeader',
                propertyName: '显示时分秒标题',
                propertyType: 'select',
                description: '时间选择器是否显示时分秒的标题',
                iterator: [{ key: true, value: '是' }, { key: false, value: '否' }]
            }
        );
        const self = this;
        const config = {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);

            }

        };
        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;

    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        let behaviorProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
        }

        behaviorProperties.push(
            {
                propertyID: 'editable',
                propertyName: '允许输入',
                propertyType: 'select',
                description: '是否允许输入',
                iterator: [{ key: true, value: '是' }, { key: false, value: '否' }]
            },
            {
                propertyID: 'use12Hours',
                propertyName: '使用12小时输入模式',
                propertyType: 'select',
                description: '是否使用12小时输入模式',
                iterator: [{ key: true, value: '是' }, { key: false, value: '否' }]
            },
            {
                propertyID: 'hourStep',
                propertyName: '时步长',
                propertyType: 'number',
                description: '时步长设置',
                decimals: 0,
                min: 1
            },
            {
                propertyID: 'minuteStep',
                propertyName: '分步长',
                propertyType: 'number',
                description: '分步长设置',
                decimals: 0,
                min: 1
            },
            {
                propertyID: 'secondStep',
                propertyName: '秒步长',
                propertyType: 'number',
                description: '秒步长设置',
                decimals: 0,
                min: 1
            }
        );

        const self = this;
        const config = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);
            }
        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    private getTimeEventPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
        const eventList = [];
        if (showPosition !== 'gridFieldEditor') {
            eventList.push({
                label: 'openChange',
                name: '面板打开状态切换'
            });
        }

        return this.getEventPropertyConfig(propertyData, viewModelId, showPosition, { customEventList: eventList });
    }



    /**
     * 列编辑器属性
     * @param gridFieldData 列数据
     */
    getGridFieldEdtiorPropConfig(gridFieldData: any, viewModelId: string) {

        const propertyData = gridFieldData.editor;

        this.propertyConfig = [];

        // 编辑器类型属性
        const editorTypeConfig = this.getGridFieldEditorTypePropertyConfig(gridFieldData, viewModelId);
        if (editorTypeConfig) {
            this.propertyConfig.push(editorTypeConfig);
        }

        // 外观属性
        const apperanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.propertyConfig.push(apperanceConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.appendBehaviorPropsForGridFieldEditor(behaviorConfig, propertyData, viewModelId);
        this.propertyConfig.push(behaviorConfig);

        // 事件属性
        const eventConfig = this.getTimeEventPropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.propertyConfig.push(eventConfig);

        return this.propertyConfig;
    }

    /**
     * table单元格编辑器属性
     * @param tdData 单元格数据
     * @param viewModelId viewModelId
     * @returns 属性配置
     */
    getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
        const propertyData = tdData.editor;

        this.propertyConfig = [];

        // 外观属性
        const appearanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appearanceConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
        behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
        this.propertyConfig.push(behaviorConfig);

        // 扩展区域属性
        const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appendPropConfig);

        // 事件属性
        const eventConfig = this.getTimeEventPropConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(eventConfig);


        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, viewModelId, 'tableTdEditor');
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }
        return this.propertyConfig;
    }
}
