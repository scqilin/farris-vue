import FdDateBoxComponent from './component/fd-date-box';
import { DateBoxSchema } from './schema/schema';

import FdDateBoxTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const DateBox: ComponentExportEntity = {
    type: 'DateBox',
    component: FdDateBoxComponent,
    template: FdDateBoxTemplates,
    metadata: cloneDeep(DateBoxSchema)
};
