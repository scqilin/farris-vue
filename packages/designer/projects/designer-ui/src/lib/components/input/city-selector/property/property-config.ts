import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../../lib/entity/property-change-entity';
import { InputProps } from '../../common/property/input-property-config';

export class CitySelectorProp extends InputProps {


    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        let propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, this.viewModelId);
        if (exprPropConfig) {
            propertyConfig.push(exprPropConfig);
        }

        return propertyConfig;
    }


    getOpenModalPropConfigs() {

    }

    /**
     * 外观属性
     */
    private getAppearancePropConfig(
        propertyData: any, viewModelId: string, showPosition = 'card'):
        ElementPropertyConfig {
        const self = this;
        let appearanceProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            appearanceProperties = this.getAppearanceCommonPropConfig(propertyData, viewModelId, showPosition);
        }
        const config = {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                if (!changeObject) {
                    return;
                }
                self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);
            }

        };
        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    /**
     * 行为属性
     */
    private getBehaviorPropConfig(
        propertyData: any, viewModelId: string, showPosition = 'card'):
        ElementPropertyConfig {

        const self = this;
        let behaviorProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
        }
        behaviorProperties.push(
            {
                propertyID: 'editable',
                propertyName: '允许输入',
                propertyType: 'boolean',
                description: '是否允许输入',
                visible: false // 控件不支持
            },
            {
                propertyID: 'enableClear',
                propertyName: '启用清除按钮',
                propertyType: 'boolean',
                isible: false // 控件不支持
            }
        );

        const config = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);

                switch (changeObject.propertyID) {

                }
            }
        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;

    }

    /**
     * 事件属性
     */
    private getEventPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            },
            {
                label: 'onBlur',
                name: '失去焦点事件'
            },
            {
                label: 'onClear',
                name: '清除事件'
            }
        ];

        return this.getEventPropertyConfig(propertyData, viewModelId, showPosition, { customEventList: eventList });

    }

    /**
     * 动态事件
     * @param propertyData 属性值
     * @param eventList  已有事件
     */
    switchEvents(propertyData, eventList) {
        eventList = super.switchEvents(propertyData, eventList);

        if (propertyData.enableClear) {
            const onClear = eventList.find(eventListItem => eventListItem.label === 'onClear');
            if (!onClear) {
                eventList.push({
                    label: 'onClear',
                    name: '清除事件'
                });
            }
        } else {
            eventList = eventList.filter(eventListItem => eventListItem.label !== 'onClear');
        }

        return eventList;
    }

    /**
     * 列编辑器属性
     */
    getGridFieldEdtiorPropConfig(gridFieldData: any, viewModelId: string) {
        const propertyData = gridFieldData.editor;
        this.propertyConfig = [];

        // 编辑器类型属性
        const editorTypeConfig = this.getGridFieldEditorTypePropertyConfig(gridFieldData, viewModelId);
        if (editorTypeConfig) {
            this.propertyConfig.push(editorTypeConfig);
        }

        // 外观属性
        const apperanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.propertyConfig.push(apperanceConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.appendBehaviorPropsForGridFieldEditor(behaviorConfig, propertyData, viewModelId);
        this.propertyConfig.push(behaviorConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, viewModelId, 'gridFieldEditor');

        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;

    }

    /**
     * table单元格编辑器属性
     * @param tdData 单元格数据
     * @param viewModelId viewModelId
     * @returns 属性配置
     */
    getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
        const propertyData = tdData.editor;

        this.propertyConfig = [];

        // 外观属性
        const apperanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(apperanceConfig);

        // 扩展区域属性
        const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appendPropConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
        behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
        this.propertyConfig.push(behaviorConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, viewModelId, 'tableTdEditor');
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }

}
