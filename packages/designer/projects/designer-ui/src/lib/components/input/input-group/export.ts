import FdInputGroupComponent from './component/fd-input-group';
import { InputGroupSchema } from './schema/schema';

import FdInputGroupTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const InputGroup: ComponentExportEntity = {
    type: 'InputGroup',
    component: FdInputGroupComponent,
    template: FdInputGroupTemplates,
    metadata: cloneDeep(InputGroupSchema)
};
