

export default (ctx: any) => {
    return `
    <div class="farris-input-wrap">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="k-checkbox" onclick="return false;">
            <label class="k-checkbox-label"></label>
        </div>
    </div>`;
};
