import { InputAppendUtils } from '../../../../utils/input-append.utils';

export default (ctx: any) => {
    // 启用扩展区域
    let inputAppendEle = '';
    if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
        inputAppendEle = InputAppendUtils.getInputGroupAppendElement(ctx);
    }
    const beginPlaceHolder = ctx.component && ctx.component.beginPlaceHolder || '请输入开始数字';
    const endPlaceHolder = ctx.component && ctx.component.endPlaceHolder || '请输入结束数字';
    return `
<div class="farris-input-wrap">
    <div class="f-cmp-number-spinner ide-number-range">
        <div class="input-group number-range f-cmp-number-spinner f-state-disabled">
            <div class="form-control input-container">
                <div class="sub-input-group">
                    <input class="sub-input f-state-focus f-state-hover" type="text" placeholder="${beginPlaceHolder}"
                        style="text-align: left;" disabled />
                </div>
                <span class="spliter">~</span>
                <div class="sub-input-group"><input class="sub-input f-state-focus f-state-hover" type="text"
                        placeholder="${endPlaceHolder}" style="text-align: left;" disabled>
                </div>
            </div>
            ${inputAppendEle}
        </div>
    </div>
</div>`;
};
