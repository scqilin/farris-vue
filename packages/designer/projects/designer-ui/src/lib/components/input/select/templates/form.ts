import { InputAppendUtils } from '../../../../utils/input-append.utils';


export default (ctx: any) => {
    // 启用扩展区域
    let inputAppendEle = '';
    if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
        inputAppendEle = InputAppendUtils.getInputGroupAppendElement(ctx);
    }

    return `<div class="farris-input-wrap">
    <div class="f-cmp-inputgroup ide-input-group">
        <div class="input-group f-state-disabled">
            <input class="form-control text-left" readonly name="input-group-value" type="text" placeholder="${ctx.component.placeHolder || '请选择'}" >
            <div class="input-group-append">
                <span class="input-group-text"><i class="f-icon f-icon-arrow-60-down"></i></span>
            </div>
            ${inputAppendEle}
        </div>
    </div>
</div>`;
};
