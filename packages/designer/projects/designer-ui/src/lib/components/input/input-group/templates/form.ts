import { InputAppendUtils } from '../../../../utils/input-append.utils';

export default (ctx: any) => {
    const groupTextSpan = ctx.component.groupText ? ctx.component.groupText : `<i class="f-icon f-icon-lookup"></i>`;

    // 启用扩展区域
    let inputAppendEle = '';
    if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
        inputAppendEle = InputAppendUtils.getInputGroupAppendElement(ctx);
    }

    return `
    <div class="farris-input-wrap">
        <div class="ide-input-group f-cmp-inputgroup">
            <div class="lookupbox input-group f-state-disabled">
                <input class="form-control text-left" name="input-group-value" type="text" readonly placeholder="${ctx.component.placeHolder || ''}">
                <div class="input-group-append">
                    <span class="input-group-text">${groupTextSpan}</span>
                </div>
                ${inputAppendEle}
            </div>
        </div>
    </div>`;
};
