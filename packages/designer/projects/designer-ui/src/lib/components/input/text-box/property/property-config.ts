import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { GSPElementDataType } from '@gsp-bef/gsp-cm-metadata';
import { FormPropertyChangeObject } from '../../../../../lib/entity/property-change-entity';
import { InputProps } from '../../common/property/input-property-config';
import { getInputExByInputType, InputControlTypeMapping } from './input-format-map';

export class TextBoxProp extends InputProps {

    propertyConfig: ElementPropertyConfig[];



    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearanceProperties);


        // 格式校验属性
        const formatPropConfig = this.getformatValidation(propertyData, this.viewModelId);
        propertyConfig.push(formatPropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 扩展区域属性
        // const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, this.viewModelId);
        // propertyConfig.push(appendPropConfig);



        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, this.viewModelId);
        if (exprPropConfig) {
            propertyConfig.push(exprPropConfig);
        }
        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }

    private getAppearancePropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        const appearanceProperties = this.getAppearanceCommonPropConfig(propertyData, viewModelId, showPosition);
        const self = this;
        const config = {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);

            }

        };
        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
        // 获取绑定字段schema中的长度属性
        let maxLength;
        if (propertyData.binding && propertyData.binding.type === 'Form') {
            const fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, viewModelId);
            if (fieldInfo && fieldInfo.schemaField) {
                maxLength = fieldInfo.schemaField.type.length;
            }
        }
        let behaviorProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
        }

        behaviorProperties.push(
            {
                propertyID: 'maxLength',
                propertyName: '最大长度',
                propertyType: 'number',
                description: '最大长度',
                decimals: 0,
                min: 0,
                max: maxLength
            },
            {
                propertyID: 'isPassword',
                propertyName: '密码输入框',
                propertyType: 'boolean',
                description: '是否为密码输入框',
                defaultValue: false
            }
        );

        const self = this;
        const config = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);
            }
        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    getGridFieldEdtiorPropConfig(gridFieldData: any, viewModelId: string) {
        const propertyData = gridFieldData.editor;
        this.propertyConfig = [];

        // 编辑器类型属性
        const editorTypeConfig = this.getGridFieldEditorTypePropertyConfig(gridFieldData, viewModelId);
        if (editorTypeConfig) {
            this.propertyConfig.push(editorTypeConfig);
        }

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.appendBehaviorPropsForGridFieldEditor(behaviorConfig, propertyData, viewModelId);
        this.propertyConfig.push(behaviorConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }

    /**
     * table单元格编辑器属性
     * @param tdData 单元格数据
     * @param viewModelId viewModelId
     * @returns 属性配置
     */
    getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
        const propertyData = tdData.editor;

        this.propertyConfig = [];

        // 外观属性
        const appearanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appearanceConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
        behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
        this.propertyConfig.push(behaviorConfig);

        // 扩展区域属性
        const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appendPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, viewModelId, 'tableTdEditor');
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId, 'tableTdEditor');
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }



    public getformatValidation(propertyData: any, viewModelId: string) {

        const self = this;

        return {
            categoryId: 'formatValidation',
            categoryName: '输入格式校验',
            enableCascade: true,
            propertyData: propertyData.formatValidation,
            parentPropertyID: 'formatValidation',
            properties: [
                {
                    propertyID: 'type',
                    propertyName: '输入类型',
                    propertyType: 'select',
                    iterator: InputControlTypeMapping.String,
                    defaultValue: 'none'
                },
                {
                    propertyID: 'expression',
                    propertyName: '匹配正则',
                    propertyType: 'string',
                    defaultValue: '',
                    visible: propertyData.formatValidation && propertyData.formatValidation.type === 'custom'
                },
                {
                    propertyID: 'message',
                    propertyName: '输入错误提示',
                    propertyType: 'string',
                    defaultValue: '',
                    visible: propertyData.formatValidation && propertyData.formatValidation.type !== 'none'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'type': {
                        propData.expression = getInputExByInputType(changeObject.propertyValue);
                        if (propData.expression) {
                            const msg = self.getMessage(changeObject.propertyValue);
                            propData.message = msg ? `请输入正确的${msg}` : '';
                        }
                        if (changeObject.propertyValue === 'none' || changeObject.propertyValue === 'custom') {
                            propData.message = '';
                        }


                        const expProp = this.properties.find(prop => prop.propertyID === 'expression');
                        expProp.visible = propertyData.formatValidation && propertyData.formatValidation.type === 'custom';

                        const messageProp = this.properties.find(prop => prop.propertyID === 'message');
                        messageProp.visible = propertyData.formatValidation && propertyData.formatValidation.type !== 'none';

                        break;
                    }
                }
            }
        };

    }

    private getMessage(propertyValue) {
        let msg = '';
        InputControlTypeMapping[GSPElementDataType.String].forEach((element) => {
            if (element.key === propertyValue) {
                msg = element.value;
            }
        });
        return msg;
    }
}
