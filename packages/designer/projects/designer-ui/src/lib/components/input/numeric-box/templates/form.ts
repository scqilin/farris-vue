import { InputAppendUtils } from '../../../../utils/input-append.utils';

export default (ctx: any) => {
    let inputAppendEle = '';

    // 启用扩展区域
    if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
        inputAppendEle = InputAppendUtils.getInputGroupAppendElement(ctx);
    }

    return `
    <div class="farris-input-wrap">
        <div class="f-cmp-number-spinner ide-numeric-box f-cmp-inputgroup ide-input-group">
            <div class="input-group  f-state-disabled">
                <input class="form-control" type="input" placeholder="${ctx.component.placeHolder || '请输入数字'}" readonly style="padding-right: 4px; text-align: left;">

                <div class="input-group-append btn-group btn-group-number m-0">
                    <button class="btn btn-secondary btn-number-flag">
                        <span class="icon k-i-arrow-chevron-up number-arrow-chevron"></span>
                    </button>
                    <button class="btn btn-secondary btn-number-flag">
                        <span class="icon k-i-arrow-chevron-down number-arrow-chevron"></span>
                    </button>
                </div>
                ${inputAppendEle}
            </div>
        </div>
    </div>
      `;
};
