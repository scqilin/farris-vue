import { InputAppendUtils } from '../../../../utils/input-append.utils';


export default (ctx: any) => {
  const styleHeight = ctx.component.size && ctx.component.size.height ? 'height:' + ctx.component.size.height + 'px' : '';

  let inputEle = `<textarea readonly class="form-control"  style="${styleHeight}"  placeholder="${ctx.component.placeHolder || ''}"></textarea>`;

  // 启用扩展区域
  if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
    inputEle = InputAppendUtils.getMultiTextBoxAppendElement(ctx, inputEle);
  }
  return `
  <div class="farris-input-wrap">
    ${inputEle}
  </div>`;
};
