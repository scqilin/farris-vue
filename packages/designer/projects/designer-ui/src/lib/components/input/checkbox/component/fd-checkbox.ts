import { CheckBoxSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { CheckBoxProp } from '../property/property-config';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeCheckBoxProp } from '../property/nocode-property-config';
export default class FdCheckBoxComponent extends FarrisDesignField {

    component;
    constructor(component: any, options: any) {
        super(component, options);
    }

    init(): void {
        super.init();
        this.adaptOldControl();
    }

    getDefaultSchema(): any {
        return CheckBoxSchema;
    }


    getStyles(): string {
        return 'display: inline-block;';
    }


    render(): any {
        return super.render(this.renderTemplate('CheckBox', {
            component: this.component
        }), !this.component.showInTable);
    }

    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        if (this.envType === DesignerEnvType.noCode) {
            const prop: NoCodeCheckBoxProp = new NoCodeCheckBoxProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        } else {
            const prop: CheckBoxProp = new CheckBoxProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        }
    }

    private adaptOldControl() {
        // 移除冗余的属性
        if (this.component.hasOwnProperty('checked')) {
            delete this.component.checked;
        }
    }
}
