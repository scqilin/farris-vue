import FdCheckBoxComponent from './component/fd-checkbox';
import { CheckBoxSchema } from './schema/schema';

import FdCheckBoxrTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const CheckBox: ComponentExportEntity = {
    type: 'CheckBox',
    component: FdCheckBoxComponent,
    template: FdCheckBoxrTemplates,
    metadata: cloneDeep(CheckBoxSchema)
};
