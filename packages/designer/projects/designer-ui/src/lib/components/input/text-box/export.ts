import FdTextComponent from './component/fd-text';
import { TextBoxSchema } from './schema/schema';

import FdTextTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const TextBox: ComponentExportEntity = {
    type: 'TextBox',
    component: FdTextComponent,
    template: FdTextTemplates,
    metadata: cloneDeep(TextBoxSchema)
};
