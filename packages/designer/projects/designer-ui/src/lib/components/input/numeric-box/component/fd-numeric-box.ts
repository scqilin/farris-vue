import { NumericBoxSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { NumericBoxProp } from '../property/property-config';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeNumericBoxProp } from '../property/nocode-property-config';


export default class FdNumericBoxComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }


  getDefaultSchema(): any {
    return NumericBoxSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('NumericBox', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;


    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeNumericBoxProp = new NoCodeNumericBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: NumericBoxProp = new NumericBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }

  }

}
