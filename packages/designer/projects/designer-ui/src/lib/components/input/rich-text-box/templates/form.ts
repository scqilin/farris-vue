import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';


export default (ctx: any) => {
  let srcStr = `${ControlCssLoaderUtils.getAssetsUrl()}/images/input/rich-text-box/`;
  if (ctx.component.controlSource === 'concise') {
    srcStr += 'normal.jpg';
  }
  if (ctx.component.controlSource === 'advanced') {
    srcStr += 'advanced.jpg';
  }
  return `
  <div class="farris-input-wrap ide-rich-text-box" >
    <img src="${srcStr}" class="f-form-control-textarea" />
  </div>`;
};
