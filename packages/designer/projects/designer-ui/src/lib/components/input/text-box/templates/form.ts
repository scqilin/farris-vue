import { InputAppendUtils } from '../../../../utils/input-append.utils';

export default (ctx: any) => {
  let inputEle = `<input type="input" class="form-control" readonly  placeholder="${ctx.component.placeHolder || ''}">`;

  // 启用扩展区域
  if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
    inputEle = InputAppendUtils.getTextBoxAppendElement(ctx, inputEle);
  }

  return ` <div class="farris-input-wrap"> ${inputEle} </div>`;

};

