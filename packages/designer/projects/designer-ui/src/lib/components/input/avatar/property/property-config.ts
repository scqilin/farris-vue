import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../../lib/entity/property-change-entity';
import { InputProps } from '../../common/property/input-property-config';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';

export class AvatarProp extends InputProps {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getAvatarEventPropConfig(propertyData, this.viewModelId);
        this.propertyConfig.push(eventPropConfig);


        return this.propertyConfig;
    }


    private getAppearancePropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        const self = this;
        let appearanceProperties = [];
        if (showPosition === 'card') {
            let appearanceCommonProps = this.getAppearanceCommonPropConfig(propertyData, viewModelId);
            const supportedProps = 'responseLayout,appearance,size';
            appearanceCommonProps = appearanceCommonProps.filter(p => supportedProps.includes(p.propertyID));

            appearanceProperties = appearanceProperties.concat(appearanceCommonProps);
        }

        const config = {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);
            },

        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        let behaviorProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
        }
        behaviorProperties.push(
            {
                propertyID: 'cover',
                propertyName: '头像图片默认路径',
                propertyType: 'string',
                description: '头像图片默认路径设置'
            },
            {
                propertyID: 'maxSize',
                propertyName: '允许图片大小（M）',
                propertyType: 'number',
                description: '图片最大size 单位M',
                min: 0,
            },
            {
                propertyID: 'imgType',
                propertyName: '允许图片格式',
                propertyType: 'multiSelect',
                description: '图片限制的类型',
                iterator: [
                    { key: 'png', value: 'png' },
                    { key: 'jpg', value: 'jpg' },
                    { key: 'jpeg', value: 'jpeg' },
                    { key: 'webp', value: 'webp' },
                    { key: 'svg', value: 'svg' },
                    { key: 'gif', value: 'gif' },
                    { key: 'bmp', value: 'bmp' }
                ],
                multiSelectDataType: 'array'
            },
            {
                propertyID: 'imgShape',
                propertyName: '头像形状',
                propertyType: 'select',
                description: '头像形状选择',
                iterator: [
                    { key: 'circle', value: '圆形' },
                    { key: 'square', value: '方形' }
                ]
            },
            {
                propertyID: 'imgTitle',
                propertyName: '头像标题',
                propertyType: 'string',
                description: '头像悬停时的标题'
            }
        );

        const self = this;
        const config = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);
            }

        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    private getAvatarEventPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
        const eventList = [
            {
                label: 'imgChange',
                name: '图片变化后事件'
            },
        ];
        return this.getEventPropertyConfig(propertyData, viewModelId, showPosition, { customEventList: eventList });

    }

    /**
     * table单元格编辑器属性
     * @param tdData 单元格数据
     * @param viewModelId viewModelId
     * @returns 属性配置
     */
    getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
        const propertyData = tdData.editor;

        this.propertyConfig = [];

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
        behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
        this.propertyConfig.push(behaviorConfig);

        // 事件属性
        const eventPropConfig = this.getAvatarEventPropConfig(propertyData, this.viewModelId, 'tableTdEditor');
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }

}
