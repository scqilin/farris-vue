import FdCheckGroupComponent from './component/fd-check-group';
import { CheckGroupSchema } from './schema/schema';

import FdCheckGroupTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const CheckGroup: ComponentExportEntity = {
    type: 'CheckGroup',
    component: FdCheckGroupComponent,
    template: FdCheckGroupTemplates,
    metadata: cloneDeep(CheckGroupSchema)
};
