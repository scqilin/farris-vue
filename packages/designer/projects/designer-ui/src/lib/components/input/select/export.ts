import FdEnumFieldComponent from './component/fd-select';
import { EnumFieldSchema } from './schema/schema';

import FdEnumFieldTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const EnumField: ComponentExportEntity = {
    type: 'EnumField',
    component: FdEnumFieldComponent,
    template: FdEnumFieldTemplates,
    metadata: cloneDeep(EnumFieldSchema)
};
