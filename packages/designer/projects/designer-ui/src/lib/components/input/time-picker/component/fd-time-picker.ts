import { TimePickerSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { TimePickerProp } from '../property/property-config';
import { NoCodeTimePickerProp } from '../property/nocode-property-config';
import { DesignerEnvType } from '@farris/designer-services';

export default class FdTimePickerComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return TimePickerSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('TimePicker', {
      component: this.component
    }),!this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;

    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeTimePickerProp = new NoCodeTimePickerProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: TimePickerProp = new TimePickerProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }
}
