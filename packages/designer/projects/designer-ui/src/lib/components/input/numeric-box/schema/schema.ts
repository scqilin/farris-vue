export const NumericBoxSchema = {
    id: 'numericBox',
    type: 'NumericBox',
    titleSourceType: 'static',
    title: '数值框',
    controlSource: 'Farris',
    appearance: null,
    size: null,
    binding: null,
    readonly: false,
    require: false,
    disable: false,
    placeHolder: '',
    textAlign: 'left',
    precisionSourceType: 'static',
    precision: null,
    validation: null,
    maxValue: null,
    minValue: null,
    step: 1,
    useThousands: true,
    formatter: null,
    parser: null,
    canNull: true,
    bigNumber: false,
    maxLength: null,
    holdPlace: false,
    linkedLabelEnabled: false,
    linkedLabelClick: null,
    visible: true,
    isTextArea: true,
    tabindex: -1,
    hasDefaultFocus: false,
    focusState: null,
    titleWidth: null,
    localization: false,
    showZero: true,
    showButton: true,
    enableAppend: false,
    inputAppendType: 'button',
    inputAppendDisabled: false,
    autoHeight: false,
    maxHeight: 500
};

