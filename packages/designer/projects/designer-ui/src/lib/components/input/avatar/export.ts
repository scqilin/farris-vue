import FdAvatarComponent from './component/fd-avatar';
import { AvatarSchema } from './schema/schema';

import FdAvatarTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const Avatar: ComponentExportEntity = {
    type: 'Avatar',
    component: FdAvatarComponent,
    template: FdAvatarTemplates,
    metadata: cloneDeep(AvatarSchema)
};
