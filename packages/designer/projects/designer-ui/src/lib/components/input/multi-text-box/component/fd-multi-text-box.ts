import { MultiTextBoxSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { MultiTextBoxProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeMultiTextBoxProp } from '../property/nocode-property-config';

export default class FdMultiTextBoxComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return MultiTextBoxSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('MultiTextBox', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;
    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeMultiTextBoxProp = new NoCodeMultiTextBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: MultiTextBoxProp = new MultiTextBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }

  onPropertyChanged(changeObject: FormPropertyChangeObject): void {

    super.onPropertyChanged(changeObject, ['height'])
  }

}
