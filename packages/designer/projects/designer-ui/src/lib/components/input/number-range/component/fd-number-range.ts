import { NumberRangeSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { NumberRangeProp } from '../property/property-config';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeNumberRangeProp } from '../property/nocode-property-config';

export default class FdNumberRangeComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return NumberRangeSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('NumberRange', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;

    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeNumberRangeProp = new NoCodeNumberRangeProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: NumberRangeProp = new NumberRangeProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }


}
