export const ImageSchema =  {
    id: 'image',
    type: 'Image',
    title: '图像',
    disable: false,
    appearance: null,
    border: null,
    src: null,
    alt: null,
    visible: true,
    click: null,
    rootId: 'default-root'
};

