import FdSwitchFieldComponent from './component/fd-switch';
import { SwitchFieldSchema } from './schema/schema';

import FdSwitchFieldTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const SwitchField: ComponentExportEntity = {
    type: 'SwitchField',
    component: FdSwitchFieldComponent,
    template: FdSwitchFieldTemplates,
    metadata: cloneDeep(SwitchFieldSchema)
};
