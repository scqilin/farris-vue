import { Injector } from '@angular/core';
import { ExpressionEditorComponent, ExpressionEditorParam, BindingEditorConverter } from '@farris/designer-devkit';
import { IDesignerHost } from '@farris/designer-element';
import { EventEditorService, DataStatesService, DesignViewModelField, DesignViewModelService, DomService, FormBasicService, FormBindingType, FormExpression, FormSchemaEntityField, FormSchemaEntityFieldType$Type, FormVariable, RefreshFormService, SchemaService, UniformEditorDataUtil, WebCmdService } from '@farris/designer-services';
import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
// import { CustomWidthEditorComponent, DefaultValueEditorComponent } from '@farris/nocode-designer-plugin';
import { ColumnWidthSettingComponent } from '@farris/response-layout-editor';
import { NotifyService } from '@farris/ui-notify';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DgControl } from '../../../../utils/dg-control';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { FormUnifiedColumnLayout } from '../../../container/form/property/editor/form-layout-setting/form-unified-column-layout';
import { ExpressionProp } from './expression/expression-property-config';
import { FormResponseLayoutContext, ResponseLayoutEditorService } from './response-layout-editor';

export class NoCodeInputProps extends ExpressionProp {
    /** 布局配置服务 */
    private responseLayoutService: ResponseLayoutEditorService;
    public webCmdService: WebCmdService;
    public domService: DomService;
    public schemaService: SchemaService;
    public dgVMService: DesignViewModelService;
    public formBasicService: FormBasicService;
    public controlCreatorService: any;
    public refreshFormService: RefreshFormService;
    public injector: Injector;
    public notifyService: NotifyService;
    public eventEditorService: EventEditorService;

    public viewModelId: string;
    public componentId: string;

    /** 控件绑定的模型字段 */
    private dgVMField: DesignViewModelField;


    dataStatesService: DataStatesService;

    /** Schema-控件映射类 */
    private schemaDOMMapping: any;

    // 在列表中使用控件，列数据
    gridFieldData: any;
    /** 控件绑定的变量 */
    bindingVarible: FormVariable;

    /** 表单字段schema信息，与be保持一致 */
    fieldSchemaConsistentWithBE: FormSchemaEntityField;

    constructor(private serviceHost: IDesignerHost, viewModelId: string, componentId: string) {
        super(serviceHost.getService('DomService'));
        this.eventEditorService = serviceHost.getService('EventEditorService');
        this.domService = serviceHost.getService('DomService');
        this.webCmdService = serviceHost.getService('WebCmdService');
        this.schemaService = serviceHost.getService('SchemaService');
        this.dgVMService = serviceHost.getService('DesignViewModelService');
        this.schemaDOMMapping = serviceHost.getService('SchemaDOMMapping');
        this.formBasicService = serviceHost.getService('FormBasicService');
        this.controlCreatorService = serviceHost.getService('ControlCreatorService');
        this.refreshFormService = serviceHost.getService('RefreshFormService');
        this.injector = serviceHost.getService('Injector');
        this.notifyService = this.injector.get(NotifyService);
        this.viewModelId = viewModelId;
        this.componentId = componentId;
    }

    /**
     * 获取基本信息属性配置
     * @param propertyData 
     * @param viewModelId 
     * @param showPosition 
     * @returns 
     */
    getBasicPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        const basicProperties = this.getBasicCommonPropConfig(propertyData);

        const self = this;
        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: basicProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'type': {
                        self.changeControlType(propertyData, changeObject.propertyValue);
                        break;
                    }
                    case 'bindingCode': {
                        changeObject.needSyncToSchemaField = true;
                        break;
                    }
                }

            },

        };
    }

    getBasicCommonPropConfig(propertyData: any): PropertyEntity[] {
        const canChangeControlType = this.checkCanChangeControlType(propertyData, this.viewModelId);
        let controlTypeList = [];
        if (!canChangeControlType) {
            controlTypeList = [{
                key: propertyData.type,
                value: (DgControl[propertyData.type] && DgControl[propertyData.type].name) || propertyData.type
            }];
        }

        if (this.dgVMField) {
            // 绑定字段
            controlTypeList = this.schemaDOMMapping.getEditorTypesByMDataType(this.dgVMField.type.name, this.dgVMField.multiLanguage, 'form', this.formBasicService.envType);
        } else if (this.bindingVarible) {
            // 绑定变量
            controlTypeList = this.schemaDOMMapping.getEditorTypesByMDataType(this.bindingVarible.type, false, 'form', this.formBasicService.envType);
        }
        const isSimpleField = this.dgVMField && this.dgVMField.label && !this.dgVMField.label.includes('_');
        const bindingFieldId = this.dgVMField ? this.dgVMField.id : null;
        const isNewField = this.schemaService.addedFieldsByControlBox.includes(bindingFieldId);
        return [
            {
                propertyID: 'type',
                propertyName: '控件类型',
                propertyType: 'select',
                description: '组件的类型',
                readonly: true,
                iterator: controlTypeList
            },
            {
                propertyID: 'binding',
                propertyName: '绑定字段',
                propertyType: 'string',
                visible: false,
                readonly: false,
                converter: {
                    convertTo() {
                        return propertyData.binding.path;
                    }
                }
            },
            {
                propertyID: 'bindingCode',
                propertyName: '绑定字段',
                propertyType: 'string',
                readonly: !(isSimpleField && isNewField), // 目前只支持拖拽新增的简单字段修改编号。
                // notAllowedChars:[/^[A-Z0-9!@#$%^&*()_+=[\]{};':",./<>?\\|~-].*$/],///^[A-Z0-9!@#$%^&*()_+=\[\]{};':"\\|,.<>/?]+[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>/?]+$/
                defaultValue: propertyData.binding.path,
                description: '绑定字段的编号。必须以小写英文字母开头, 并且只能由英文字母、数字组成！'
            }
        ];
    }

    /**
     * 获取输入类控件的通用外观属性
     */
    protected getAppearancePropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        const appearanceProperties = this.getAppearanceCommonPropConfig(propertyData, viewModelId, showPosition);
        const self = this;
        const config = {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                if (!changeObject) {
                    return;
                }
                self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);

            }

        };
        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }
    /**
     * 获取输入类控件的通用外观属性配置
     */
    getAppearanceCommonPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): PropertyEntity[] {

        // 文本、多行文本、数字、日期时间、富文本
        const placeHolderTypes = [
            DgControl.TextBox.type,
            DgControl.MultiTextBox.type,
            DgControl.NumericBox.type,
            DgControl.DateBox.type,
            DgControl.RichTextBox.type,
            // DgControlType.EnumField,
        ];

        if (showPosition === 'tableTdEditor') {
            return [
                {
                    propertyID: 'title',
                    propertyName: '标签',
                    propertyType: 'string',
                    description: '标签名称',
                    visible: !propertyData.titleSourceType || propertyData.titleSourceType === 'static'
                },
                {
                    propertyID: 'placeHolder',
                    propertyName: '占位符',
                    propertyType: 'string',
                    description: '占位符文本',
                    readonly: false,
                    visible: placeHolderTypes.includes(propertyData.type),
                }
            ];
        }

        this.responseLayoutService = new ResponseLayoutEditorService(this.domService, this.componentId);
        return [
            {
                propertyID: 'title',
                propertyName: '标签',
                propertyType: 'string',
                description: '标签名称',
                visible: !propertyData.titleSourceType || propertyData.titleSourceType === 'static'
            },
            {
                propertyID: 'placeHolder',
                propertyName: '提示文字',
                propertyType: 'string',
                visible: placeHolderTypes.includes(propertyData.type),
                notAllowedChars: ['\"', '\'']
            },
            {
                propertyID: 'labelAutoWidth', //  f-form-lable-auto
                propertyName: '标签长度自适应',
                propertyType: 'boolean',
                description: '展示完整标签',
                defaultValue: false,
                iterator: [
                    { key: true, value: '是' },
                    { key: false, value: '否' }
                ],
                visible: viewModelId === 'root-viewmodel'
            },
            {
                propertyID: 'labelAutoOverflow',
                propertyName: '标签换行',
                defaultValue: false,
                propertyType: 'select',
                iterator: [
                    { key: true, value: '是' },
                    { key: false, value: '否' }
                ],
                visible: !propertyData.labelAutoWidth
            },
            {
                propertyID: 'responseLayout',
                propertyName: '布局配置',
                propertyType: 'custom',
                description: '布局配置设置',
                editor: ColumnWidthSettingComponent,
                beforeOpenModal: () => this.responseLayoutService.checkCanOpenLayoutEditor(propertyData)
            },
            // {
            //     propertyID: 'col',
            //     propertyName: '占用宽度(%)',
            //     propertyType: 'custom',
            //     editor: CustomWidthEditorComponent,
            //     beforeOpenModal: () => this.setWidthInitDefaultValue(propertyData, viewModelId)
            // },
            {
                propertyID: 'enableTitleTips',
                propertyName: '启用标签提示',
                propertyType: 'boolean',
                description: '是否显示标签提示，一般用于前置任务中',
                defaultValue: false,
                visible: false
            },
            {
                propertyID: 'titleTipsTemplate',
                propertyName: '标签提示内容',
                propertyType: 'string',
                description: '标签提示内容设置',
            },
            {
                propertyID: 'autoHeight',
                propertyName: '自动高度',
                propertyType: 'select',
                iterator: [{ key: true, value: '是' }, { key: false, value: '否' }],
                visible: [DgControl.MultiTextBox.type].includes(propertyData.type)
            }
        ];
    }

    /**
     * 处理外观类属性的联动
     */
    changeAppearancePropertyRelates(properties: PropertyEntity[], changeObject: FormPropertyChangeObject, propertyData, parameters) {
        if (!changeObject) {
            return;
        }

        switch (changeObject.propertyID) {
            case 'title': {
                if (propertyData.binding && propertyData.binding.field && propertyData.binding.type === FormBindingType.Form) {
                    changeObject.needSyncToSchemaField = true;
                }
                changeObject.needUpdateControlTreeNodeName = true;

                break;
            }
            case 'responseLayout': {
                this.responseLayoutService.responseLayoutConfig = changeObject.propertyValue;
                const formNodeId = this.responseLayoutService.changeFormControlsByResponseLayoutConfig(this.componentId);
                this.updateUnifiedLayoutAfterResponseLayoutChanged(this.componentId);

                // 局部刷新组件
                if (formNodeId) {
                    changeObject.needRefreshedComponentId = formNodeId;
                }
                delete propertyData.responseLayout;

                break;
            }
            case 'col': {
                const classList: Array<string> = (propertyData.appearance.class || '').split(' ');
                const newClassList = classList.filter((value, index) => {
                    return value.indexOf('col-') < 0;
                });
                newClassList.push(propertyData.col);
                propertyData.appearance.class = newClassList.toString().replace(/,/g, ' ');
                break;
            }
            case 'labelAutoOverflow': {
                const labelAutoOverflowClass = ' farris-group-multi-label';
                if (changeObject.propertyValue) {
                    propertyData.appearance.class = propertyData.appearance.class + labelAutoOverflowClass;
                } else {
                    propertyData.appearance.class = propertyData.appearance.class.replace(labelAutoOverflowClass, '');
                }
                break;
            }
            case 'titleTipsTemplate': {
                propertyData.enableTitleTips = !!changeObject.propertyValue;
                break;
            }
            case 'labelAutoWidth': {
                const labelAutoWidthClass = ' f-form-lable-auto';
                if (changeObject.propertyValue) {
                    propertyData.appearance.class = propertyData.appearance.class + labelAutoWidthClass;
                } else {
                    propertyData.appearance.class = propertyData.appearance.class.replace(labelAutoWidthClass, '');
                }

                const titleWidth = properties.find((p: PropertyEntity) => p.propertyID === 'titleWidth');
                if (titleWidth) {
                    titleWidth.visible = !changeObject.propertyValue;
                }
                const labelAutoOverflow = properties.find((p: PropertyEntity) => p.propertyID === 'labelAutoOverflow');
                if (labelAutoOverflow) {
                    labelAutoOverflow.visible = !changeObject.propertyValue;
                }
                break;
            }
        }
    }

    /**
     * 获取输入类控件的通用行为属性
     */
    getBehaviorCommonPropConfig(propertyData: any, viewModelId: string): PropertyEntity[] {
        // 只有绑定字段的控件支持表达式，绑定变量的控件不支持表达式
        const bindingFieldId = propertyData.binding && propertyData.binding.type === FormBindingType.Form && propertyData.binding.field;
        const expField = this.domService.expressions && this.domService.expressions.find(e => e.fieldId === bindingFieldId);

        // 获取绑定字段schema中的长度属性
        // let fieldLength;
        let canModifyLength = false;
        let canModifyDefaultValue = false;
        let entityTypeCanModifyDefaultValue = false;
        let fieldInfo;
        // 视图模型中[字段更新时机]属性现在要在控件上维护，所以在控件上复制一份属性值
        if (bindingFieldId) {
            if (!this.dgVMField) {
                const dgViewModel = this.dgVMService.getDgViewModel(viewModelId);
                this.dgVMField = dgViewModel.fields.find(f => f.id === bindingFieldId);
            }
            propertyData.updateOn = this.dgVMField.updateOn;

            if (propertyData.binding && propertyData.binding.fullPath && propertyData.binding.fullPath.indexOf('.') > -1) {
                fieldInfo = this.schemaService.getFieldByPathAndVMID(propertyData.binding.fullPath.split('.')[0], this.viewModelId);
            } else {
                fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, viewModelId);
            }
            if (fieldInfo && fieldInfo.schemaField) {
                // fieldLength = fieldInfo.schemaField.type.length;
                canModifyLength = fieldInfo.schemaField.type.$type === FormSchemaEntityFieldType$Type.StringType;

                this.fieldSchemaConsistentWithBE = this.getFieldSchemaConsistentWithBE(propertyData.binding.field);
                // fieldLengthInBE = this.fieldSchemaConsistentWithBE && this.fieldSchemaConsistentWithBE.type.length;

                canModifyDefaultValue = [
                    FormSchemaEntityFieldType$Type.StringType, FormSchemaEntityFieldType$Type.NumericType,
                    FormSchemaEntityFieldType$Type.DateType, FormSchemaEntityFieldType$Type.DateTimeType].includes(fieldInfo.schemaField.type.$type);
                entityTypeCanModifyDefaultValue = [
                    FormSchemaEntityFieldType$Type.EntityType,
                    FormSchemaEntityFieldType$Type.ObjectType
                ].includes(fieldInfo.schemaField.type.$type);
                propertyData.defaultValue = fieldInfo.schemaField.defaultValue;
            }
        }


        return [
            // {
            //     propertyID: 'binding',
            //     propertyName: '绑定',
            //     propertyType: 'modal',
            //     description: '绑定的表单字段',
            //     editor: BindingEditorComponent,
            //     editorParams: { viewModelId, componentId: this.componentId, controlType: propertyData.type },
            //     converter: new BindingEditorConverter()
            // },
            {
                propertyID: 'visible',
                propertyName: '是否可见',
                propertyType: 'unity',
                description: '运行时组件是否可见',
                editorParams: {
                    constType: 'enum',
                    editorOptions: {
                        types: ['const', 'expression'],
                        enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                        expressionConfig:
                        {
                            editor: ExpressionEditorComponent,
                            beforeOpenModal: () => this.getExpressionEditorParams(propertyData, '可见编辑器', 'visible'),
                            exprValue: this.getExprValue(expField, propertyData.visible)
                        }
                    }
                }
            },
            {
                propertyID: 'readonly',
                propertyName: '只读',
                propertyType: 'unity',
                description: '是否只读',
                editorParams: {
                    constType: 'enum',
                    editorOptions: {
                        types: ['const', 'expression'],
                        enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                        expressionConfig:
                        {
                            editor: ExpressionEditorComponent,
                            beforeOpenModal: () => this.getExpressionEditorParams(propertyData, '只读编辑器', 'readonly'),
                            exprValue: this.getExprValue(expField, propertyData.readonly)
                        }
                    }
                },
            },

            {
                propertyID: 'require',
                propertyName: '必填',
                propertyType: 'unity',
                description: '是否必填',
                editorParams: {
                    constType: 'enum',
                    editorOptions: {
                        types: ['const', 'expression'],
                        enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                        expressionConfig:
                        {
                            editor: ExpressionEditorComponent,
                            beforeOpenModal: () => this.getExpressionEditorParams(propertyData, '必填编辑器', 'require'),
                            exprValue: this.getExprValue(expField, propertyData.require)
                        }
                    }
                }
            },
            // {
            //     propertyID: 'maxLength',
            //     propertyName: '长度',
            //     propertyType: 'number',
            //     description: '最大长度',
            //     decimals: 0,
            //     visible: canModifyLength
            // },
            {
                propertyID: 'defaultValue',
                propertyName: '默认值',
                propertyType: 'unity',
                description: '绑定字段默认值',
                visible: canModifyDefaultValue,
                editorParams: {
                    constType: 'string',
                    editorOptions: {
                        types: ['custom', 'expression'],
                        expressionConfig:
                        {
                            editor: ExpressionEditorComponent,
                            beforeOpenModal: () => this.getDefaultValueExpressionEditorParams(propertyData, viewModelId),
                            exprValue: this.getDefaultValueExprValue(propertyData, viewModelId)
                        }
                    }
                }

            },
            // {
            //     propertyID: 'defaultValue2',
            //     propertyName: '默认值',
            //     propertyType: 'custom',
            //     description: '绑定字段默认值2',
            //     visible: entityTypeCanModifyDefaultValue,
            //     editor: DefaultValueEditorComponent,
            //     beforeOpenModal: () => this.getDefaultValueExprValue2(propertyData, viewModelId)
            // }
        ];
    }

    /**
     * 行为类属性的变更联动方法
     */
    changeBehaviorPropertyRelates(
        properties: PropertyEntity[], changeObject: FormPropertyChangeObject,
        propertyData: any, parameters, showPosition = 'card') {
        if (!changeObject) {
            return;
        }
        switch (changeObject.propertyID) {
            case 'binding': {
                this.updateControlDomAfterChangingBinding(propertyData, parameters, this.viewModelId);
                break;
            }
            case 'readonly': case 'visible': case 'require': {
                // 属性值从表达式切换为其他类型：需要清空DOM中的表达式配置
                const newValueType = changeObject.propertyValue && changeObject.propertyValue.type;
                this.clearExpression(newValueType, propertyData, changeObject.propertyID);
                break;
            }
            case 'updateOn': {
                // 字段更新时机
                const dgVM = this.dgVMService.getDgViewModel(this.viewModelId);
                if (dgVM && this.dgVMField) {
                    dgVM.changeField(this.dgVMField.id, { updateOn: changeObject.propertyValue });
                }
                // delete propertyData.updateOn;

                break;
            }
            case 'maxLength': {
                this.syncSchemaAfterLengthChanged(changeObject, propertyData);

                break;
            }
            case 'defaultValue': {
                // 默认值同步至schema，在表单保存时同步be vo
                if (propertyData.binding && propertyData.binding.field) {
                    let fieldInfo;
                    if (propertyData.binding.fullPath && propertyData.binding.fullPath.indexOf('.') > -1) {
                        fieldInfo = this.schemaService.getFieldByPathAndVMID(propertyData.binding.fullPath.split('.')[0], this.viewModelId);
                    } else {
                        fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, this.viewModelId);
                    }
                    if (fieldInfo && fieldInfo.schemaField) {
                        if (!changeObject.propertyValue || typeof (changeObject.propertyValue) === 'string') {
                            fieldInfo.schemaField.defaultValue = changeObject.propertyValue || '';
                        } else {
                            fieldInfo.schemaField.defaultValue = {
                                type: "expression",
                                value: changeObject.propertyValue.expressionId
                            };
                        }

                    }
                    delete propertyData.defaultValue;
                }
                break;
            }
            case 'defaultValue2': {
                // 默认值同步至schema，在表单保存时同步be vo
                if (propertyData.binding && propertyData.binding.field) {
                    let fieldInfo;
                    if (propertyData.binding.fullPath && propertyData.binding.fullPath.indexOf('.') > -1) {
                        fieldInfo = this.schemaService.getFieldByPathAndVMID(propertyData.binding.fullPath.split('.')[0], this.viewModelId);
                    } else {
                        fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, this.viewModelId);
                    }
                    if (fieldInfo && fieldInfo.schemaField) {
                        if (!changeObject.propertyValue || typeof (changeObject.propertyValue) === 'string') {
                            fieldInfo.schemaField.defaultValue = changeObject.propertyValue || '';
                        } else {
                            fieldInfo.schemaField.defaultValue = {
                                type: 'expression',
                                value: changeObject.propertyValue.expressionId
                            };
                        }

                    }
                    delete propertyData.defaultValue2;
                }
                break;
            }

        }

    }


    /**
     * 根据字段ID获取schema字段信息
     */
    private getFieldSchemaConsistentWithBE(fieldId: string) {
        const previousDomJson = this.domService.getPreviousDomJson();
        const entities = previousDomJson.module.schemas[0].entities;
        if (!entities || entities.length === 0) {
            return;
        }
        for (const viewModel of this.domService.viewmodels) {
            if (viewModel.bindTo === '/' && !viewModel.parent) {
                continue;
            }
            const fields = this.schemaService.getTableFieldsByBindTo(entities, viewModel.bindTo);
            if (!fields) {
                continue;
            }
            const result = this.schemaService.getFieldInfoByID(fields, '', fieldId);
            if (result && result.schemaField) {
                return result.schemaField;
            }
        }
    }
    /**
     * 修改长度后同步schema
     * @param changeObject 变更集
     * @param propertyData 属性值
     */
    private syncSchemaAfterLengthChanged(changeObject: FormPropertyChangeObject, propertyData: any) {
        const previousFieldLength = this.fieldSchemaConsistentWithBE && this.fieldSchemaConsistentWithBE.type.length;
        let currentFieldLength;
        // 当前拖拽新增的字段，be中没有：同步schema
        if (!previousFieldLength) {
            currentFieldLength = changeObject.propertyValue;
        } else {
            // be中已有的字段：若将长度大于be长度，则同步；若长度小于be长度，则将schema长度修改为be中的长度
            currentFieldLength = changeObject.propertyValue > previousFieldLength ? changeObject.propertyValue : previousFieldLength;
        }

        // 长度同步至schema，在表单保存时同步be vo
        if (propertyData.binding && propertyData.binding.field) {
            const fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, this.viewModelId);
            if (fieldInfo && fieldInfo.schemaField) {
                fieldInfo.schemaField.type.length = currentFieldLength;
            }
        }

    }

    /**
     * 修改绑定后要同步变更控件path属性，若是帮助控件还要变更数据源属性并同步viewModel
     */
    updateControlDomAfterChangingBinding(propertyData: any, parameters: any, viewModelId: string) {
        const selectedData = parameters && parameters.selectedData;
        if (!selectedData) {
            return;
        }
        // 变更path属性
        propertyData.path = selectedData.bindingPath;

        // 切换当前dgVMField
        this.checkCanChangeControlType(propertyData, viewModelId);

        // 帮助控件变更dataSource.uri属性
        if (propertyData.type === DgControl.LookupEdit.type && propertyData.dataSource && propertyData.dataSource.uri) {
            const uri = propertyData.dataSource.uri;
            const index = uri.indexOf('.');
            if (index < 0) {
                return;
            }

            if (propertyData.binding.type === FormBindingType.Form) {
                const newUri = uri.slice(0, index) + '.' + selectedData.bindingField;
                propertyData.dataSource.uri = newUri;

                // 同步viewModel Field
                const dgViewModel = this.dgVMService.getDgViewModel(viewModelId);
                dgViewModel.changeField(propertyData.binding.field, { editor: { dataSource: propertyData.dataSource } });
            } else {
                const newUri = uri.slice(0, index) + '.' + selectedData.code;
                propertyData.dataSource.uri = newUri;
            }

        }
    }

    /**
     * 修改控件布局配置后更新Form统一布局配置
     * @param componentId 组件Id
     * @param controlLayoutConfig 某单独变动的控件配置项，FormResponseLayoutContext类型
     */
    private updateUnifiedLayoutAfterResponseLayoutChanged(componentId: string, controlLayoutConfig?: any): FormUnifiedColumnLayout {
        const componentNode = this.domService.getComponentById(componentId);
        if (!componentNode || !componentNode.componentType || !componentNode.componentType.startsWith('form')) {
            return;
        }
        const formNode = this.domService.selectNode(componentNode, item => item.type === DgControl.Form.type);
        if (!formNode || !formNode.unifiedLayout) {
            return;
        }

        const responseLayoutConfig: FormResponseLayoutContext[] = [];

        this.responseLayoutService.getFormResonseLayoutConfig(formNode, responseLayoutConfig, 1);

        if (controlLayoutConfig) {
            const changedControl = responseLayoutConfig.find(c => c.id === controlLayoutConfig.id);
            Object.assign(changedControl, controlLayoutConfig);
        }

        // 收集每种屏幕下的列数
        const columnInSMArray = responseLayoutConfig.map(config => config.columnInSM);
        const columnInMDArray = responseLayoutConfig.map(config => config.columnInMD);
        const columnInLGArray = responseLayoutConfig.map(config => config.columnInLG);
        const columnInELArray = responseLayoutConfig.map(config => config.columnInEL);

        // 只有每个控件的宽度都一样时，才认为form上有统一宽度，否则认为是自定义的控件宽度，此处传递null
        const uniqueColClassInSM = this.checkIsUniqueColumn(columnInSMArray) ? columnInSMArray[0] : null;
        const uniqueColClassInMD = this.checkIsUniqueColumn(columnInMDArray) ? columnInMDArray[0] : null;
        const uniqueColClassInLG = this.checkIsUniqueColumn(columnInLGArray) ? columnInLGArray[0] : null;
        const uniqueColClassInEL = this.checkIsUniqueColumn(columnInELArray) ? columnInELArray[0] : null;


        Object.assign(formNode.unifiedLayout, {
            uniqueColClassInSM,
            uniqueColClassInMD,
            uniqueColClassInLG,
            uniqueColClassInEL
        });
    }
    /**
     * 校验宽度样式值是否一致
     */
    private checkIsUniqueColumn(columnsInScreen: number[]) {
        const keySet = new Set(columnsInScreen);
        const exclusiveKeys = Array.from(keySet);

        if (exclusiveKeys.length === 1) {
            return true;
        }
        return false;
    }


    /**
     * 获取表达式编辑需要的参数：在属性面板弹出表达式窗口时调用
     */
    private getExpressionEditorParams(propertyData: any, modalTitle: string, expressionType: string) {

        let expressionStr = null;

        const bindingFieldId = propertyData.binding && propertyData.binding.field;

        const expressionParams: ExpressionEditorParam = {
            modalTitle: modalTitle || '表达式编辑器',
            fieldId: bindingFieldId,
            viewModelId: this.viewModelId,
            expType: expressionType
        };

        // 必填表达式需要配置提示消息
        if (expressionType === 'require') {
            expressionParams.message = '';
        }


        const propertyValue = propertyData[expressionType];
        if (propertyValue && propertyValue.expressionId && bindingFieldId) {
            const expField = this.domService.expressions.find(e => e.fieldId === bindingFieldId);

            // 检测到现有的表达式配置
            if (expField) {
                const originalExpConfig = expField.expression.find(exp => exp.id === propertyValue.expressionId);
                expressionStr = originalExpConfig.value;

                if (expressionType === 'require') {
                    expressionParams.message = originalExpConfig.message;
                }
            }
        }

        return {
            editorParams: expressionParams,
            value: expressionStr
        };
    }

    /**
     * 获取属性编辑器需要的初始绑定值
     * @param expField 表达式配置字段
     * @param propertyValue 属性值
     */
    private getExprValue(expField: FormExpression, propertyValue: any) {
        let expressionStr = null;
        if (propertyValue && propertyValue.expressionId && expField) {
            const originalExpConfig = expField.expression.find(exp => exp.id === propertyValue.expressionId);
            expressionStr = originalExpConfig && originalExpConfig.value;

        }

        return {
            type: 'expression',
            value: {
                value: expressionStr,
                parameters: propertyValue && propertyValue.expressionId,
                type: 'Expression'
            }
        };
    }

    /**
     * 获取默认值表达式编辑需要的参数：在属性面板弹出表达式窗口时调用
     */
    private getDefaultValueExpressionEditorParams(propertyData: any, viewModelId: string) {

        const expressionParams: ExpressionEditorParam = {
            modalTitle: '默认值编辑器',
            fieldId: propertyData.binding && propertyData.binding.field,
            viewModelId: null,
            expType: 'defaultValue',
            hideFieldSchema: true
        };

        let defaultValue = null;
        const fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, viewModelId);
        if (fieldInfo) {
            defaultValue = fieldInfo.schemaField.defaultValue;
        }

        // 组装成多值属性编辑器要求的格式

        if (typeof (defaultValue) === 'string') {
            return {
                editorParams: expressionParams,
                value: null
            }
        } else {
            return {
                editorParams: expressionParams,
                value: defaultValue.value
            }
        }


    }
    /**
     * 获取属性编辑器需要的初始绑定值
     * @param expField 表达式配置字段
     * @param propertyValue 属性值
     */
    private getDefaultValueExprValue(propertyData: any, viewModelId: string) {
        let defaultValue = null;
        let fieldInfo = null;
        if (propertyData.binding.fullPath && propertyData.binding.fullPath.indexOf('.') > -1) {
            fieldInfo = this.schemaService.getFieldByPathAndVMID(propertyData.binding.fullPath.split('.')[0], this.viewModelId);
        } else {
            fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, viewModelId);
        }
        if (fieldInfo) {
            defaultValue = fieldInfo.schemaField.defaultValue;
        }

        // 组装成多值属性编辑器要求的格式
        if (!defaultValue || typeof (defaultValue) === 'string') {
            return {
                type: 'expression',
                value: {
                    value: null,
                    type: 'Expression'
                }
            }

        } else {
            return {
                type: 'expression',
                value: {
                    value: defaultValue.value,
                    type: 'Expression'
                }
            }
        }

    }

    /**
     * 获取属性编辑器需要的初始绑定值
     * @param expField 表达式配置字段
     * @param propertyValue 属性值
     */
    private getDefaultValueExprValue2(propertyData: any, viewModelId: string) {
        let defaultValue = null;
        let fieldInfo = null;
        if (propertyData.binding.fullPath && propertyData.binding.fullPath.indexOf('.') > -1) {
            fieldInfo = this.schemaService.getFieldByPathAndVMID(propertyData.binding.fullPath.split('.')[0], this.viewModelId);
        } else {
            fieldInfo = this.schemaService.getFieldByIDAndVMID(propertyData.binding.field, viewModelId);
        }
        if (fieldInfo) {
            defaultValue = fieldInfo.schemaField.defaultValue;
        }

        //组装成多值属性编辑器要求的格式
        let returnValue = {
            result: true,
            parameters: {
                type: propertyData.type,
                multiSelect: propertyData.multiSelect,
                value: defaultValue
            }
        }
        if (propertyData.type == DgControl.AdminOrganizationSelector.type) {
            returnValue.parameters['onlyDisplayCompany'] = propertyData.hasOwnProperty('onlyDisplayCompany') ? propertyData.onlyDisplayCompany : false;
        }
        return returnValue;
    }

    /**
     * 校验控件是否支持切换类型
     * @param control 控件
     */
    private checkCanChangeControlType(propertyData: any, viewModelId: string): boolean {
        if (!propertyData.binding) {
            return false;
        }
        // 没有绑定信息的控件
        if (!propertyData.binding) {
            return false;
        }

        // 不可编辑的列表、树列不能切换类型
        if (propertyData.type === DgControl.TreeGridField.type || (propertyData.type === DgControl.GridField.type && !propertyData.editor)) {
            return false;
        }
        if (propertyData.binding.type === 'Variable') {
            this.bindingVarible = this.domService.getVariableById(propertyData.binding.field);
            // vm中已移除的变量
            if (!this.bindingVarible) {
                return false;
            }

        } else {
            const dgViewModel = this.dgVMService.getDgViewModel(viewModelId);
            this.dgVMField = dgViewModel.fields.find(f => f.id === propertyData.binding.field);

            // schema中已移除的字段
            if (!this.dgVMField) {
                return false;
            }

        }
        return true;

    }

    /**
     * 清除原表达式
     */
    clearExpression(newValueType: string, propertyData: any, propertyID: string) {
        if (newValueType !== 'Expression' && this.domService.expressions) {
            const bindingFieldId = propertyData.binding && propertyData.binding.field;
            const exp = this.domService.expressions.find(e => e.fieldId === bindingFieldId);
            if (!exp || !exp.expression) {
                return;
            }
            exp.expression = exp.expression.filter(e => e.type !== propertyID);
        }

    }
    /**
     * 卡片控件：切换控件类型后事件
     * @param propertyData 控件DOM属性
     * @param newControlType 新控件类型
     */
    changeControlType(propertyData, newControlType: string) {
        // 1、定位控件父容器
        const cmp = this.domService.getComponentById(this.componentId);
        const parentContainer = this.getControlParentContainer(propertyData.id, cmp);
        if (!parentContainer) {
            return;
        }
        const index = parentContainer.contents.findIndex(c => c.id === propertyData.id);
        const oldControl = parentContainer.contents[index];

        let newControl;
        // 绑定字段
        if (this.dgVMField) {
            // ①、记录viewModel 变更---全量替换editor
            const dgViewModel = this.dgVMService.getDgViewModel(this.viewModelId);
            dgViewModel.changeField(this.dgVMField.id, {
                editor: {
                    $type: newControlType
                },
                name: this.dgVMField.name,
                require: this.dgVMField.require,
                readonly: this.dgVMField.readonly
            }, false);
            // ② 创建新控件
            newControl = this.controlCreatorService.createControlBySchemaFeild(this.dgVMField, cmp.componentType);
        } else {
            // 绑定变量
            // ① 记录viewModel 变更---全量替换editor
            const vmVariable = this.domService.getViewModelFieldById(this.viewModelId, this.bindingVarible.id);
            Object.assign(vmVariable, {
                editor: {
                    $type: newControlType
                }
            });
            // ② 创建新控件
            newControl = this.controlCreatorService.createControlByVariable(this.bindingVarible, cmp.componentType, newControlType, this.componentId);
        }


        //  保留原id和isRTControl、样式等属性
        Object.assign(newControl, {
            id: oldControl.id,
            isRTControl: !!oldControl.isRTControl,
            appearance: oldControl.appearance,
            size: oldControl.size,
            titleSourceType: oldControl.titleSourceType,
            title: oldControl.title,
            titleDataSource: oldControl.titleDataSource,
            titleWidth: oldControl.titleWidth,
            labelAutoWidth: oldControl.labelAutoWidth,
            labelAutoOverflow: oldControl.labelAutoOverflow,
            enableTitleTips: oldControl.enableTitleTips,
            titleTipsTemplate: oldControl.titleTipsTemplate,
            isTextArea: newControl.isTextArea && oldControl.isTextArea,
            placeHolder: oldControl.placeHolder,
            holdPlace: oldControl.holdPlace,
            readonly: oldControl.readonly,
            require: oldControl.require,
            visible: oldControl.visible
        });

        // 4、替换控件
        parentContainer.contents.splice(index, 1, newControl);

        // 5、触发父容器刷新
        this.refreshFormService.refreshFormDesigner.next(parentContainer.id);

        return parentContainer.id;

    }

    /**
     * 获取控件：以contents为节点进行定位
     * @param controlId 控件ID
     * @param domJson 容器
     */
    private getControlParentContainer(controlId: string, domJson: any) {
        let container;
        if (!domJson || !domJson.contents || domJson.contents.length === 0) {
            return null;
        }

        for (const content of domJson.contents) {
            if (content.id === controlId) {
                container = domJson;
                return domJson;
            }
            container = this.getControlParentContainer(controlId, content);
            if (container) {
                return container;
            }
        }
    }


    /**
     * 列编辑器类型切换属性
     */
    getGridFieldEditorTypePropertyConfig(gridFieldData: any, viewModelId: string) {
        const self = this;
        const propertyData = gridFieldData.editor;
        this.gridFieldData = gridFieldData;
        let config: ElementPropertyConfig;

        const canChangeControlType = this.checkCanChangeControlType(propertyData, viewModelId);

        if (canChangeControlType) {
            let controlTypeList = [];
            // tslint:disable-next-line:max-line-length
            controlTypeList = this.schemaDOMMapping.getEditorTypesByMDataType(this.dgVMField.type.name, this.dgVMField.multiLanguage, 'dataGrid', this.formBasicService.envType);
            config = {
                categoryId: 'gridFieldEditor_editorType',
                categoryName: '编辑器类型',
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: 'gridFieldEditor',
                tabName: '编辑器',
                properties: [
                    {
                        propertyID: 'type',
                        propertyName: '编辑器类型',
                        propertyType: 'select',
                        readonly: !canChangeControlType,
                        iterator: controlTypeList,
                        refreshPanelAfterChanged: true
                    }
                ],
                setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                    if (!changeObject) {
                        return;
                    }
                    switch (changeObject && changeObject.propertyID) {
                        case 'type': {
                            self.changeGridFieldEditorType(changeObject.propertyValue, viewModelId);
                            break;
                        }
                    }
                }
            };

        }
        return config;
    }
    /**
     * datagrid field:列编辑器类型切换后事件
     * @param newControlType 新控件类型
     * @param viewModelId 视图模型ID
     */
    changeGridFieldEditorType(newControlType: string, viewModelId: string) {

        // 1、记录viewModel 变更---全量替换editor
        const dgViewModel = this.dgVMService.getDgViewModel(viewModelId);
        dgViewModel.changeField(this.dgVMField.id, {
            editor: {
                $type: newControlType
            },
            name: this.dgVMField.name,
            require: this.dgVMField.require,
            readonly: this.dgVMField.readonly
        }, false);


        // 2、定位列表
        const cmp = this.domService.getComponentByVMId(viewModelId);
        const grid = this.domService.selectNode(cmp, item => item.type === DgControl.DataGrid.type || item.type === DgControl.TreeGrid.type);

        if (!grid) {
            return;
        }
        // 3、创建新列
        const editable = grid.fieldEditable;
        const dataField = this.dgVMField.bindingPath;
        const gridFieldType = grid.type === DgControl.DataGrid.type ? DgControl.GridField.type : DgControl.TreeGridField.type;

        const newField = this.controlCreatorService.createGridFieldBySchemaFeild(this.dgVMField, dataField, editable,
            gridFieldType, grid.controlSource);

        // 4、替换列编辑器
        const index = grid.fields.findIndex(c => c.id === this.gridFieldData.id);
        if (index > -1 && grid.fields[index].editor) {
            grid.fields[index].editor = Object.assign(newField.editor, {
                id: grid.fields[index].editor.id,
                isRTControl: !!grid.fields[index].editor.isRTControl,
                title: grid.fields[index].editor.title
            });
        }


    }

    /**
     * 追加列编辑器内的必填、字段更新时机属性
     * @param editorProp 列编辑器属性分类
     * @param gridFieldEditorData  列编辑器数据
     * @param viewModelId viewModelId
     */
    appendBehaviorPropsForGridFieldEditor(editorProp: ElementPropertyConfig, gridFieldEditorData: any, viewModelId: string) {
        // 获取绑定字段对应的表达式
        const bindingFieldId = gridFieldEditorData.binding && gridFieldEditorData.binding.type === FormBindingType.Form && gridFieldEditorData.binding.field;
        const expField = this.domService.expressions && this.domService.expressions.find(e => e.fieldId === bindingFieldId);

        // 视图模型中[字段更新时机]属性现在要在控件上维护，所以在控件上复制一份属性值
        if (bindingFieldId && !gridFieldEditorData.updateOn && this.dgVMField) {
            gridFieldEditorData.updateOn = this.dgVMField.updateOn;
        }

        editorProp.properties.unshift(
            {
                propertyID: 'require',
                propertyName: '是否必填',
                propertyType: 'unity',
                description: '是否必填',
                editorParams: {
                    controlName: UniformEditorDataUtil.getControlName(gridFieldEditorData),
                    constType: 'enum',
                    newVariableType: 'Boolean',
                    newVariablePrefix: 'is',
                    editorOptions: {
                        types: ['const', 'expression'],
                        enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                        expressionConfig:
                        {
                            editor: ExpressionEditorComponent,
                            beforeOpenModal: () => this.getExpressionEditorParams(gridFieldEditorData, '必填编辑器', 'require'),
                            exprValue: this.getExprValue(expField, gridFieldEditorData.require)
                        }
                    }
                }
            },
            // {
            //     propertyID: 'updateOn',
            //     propertyName: '绑定字段更新时机',
            //     propertyType: 'select',
            //     iterator: [
            //         { key: 'change', value: '值变化时' },
            //         { key: 'blur', value: '控件失去焦点时' }
            //     ],
            //     description: '控件绑定字段的更新时机'
            // }
        );
    }

    /**
     * 组装输入类控件的交互面板：包含标签超链、绑定字段值变化前后事件等。
     * @param propertyData 属性值
     * @param viewModelId 视图模型id
     * @param showPosition 面板展示位置
     * @param customEvent 输入控件特有的事件配置
     */
    public getEventPropertyConfig(propertyData: any, viewModelId: string, showPosition = 'card', customEvent?: {
        customEventList?: { label: string, name: string }[],
        setPropertyRelates?: (propertyData) => void
    }): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;

        // 静态事件
        let eventList = [];
        if (customEvent && customEvent.customEventList) {
            eventList = eventList.concat(customEvent.customEventList);
        }
        // 动态事件
        eventList = this.switchEvents(propertyData, eventList);

        // 追加值变化、标签超链事件
        this.appendFieldValueChangeEvents(propertyData, eventList);
        this.appendLinkLabelClickEvent(propertyData, eventList, showPosition);



        const config = {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList, this.switchEvents),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: FormPropertyChangeObject, data: any, parameters: any) {
                delete propertyData[viewModelId];
                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates;
                    EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, parameters['events'], parameters);
                    this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, parameters['events'], self.switchEvents);
                }

                if (customEvent && customEvent.setPropertyRelates) {
                    customEvent.setPropertyRelates(propertyData);
                }

                // 联动修改【启用标签超链接】属性
                if (showPosition === 'gridFieldEditor' && self.gridFieldData) {
                    // 列表列的超链接属性是在GridField上的，不是在editor里，所以这里要单独赋值
                    self.gridFieldData.linkedLabelClick = propertyData.linkedLabelClick;
                    self.gridFieldData.linkedLabelEnabled = !!propertyData.linkedLabelClick;
                    // 清除editor内的超链属性
                    propertyData.linkedLabelClick = null;
                    propertyData.linkedLabelEnabled = false;
                } else {
                    propertyData.linkedLabelEnabled = !!propertyData.linkedLabelClick;
                }


                // 同步视图模型值变化事件
                const dgVM = self.dgVMService.getDgViewModel(self.viewModelId);
                if (dgVM && self.dgVMField) {
                    dgVM.changeField(self.dgVMField.id, { valueChanging: propertyData.fieldValueChanging, valueChanged: propertyData.fieldValueChanged });
                }
            }
        };
        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor'
            });
        }

        return config;
    }

    /**
     * 处理交互面板中的动态事件
     * @param propertyData 属性值
     * @param eventList 事件列表
     */
    switchEvents(propertyData: any, eventList: any[]) {
        // 根据是否【启用扩展区域】属性判断是否显示【扩展按钮点击事件】
        if (propertyData.enableAppend !== false && propertyData.inputAppendType === 'button') {
            const eventListExist = eventList.find(eventListItem => eventListItem.label === 'inputAppendClickEvent');
            if (!eventListExist) {
                eventList.push(
                    {
                        label: 'inputAppendClickEvent',
                        name: '扩展按钮点击事件'
                    }
                );
            }
        } else {
            if (eventList.length !== 0) {
                eventList = eventList.filter(eventListItem => eventListItem.label !== 'inputAppendClickEvent');
            }
        }

        return eventList;

    }
    /**
     * 将标签超链属性追加到交互面板，
     */
    private appendLinkLabelClickEvent(propertyData: any, eventList: any[], showPosition: string) {
        // 只有卡片和可编辑列表中有标签超链
        if (showPosition !== 'card' && showPosition !== 'gridFieldEditor') {
            return;
        }
        // 部分控件没有标签
        const controlsWithoutLabel = [DgControl.Avatar.type, DgControl.Image.type];
        if (controlsWithoutLabel.includes(propertyData.type)) {
            return;
        }

        eventList.push({
            label: 'linkedLabelClick',
            name: '标签超链事件'
        });

        // 因为需要将GridField editor内的事件与GridField上超链事件合并起来展示到交互面板中，所以这里先将事件复制到editor里去
        if (showPosition === 'gridFieldEditor' && this.gridFieldData) {
            propertyData.linkedLabelClick = this.gridFieldData.linkedLabelClick;
        }

    }
    /**
     * 将字段值变化前事件、变化后事件追加到交互面板
     * 注意：因为绑定字段值变化后事件与下拉框的值变化事件(valueChanged)重名，所以这里将事件label都重命名下
     */
    private appendFieldValueChangeEvents(propertyData: any, eventList: any[]) {
        // 若绑定了字段，则需要显示字段变更前后事件
        if (propertyData.binding && propertyData.binding.type === FormBindingType.Form && propertyData.binding.field) {
            const valueChangingExist = eventList.find(eventListItem => eventListItem.label === 'fieldValueChanging');
            if (!valueChangingExist) {
                eventList.push(
                    {
                        label: 'fieldValueChanging',
                        name: '绑定字段值变化前事件'
                    }
                );
            }
            const valueChangedExist = eventList.find(eventListItem => eventListItem.label === 'fieldValueChanged');
            if (!valueChangedExist) {
                eventList.push(
                    {
                        label: 'fieldValueChanged',
                        name: '绑定字段值变化后事件'
                    }
                );
            }
            if (this.dgVMField) {
                // 因为字段变更属性是存到VM中的，但是这里需要在控件上编辑，所以复制一份数据
                propertyData.fieldValueChanging = this.dgVMField.valueChanging;
                propertyData.fieldValueChanged = this.dgVMField.valueChanged;
            }


        } else {
            // 未绑定字段，则移除值变化事件
            eventList = eventList.filter(eventListItem => eventListItem.label !== 'fieldValueChanging' && eventListItem.label !== 'fieldValueChanged');
        }
    }

    /**
     * 获取自定义宽度控件设置初始默认值
     * @param propertyData 属性值
     * @param viewModelId 视图模型id
     */
    setWidthInitDefaultValue(propertyData: any, viewModelId: string) {
        const iterator = [
            { value: '25', label: 'col-3' },
            { value: '33', label: 'col-4' },
            { value: '50', label: 'col-6' },
            { value: '75', label: 'col-9' },
            { value: '100', label: 'col-12' }
        ];
        const result = {
            result: true,
            parameters: {
                customOptions: iterator,
                defaultValue: ''
            }
        };
        if (propertyData.appearance.class) {
            let styleClass = propertyData.appearance.class.split(' ');
            for (let i in styleClass) {
                switch (styleClass[i]) {
                    case 'col-md-3': case 'col-md-4':
                    case 'col-md-6': case 'col-md-9':
                    case 'col-md-12': {
                        propertyData.col = '1';
                    }
                }
            };
        }
        const value = propertyData.col;
        result.parameters.defaultValue = value ? value : 'col-6';
        return result;
    }

}
