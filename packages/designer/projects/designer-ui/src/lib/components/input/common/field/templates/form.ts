

export default (ctx: any) => {

  if (ctx.showLabel) {
    return getTemplateWithLabel(ctx);
  } else {
    return getTemplateWithoutLabel(ctx);
  }

};

/**
 * 包含标签的输入类模板
 */
function getTemplateWithLabel(ctx: any) {

  const require = ctx.component.require === true ? '<span class="farris-label-info text-danger">*</span>' : '';
  const titleTips = ctx.component.enableTitleTips ? `<span  class="farris-label-tips"><i class="f-icon f-icon-description-tips"></i> </span>` : '';
  let resultStr = `<div class="farris-group-wrap"><div class="form-group farris-form-group`;
  // 启用标签提示
  if (ctx.component.enableTitleTips) {
    resultStr += ` form-group--has-tips`;
  }
  // 绑定字段失效提示信息
  const inValidBindingTips = getInValidBindingTipsAfterLabel(ctx);

  const labelTitle = inValidBindingTips ? '绑定信息已失效，请切换绑定或移除控件' : ctx.component.title;
  const labelColor = inValidBindingTips ? ` style='color:red;'` : '';

  resultStr += `
     "><label class="col-form-label" ${labelColor}>
            ${inValidBindingTips}
            ${require}
            <span class="farris-label-text" title="${labelTitle}">${ctx.component.title}</span>
        </label>
        ${ctx.element}
        ${titleTips}
    </div>
  </div>`;
  return resultStr;
}


/**
 * 不包含标签的输入类模板
 */
function getTemplateWithoutLabel(ctx: any) {

  const inValidBindingTips = getInValidBindingTipsAfterWrap(ctx);

  return ` ${ctx.element}${inValidBindingTips}`;
}

/** 绑定字段失效提示，用于卡片内的输入控件 */
function getInValidBindingTipsAfterLabel(ctx: any) {
  if (ctx.isValidBinding) {
    return '';
  }
  return `<span class="f-icon f-icon-warning"  style="color: red; min-width: 16px;" title="绑定信息已失效，请切换绑定或移除控件"></span>`;
}

/** 绑定字段失效提示，用于表格类控件没有label的场景 */
function getInValidBindingTipsAfterWrap(ctx: any) {
  if (ctx.isValidBinding) {
    return '';
  }
  return `<span class="f-icon f-icon-warning"  style="position: absolute;top: 0;right: 11px;color: red;display: flex;height: 100%;align-items: center;" title="绑定信息已失效，请切换或清空绑定"></span>`;
}
