import { TextBoxSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { TextBoxProp } from '../property/property-config';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeTextBoxProp } from '../property/nocode-property-config';

export default class FdTextComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return TextBoxSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('TextBox', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;


    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeTextBoxProp = new NoCodeTextBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: TextBoxProp = new TextBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }


}
