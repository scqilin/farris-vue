export const ImageUploadSchema =  {
    id: 'ImageUpload',
    type: 'ImageUpload',
    title: '图像上传',
    size: null,
    disable: false,
    appearance: null,
    border: null,
    maxUploads: 0,
    maxSize: null,
    imgType: [
      'png',
      'jpg',
      'jpeg',
      'webp',
      'svg',
      'gif',
      'bmp'
    ],
    storeType: 'metadata',
    rootId: 'default-root',
    formId: 'viewModel.bindingData && viewModel.bindingData[\'id\']',
    readonly: false,
    require: false,
    visible: true,
    click: null,
    imgChange: null,
    linkedLabelEnabled: false,
    linkedLabelClick: null
};

