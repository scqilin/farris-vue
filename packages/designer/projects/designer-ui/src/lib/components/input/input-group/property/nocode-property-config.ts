import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { InputGroupProp } from './property-config';

export class NoCodeInputGroupProp extends InputGroupProp {


    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        let propertyConfig: ElementPropertyConfig[] = [];

        // 弹出表单框属性
        const openModalPropConfigs = this.getOpenModalPropConfigs(propertyData, this.viewModelId);

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId, openModalPropConfigs);
        propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId, openModalPropConfigs);
        propertyConfig.push(behaviorPropConfig);
        propertyConfig = propertyConfig.concat(openModalPropConfigs);

        // 扩展区域属性
        // const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, this.viewModelId);
        // propertyConfig.push(appendPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, this.viewModelId);
        if (exprPropConfig) {
            propertyConfig.push(exprPropConfig);
        }

        return propertyConfig;
    }
}
