import FdTagsComponent from './component/fd-tags';
import { TagsSchema } from './schema/schema';

import FdTagsTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const Tags: ComponentExportEntity = {
    type: 'Tags',
    component: FdTagsComponent,
    template: FdTagsTemplates,
    metadata: cloneDeep(TagsSchema)
};
