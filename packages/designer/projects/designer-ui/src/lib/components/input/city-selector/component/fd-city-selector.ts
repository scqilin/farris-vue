import { DesignerEnvType } from '@farris/designer-services';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import FarrisDesignField from '../../common/field/component/field';
import { NoCodeCitySelectorProp } from '../property/nocode-property-config';
import { CitySelectorProp } from '../property/property-config';
import { CitySelectorSchemas } from '../schema/schema';

export default class FdInputGroupComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }

  getDefaultSchema(): any {
    return CitySelectorSchemas;
  }

  getStyles(): string {
    return 'display: inline-block;';
  }

  render(): any {

    return super.render(this.renderTemplate('CitySelector', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;

    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeCitySelectorProp = new NoCodeCitySelectorProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: CitySelectorProp = new CitySelectorProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }

  onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
    super.onPropertyChanged(changeObject, ['groupText']);
  }
}
