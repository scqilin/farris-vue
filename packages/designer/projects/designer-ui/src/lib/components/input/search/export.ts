import { ComponentExportEntity } from '@farris/designer-element';
import FdSearchTemplates from './templates';
/**
 * 搜索控件，目前只用于筛选条和筛选方案，不单独使用。
 */
export const Search: ComponentExportEntity = {
    type: 'Search',
    template: FdSearchTemplates,
    metadata: {}
};
