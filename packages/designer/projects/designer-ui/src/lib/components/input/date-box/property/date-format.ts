

export const DATE_FORMATS = {

    timeFormats: [
        { key: 'yyyy-MM-dd HH:mm:ss', value: 'yyyy-MM-dd HH:mm:ss' },
        { key: 'yyyy/MM/dd HH:mm:ss', value: 'yyyy/MM/dd HH:mm:ss' },
        { key: 'yyyy年MM月dd日 HH时mm分ss秒', value: 'yyyy年MM月dd日 HH时mm分ss秒' },
        { key: 'yyyyMMddHHmmss', value: 'yyyyMMddHHmmss' },
        { key: 'yyyy-MM-dd HH:mm', value: 'yyyy-MM-dd HH:mm' },
        { key: 'yyyy/MM/dd HH:mm', value: 'yyyy/MM/dd HH:mm' },
        { key: 'yyyy年MM月dd日 HH时mm分', value: 'yyyy年MM月dd日 HH时mm分' },
        { key: 'yyyyMMddHHmm', value: 'yyyyMMddHHmm' }
    ],

    yMdFormats: [
        { key: 'yyyy-MM-dd', value: 'yyyy-MM-dd' },
        { key: 'yyyy年MM月dd日', value: 'yyyy年MM月dd日' },
        { key: 'yyyy/MM/dd', value: 'yyyy/MM/dd' },
        { key: 'yyyyMMdd', value: 'yyyyMMdd' },
        { key: 'MM/dd/yyyy', value: 'MM/dd/yyyy' }
    ],

    yMFormats: [
        { key: 'yyyy年MM月', value: 'yyyy年MM月' },
        { key: 'yyyy-MM', value: 'yyyy-MM' },
        { key: 'yyyy/MM', value: 'yyyy/MM' },
        { key: 'yyyyMM', value: 'yyyyMM' }
    ],

    mdFormats: [
        { key: 'MM/dd', value: 'MM/dd' },
        { key: 'MM月dd日', value: 'MM月dd日' },
        { key: 'MMdd', value: 'MMdd' },
        { key: 'MM-dd', value: 'MM-dd' }
    ],

    yFormats: [
        { key: 'yyyy', value: 'yyyy' },
        { key: 'yyyy年', value: 'yyyy年' }
    ]
};
