

export default (ctx: any) => {
  const textField = ctx.component.textField || 'name';
  const valueField = ctx.component.valueField || 'value';
  const checkDatas = ctx.component.items && ctx.component.items.length>0 ? ctx.component.items : [{ value: '1', name: '选项1' }];
  let checkItems = '';
  checkDatas.forEach((item, index) => {
    checkItems += `<div class="custom-control custom-checkbox" > `;
    checkItems += ` <input class="custom-control-input" type = "checkbox" name = "${ctx.component.id}" disabled id = "checkbox_${ctx.component.id}_${index}" value = "${item[valueField]}" tabindex = "0" >  `;
    checkItems += `<label class="custom-control-label" for= "checkbox_${ctx.component.id}_${index}" title = "${item[textField]} " > ${item[textField]} </label></div >`;
  });
  let resultStr = ` <div class="farris-input-wrap ide-checkgroup`;
  if(ctx.component.isHorizontal){
    resultStr +=' farris-checkradio-hor';
  }
  resultStr += `">${checkItems} </div>`;
  return resultStr;
};
