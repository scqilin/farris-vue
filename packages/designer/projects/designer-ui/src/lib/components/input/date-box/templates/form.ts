import { InputAppendUtils } from '../../../../utils/input-append.utils';


export default (ctx: any) => {

  // 启用扩展区域
  let inputAppendEle = '';
  if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
    inputAppendEle = InputAppendUtils.getInputGroupAppendElement(ctx);
  }

  return `
  <div class="farris-input-wrap ide-datebox">
    <div class="f-cmp-datepicker ide-input-group f-cmp-inputgroup">
      <div class="input-group f-state-disabled">
        <input placeholder="${ctx.component.placeHolder || '请选择日期'}" readonly class="form-control" name="farris-date" style="padding-right: 4px;" type="text">
        <div class="input-group-append f-cmp-iconbtn-wrapper position-relative">
            <button class="btn f-cmp-iconbtn" type="button">
              <span class="f-icon f-icon-date"></span>
            </button>
        </div>
        ${inputAppendEle}
      </div>
    </div>
  </div>`;
};



