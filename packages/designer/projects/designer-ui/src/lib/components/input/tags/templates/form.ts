

export default (ctx: any) => {
  let tagsData = ctx.component.dataSourceType === 'dynamic' ? '标记一,标记二' : ctx.component.tagData;
  let tagArrays = tagsData.split(',').filter(item => {
    if (item !== '') {
      return item;
    }
  });
  function hexToRgba(hex, opacity = 1) {
    const reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    var sColor = hex.toLowerCase();
    if (sColor && reg.test(sColor)) {
      if (sColor.length === 4) {
        var sColorNew = "#";
        for (var i = 1; i < 4; i += 1) {
          sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
        }
        sColor = sColorNew;
      }
      //处理六位的颜色值
      var sColorChange = [];
      for (var i = 1; i < 7; i += 2) {
        sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
      }
      return "RGB(" + sColorChange.join(",") + ',' + opacity + ")";
    } else {
      return sColor;
    }
  }

  let colorStyle = ``;
  if (ctx.component.color) {
    colorStyle = `color:${ctx.component.color};border-color:${ctx.component.color};background:${hexToRgba(ctx.component.color, 0.1)}`;
  }
  if (!tagArrays) tagArrays = [];

  let resultStr = '';
  resultStr = `<div class="farris-tags ide-tags`;
  resultStr += ctx.component.className ? ' ' + ctx.component.className : '';
  resultStr += `"> <ul class="farris-tags-item-container">`;
  let itemsStr = ``;
  tagArrays.forEach(tagName => {
    itemsStr += ` <li class="farris-tag-item`;
    itemsStr += ctx.component.tagType ? ' farris-tag-item-' + ctx.component.tagType : '';
    itemsStr += ctx.component.color ? ' farris-tag-item-has-color' : '';
    itemsStr += `"`;
    itemsStr += colorStyle ? ` style="${colorStyle}"` : '';
    itemsStr += `><span class="tag-box">${tagName}</span>
            <span class="tag-delete">
                <i class="f-icon f-icon-close"></i>
            </span>
        </li>`
  });
  resultStr += `${itemsStr}<li class="farris-tag-item farris-tag-add-button" >
  <span class="f-icon f-icon-amplification"></span>
  <span class="farris-tag-add-text">添加</span>
</li></ul></div>`;
  return resultStr;
};
