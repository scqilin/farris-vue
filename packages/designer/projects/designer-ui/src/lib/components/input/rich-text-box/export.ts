import FdRichTextBoxComponent from './component/fd-rich-text-box';
import { RichTextBoxSchema } from './schema/schema';

import FdRichTextBoxTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const RichTextBox: ComponentExportEntity = {
    type: 'RichTextBox',
    component: FdRichTextBoxComponent,
    template: FdRichTextBoxTemplates,
    metadata: cloneDeep(RichTextBoxSchema)
};
