import { ImageUploadSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { ImageUploadProp } from '../property/property-config';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';

export default class FdImageUploadComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
    // 加载css相关文件
    ControlCssLoaderUtils.loadCss('img-upload.css');
  }



  getDefaultSchema(): any {
    return ImageUploadSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('ImageUpload', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;
    const prop: ImageUploadProp = new ImageUploadProp(serviceHost, this.viewModelId, this.componentId);
    const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
    return propertyConfig;
  }


}
