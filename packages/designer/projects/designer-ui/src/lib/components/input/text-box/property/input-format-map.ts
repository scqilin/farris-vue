import { GSPElementDataType, GSPElementObjectType } from '@gsp-bef/gsp-cm-metadata';


/**
 * 输入格式校验
 *
 * 输入文本类型
 */
export const InputControlTypeMapping = {
    [GSPElementDataType.String]: [
        { key: 'none', value: '无', Ex: 'nononononono' },
        { key: 'cellNumber', value: '手机号' },
        { key: 'tel', value: '座机号/传真号' },
        { key: 'telOrCell', value: '手机号/座机号/传真号' },
        { key: 'postCode', value: '邮编' },
        // { key: 'workCode', value: '工号' },
        { key: 'email', value: '电子邮箱' },
        { key: 'idCode', value: '身份证号' },
        { key: 'carCode', value: '车牌号' },
        // { key: 'carCodeNew', value: '新能源车牌号' },
        { key: 'subjectCode', value: '10位数字会计科目代码' },
        { key: 'custom', value: '自定义' },
    ],
    [GSPElementDataType.Text]: [
        { key: 'none', value: '无' },
    ],
    [GSPElementDataType.Decimal]: [
        { key: 'none', value: '无' },
    ],
    [GSPElementDataType.Integer]: [
        { key: 'none', value: '无' },
    ],
    Number: [
        { key: 'none', value: '无' },
    ],
    BigNumber: [
        { key: 'none', value: '无' },
    ],
    [GSPElementDataType.Date]: [
        { key: 'none', value: '无' },
    ],
    [GSPElementDataType.DateTime]: [
        { key: 'none', value: '无' },
    ],
    [GSPElementDataType.Boolean]: [
        { key: 'none', value: '无' },
    ],
    [GSPElementObjectType.Enum]: [
        { key: 'none', value: '无' },
    ],
    Object: [
        { key: 'none', value: '无' },
    ]
};



/**
 * 根据所选输入类型获取校验正则
 */
export function getInputExByInputType(inputTypeKey: string): string {
    switch (inputTypeKey) {
        case 'none': { 
            return '';
        }
        case 'cellNumber': {
            return '^1[0-9]{10}$';
        }
        case 'tel': {
            return '^(0[0-9]{2,3}\\-)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?$';
        }
        case 'telOrCell': {
            return '^(0[0-9]{2,3}\\-)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?$|^1[0-9]{10}$';
        }
        case 'postCode': {
            return '^[1-9]\\d{5}(?!\\d)$';
        }
        case 'workCode': {
            return '^\\d{8}$';
        }
        case 'email': {
            return '^[A-Za-z\\d]+([-_.][A-Za-z\\d]+)*@([A-Za-z\\d]+[-.])+[A-Za-z]{2,5}$';
        }
        case 'idCode': {
            return '^[1-9]\\d{5}[1-9]\\d{3}((0[1-9])|(1[0-2]))(0[1-9]|([1|2][0-9])|3[0-1])((\\d{4})|\\d{3}X)$';
        }
        case 'carCode': {
            return '[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1}';
        }
        case 'carCodeNew': {
            return '[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF])|([DF][A-HJ-NP-Z0-9][0-9]{4}))';
        }
        case 'subjectCode': {
            return '^\\d{10}$';
        }
    }
    return '';
}
