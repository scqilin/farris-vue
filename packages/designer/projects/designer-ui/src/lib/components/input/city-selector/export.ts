import FdInputGroupComponent from './component/fd-city-selector';
import { CitySelectorSchemas } from './schema/schema';

import { ComponentExportEntity } from '@farris/designer-element';
import { cloneDeep } from 'lodash-es';
import FdTemplates from './templates';

export const CitySelector: ComponentExportEntity = {
    type: 'CitySelector',
    component: FdInputGroupComponent,
    template: FdTemplates,
    metadata: cloneDeep(CitySelectorSchemas)
};
