export const RichTextBoxSchema = {
    id: 'richTextBox',
    type: 'RichTextBox',
    titleSourceType: 'static',
    title: '富文本框',
    controlSource: 'concise',
    appearance: null,
    size: null,
    customEditorStyle: '',
    binding: null,
    readonly: false,
    require: false,
    disable: false,
    placeHolder: '',
    theme: 'snow',
    maxLength: null,
    minLength: null,
    customToolbarPosition: 'top',
    holdPlace: false,
    linkedLabelEnabled: false,
    linkedLabelClick: null,
    visible: true,
    tabindex: -1,
    hasDefaultFocus: false,
    focusState: null,
    titleWidth: null,
    isTextArea: true,
    fontFamilyList: [
        'Microsoft YaHei', 'Helvetica Neue', 'PingFang SC', 'sans-serif', 'simsun', 'serif',
        'SimHei', 'arial', 'helvetica', 'arial black', 'avant garde', 'book antiqua', 'palatino', 'FangSong'
    ],
    defaultFontFamily: 'Microsoft YaHei',
    customToolbar: false,
    toolbar: null,
    autoHeight: false,
    maxHeight: 500
};
