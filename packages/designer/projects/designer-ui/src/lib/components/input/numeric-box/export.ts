import FdNumericBoxComponent from './component/fd-numeric-box';
import { NumericBoxSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdNumericBoxTemplates from './templates';


export const NumericBox: ComponentExportEntity = {
    type: 'NumericBox',
    component: FdNumericBoxComponent,
    template: FdNumericBoxTemplates,
    metadata: NumericBoxSchema
};
