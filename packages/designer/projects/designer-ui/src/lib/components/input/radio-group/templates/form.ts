

export default (ctx: any) => {
  const textField = ctx.component.textField || 'name';
  const valueField = ctx.component.valueField || 'value';
  const radioDatas = ctx.component.enumData && ctx.component.enumData.length > 0 ? ctx.component.enumData : [{ value: '1', name: '选项1' }];
  let radioItems = '';
  radioDatas.forEach((item, index) => {
    radioItems += `<div class="custom-control custom-radio" > `;
    radioItems += ` <input class="custom-control-input" type = "radio" name = "${ctx.component.id}" disabled id = "radio${ctx.component.id}_${index}" value = "${item[valueField]}" tabindex = "0" >  `;
    radioItems += `<label class="custom-control-label" for= "radio${ctx.component.id}_${index}" title = "${item[textField]} " > ${item[textField]} </label></div >`;
  });
  let resultStr = ` <div class="farris-input-wrap ide-radiogroup`;
  if (ctx.component.isHorizontal) {
    resultStr += ' farris-checkradio-hor';
  }
  resultStr += `">${radioItems} </div>`;
  return resultStr;
};
