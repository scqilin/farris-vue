import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { GSPElementDataType } from '@gsp-bef/gsp-cm-metadata';
import { FormPropertyChangeObject } from '../../../../../lib/entity/property-change-entity';
import { NoCodeInputProps } from '../../common/property/nocode-input-property-config';
import { getInputExByInputType, InputControlTypeMapping } from './nocode-input-format-map';

export class NoCodeTextBoxProp extends NoCodeInputProps {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
        propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);


        // 格式校验属性
        const formatPropConfig = this.getformatValidation(propertyData, this.viewModelId);
        propertyConfig.push(formatPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, this.viewModelId);
        if (exprPropConfig) {
            propertyConfig.push(exprPropConfig);
        }

        return propertyConfig;
    }


    private getBehaviorPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {

        let behaviorProperties = [];
        if (showPosition === 'card' || showPosition === 'tableTdEditor') {
            behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
        }

        const self = this;
        const config = {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {
                self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);
            }
        };

        if (showPosition !== 'card') {
            Object.assign(config, {
                categoryId: showPosition + '_' + config.categoryId,
                propertyData,
                enableCascade: true,
                parentPropertyID: 'editor',
                tabId: showPosition,
                tabName: '编辑器'
            });
        }
        return config;
    }

    public getformatValidation(propertyData: any, viewModelId: string) {

        const self = this;

        return {
            categoryId: 'formatValidation',
            categoryName: '输入格式校验',
            enableCascade: true,
            propertyData: propertyData.formatValidation,
            parentPropertyID: 'formatValidation',
            properties: [
                {
                    propertyID: 'expression',
                    propertyName: '匹配正则',
                    propertyType: 'string',
                    defaultValue: '',
                    visible: false
                },
                {
                    propertyID: 'type',
                    propertyName: '输入类型',
                    propertyType: 'select',
                    iterator: InputControlTypeMapping.String,
                    defaultValue: 'none'
                },
                {
                    propertyID: 'message',
                    propertyName: '输入错误提示',
                    propertyType: 'string',
                    defaultValue: '',
                    visible: propertyData.formatValidation && propertyData.formatValidation.type !== 'none'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'type': {
                        propData.expression = getInputExByInputType(changeObject.propertyValue);
                        if (propData.expression) {
                            propData.message = `请输入正确的${self.getMessage(changeObject.propertyValue)}`;
                        }
                        if (changeObject.propertyValue === 'none') {
                            propData.message = '';
                        }

                        const messageProp = this.properties.find(prop => prop.propertyID === 'message');
                        messageProp.visible = propertyData.formatValidation && propertyData.formatValidation.type !== 'none';
                        break;
                    }
                }
            }
        };

    }

    private getMessage(propertyValue) {
        let msg = '';
        InputControlTypeMapping[GSPElementDataType.String].forEach((element) => {
            if (element.key === propertyValue) {
                msg = element.value;
            }
        });
        return msg;
    }
    getGridFieldEdtiorPropConfig(gridFieldData: any, viewModelId: string) {
        const propertyData = gridFieldData.editor;
        this.propertyConfig = [];

        // 编辑器类型属性
        const editorTypeConfig = this.getGridFieldEditorTypePropertyConfig(gridFieldData, viewModelId);
        if (editorTypeConfig) {
            this.propertyConfig.push(editorTypeConfig);
        }

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'gridFieldEditor');
        this.appendBehaviorPropsForGridFieldEditor(behaviorConfig, propertyData, viewModelId);
        this.propertyConfig.push(behaviorConfig);

        return this.propertyConfig;
    }
    /**
     * table单元格编辑器属性
     * @param tdData 单元格数据
     * @param viewModelId viewModelId
     * @returns 属性配置
     */
    getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
        const propertyData = tdData.editor;

        this.propertyConfig = [];

        // 外观属性
        const appearanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'tableTdEditor');
        this.propertyConfig.push(appearanceConfig);

        // 行为属性
        const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
        behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
        this.propertyConfig.push(behaviorConfig);

        // 扩展区域属性
        // const appendPropConfig = this.getInputAppendPropertyConfig(propertyData, viewModelId, 'tableTdEditor');
        // this.propertyConfig.push(appendPropConfig);

        // 表达式属性
        const exprPropConfig = this.getExpressionPropConfig(propertyData, viewModelId, 'tableTdEditor');
        if (exprPropConfig) {
            this.propertyConfig.push(exprPropConfig);
        }

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId, 'tableTdEditor');
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }
}
