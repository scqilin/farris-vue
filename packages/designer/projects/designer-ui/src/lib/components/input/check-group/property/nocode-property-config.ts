import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { EditorTypes } from '@farris/ui-datagrid-editors';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { ItemCollectionConverter, ItemCollectionEditorComponent } from '@farris/designer-devkit';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { NoCodeInputProps } from '../../common/property/nocode-input-property-config';

export class NoCodeCheckGroupProp extends NoCodeInputProps {

  propertyConfig: ElementPropertyConfig[];

  getPropConfig(propertyData: any): ElementPropertyConfig[] {
    this.propertyConfig = [];

    // 基本信息属性
    const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
    this.propertyConfig.push(basicPropConfig);

    // 外观属性
    const appearanceProperties = this.getAppearancePropConfig(propertyData, this.viewModelId);
    this.propertyConfig.push(appearanceProperties);

    // 行为属性
    const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
    this.propertyConfig.push(behaviorPropConfig);

    // 表达式属性
    const exprPropConfig = this.getExpressionPropConfig(propertyData, this.viewModelId);
    if (exprPropConfig) {
      this.propertyConfig.push(exprPropConfig);
    }

    return this.propertyConfig;

  }


  protected getAppearancePropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
    const self = this;
    let appearanceProperties = [];
    if (showPosition === 'card' || showPosition === 'tableTdEditor') {
      const appearanceCommonProps = this.getAppearanceCommonPropConfig(propertyData, viewModelId, showPosition);
      appearanceProperties = appearanceProperties.concat(appearanceCommonProps);
    }
    appearanceProperties.push(
      {
        propertyID: 'isHorizontal',
        propertyName: '水平布局',
        propertyType: 'boolean',
        description: '是否水平布局'
      },
      {
        propertyID: 'splitter',
        propertyName: '分隔符',
        propertyType: 'string',
        description: '分隔符设置'
      }
    );
    const config = {
      categoryId: 'appearance',
      categoryName: '外观',
      properties: appearanceProperties,
      setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

        if (!changeObject) {
          return;
        }
        self.changeAppearancePropertyRelates(this.properties, changeObject, propertyData, parameters);

      },

    };

    if (showPosition !== 'card') {
      Object.assign(config, {
        categoryId: showPosition + '_' + config.categoryId,
        propertyData,
        enableCascade: true,
        parentPropertyID: 'editor',
        tabId: showPosition,
        tabName: '编辑器'
      });
    }
    return config;
  }

  private getBehaviorPropConfig(propertyData: any, viewModelId: string, showPosition = 'card'): ElementPropertyConfig {
    const self = this;
    let behaviorProperties = [];
    if (showPosition === 'card' || showPosition === 'tableTdEditor') {
      behaviorProperties = this.getBehaviorCommonPropConfig(propertyData, viewModelId);
    }
    behaviorProperties.push(
      {
        propertyID: 'items',
        propertyName: '枚举数据',
        propertyType: 'modal',
        description: '枚举数据设置',
        editor: ItemCollectionEditorComponent,
        editorParams: {
          columns: [
            { field: 'value', title: '枚举值', editor: { type: EditorTypes.TEXTBOX } },
            { field: 'name', title: '枚举名称', editor: { type: EditorTypes.TEXTBOX } }
          ],
          requiredFields: ['value', 'name'],
          uniqueFields: ['value', 'name'],
          modalTitle: '选项编辑器',
          canEmpty: false,
          dynamicMappingKeys: true,
          valueField: propertyData.valueField,
          nameField: propertyData.textField
        },
        converter: new ItemCollectionConverter(),
        visible: propertyData.dataSourceType === 'static'
      }
    );
    const config = {
      categoryId: 'behavior',
      categoryName: '行为',
      properties: behaviorProperties,
      setPropertyRelates(changeObject, prop, parameters) {

        if (!changeObject) {
          return;
        }
        self.changeBehaviorPropertyRelates(this.properties, changeObject, propertyData, parameters, showPosition);
      }
    };

    if (showPosition !== 'card') {
      Object.assign(config, {
        categoryId: showPosition + '_' + config.categoryId,
        propertyData,
        enableCascade: true,
        parentPropertyID: 'editor',
        tabId: showPosition,
        tabName: '编辑器'
      });
    }
    return config;
  }
  /**
   * table单元格编辑器属性
   * @param tdData 单元格数据
   * @param viewModelId viewModelId
   * @returns 属性配置
   */
  getTableTdEdtiorPropConfig(tdData: any, viewModelId: string): ElementPropertyConfig[] {
    const propertyData = tdData.editor;

    this.propertyConfig = [];

    // 外观属性
    const appearanceConfig = this.getAppearancePropConfig(propertyData, viewModelId, 'tableTdEditor');
    this.propertyConfig.push(appearanceConfig);


    // 行为属性
    const behaviorConfig = this.getBehaviorPropConfig(propertyData, viewModelId, 'tableTdEditor');
    behaviorConfig.properties = behaviorConfig.properties.filter(p => !'binding,visible'.includes(p.propertyID));
    this.propertyConfig.push(behaviorConfig);

    // 表达式属性
    const exprPropConfig = this.getExpressionPropConfig(propertyData, viewModelId, 'tableTdEditor');
    if (exprPropConfig) {
      this.propertyConfig.push(exprPropConfig);
    }

    return this.propertyConfig;
  }

}
