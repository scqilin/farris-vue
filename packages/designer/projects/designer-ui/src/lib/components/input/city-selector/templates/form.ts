export default (ctx: any) => {
    return `
    <div class="farris-input-wrap">
        <div class="ide-input-group f-cmp-inputgroup">
            <div class="lookupbox input-group f-state-disabled">
                <div class="farris-input-wrap">
                    <div class="d-flex flex-row">
                        <input class="form-control" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;
};
