export const LanguageTextBoxSchema = {
    id: 'LanguageTextBox',
    type: 'LanguageTextBox',
    titleSourceType: 'static',
    title: '文本',
    appearance: null,
    languages: [],
    size: null,
    binding: null,
    readonly: false,
    require: false,
    disable: false,
    enableClear: true,
    placeHolder: '',
    maxLength: null,
    holdPlace: false,
    isTextArea: true,
    linkedLabelEnabled: false,
    linkedLabelClick: null,
    visible: true,
    tabindex: -1,
    hasDefaultFocus: false,
    focusState: null,
    titleWidth: null,
    useFrameworkLanguage: true,
    editable: false,
    enableAppend: false,
    inputAppendType: 'button',
    inputAppendDisabled: false,
    autoHeight: false,
    maxHeight: 500
};

