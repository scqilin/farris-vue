import FdRadioGroupComponent from './component/fd-radio-group';
import { RadioGroupSchema } from './schema/schema';

import FdRadioGroupTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const RadioGroup: ComponentExportEntity = {
    type: 'RadioGroup',
    component: FdRadioGroupComponent,
    template: FdRadioGroupTemplates,
    metadata: cloneDeep(RadioGroupSchema)
};
