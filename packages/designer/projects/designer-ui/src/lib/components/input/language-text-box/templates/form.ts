

export default (ctx: any) => {
    return `
    <div class="farris-input-wrap">
      <div class="ide-input-group f-cmp-inputgroup ide-language-text-box">
          <div class="farris-language-textbox input-group f-state-disabled">
              <input class="form-control text-left" readonly name="input-group-value" type="text">
              <div class="input-group-append">
                  <span class="input-group-text"><span class="f-icon f-icon-yxs_earth"></span></span>
              </div>
          </div>
      </div>
  </div>`;
};
