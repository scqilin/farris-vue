import FdTimePickerComponent from './component/fd-time-picker';
import { TimePickerSchema } from './schema/schema';

import FdTimePickerTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const TimePicker: ComponentExportEntity = {
    type: 'TimePicker',
    component: FdTimePickerComponent,
    template: FdTimePickerTemplates,
    metadata: cloneDeep(TimePickerSchema)
};
