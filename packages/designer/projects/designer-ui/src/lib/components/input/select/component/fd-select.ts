import { EnumFieldSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeEnumFieldProp } from '../property/nocode-property-config';
import { EnumFieldProp } from '../property/property-config';

export default class FdEnumFieldComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return EnumFieldSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('EnumField', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;
    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeEnumFieldProp = new NoCodeEnumFieldProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: EnumFieldProp = new EnumFieldProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }
}
