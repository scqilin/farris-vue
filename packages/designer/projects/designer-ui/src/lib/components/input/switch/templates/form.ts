

export default (ctx: any) => {
  const clsStr = ctx.component.square ? ' square' : '';
  return `
  <div class="farris-input-wrap">
    <div style="display:flex;height:100%;align-items:center;">
        <span class="switch f-cmp-switch switch-medium ${clsStr}">
          <small></small>
        </span>
    </div>
  </div>
  `;
};

