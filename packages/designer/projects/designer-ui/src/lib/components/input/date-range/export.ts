import { ComponentExportEntity } from '@farris/designer-element';
import FdDateRangeTemplates from './templates';
/**
 * 日期区间，目前只用于筛选条和筛选方案，不单独使用。
 */
export const DateRange: ComponentExportEntity = {
    type: 'DateRange',
    template: FdDateRangeTemplates,
    metadata: {}
};
