

export default (ctx: any) => {
    const beginPlaceHolder = ctx.component && ctx.component.beginPlaceHolder || '请输入开始日期';
    const endPlaceHolder = ctx.component && ctx.component.endPlaceHolder || '请输入结束日期';

    return `<div class="f-cmp-datepicker ide-number-range">
            <div class="input-group number-range f-cmp-datepicker f-state-disabled">
                <div class="form-control date-range-wrapper input-container">
                    <div class="sub-input-wrapper sub-input-wrapper-begin sub-input-group">
                        <input class="sub-input f-state-focus f-state-hover" type="text" style="text-align: left;" disabled placeholder="${beginPlaceHolder}">
                    </div>
                    <span class="f-icon f-icon-orientation-arrow sub-input-spliter"></span>
                    <div class="sub-input-wrapper sub-input-wrapper-end sub-input-group">
                    <input class="sub-input f-state-focus f-state-hover" type="text" style="text-align: left;" disabled  placeholder="${endPlaceHolder}">
                    </div>
                </div>
            </div>
        </div>`;
};
