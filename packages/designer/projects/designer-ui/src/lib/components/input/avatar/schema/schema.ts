export const AvatarSchema ={
    id: 'avatar',
    type: 'Avatar',
    title: '图像',
    size: null,
    disable: false,
    appearance: null,
    border: null,
    cover: '',
    maxSize: null,
    imgType: null,
    imgShape: 'circle',
    readonly: false,
    imgTitle: null,
    imgChange: null,
    visible: true,
    linkedLabelEnabled: false,
    linkedLabelClick: null
};

