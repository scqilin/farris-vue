export default (ctx: any) => {
    return `
<div class="farris-input-wrap">
    <div class="f-avatar f-avatar-readonly f-avatar-square">
        <div>
            <div class="el-upload el-upload--picture-card">
                <span>
                    <span class="f-icon f-icon-camera"></span>
                </span>
            </div>
        </div>
    </div>
</div>
`;
};



