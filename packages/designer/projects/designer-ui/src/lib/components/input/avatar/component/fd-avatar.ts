import { AvatarSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { AvatarProp } from '../property/property-config';

export default class FdAvatarComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return AvatarSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('Avatar', {
      component: this.component
    }), false);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;
    const prop: AvatarProp = new AvatarProp(serviceHost, this.viewModelId, this.componentId);
    const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
    return propertyConfig;
  }

}
