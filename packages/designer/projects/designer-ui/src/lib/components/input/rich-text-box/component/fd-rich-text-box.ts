import { RichTextBoxSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { RichTextBoxProp } from '../property/property-config';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeRichTextBoxProp } from '../property/nocode-property-config';


import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export default class FdRichTextBoxComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return RichTextBoxSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('RichTextBox', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;


    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeRichTextBoxProp = new NoCodeRichTextBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: RichTextBoxProp = new RichTextBoxProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }

  }

  onPropertyChanged(changeObject: FormPropertyChangeObject): void {
    super.onPropertyChanged(changeObject, ['controlSource'])
  }

}
