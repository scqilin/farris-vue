import { ImageSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { ImageProp } from '../property/property-config';

export default class FdImageComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return ImageSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('Image', {
      component: this.component
    }),false);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;
    const prop: ImageProp = new ImageProp(serviceHost, this.viewModelId, this.componentId);
    const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
    return propertyConfig;
  }


}
