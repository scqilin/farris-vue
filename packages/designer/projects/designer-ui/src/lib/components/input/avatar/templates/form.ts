import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';

export default (ctx: any) => {
  let resultStr = '';
  if (ctx.component.showInTable) {
    resultStr = ` <div class="farris-input-wrap ide-avatar">`;
  } else {
    resultStr = `<div class="farris-input-wrap f-utils-hcenter-vcenter  ide-avatar">`;
  }
  resultStr += ` <div class="f-avatar f-avatar-readonly f-avatar-circle">
    <img class="f-avatar-image" src="${ControlCssLoaderUtils.getAssetsUrl()}/images/input/avatar/avatar-input.png" >
  </div>
</div>`;
  return resultStr;
};
