import { TagsSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { TagsProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import { NoCodeTagsProp } from '../property/nocode-property-config';
import { DesignerEnvType } from '@farris/designer-services';

export default class FdTagsComponent extends FarrisDesignField {

    constructor(component: any, options: any) {
        super(component, options);

        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('tags.css');
    }



    getDefaultSchema(): any {
        return TagsSchema;
    }


    getStyles(): string {
        return 'display: inline-block;';
    }


    render(): any {

        return super.render(this.renderTemplate('Tags', {
            component: this.component
        }), !this.component.showInTable);
    }

    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;

        if (this.envType === DesignerEnvType.noCode) {
            const prop: NoCodeTagsProp = new NoCodeTagsProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        } else {
            const prop: TagsProp = new TagsProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        }
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
        super.onPropertyChanged(changeObject, ['className', 'color', 'tagType']);
    }

}
