import { ExpressionEditorComponent } from '@farris/designer-devkit';
import { ExpressionProp } from './expression-property-config';

export class NocodeExpressionProp extends ExpressionProp {


    protected getExpressionProperties(fieldId, viewModelId: string, expressionData: any, propertyData: any, showExpInTableTemplate: boolean) {
        return [
            {
                propertyID: 'compute',
                propertyName: '计算表达式',
                propertyType: 'modal',
                editor: ExpressionEditorComponent,
                editorParams: {fieldId, viewModelId, expType: 'compute', modalTitle: '计算表达式编辑器'},
                showClearButton: true,
                afterClickClearButton: () => {
                    this.clearExpressionData(fieldId, 'compute');
                }
            },
            {
                propertyID: 'dependency',
                propertyName: '依赖表达式',
                propertyType: 'modal',
                editor: ExpressionEditorComponent,
                editorParams: {fieldId, viewModelId, expType: 'dependency', modalTitle: '依赖表达式编辑器'},
                showClearButton: true,
                afterClickClearButton: () => {
                    this.clearExpressionData(fieldId, 'dependency');
                }
            },
            {
                propertyID: 'validate',
                propertyName: '校验表达式',
                propertyType: 'modal',
                editor: ExpressionEditorComponent,
                editorParams: {
                    fieldId,
                    viewModelId,
                    expType: 'validate',
                    message: expressionData.validateMessage,
                    modalTitle: '校验表达式编辑器'
                },
                showClearButton: true,
                afterClickClearButton: () => {
                    this.clearExpressionData(fieldId, 'validate');
                }
            }
        ];
    }
}
