import { SwitchFieldSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { SwitchFieldProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeSwitchFieldProp } from '../property/nocode-property-config';

export default class FdSwitchFieldComponent extends FarrisDesignField {

    component;
    constructor(component: any, options: any) {
        super(component, options);
    }
    init(): void {
        super.init();
        this.adaptOldControl();
    }

    getDefaultSchema(): any {
        return SwitchFieldSchema;
    }

    getStyles(): string {
        return 'display: inline-block;';
    }

    render(): any {

        return super.render(this.renderTemplate('SwitchField', {
            component: this.component
        }), !this.component.showInTable);
    }

    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;

        if (this.envType === DesignerEnvType.noCode) {
            const prop: NoCodeSwitchFieldProp = new NoCodeSwitchFieldProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        } else {
            const prop: SwitchFieldProp = new SwitchFieldProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        }
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject): void {
        super.onPropertyChanged(changeObject, ['square']);
    }

    private adaptOldControl() {
        // 移除冗余的属性
        if (this.component.hasOwnProperty('checked')) {
            delete this.component.checked;
        }
    }
}
