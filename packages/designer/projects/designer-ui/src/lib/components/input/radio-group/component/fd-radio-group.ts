import { RadioGroupSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { RadioGroupProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DesignerEnvType } from '@farris/designer-services';
import { NoCodeRadioGroupProp } from '../property/nocode-property-config';

export default class FdRadioGroupComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }

  getDefaultSchema(): any {
    return RadioGroupSchema;
  }

  getStyles(): string {
    return 'display: inline-block;';
  }

  render(): any {

    return super.render(this.renderTemplate('RadioGroup', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;

    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeRadioGroupProp = new NoCodeRadioGroupProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: RadioGroupProp = new RadioGroupProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }

  onPropertyChanged(changeObject: FormPropertyChangeObject): void {
    super.onPropertyChanged(changeObject, ['isHorizontal', 'enumData']);
  }

}
