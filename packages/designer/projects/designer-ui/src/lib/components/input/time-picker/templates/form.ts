import { InputAppendUtils } from '../../../../utils/input-append.utils';


export default (ctx: any) => {
  // 启用扩展区域
  let inputAppendEle = '';
  if (ctx.component.enableAppend !== false && ctx.component.inputAppendText) {
    inputAppendEle = InputAppendUtils.getInputGroupAppendElement(ctx);
  }

  return `<div class="farris-input-wrap ide-datebox">
  <div class="f-component-timepicker f-cmp-inputgroup ide-input-group">
    <div class="input-group f-state-disabled">
        <input placeholder="${ctx.component.placeHolder || '请选择时间'}" readonly class="form-control" name="farris-time-picker" type="text">
        <div class="input-group-append f-cmp-iconbtn-wrapper position-relative">
            <span class="input-group-text f-cmp-iconbtn" style="padding:0px 4px;padding-top: 2px;">
                <i class="f-icon f-icon-timepicker" style="font-size:0.85rem"></i>
            </span>
        </div>
        ${inputAppendEle}
    </div>
  </div></div>`;
};



