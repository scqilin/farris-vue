import { InputGroupSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { InputGroupProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DesignerEnvType, DomService } from '@farris/designer-services';
import { NoCodeInputGroupProp } from '../property/nocode-property-config';
import { BuilderHTMLElement } from '@farris/designer-element';

export default class FdInputGroupComponent extends FarrisDesignField {

    component;
    constructor(component: any, options: any) {
        super(component, options);
    }

    init(): void {
        super.init();
        this.adaptOldControl();
    }

    getDefaultSchema(): any {
        return InputGroupSchema;
    }


    getStyles(): string {
        return 'display: inline-block;';
    }


    render(): any {
        return super.render(this.renderTemplate('InputGroup', {
            component: this.component
        }), !this.component.showInTable);
    }

    attach(element: BuilderHTMLElement): any {
        const superAttach: any = super.attach(element);



        return superAttach;
    }
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;

        if (this.envType === DesignerEnvType.noCode) {
            const prop: NoCodeInputGroupProp = new NoCodeInputGroupProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        } else {
            const prop: InputGroupProp = new InputGroupProp(serviceHost, this.viewModelId, this.componentId);
            const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
            return propertyConfig;
        }

    }

    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
        super.onPropertyChanged(changeObject, ['groupText']);
    }

    /**
     * 智能输入框启用弹出表单模式后，需要记录配置的按钮展示名称和路径，用于交互面板已绑定事件窗口
     */
    setComponentBasicInfoMap() {
        super.setComponentBasicInfoMap();

        this.setComponentExtendInfoMap();

    }

    /**
     * 内部嵌套控件的路径信息
     */
    setComponentExtendInfoMap() {
        if (!this.component.modalConfig || !this.component.modalConfig.footerButtons || !this.component.modalConfig.footerButtons.length) {
            return;
        }
        const domService = this.options.designerHost.getService('DomService') as DomService;
        const inputGroupPath = domService.controlBasicInfoMap.get(this.component.id);

        this.component.modalConfig.footerButtons.forEach(toolbar => {
            domService.controlBasicInfoMap.set(toolbar.id, {
                showName: toolbar.text,
                parentPathName: `${inputGroupPath.parentPathName} > ${toolbar.text}`
            });
        });
    }

    private adaptOldControl() {
        // 移除冗余的属性
        if (this.component.hasOwnProperty('valueChange')) {
            delete this.component.valueChange;
        }
    }
}
