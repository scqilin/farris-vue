import { LanguageTextBoxSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig, PropertyChangeObject } from '@farris/ide-property-panel';
import { LanguageTextBoxProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export default class FdLanguageTextBoxComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return LanguageTextBoxSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('LanguageTextBox', {
      component: this.component
    }),!this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;
    const prop: LanguageTextBoxProp = new LanguageTextBoxProp(serviceHost, this.viewModelId, this.componentId);
    const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
    return propertyConfig;
  }


}
