import FdLanguageTextBoxComponent from './component/fd-language';
import { LanguageTextBoxSchema } from './schema/schema';

import FdLanguageTextBoxTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const LanguageTextBox: ComponentExportEntity = {
    type: 'LanguageTextBox',
    component: FdLanguageTextBoxComponent,
    template: FdLanguageTextBoxTemplates,
    metadata: cloneDeep(LanguageTextBoxSchema)
};
