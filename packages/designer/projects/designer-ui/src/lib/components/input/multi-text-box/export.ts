import FdMultiTextBoxComponent from './component/fd-multi-text-box';
import { MultiTextBoxSchema } from './schema/schema';

import FdMultiTextBoxTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const MultiTextBox: ComponentExportEntity = {
    type: 'MultiTextBox',
    component: FdMultiTextBoxComponent,
    template: FdMultiTextBoxTemplates,
    metadata: cloneDeep(MultiTextBoxSchema)
};
