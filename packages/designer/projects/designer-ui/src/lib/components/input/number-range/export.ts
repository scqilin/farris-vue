import FdNumberRangeComponent from './component/fd-number-range';
import { NumberRangeSchema } from './schema/schema';

import FdNumberRangeTemplates from './templates';
import { cloneDeep } from 'lodash-es';
import { ComponentExportEntity } from '@farris/designer-element';

export const NumberRange: ComponentExportEntity = {
    type: 'NumberRange',
    component: FdNumberRangeComponent,
    template: FdNumberRangeTemplates,
    metadata: cloneDeep(NumberRangeSchema)
};
