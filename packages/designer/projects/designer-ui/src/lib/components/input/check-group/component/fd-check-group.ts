import { CheckGroupSchema } from '../schema/schema';
import FarrisDesignField from '../../common/field/component/field';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { CheckGroupProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { NoCodeCheckGroupProp } from '../property/nocode-property-config';
import { DesignerEnvType } from '@farris/designer-services';

export default class FdCheckGroupComponent extends FarrisDesignField {

  component;
  constructor(component: any, options: any) {
    super(component, options);
  }



  getDefaultSchema(): any {
    return CheckGroupSchema;
  }


  getStyles(): string {
    return 'display: inline-block;';
  }


  render(): any {

    return super.render(this.renderTemplate('CheckGroup', {
      component: this.component
    }), !this.component.showInTable);
  }

  getPropertyConfig(): ElementPropertyConfig[] {
    const serviceHost = this.options.designerHost;

    if (this.envType === DesignerEnvType.noCode) {
      const prop: NoCodeCheckGroupProp = new NoCodeCheckGroupProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    } else {
      const prop: CheckGroupProp = new CheckGroupProp(serviceHost, this.viewModelId, this.componentId);
      const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
      return propertyConfig;
    }
  }

  onPropertyChanged(changeObject: FormPropertyChangeObject): void {
    super.onPropertyChanged(changeObject, ['isHorizontal', 'items']);

  }

}
