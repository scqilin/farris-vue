import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { TemplateField } from '../template-binding-editor.component';
import { FormSchemaEntityField } from '@farris/designer-services';
import { IdService } from '@farris/ui-common';

export interface VariableGroup {
  id: string;
  name: string;
  items: TemplateField[];
  canAdd: boolean;
  presetTypes: { id: string, name: string }[];
}

@Component({
  selector: 'variable-list',
  templateUrl: './variable-list.component.html',
  styleUrls: ['./variable-list.component.css']
})
export class VariableListComponent implements OnInit, OnChanges {

  @Input() variables: TemplateField[];

  @Input() schemaFields: FormSchemaEntityField[];

  @Output() variableChanged = new EventEmitter<TemplateField>();

  @Output() variableAdded = new EventEmitter<string>();

  @Output() variableRemoved = new EventEmitter<TemplateField>();

  @Output() moveUpVariable = new EventEmitter<TemplateField>();

  @Output() moveDownVariable = new EventEmitter<TemplateField>();

  variableGroups: VariableGroup[] = [];

  selectItem: any;

  constructor(private idService: IdService) { }

  ngOnInit() {
    this.updateVariableGroup(this.variables);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.variables) {
      this.updateVariableGroup(this.variables);
    }
  }

  private updateVariableGroup(variables: TemplateField[]) {
    const groups: VariableGroup[] = [];
    if (variables && variables.length) {
      variables.reduce<VariableGroup[]>((currentGroups, variableField) => {
        const variableCategory = variableField.category;
        let specialCategoryGroup = currentGroups.find(group => group.id === variableCategory);
        if (!specialCategoryGroup) {
          specialCategoryGroup = {
            id: variableCategory,
            name: variableCategory,
            items: [],
            canAdd: true,
            presetTypes: []
          };
          currentGroups.push(specialCategoryGroup);
        }
        variableField.guid = this.idService.guid();
        specialCategoryGroup.items.push(variableField);
        if (specialCategoryGroup.presetTypes.findIndex(preset => preset.id === variableField.presetType) < 0) {
          specialCategoryGroup.presetTypes.push({
            id: variableField.presetType,
            name: variableField.presetType
          });
        }
        return currentGroups;
      }, groups);
    }
    this.variableGroups = groups;
  }

  trackById(index: number, group: VariableGroup): string { return group.id; }

  trackByItemId(index: number, item: TemplateField): string { return item.guid; }

  onVariableItemChanged(variableItem: TemplateField) {
    this.variableChanged.emit(variableItem);
  }

  addItem(groupId: string) {
    this.variableAdded.emit(groupId);
  }

  removeItem(variableItem: TemplateField) {
    this.variableRemoved.emit(variableItem);
  }

  moveUp(variableItem: TemplateField) {
    const variableGroup = this.variableGroups.find(group => group.id === variableItem.category);
    if (variableGroup && variableGroup.items.findIndex(item => item.id === variableItem.id) > 0) {
      this.moveUpVariable.emit(variableItem);
    }
  }

  moveDown(variableItem: TemplateField) {
    const variableGroup = this.variableGroups.find(group => group.id === variableItem.category);
    if (variableGroup && variableGroup.items.findIndex(item => item.id === variableItem.id) < variableGroup.items.length - 1) {
      this.moveDownVariable.emit(variableItem);
    }
  }

  selectItemChanged(templateFieldItem: TemplateField) {
    this.selectItem = templateFieldItem;
  }
}
