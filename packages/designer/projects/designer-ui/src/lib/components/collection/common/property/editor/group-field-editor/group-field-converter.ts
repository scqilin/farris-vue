import { TypeConverter } from '@farris/ide-property-panel';

export class GroupFieldConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (data && Array.isArray(data)) {
            return '共 ' + data.length + ' 列';
        } else {
            return data;
        }

    }
}
