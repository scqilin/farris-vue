import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';


export default (ctx: any) => {
  return `
<div class="ide-list-view h-100">
  <div class="f-list-view">
    <div class="f-list-view-content">
      <ul class="f-list-view-group">
        <li class="ide-list-view-group-item">
          <div class="ide-list-content">
            <div class="f-template-common-row d-flex">
              <div class="item-action-primary"><img class="ap-img" src="${ControlCssLoaderUtils.getAssetsUrl()}/images/collection/list-view/avatar-1.png"></div>
              <div class="listview-item-content d-flex k-align-items-center">
                <div class="listview-item-main">
                  <h4 class="listview-item-title">华东地区项目的物资采购</h4>
                  <h5 class="listview-item-subtitle">关于华东地区项目的物资采购订购的一些说明描述</h5>
                </div>
                <div class="listview-item-btns"><button class="btn btn-link">编辑</button><button
                    class="btn btn-link">删除</button></div>
              </div>
            </div>
          </div>
        </li>

        <li class="ide-list-view-group-item">
          <div class="ide-list-content">
            <div class="f-template-common-row d-flex">
              <div class="item-action-primary"><img class="ap-img" src="${ControlCssLoaderUtils.getAssetsUrl()}/images/collection/list-view/avatar-2.png"></div>
              <div class="listview-item-content d-flex k-align-items-center">
                <div class="listview-item-main">
                  <h4 class="listview-item-title">集团本部食材采购部</h4>
                  <h5 class="listview-item-subtitle">关于集团本部食材采购部的一些说明描述</h5>
                </div>
                <div class="listview-item-btns"><button class="btn btn-link">编辑</button><button
                    class="btn btn-link">删除</button></div>
              </div>
            </div>
          </div>
        </li>

        <li class="ide-list-view-group-item">
          <div class="ide-list-content">
            <div class="f-template-common-row d-flex">
              <div class="item-action-primary"><img class="ap-img" src="${ControlCssLoaderUtils.getAssetsUrl()}/images/collection/list-view/avatar-3.png"></div>
              <div class="listview-item-content d-flex k-align-items-center">
                <div class="listview-item-main">
                  <h4 class="listview-item-title">集团运输服务票据</h4>
                  <h5 class="listview-item-subtitle">关于集团运输服务票据的一些说明描述</h5>
                </div>
                <div class="listview-item-btns"><button class="btn btn-link">编辑</button><button
                    class="btn btn-link">删除</button></div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
  
  `;
};

