import { contextMenu } from './context-menu-config';
import { cloneDeep } from 'lodash-es';
import { RowNode } from '@farris/ui-treetable';
import { ContextMenuManager } from '../../../../service/context-menu.manager';
import { ControlContextMenuItem } from '../../../../entity/control-context-menu';
import { DataGridMultiViewContextMenuService } from './services/multi-view.service';
import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { DataGridFilterContextMenuService } from './services/list-filter.service';
import { DataGridQuerySchemeContextMenuService } from './services/query-scheme.service';
import { DataGridEditByCardContextMenuService } from './services/card-edit.service';

export class DataGridContextMenuManager extends ContextMenuManager {

    filterContextMenuSerivice: DataGridFilterContextMenuService;
    multiViewContextMenuService: DataGridMultiViewContextMenuService;
    querySchemeContextMenuService: DataGridQuerySchemeContextMenuService;
    cardEditService: DataGridEditByCardContextMenuService;

    private controlCreatorService: any;
    constructor(cmp: FarrisDesignBaseComponent, rowNode: RowNode) {
        super(cmp, rowNode);

        this.controlCreatorService = this.serviceHost.getService('ControlCreatorService');

        this.filterContextMenuSerivice = new DataGridFilterContextMenuService(this.injector);
        this.multiViewContextMenuService = new DataGridMultiViewContextMenuService(this.injector);
        this.querySchemeContextMenuService = new DataGridQuerySchemeContextMenuService(this.injector);
        this.cardEditService = new DataGridEditByCardContextMenuService(this.injector, this.controlCreatorService);


    }
    /**
     * 过滤、修改控件树右键菜单
     */
    setContextMenuConfig() {
        let menuConfig = cloneDeep(contextMenu) as ControlContextMenuItem[];

        menuConfig = this.filterContextMenuSerivice.assembleListFilterMenu(this.cmpInstance.viewModelId, menuConfig);
        menuConfig = this.querySchemeContextMenuService.assembleQuerySchemeMenu(this.cmpInstance.componentId, menuConfig);
        menuConfig = this.multiViewContextMenuService.assembleMultiViewMenu(menuConfig, this.cmpInstance.id, this.cmpInstance.componentId);;
        menuConfig = this.cardEditService.assembleCardEditMenu(menuConfig, this.cmpInstance);

        // 配置菜单点击事件
        this.addContextMenuHandle(menuConfig);

        return menuConfig || [];
    }

    /**
     * 点击控件树右键菜单
     */
    contextMenuClicked(e: { data: RowNode, menu: ControlContextMenuItem }) {

        const menu = e.menu;
        const viewModelId = this.cmpInstance.viewModelId;
        switch (menu.id) {
            // 启用筛选条
            case 'enableListFilter': {
                this.filterContextMenuSerivice.openListFilterModal(viewModelId, this.cmpInstance.id);
                break;
            }
            // 移除筛选条
            case 'removeListFilter': {
                this.filterContextMenuSerivice.removeListFilter(viewModelId, this.cmpInstance);
                break;
            }
            // 启用筛选方案
            case 'enableQueryScheme': {
                this.querySchemeContextMenuService.openQuerySchemeModal(viewModelId);
                break;
            }
            // 移除筛选方案
            case 'removeQueryScheme': {
                this.querySchemeContextMenuService.removeQueryScheme(this.cmpInstance);
                break;
            }
            // 启用多视图
            case 'enableMultiViewContainer': {
                this.multiViewContextMenuService.enableMultiViewContainer(viewModelId, this.cmpInstance.component);
                break;
            }
            // 移除多视图
            case 'removeMultiViewContainer': {
                this.multiViewContextMenuService.removeMultiViewContainer(this.cmpInstance);
                break;
            }
            case 'EnableEditInModalCard': case 'EnableEditInSidebarCard': {
                this.cardEditService.createCardEdit(menu, this.cmpInstance);
                break;
            }
            case 'DisableEditInModalCard': case 'DisableEditInSidebarCard': {
                this.cardEditService.disableCardEdit(menu, this.cmpInstance);
                break;
            }
            default: {
                this.notifyService.warning('暂不支持');
            }
        }

    }
}

