/**
 * 获取日视图模板
 */
export default (ctx: any) => {
    return `
<day-view class="h-100 d-flex flex-column">
    <div class="header f-utils-overflow-hidden" ref=${ctx.headerKey} style="height:40px">
        <div class="header-row" style="display: flex;width: 2700px;">
            <div class="header-cell width-200px roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;"><span class="time ml-2">${ctx.component.placeTitle || '地点名称'}</span>
            </div>
            <div class="header-cell width-200px time-cell width-100px" style="width: 50px"></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">00:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">01:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">02:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">03:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">04:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">05:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">06:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">07:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">08:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">09:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">10:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">11:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">12:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">13:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">14:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">15:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">16:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">17:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">18:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">19:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">20:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">21:00</span></div>
            <div class="header-cell width-200px time-cell width-100px"><span class="time">22:00</span></div>
            <div class="header-cell width-200px time-cell width-100px" style="width: 50px"><span class="time">23:00</span></div>
        </div>
    </div>
    <div class="body nobtn" ref=${ctx.bodyKey} style="flex-grow: 1;overflow-x: scroll;">

        <div class="room-row" style="height: ${ctx.component.rowHeight}px;width: 2700px">
            <div class="room-cell d-flex roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">

                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点1 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 20人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
            <div class="room-cell d-flex time-cell width-100px">

                <div class="item-content" style="width: 250px;left: 949.667px;">
                    <div class="item-content-wrap blue">

                        <div class="room-subject p-2">
                            <dl>
                                <dt>
                                    <span class="room-subject_category">中层会议</span>
                                    关于开展“我与企业文化”主题征文和演讲比赛的会议
                                </dt>
                                <dd
                                    style="margin: 0;line-height: 18px;color: #878d99;text-overflow: ellipsis;overflow: hidden;padding-top: 3px;">
                                    <span style="margin-right: 10px">综合办公室
                                    </span><span>09:30~12:00</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="item-content" style="width: 341.667px; left: 1308.33px;">
                    <div class="item-content-wrap red">
                        <div class="room-subject p-2">
                            <dl>
                                <dt>
                                    <span class="room-subject_category">专项会议</span>
                                    关于员工健康体检工作的准备会议
                                </dt>
                                <dd
                                    style="margin: 0;line-height: 18px;color: #878d99;text-overflow: ellipsis;overflow: hidden;padding-top: 3px;">
                                    <span style="margin-right: 10px">综合办公室
                                    </span><span>13:05~16:25</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
        </div>
        <div class="room-row" style="height: ${ctx.component.rowHeight}px;">
            <div class="room-cell d-flex roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">
                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点2 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 200人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
            <div class="room-cell d-flex time-cell width-100px">

                <div class="item-content" style="width: 250px; left: 1775px;">
                    <div class="item-content-wrap red">

                        <div class="room-subject p-2" ng-reflect-klass="room-subject p-2"
                            ng-reflect-ng-class="[object Object]">
                            <dl>
                                <dt>
                                    <span class="room-subject_category ">专项会议</span>
                                    关于迎新春活动的准备会议
                                </dt>
                                <dd
                                    style="margin: 0;line-height: 18px;color: #878d99;text-overflow: ellipsis;overflow: hidden;padding-top: 3px;">
                                    <span style="margin-right: 10px">
                                    </span><span>17:45~20:15</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
        </div>
        <div class="room-row" style="height: ${ctx.component.rowHeight}px;">
            <div class="room-cell d-flex roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">
                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点3 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 40人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
            <div class="room-cell d-flex time-cell width-100px">
                <div class="item-content" style="width: 346px; left: 800px;">
                    <div class="item-content-wrap mintgreen">
                        <div class="room-subject p-2">
                            <dl>
                                <dt>
                                    <span class="room-subject_category">培训会议</span>
                                    【因分享 更热爱】Spotlight第二十四期活动
                                </dt>
                                <dd
                                    style="margin: 0;line-height: 18px;color: #878d99;text-overflow: ellipsis;overflow: hidden;padding-top: 3px;">
                                    <span style="margin-right: 10px">技术创新部
                                    </span><span>08:00~11:25</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
        </div>

        <div class="room-row" style="height: ${ctx.component.rowHeight}px;">
            <div class="room-cell d-flex roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">
                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点4 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 50人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
        </div>

        <div class="room-row" style="height: ${ctx.component.rowHeight}px;">
            <div class="room-cell d-flex roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">

                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点5 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 10人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
            <div class="room-cell d-flex time-cell width-100px">
                <div class="item-content" style="width: 200px;left: 999.33px;">
                    <div class="item-content-wrap gray">
                        <div class="room-subject p-2">
                            <dl>
                                <dt>
                                    <span class="room-subject_category">普通会议</span>
                                    关于宣传全国“消防日”活动的准备会议
                                </dt>
                                <dd
                                    style="margin: 0;line-height: 18px;color: #878d99;text-overflow: ellipsis;overflow: hidden;padding-top: 3px;">
                                    <span style="margin-right: 10px">综合办公室
                                    </span><span>10:00~12:00</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
        </div>
        <div class="room-row" style="height: ${ctx.component.rowHeight}px;">
            <div class="room-cell d-flex roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">


                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点6 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 100人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
            <div class="room-cell d-flex time-cell width-100px">

            </div>

            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px"></div>
            <div class="room-cell d-flex time-cell width-100px" style="width: 50px"></div>
        </div>
    </div>
</day-view>
    `;
};


