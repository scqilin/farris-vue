import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateButtonEditorComponent } from './template-button-editor.component';

describe('TemplateButtonEditorComponent', () => {
  let component: TemplateButtonEditorComponent;
  let fixture: ComponentFixture<TemplateButtonEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateButtonEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateButtonEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
