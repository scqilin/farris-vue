import FdTableComponent from './component/fd-table';
import { TableSchema, TableTdSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdTableTemplates from './templates';


export const Table: ComponentExportEntity = {
    type: 'Table',
    component: FdTableComponent,
    template: FdTableTemplates,
    metadata: TableSchema
};


export const TableTd: ComponentExportEntity = {
    type: 'TableTd',
    metadata: TableTdSchema
};
