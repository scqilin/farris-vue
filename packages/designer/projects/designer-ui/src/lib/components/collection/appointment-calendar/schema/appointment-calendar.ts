export const AppointmentCalendarSchema = {
    id: 'appointmentCalendar',
    type: 'AppointmentCalendar',
    appearance: {
        class: 'f-utils-fill-flex-row'
    },
    dataSource: '',
    placeDataSource: null,
    placeDataSourceUrl: '',
    placeDataSourceUrlType: 'GET',
    placeTitle: '地点名称',
    placeNameField: '',
    placeIdField: 'id',
    placeNameTemplate: '',
    contentTemplate: '',
    viewType: 'day',
    reservationTitle: '我要预定',
    rowHeight: 62,
    placeNameWidth: 200,
    weekRowHeight: 85,
    weekCellWidth: 180,
    reserveInfoFields: {
        startDate: null,
        endDate: null,
        idField: 'id',
        placeid: null,
        title: null
    },
    dayViewCls: null,
    detailCls: null,
    filterChange: null,
    selectChange: null,
    editHandler: null,
    removeHandler: null,
    reservation: null,
    visible: true
};
