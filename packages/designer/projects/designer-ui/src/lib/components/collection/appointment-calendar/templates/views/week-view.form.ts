/**
 * 获取周视图模板
 */
export default (ctx: any) => {
    return `
<div class="h-100 d-flex flex-column">
    <div class="header f-utils-overflow-hidden" style="height:40px" ref=${ctx.weekHeaderKey}>
        <div class="header-row">
            <div class="header-cell width-200px roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;"><span class="time ml-2">${ctx.component.placeTitle || '地点名称'}</span>
            </div>
            <div style="flex: 7;display: flex;">

                <div class="header-cell width-200px time-cell" title="2023-02-26 周日" style="width: ${ctx.component.weekCellWidth}px;"><span
                        class="rtv-week-day">26</span><span>周日</span></div>
                <div class="header-cell width-200px time-cell" title="2023-02-27 周一" style="width: ${ctx.component.weekCellWidth}px;"><span
                        class="rtv-week-day">27</span><span>周一</span></div>
                <div class="header-cell width-200px time-cell" title="2023-02-28 周二" style="width: ${ctx.component.weekCellWidth}px;"><span
                        class="rtv-week-day">28</span><span>周二</span></div>
                <div class="header-cell width-200px time-cell" title="2023-03-01 周三" style="width: ${ctx.component.weekCellWidth}px;"><span
                        class="rtv-week-day">1</span><span>周三</span></div>
                <div class="header-cell width-200px time-cell" title="2023-03-02 周四" style="width: ${ctx.component.weekCellWidth}px;"><span
                        class="rtv-week-day">2</span><span>周四</span></div>
                <div class="header-cell width-200px time-cell" title="2023-03-03 周五" style="width: ${ctx.component.weekCellWidth}px;"><span
                        class="rtv-week-day">3</span><span>周五</span></div>
                <div class="header-cell width-200px time-cell" title="2023-03-04 周六" style="width: ${ctx.component.weekCellWidth}px;"><span
                        class="rtv-week-day">4</span><span>周六</span></div>
            </div>
        </div>
    </div>
    <div class="body nobtn" style="flex-grow: 1;overflow-x: scroll;"  ref=${ctx.weekBodyKey}>

        <div class="room-row" style="height: ${ctx.component.weekRowHeight}px;">
            <div class="room-cell roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">


                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点1 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 20人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div style="flex: 7;display: flex;">

                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                            <li>
                                <div class="rtv-week-day-item" style="display: flex;flex-direction: row;">
                                    <div style="flex-grow: 1;overflow: hidden;text-overflow: ellipsis;">
                                        关于开展“我与企业文化”主题征文和演讲比赛的会议 </div>
                                    <div style="flex: none;text-align: right; padding-left: 3px;"> 09:30 </div>
                                </div>
                            </li>
                            <li>
                                <div class="rtv-week-day-item" style="display: flex;flex-direction: row;">
                                    <div style="flex-grow: 1;overflow: hidden;text-overflow: ellipsis;">
                                        关于员工健康体检工作的准备会议 </div>
                                    <div style="flex: none;text-align: right; padding-left: 3px;"> 13:05 </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="room-row" style="height: ${ctx.component.weekRowHeight}px;">
            <div class="room-cell roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">
                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点2 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 200人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div style="flex: 7;display: flex;">

                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                            <li>
                                <div class="rtv-week-day-item" style="display: flex;flex-direction: row;">
                                    <div style="flex-grow: 1;overflow: hidden;text-overflow: ellipsis;">
                                        关于迎新春活动的准备会议 </div>
                                    <div style="flex: none;text-align: right; padding-left: 3px;"> 17:45 </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="room-row" style="height: ${ctx.component.weekRowHeight}px;">
            <div class="room-cell roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">


                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点3 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 40人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div style="flex: 7;display: flex;">

                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                            <li>
                                <div class="rtv-week-day-item" style="display: flex;flex-direction: row;">
                                    <div style="flex-grow: 1;overflow: hidden;text-overflow: ellipsis;">
                                        【因分享 更热爱】Spotlight第二十四期活动 </div>
                                    <div style="flex: none;text-align: right; padding-left: 3px;"> 08:00 </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="room-row" style="height: ${ctx.component.weekRowHeight}px;">
            <div class="room-cell roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">


                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点4 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 50人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div style="flex: 7;display: flex;">

                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        <li>
                        <div class="rtv-week-day-item" style="display: flex;flex-direction: row;">
                            <div style="flex-grow: 1;overflow: hidden;text-overflow: ellipsis;">
                                关于安全隐患大排查工作的准备会议 </div>
                            <div style="flex: none;text-align: right; padding-left: 3px;"> 10:45 </div>
                        </div>
                    </li>
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="room-row" style="height: ${ctx.component.weekRowHeight}px;">
            <div class="room-cell roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">


                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点5 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 10人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div style="flex: 7;display: flex;">

                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                            <li>
                                <div class="rtv-week-day-item" style="display: flex;flex-direction: row;">
                                    <div style="flex-grow: 1;overflow: hidden;text-overflow: ellipsis;">
                                        关于宣传全国“消防日”活动的准备会议 </div>
                                    <div style="flex: none;text-align: right; padding-left: 3px;"> 10:00 </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="room-row" style="height: ${ctx.component.weekRowHeight}px;">
            <div class="room-cell roomname fixed-left" style="width: ${ctx.component.placeNameWidth}px;">


                <div class="pl-2" style="overflow: hidden;">
                    <dl>
                        <dt style="font-size: 13px;line-height: 20px;">地点6 </dt>
                        <dd
                            style="margin: 0;line-height: 18px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
                            <span class="f-icon f-icon-team" style="font-size:13px"></span> 100人 <span
                                class="f-icon f-icon-info-circle" style="font-size:13px; margin-left: 10px;"></span>
                            电视、投影仪、白板、音响、麦克风
                        </dd>
                    </dl>
                </div>
            </div>
            <div style="flex: 7;display: flex;">

                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">
                            <li>
                                <div class="rtv-week-day-item" style="display: flex;flex-direction: row;">
                                    <div style="flex-grow: 1;overflow: hidden;text-overflow: ellipsis;">
                                        关于开展安全生产专项教育全员学习活动的通知会议 </div>
                                    <div style="flex: none;text-align: right; padding-left: 3px;"> 10:00 </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="room-cell time-cell" style="align-items: start; width: ${ctx.component.weekCellWidth}px;">
                    <div class="p-1" style="width: 100%;overflow: hidden;">
                        <ul style="list-style: none;word-break: keep-all;">

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
    `;
};
