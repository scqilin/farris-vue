import { AppointmentCalendarSchema } from '../schema/appointment-calendar';
import FdCollectionBaseComponent from '../../common/collection-base/fd-collection-base';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { AppointmentCalendarProp } from '../property/property-config';
import { BuilderHTMLElement } from '@farris/designer-element';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { DesignerEnvType } from '@farris/designer-services';

export default class FdAppointmentCalendarComponent extends FdCollectionBaseComponent {

    get bodyKey() {
        return `calendar-body-${this.key}`;
    }
    get headerKey() {
        return `calendar-header-${this.key}`;
    }
    get weekBodyKey() {
        return `calendar-week-body-${this.key}`;
    }
    get weekHeaderKey() {
        return `calendar-week-header-${this.key}`;
    }

    get viewTypeKey() {
        return `calendar-view-type-${this.key}`;
    }
    constructor(component: any, options: any) {
        super(component, options);

        // 加载css相关文件
        // ControlCssLoaderUtils.loadCss('appointment-calendar.css');
    }

    getDefaultSchema(): any {
        return AppointmentCalendarSchema;
    }

    getTemplateName(): string {
        return 'AppointmentCalendar';
    }

    hideNestedPaddingInDesginerView(): boolean {
        return true;
    }
    render(): any {
        if (!this.component.currentViewInDesignerView) {
            this.component.currentViewInDesignerView = this.component.viewType || 'day';
        }

        return super.render(this.renderTemplate('AppointmentCalendar', {
            component: this.component,
            bodyKey: this.bodyKey,
            headerKey: this.headerKey,
            weekBodyKey: this.weekBodyKey,
            weekHeaderKey: this.weekHeaderKey,
            viewTypeKey: this.viewTypeKey
        }));
    }
    attach(element: HTMLElement): Promise<any> {
        const superAttach = super.attach(element);

        this.loadRefs(element, {
            [this.bodyKey]: 'single',
            [this.headerKey]: 'single',
            [this.weekBodyKey]: 'single',
            [this.weekHeaderKey]: 'single',
            [this.viewTypeKey]: 'multiple'
        });

        // 日视图，横向滚动条定位到8:00
        if (this.refs[this.bodyKey]) {
            this.refs[this.bodyKey].scrollLeft = 800;
        }
        if (this.refs[this.headerKey]) {
            this.refs[this.headerKey].scrollLeft = 800;
        }

        // 日视图，横向滚动条带动头部区域的横向滚动条
        this.addEventListener(this.refs[this.bodyKey], 'scroll', (event) => {
            event.preventDefault();
            event.stopPropagation();

            if (this.refs[this.headerKey]) {
                this.refs[this.headerKey].scrollLeft = this.refs[this.bodyKey].scrollLeft;
            }

        });

        // 周视图，横向滚动条带动头部区域的横向滚动条
        this.addEventListener(this.refs[this.weekBodyKey], 'scroll', (event) => {
            event.preventDefault();
            event.stopPropagation();

            if (this.refs[this.weekHeaderKey]) {
                this.refs[this.weekHeaderKey].scrollLeft = this.refs[this.weekBodyKey].scrollLeft;
            }

        });

        // 切换日视图、周视图
        if (this.refs[this.viewTypeKey]) {
            this.refs[this.viewTypeKey].forEach(buttonEle => {
                this.addEventListener(buttonEle, 'click', (event) => {
                    event.preventDefault();
                    event.stopPropagation();

                    const targetType = buttonEle.getAttribute('viewType');
                    if (targetType && targetType !== this.component.currentViewInDesignerView) {
                        this.component.currentViewInDesignerView = targetType;
                        this.triggerRedraw();
                    }

                });
            });
        }
        return superAttach;
    }
    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;

        // if (this.envType === DesignerEnvType.noCode) {
        //     const prop: NoCodeAppointmentCalendarProp = new NoCodeAppointmentCalendarProp(serviceHost, this.viewModelId, this.componentId);
        //     const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        //     return propertyConfig;
        // } else {
        const prop: AppointmentCalendarProp = new AppointmentCalendarProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
        // }
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
        const propIds = ['placeNameWidth', 'rowHeight', 'weekRowHeight', 'weekCellWidth', 'placeTitle'];

        super.onPropertyChanged(changeObject, propIds);
    }
    canAccepts(sourceElement: BuilderHTMLElement, targetElement?: BuilderHTMLElement): boolean {
        return false;
    }
}
