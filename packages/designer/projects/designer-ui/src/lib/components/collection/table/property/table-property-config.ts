import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { CollectionProp } from '../../common/property/collection-property-config';
import { DgControl } from '../../../../utils/dg-control';
import { TableTdManager } from '../component/td-manager/td-manager';
import { FarrisColorpickerPlusComponent } from '@farris/colorpicker-plus';

export class TableProp extends CollectionProp {

    getPropConfig(propertyData: any, tdManager: TableTdManager): ElementPropertyConfig[] {


        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData);
        propertyConfig.push(basicPropConfig);


        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig(propertyData, tdManager);
        propertyConfig.push(appearancePropConfig);

        // 边框属性
        const borderPropConfig = this.getBorderPropConfig(propertyData, tdManager);
        propertyConfig.push(borderPropConfig);

        // 字体属性
        const fontPropConfig = this.getFontPropConfigg(propertyData, tdManager);
        propertyConfig.push(fontPropConfig);

        return propertyConfig;

    }

    private getBasicPropConfig(propertyData: any): ElementPropertyConfig {
        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '组件的id',
                    readonly: true
                },
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件类型',
                    iterator: [{ key: propertyData.type, value: DgControl.Table.name }],
                    readonly: true
                }
            ]
        };
    }

    private getAppearancePropConfig(propertyData: any, tdManager: TableTdManager): ElementPropertyConfig {
        const props = this.getCommonAppearanceProperties();

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: props
        };
    }

    
    private getBorderPropConfig(propertyData: any, tdManager: TableTdManager): ElementPropertyConfig {

        return {
            categoryId: 'border',
            categoryName: '边框',
            propertyData: propertyData.border,
            enableCascade: true,
            parentPropertyID: 'border',
            properties: [
                {
                    propertyID: 'width',
                    propertyName: '宽度',
                    description: '设置表格边框宽度，单位px',
                    propertyType: 'number',
                    min: 0,
                    max: 10,
                    decimals: 0
                },
                {
                    propertyID: 'enableColor',
                    propertyName: '启用自定义颜色',
                    propertyType: 'boolean',
                    description: '是否启用自定义颜色',
                    defaultValue: false
                },
                {
                    propertyID: 'color',
                    propertyName: '自定义颜色',
                    propertyType: 'custom',
                    description: '统一设置表格边框颜色',
                    editor: FarrisColorpickerPlusComponent,
                    editorParams: {
                        preColor: propertyData.border && propertyData.border.color,
                        presets: ['#eaecf3', '#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4', '#009688', '#4caf50', '#8bc34a', '#cddc39', '#ffeb3b', '#ffc107', '#ff9800', '#ff5722  ', '#9e9e9e']
                    },
                    visible: propertyData.border.enableColor || false,
                }
            ],
            setPropertyRelates(changeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'enableColor': {
                        const color = this.properties.find(p => p.propertyID === 'color');
                        if (color) {
                            color.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
        };
    }

    private getFontPropConfigg(propertyData: any, tdManager: TableTdManager): ElementPropertyConfig {

        const supportedFontFamily = ['sans-serif', 'simsun', 'SimHei', 'arial', 'FangSong'];
        const fontIteraterList = [];
        supportedFontFamily.forEach(font => {
            fontIteraterList.push({ key: font, value: font });
        });
        return {
            categoryId: 'font',
            categoryName: '字体',
            propertyData: propertyData.font,
            enableCascade: true,
            parentPropertyID: 'font',
            properties: [
                {
                    propertyID: 'size',
                    propertyName: '大小',
                    description: '设置字体大小，单位px',
                    propertyType: 'number',
                    min: 10,
                    max: 20,
                    decimals: 0
                },
                {
                    propertyID: 'enableColor',
                    propertyName: '启用自定义颜色',
                    propertyType: 'boolean',
                    description: '是否启用自定义颜色',
                    defaultValue: false
                },
                {
                    propertyID: 'color',
                    propertyName: '自定义颜色',
                    propertyType: 'custom',
                    description: '设置字体框颜色',
                    editor: FarrisColorpickerPlusComponent,
                    editorParams: {
                        preColor: propertyData.font && propertyData.font.color,
                        presets: ['#2d2f33', '#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4', '#009688', '#4caf50', '#8bc34a', '#cddc39', '#ffeb3b', '#ffc107', '#ff9800', '#ff5722  ', '#9e9e9e']
                    },
                    visible: propertyData.font.enableColor || false,
                },
                {
                    propertyID: 'family',
                    propertyName: '字体类别',
                    propertyType: 'select',
                    description: '设置表格字体类别',
                    iterator: fontIteraterList
                }
            ],
            setPropertyRelates(changeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'enableColor': {
                        const color = this.properties.find(p => p.propertyID === 'color');
                        if (color) {
                            color.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
        };
    }
}
