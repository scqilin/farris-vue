export const contextMenu = [
  {
    title: '筛选条',
    id: 'ListFilter'
  },
  {
    title: '筛选方案',
    id: 'QueryScheme'
  },
  {
    title: '多视图',
    id: 'MultiViewContainer'
  },
  {
    title: '弹出编辑',
    id: 'EditInModalCard',
    category: 'editInCard',
    type: 'modal'
  },
  {
    title: '侧边栏编辑',
    id: 'EditInSidebarCard',
    category: 'editInCard',
    type: 'sidebar'
  }
];
