import { IdService } from '@farris/ui-common';
import { cloneDeep } from 'lodash-es';
import { TableTdSchema } from '../../schema/schema';

export class TableTdWidgetHelper {

    constructor() { }

    private getTableTdMetadata(): any {
        const metadata = cloneDeep(TableTdSchema);
        metadata.id = new IdService().generate();
        return metadata;
    }
    insertColumns(rows: any[], targetColumnIndex: number) {

        for (let e = 0; e < rows.length; e++) {
            const n = this.findPrevColIndex(e, targetColumnIndex, rows);
            if (n !== targetColumnIndex && rows[e].columns[targetColumnIndex].invisible) {
                const tdMetadata = this.getTableTdMetadata();
                tdMetadata.invisible = true;
                rows[e].columns.splice(targetColumnIndex, 0, tdMetadata);
                const i = rows[e].columns[n].colspan;
                rows[e].columns[n].colspan = i + 1;
                for (let o = 0; o < i + 1; o++) {
                    rows[e].columns[n + o].markCol = o;
                }
            } else {
                const tdMetadata = this.getTableTdMetadata();
                rows[e].columns.splice(targetColumnIndex, 0, tdMetadata);
            }

        }
    }

    findPrevColIndex(t: number, e: number, rows: any[]) {
        return e >= rows[t].columns.length || !rows[t].columns[e].markCol ? e : this.findPrevColIndex(t, e - 1, rows);
    }
    findNextColIndex(t: number, e: number, rows: any[]) {
        return e >= rows[t].columns.length || !rows[t].columns[e].markCol ? e : this.findNextColIndex(t, e + 1, rows);
    }

    findPrevRowIndex(t: number, e: number, rows: any[]) {
        return t >= rows.length || !rows[t].columns[e].markRow ? t : this.findPrevRowIndex(t - 1, e, rows);
    }

    findNextRowIndex(t: number, e: number, rows: any[]) {
        return t >= rows.length || !rows[t].columns[e].markRow ? t : this.findNextRowIndex(t + 1, e, rows);
    }
    insertRow(t: number, rows: any[]) {
        const n = [];
        for (let i = 0; i < rows[0].columns.length; i++) {
            const o = this.findPrevRowIndex(t, i, rows);
            const tdMetadata = this.getTableTdMetadata();

            if (o !== t && rows[t].columns[i].invisible) {
                tdMetadata.invisible = true;
                n.push(tdMetadata),
                    rows[o].columns[i].rowspan = rows[o].columns[i].rowspan + 1;
            } else {
                n.push(tdMetadata);
            }

        }
        rows.splice(t, 0, {
            id: new IdService().generate(),
            type: 'TableRow',
            columns: n
        });

        for (let e = 0; e < rows[t].columns.length; e++) {
            if (rows[t].columns[e].invisible) {
                const i = this.findPrevRowIndex(t - 1, e, rows);
                if (!rows[i].columns[e].invisible) {
                    this.markTableItem(i, e, rows);
                }

            }
        }

    }
    markTableItem(t: number, e: number, rows: any[]) {
        for (let n = rows[t].columns[e].colspan, i = rows[t].columns[e].rowspan, o = 0; o < i; o++) {
            for (let r = 0; r < n; r++) {
                rows[t + o].columns[e + r].markCol = r;
                rows[t + o].columns[e + r].markRow = o;
            }

        }

    }
}

