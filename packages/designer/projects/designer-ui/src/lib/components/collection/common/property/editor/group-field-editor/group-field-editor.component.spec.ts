import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupFieldEditorComponent } from './group-field-editor.component';

describe('GroupFieldEditorComponent', () => {
  let component: GroupFieldEditorComponent;
  let fixture: ComponentFixture<GroupFieldEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupFieldEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupFieldEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
