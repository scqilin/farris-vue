import { CodeEditorComponent, ItemCollectionConverter, ItemCollectionEditorComponent } from '@farris/designer-devkit';
import { FormSchemaEntityField, FormSchemaEntityFieldType$Type, FormSchemaEntityFieldTypeName } from '@farris/designer-services';
import { FormPropertyChangeObject } from '../../../../../../entity/property-change-entity';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { EditorTypes } from '@farris/ui-datagrid-editors';


export class AppointmentCalendarField {
    id: string;
    title: string;
    field: string;
    binding?: {
        path: string;
        field: string;
        fullPath: string;
    };
}
export class AppointmentCalendarFieldService {

    private propertyConfig: ElementPropertyConfig[];
    constructor() { }

    getCalendarFieldBySchema(schemaField: FormSchemaEntityField) {

        return {
            id: schemaField.id,
            title: schemaField.name,
            field: schemaField.bindingPath,
            binding: {
                path: schemaField.bindingField,
                field: schemaField.id,
                fullPath: schemaField.path
            },
            formatter: this.getFieldFormatter(schemaField)
        };
    }


    getPropertyConfig(propertyData: any, schemaField: FormSchemaEntityField): ElementPropertyConfig[] {

        this.propertyConfig = [];
        const usualConfig = this.getUsualPropertyConfig(propertyData, schemaField);
        this.propertyConfig = this.propertyConfig.concat(usualConfig);

        const formatterConfig = this.getFormatterPropertyConfig(propertyData);
        this.propertyConfig.push(formatterConfig);

        return this.propertyConfig;

    }

    private getUsualPropertyConfig(propertyData: any, schemaField: FormSchemaEntityField): ElementPropertyConfig[] {
        const self = this;

        return [{
            categoryId: 'detailPanel',
            categoryName: '字段配置',
            properties: [
                {
                    propertyID: 'title',
                    propertyName: '标题',
                    propertyType: 'string'
                }
            ]
        },
            // {
            //     categoryId: 'formatterType',
            //     categoryName: '格式化类型',
            //     hide: !propertyData.formatter,
            //     hideTitle: true,
            //     enableCascade: true,
            //     propertyData: propertyData.formatter,
            //     parentPropertyID: 'formatter',
            //     properties: [
            //         {
            //             propertyID: 'type',
            //             propertyName: '格式化类型',
            //             propertyType: 'select',
            //             description: '格式化类型选择',
            //             iterator: [
            //                 { key: 'none', value: '无' },
            //                 { key: 'number', value: '数字' },
            //                 { key: 'boolean', value: '布尔-文本' },
            //                 { key: 'boolean2', value: '布尔-图标' },
            //                 { key: 'datetime', value: '日期' },
            //                 { key: 'enum', value: '枚举' },
            //                 // { key: 'custom', value: '自定义' }
            //             ],
            //             refreshPanelAfterChanged: true
            //         }
            //     ],

            // }
        ];
    }


    private getFormatterPropertyConfig(propertyData: any): ElementPropertyConfig {
        const self = this;
        return {
            categoryId: 'formatter',
            categoryName: '格式化',
            hide: !propertyData.formatter,
            enableCascade: true,
            propertyData: propertyData.formatter,
            parentPropertyID: 'formatter',
            properties: [
                {
                    propertyID: 'type',
                    propertyName: '格式化类型',
                    propertyType: 'select',
                    description: '格式化类型选择',
                    iterator: [
                        { key: 'none', value: '无' },
                        { key: 'number', value: '数字' },
                        { key: 'boolean', value: '布尔-文本' },
                        { key: 'boolean2', value: '布尔-图标' },
                        { key: 'datetime', value: '日期' },
                        { key: 'enum', value: '枚举' },
                        // { key: 'custom', value: '自定义' }
                    ],
                    refreshPanelAfterChanged: true
                },
                {
                    propertyID: 'options',
                    propertyName: '格式化配置',
                    propertyType: 'cascade',
                    hideCascadeTitle: true,
                    isExpand: true,
                    visible: propertyData.formatter && propertyData.formatter.type !== 'none',
                    cascadeConfig: [
                        // {
                        //     propertyID: 'customFormat',
                        //     propertyName: '自定义格式化方法',
                        //     propertyType: 'modal',
                        //     description: '自定义格式化方法设置',
                        //     editor: CodeEditorComponent,
                        //     visible: propertyData.formatter && propertyData.formatter.type === 'custom',
                        //     group: 'custom',
                        //     editorParams: {
                        //         language: 'javascript',
                        //         exampleCode:
                        //             '\r\n  (value, rowData) => {\r\n    return value + \'的奖金\';\r\n  }\r\n\r\n ' +
                        //             '注：\r\n  value：单元格数据  \r\n  rowData：行数据  \r\n'
                        //     }
                        // },
                        {
                            propertyID: 'prefix',
                            propertyName: '前置字符',
                            propertyType: 'string',
                            description: '数字前置字符设置',
                            visible: propertyData.formatter && propertyData.formatter.type === 'number',
                            group: 'number',
                            refreshPanelAfterChanged: true
                        },
                        {
                            propertyID: 'suffix',
                            propertyName: '后置字符',
                            propertyType: 'string',
                            description: '数字后置字符设置',
                            visible: propertyData.formatter && propertyData.formatter.type === 'number',
                            group: 'number',
                            refreshPanelAfterChanged: true
                        },
                        {
                            propertyID: 'precision',
                            propertyName: '精度',
                            propertyType: 'number',
                            description: '数字精度设置',
                            decimals: 0,
                            min: 0,
                            defaultValue: 0,
                            visible: propertyData.formatter && propertyData.formatter.type === 'number',
                            group: 'number',
                            refreshPanelAfterChanged: true
                        },
                        {
                            propertyID: 'decimal',
                            propertyName: '小数分隔符',
                            propertyType: 'string',
                            description: '数字小数分隔符设置',
                            defaultValue: '.',
                            visible: propertyData.formatter && propertyData.formatter.type === 'number',
                            group: 'number',
                            refreshPanelAfterChanged: true
                        }, {
                            propertyID: 'thousand',
                            propertyName: '千分位',
                            propertyType: 'string',
                            description: '数字千分位设置',
                            defaultValue: ',',
                            visible: propertyData.formatter && propertyData.formatter.type === 'number',
                            group: 'number',
                            refreshPanelAfterChanged: true
                        },
                        {
                            propertyID: 'trueText',
                            propertyName: 'true文本',
                            propertyType: 'string',
                            description: 'true文本设置',
                            visible: propertyData.formatter &&
                                (propertyData.formatter.type === 'boolean' || propertyData.formatter.type === 'boolean2'),
                            group: 'boolean'
                        },
                        {
                            propertyID: 'falseText',
                            propertyName: 'false文本',
                            propertyType: 'string',
                            description: 'false文本设置',
                            visible: propertyData.formatter &&
                                (propertyData.formatter.type === 'boolean' || propertyData.formatter.type === 'boolean2'),
                            group: 'boolean'
                        },
                        {
                            propertyID: 'format',
                            propertyName: '日期格式',
                            propertyType: 'string',
                            description: '日期格式设置',
                            visible: propertyData.formatter && (propertyData.formatter.type === 'date' || propertyData.formatter.type === 'datetime'),
                            group: 'date',
                            refreshPanelAfterChanged: true
                        },
                        {
                            propertyID: 'data',
                            propertyName: '枚举数据',
                            propertyType: 'modal',
                            description: '枚举数据设置',
                            editor: ItemCollectionEditorComponent,
                            converter: new ItemCollectionConverter(),
                            editorParams: {
                                columns: [
                                    { field: 'value', title: '枚举值', editor: { type: EditorTypes.TEXTBOX } },
                                    { field: 'name', title: '枚举名称', editor: { type: EditorTypes.TEXTBOX } },
                                ],
                                requiredFields: ['value', 'name'],
                                uniqueFields: ['value', 'name'],
                                modalTitle: '枚举编辑器',
                                canEmpty: true
                            },
                            visible: propertyData.formatter && propertyData.formatter.type === 'enum'
                        },
                    ]
                }

            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters: any) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'type': {
                        // 初始格式化属性
                        const newFormatter = self.getFieldFormatter(null, changeObject.propertyValue);
                        Object.assign(propertyData.formatter.options, newFormatter.options);


                        const formatterConfig = self.getFormatterPropertyConfig(propertyData);
                        const currentFormatterConfig = self.propertyConfig.find(cat => cat.categoryId === 'formatter');
                        Object.assign(currentFormatterConfig, formatterConfig);


                        break;
                    }
                }
            }
        };
    }




    private getFieldFormatter(schemaField?: FormSchemaEntityField, formtterType?: string) {
        let fieldType = formtterType;
        if (schemaField && schemaField.type && schemaField.type.name) {
            fieldType = schemaField.type.name.toLowerCase();
        }
        switch (fieldType) {
            case 'date': {
                return {
                    type: 'datetime',
                    options: {
                        format: 'yyyy-MM-dd'
                    }
                };
            }
            case 'datetime': {
                return {
                    type: 'datetime',
                    options: {
                        format: 'yyyy-MM-dd HH:mm:ss'
                    }
                };
            }
            case 'enum': {
                return {
                    type: 'enum',
                    options: {
                        data: schemaField ? (schemaField.type.enumValues || []) : [],
                        textField: 'name',
                        valueField: 'value',
                        idField: 'value'
                    }
                };
            }
            case 'number': {
                return {
                    type: 'number',
                    options: {
                        prefix: '',
                        suffix: '',
                        precision: schemaField ? schemaField.type.precision : 0,
                        thousand: ',',
                        decimal: '.'
                    }
                };
            }
            case 'boolean': {
                return {
                    type: 'boolean',
                    options: {
                        trueText: '是',
                        falseText: '否'
                    }
                };
            }
            case 'boolean2': {
                return {
                    type: 'boolean',
                    options: {
                        trueText: '<span class="f-icon f-icon-checkbox-checked f-datagrid-default-show-icon"></span>',
                        falseText: '<span class="f-icon f-icon-checkbox f-datagrid-default-show-icon"></span>'
                    }
                };
            }
            default: {
                return {
                    type: 'none',
                    options: {}
                };
            }
        }
    }

}

