import { ExpressionProp } from '../../../input/common/property/expression/expression-property-config';
import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { EventEditorService, WebCmdService, DomService, SchemaService, FormBasicService, RefreshFormService, UniformEditorDataUtil, StateMachineService, DesignViewModelService } from '@farris/designer-services';
import { IDesignerHost } from '@farris/designer-element';
import { StyleEditorComponent } from '@farris/designer-devkit';
import { NotifyService } from '@farris/ui-notify';
import { DgControl } from '../../../../utils/dg-control';
import { NoCodeNumericBoxProp } from '../../../input/numeric-box/property/nocode-property-config';
import { NoCodeNumberRangeProp } from '../../../input/number-range/property/nocode-property-config';
import { NoCodeTimePickerProp } from '../../../input/time-picker/property/nocode-property-config';
import { NoCodeCheckBoxProp } from '../../../input/checkbox/property/nocode-property-config';
import { InputGroupProp } from '../../../input/input-group/property/property-config';
import { LanguageTextBoxProp } from '../../../input/language-text-box/property/property-config';
import { NoCodeTextBoxProp } from '../../../input/text-box/property/nocode-property-config';
import { NoCodeSwitchFieldProp } from '../../../input/switch/property/nocode-property-config';
import { NoCodeMultiTextBoxProp } from '../../../input/multi-text-box/property/nocode-property-config';
import { EnumFieldProp } from '../../../input/select/property/property-config';
import { NoCodeDateBoxProp } from '../../../input/date-box/property/nocode-property-config';
// import { NoCodeLookupEditProp } from '../../../input/lookup-edit/property/nocode-property-config';
// import { PersonnelSelectorProp } from '../../../input/personnel-selector/property/property-config';
import { AvatarProp } from '../../../input/avatar/property/property-config';
import { ImageProp } from '../../../input/image/property/property-config';
import { NoCodeCheckGroupProp } from '../../../input/check-group/property/nocode-property-config';
import { NoCodeRadioGroupProp } from '../../../input/radio-group/property/nocode-property-config';
import { NoCodeRichTextBoxProp } from '../../../input/rich-text-box/property/nocode-property-config';
import { NoCodeTagsProp } from '../../../input/tags/property/nocode-property-config';
// import { NoCodeOrganizationSelectorProp } from '../../../input/organization-selector/property/nocode-property-config';
import { MessagerService } from '@farris/ui-messager';
// import { EmployeeSelectorProp } from '../../../input/employee-selector/property/property-config';
// import { AdminOrganizationSelectorProp } from '../../../input/admin-org-selector/property/property-config';
// import { ExtIntergrationProp } from '../../../input/extintergration-input/property/property-config';
import { NoCodeCitySelectorProp } from '../../../input/city-selector/property/nocode-property-config';
import { ImageUploadProp } from '../../../input/image-upload/property/property-config';
// import { NoCodeOaRelationProp } from '../../../input/oa-relation/property/nocode-property-config';

// TODO： 这里应该是对CollectionProp的继承，否则后面添加任何低零代码的共同特性，都需要加两遍。
export class NoCodeCollectionProp extends ExpressionProp {
  public formBasicService: FormBasicService;
  public refreshFormService: RefreshFormService;
  public domService: DomService;
  public notifyService: NotifyService;
  public schemaService: SchemaService;
  public webCmdService: WebCmdService;
  public eventEditorService: EventEditorService;
  public viewModelId: string;
  public componentId: string;
  public msgService: MessagerService;
  public stateMachineService: StateMachineService;
  public dgVMService: DesignViewModelService;

  constructor(private serviceHost: IDesignerHost, viewModelId: string, componentId: string) {
    super(serviceHost.getService('DomService'));
    this.viewModelId = viewModelId;
    this.componentId = componentId;
    this.webCmdService = serviceHost.getService('WebCmdService');
    this.eventEditorService = serviceHost.getService('EventEditorService');
    this.domService = serviceHost.getService('DomService');
    this.formBasicService = serviceHost.getService('FormBasicService');
    this.refreshFormService = serviceHost.getService('RefreshFormService');
    this.schemaService = serviceHost.getService('SchemaService');
    this.stateMachineService = serviceHost.getService('StateMachineService');
    const injector = serviceHost.getService('Injector');
    this.notifyService = injector.get(NotifyService);
    this.msgService = injector.get(MessagerService);
  }



  getCommonAppearanceProperties(): PropertyEntity[] {

    return [
      // {
      //   propertyID: 'appearance',
      //   propertyName: '样式',
      //   propertyType: 'cascade',
      //   cascadeConfig: [
      //     {
      //       propertyID: 'class',
      //       propertyName: 'class样式',
      //       propertyType: 'string',
      //       description: '组件的CSS样式'
      //     },
      //     {
      //       propertyID: 'style',
      //       propertyName: 'style样式',
      //       propertyType: 'modal',
      //       description: '组件的内容样式',
      //       editor: StyleEditorComponent,
      //       showClearButton: true
      //     }
      //   ]
      // },
      {
        propertyID: 'size',
        propertyName: '尺寸',
        propertyType: 'cascade',
        cascadeConfig: [
          {
            propertyID: 'width',
            propertyName: '宽度（px）',
            propertyType: 'number',
            description: '组件的宽度',
            min: 0,
            decimals: 0
          },
          {
            propertyID: 'height',
            propertyName: '高度（px）',
            propertyType: 'number',
            description: '组件的高度',
            min: 0,
            decimals: 0
          }
        ]
      }
    ];

  }

  getVisiblePropertyEntity(propertyData: any, viewModelId: string) {
    return {
      propertyID: 'visible',
      propertyName: '是否可见',
      propertyType: 'unity',
      description: '运行时组件是否可见',
      editorParams: {
        controlName: UniformEditorDataUtil.getControlName(propertyData),
        constType: 'enum',
        editorOptions: {
          types: ['const', 'variable'],
          enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
          variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
          getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
          newVariableType: 'Boolean',
          newVariablePrefix: 'is'
        }
      }
    };
  }


  /**
   * 列编辑器属性
   * @param propertyData 列属性值
   * @param viewModelId viewModelId
   */
  getNoCodeFieldEditorProp(propertyData: any, showPosition = 'gridFieldEditor', isSimpleTable = false) {
    let inputPropInstance;
    switch (propertyData.editor.type) {
      case DgControl.NumericBox.type: {
        inputPropInstance = new NoCodeNumericBoxProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.NumberRange.type: {
        inputPropInstance = new NoCodeNumberRangeProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.TimePicker.type: {
        inputPropInstance = new NoCodeTimePickerProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.CheckBox.type: {
        inputPropInstance = new NoCodeCheckBoxProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.InputGroup.type: {
        inputPropInstance = new InputGroupProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.LanguageTextBox.type: {
        inputPropInstance = new LanguageTextBoxProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.MultiTextBox.type: {
        inputPropInstance = new NoCodeMultiTextBoxProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.SwitchField.type: {
        inputPropInstance = new NoCodeSwitchFieldProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.TextBox.type: {
        inputPropInstance = new NoCodeTextBoxProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.EnumField.type: {
        inputPropInstance = new EnumFieldProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.DateBox.type: {
        inputPropInstance = new NoCodeDateBoxProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      // case DgControl.LookupEdit.type: {
      //   inputPropInstance = new NoCodeLookupEditProp(this.serviceHost, this.viewModelId, this.componentId);
      //   break;
      // }
      // case DgControl.PersonnelSelector.type: {
      //   inputPropInstance = new PersonnelSelectorProp(this.serviceHost, this.viewModelId, this.componentId);
      //   break;
      // }
      case DgControl.Avatar.type: {
        inputPropInstance = new AvatarProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.CheckGroup.type: {
        inputPropInstance = new NoCodeCheckGroupProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.Image.type: {
        inputPropInstance = new ImageProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.RadioGroup.type: {
        inputPropInstance = new NoCodeRadioGroupProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.RadioGroup.type: {
        inputPropInstance = new NoCodeRadioGroupProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.RichTextBox.type: {
        inputPropInstance = new NoCodeRichTextBoxProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.Tags.type: {
        inputPropInstance = new NoCodeTagsProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      // case DgControl.OrganizationSelector.type: {
      //   inputPropInstance = new NoCodeOrganizationSelectorProp(this.serviceHost, this.viewModelId, this.componentId);
      //   break;
      // }
      // case DgControl.EmployeeSelector.type: {
      //   inputPropInstance = new EmployeeSelectorProp(this.serviceHost, this.viewModelId, this.componentId);
      //   break;
      // }
      // case DgControl.AdminOrganizationSelector.type: {
      //   inputPropInstance = new AdminOrganizationSelectorProp(this.serviceHost, this.viewModelId, this.componentId);
      //   break;
      // }
      // case DgControl.ExtIntergration.type: {
      //   inputPropInstance = new ExtIntergrationProp(this.serviceHost, this.viewModelId, this.componentId);
      //   break;
      // }
      // case DgControl.OaRelation.type: {
      //   inputPropInstance = new NoCodeOaRelationProp(this.serviceHost, this.viewModelId, this.componentId);
      //   break;
      // }
      case DgControl.CitySelector.type: {
        inputPropInstance = new NoCodeCitySelectorProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
      case DgControl.ImageUpload.type: {
        inputPropInstance = new ImageUploadProp(this.serviceHost, this.viewModelId, this.componentId);
        break;
      }
    }

    if (inputPropInstance) {
      if (showPosition === 'gridFieldEditor' && inputPropInstance.getGridFieldEdtiorPropConfig) {
        return inputPropInstance.getGridFieldEdtiorPropConfig(propertyData, this.viewModelId);
      }
      if (showPosition === 'tableTdEditor' && inputPropInstance.getTableTdEdtiorPropConfig) {
        return inputPropInstance.getTableTdEdtiorPropConfig(propertyData, this.viewModelId, isSimpleTable);
      }
    }

    return [];
  }
}
