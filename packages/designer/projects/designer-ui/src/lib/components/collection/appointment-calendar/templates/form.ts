import getDayViewTemplate from './views/day-view.form';
import getWeekViewTemplate from './views/week-view.form';

export default (ctx: any) => {

    const currentView = ctx.component.currentViewInDesignerView || 'day';
    const isDayView = currentView === 'day';
    const dayButtonCls = isDayView ? 'btn-active' : '';
    const weekButtonCls = !isDayView ? 'btn-active' : '';
    const viewTemplate = isDayView ? getDayViewTemplate(ctx) : getWeekViewTemplate(ctx);

    const dateTemplate = isDayView ? getSingleDateTemplate() : getDateRangeTemplate();

    return `
<div class="f-utils-fill-flex-row">
    <div class="rtv-container f-utils-flex-column w-100 h-100 ${currentView === 'week' ? 'rtv-container-week' : ''}">
        <toolbar>
            <div class="toolbar">
                <div class="view-type ">
                    <button class="btn-day ${dayButtonCls}" viewType='day' type="button" ref=${ctx.viewTypeKey}>日</button>
                    <button class="btn-day ${weekButtonCls}"  viewType='week' type="button"  ref=${ctx.viewTypeKey}>周</button>
                </div>
                <span class="line"></span>
                <div class="btns">
                    <button class="btn-today">${isDayView ? '今天' : '本周'}</button>
                    <div class="btn-group">
                        <button class="btn-prev" title=${isDayView ? '上一天' : '上一周'}>&lt;</button>
                        <button class="btn-next" title=${isDayView ? '下一天' : '下一周'}>&gt;</button>
                    </div>
                </div>
                <div class="inputs d-flex">
                    <div class="datebox mr-4" style="width: ${isDayView ? '180px' : '260px'};">
                        <div class="f-cmp-datepicker f-cmp-inputgroup">
                            <div class="input-group f-state-editable">
                                ${dateTemplate}
                                <div class="input-group-append f-cmp-iconbtn-wrapper" style="position: relative;">
                                    <button class="btn f-cmp-iconbtn" type="button">
                                        <span class="f-icon f-icon-date"></span></button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </toolbar>
        <div class="main f-utils-fill-flex-column">
            ${viewTemplate}
        </div>
    </div>
</div>
`;

};

/** 单日期显示文本 */
function getSingleDateTemplate() {
    return '<span class="form-control f-utils-fill" style="padding-right: 4px;" type="text">2023年02月27日</span>';
}

/** 日期区间显示文本 */
function getDateRangeTemplate() {
    return `
    <div class="d-flex">
        <span class="form-control f-utils-fill" style="padding-right: 4px;" type="text">2023年02月26日</span>
        <span  class="f-icon f-icon-orientation-arrow sub-input-spliter" style="line-height: 1.4286;"></span>
        <span class="form-control f-utils-fill" style="padding-right: 4px;" type="text">2023年03月04日</span>
    </div>
    `;
}
