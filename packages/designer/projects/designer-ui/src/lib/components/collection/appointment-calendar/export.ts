import { AppointmentCalendarSchema } from './schema/appointment-calendar';
import { ComponentExportEntity } from '@farris/designer-element';
import FdAppointmentCalendarTemplates from './templates';
import FdAppointmentCalendarComponent from './component/fd-appointment-calendar';


export const AppointmentCalendar: ComponentExportEntity = {
    type: 'AppointmentCalendar',
    component: FdAppointmentCalendarComponent,
    template: FdAppointmentCalendarTemplates,
    metadata: AppointmentCalendarSchema
};
