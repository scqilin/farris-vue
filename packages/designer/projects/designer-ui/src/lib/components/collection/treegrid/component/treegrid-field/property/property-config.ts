import { CollectionProp } from '../../../../common/property/collection-property-config';
import { ElementPropertyConfig, KeyMap, PropertyEntity } from '@farris/ide-property-panel';
import { DgControl } from '../../../../../../utils/dg-control';
import { FormPropertyChangeObject } from '../../../../../../entity/property-change-entity';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { FieldStyleConfigComponent } from '../../../../common/property/editor/field-style-config/field-style-config.component';
import { DesignerEnvType, GridFieldDataType } from '@farris/designer-services';
import {
    BindingEditorComponent, BindingEditorConverter,
    ItemCollectionConverter, ItemCollectionEditorComponent, JavaScriptBooleanEditorComponent
} from '@farris/designer-devkit';
import { EditorTypes } from '@farris/ui-datagrid-editors';
import { GridFieldFormatterConverter } from '../../../../common/property/editor/grid-field-formatter-converter';

export class TreeGridFieldProp extends CollectionProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any, gridData: any): ElementPropertyConfig[] {

        this.propertyConfig = [];
        const viewModelId = this.viewModelId;

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 快捷配置属性
        const fieldStyleConfig = this.getFieldStyleProperties(propertyData, viewModelId, gridData);
        this.propertyConfig.push(fieldStyleConfig);

        // 外观属性
        const appearanceProperties = this.getAppearanceProperties(propertyData, viewModelId);
        this.propertyConfig.push(appearanceProperties);

        // 行为属性
        const behaviorProperties = this.getBehaviorProperties(propertyData, viewModelId, gridData);
        this.propertyConfig.push(behaviorProperties);

        // 列编辑器属性
        if (gridData.fieldEditable && propertyData.editor) {
            const editorProp = this.getFieldEditorProp(propertyData);
            if (editorProp.length > 0) {
                this.propertyConfig = this.propertyConfig.concat(editorProp);
            }
        }
        return this.propertyConfig;
    }

    private getBasicPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '组件的id',
                    readonly: true
                },
                {
                    propertyID: 'caption',
                    propertyName: '标题',
                    propertyType: 'string',
                    description: '列标题名称'
                }
            ]
        };
    }


    private getFieldStyleProperties(propertyData: any, viewModelId: string, gridData: any): ElementPropertyConfig {
        const self = this;
        return {
            categoryId: 'fieldStyleShortcutConfig',
            categoryName: '快捷配置',
            hide: !['date', 'datetime', 'enum', 'number'].includes(propertyData.dataType),
            properties: [
                {
                    propertyID: 'fieldStyleConfig',
                    propertyType: 'custom',
                    editor: FieldStyleConfigComponent,
                    editorParams: {
                        propertyData,
                        needClearConfig: false
                    },

                    description: '提供常用的日期、数字展示格式，提供带图标的枚举数据模板',
                    refreshPanelAfterChanged: true
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters: any) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'fieldStyleConfig': {
                        // 数字、日期重置格式化属性
                        if (changeObject.propertyValue && changeObject.propertyValue.formatter) {
                            const formatterValue = changeObject.propertyValue.formatter;
                            Object.assign(propertyData.formatter, formatterValue);
                        }
                        // 枚举重置列模板属性
                        if (changeObject.propertyValue && changeObject.propertyValue.colTemplate) {
                            propertyData.colTemplate = changeObject.propertyValue.colTemplate;

                            propertyData.iconConfigForEnumData = changeObject.propertyValue.enumIconConfig;

                            const fieldStyleConfig = this.properties.find((p: PropertyEntity) => p.propertyID === 'fieldStyleConfig');
                            fieldStyleConfig.editorParams.propertyData = propertyData;
                            fieldStyleConfig.editorParams.needClearConfig = false;
                        }
                        delete propertyData.fieldStyleConfig;


                        // 变更【外观分类】下的格式化属性
                        const currentAppearanceConfig = self.propertyConfig.find(cat => cat.categoryId === 'appearance');
                        const formatterProp = currentAppearanceConfig.properties.find(p => p.propertyID === 'formatter');
                        if (formatterProp) {
                            const newFormattterProps = self.getFormatterProps(propertyData);
                            Object.assign(formatterProp.cascadeConfig, newFormattterProps);
                        }
                        break;
                    }
                }
            }
        };
    }
    private getAppearanceProperties(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const formatterProps = this.getFormatterProps(propertyData);

        const appearanceProperties: PropertyEntity[] = [
            {
                propertyID: 'size',
                propertyName: '尺寸',
                propertyType: 'cascade',
                cascadeConfig: [
                    {
                        propertyID: 'width',
                        propertyName: '宽度（px）',
                        propertyType: 'number',
                        description: '组件的宽度',
                        min: 0,
                        decimals: 0
                    }
                ]
            },
            {
                propertyID: 'showTips',
                propertyName: '显示单元格提示',
                propertyType: 'boolean',
                description: '是否显示单元格提示',
                defaultValue: false
            },
            {
                propertyID: 'tipContent',
                propertyName: '单元格提示内容',
                propertyType: 'modal',
                description: '单元格提示内容设置',
                editor: CodeEditorComponent,
                visible: propertyData.showTips,
                editorParams: {
                    language: 'javascript',
                    exampleCode:
                        '(value, rowData, column) => {\r\n    return value + \'的提示\';\r\n}\r\n\r\n\r\n ' +
                        '注：\r\n  value：单元格数据  \r\n  rowData：行数据\r\n  column：列数据'
                }
            },
            {
                propertyID: 'formatter',
                propertyName: '格式化',
                propertyType: 'cascade',
                cascadeConfig: formatterProps,
                cascadeConverter: new GridFieldFormatterConverter()
            },
            {
                propertyID: 'colTemplate',
                propertyName: '列模板',
                propertyType: 'modal',
                description: '列模板设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html',
                    exampleCode: ''
                },
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'styler',
                propertyName: '单元格样式',
                propertyType: 'modal',
                description: '单元格样式设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'javascript',
                    exampleCode: '(value, rowData, rowIndex) => {\r\n    return {\r\n        style: {\r\n            \'color\': \'red\',\r\n' +
                        '            \'font-weight\': 800\r\n        }\r\n    };\r\n}\r\n\r\n\r\n注：\r\n  value：单元格数据  \r\n  ' +
                        'rowData：行数据  \r\n  rowIndex：当前行索引'
                }
            },
            {
                propertyID: 'textAlign',
                propertyName: '数据水平对齐方式',
                propertyType: 'select',
                description: '数据水平对齐方式设置',
                defaultValue: 'left',
                iterator: [{ key: 'left', value: '靠左' }, { key: 'right', value: '靠右' }, { key: 'center', value: '居中' }]
            },
            {
                propertyID: 'hAlign',
                propertyName: '表头对齐方式',
                propertyType: 'select',
                description: '表头对齐方式设置',
                defaultValue: 'left',
                iterator: [{ key: 'left', value: '靠左' }, { key: 'right', value: '靠右' }, { key: 'center', value: '居中' }]
            }
        ];

        if (propertyData.controlSource === 'Primeng') {
            const allowedProps = ['appearance', 'size', 'formatter'];
            appearanceProperties.map(p => {
                if (!allowedProps.includes(p.propertyID)) {
                    p.visible = false;
                }
            });
        }

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject, data, parameters: any) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'type': {
                        // 列格式化属性
                        if (changeObject.parentPropertyID === 'formatter') {
                            const formatter = this.properties.find(p => p.propertyID === 'formatter');
                            if (formatter && formatter.cascadeConfig) {
                                formatter.cascadeConfig.filter(p => p.group).forEach(p => {
                                    p.visible = p.group === changeObject.propertyValue;
                                });
                            }

                            propertyData.formatter = self.setDataGridFormatterDefaults(changeObject.propertyValue, propertyData.formatter);


                            // 联动修改列样式的快捷配置
                            const fieldStyleShortcutConfig = self.propertyConfig.find(c => c.categoryId === 'fieldStyleShortcutConfig');
                            if (fieldStyleShortcutConfig && fieldStyleShortcutConfig.properties && fieldStyleShortcutConfig.properties.length) {
                                const fieldStyleProp = fieldStyleShortcutConfig.properties.find(p => p.propertyID === 'fieldStyleConfig');
                                if (fieldStyleProp) {
                                    fieldStyleProp.editorParams.propertyData = propertyData;
                                }
                            }
                        }
                        break;
                    }
                    case 'colTemplate': {
                        // 联动修改列样式的快捷配置
                        self.clearIconConfigAfterColTemplateChanged(propertyData, changeObject, parameters);

                        const fieldStyleShortcutConfig = self.propertyConfig.find(c => c.categoryId === 'fieldStyleShortcutConfig');
                        if (fieldStyleShortcutConfig && fieldStyleShortcutConfig.properties && fieldStyleShortcutConfig.properties.length) {
                            const fieldStyleProp = fieldStyleShortcutConfig.properties.find(p => p.propertyID === 'fieldStyleConfig');
                            if (fieldStyleProp) {
                                fieldStyleProp.editorParams.propertyData = propertyData;
                                fieldStyleProp.editorParams.needClearConfig = true;
                            }
                        }

                        break;
                    }
                    case 'showTips': {
                        const tipContent = this.properties.find(p => p.propertyID === 'tipContent');
                        if (tipContent) {
                            tipContent.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    default: {
                        // 联动修改列样式的快捷配置
                        if (changeObject.parentPropertyID === 'formatter') {
                            const fieldStyleShortcutConfig = self.propertyConfig.find(c => c.categoryId === 'fieldStyleShortcutConfig');
                            if (fieldStyleShortcutConfig && fieldStyleShortcutConfig.properties && fieldStyleShortcutConfig.properties.length) {
                                const fieldStyleProp = fieldStyleShortcutConfig.properties.find(p => p.propertyID === 'fieldStyleConfig');
                                if (fieldStyleProp) {
                                    fieldStyleProp.editorParams.propertyData = propertyData;
                                }
                            }
                        }
                    }
                }

                self.setGridFieldEditorPropSync(propertyData, changeObject);
            }
        };
    }

    /**
     * 组装列格式化属性
     */
    private getFormatterProps(propertyData: any): PropertyEntity[] {
        return [
            {
                propertyID: 'type',
                propertyName: '格式化类型',
                propertyType: 'select',
                description: '格式化',
                iterator: this.getDateFormatter(propertyData),
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'customFormat',
                propertyName: '自定义格式化方法',
                propertyType: 'modal',
                description: '自定义格式化方法设置',
                editor: CodeEditorComponent,
                visible: propertyData.formatter && propertyData.formatter.type === 'custom',
                group: 'custom',
                editorParams: {
                    language: 'javascript',
                    exampleCode:
                        '\r\n  (value, rowData) => {\r\n    return value + \'的奖金\';\r\n  }\r\n\r\n ' +
                        '注：\r\n  value：单元格数据  \r\n  rowData：行数据  \r\n'
                }
            },
            {
                propertyID: 'prefix',
                propertyName: '前置字符',
                propertyType: 'string',
                description: '数字前置字符设置',
                visible: propertyData.formatter && propertyData.formatter.type === 'number',
                group: 'number',
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'suffix',
                propertyName: '后置字符',
                propertyType: 'string',
                description: '数字后置字符设置',
                visible: propertyData.formatter && propertyData.formatter.type === 'number',
                group: 'number',
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'precision',
                propertyName: '精度',
                propertyType: 'number',
                description: '数字精度设置',
                decimals: 0,
                min: 0,
                defaultValue: 0,
                visible: propertyData.formatter && propertyData.formatter.type === 'number',
                group: 'number',
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'decimal',
                propertyName: '小数分隔符',
                propertyType: 'string',
                description: '数字小数分隔符设置',
                defaultValue: '.',
                visible: propertyData.formatter && propertyData.formatter.type === 'number',
                group: 'number',
                refreshPanelAfterChanged: true
            }, {
                propertyID: 'thousand',
                propertyName: '千分位',
                propertyType: 'string',
                description: '数字千分位设置',
                defaultValue: ',',
                visible: propertyData.formatter && propertyData.formatter.type === 'number',
                group: 'number',
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'trueText',
                propertyName: 'true文本',
                propertyType: 'string',
                description: 'true文本设置',
                visible: propertyData.formatter && propertyData.formatter.type === 'boolean',
                group: 'boolean'
            },
            {
                propertyID: 'falseText',
                propertyName: 'false文本',
                propertyType: 'string',
                description: 'false文本设置',
                visible: propertyData.formatter && propertyData.formatter.type === 'boolean',
                group: 'boolean'
            },
            {
                propertyID: 'dateFormat',
                propertyName: '日期格式',
                propertyType: 'string',
                description: '日期格式设置',
                visible: propertyData.formatter && propertyData.formatter.type === 'date',
                group: 'date',
                refreshPanelAfterChanged: true
            }
        ];
    }
    private getBehaviorProperties(propertyData: any, viewModelId: string, gridData: any): ElementPropertyConfig {
        const self = this;
        // 获取绑定字段类型
        let dgVMField;
        let dgVMFieldType;
        const dgViewModel = this.dgVMService.getDgViewModel(viewModelId);
        if (propertyData.binding && dgViewModel) {
            dgVMField = dgViewModel.fields.find(f => f.id === propertyData.binding.field);
            if (dgVMField) {
                dgVMFieldType = dgVMField.multiLanguage ? 'multiLanguage' : dgVMField.type.name;
            }
        }

        const behaviorProperties = [
            {
                propertyID: 'binding',
                propertyName: '绑定',
                propertyType: 'modal',
                description: '绑定的表单字段',
                editor: BindingEditorComponent,
                editorParams: {
                    viewModelId,
                    componentId: propertyData.componentId,
                    controlType: propertyData.type,
                    allowedBindingType: 'Form',
                    gridFielType: dgVMFieldType,
                    unusedOnly: true
                },
                converter: new BindingEditorConverter(),
                readonly: !dgVMFieldType // 失效的字段，不允许切换类型，只能删除
            },
            // {
            //     propertyID: 'multiLanguage',
            //     propertyName: '是否多语字段',
            //     propertyType: 'boolean',
            //     readonly: true
            // },
            // {
            //     propertyID: 'languages',
            //     propertyName: '语言列表',
            //     propertyType: 'modal',
            //     editor: ItemCollectionEditorComponent,
            //     editorParams: {
            //         columns: [
            //             { field: 'code', title: '编号', editor: { type: EditorTypes.TEXTBOX } },
            //             { field: 'name', title: '名称', editor: { type: EditorTypes.TEXTBOX } }],
            //         requiredFields: ['code', 'name'],
            //         uniqueFields: ['code', 'name'],
            //         modalTitle: '语言编辑器',
            //         canEmpty: false
            //     },
            //     converter: new ItemCollectionConverter(),
            //     visible: propertyData.multiLanguage
            // },
            {
                propertyID: 'visible',
                propertyName: '是否可见',
                propertyType: 'modal',
                description: '运行时组件是否可见',
                editor: JavaScriptBooleanEditorComponent,
                editorParams: {
                    modalTitle: '可见编辑器',
                    viewModelId,
                    showJavascript: true,
                    exampleCode: '(rowData) => { \r\n    // ...\r\n    return true;\r\n}\r\n\r\n参数：\r\nrowData：行数据' +
                        '\r\n\r\n\r\n函数返回布尔类型：\r\ntrue: 可见\r\nfalse: 隐藏'
                }
            },
            {
                propertyID: 'enumData',
                propertyName: '枚举数据',
                propertyType: 'modal',
                description: '枚举数据设置',
                editor: ItemCollectionEditorComponent,
                converter: new ItemCollectionConverter(),
                editorParams: {
                    columns: [
                        { field: 'value', title: '枚举值', editor: { type: EditorTypes.TEXTBOX } },
                        { field: 'name', title: '枚举名称', editor: { type: EditorTypes.TEXTBOX } },
                    ],
                    requiredFields: ['value', 'name'],
                    uniqueFields: ['value', 'name'],
                    modalTitle: '枚举编辑器',
                    canEmpty: false
                },
                visible: propertyData.dataType === 'enum'
            },
            {
                propertyID: 'frozen',
                propertyName: '固定列',
                propertyType: 'select',
                description: '固定列选择',
                iterator: [
                    { key: false, value: '不固定' },
                    { key: 'left', value: '固定左侧' },
                    { key: 'right', value: '固定右侧' }
                ],
                readonly: gridData.enableHeaderGroup
            },
            {
                propertyID: 'sortable',
                propertyName: '允许排序',
                propertyType: 'boolean',
                description: '是否可排序'
            },
            {
                propertyID: 'localization',
                propertyName: '启用数据国际化',
                propertyType: 'boolean',
                description: '是否启用数据国际化',
                visible: this.checkLocalizationVisible(propertyData)
            },
            {
                propertyID: 'localizationType',
                propertyName: '数据国际化类型',
                propertyType: 'select',
                description: '数据国际化类型选择',
                visible: this.checkLocalizationVisible(propertyData) && propertyData.localization,
                iterator: this.getLocalizationTypes(propertyData)
            }
        ];

        if (propertyData.controlSource === 'Primeng') {
            const allowedProps = ['binding', 'enumData', 'sortable'];
            behaviorProperties.map(p => {
                if (!allowedProps.includes(p.propertyID)) {
                    p.visible = false;
                }
            });
        }

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject, data) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'binding': {
                        // 修改绑定后需要刷新实体树
                        self.refreshFormService.refreshSchamaTree.next();
                        break;
                    }
                    case 'localization': {
                        const localizationType = this.properties.find(p => p.propertyID === 'localizationType');
                        if (localizationType) {
                            localizationType.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    case 'frozen': {
                        if (propertyData.controlSource === 'Farris') {
                            if (changeObject.propertyValue) {
                                self.notifyService.warning('请确保树列表未启用多表头！');
                            }
                        }
                        break;
                    }
                }

                self.setGridFieldEditorPropSync(propertyData, changeObject);
            }
        };
    }


    /**
     * farris datagrid 列 设置格式默认值
     * @param type 格式化类型
     * @param col 列格式值
     */
    private setDataGridFormatterDefaults(type: string, col: any) {
        switch (type) {
            case 'number': {
                col = Object.assign({
                    precision: 0,
                    thousand: ',',
                    prefix: '',
                    suffix: '',
                    decimal: '.'
                }, col);
                break;
            }
            case 'boolean': {
                col = Object.assign({
                    trueText: '是',
                    falseText: '否'
                }, col);
                break;
            }
            case 'date': {
                col = Object.assign({
                    dateFormat: 'yyyy-MM-dd',
                }, col);
                break;
            }
        }
        return col;
    }


    /**
     * 设置列表列和编辑器的属性联动(目前树表没有列编辑器)
     * @param propertyData 属性值
     * @param changeObject 变更集
     */
    private setGridFieldEditorPropSync(propertyData, changeObject) {
        // 同步列编辑器
        if (propertyData.editor) {
            if (changeObject.propertyID === 'format' || changeObject.propertyID === 'enumData' ||
                changeObject.propertyID === 'binding' || changeObject.propertyID === 'languages') {
                propertyData.editor[changeObject.propertyID] = changeObject.propertyValue;
            }
        }
    }


    private getLocalizationTypes(propertyData: any): KeyMap[] {
        switch (propertyData.dataType) {
            case GridFieldDataType.date: {
                return [{ key: 'Date', value: '日期' }];
            }
            case GridFieldDataType.datetime: {
                return [{ key: 'DateTime', value: '日期时间' }];
            }
            default: {
                return [{ key: 'Date', value: '日期' }, { key: 'DateTime', value: '日期时间' }];
            }
        }
    }


    /**
     * 限定数据国际化类型属性展示范围：
     * 1、日期、日期时间类型字段---显示
     * 2、可编辑的列表:编辑器配置成了日期控件---显示
     * 3、不可编辑的列表：格式化属性配置成日期---显示
     */
    private checkLocalizationVisible(propertyData: any): boolean {
        if (propertyData.controlSource !== 'Farris') {
            return false;
        }
        if (this.formBasicService.envType === DesignerEnvType.noCode) {
            return false;
        }
        switch (propertyData.dataType) {
            // 日期类型字段
            case 'date': case 'datetime': {
                return true;
            }
            // 字符串类型字段
            case 'string': {
                // 可编辑列表
                if (propertyData.editor && propertyData.editor.type === DgControl.DateBox.type) {
                    return true;
                }
                // 不可编辑列表
                if (!propertyData.editor && propertyData.formatter && propertyData.formatter.type === 'date') {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 枚举类型字段，在变更列模板后需要重置图标配置数据
     * @param propertyData 列数据
     * @param changeObject 属性变更集
     * @param parameters 变更参数
     */
    private clearIconConfigAfterColTemplateChanged(propertyData: any, changeObject: FormPropertyChangeObject, parameters: any) {
        if (propertyData.dataType !== 'enum' || !propertyData.iconConfigForEnumData) {
            return;
        }

        if (changeObject.propertyValue !== parameters.oldValue) {
            propertyData.iconConfigForEnumData = null;
            this.notifyService.warning('变更列模板后，自动重置列样式快捷配置！');
        }
    }
    /**
     * 获取列格式化类型
     */
    private getDateFormatter(propertyData: any) {

        const iterator = [
            { key: 'none', value: '无' },
            { key: 'number', value: '数字' },
            { key: 'boolean', value: '布尔-文本' },
            { key: 'boolean2', value: '布尔-图标' },
            { key: 'date', value: '日期' },
            { key: 'enum', value: '枚举' },
            { key: 'custom', value: '自定义' }
        ];

        // 绑定字段为日期时，支持相对时间格式
        if (propertyData.dataType === GridFieldDataType.date || propertyData.dataType === GridFieldDataType.datetime) {
            const timego = { key: 'timeago', value: '相对时间' };
            iterator.splice(5, 0, timego);
        }

        return iterator;
    }
}
