export default (ctx: any) => {
  /** 收集表格行模板 */
  let trEls = '';
  ctx.component.rows.forEach((trEle, rowIndex) => {
    if (!trEle.columns || !trEle.columns.length) {
      return '';
    }

    /** 收集单元格模板 */
    let tdEles = '';
    trEle.columns.forEach((tdEle, columnIndex) => {
      const tdEleStr = getTableTdElementStr(tdEle, columnIndex, rowIndex, ctx);
      tdEles += tdEleStr;
    });

    trEls += `<tr>${tdEles}</tr>`;

  });

  const tableClass = (ctx.component.appearance && ctx.component.appearance.class || '') + ' ide-cmp-table';
  const tableStyle = getTableStyle(ctx);

  return `
            <div class="drag-container" id = "${ctx.component.id}"  >
                <table class="${tableClass}" style="${tableStyle}" >
                    ${trEls}
                </table>
            </div>
      `;
};


/**
 * 获取表格层级的内联样式
 * @param ctx 模板构造信息
 */
function getTableStyle(ctx: any): string {
  // 定制边框宽度、颜色
  const borderWidth = getBorderWidth(ctx.component.border);

  const borderColor = ctx.component.border.enableColor && ctx.component.border && ctx.component.border.color ? ctx.component.border.color : '#eaecf3';

  let tableStyle = ctx.component.appearance && ctx.component.appearance.style || '';
  if (ctx.component.size && ctx.component.size.width) {
    tableStyle = tableStyle + ';width:' + ctx.component.size.width + 'px';
  }
  if (ctx.component.size && ctx.component.size.height) {
    tableStyle = tableStyle + ';height:' + ctx.component.size.height + 'px';
  }

  // 为了防止table外层出现两层边框，区分宽度并设置不同的样式
  if (borderWidth === 1) {
    tableStyle = tableStyle + ';box-shadow:0px 0px 0px 1px ' + borderColor;
  } else {
    tableStyle = tableStyle + ';box-shadow:none;border:none';
  }

  return tableStyle;
}


/**
 * 获取单元格td层级的内联样式
 * @param tdEle 单元格schema对象
 * @param columnIndex 列索引
 * @param rowIndex 行索引
 * @param ctx 模板构造信息
 */
function getTableTdElementStr(tdEle: any, columnIndex: number, rowIndex: number, ctx: any): string {
  let tdContent = '';

  const tdStyle = getTableTdStyle(ctx, tdEle);
  const tdClass = getTableTdClass(ctx, tdEle);

  // 静态文本类单元格
  if (tdEle.tdType === 'staticText' && tdEle.staticText && tdEle.staticText.text) {
    const require = tdEle.staticText.require === true ? `<span class="farris-label-info text-danger">*</span>` : '';
    tdContent = `
                    <div class="farris-label-wrap tdLabel">
                        <label class="col-form-label">
                            ${require}
                            <span class="farris-label-text">${tdEle.staticText.text || ' '}</span>
                        </label>
                    </div>`;
  } else if (tdEle.tdType === 'editor' && tdEle.editor && tdEle.editor.type) {
    // 编辑器类单元格
    tdContent = ctx.tdComponents[rowIndex][columnIndex];
  }

  // 给单元格增加拖拽限制
  if (ctx.checkTdCanDragAndDrop) {
    tdContent = ctx.checkTdCanDragAndDrop(tdEle, tdContent, ctx.component.devMode);
  }
  // 返回单元格DOM
  return `
    <td style="${tdStyle}" class="${tdClass}" colspan=${tdEle.colspan} rowspan=${tdEle.rowspan} id=${tdEle.id} ref=${ctx.tdLinkKey}  groupid=${tdEle.groupId || ''} tdtype=${tdEle.tdType}>
      <div class="h-100 tdDragContainer" dragref="${tdEle.id}-container" ref="nested-${tdEle.id}">
          ${tdContent}
          <div class="widget-panel no-drag no-drop" ref="${ctx.tdWidgetLinkKey}">
              <span class="f-icon f-icon-custom"></span>
          </div>
      </div>
    </td>
    `;

}


/**
 * 获取单元格td层级的内联样式
 * @param ctx 模板构造信息
 * @param tdEle 单元格schema对象
 */
function getTableTdStyle(ctx: any, tdEle: any): string {

  // 获取定制边框宽度、颜色
  const borderWidth = getBorderWidth(ctx.component.border);
  const borderColor = ctx.component.border.enableColor && ctx.component.border && ctx.component.border.color ? ctx.component.border.color : '#eaecf3';
  const fontSize = ctx.component.font && ctx.component.font.size ? ctx.component.font.size : 13;

  const fontColor = ctx.component.font.enableColor && ctx.component.font && ctx.component.font.color ? ctx.component.font.color : '#2d2f33';
  const fontFamily = ctx.component.font && ctx.component.font.family ? ctx.component.font.family : 'sans-serif';

  // 应用appearance 中定义的宽高
  let tdStyle = tdEle.appearance && tdEle.appearance.style ? tdEle.appearance.style : '';
  if (tdEle.size && tdEle.size.width) {
    tdStyle = tdStyle + ';width:' + tdEle.size.width + 'px';
  }
  if (tdEle.size && tdEle.size.width) {
    tdStyle = tdStyle + ';height:' + tdEle.size.height + 'px';
  }

  // 应用表格上定义的边框、字体
  tdStyle = tdStyle + ';border:' + borderWidth + 'px solid ' + borderColor;
  tdStyle = tdStyle + ';font-size:' + fontSize + 'px';
  tdStyle = tdStyle + ';color:' + fontColor;
  tdStyle = tdStyle + ';font-family:' + fontFamily;

  // 选中状态下的单元格需要设置边框宽度
  if (tdEle.id === ctx.targetTdId || (ctx.targetMergedTdIds && ctx.targetMergedTdIds.includes(tdEle.id))) {
    tdStyle += ctx.getSelectedBorderStyle ? ctx.getSelectedBorderStyle(tdEle) : '';
  }

  return tdStyle;
}

/**
 * 获取单元格td层级的class样式
 * @param ctx 模板构造信息
 * @param tdEle 单元格schema对象
 */
function getTableTdClass(ctx: any, tdEle: any): string {
  let tdClass = tdEle.appearance && tdEle.appearance.class ? tdEle.appearance.class : '';
  if (tdEle.invisible) {
    tdClass += tdEle.invisible ? ' d-none' : '';
  }
  if (tdEle.id === ctx.targetTdId || (ctx.targetMergedTdIds && ctx.targetMergedTdIds.includes(tdEle.id))) {
    tdClass += ' dgComponentSelected';
  }

  return tdClass;
}

function getBorderWidth(border: any) {

  return border && border.width !== null && border.width !== undefined ? border.width : 1;
}
