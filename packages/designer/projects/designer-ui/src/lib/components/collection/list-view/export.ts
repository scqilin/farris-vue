import { ComponentExportEntity } from "@farris/designer-element";
import { FdListViewComponent } from "./component/fd-list-view";
import { ListViewSchema } from "./schema/schema";
import FdListViewTemplates from './templates';

export const ListView: ComponentExportEntity = {
    type: 'TreeGrid',
    component: FdListViewComponent,
    template: FdListViewTemplates,
    metadata: ListViewSchema
};
