import { CollectionProp } from '../../common/property/collection-property-config';
import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { DgControl } from '../../../../utils/dg-control';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { GridFieldEditorComponent } from '../../common/property/editor/grid-field-editor/grid-field-editor.component';
import { GridFieldConverter } from '../../common/property/editor/grid-field-editor/grid-field-converter';
import { GroupFieldEditorComponent } from '../../common/property/editor/group-field-editor/group-field-editor.component';
import { GroupFieldConverter } from '../../common/property/editor/group-field-editor/group-field-converter';
import { ColumnGroupEditorComponent } from '@farris/header-group-editor';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { cloneDeep } from 'lodash-es';
// import { EntityFilterConditionComponent } from '../../common/property/editor/filter-condition/entity-filter-condition.component';
// import { EntityFilterConditionConverter } from '../../common/property/editor/filter-condition/entity-filter-condition-converter';

export class TreeGridProp extends CollectionProp {

    propertyConfig: ElementPropertyConfig[];


    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        this.propertyConfig = [];
        const viewModelId = this.viewModelId;

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearanceProperties(propertyData, viewModelId);
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorProperties(propertyData, viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        if (propertyData.controlSource === 'Farris') {
            // 图标属性
            const iconPropConfig = this.getIconProperties(propertyData, viewModelId);
            this.propertyConfig.push(iconPropConfig);

            // 分页属性
            const paginationPropertyConfig = this.getPaginationCofig(propertyData, viewModelId);
            this.propertyConfig.push(paginationPropertyConfig);
        }

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, viewModelId);
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }
    private getBasicPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '组件的id',
                    readonly: true
                },
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件的类型',
                    iterator: [{ key: propertyData.type, value: DgControl.TreeGrid.name }],
                },
                {
                    propertyID: 'controlSource',
                    propertyName: '控件来源',
                    propertyType: 'string',
                    readonly: true,
                    visible: propertyData.controlSource !== 'Farris'
                },
                {
                    propertyID: 'dataSource',
                    propertyName: '数据源',
                    propertyType: 'modal',
                    description: '数据来源'
                }
            ],
        };
    }

    private getAppearanceProperties(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const appearanceProperties: PropertyEntity[] = this.getCommonAppearanceProperties();
        appearanceProperties.push(
            {
                propertyID: 'fields',
                propertyName: '列',
                propertyType: 'modal',
                description: '列设置',
                editor: GridFieldEditorComponent,
                editorParams: {
                    fieldEditable: propertyData.fieldEditable,
                    viewModelId,
                    gridType: propertyData.type,
                    controlSource: propertyData.controlSource
                },
                converter: new GridFieldConverter()
            },
            {
                propertyID: 'fit',
                propertyName: '填充',
                propertyType: 'boolean',
                description: '是否填充'
            },
            {
                propertyID: 'showBorder',
                propertyName: '显示边框',
                propertyType: 'boolean',
                description: '是否显示边框',
                defaultValue: false
            },
            {
                propertyID: 'fitColumns',
                propertyName: '填充列宽',
                propertyType: 'boolean',
                description: '根据列比例自动填充表格宽度，不会出现滚动条。请先关闭【自动列宽】，再启用此属性！',
                readonly: propertyData.autoWidth
            },
            {
                propertyID: 'autoWidth',
                propertyName: '自动列宽',
                propertyType: 'boolean',
                description: '根据单元格内容自动调整列宽，超出表格宽度后出现滚动条。请先关闭【填充列宽】【表头列允许拖动】和【启用多表头】，再启用此属性！',
                readonly: propertyData.fitColumns || propertyData.resizableColumns || propertyData.enableHeaderGroup,
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'showHeader',
                propertyName: '显示表头',
                propertyType: 'boolean',
                description: '是否显示表头',
                defaultValue: false
            },
            {
                propertyID: 'striped',
                propertyName: '斑马线效果',
                propertyType: 'boolean',
                description: '是否显示斑马线',
                defaultValue: true
            },
            {
                propertyID: 'styler',
                propertyName: '行样式',
                propertyType: 'modal',
                description: '行样式设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'javascript',
                    exampleCode: '(rowData) => {\r\n    return {\r\n        style: {\r\n            color: \'#5A8129\',\r\n' +
                        '            background: \'#CCE7A4\'\r\n        }\r\n    };\r\n}\r\n\r\n注： rowData 行数据'
                },
            }
        );

        if (propertyData.controlSource === 'Primeng') {
            // primeng支持显示的属性
            const primengProps = ['appearance', 'size', 'fields'];
            appearanceProperties.map(p => {
                if (!primengProps.includes(p.propertyID)) {
                    p.visible = false;
                }
            });
        }

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: appearanceProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'fields': {
                        if (propertyData.enableHeaderGroup) {
                            self.notifyService.info('修改显示列后请重新配置多表头');
                        }
                        break;
                    }
                    case 'autoWidth': {
                        const fitColumns = this.properties.find(p => p.propertyID === 'fitColumns');
                        if (fitColumns) {
                            fitColumns.readonly = changeObject.propertyValue;
                        }
                        const behavior = self.propertyConfig.find(c => c.categoryId === 'behavior');
                        if (behavior && behavior.properties) {
                            const resizableColumns = behavior.properties.find(p => p.propertyID === 'resizableColumns');
                            if (resizableColumns) {
                                resizableColumns.readonly = changeObject.propertyValue;
                            }
                            const enableHeaderGroup = behavior.properties.find(p => p.propertyID === 'enableHeaderGroup');
                            if (enableHeaderGroup) {
                                enableHeaderGroup.readonly = changeObject.propertyValue;
                            }
                        }
                        if (changeObject.propertyValue) {
                            propertyData.fitColumns = false;
                            propertyData.resizableColumns = false;
                            propertyData.enableHeaderGroup = false;
                            self.notifyService.warning('启用自动列宽后，填充列宽失效，不允许拖动表头列，不允许启用多表头！');
                        }
                        break;
                    }
                    case 'fitColumns': {
                        const autoWidth = this.properties.find(p => p.propertyID === 'autoWidth');
                        if (autoWidth) {
                            autoWidth.readonly = changeObject.propertyValue || propertyData.resizableColumns || propertyData.enableHeaderGroup;
                        }
                        if (changeObject.propertyValue) {
                            propertyData.autoWidth = false;
                            self.notifyService.warning('启用填充列宽后，自动列宽失效！');
                        }
                        break;
                    }
                }
            }
        };
    }

    private getBehaviorProperties(propertyData: any, viewModelId: string) {
        const self = this;
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        const viewModel = this.domService.getViewModelById(viewModelId);
        const behaviorProperties: PropertyEntity[] = [
            visibleProp,
            {
                propertyID: 'multiSelect',
                propertyName: '允许多选',
                propertyType: propertyData.controlSource === 'Primeng' ? 'boolean' : 'unity',
                description: '是否允许多选',
                defaultValue: false,
                readonly: propertyData.controlSource === 'Primeng',
                editorParams: {
                    controlName: UniformEditorDataUtil.getControlName(propertyData),
                    constType: 'enum',
                    editorOptions: {
                        types: ['const', 'variable'],
                        enums: [{ key: true, value: 'true' }, { key: false, value: 'false' }],
                        variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        newVariableType: 'Boolean',
                        newVariablePrefix: 'is'
                    }
                },
                refreshPanelAfterChanged: true
            },
            {
                propertyID: 'showCheckbox',
                propertyName: '显示复选框',
                propertyType: propertyData.controlSource === 'Primeng' ? 'boolean' : 'unity',
                description: '是否显示复选框',
                defaultValue: false,
                readonly: propertyData.multiSelect === true,
                editorParams: {
                    controlName: UniformEditorDataUtil.getControlName(propertyData),
                    constType: 'enum',
                    editorOptions: {
                        types: ['const', 'variable'],
                        enums: [{ key: true, value: 'true' }, { key: false, value: 'false' }],
                        variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        newVariableType: 'Boolean',
                        newVariablePrefix: 'is'
                    }
                }
            },
            {
                propertyID: 'showAllCheckbox',
                propertyName: '显示全选',
                propertyType: propertyData.controlSource === 'Primeng' ? 'boolean' : 'unity',
                description: '是否显示全选',
                defaultValue: false,
                readonly: !propertyData.multiSelect,
                editorParams: {
                    controlName: UniformEditorDataUtil.getControlName(propertyData),
                    constType: 'enum',
                    editorOptions: {
                        types: ['const', 'variable'],
                        enums: [{ key: true, value: 'true' }, { key: false, value: 'false' }],
                        variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                        newVariableType: 'Boolean',
                        newVariablePrefix: 'is'
                    }
                }
            },
            {
                propertyID: 'checkOnSelect',
                propertyName: '选中当前行并勾选',
                propertyType: 'boolean',
                description: '是否启用选中当前行并勾选',
                visible: propertyData.multiSelect,
                defaultValue: false
            },
            {
                propertyID: 'selectOnCheck',
                propertyName: '勾选并选中当前行',
                propertyType: 'boolean',
                description: '是否启用勾选并选中当前行',
                visible: propertyData.multiSelect,
                defaultValue: false
            },
            {
                propertyID: 'cascadeCheck',
                propertyName: '启用级联选中功能',
                propertyType: 'boolean',
                description: '是否启用级联选中功能',
                defaultValue: true
            },
            {
                propertyID: 'cascadeDown',
                propertyName: '启用向下级联',
                propertyType: 'boolean',
                description: '是否启用向下级联',
                defaultValue: true
            },
            {
                propertyID: 'cascadeUp',
                propertyName: '启用向上级联',
                propertyType: 'boolean',
                description: '是否启用向上级联',
                defaultValue: true
            },
            {
                propertyID: 'keepSelect',
                propertyName: '保持选中状态',
                propertyType: 'boolean',
                description: '选中后再次点击不会取消选中状态',
                defaultValue: true
            }, {
                propertyID: 'onlySelectSelf',
                propertyName: '仅当前行选中',
                propertyType: 'boolean',
                description: '是否仅当前行选中',
                defaultValue: true
            },
            {
                propertyID: 'virtualized',
                propertyName: '启用虚拟加载机制',
                propertyType: 'boolean',
                description: '是否启用虚拟加载机制',
                defaultValue: false
            },
            {
                propertyID: 'udtField',
                propertyName: '分级码字段',
                propertyType: 'select',
                description: '分级码字段设置',
                iterator: this.schemaService.getTreeGridUdtFields(viewModelId)
            },
            {
                propertyID: 'expandLevel',
                propertyName: '展开层次',
                propertyType: 'number',
                description: '树节点展开层次：-1不展开；0全部展开',
                decimals: 0,
                min: -1
            },
            {
                propertyID: 'showLines',
                propertyName: '启用节点连接线',
                propertyType: 'boolean',
                description: '是否启用节点连接线',
                defaultValue: false
            },
            {
                propertyID: 'showLinesOnHover',
                propertyName: '节点连接线显示方式',
                propertyType: 'select',
                description: '节点连接线显示方式选择',
                defaultValue: false,
                visible: propertyData.showLines,
                iterator: [{ key: true, value: '鼠标滑过' }, { key: false, value: '一直显示' }]
            },
            {
                propertyID: 'nowrap',
                propertyName: '数据禁止折行',
                propertyType: 'boolean',
                description: '数据是否禁止折行',
                defaultValue: true
            },
            {
                propertyID: 'resizableColumns',
                propertyName: '表头列允许拖动',
                propertyType: 'boolean',
                description: '表头列是否允许拖动。请先关闭【填充列宽】，再启用此属性！',
                defaultValue: true,
                refreshPanelAfterChanged: true,
                readonly: propertyData.autoWidth
            },
            {
                propertyID: 'headerWrap',
                propertyName: '允许表头折行',
                propertyType: 'boolean',
                description: '是否允许表头折行',
            },
            {
                propertyID: 'enableHeaderGroup',
                propertyName: '启用多表头',
                propertyType: 'boolean',
                defaultValue: false,
                readonly: propertyData.autoWidth,
                description: '是否启用多表头，请先关闭【自动列宽】，再启用此属性！'
            },
            {
                propertyID: 'headerGroup',
                propertyName: '多表头设置',
                propertyType: 'modal',
                editor: ColumnGroupEditorComponent,
                editorParams: {
                    fields: propertyData.fields
                },
                visible: !propertyData.autoWidth && propertyData.enableHeaderGroup
            },
            // {
            //     propertyID: 'conditions',
            //     propertyName: '过滤条件',
            //     propertyType: 'modal',
            //     description: '列表过滤条件设置',
            //     editor: EntityFilterConditionComponent,
            //     converter: new EntityFilterConditionConverter(),
            //     editorParams: {
            //         viewModelId
            //     },
            //     visible: propertyData.controlSource === 'Farris' && viewModel.bindTo === '/'
            // }
        ];

        if (propertyData.controlSource === 'Primeng') {
            this.handlePrimeNGProps(behaviorProperties, propertyData);
        }

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: behaviorProperties,
            setPropertyRelates(changeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'showLines':
                        this.properties.forEach(p => {
                            if (p.propertyID === 'showLinesOnHover') {
                                p.visible = changeObject.propertyValue;
                                data.showLinesOnHover = false;
                            }
                        });
                        break;
                    case 'multiSelect': {
                        this.properties.forEach(p => {
                            if (p.propertyID === 'checkOnSelect' || p.propertyID === 'selectOnCheck') {
                                p.visible = !!changeObject.propertyValue;
                            }
                            // 多选时必须显示复选框并且不可勾掉
                            if (p.propertyID === 'showCheckbox') {
                                p.readonly = changeObject.propertyValue === true;
                                if (p.readonly) {
                                    propertyData.showCheckbox = true;
                                }
                            }
                            // 不启用多选时必须隐藏全选
                            if (p.propertyID === 'showAllCheckbox') {
                                p.readonly = !changeObject.propertyValue;
                                if (p.readonly) {
                                    propertyData.showAllCheckbox = false;
                                }
                            }
                        });

                        break;
                    }
                    case 'nowrap': {
                        if (!changeObject.propertyValue) {
                            self.notifyService.info('启用数据折行后自动取消虚拟加载');
                            propertyData.virtualized = false;
                        }
                        break;
                    }
                    case 'virtualized': {
                        if (changeObject.propertyValue && !propertyData.nowrap) {
                            self.notifyService.info('启用虚拟加载后自动取消数据折行');
                            propertyData.nowrap = true;
                        }
                        break;
                    }
                    case 'enableHeaderGroup': {
                        const headerGroup = this.properties.find(p => p.propertyID === 'headerGroup');
                        if (headerGroup) {
                            headerGroup.visible = !propertyData.autoWidth && changeObject.propertyValue;
                        }
                        // farris 列表启用多表头后 取消所有列的固定列属性
                        if (propertyData.controlSource === 'Farris' && changeObject.propertyValue) {
                            self.notifyService.info('启用多表头后自动取消固定列');
                            propertyData.fields.forEach(field => {
                                field.frozen = false;
                            });
                        }
                        // farris 列表启用多表头后 禁用填充列宽属性
                        if (propertyData.controlSource === 'Farris') {
                            if (changeObject.propertyValue) {
                                self.notifyService.info('启用多表头后自动取消自动列宽');
                                propertyData.autoWidth = false;
                            }
                            const appearance = self.propertyConfig.find(c => c.categoryId === 'appearance');
                            if (appearance && appearance.properties) {
                                const autoWidth = appearance.properties.find(p => p.propertyID === 'autoWidth');
                                if (autoWidth) {
                                    autoWidth.readonly = changeObject.propertyValue;
                                }
                            }
                        }
                        break;
                    }
                    case 'resizableColumns': {
                        const appearance = self.propertyConfig.find(c => c.categoryId === 'appearance');
                        if (appearance && appearance.properties) {
                            const autoWidth = appearance.properties.find(p => p.propertyID === 'autoWidth');
                            if (autoWidth) {
                                autoWidth.readonly = propertyData.fitColumns || propertyData.resizableColumns || propertyData.enableHeaderGroup;
                            }
                        }

                        if (changeObject.propertyValue) {
                            propertyData.autoWidth = false;

                            self.notifyService.warning('启用表头拖动后，填充列宽失效！');
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
        };
    }

    private getEventPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        let eventList = [
            {
                label: 'onSelectionChange',
                name: '行切换事件'
            }
        ];
        eventList = self.switchEvents(propertyData, eventList);
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList, self.switchEvents),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, parameters['events'], parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, parameters['events'], self.switchEvents);
            }
        };
    }

    private switchEvents(propertyData, eventList) {
        let events = [
            {
                label: 'expandNode',
                name: '节点展开事件'
            },
            {
                label: 'beforeClickNode',
                name: '选中前事件'
            },
            {
                label: 'nodeUnSelected',
                name: '取消选中事件'
            },
            {
                label: 'nodeChecked',
                name: '勾选事件'
            },
            {
                label: 'nodeUnChecked',
                name: '取消勾选事件'
            },
            {
                label: 'beforeCheckAll',
                name: '全选前事件'
            },
            {
                label: 'checkAll',
                name: '全选事件'
            },
            {
                label: 'unCheckAll',
                name: '取消全选'
            },
            {
                label: 'beforeUnCheckAll',
                name: '取消全选前事件'
            },
            {
                label: 'pageChange',
                name: '切换页码事件'
            },
            {
                label: 'pageSizeChanged',
                name: '分页条数变化事件'
            },
            {
                label: 'childsPageChanged',
                name: '子级节点分页事件'
            }];
        if (propertyData.controlSource === 'Farris') {
            const eventListExist = eventList.find(eventListItem => eventListItem.label == 'expandNode')
            if (eventListExist == undefined) {
                events.forEach(event => { eventList.push(cloneDeep(event)); })
            }
        } else {
            events.forEach(event => {
                eventList = eventList.filter(eventListItem => {
                    if (eventListItem.label != event.label) {
                        return true;
                    }
                });
            });
        }
        return eventList;
    }

    private getIconProperties(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const customProperties: PropertyEntity[] = [
            {
                propertyID: 'showIcon',
                propertyName: '启用图标',
                propertyType: 'boolean',
                defaultValue: false
            },
            {
                propertyID: 'useIconFromData',
                propertyName: '图标来源',
                propertyType: 'select',
                iterator: [{ key: true, value: '动态设置' }, { key: false, value: '固定设置' }],
                defaultValue: false,
                visible: propertyData.showIcon
            },
            {
                propertyID: 'leafIcon',
                propertyName: '叶子图标',
                propertyType: propertyData.useIconFromData ? 'modal' : 'string',
                editor: GroupFieldEditorComponent,
                editorParams: {
                    viewModelId,
                    selectNumber: 1,
                    resultType: 'string'
                },
                converter: new GroupFieldConverter(),
                visible: propertyData.showIcon
            },
            {
                propertyID: 'expandIcon',
                propertyName: '展开图标',
                propertyType: propertyData.useIconFromData ? 'modal' : 'string',
                editor: GroupFieldEditorComponent,
                editorParams: {
                    viewModelId,
                    selectNumber: 1,
                    resultType: 'string'
                },
                converter: new GroupFieldConverter(),
                visible: propertyData.showIcon
            },
            {
                propertyID: 'collapseIcon',
                propertyName: '折叠图标',
                propertyType: propertyData.useIconFromData ? 'modal' : 'string',
                editor: GroupFieldEditorComponent,
                editorParams: {
                    viewModelId,
                    selectNumber: 1,
                    resultType: 'string'
                },
                converter: new GroupFieldConverter(),
                visible: propertyData.showIcon
            }
        ];
        return {
            categoryId: 'icon',
            categoryName: '图标',
            properties: customProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'showIcon': {
                        this.properties.forEach(p => {
                            if (p.propertyID !== 'showIcon') {
                                p.visible = changeObject.propertyValue;
                            }
                        });
                        break;
                    }
                    case 'useIconFromData': {
                        this.properties.forEach(p => {
                            if ('leafIcon,expandIcon,collapseIcon'.includes(p.propertyID)) {
                                p.propertyType = changeObject.propertyValue ? 'modal' : 'string';
                                propertyData[p.propertyID] = null;
                            }
                        });
                    }
                }
            }

        };
    }

    private handlePrimeNGProps(properties: PropertyEntity[], propertyData: any) {
        // primeng支持显示的属性
        const primengProps = ['udtField', 'resizableColumns', 'idField', 'enableHeaderGroup', 'headerGroup'];
        properties.map(p => {
            if (!primengProps.includes(p.propertyID)) {
                p.visible = false;
            }
        });

    }

    /**
     * 分页属性（子表暂不支持）
     */
    private getPaginationCofig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const viewModel = this.domService.getViewModelById(viewModelId);

        if (!viewModel.pagination) {
            viewModel.pagination = {
                enable: false
            };
        }
        const vmPagination = viewModel.pagination;
        // 这三个属性是存在vm中，但是需要在treegrid控件上编辑
        propertyData.pagination = vmPagination.enable;
        propertyData.pageList = vmPagination.pageList;
        propertyData.pageSize = vmPagination.pageSize;

        const pageSizeIterator = this.getPageSizeIterator(propertyData.pageList);
        return {
            categoryId: 'pagination',
            categoryName: '分页',
            properties: [
                {
                    propertyID: 'pagination',
                    propertyName: '启用分页',
                    propertyType: 'boolean',
                    defaultValue: false,
                    readonly: viewModel.bindTo !== '/'
                },
                {
                    propertyID: 'showPageSize',
                    propertyName: '显示分页条数',
                    propertyType: 'boolean',
                    defaultValue: false,
                    visible: propertyData.pagination
                },
                {
                    propertyID: 'pageList',
                    propertyName: '分页条数',
                    propertyType: 'string',
                    description: '请输入以英文逗号分开的数字，最大值为1000。例如：10,20,30,50,100',
                    visible: propertyData.pagination && propertyData.showPageSize,
                    notAllowedChars: [/[^\d,]/g]
                },
                {
                    propertyID: 'pageSize',
                    propertyName: '默认分页条数',
                    propertyType: 'select',
                    iterator: pageSizeIterator,
                    visible: propertyData.pagination
                },
                {
                    propertyID: 'showGotoInput',
                    propertyName: '显示页码输入框',
                    propertyType: 'boolean',
                    description: '是否显示页码输入框',
                    visible: propertyData.pagination,
                    defaultValue: false
                },
                {
                    propertyID: 'paginationForChildren',
                    propertyName: '启用分页加载子级节点',
                    propertyType: 'boolean',
                    description: '是否启用分页加载子级节点',
                    defaultValue: false
                },
                {
                    propertyID: 'showTotalForChildren',
                    propertyName: '显示子级分页总记录数',
                    propertyType: 'boolean',
                    description: '是否显示子级分页总记录数',
                    defaultValue: false,
                    visible: propertyData.paginationForChildren
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'pagination': {
                        this.properties.map(p => {
                            if (!['pagination', 'paginationForChildren', 'showTotalForChildren'].includes(p.propertyID)) {
                                p.visible = changeObject.propertyValue;
                            }
                            if (p.propertyID === 'pageList') {
                                p.visible = changeObject.propertyValue && propertyData.showPageSize;
                            }
                        });
                        vmPagination.enable = changeObject.propertyValue;
                        if (changeObject.propertyValue) {
                            if (!propertyData.pageSize) {
                                propertyData.pageSize = 20;
                                vmPagination.pageSize = 20;
                            }
                            self.setDefaultPageList(propertyData, vmPagination);
                            const pageSize = this.properties.find(p => p.propertyID === 'pageSize');
                            if (pageSize) {
                                pageSize.iterator = self.getPageSizeIterator(propertyData.pageList);
                            }
                        }
                        break;
                    }
                    case 'pageList': {
                        vmPagination.pageList = changeObject.propertyValue;
                        const pageSize = this.properties.find(p => p.propertyID === 'pageSize');
                        if (pageSize) {
                            pageSize.iterator = self.getPageSizeIterator(changeObject.propertyValue);
                        }
                        if (!changeObject.propertyValue.includes(propertyData.pageSize)) {
                            vmPagination.pageSize = propertyData.pageSize = null;
                        }
                        break;
                    }
                    case 'pageSize': {
                        vmPagination.pageSize = changeObject.propertyValue;
                        break;
                    }
                    case 'showPageSize': {
                        const pageList = this.properties.find(p => p.propertyID === 'pageList');
                        if (pageList) {
                            pageList.visible = changeObject.propertyValue;
                        }

                        if (changeObject.propertyValue) {
                            self.setDefaultPageList(propertyData, vmPagination);
                            const pageSize = this.properties.find(p => p.propertyID === 'pageSize');
                            if (pageSize) {
                                pageSize.iterator = self.getPageSizeIterator(propertyData.pageList);
                            }
                        }
                        break;

                    }
                    case 'paginationForChildren': {
                        const showTotalForChildren = this.properties.find(p => p.propertyID === 'showTotalForChildren');
                        if (showTotalForChildren) {
                            showTotalForChildren.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                }
            }

        };
    }

    private setDefaultPageList(propertyData: any, vmPagination: any) {

        if (!propertyData.pageList) {
            propertyData.pageList = '10,20,30,50,100';
            if (propertyData.pageSize && !propertyData.pageList.includes(propertyData.pageSize)) {
                let pageList = [10, 20, 30, 50, 100, propertyData.pageSize];
                pageList = pageList.sort((A, B) => A - B);
                propertyData.pageList = pageList.toString();
            }

            vmPagination.pageList = propertyData.pageList;
        }
    }


    getPageSizeIterator(pageList: string) {
        if (!pageList) {
            pageList = '10,20,30,50,100';
        }
        const iterator = [];
        pageList.split(',').forEach(pageSize => {
            iterator.push({
                key: Number.parseInt(pageSize, 10),
                value: pageSize
            });
        });
        return iterator;
    }
}
