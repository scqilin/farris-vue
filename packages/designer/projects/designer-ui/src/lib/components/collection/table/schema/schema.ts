
export const TableSchema = {
    id: 'table',
    type: 'Table',
    appearance: {
        class: 'table table-bordered f-table-has-form'
    },
    size: null,
    border: {
        width: 1,
        enableColor: false,
        color: '#eaecf3'
    },
    font: {
        size: 13,
        enableColor: false,
        color: '#2d2f33',
        family: 'sans-serif',
    },
    rows: [],
    visible: true
};

export const TableTdSchema = {
    id: '',
    type: 'TableTd',
    tdType: 'editor',
    staticText: {
        text: '',
        require: false
    },
    editor: {
        type: null,
        binding: null
    },
    colspan: 1,
    rowspan: 1,
    align: 'right',
    valign: 'top',
    appearance: {
        class: 'farris-group-wrap--input',
        style: ''
    },
    size: null

};

