
export default (ctx: any) => {
  const require = ctx.component.editor && ctx.component.editor.require ? '<span class="text-danger">*</span>' : '';

  // 绑定字段失效提示信息
  const inValidBindingTips = getInValidBindingTipsAfterLabel(ctx);
  const labelTitle = inValidBindingTips ? '绑定信息已失效，请手动移除列' : '';
  const labelColor = inValidBindingTips ? ` style='color:red;'` : '';

  const fieldWidth = ctx.component.size && ctx.component.size.width ? ctx.component.size.width : 120;
  return `
  <div  ref="${ctx.component.id}" style="width: ${fieldWidth}px" class="header-cell ${ctx.className}" class="drag-container" dragref="${ctx.component.id}-container">
    <div class="caption" title=${labelTitle} ${labelColor}>${inValidBindingTips}${require} ${ctx.component.caption} </div>
  </div>`;
};

/** 绑定字段失效提示，用于卡片内的输入控件 */
function getInValidBindingTipsAfterLabel(ctx: any) {
  if (ctx.isValidBinding) {
    return '';
  }
  return `<span class="f-icon f-icon-warning mr-1"  style="color: red;"></span>`;
}

