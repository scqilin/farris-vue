import { TreeGridSchema, TreeGridFieldSchema } from './schema/treegrid';
import { ComponentExportEntity } from '@farris/designer-element';
import FdTreeGridTemplates from '../common/templates/grid';
import FdTreeGridComponent from './component/fd-treegrid';
import FdTreeGridFieldTemplates from '../common/templates/grid-field';
import FdTreeGridFieldComponent from './component/treegrid-field/component/fd-treegrid-field';


export const TreeGrid: ComponentExportEntity = {
    type: 'TreeGrid',
    component: FdTreeGridComponent,
    template: FdTreeGridTemplates,
    metadata: TreeGridSchema
};

export const TreeGridField: ComponentExportEntity = {
    type: 'TreeGridField',
    component: FdTreeGridFieldComponent,
    metadata: TreeGridFieldSchema,
    template: FdTreeGridFieldTemplates,
};
