import FdDataGridFieldComponent from '../../../../datagrid/component/datagrid-field/component/fd-datagrid-field';
import { TreeGridFieldSchema } from '../../../schema/treegrid';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { TreeGridFieldProp } from '../property/property-config';
import { DomService } from '@farris/designer-services';

export default class FdTreeGridFieldComponent extends FdDataGridFieldComponent {

    getDefaultSchema(): any {
        return TreeGridFieldSchema;
    }

    render(): any {
        this.init();
        this.checkBindingFormFieldValidation();
        return this.renderTemplate('TreeGridField', {
            component: this.component,
            focusedFieldId: this.focusedFieldId,
            className: this.getSelectedClassName(),
            isValidBinding: this.isValidBinding
        });
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options && this.options.designerHost;
        const prop: TreeGridFieldProp = new TreeGridFieldProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component, this.parentGridInstance.component);
        return propertyConfig;
    }

    /**
     * 设置列的展示名称和路径，用于交互面板已绑定事件窗口
     */
    setComponentBasicInfoMap() {
        const domService = this.options.designerHost.getService('DomService') as DomService;

        // 列的展示名称和路径
        domService.controlBasicInfoMap.set(this.component.id, {
            showName: this.component.caption,
            parentPathName: `树表格 > ${this.component.caption}`
        });

        // 列编辑器的展示名称及路径
        if (this.component.editor) {
            domService.controlBasicInfoMap.set(this.component.editor.id, {
                showName: this.component.caption,
                parentPathName: `树表格 > ${this.component.caption}`
            });
        }
    }
}
