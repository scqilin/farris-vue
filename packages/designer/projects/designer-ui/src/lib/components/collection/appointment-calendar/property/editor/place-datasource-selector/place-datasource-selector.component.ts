import {
    Component, OnInit, Output, Input, EventEmitter, ViewChild, TemplateRef, HostBinding, NgZone, OnDestroy
} from '@angular/core';
import { NotifyService } from '@farris/ui-notify';
import { FormBasicService } from '@farris/designer-services';

@Component({
    selector: 'app-place-datasource-selector',
    templateUrl: './place-datasource-selector.component.html'
})
export class PlaceDataSourceSelectorComponent implements OnInit, OnDestroy {
    @Output() closeModal = new EventEmitter<any>();
    @Output() submitModal = new EventEmitter<any>();
    @Input() value;
    @Input() editorParams = { placeDataSource: { id: '', name: '' }, placeDataSourceUrl: '', placeDataSourceUrlType: '' };

    @ViewChild('footer') modalFooter: TemplateRef<any>;
    modalConfig = {
        title: '地点数据源选择器',
        width: 1000,
        height: 700,
        showButtons: false,
        showMaxButton: true
    };

    /** 选择元数据的组件需要的入参 */
    selectorEditorParams;

    constructor(
        private formBasicServ: FormBasicService, private notifyService: NotifyService) { }

    ngOnInit(): void {
        this.selectorEditorParams = {
            relativePath: this.formBasicServ.formMetaBasicInfo.relativePath,
            range: 'Su',
            source: 'vo'
        };
    }

    ngOnDestroy(): void {

    }
    afterSelectMatadata(e) {
        if (!e.metadata) {
            return;
        }
        // let placeDataSource = this.editorParams.placeDataSource ? cloneDeep(this.editorParams.placeDataSource) : null;
        const placeDataSource = {
            voId: e.metadata.id,
            voName: e.metadata.name
        };

        const paths = e.metadata.relativePath.split('/');
        this.editorParams.placeDataSourceUrl = `/api/${paths[0]}/${paths[1]}/v1.0/${e.metadata.code}`.toLowerCase();
        this.editorParams.placeDataSourceUrlType = 'GET';

        this.submitModal.emit({ value: placeDataSource });
    }

    afterCancelMetadata() {

        this.closeModal.emit();
    }
}
