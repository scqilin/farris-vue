import { CollectionProp } from '../../common/property/collection-property-config';
import { ElementPropertyConfig, PropertyEntity } from '@farris/ide-property-panel';
import { DgControl } from '../../../../utils/dg-control';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { UniformEditorDataUtil } from '@farris/designer-services';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { CollectionWithPropertyConverter, CollectionWithPropertyEditorComponent } from '@farris/designer-devkit';
import { GridContextMenuProp } from './context-menu-property-config';
import { GridFieldEditorComponent } from '../../common/property/editor/grid-field-editor/grid-field-editor.component';
import { GridFieldConverter } from '../../common/property/editor/grid-field-editor/grid-field-converter';
import { GroupFieldEditorComponent } from '../../common/property/editor/group-field-editor/group-field-editor.component';
import { GroupFieldConverter } from '../../common/property/editor/group-field-editor/group-field-converter';
import { ColumnGroupEditorComponent } from '@farris/header-group-editor';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
// import { EntityFilterConditionComponent } from '../../common/property/editor/filter-condition/entity-filter-condition.component';
// import { EntityFilterConditionConverter } from '../../common/property/editor/filter-condition/entity-filter-condition-converter';

export class DataGridProp extends CollectionProp {

    propertyConfig: ElementPropertyConfig[];

    getPropConfig(propertyData: any): ElementPropertyConfig[] {

        this.propertyConfig = [];
        const viewModelId = this.viewModelId;

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, viewModelId);
        this.propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearanceProperties(propertyData, viewModelId);
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorProperties(propertyData, viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 分组属性
        const groupPropConfig = this.getGroupProperties(propertyData, viewModelId);
        this.propertyConfig.push(groupPropConfig);

        if (propertyData.controlSource === 'Farris') {
            // 操作列属性
            const operationPropConfig = this.getOperationProperties(propertyData, viewModelId);
            this.propertyConfig.push(operationPropConfig);

            // 合计属性
            const footerPropConfig = this.getFooterPropConfig(propertyData, viewModelId);
            this.propertyConfig.push(footerPropConfig);

            // 过滤行属性
            const filterRowProperties = this.getFilterRowConfig(propertyData, viewModelId);
            this.propertyConfig.push(filterRowProperties);

            // 分页属性
            const paginationPropertyConfig = this.getPaginationCofig(propertyData, viewModelId);
            this.propertyConfig.push(paginationPropertyConfig);

            // 右键菜单
            const contextMenuPropertyConfig = this.getContextMenuCofig(propertyData, viewModelId);
            this.propertyConfig.push(contextMenuPropertyConfig);
        }

        // 事件属性
        const eventPropConfig = this.getEventPropConfig(propertyData, viewModelId);
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;

    }

    private getBasicPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '组件的id',
                    readonly: true
                },
                {
                    propertyID: 'type',
                    propertyName: '控件类型',
                    propertyType: 'select',
                    description: '组件类型',
                    iterator: [{ key: propertyData.type, value: DgControl.DataGrid.name }],
                },
                {
                    propertyID: 'controlSource',
                    propertyName: '控件来源',
                    propertyType: 'select',
                    description: '组件的控件来源',
                    iterator: [{ key: 'Kendo', value: 'Kendo' }, { key: 'Farris', value: 'Farris' }],
                    refreshPanelAfterChanged: true,
                    visible: propertyData.controlSource !== 'Farris'
                },
                {
                    propertyID: 'dataSource',
                    propertyName: '数据源',
                    propertyType: 'modal',
                    description: '数据来源'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, prop, parameters) {

                switch (changeObject && changeObject.propertyID) {
                    case 'controlSource': {
                        this.properties.forEach(p => {
                            if (p.controlSource) {
                                p.visible = changeObject.propertyValue === p.controlSource;
                            }
                        });

                        propertyData.fields.forEach(field => {
                            field.controlSource = changeObject.propertyValue;

                            // 切换frozen的值
                            if (changeObject.propertyValue === 'Kendo') {
                                if (field.frozen === 'right') {
                                    field.frozen = true;
                                } else {
                                    field.frozen = false;
                                }
                            }
                        });


                        // 变更【外观分类】下的属性
                        const newAppearancePropConfig = self.getAppearanceProperties(propertyData, viewModelId);
                        const currentAppearanceConfig = self.propertyConfig.find(cat => cat.categoryId === 'appearance');
                        Object.assign(currentAppearanceConfig, newAppearancePropConfig);

                        // 变更【行为分类】下的属性
                        const newBehaviorPropConfig = self.getBehaviorProperties(propertyData, viewModelId);
                        const currentBehaviorConfig = self.propertyConfig.find(cat => cat.categoryId === 'behavior');
                        Object.assign(currentBehaviorConfig, newBehaviorPropConfig);

                        // 变更【操作列分类】的隐现
                        const operationConfig = self.propertyConfig.find(cat => cat.categoryId === 'operation');
                        if (operationConfig) { operationConfig.hide = changeObject.propertyValue !== 'Farris'; }

                        // 变更【分组分类】下的属性
                        const newGroupPropConfig = self.getGroupProperties(propertyData, viewModelId);
                        const currentGroupConfig = self.propertyConfig.find(cat => cat.categoryId === 'gridGroup');
                        Object.assign(currentGroupConfig, newGroupPropConfig);

                        // 变更【合计分类】的显隐
                        const footerConfig = self.propertyConfig.find(cat => cat.categoryId === 'footer');
                        if (footerConfig) { footerConfig.hide = changeObject.propertyValue !== 'Farris'; }

                        // 变更【过滤行分类】的显隐
                        const filterRowConfig = self.propertyConfig.find(cat => cat.categoryId === 'filterRow');
                        if (filterRowConfig) { filterRowConfig.hide = changeObject.propertyValue !== 'Farris'; }

                        // 变更【分页分类】的显隐
                        const paginationConfig = self.propertyConfig.find(cat => cat.categoryId === 'pagination');
                        if (paginationConfig) { paginationConfig.hide = changeObject.propertyValue !== 'Farris'; }

                        // 变更【事件分类】的显隐
                        const newEventPropConfig = self.getEventPropConfig(propertyData, viewModelId);
                        const currentEventConfig = self.propertyConfig.find(cat => cat.categoryId === 'commands');
                        Object.assign(currentEventConfig, newEventPropConfig);

                        // 变更【右键菜单分类】的显隐
                        const contextMenuConfig = self.propertyConfig.find(cat => cat.categoryId === 'contextMenu');
                        if (contextMenuConfig) { contextMenuConfig.hide = changeObject.propertyValue !== 'Farris'; }
                    }
                }
            }
        };
    }

    private getAppearanceProperties(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const props = this.getCommonAppearanceProperties();
        props.push(
            {
                propertyID: 'fields',
                propertyName: '列',
                propertyType: 'modal',
                description: '列设置',
                editor: GridFieldEditorComponent,
                editorParams: {
                    fieldEditable: propertyData.fieldEditable,
                    viewModelId,
                    gridType: propertyData.type,
                    controlSource: propertyData.controlSource
                },
                converter: new GridFieldConverter()
            },
            {
                propertyID: 'showBorder',
                propertyName: '显示线条',
                propertyType: 'boolean',
                description: '是否显示线条',
                defaultValue: false
            },
            {
                propertyID: 'showLineNumber',
                propertyName: '显示行号',
                propertyType: 'boolean',
                description: '是否显示行号',
                defaultValue: false
            },
            {
                propertyID: 'lineNumberTitle',
                propertyName: '行号标题',
                propertyType: 'string',
                description: '行号标题设置',
                visible: propertyData.showLineNumber
            },
            {
                propertyID: 'lineNumberWidth',
                propertyName: '行号列宽',
                propertyType: 'number',
                description: '行号列宽设置',
                min: 0,
                max: 100,
                visible: propertyData.showLineNumber
            },
            {
                propertyID: 'striped',
                propertyName: '显示斑马线',
                propertyType: 'boolean',
                description: '是否显示斑马线',
                visible: propertyData.controlSource === 'Farris',
                defaultValue: true,
                controlSource: 'Farris'
            },
            {
                propertyID: 'rowHeight',
                propertyName: '行高',
                propertyType: 'number',
                description: '行高设置',
                visible: propertyData.controlSource === 'Farris',
                min: 30,
                max: 200
            },
            {
                propertyID: 'fitColumns',
                propertyName: '填充列宽',
                propertyType: 'boolean',
                description: '根据列比例自动填充表格宽度，不会出现滚动条。',
                visible: propertyData.controlSource === 'Farris',
                defaultValue: false,
                controlSource: 'Farris'
            },
            {
                propertyID: 'autoFitColumns',
                propertyName: '自适应表格宽度',
                propertyType: 'boolean',
                description: '表格是否自适应宽度',
                visible: propertyData.controlSource === 'Farris' && propertyData.fitColumns,
                defaultValue: false,
                controlSource: 'Farris'
            },
            {
                propertyID: 'enableEditCellStyle',
                propertyName: '标识可编辑单元格',
                propertyType: 'boolean',
                description: '是否标识可编辑的单元格',
                defaultValue: false
            },
            {
                propertyID: 'enableHighlightCell',
                propertyName: '高亮显示选中单元格',
                propertyType: 'boolean',
                description: '是否高亮显示选中单元格',
            },
            {
                propertyID: 'mergeCell',
                propertyName: '合并单元格',
                propertyType: 'boolean',
                description: '是否合并单元格',
                visible: propertyData.controlSource === 'Farris',
                defaultValue: false,
                refreshPanelAfterChanged: true, // 更新分组属性
                controlSource: 'Farris'
            },
            {
                propertyID: 'mergeFields',
                propertyName: '合并单元格字段',
                propertyType: 'modal',
                description: '合并单元格字段',
                visible: propertyData.controlSource === 'Farris' && propertyData.mergeCell,
                editor: GroupFieldEditorComponent,
                editorParams: {
                    viewModelId,
                },
                converter: new GroupFieldConverter()// 更新分组属性
            },
            {
                propertyID: 'styler',
                propertyName: '行样式',
                propertyType: 'modal',
                description: '行样式设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'javascript',
                    exampleCode: '(rowData) => {\r\n    return {\r\n        style: {\r\n            color: \'#5A8129\',\r\n' +
                        '            background: \'#CCE7A4\'\r\n        }\r\n    };\r\n}\r\n\r\n注： rowData 行数据'
                },
                visible: propertyData.controlSource === 'Farris',
                controlSource: 'Farris'
            },
            {
                propertyID: 'emptyTemplate',
                propertyName: '空数据模板',
                propertyType: 'modal',
                description: '空数据模板设置',
                editor: CodeEditorComponent,
                visible: propertyData.controlSource === 'Farris',
                editorParams: {
                    language: 'html'
                },
                controlSource: 'Farris',
                showClearButton: true
            },
            {
                propertyID: 'autoHeight',
                propertyName: '自动高度',
                propertyType: 'boolean',
                description: '是否自动高度',
                defaultValue: false,
                visible: propertyData.controlSource === 'Farris',
                controlSource: 'Farris'
            },
            {
                propertyID: 'maxHeight',
                propertyName: '最大高度',
                propertyType: 'number',
                description: '启用自动高度时，设置表格的最大高度。值为0时，表示不限制高度。',
                visible: propertyData.controlSource === 'Farris' && propertyData.autoHeight,
                min: 0
            },
            {
                propertyID: 'useBlankWhenDataIsEmpty',
                propertyName: '空数据表格显示空白行',
                propertyType: 'boolean',
                description: '表格没有数据时是否显示空白行',
                visible: propertyData.controlSource === 'Farris',
                controlSource: 'Farris',
                defaultValue: false
            },
            {
                propertyID: 'emptyDataHeight',
                propertyName: '空白行高度',
                propertyType: 'number',
                description: '空数据行高度设置',
                visible: propertyData.controlSource === 'Farris',
                min: 1,
                max: 1000
            },
            {
                propertyID: 'scrollBarShowMode',
                propertyName: '滚动条显示模式',
                propertyType: 'select',
                description: '滚动条显示模式设置',
                visible: propertyData.controlSource === 'Farris',
                iterator: [{ key: 'auto', value: '鼠标划过时显示' }, { key: 'allways', value: '始终显示' }],
                controlSource: 'Farris'
            },
            {
                propertyID: 'showScrollArrow',
                propertyName: '显示滚动条微调按钮',
                propertyType: 'boolean',
                description: '是否显示滚动条微调按钮',
                visible: propertyData.controlSource === 'Farris' && propertyData.scrollBarShowMode === 'allways',
                controlSource: 'Farris',
                defaultValue: false
            }

        );

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: props,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'fields': {
                        if (propertyData.enableHeaderGroup) {
                            self.notifyService.info('修改显示列后请重新配置多表头');
                        }
                        break;
                    }
                    case 'mergeCell': {
                        const mergeFields = this.properties.find(p => p.propertyID === 'mergeFields');
                        if (mergeFields) {
                            mergeFields.visible = changeObject.propertyValue;
                        }
                        if (changeObject.propertyValue) {
                            self.notifyService.info('启用合并单元格后自动取消分组');
                            propertyData.groupable = false;
                            const groupCategoryConfig = self.propertyConfig.find(cat => cat.categoryId === 'gridGroup');
                            if (groupCategoryConfig && groupCategoryConfig.properties) {
                                groupCategoryConfig.properties.forEach(p => {
                                    if (p.propertyID !== 'groupable') {
                                        p.visible = false;
                                    }
                                });
                            }
                        }
                        break;
                    }
                    case 'fitColumns': {
                        const autoFitColumns = this.properties.find(p => p.propertyID === 'autoFitColumns');
                        if (autoFitColumns) {
                            autoFitColumns.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    case 'showLineNumber': {
                        const lineNumber = this.properties.filter(p => p.propertyID === 'lineNumberTitle' ||
                            p.propertyID === 'lineNumberWidth');
                        if (lineNumber && lineNumber.length) {
                            lineNumber.forEach(element => {
                                element.visible = changeObject.propertyValue;
                            });
                        }
                        break;
                    }
                    case 'scrollBarShowMode': {
                        const showScrollArrow = this.properties.find(p => p.propertyID === 'showScrollArrow');
                        if (showScrollArrow) {
                            showScrollArrow.visible = changeObject.propertyValue === 'allways';
                        }
                        break;
                    }
                    case 'autoHeight': {
                        const maxHeight = this.properties.find(p => p.propertyID === 'maxHeight');
                        if (maxHeight) {
                            maxHeight.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
        };

    }

    private getBehaviorProperties(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        const self = this;
        // 补充是否允许为空属性
        const viewModel = this.domService.getViewModelById(viewModelId);
        if (!viewModel.hasOwnProperty('allowEmpty')) {
            viewModel.allowEmpty = true;
        }

        propertyData.allowEmpty = viewModel.allowEmpty;
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'allowEmpty',
                    propertyName: '是否允许为空',
                    propertyType: 'unity',
                    description: '运行时表格数据是否允许为空',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable', 'stateMachine', 'custom'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is',
                            stateMachine: this.stateMachineService.stateMachineMetaData
                        }
                    },
                    visible: propertyData.fieldEditable
                },
                {
                    propertyID: 'useControlPanel',
                    propertyName: '启用列设置',
                    propertyType: 'boolean',
                    description: '是否启用列设置',
                    defaultValue: false,
                    visible: propertyData.controlSource === 'Farris',
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'enableSimpleMode',
                    propertyName: '启用简洁列设置模式',
                    propertyType: 'boolean',
                    description: '列设置是否启用简洁模式',
                    visible: propertyData.useControlPanel,
                    defaultValue: false
                },
                {
                    propertyID: 'enableScheme',
                    propertyName: '启用列设置方案',
                    propertyType: 'boolean',
                    description: '列设置是否启用方案',
                    visible: propertyData.useControlPanel && propertyData.enableSimpleMode,
                    defaultValue: false
                },
                {
                    propertyID: 'multiSelect',
                    propertyName: '允许多选',
                    propertyType: 'unity',
                    description: '是否允许多选',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable', 'stateMachine', 'custom'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is',
                            stateMachine: this.stateMachineService.stateMachineMetaData
                        }
                    }
                },
                {
                    propertyID: 'showCheckbox',
                    propertyName: '显示复选框',
                    propertyType: 'unity',
                    description: '是否显示复选框',
                    visible: propertyData.controlSource === 'Farris',
                    readonly: propertyData.multiSelect === true,
                    controlSource: 'Farris',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable', 'custom'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'

                        },
                    }
                },
                {
                    propertyID: 'showAllCheckbox',
                    propertyName: '显示全选',
                    propertyType: 'unity',
                    description: '是否显示全选',
                    visible: propertyData.controlSource === 'Farris',
                    readonly: !propertyData.multiSelect,
                    controlSource: 'Farris',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable', 'custom'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'

                        }
                    }
                },
                {
                    propertyID: 'selectionMode',
                    propertyName: '选择模式',
                    propertyType: 'select',
                    description: '编辑方式选择',
                    iterator: [{ key: 'default', value: 'Windows模式' }, { key: 'custom', value: '自定义模式' }],
                    visible: propertyData.controlSource === 'Farris' && propertyData.multiSelect,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'checkOnSelect',
                    propertyName: '选中并勾选当前行',
                    propertyType: 'boolean',
                    description: '是否启用选中并勾选当前行',
                    visible: propertyData.multiSelect && propertyData.selectionMode === 'custom',
                    defaultValue: false
                },
                {
                    propertyID: 'selectOnCheck',
                    propertyName: '勾选并选中当前行',
                    propertyType: 'boolean',
                    description: '是否启用勾选并选中当前行',
                    visible: propertyData.multiSelect && propertyData.selectionMode === 'custom',
                    defaultValue: false
                },
                {
                    propertyID: 'keepSelect',
                    propertyName: '保持选中',
                    propertyType: 'boolean',
                    description: '选中后再次点击不会取消选中状态',
                    visible: propertyData.multiSelect && propertyData.selectionMode === 'custom'
                },
                {
                    propertyID: 'enableMorePageSelect',
                    propertyName: '启用跨页多选',
                    propertyType: 'boolean',
                    description: '是否启用跨页多选',
                    visible: propertyData.multiSelect,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'showSelectedList',
                    propertyName: '显示已选记录列表',
                    propertyType: 'boolean',
                    description: '是否显示已选记录列表',
                    visible: propertyData.multiSelect,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'selectedItemFormatter',
                    propertyName: '已选记录格式化',
                    propertyType: 'modal',
                    description: '是否将已选记录格式化',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'javascript',
                        exampleCode: '(rowData) => {\r\n    return rowData[\'id\'] + rowData[\'namd\'][\'dtname\'];\r\n}\r\n'
                    },
                    visible: propertyData.controlSource === 'Farris' && propertyData.multiSelect && propertyData.showSelectedList,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'enableHeaderGroup',
                    propertyName: '启用多表头',
                    propertyType: 'boolean',
                    description: '是否启用多表头',
                    defaultValue: false,
                    visible: propertyData.controlSource === 'Farris',
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'headerGroup',
                    propertyName: '多表头设置',
                    propertyType: 'modal',
                    description: '多表头设置',
                    editor: ColumnGroupEditorComponent,
                    editorParams: {
                        fields: propertyData.fields
                    },
                    visible: propertyData.controlSource === 'Farris' && propertyData.enableHeaderGroup
                },
                {
                    propertyID: 'headerWrap',
                    propertyName: '允许表头折行',
                    propertyType: 'boolean',
                    description: '是否允许表头折行'
                },
                {
                    propertyID: 'AutoColumnWidthUseDblclick',
                    propertyName: '启用双击表头适应内容',
                    propertyType: 'boolean',
                    description: '启用双击表头适应内容',
                    visible: true,
                    defaultValue: true,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'nowrap',
                    propertyName: '数据禁止折行',
                    propertyType: 'boolean',
                    description: '数据是否禁止折行',
                    defaultValue: true,
                    visible: propertyData.controlSource === 'Farris',
                    controlSource: 'Farris'
                },
                // {
                //     propertyID: 'draggable',
                //     propertyName: '允许拖拽布局',
                //     propertyType: 'boolean',
                //     description: '是否允许拖拽的方式改变布局',
                //     defaultValue: false
                // },
                {
                    propertyID: 'enableDragColumn',
                    propertyName: '允许拖拽表头',
                    propertyType: 'boolean',
                    description: '是否允许拖拽表头位置',
                    defaultValue: false,
                    visible: propertyData.controlSource === 'Farris' && !propertyData.enableHeaderGroup
                },
                {
                    propertyID: 'virtualized',
                    propertyName: '启用虚拟加载机制',
                    propertyType: 'boolean',
                    description: '是否启用虚拟加载机制',
                    defaultValue: false,
                    visible: propertyData.controlSource === 'Farris',
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'virtualizedAsyncLoad',
                    propertyName: '启用异步加载数据',
                    propertyType: 'boolean',
                    description: '是否启用异步加载数据',
                    visible: propertyData.controlSource === 'Farris',
                    defaultValue: false,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'idField',
                    propertyName: '主键字段',
                    propertyType: 'string',
                    description: '主键字段设置',
                    defaultValue: 'id',
                    visible: propertyData.controlSource === 'Farris',
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'editMode',
                    propertyName: '编辑方式',
                    propertyType: 'select',
                    description: '编辑方式选择',
                    // iterator: [{ key: 'cell', value: '编辑单元格' }, { key: 'row', value: '行内编辑' }],
                    iterator: [{ key: 'cell', value: '编辑单元格' }],
                    visible: propertyData.controlSource === 'Farris',
                    defaultValue: 'cell',
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'disableRow',
                    propertyName: '禁止行选中或勾选',
                    propertyType: 'modal',
                    description: '是否禁止行选中或勾选',
                    visible: propertyData.controlSource === 'Farris',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'javascript',
                        exampleCode:
                            '(row, index) => {\r\n    return row[\'sex\'] === \'女\';\r\n}\r\n\r\n\r\n参数：\r\n  ' +
                            'row：行数据  \r\n  index：行索引\r\n\r\n返回值：\r\n  true：禁止选择\r\n  false：可选中'
                    },
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'multiSort',
                    propertyName: '允许多列排序',
                    propertyType: 'boolean',
                    description: '是否允许多列排序',
                    visible: propertyData.controlSource === 'Farris',
                    defaultValue: false,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'clearSelectionsWhenDataIsEmpty',
                    propertyName: '数据源为空时清空已选记录',
                    propertyType: 'boolean',
                    description: '数据源为空时是否清空已选记录'
                },
                {
                    propertyID: 'selectOnEditing',
                    propertyName: '编辑时选中文本',
                    propertyType: 'boolean',
                    description: '进入编辑态后是否将单元格内文本全选中'
                },
                {
                    propertyID: 'remoteSort',
                    propertyName: '是否启用远端排序',
                    propertyType: 'boolean',
                    defaultValue: false
                },
                // {
                //     propertyID: 'conditions',
                //     propertyName: '过滤条件',
                //     propertyType: 'modal',
                //     description: '列表过滤条件设置',
                //     editor: EntityFilterConditionComponent,
                //     converter: new EntityFilterConditionConverter(),
                //     editorParams: {
                //         viewModelId
                //     },
                //     visible: propertyData.controlSource === 'Farris' && viewModel.bindTo === '/'
                // }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                switch (changeObject && changeObject.propertyID) {

                    case 'enableHeaderGroup': {
                        const headerGroup = this.properties.find(p => p.propertyID === 'headerGroup');
                        if (headerGroup) {
                            headerGroup.visible = changeObject.propertyValue;
                        }

                        // farris 列表启用多表头后 取消所有列的固定列属性
                        if (propertyData.controlSource === 'Farris' && changeObject.propertyValue) {
                            self.notifyService.info('启用多表头后自动取消固定列');
                            propertyData.fields.forEach(field => {
                                field.frozen = 'none';
                            });
                        }

                        const enableDragColumn = this.properties.find(p => p.propertyID === 'enableDragColumn');
                        if (enableDragColumn) {
                            enableDragColumn.visible = !changeObject.propertyValue;
                            propertyData.enableDragColumn = changeObject.propertyValue ? false : propertyData.enableDragColumn;
                        }

                        // changeObject.relateChangeProps = [{
                        //     propertyID: 'enableDragColumn',
                        //     propertyValue: propertyData.enableDragColumn
                        // }];
                        break;
                    }
                    case 'multiSelect': {
                        this.properties.forEach(p => {
                            if (p.propertyID === 'checkOnSelect' || p.propertyID === 'selectOnCheck') {
                                p.visible = !!changeObject.propertyValue && propertyData.selectionMode === 'custom';
                            }
                            if (p.propertyID === 'showSelectedList') {
                                p.visible = !!changeObject.propertyValue;
                            }
                            // 多选时必须显示复选框并且不可勾掉
                            if (p.propertyID === 'showCheckbox') {
                                p.readonly = changeObject.propertyValue === true;
                                propertyData.showCheckbox = (changeObject.propertyValue === true) || propertyData.showCheckbox;
                            }
                            // 不启用多选时必须隐藏全选
                            if (p.propertyID === 'showAllCheckbox') {
                                p.readonly = !changeObject.propertyValue;
                                if (p.readonly) {
                                    propertyData.showAllCheckbox = !!changeObject.propertyValue;
                                }
                            }

                            if (p.propertyID === 'selectedItemFormatter') {
                                p.visible = !!changeObject.propertyValue && propertyData.showSelectedList;
                            }
                            if (p.propertyID === 'enableMorePageSelect') {
                                p.visible = !!changeObject.propertyValue;
                                propertyData.enableMorePageSelect = !changeObject.propertyValue ? false : propertyData.enableMorePageSelect;
                            }
                            // 多选时才显示选择模式
                            if (p.propertyID === 'selectionMode') {
                                p.visible = propertyData.controlSource === 'Farris' && changeObject.propertyValue;
                            }
                            // 多选时才显示选择模式
                            if (p.propertyID === 'keepSelect') {
                                p.visible = changeObject.propertyValue && propertyData.selectionMode === 'custom';
                            }
                        });
                        break;
                    }
                    case 'nowrap': {
                        if (!changeObject.propertyValue) {
                            self.notifyService.info('启用数据折行后自动取消虚拟加载');
                            propertyData.virtualized = false;
                        }
                        break;
                    }
                    case 'virtualized': {
                        if (changeObject.propertyValue && !propertyData.nowrap) {
                            self.notifyService.info('启用虚拟加载后自动取消数据折行');
                            propertyData.nowrap = true;
                        }
                        break;
                    }
                    // case 'virtualizedAsyncLoad': {
                    //     const scrollYLoad = this.properties.find(p => p.propertyID === 'scrollYLoad');
                    //     if (scrollYLoad) {
                    //         scrollYLoad.visible = changeObject.propertyValue;
                    //     }
                    //     break;
                    // }
                    case 'showSelectedList': {
                        const selectedItemFormatter = this.properties.find(p => p.propertyID === 'selectedItemFormatter');
                        if (selectedItemFormatter) {
                            selectedItemFormatter.visible = changeObject.propertyValue;
                        }
                        break;
                    }
                    case 'selectionMode': {
                        const keepSelect = this.properties.find(p => p.propertyID === 'keepSelect');
                        if (keepSelect) {
                            keepSelect.visible = propertyData.multiSelect && changeObject.propertyValue === 'custom';
                        }
                        const checkOnSelect = this.properties.find(p => p.propertyID === 'checkOnSelect');
                        if (checkOnSelect) {
                            checkOnSelect.visible = changeObject.propertyValue === 'custom';
                        }
                        const selectOnCheck = this.properties.find(p => p.propertyID === 'selectOnCheck');
                        if (selectOnCheck) {
                            selectOnCheck.visible = changeObject.propertyValue === 'custom';
                        }
                        break;
                    }
                    case 'useControlPanel': {
                        propertyData.enableSimpleMode = changeObject.propertyValue;
                        propertyData.enableScheme = changeObject.propertyValue;

                        const enableSimpleMode = this.properties.find(p => p.propertyID === 'enableSimpleMode');
                        if (enableSimpleMode) {
                            enableSimpleMode.visible = changeObject.propertyValue;
                        }
                        const enableScheme = this.properties.find(p => p.propertyID === 'enableScheme');
                        if (enableScheme) {
                            enableScheme.visible = changeObject.propertyValue && propertyData.enableSimpleMode;
                        }
                        break;
                    }
                    case 'enableSimpleMode': {
                        const enableScheme = this.properties.find(p => p.propertyID === 'enableScheme');
                        if (enableScheme) {
                            enableScheme.visible = changeObject.propertyValue;
                        }
                        if (!changeObject.propertyValue) {
                            propertyData.enableScheme = false;
                        }
                        break;
                    }
                    case 'allowEmpty': {
                        viewModel.allowEmpty = changeObject.propertyValue;

                        self.syncAllowEmptyToModalComponent(propertyData, changeObject.propertyValue);
                        break;
                    }
                }
            }
        };
    }

    private getOperationProperties(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'operation',
            categoryName: '操作列',
            properties: [
                {
                    propertyID: 'enableCommandColumn',
                    propertyName: '启用操作列',
                    propertyType: 'select',
                    description: '是否启用操作列',
                    detail: 'https://igix.inspures.com/igixword/main.html#datagrid',
                    iterator: [{ key: false, value: '不启用' }, { key: true, value: '启用默认操作列' }, { key: 'custom', value: '自定义操作列' }],
                    visible: propertyData.controlSource === 'Farris',
                    defaultValue: false,
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'showCommandColumn',
                    propertyName: '显示操作列',
                    propertyType: 'unity',
                    description: '是否显示操作列，可以根据场景动态配置是否显示操作列',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable', 'custom'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        }
                    },
                    visible: propertyData.controlSource === 'Farris' && !!propertyData.enableCommandColumn,

                },
                {
                    propertyID: 'customCommandColumnTempl',
                    propertyName: '自定义操作列模板',
                    propertyType: 'modal',
                    description: '自定义操作列模板设置',
                    editor: CodeEditorComponent,
                    visible: propertyData.controlSource === 'Farris' && propertyData.enableCommandColumn === 'custom',
                    editorParams: {
                        language: 'html'
                    },
                    controlSource: 'Farris'
                },
                {
                    propertyID: 'commandColumnWidth',
                    propertyName: '操作列宽度（px）',
                    propertyType: 'number',
                    description: '操作列宽度设置',
                    min: 0,
                    decimals: 0,
                    visible: propertyData.controlSource === 'Farris' && !!propertyData.enableCommandColumn,
                    controlSource: 'Farris'
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                switch (changeObject && changeObject.propertyID) {

                    case 'enableCommandColumn':
                        this.properties.forEach(p => {
                            const propId = p.propertyID;
                            if (propId === 'enableCommandColumn') {
                                return;
                            }
                            if (propId === 'customCommandColumnTempl') {
                                p.visible = changeObject.propertyValue === 'custom';
                            } else {
                                p.visible = !!changeObject.propertyValue;
                            }
                        });
                        break;


                    default:
                        break;
                }
            }
        };

    }

    private getGroupProperties(propertyData: any, viewModelId: string) {
        const self = this;
        return {
            categoryId: 'gridGroup',
            categoryName: '分组',
            properties: [
                {
                    propertyID: 'groupable',
                    propertyName: '启用分组',
                    propertyType: 'boolean',
                    description: '是否启用分组',
                    detail: 'https://igix.inspures.com/igixword/main.html#datagrid',
                    refreshPanelAfterChanged: true // 更新常规分类下的合并单元格属性、固定列属性
                }, {
                    propertyID: 'group',
                    propertyName: '分组列',
                    propertyType: 'modal',
                    description: '分组列选择',
                    visible: propertyData.groupable,
                    editor: GroupFieldEditorComponent,
                    editorParams: {
                        viewModelId,
                        selectNumber: propertyData.controlSource === 'Farris' ? 2 : 1 // farrisgrid支持选择两列
                    },
                    converter: new GroupFieldConverter()
                },
                {
                    propertyID: 'showGroupColumn',
                    propertyName: '显示分组列',
                    propertyType: 'boolean',
                    description: '是否显示分组列',
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris',
                    defaultValue: true
                },
                {
                    propertyID: 'groupTotalText',
                    propertyName: '分组合计文本',
                    propertyType: 'string',
                    description: '分组合计文本设置',
                    visible: propertyData.groupable && propertyData.controlSource === 'Kendo'
                },
                {
                    propertyID: 'groupFormatter',
                    propertyName: '分组格式化',
                    propertyType: 'modal',
                    description: '分组格式化设置',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'javascript',
                        exampleCode:
                            '(rowData) => {\r\n    if (rowData.field === \'name\') {\r\n        return `<b style=\"color:red\">姓名' +
                            '： ${rowData.value} [${rowData.total}]</b>`;\r\n    } else {\r\n        return `<b style=\"color:#886ab5\">婚否' +
                            '：${rowData.value} [${rowData.total}]</b>`;\r\n    }\r\n}\r\n\r\n\r\n注：\r\n  rowData： 行数据'
                    },
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris'
                },
                {
                    propertyID: 'groupStyler',
                    propertyName: '分组样式',
                    propertyType: 'modal',
                    description: '分组样式设置',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'javascript',
                        exampleCode:
                            '(rowData) => {\r\n    if (rowData.field === \'name\') {\r\n        return {\r\n            ' +
                            'style: {\r\n                background: \'#EFF5E5\',\r\n                color: \'#5A8129\'\r\n' +
                            '            }\r\n        };\r\n    } else if (rowData.field === \'sex\') {\r\n        return {\r\n' +
                            '            style: {\r\n                background: \'#FFEAC1\',\r\n                color: \'#E99100\'\r\n' +
                            '            }\r\n        };\r\n    }\r\n}\r\n\r\n\r\n注：\r\n   rowData: 行数据'
                    },
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris'
                },
                {
                    propertyID: 'groupFooter',
                    propertyName: '启用分组合计行',
                    propertyType: 'boolean',
                    description: '是否启用分组合计行',
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris',
                    defaultValue: false
                },
                {
                    propertyID: 'showRowGroupPanel',
                    propertyName: '启用分组面板',
                    propertyType: 'boolean',
                    description: '是否启用分组面板',
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris',
                    defaultValue: false
                },
                {
                    propertyID: 'groupSummaryPosition',
                    propertyName: '合计数据显示位置',
                    propertyType: 'select',
                    description: '合计数据显示位置设置',
                    iterator: [{ key: 'groupFooterRow', value: '独占一行' }, { key: 'inGroupRow', value: '分组行内' }],
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris'
                },
                {
                    propertyID: 'disableGroupOnEditing',
                    propertyName: '编辑时取消分组',
                    propertyType: 'boolean',
                    description: '进入编辑态后是否取消表格分组',
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris'
                },
                {
                    propertyID: 'expandGroupRows',
                    propertyName: '默认展开分组行',
                    propertyType: 'boolean',
                    description: '数据加载后，分组行是否处于展开状态',
                    visible: propertyData.groupable && propertyData.controlSource === 'Farris'
                }
            ],
            setPropertyRelates(changeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'groupable': {
                        this.properties.forEach(p => {
                            if (p.propertyID === 'groupable') {
                                return;
                            }
                            if (p.propertyID === 'group') {
                                p.visible = changeObject.propertyValue;
                            }
                            if (p.propertyID === 'groupTotalText') {
                                p.visible = changeObject.propertyValue && propertyData.controlSource === 'Kendo';
                            } else {
                                p.visible = changeObject.propertyValue && propertyData.controlSource === 'Farris';
                            }
                            // if (p.propertyID === 'groupFormatter' || p.propertyID === 'groupStyler'
                            //     || p.propertyID === 'showGroupColumn' || p.propertyID === 'groupFooter'
                            //     || p.propertyID === 'showRowGroupPanel' || p.propertyID === 'groupSummaryPosition'
                            //     || p.propertyID === 'disableGroupOnEditing') {
                            //     p.visible = changeObject.propertyValue && propertyData.controlSource === 'Farris';
                            // }
                        });
                        // farris 列表启用分组后 取消所有列的固定列属性、取消合并单元格属性
                        if (propertyData.controlSource === 'Farris' && changeObject.propertyValue) {
                            self.notifyService.info('启用分组后自动取消合并单元格和固定列');
                            propertyData.fields.forEach(field => {
                                field.frozen = 'none';
                            });
                            propertyData.mergeCell = false;

                            const customCategoryConfig = self.propertyConfig.find(cat => cat.categoryId === 'custom');
                            if (customCategoryConfig && customCategoryConfig.properties) {
                                const mergeFields = customCategoryConfig.properties.find(p => p.propertyID === 'mergeFields');
                                if (mergeFields) {
                                    mergeFields.visible = false;
                                }
                            }

                        }

                        break;
                    }
                    default:
                        break;
                }
            }
        };

    }

    private getFooterPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const footerProprites: PropertyEntity[] = [
            {
                propertyID: 'showFooter',
                propertyName: '显示合计行',
                propertyType: 'boolean',
                description: '是否显示合计行',
                detail: 'https://igix.inspures.com/igixword/main.html#datagrid',
                defaultValue: false
            },
            {
                propertyID: 'footerTemplate',
                propertyName: '合计行模板',
                propertyType: 'modal',
                description: '合计行模板设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                },
                visible: propertyData.showFooter
            },
            {
                propertyID: 'footerDataFrom',
                propertyName: '合计行数据来源',
                propertyType: 'select',
                description: '合计行数据来源选择',
                defaultValue: 'client',
                visible: propertyData.showFooter,
                iterator: [{ key: 'client', value: '客户端' }, { key: 'server', value: '服务端' }]
            },
            {
                propertyID: 'footerHeight',
                propertyName: '合计行行高',
                propertyType: 'number',
                description: '合计行行高设置',
                min: 0,
                visible: propertyData.showFooter
            },
            {
                propertyID: 'footerPosition',
                propertyName: '合计行显示位置',
                propertyType: 'select',
                iterator: [{ key: 'top', value: '顶部' }, { key: 'bottom', value: '底部' }, { key: 'both', value: '同时显示' }],
                description: '合计行显示位置设置',
                visible: propertyData.showFooter
            },
            {
                propertyID: 'footerStyler',
                propertyName: '合计行样式',
                propertyType: 'modal',
                description: '合计行样式设置',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'javascript',
                    exampleCode: "() => {\r\n    return {\r\n        style: {\r\n            color: '#5A8129',\r\n            background: '#CCE7A4'\r\n        }\r\n    };\r\n}"
                },
                visible: propertyData.showFooter
            }
        ];

        return {
            categoryId: 'footer',
            categoryName: '合计',
            properties: footerProprites,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                switch (changeObject && changeObject.propertyID) {
                    case 'showFooter': {
                        this.properties.forEach(p => {
                            if (p.propertyID === 'showFooter') { return; }

                            p.visible = changeObject.propertyValue;
                            if (p.propertyID === 'footerDataCommand') {
                                p.visible = changeObject.propertyValue && propertyData.footerDataFrom === 'server';
                            }
                        });
                        break;
                    }
                    case 'footerDataFrom': {
                        const footerDataCommand = this.properties.find(p => p.propertyID === 'footerDataCommand');
                        if (footerDataCommand) {
                            footerDataCommand.visible = changeObject.propertyValue === 'server';
                            propertyData.footerDataCommand = changeObject.propertyValue === 'server' ?
                                propertyData.footerDataCommand : null;
                        }
                    }
                }
            }
        };
    }

    /**
     * 过滤。使用filterType属性控制过滤的类型（enableFilterRow/enableSmartFilter）
     * filterType属性是设计器自用的，不对应datagrid控件属性
     */
    private getFilterRowConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const filterRowProperties: PropertyEntity[] = [
            {
                propertyID: 'filterType',
                propertyName: '启用过滤行',
                propertyType: 'select',
                description: '是否启用过滤行',
                iterator: [
                    { key: 'none', value: '不启用' },
                    { key: 'enableFilterRow', value: '启用过滤行' },
                    { key: 'enableSmartFilter', value: '启用智能过滤' }
                ]
            },
            {
                propertyID: 'remoteFilter',
                propertyName: '服务器端过滤',
                propertyType: 'boolean',
                description: '是否启用服务器端过滤',
                defaultValue: false,
                visible: propertyData.filterType !== 'none'
            },
            {
                propertyID: 'showFilterBar',
                propertyName: '显示过滤条件工具条',
                propertyType: 'boolean',
                description: '是否显示过滤条件工具条',
                defaultValue: false,
                visible: propertyData.filterType !== 'none'
            }
        ];
        return {
            categoryId: 'filterRow',
            categoryName: '过滤行',
            properties: filterRowProperties,
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'filterType': {
                        this.properties.map(p => {
                            if (p.propertyID !== 'filterType') {
                                p.visible = changeObject.propertyValue !== 'none';
                            }
                        });

                        // 关联变更：同步属性值
                        propertyData.enableFilterRow = changeObject.propertyValue === 'enableFilterRow';
                        propertyData.enableSmartFilter = changeObject.propertyValue === 'enableSmartFilter';

                        // 启用过滤行后自动配置列的是否过滤属性
                        propertyData.fields.forEach(field => {
                            field.enableFilter = changeObject.propertyValue !== 'none';
                        });
                        break;
                    }
                }
            }
        };
    }

    // 事件编辑器集成
    private getEventPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        let eventList = [
            {
                label: 'onSelectionChange',
                name: '行切换事件'
            }, {
                label: 'rowClick',
                name: '行点击事件'
            }, {
                label: 'appendRow',
                name: '回车新增行事件'
            }, {
                label: 'pageChange',
                name: '切换页码事件'
            }, {
                label: 'pageSizeChanged',
                name: '分页条数变化事件'
            }, {
                label: 'beforeSelect',
                name: '选中前事件'
            }, {
                label: 'beforeUnSelect',
                name: '取消选中前事件'
            }, {
                label: 'beforeCheck',
                name: '勾选前事件'
            }, {
                label: 'checked',
                name: '勾选后事件'
            }, {
                label: 'beforeUnCheck',
                name: '取消勾选前事件'
            }, {
                label: 'unChecked',
                name: '取消勾选后事件'
            }, {
                label: 'dblClickRow',
                name: '行双击事件'
            }, {
                label: 'beforeEdit',
                name: '编辑前事件'
            }, {
                label: 'checkAll',
                name: '全选事件'
            }, {
                label: 'unCheckAll',
                name: '取消全选事件'
            }, {
                label: 'filterChanged',
                name: '过滤事件',
            }, {
                label: 'columnSorted',
                name: '排序事件',
            }];
        eventList = self.switchEvents(propertyData, eventList);
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList, self.switchEvents),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates; // 添加自定义方法后，调用此回调方法，用于处理联动属性
                    EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, parameters['events'], parameters);
                    this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, parameters['events'], self.switchEvents);

                }
                // 联动修改排序开关
                propertyData.remoteSort = propertyData.columnSorted ? true : false;

            }
        };
    }

    private switchEvents(propertyData, eventList) {
        if (propertyData.controlSource === 'Farris' && propertyData.virtualizedAsyncLoad) {
            const eventListExist = eventList.find(eventListItem => eventListItem.label === 'scrollYLoad');
            if (eventListExist === undefined) {
                eventList.push(
                    {
                        label: 'scrollYLoad',
                        name: '滚动加载事件'
                    }
                );
            }
        } else {
            eventList = eventList.filter(eventListItem => eventListItem.label !== 'scrollYLoad');
        }
        if (propertyData.controlSource === 'Farris' && propertyData.enableCommandColumn) {
            const eventListExist = eventList.find(eventListItem => eventListItem.label === 'onEditClicked');
            if (eventListExist === undefined) {
                eventList.push(
                    {
                        label: 'onEditClicked',
                        name: '操作列点击编辑'
                    },
                    {
                        label: 'onDeleteClicked',
                        name: '操作列点击删除'
                    }
                );
            }
        } else {
            eventList = eventList.filter(eventListItem => eventListItem.label !== 'onEditClicked');
            eventList = eventList.filter(eventListItem => eventListItem.label !== 'onDeleteClicked');
        }


        if (propertyData.controlSource === 'Farris' && propertyData.virtualizedAsyncLoad) {
            const eventListExist = eventList.find(eventListItem => eventListItem.label === 'footerDataCommand');
            if (eventListExist === undefined) {
                eventList.push(
                    {
                        label: 'footerDataCommand',
                        name: '合计命令'
                    });
            }
        } else {
            eventList = eventList.filter(eventListItem => eventListItem.label !== 'footerDataCommand');
        }
        return eventList;
    }


    /**
     * 分页属性（子表暂不支持）
     */
    private getPaginationCofig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;
        const viewModel = this.domService.getViewModelById(viewModelId);

        if (!viewModel.pagination) {
            viewModel.pagination = {
                enable: false
            };
        }
        const vmPagination = viewModel.pagination;
        // 这三个属性是存在vm中，但是需要在datagrid控件上编辑
        propertyData.pagination = vmPagination.enable;
        propertyData.pageList = vmPagination.pageList;
        propertyData.pageSize = vmPagination.pageSize;

        const pageSizeIterator = this.getPageSizeIterator(propertyData.pageList);
        return {
            categoryId: 'pagination',
            categoryName: '分页',
            properties: [
                {
                    propertyID: 'pagination',
                    propertyName: '启用分页',
                    propertyType: 'boolean',
                    description: '是否启用分页',
                    defaultValue: true,
                    readonly: viewModel.bindTo !== '/'
                },
                {
                    propertyID: 'lockPagination',
                    propertyName: '锁定分页',
                    propertyType: 'unity',
                    description: '是否锁定分页',
                    visible: propertyData.pagination,
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'custom'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                        }
                    }
                },
                {
                    propertyID: 'showPageSize',
                    propertyName: '显示分页条数',
                    propertyType: 'boolean',
                    description: '是否显示分页条数',
                    defaultValue: false,
                    visible: propertyData.pagination
                },
                {
                    propertyID: 'pageList',
                    propertyName: '分页条数',
                    propertyType: 'string',
                    description: '请输入以英文逗号分开的数字，最大值为1000。例如：10,20,30,50,100',
                    visible: propertyData.pagination && propertyData.showPageSize,
                    notAllowedChars: [/[^\d,]/g]
                },
                {
                    propertyID: 'pageSize',
                    propertyName: '默认分页条数',
                    propertyType: 'select',
                    description: '默认分页条数设置',
                    iterator: pageSizeIterator,
                    visible: propertyData.pagination
                },
                {
                    propertyID: 'showGotoInput',
                    propertyName: '显示页码输入框',
                    propertyType: 'boolean',
                    description: '是否显示页码输入框',
                    visible: propertyData.pagination,
                    defaultValue: false
                },
                {
                    propertyID: 'pagerContentTemplate',
                    propertyName: '扩展区域模板',
                    propertyType: 'modal',
                    editor: CodeEditorComponent,
                    editorParams: {
                        language: 'html'
                    },
                    description: '分页条扩展区域模板',
                    visible: propertyData.pagination,
                    showClearButton: true
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'pagination': {
                        this.properties.map(p => {
                            if (p.propertyID !== 'pagination') {
                                p.visible = changeObject.propertyValue;
                            }
                            if (p.propertyID === 'pageList') {
                                p.visible = changeObject.propertyValue && propertyData.showPageSize;
                            }
                        });
                        vmPagination.enable = changeObject.propertyValue;
                        if (changeObject.propertyValue) {
                            if (!propertyData.pageSize) {
                                propertyData.pageSize = 20;
                                vmPagination.pageSize = 20;
                            }
                            self.setDefaultPageList(propertyData, vmPagination);
                            const pageSize = this.properties.find(p => p.propertyID === 'pageSize');
                            if (pageSize) {
                                pageSize.iterator = self.getPageSizeIterator(propertyData.pageList);
                            }

                            // 配置切换页码事件
                            if (!propertyData.pageChange) {
                                const changePageCmdCode = self.setChangePageEvent(propertyData, viewModelId);
                                propertyData.pageChange = changePageCmdCode;
                            }

                        }
                        break;
                    }
                    case 'pageList': {
                        vmPagination.pageList = changeObject.propertyValue;
                        const pageSize = this.properties.find(p => p.propertyID === 'pageSize');
                        if (pageSize) {
                            pageSize.iterator = self.getPageSizeIterator(changeObject.propertyValue);
                        }
                        if (!changeObject.propertyValue.includes(propertyData.pageSize)) {
                            vmPagination.pageSize = propertyData.pageSize = null;
                        }
                        break;
                    }
                    case 'pageSize': {
                        vmPagination.pageSize = changeObject.propertyValue;
                        break;
                    }
                    case 'showPageSize': {
                        const pageList = this.properties.find(p => p.propertyID === 'pageList');
                        if (pageList) {
                            pageList.visible = changeObject.propertyValue;
                        }

                        if (changeObject.propertyValue) {
                            self.setDefaultPageList(propertyData, vmPagination);
                            const pageSize = this.properties.find(p => p.propertyID === 'pageSize');
                            if (pageSize) {
                                pageSize.iterator = self.getPageSizeIterator(propertyData.pageList);
                            }

                            // 默认配置分页条数变化事件
                            if (!propertyData.pageSizeChanged) {
                                propertyData.pageSizeChanged = propertyData.pageChange;
                            }


                        }


                    }
                }
            }

        };
    }

    private setDefaultPageList(propertyData: any, vmPagination: any) {

        if (!propertyData.pageList) {
            propertyData.pageList = '10,20,30,50,100';
            if (propertyData.pageSize && !propertyData.pageList.includes(propertyData.pageSize)) {
                let pageList = [10, 20, 30, 50, 100, propertyData.pageSize];
                pageList = pageList.sort((A, B) => A - B);
                propertyData.pageList = pageList.toString();
            }

            vmPagination.pageList = propertyData.pageList;
        }
    }


    getPageSizeIterator(pageList: string) {
        if (!pageList) {
            pageList = '10,20,30,50,100';
        }
        const iterator = [];
        pageList.split(',').forEach(pageSize => {
            iterator.push({
                key: Number.parseInt(pageSize, 10),
                value: pageSize
            });
        });
        return iterator;
    }
    /**
     * 启用分页后自动配置切换页码事件
     */
    private setChangePageEvent(propertyData: any, viewModelId: string) {
        // 检查视图模型中是否有handlerName=ChangePage的命令
        const vm = this.domService.getViewModelById(viewModelId);
        if (!vm || !vm.commands || vm.commands.length === 0) {
            return;
        }
        const changePageCmd = vm.commands.find(command => command.handlerName === 'ChangePage');
        if (changePageCmd) {
            return changePageCmd.code;
        }
    }


    private getContextMenuCofig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const contextMenuProp = new GridContextMenuProp(this.domService, this.formBasicService, this.webCmdService, this.msgService, this.eventEditorService);
        return {
            categoryId: 'contextMenu',
            categoryName: '右键菜单',
            hide: propertyData.controlSource !== 'Farris',
            properties: [
                {
                    propertyID: 'enableContextMenu',
                    propertyName: '启用右键菜单',
                    propertyType: 'unity',
                    description: '是否启用右键菜单',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        }
                    }
                },
                {
                    propertyID: 'contextMenuItems',
                    propertyName: '右键菜单配置',
                    propertyType: 'modal',
                    description: '配置右键菜单项',
                    visible: propertyData.enableContextMenu,
                    editor: CollectionWithPropertyEditorComponent,
                    converter: new CollectionWithPropertyConverter(),
                    editorParams: {
                        modalTitle: '右键菜单编辑器',
                        viewModelId,
                        idKey: 'id',
                        textKey: 'title',
                        childrenKey: 'children',
                        maxCascadeLevel: 2,
                        controlType: 'DataGridContextMenuItem',
                        defaultControlValue: {
                            id: 'menu',
                            title: '菜单'
                        },
                        parentNodeId: propertyData.id,
                        getPropertyConfig: (selectedNode, triggerModalSave: any) => contextMenuProp.getPropConfig(viewModelId, selectedNode, triggerModalSave)
                    }
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, data) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {
                    case 'enableContextMenu': {
                        const contextMenuItems = this.properties.find(p => p.propertyID === 'contextMenuItems');
                        if (contextMenuItems) {
                            contextMenuItems.visible = changeObject.propertyValue;
                        }
                    }
                }
            }
        };
    }

    /**
     * 同步弹出组件内部的子组件的allowEmpty属性
     */
    private syncAllowEmptyToModalComponent(propertyData: any, allowEmpty: boolean) {
        if (!propertyData.modalComponentId) {
            return;
        }

        const modalCmp = this.domService.getComponentById(propertyData.modalComponentId);
        if (!modalCmp) {
            return;
        }

        // 同步弹出组件的视图模型
        const modalVM = this.domService.getViewModelById(modalCmp.viewModel);
        if (modalVM) {
            modalVM.allowEmpty = allowEmpty;
        }

        // 同步弹出子组件
        this.syncAllowEmptyToModalChildComponent(modalCmp.contents, allowEmpty);
    }

    /**
     * 同步弹出组件内部的子组件的allowEmpty属性
     */
    private syncAllowEmptyToModalChildComponent(contents: any[], allowEmpty: boolean) {

        if (contents && contents.length) {
            contents.forEach(c => {
                if (c.contents) {
                    this.syncAllowEmptyToModalChildComponent(c.contents, allowEmpty);
                }

                if (c.type !== DgControl.ComponentRef.type) {
                    return;
                }
                const modalFormCmp = this.domService.getComponentById(c.component);
                const modalVM = this.domService.getViewModelById(modalFormCmp.viewModel);
                if (modalVM) {
                    modalVM.allowEmpty = allowEmpty;
                }

            });
        }
    }
}
