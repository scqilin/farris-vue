import FdDataGridComponent from './component/fd-datagrid';
import { DataGridContextMenuItemSchema, DataGridSchema, GridFieldSchema } from './schema/datagrid';
import { ComponentExportEntity } from '@farris/designer-element';
import FdDataGridTemplates from '../common/templates/grid';
import FdDataGridFieldComponent from './component/datagrid-field/component/fd-datagrid-field';
import FdDataGridFieldTemplates from '../common/templates/grid-field';


export const DataGrid: ComponentExportEntity = {
    type: 'DataGrid',
    component: FdDataGridComponent,
    template: FdDataGridTemplates,
    metadata: DataGridSchema
};

export const GridField: ComponentExportEntity = {
    type: 'GridField',
    component: FdDataGridFieldComponent,
    template: FdDataGridFieldTemplates,
    metadata: GridFieldSchema
};

export const DataGridContextMenuItem: ComponentExportEntity = {
    type: 'DataGridContextMenuItem',
    metadata: DataGridContextMenuItemSchema
};
