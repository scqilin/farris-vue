import { TypeConverter } from '@farris/ide-property-panel';

export class GridFieldFormatterConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (!data) {
            return '';
        }
        if (data.type) {
            const iterator = [
                { key: 'none', value: '无' },
                { key: 'number', value: '数字' },
                { key: 'boolean', value: '布尔-文本' },
                { key: 'boolean2', value: '布尔-图标' },
                { key: 'date', value: '日期' },
                { key: 'enum', value: '枚举' },
                { key: 'custom', value: '自定义' },
                { key: 'timeago', value: '相对时间' }
            ];
            const item = iterator.find(i => i.key === data.type);
            if (item) {
                return item.value;
            }
            return JSON.stringify(data);
        } else {
            return JSON.stringify(data);
        }
    }


}
