import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { TreeNode } from '@farris/ui-treetable';
import { EventEditorService, WebCmdService, FormBasicService, DomService, UniformEditorDataUtil } from '@farris/designer-services';
import { IconSelectEditorComponent } from '@farris/designer-devkit';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';
import { EventsEditorFuncUtils } from '../../../../utils/events-editor-func';
import { MessagerService } from '@farris/ui-messager';


export class GridContextMenuProp {
    propertyConfig: ElementPropertyConfig[];

    /** 在弹窗中使用按钮配置时，交互面板若要跳转到代码视图，需要先关闭弹窗 */
    triggerModalSave?: any;

    constructor(
        private domService: DomService,
        public formBasicService: FormBasicService,
        public webCmdService: WebCmdService,
        private msgService: MessagerService,
        private eventEditorService: EventEditorService) { }

    getPropConfig(viewModelId: string, selectedNode: TreeNode, triggerModalSave: any): ElementPropertyConfig[] {
        this.propertyConfig = [];
        this.triggerModalSave = triggerModalSave;

        const propertyData = selectedNode.data;

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig();
        this.propertyConfig.push(basicPropConfig);


        // 外观属性
        const appearancePropConfig = this.getAppearanceProperties();
        this.propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorProperties(propertyData, viewModelId);
        this.propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventProperties(propertyData, viewModelId, selectedNode);
        this.propertyConfig.push(eventPropConfig);

        return this.propertyConfig;
    }

    private getBasicPropConfig(): ElementPropertyConfig {

        return {
            categoryId: 'basic',
            categoryName: '基本信息',
            properties: [
                {
                    propertyID: 'id',
                    propertyName: '标识',
                    propertyType: 'string',
                    description: '菜单的id',
                    readonly: true
                }
            ]
        };
    }
    private getAppearanceProperties(): ElementPropertyConfig {

        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: [
                {
                    propertyID: 'title',
                    propertyName: '名称',
                    propertyType: 'string',
                    description: '菜单标题名称'
                },
                {
                    propertyID: 'icon',
                    propertyName: '图标',
                    propertyType: 'modal',
                    description: '菜单名称前面的图标',
                    editor: IconSelectEditorComponent,
                    editorParams: { needIconClass: true }
                }
            ]
        };
    }

    private getBehaviorProperties(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const self = this;

        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                {
                    propertyID: 'visible',
                    propertyName: '是否可见',
                    propertyType: 'unity',
                    description: '运行时菜单是否可见',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        }
                    }
                },
                {
                    propertyID: 'disable',
                    propertyName: '是否禁用',
                    propertyType: 'unity',
                    description: '运行时菜单是否禁用',
                    editorParams: {
                        controlName: UniformEditorDataUtil.getControlName(propertyData),
                        constType: 'enum',
                        editorOptions: {
                            types: ['const', 'variable'],
                            enums: [{ key: true, value: '是' }, { key: false, value: '否' }],
                            variables: UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            getVariables: () => UniformEditorDataUtil.getVariables(viewModelId, this.domService),
                            newVariableType: 'Boolean',
                            newVariablePrefix: 'is'
                        }
                    }
                }
            ]
        };
    }

    private getEventProperties(propertyData: any, viewModelId: string, selectedNode: TreeNode): ElementPropertyConfig {
        const self = this;
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'click',
                name: '点击事件'
            },
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject, data, parameters) {
                delete propertyData[viewModelId];

                // 暂存控件信息，用于自动创建新方法的方法编号和名称
                parameters.controlInfo = { type: 'DataGridContextMenu', name: '列表右键菜单' };

                // 若需要跳转到代码视图，首先要求用户将当前弹出窗口关闭；
                if (parameters.isAddControllerMethod && self.triggerModalSave) {
                    self.msgService.question('确定关闭当前编辑器并跳转到代码视图吗？', () => {
                        self.triggerModalSave();

                        EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                        this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
                    });
                } else {
                    EventsEditorFuncUtils.saveRelatedParameters(eventEditorService, domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                    this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);
                }
            }
        };

    }
}
