import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { TemplateField } from '../template-binding-editor.component';
import { FormSchemaEntityField, FormSchemaEntityFieldType } from '@farris/designer-services';
import { cloneDeep } from 'lodash-es';

export interface BindingSchemaField {
  id: string;
  code: string;
  name: string;
  displayName: string;
  bindingPath: string;
  type: FormSchemaEntityFieldType;
}

@Component({
  selector: 'variable-list-item',
  templateUrl: './variable-list-item.component.html',
  styleUrls: ['./variable-list-item.component.css']
})
export class VariableListItemComponent implements OnInit, OnChanges {

  @Input() active: boolean;

  @Input() item: TemplateField;

  @Input() schemaFields: FormSchemaEntityField[];

  @Input() presetTypes: string[];

  @Output() itemChanged = new EventEmitter<TemplateField>();

  @Output() itemRemoved = new EventEmitter<TemplateField>();

  @Output() moveUpItem = new EventEmitter<TemplateField>();

  @Output() moveDownItem = new EventEmitter<TemplateField>();

  bindingSchemaFields: BindingSchemaField[];

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.schemaFields) {
      this.bindingSchemaFields = this.getBindableFields();
    }
    if (changes.item) {
      if (changes.item.currentValue.presetType === 'badge' || changes.item.currentValue.presetType === 'status') {
        this.bindingSchemaFields = this.getEnumableFields();
      }
    }
  }

  onBindingFieldChanged($event) {
    const selectedField = $event.data as FormSchemaEntityField;
    this.item.binding.field = selectedField.id;
    this.item.binding.path = selectedField.bindingPath;
    if (selectedField.type && selectedField.type.name === 'Enum' && selectedField.type.enumValues) {
      this.item.enumData = cloneDeep(selectedField.type.enumValues);
    }
    this.itemChanged.emit(this.item);
  }

  onPresetTypeChanged($event) {
    const { id: selectedPresetType } = $event.data as { id: string, name: string };
    if (selectedPresetType === 'badge' || selectedPresetType === 'status') {
      this.bindingSchemaFields = this.getEnumableFields();
    } else {
      this.bindingSchemaFields = this.getBindableFields();
    }
    this.item.presetType = selectedPresetType;
    this.item.binding.field = '';
    this.item.binding.path = '';
    this.item.enumData = [];
    this.itemChanged.emit(this.item);
  }

  removeItem() {
    this.itemRemoved.emit(this.item);
  }

  moveUp() {
    this.moveUpItem.emit(this.item);
  }

  moveDown() {
    this.moveDownItem.emit(this.item);
  }

  private getEnumableFields(): BindingSchemaField[] {
    return this.schemaFields
      .filter(item => !!item.type.enumValues)
      .map<BindingSchemaField>(item => {
        return {
          id: item.id,
          code: item.code,
          name: item.name,
          displayName: `${item.name}(${item.label})`,
          bindingPath: item.bindingPath,
          type: item.type
        };
      });
  }

  private getBindableFields(): BindingSchemaField[] {
    return this.schemaFields
      .map<BindingSchemaField>(item => {
        return {
          id: item.id,
          code: item.code,
          name: item.name,
          displayName: `${item.name}(${item.label})`,
          bindingPath: item.bindingPath,
          type: item.type
        };
      });
  }
}
