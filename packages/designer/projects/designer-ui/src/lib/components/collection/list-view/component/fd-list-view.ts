import FdCollectionBaseComponent from '../../common/collection-base/fd-collection-base';
import { ListViewSchema } from '../schema/schema';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ListViewProp } from '../property/property';
import { BuilderHTMLElement } from '@farris/designer-element';
import { ControlCssLoaderUtils } from '../../../../utils/control-css-loader';
import { DesignerEnvType, DomService, RefreshFormService } from '@farris/designer-services';
import { FormPropertyChangeObject } from '../../../../entity/property-change-entity';

export class FdListViewComponent extends FdCollectionBaseComponent {

    constructor(component: any, options: any) {
        super(component, options);

        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('list-view.css');
    }

    getDefaultSchema(): any {
        return ListViewSchema;
    }

    getClassName(): string {
        let clsName = super.getClassName();
        if (this.component.fill) {
            clsName += ' f-listview-fill';
        }
        return clsName;
    }
    init(): void {
        this.adaptOldControlSchema();
        super.init();
    }
    render(): any {
        this.setToolbarComponentBasicInfoMap();

        return super.render(this.renderTemplate('ListView', {
            component: this.component
        }));
    }
    canAccepts(sourceElement: BuilderHTMLElement, targetElement?: BuilderHTMLElement): boolean {
        return false;
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: ListViewProp = new ListViewProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    onPropertyChanged(changeObject: FormPropertyChangeObject, propertyIDs?: string[]): void {
        super.onPropertyChanged(changeObject);

        // 模板中可能有按钮，需要更新map信息
        if (changeObject.propertyID === 'contentTemplate') {
            this.setToolbarComponentBasicInfoMap();

            // 刷新实体树，更新字段前方的图标颜色---暂时只有低代码显示图标
            if (this.envType === DesignerEnvType.designer) {
                const refreshService = this.options.designerHost.getService('RefreshFormService') as RefreshFormService;
                refreshService.refreshSchamaTree.next();
            }

        }
    }
    /**
     * 主体Html模板中若配置了按钮，需要设置按钮所在的路径信息，用于交互面板的显示
     */
    private setToolbarComponentBasicInfoMap() {
        if (!this.component.contentTemplate || !this.component.contentTemplate.toolbar ||
            !this.component.contentTemplate.toolbar.contents || !this.component.contentTemplate.toolbar.contents.length) {
            return;
        }
        const domService = this.options.designerHost.getService('DomService') as DomService;

        this.component.contentTemplate.toolbar.contents.forEach(button => {
            domService.controlBasicInfoMap.set(button.id, {
                showName: button.text,
                parentPathName: `列表 > ${button.text}`
            });

        });


    }
    /**
     * 适配表单控件的元数据变更
     */
    private adaptOldControlSchema() {
        // 补充旧表单中ListView 的viewmodel没有保存分页信息
        const domService = this.options.designerHost.getService('DomService') as DomService;
        const viewModel = domService.getViewModelById(this.viewModelId);
        if (!this.component.supportPaging || !viewModel) {
            return;
        }

        viewModel.pagination = Object.assign(viewModel.pagination, {
            enable: true,
            pageList: this.component.pageList,
            pageSize: this.component.pageSize
        });

    }
}
