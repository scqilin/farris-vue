import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { ContainerUsualProp } from '../../container/common/property/container-property-config';
import { FormPropertyChangeObject } from '../../../entity/property-change-entity';
import { ProgressStepsConverter } from './editor/progress-steps-editor/progress-steps-convert';
import { ProgressStepsEditorComponent } from './editor/progress-steps-editor/progress-steps-editor.component';

export class WizardProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 数据属性
        const customPropConfig = this.getDataPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(customPropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        return propertyConfig;
    }

    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();
        commonProps.push(
            {
                propertyID: 'fill',
                propertyName: '是否填充',
                propertyType: 'boolean'
            },
            {
                propertyID: 'stepDirection',
                propertyName: '步骤条排列方式',
                propertyType: 'select',
                iterator: [{ key: 'vertical', value: '垂直' }, { key: 'horizontal', value: '水平' }]
            },
            {
                propertyID: 'stepPosition',
                propertyName: '步骤条摆放位置',
                propertyType: 'select',
                iterator: [
                    { key: 'PageLeft', value: '左侧' },
                    { key: 'Top', value: '左上方' },
                    { key: 'HeaderRight', value: '右上方' },
                    { key: 'None', value: '隐藏' },
                ]
            }
        );
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }

    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId)
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: [
                visibleProp,
                {
                    propertyID: 'stepClickable',
                    propertyName: '步骤条是否支持点击',
                    propertyType: 'boolean',
                    defaultValue: false
                },
                {
                    propertyID: 'storedIndex',
                    propertyName: '是否存储进度',
                    propertyType: 'boolean',
                    defaultValue: false
                },
                {
                    propertyID: 'storedIndexState',
                    propertyName: '保持状态',
                    propertyType: 'boolean',
                    defaultValue: false
                }
            ]
        };
    }

    private getDataPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        return {
            categoryId: 'progressData',
            categoryName: '数据配置',
            propertyData: propertyData.progressData,
            enableCascade: true,
            parentPropertyID: 'progressData',
            properties: [
                {
                    propertyID: 'stepMessages',
                    propertyName: '步骤条',
                    propertyType: 'modal',
                    editor: ProgressStepsEditorComponent,
                    editorParams: {
                        wizardPages: propertyData.contents,
                        activeIndex: propertyData.progressData ? propertyData.progressData.activeIndex : 0
                    },
                    converter: new ProgressStepsConverter()
                },
                {
                    propertyID: 'activeIndex',
                    propertyName: '默认显示页面索引',
                    propertyType: 'number',
                    decimals: 0,
                    min: 0,
                    max: propertyData.progressData && propertyData.progressData.stepMessages &&
                        propertyData.progressData.stepMessages.length > 0 ?
                        propertyData.progressData.stepMessages.length - 1 : 0,
                    defaultValue: 0
                }
            ],
            setPropertyRelates(changeObject: FormPropertyChangeObject, propData, parameters: any) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject.propertyID) {

                    case 'stepMessages': {
                        const activeIndex = this.properties.find(p => p.propertyID === 'activeIndex');
                        if (activeIndex) {
                            activeIndex.max = changeObject.propertyValue.length > 0 ? changeObject.propertyValue.length - 1 : 0;
                        }
                        if (propertyData.progressData.activeIndex > activeIndex.max) {
                            propertyData.progressData.activeIndex = 0;
                        }
                        propertyData.contents = parameters;

                        // 属性变更后，触发刷新页面
                        changeObject.needRefreshForm = true;

                        break;
                    }

                }
            }
        };
    }
}
