import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { BuilderHTMLElement, FarrisDesignBaseNestedComponent } from '@farris/designer-element';
import { WizardDetailSchema } from '../schema/schema';
import { FormPropertyChangeObject } from '../../../entity/property-change-entity';
import { WizardDetailProp } from '../property/property-config-wizard-detail';
import { DgControl } from '../../../utils/dg-control';

export default class FdWizardDetailComponent extends FarrisDesignBaseNestedComponent {
    private wizardFill = true;
    constructor(component: any, options: any) {
        super(component, options);
        this.viewModelId = this.parent.viewModelId;
        this.componentId = this.parent.componentId;
    }
    getClassName(): any {
        return super.getClassName() + (this.wizardFill ? ' f-utils-fill-flex-column' : '');
    }
    getDefaultSchema(): any {
        return WizardDetailSchema;
    }

    getTemplateName(): string {
        return 'WizardDetail';
    }
    checkCanDeleteComponent(): boolean {
        return false;
    }
    checkCanMoveComponent(): boolean {
        return false;
    }
    // 初始化
    init(): void {
        this.wizardFill = this.parent.component.fill;
        super.init();
    }
    // 渲染模板
    render(): any {
        return super.render(this.renderTemplate('WizardDetail', {
            component: this.component,
            children: this.renderComponents(),
            nestedKey: this.nestedKey,
            wizardFill: this.wizardFill
        }));
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: WizardDetailProp = new WizardDetailProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后事件：默认监听样式类属性变更，并触发模板重绘
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {

        if (changeObject.categoryId === 'appearance' || changeObject.categoryId === 'behavior') {
            this.triggerRedraw();
        }

        // 变更步骤条后需要重构页面
        if (changeObject.propertyID === 'stepMessages') {
            this.rebuild();
        }
    }

    canAccepts(sourceElement: BuilderHTMLElement, targetElement?: BuilderHTMLElement): boolean {
        const serviceHost = this.options.designerHost;
        const dragResolveService = serviceHost.getService('DragResolveService');
        const resolveContext = dragResolveService.getComponentResolveContext(sourceElement, this);



        // 只接收外部容器
        return resolveContext.controlType === DgControl.ExternalContainer.type;

    }
}
