import { WizardSchema } from '../schema/schema';
import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { FarrisDesignBaseNestedComponent } from '@farris/designer-element';
import { WizardProp } from '../property/property-config';
import { FormPropertyChangeObject } from '../../../entity/property-change-entity';
import { ControlCssLoaderUtils } from '../../../utils/control-css-loader';
import FdWizardDetailContainerComponent from './fd-wizard-detail-container';

export default class FdWizardComponent extends FarrisDesignBaseNestedComponent {
    // 标记步骤项的实例
    wizardPageContainers = [];
    // 标记详情
    wizardPageDetails = [];
    // 标记当前步骤的id
    curWizardPageIndex = '';
    component;
    // 向导页标记，用于绑定子组件事件
    get wizardPageKey(): string {
        return `wizardPage-${this.key}`;
    }
    // 向导页步骤标记
    get wizardStepKey(): string {
        return `wizardStep-${this.key}`;
    }
    constructor(component: any, options: any) {
        super(component, options);
        // 加载css相关文件
        ControlCssLoaderUtils.loadCss('progress-steps.css');
        ControlCssLoaderUtils.loadCss('wizard.css');

        // 组件所属分类为“容器”
        this.category = 'container';
    }
    getClassName(): any {
        return super.getClassName() + (this.components.fill ? ' f-utils-fill-flex-column' : '');
    }
    /** 隐藏间距 */
    hideNestedPaddingInDesginerView() {
        return true;
    }

    getDefaultSchema(): any {
        return WizardSchema;
    }

    getTemplateName(): string {
        return 'Wizard';
    }

    // 初始化
    init(): void {
        this.components = [];
        this.wizardPageContainers = [];
        this.component.contents.forEach((wizardPage, index) => {
            // 缓存container的实例
            const options = Object.assign({}, this.options, { parent: this });
            const wizardPageIns = new FdWizardDetailContainerComponent(wizardPage, options);

            this.wizardPageContainers.push(wizardPageIns);
            // 创建Detail
            this.wizardPageDetails[index] = [];

            wizardPage.contents = wizardPage.contents || [];
            const pageOptions = Object.assign({}, this.options, { parent: this });
            wizardPage.contents.forEach(comp => {
                const wizardDetailCmp: any = this.createComponent(comp, pageOptions, null);
                this.wizardPageDetails[index].push(wizardDetailCmp);
            });
        });

        this.curWizardPageIndex = this.component.progressData.activeIndex || 0;

    }
    initWizardStep() {

    }

    // 渲染模板
    render(): any {
        this.initWizardStep();
        return super.render(this.renderTemplate('Wizard', {
            component: this.component,
            wizardPageDetailCmps: this.wizardPageDetails.map(wizardDetail => this.renderComponents(wizardDetail)),
            curWizardPageIndex: this.curWizardPageIndex,
            wizardPageKey: this.wizardPageKey,
            wizardStepKey: this.wizardStepKey
        }));
    }
    // 绑定事件
    attach(element: any): any {
        this.loadRefs(element, {
            [this.wizardPageKey]: 'multiple',
            [this.wizardStepKey]: 'single'
        });
        const superAttach: any = super.attach(element);

        const stepContainer = this.refs[this.wizardStepKey];
        const stepEls = stepContainer.querySelectorAll('.step');
        stepEls.forEach((stepEl, index) => {
            this.addEventListener(stepEl, 'click', (event) => {
                event.stopPropagation();
                if (this.curWizardPageIndex != index) {
                    this.curWizardPageIndex = index;
                    this.redraw();
                }
            });
        });

        // 绑定子组件
        this.refs[this.wizardPageKey].forEach((wizardPage, index) => {
            this.attachComponents(wizardPage, this.wizardPageDetails[index], this.component.contents[index].contents);
        });

        return superAttach;
    }

    /**
     * 组装属性面板配置数据
     */
    getPropertyConfig(): ElementPropertyConfig[] {
        const serviceHost = this.options.designerHost;
        const prop: WizardProp = new WizardProp(serviceHost, this.viewModelId, this.componentId);
        const propertyConfig: ElementPropertyConfig[] = prop.getPropConfig(this.component);
        return propertyConfig;
    }

    /**
     * 属性变更后事件：默认监听样式类属性变更，并触发模板重绘
     * @param changeObject 变更集
     */
    onPropertyChanged(changeObject: FormPropertyChangeObject): void {

        if (changeObject.categoryId === 'appearance') {
            this.triggerRedraw();
        }

        // 变更步骤条后需要重构页面
        if (changeObject.propertyID === 'stepMessages') {
            this.rebuild();
        }
    }
}
