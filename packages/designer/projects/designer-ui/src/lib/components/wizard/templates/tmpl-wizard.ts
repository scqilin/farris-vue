export default (ctx: any) => {
    // 向导步骤相关样式
    let wizardStepCls = '';
    switch (ctx.component.stepPosition) {
        case 'Top':
            wizardStepCls = 'f-component-wizard-step-top';
            break;
        case 'HeaderRight':
            wizardStepCls = 'f-component-wizard-step-headerright';

            break;
        case 'PageLeft':
            wizardStepCls = 'f-component-wizard-step-pageleft';
            break;
        case "None":
            wizardStepCls = 'f-component-wizard-step-none';
            break;
        default:
            wizardStepCls = '';
    }
    /**
     * 步骤条字符串
     */
    function getStepStructure() {
        let stepCtx = {
            direction: ctx.component.stepDirection ? ctx.component.stepDirection : 'horizontal',
            fill: false,
            extendTemplate: '',
            stepsCls: '',
            stepsData: ctx.component.progressData.stepMessages,
            activeIndex: ctx.curWizardPageIndex
        }
        // 列表样式
        let listCls = '';
        if (stepCtx.direction == 'horizontal') {
            listCls = 'f-progress-step-list';
            if (stepCtx.fill == true) {
                listCls += ' f-progress-step-horizontal-fill';
            }
        } else {
            listCls = 'f-progress-step-list-block';
            if (stepCtx.fill == true) {
                listCls += ' f-progress-step-vertical-fill';
            }
        }
        if (stepCtx.extendTemplate) {
            listCls += ' f-progress-step-has-extend';
        }
        listCls += ' ' + stepCtx.stepsCls;
        // 列表结构
        let listStr = '';
        // 
        for (let k = 0; k < stepCtx.stepsData.length; k++) {
            let curStep = stepCtx.stepsData[k];
            let liCls = k < stepCtx.activeIndex ? ' finish' : '';
            liCls += curStep.hasOwnProperty('class') && curStep['class'] ? ' ' + curStep['class'] : '';
            liCls += k == stepCtx.activeIndex ? ' active' : '';
            liCls += stepCtx.extendTemplate ? ' step-has-extend' : '';
            let stepRowCls = k < stepCtx.activeIndex ? ' step-finish' : '';
            stepRowCls += k == stepCtx.activeIndex ? ' step-active' : '';
            let iconStr = '';
            if (curStep.hasOwnProperty('icon') && curStep['icon']) {
                iconStr = `<span class="step-icon ${curStep['icon']}"></span>`;
            } else {
                if (k >= stepCtx.activeIndex) {
                    iconStr = `<span class="step-icon">${k + 1}</span>`;
                } else {
                    iconStr = `<span class="step-icon step-success k-icon k-i-check"></span>`;
                }
            }
            let lineStr = '';
            if (k < stepCtx.stepsData.length - 1) {
                lineStr = `<div class="f-progress-step-line ${k == stepCtx.activeIndex ? 'f-progress-step-line-success' : ''}">
                   <span class="triangle ${stepCtx.direction ? 'f-progress-step-vertical-triangle' : ''}"></span>
               </div>`;
            }
            listStr += `<li class="step ${liCls}" id="${curStep['id']}" style="cursor: pointer;">
               <div class="f-progressstep-row ${stepRowCls}">
                   <div class="f-progress-step-content">
                      ${iconStr}
                       <div class="f-progress-step-title">  
                          <p class="step-name ${k <= stepCtx.activeIndex ? 'step-name-success' : ''}" >${curStep.title}</p>
                       </div>   
                   </div>${lineStr} 
               </div>             
               ${stepCtx.extendTemplate}
           </li>`;
        }

        // 空数据
        let emptyStr = '';
        if (stepCtx.stepsData.length == 0) {
            emptyStr = '<div class="f-progress-step-empty">暂无数据</div>';
        }
        return `<div class="f-wizard-step"><div class="f-progress-step">
           <ul class="${listCls}" ref="${ctx.wizardStepKey}">
               ${listStr}
           </ul>
           ${emptyStr}
       </div></div>`;
    }
    let stepStr = getStepStructure();
    // 步骤条位置
    let wizardStepStr = '';
    if (ctx.component.stepPosition == 'Top') {
        wizardStepStr = stepStr;
    } else if (ctx.component.stepPosition == 'None') {
        wizardStepStr = `<div class="d-none">${stepStr}</div>`;
    }
    // Header字符串
    let headerStr = '';
    // 因为没有 HeaderTemplate 属性
    if (ctx.component.stepPosition == 'HeaderRight') {
        headerStr = `<div class="f-wizard-header">${stepStr}</div>`;
    }
    // content字符串
    let contentStr = `<div class="f-wizard-content">`;
    if (ctx.component.stepPosition == 'PageLeft') {
        contentStr += stepStr;
    }
    let wizardPagesStr = '';
    ctx.component.contents.forEach((wizardPage, index) => {
        let pageCls = index == ctx.curWizardPageIndex ? ' active' : '';
        wizardPagesStr += `<div class="f-component-wizard-page ${pageCls}" ref="${ctx.wizardPageKey}" dragref="${ctx.component.id}-container">
            ${ctx.wizardPageDetailCmps[index]}
        </div>`;
    });
    contentStr += `<div class="f-wizard-pages">${wizardPagesStr}</div></div>`;

    return `<div class="f-component-wizard ide-cmp-wizard ${ctx.component.fill ? 'f-component-wizard-fill' : ''}">
        ${wizardStepStr}
        <div class="f-wizard ${wizardStepCls}">
            ${headerStr}${contentStr}
        </div>
    </div>`;
};
