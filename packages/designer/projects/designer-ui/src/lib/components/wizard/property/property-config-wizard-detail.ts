import { ElementPropertyConfig } from '@farris/ide-property-panel';
import { CodeEditorComponent } from '@farris/designer-devkit';
import { ContainerUsualProp } from '../../container/common/property/container-property-config';
import { EventsEditorFuncUtils } from '../../../utils/events-editor-func';
import { FormPropertyChangeObject } from '../../../entity/property-change-entity';

export class WizardDetailProp extends ContainerUsualProp {

    getPropConfig(propertyData: any): ElementPropertyConfig[] {
        const propertyConfig: ElementPropertyConfig[] = [];

        // 基本信息属性
        const basicPropConfig = this.getBasicPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(basicPropConfig);

        // 外观属性
        const appearancePropConfig = this.getAppearancePropConfig();
        propertyConfig.push(appearancePropConfig);

        // 行为属性
        const behaviorPropConfig = this.getBehaviorPropConfig(propertyData, this.viewModelId);
        propertyConfig.push(behaviorPropConfig);

        // 事件属性
        const eventPropConfig = this.getEventPropertyConfig(propertyData, this.viewModelId);
        propertyConfig.push(eventPropConfig);

        return propertyConfig;
    }


    private getAppearancePropConfig(): ElementPropertyConfig {
        const commonProps = this.getCommonAppearanceProperties();
        return {
            categoryId: 'appearance',
            categoryName: '外观',
            properties: commonProps
        };
    }
    private getBehaviorPropConfig(propertyData: any, viewModelId: string): ElementPropertyConfig {
        const visibleProp = this.getVisiblePropertyEntity(propertyData, viewModelId);
        const advProperties = [
            visibleProp,
            {
                propertyID: 'headerTemplate',
                propertyName: '头部模板',
                propertyType: 'modal',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
            {
                propertyID: 'contentTemplate',
                propertyName: '内容模板',
                propertyType: 'modal',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
            {
                propertyID: 'footerTemplate',
                propertyName: '底部模板',
                propertyType: 'modal',
                editor: CodeEditorComponent,
                editorParams: {
                    language: 'html'
                }
            },
        ];
        return {
            categoryId: 'behavior',
            categoryName: '行为',
            properties: advProperties
        };
    }



    private getEventPropertyConfig(propertyData: any, viewModelId: string) {
        const domService = this.domService;
        const webCmdService = this.webCmdService;
        const formBasicService = this.formBasicService;
        const eventEditorService = this.eventEditorService;
        const eventList = [
            {
                label: 'pageDetailSelected',
                name: '详情页面选中后事件'
            }
        ];
        return {
            categoryId: 'eventsEditor',
            categoryName: '事件',
            hideTitle: true,
            properties: EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList),
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: FormPropertyChangeObject, data, parameters) {
                delete propertyData[viewModelId];
                EventsEditorFuncUtils.saveRelatedParameters(eventEditorService,domService, webCmdService, propertyData, viewModelId, eventList, parameters);
                this.properties = EventsEditorFuncUtils.formProperties(eventEditorService, formBasicService, domService, webCmdService, propertyData, viewModelId, eventList);

            }
        };
    }
}
