/**
 * 向导
 */
export const WizardSchema = {
    id: 'wizard-form',
    type: 'Wizard',
    visible: true,
    appearance: null,
    header: {
        headerTemplate: '',
        contents: []
    },
    progressData: {
        activeIndex: 0,
        stepMessages: []
    },
    stepClickable: true,
    storedIndex: false,
    fill: true,
    stepDirection: 'vertical',
    stepPosition: 'PageLeft',
    currentPageId: '',
    stateChange: '',
    contents: []
};
/**
 * 向导详情容器
 */
export const WizardDetailContainerSchema = {
    id: 'wizard-detail-container',
    type: 'WizardDetailContainer',
    name: 'wizard-detail-container',
    pageSelected: '',
    contents: [{
        id: 'wizard-detail',
        type: 'WizardDetail',
        appearance: null,
        headerTemplate: '',
        contentTemplate: '',
        footerTemplate: '',
        pageDetailSelected: '',
        contents: []
    }]
};
/**
 * 向导详情
 */
export const WizardDetailSchema = {
    id: 'wizard-detail',
    type: 'WizardDetail',
    visible: true,
    appearance: null,
    headerTemplate: '',
    contentTemplate: '',
    footerTemplate: '',
    pageDetailSelected: '',
    contents: []
};

