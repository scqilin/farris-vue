import FdWizardComponent from './component/fd-wizard';
import FdWizardDetailComponent from './component/fd-wizard-detail';
import FdWizardDetailContainerComponent from './component/fd-wizard-detail-container';
import { WizardSchema, WizardDetailContainerSchema, WizardDetailSchema } from './schema/schema';
import { ComponentExportEntity } from '@farris/designer-element';
import FdWizardTemplates from './templates/index-wizard';
import FdWizardDetailTemplates from './templates/index-wizard-detail';
import FdWizardDetailContainerTemplates from './templates/index-wizard-detail-container';

export const Wizard: ComponentExportEntity = {
    type: 'Wizard',
    component: FdWizardComponent,
    template: FdWizardTemplates,
    metadata: WizardSchema
};

export const WizardDetailContainer: ComponentExportEntity = {
    type: 'WizardDetailContainer',
    component: FdWizardDetailContainerComponent,
    template: FdWizardDetailContainerTemplates,
    metadata: WizardDetailContainerSchema
};

export const WizardDetail: ComponentExportEntity = {
    type: 'WizardDetail',
    component: FdWizardDetailComponent,
    template: FdWizardDetailTemplates,
    metadata: WizardDetailSchema
};
