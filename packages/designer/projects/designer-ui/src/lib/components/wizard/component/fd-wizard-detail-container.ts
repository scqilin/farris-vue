import { FarrisDesignBaseNestedComponent } from '@farris/designer-element';
import { WizardDetailContainerSchema } from '../schema/schema';
 
export default class FdWizardDetailContainerComponent extends FarrisDesignBaseNestedComponent {
    constructor(component: any, options: any) {
        super(component, options);
        this.viewModelId = this.parent.viewModelId;
        this.componentId = this.parent.componentId;
    }
    getDefaultSchema(): any {
        return WizardDetailContainerSchema;
    }

    getTemplateName(): string {
        return 'WizardDetailContainer';
    }
}
