import { AllComponents } from '../utils/component-list';
import { FarrisDesignBaseElement, IControlService } from '@farris/designer-element';
import { Injectable } from '@angular/core';
import { ComponentExportEntity } from '@farris/designer-element';
import { cloneDeep } from 'lodash-es';
import { DgControl } from '../utils/dg-control';
import { ControlEventPropertyIDList } from '../utils/control-event-prop';

@Injectable({
    providedIn: 'root'
})
export class ControlService extends IControlService {

    getDgControl() {
        return DgControl;
    }

    /**
     * 根据控件类型获取控件元数据
     * @param controlType 控件类型
     * @param isFromControlBox  是否为从控件工具箱拖拽生成控件
     */
    getControlMetaData(controlType: string, isFromControlBox = false, targetComponentInstance?: FarrisDesignBaseElement, controlFeature?: any) {
        const cmp = AllComponents[controlType] as ComponentExportEntity;
        if (!cmp) {
            return;
        }

        let metadata;
        // 有些场景下从工具箱拖拽生成控件时不止需要控件本身的schema结构，还需要外层包裹容器。例如标签页区域需要生成Container-Section-Tab三层结构，而不是只有Tab
        if (isFromControlBox && cmp.component.getMetadataInControlBox) {
            metadata = cloneDeep(cmp.component.getMetadataInControlBox(targetComponentInstance, controlFeature));
        } else {
            metadata = cloneDeep(cmp.metadata);
        }

        // 某些控件自带子级，并且需要保证子级的id唯一。例如toolbar
        if (cmp.uniqueMedataItems) {
            cmp.uniqueMedataItems(metadata);
        }

        return metadata;
    }

    /** 获取所有的事件名称 */
    getControlEventPropertyIDList() {
        return ControlEventPropertyIDList;
    }

}
