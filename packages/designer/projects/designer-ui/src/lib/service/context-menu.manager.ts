import { RowNode } from '@farris/ui-treetable';
import { FarrisDesignBaseComponent } from '@farris/designer-element';
import { DesignViewModelService, DomService, RefreshFormService, WebCmdService } from '@farris/designer-services';
import { IDesignerHost } from '@farris/designer-element';
import { NotifyService } from '@farris/ui-notify';
import { Injector } from '@angular/core';
import { ControlContextMenuItem } from '../entity/control-context-menu';


export class ContextMenuManager {
    /** 控件树节点行数据 */
    public rowNode: RowNode;

    /** form组件实例 */
    public cmpInstance: FarrisDesignBaseComponent;

    /** 操作表单DOM的工具类 */
    public domService: DomService;
    public webCmdService: WebCmdService;
    /** 操作表单设计时ViewModel的工具类 */
    public dgVMService: DesignViewModelService;

    public notifyService: NotifyService;

    public refreshFormService: RefreshFormService;

    public injector: Injector;


    /** 服务提供接口 */
    public serviceHost: IDesignerHost;

    constructor(cmp: FarrisDesignBaseComponent, rowNode: RowNode) {

        this.rowNode = rowNode;
        this.cmpInstance = cmp;

        this.serviceHost = cmp.options['designerHost'];
        this.injector = this.serviceHost.getService('Injector');
        this.webCmdService = this.serviceHost.getService('WebCmdService');
        this.domService = this.serviceHost.getService('DomService');
        this.dgVMService = this.serviceHost.getService('DesignViewModelService');
        this.refreshFormService = this.serviceHost.getService('RefreshFormService');
        this.notifyService = this.injector.get(NotifyService);

    }

    /**
     * 配置菜单点击事件
     * @param menuConfig 菜单项
     */
    addContextMenuHandle(menuConfig: ControlContextMenuItem[]) {
        menuConfig.forEach(menu => {
            if (typeof (menu) === 'object') {
                menu.handle = (e) => { this.contextMenuClicked(e); };

                if (menu.children) {
                    menu.children.forEach(childMenu => {
                        if (typeof (childMenu) === 'object') {
                            childMenu.handle = (e) => { this.contextMenuClicked(e); };
                            childMenu.parentMenuId = menu.id;
                        }
                    });
                }
            }
        });
    }

    /** 菜单点击事件，由各控件实现 */
    contextMenuClicked(e) {

    }
}
