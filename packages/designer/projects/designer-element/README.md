# DesignerElement

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## Code scaffolding

Run `ng generate component component-name --project designer-element` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project designer-element`.
> Note: Don't forget to add `--project designer-element` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build designer-element` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build designer-element`, go to the dist folder `cd dist/designer-element` and run `npm publish`.

## Running unit tests

Run `ng test designer-element` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
