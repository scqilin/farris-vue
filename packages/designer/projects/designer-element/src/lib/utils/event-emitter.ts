import * as em from 'eventemitter3';
import * as utils from './utils';
export default class EventEmitter extends em.EventEmitter {
    emit: any;
    _events: any;
    id: any;
    constructor(conf: any = {}) {
        super();
        const { loadLimit = 1000, eventsSafeInterval = 300 } = conf;


        const overloadHandler = () => {
            console.warn(`There were more than ${loadLimit} events emitted in ${eventsSafeInterval} ms. It might be caused by events' infinite loop`, this.id);
        };

        const dispatch = utils.observeOverload(overloadHandler, {
            limit: loadLimit,
            delay: eventsSafeInterval
        });

        this.emit = (event, data) => {
            super.emit(event, data);
            super.emit('any', data);

            dispatch();
        };
    }

    onAny = (fn) => {
        this.on('any', fn);
    }

    offAny = (fn) => {
        this.off('any', fn);
    }
}

