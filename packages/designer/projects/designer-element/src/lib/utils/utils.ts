
export function observeOverload(callback: any, options: any = {}): any {
    const { limit = 50, delay = 500 } = options;
    let callCount: number = 0;
    let timeoutID: number = 0;

    const reset = () => callCount = 0;

    return () => {
        if (timeoutID !== 0) {
            clearTimeout(timeoutID);
            timeoutID = 0;
        }

        timeoutID = <unknown>setTimeout(reset, delay) as number;

        callCount += 1;

        if (callCount >= limit) {
            clearTimeout(timeoutID);
            reset();
            return callback();
        }
    };
}

