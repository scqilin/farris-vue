import { IDesignerHost } from '../service/designer-host';
import FarrisDesignEventEmitter from '../utils/event-emitter';
import { ComponentSchema } from './builder-schema';

export class BuilderOptions {

    /** 存储渲染组件的方法 */
    hooks?: {
        renderComponent?: () => {},
        renderComponents?: () => {},
        attachComponent?: () => {},
        attachComponents?: () => {}
    };

    /** 外部传入的事件，暂时不用 */
    events?: FarrisDesignEventEmitter;

    /** 发送事件的前缀？ */
    namespace?: string;

    /** 外部传入的当前表单中禁用的控件，暂时不用 */
    disabled?: any[];

    /** 外部传入，删除控件时是否需要进行提示，暂时不用 */
    alwaysConfirmComponentRemoval?: boolean;

    /** 外部传入，拖拽添加控件时，是否需要弹出编辑框。若为false，则直接生成控件。考虑把这个属性放在各Component里 */
    noNewEdit?: boolean;

    /** 记录组件的父级组件 */
    parent?: any;

    /** 跳过渲染，目前看用于拖拽新增控件时的第一次render方法 */
    skipInit?: boolean;

    /** 表单子组件 */
    childComponents?: ComponentSchema[];

    /** 控件需要的服务 */
    designerHost?: IDesignerHost;
}
