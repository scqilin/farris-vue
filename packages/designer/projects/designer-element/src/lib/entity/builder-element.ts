import { FarrisDesignBaseComponent } from '../base-component/component';

/**
 * 设计器DOM元素结构
 */
export class BuilderHTMLElement extends HTMLElement {
    /** 记录各子元素对应的控件schema json的集合，用于container类dom节点 */
    public childrenContents?: any[];

    /** 记录element对应的component实例，用于单个component节点 */
    public componentInstance?: any;

    public component?: FarrisDesignBaseComponent;

}
