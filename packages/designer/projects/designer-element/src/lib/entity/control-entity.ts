import { FarrisDesignBaseComponent } from '../base-component/component';

/**
 * 描述控件类型、名称、图标的集合列表
 */
export class IDgControl {
    [controlType: string]: {
        type: string;
        name: string;
        icon?: string;
        dependentParentControl?: string;
    }
}

/**
 * 控件导出实体
 */
export class ComponentExportEntity {
    /** 控件类型 */
    type: string;

    /** 控件实例 */
    component?: any;

    /** 控件模板 */
    template?: any;

    /** 控件schema元数据 */
    metadata: any;

    /** 用于唯一化控件元数据子项的id */
    uniqueMedataItems?: (metadata: any) => void;
}



/**
 * UI 控件对外提供的服务类
 */
export abstract class IControlService {

    /** 获取控件列表 */
    abstract getDgControl(): any;

    /** 根据控件类型获取控件元数据 */
    abstract getControlMetaData(controlType: string, isFromControlBox?: boolean, targetComponentInstance?: FarrisDesignBaseComponent, controlFeature?: string): any;

    /** 获取所有的事件名称 */
    abstract getControlEventPropertyIDList(): any;
}

/**
 * 控件定制图标按钮，显示在右上角
 */
export class ComponentCustomToolbarConfig {
    /** 按钮标识，唯一 */
    id: string;

    /** 按钮提示文本 */
    title: string;

    class?: string;

    /** 按钮图标 */
    icon: string;

    /** 按钮点击事件 */
    click: (e?: PointerEvent, componentInstance?: FarrisDesignBaseComponent, parent?: FarrisDesignBaseComponent) => void;
}
