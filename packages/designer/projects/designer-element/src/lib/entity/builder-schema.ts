
export class ComponentSchema {

    /** 设计时使用 */
    key?: string;

    id?: string;

    type?: string;

    contents?: ComponentSchema[];

    // 其他属性
    [propName: string]: any;
}
