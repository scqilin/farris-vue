
export default (ctx: any) => {

    let iconPanel = '';
    let movePanel = '';
    let deletePanel = '';
    let selectPanel = '';
    let custonPanel = '';

    //  删除按钮
    if (ctx.canDelete) {
        deletePanel = `
            <div role="button" class="btn component-settings-button" title="删除" ref="removeComponent"
                componentid="${ctx.component.id}">
                <i class="f-icon f-icon-yxs_delete"></i>
            </div>`;
    }
    // 移动按钮
    if (ctx.canMove) {
        movePanel = `
            <div role="button" class="btn component-settings-button" title="移动" ref="moveComponent"
                componentid="${ctx.component.id}">
                <i cmpIcon="true" class="cmp_move f-icon f-icon-yxs_move"></i>
            </div>`;
    }

    // 父级按钮
    if (ctx.canSelectParent) {
        selectPanel = `
            <div role="button" class="btn component-settings-button" title="选中上层" ref="selectParentComponent"
                componentid="${ctx.component.id}">
                <i cmpIcon="true" class="cmp_move f-icon f-icon-enclosure_upload"></i>
            </div>`;
    }

    // 自定义按钮
    if (ctx.instance.customToolbarConfigs && ctx.instance.customToolbarConfigs.length) {
        ctx.instance.customToolbarConfigs.forEach(function (btnConfig, index) {
            custonPanel += `
            <div role="button" class="btn component-settings-button ${btnConfig.class || ''}" title="${btnConfig.title}"
                ref="${btnConfig.id}">
                <i class="${btnConfig.icon}"></i>
            </div>`;
        });
    }
    iconPanel = `
        <div class="component-btn-group" data-noattach="true">
            <div>
                ${deletePanel}
                ${movePanel}
                ${selectPanel}
                ${custonPanel}
            </div>
        </div>`;


    // const styles = ctx.styles ? (' style=' + ctx.styles) : '';

    return `
    <div ref="component" class="${ctx.classes}" style="${ctx.styles}" ${ctx.attributes} controltype="${ctx.component.type}" id="${ctx.id}-component">
        ${iconPanel}
        ${ctx.children}
    </div>
    `;
};
