export default (ctx: any) => {
    return `
    <div ref="form" class="d-flex flex-fill flex-column">
        ${ctx.form}
    </div>
        `;
};
