


export default (ctx: any) => {
    return `
        <div class="${ctx.classes} d-flex flex-fill flex-column" ref="webform" novalidate>${ctx.children}</div>
    `;
};
