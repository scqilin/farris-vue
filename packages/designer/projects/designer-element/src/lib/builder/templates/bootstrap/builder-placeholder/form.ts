


export default (ctx: any) => {
  return `
  <div class="drag-and-drop-alert alert alert-info no-drag w-100" style="height: 60px;display: flex;justify-content: center;align-items: center" role="alert" data-noattach="true"
      data-position="${ctx.position}">
      拖拽组件到这里
  </div>
  `;
};
