import builder from './builder';
import builderComponent from './builder-component';
import builderComponents from './builder-components';
import component from './component';
import components from './components';
import webform from './webform';
import builderPlaceholder from './builder-placeholder';

export default {
    builderComponent,
    builderComponents,
    component,
    components,
    webform,
    builderPlaceholder,
    builder
};
