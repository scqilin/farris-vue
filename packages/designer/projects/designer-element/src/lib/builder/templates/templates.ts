import templates from './index';
import { merge } from 'lodash-es';

export default class Templates {

  static templates = {};


  static setTemplates(uiTemplates) {
    const designerTemplates = templates.bootstrap;
    Templates.templates = merge(designerTemplates, uiTemplates);
  }

}
