
export default (ctx: any) => {
  return `${ctx.children.join('')}`;
};
