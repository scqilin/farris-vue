import Form from './form';
import UIComponents from './components/components';
import { BuilderOptions } from '../entity/builder-options';
import { ComponentSchema } from '../entity/builder-schema';
import { IDesignerHost } from '../service/designer-host';
import Templates from './templates/templates';
/**
 * 设计器可视化区域构造类
 */
export class DesignerBuilder extends Form {

    /**
     * 构造设计器
     * @param element 设计器父级DOM元素
     * @param builderSchema 根组件JSON Schema结构
     * @param childComponents 子组件JSON Schema集合
     */
    constructor(element: HTMLElement, builderSchema: ComponentSchema, childComponents: ComponentSchema[], uiComponents: any, designerHost: IDesignerHost) {
        // 存储ui组件
        UIComponents.setComponents(uiComponents);

        // 存储ui组件模板
        const uiTemplates = {};
        if (uiComponents) {
            Object.keys(uiComponents).forEach(cmpType => {
                uiTemplates[cmpType] = uiComponents[cmpType].template;
            });
        }
        Templates.setTemplates(uiTemplates);

        // 渲染设计器
        builderSchema = builderSchema || {};
        const options: BuilderOptions = {};
        options.childComponents = childComponents || [];
        options['designerHost'] = designerHost;
        super(element, builderSchema, options);
    }

}
