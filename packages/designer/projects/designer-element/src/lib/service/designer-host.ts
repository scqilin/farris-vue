/**
 * 设计时服务相关基类
 */
export abstract class IDesignerHost {
    abstract addService(serviceType: string, service: any): void;
    abstract getService(serviceType: string): any;
}

