import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef, OnInit, HostBinding } from '@angular/core';

@Component({
    selector: 'code-template-editor',
    templateUrl: './code-editor.component.html',
    styleUrls: ['./code-editor.component.css']
})
export class CodeEditorComponent implements OnInit {
    @Output() closeModal = new EventEmitter<any>();
    @Output() submitModal = new EventEmitter<any>();
    @Input() value: string;
    @Input() editorParams = { modalTitle: '', exampleCode: '', language: '', valueType: 'string', hideDocLink: false };
    @ViewChild('bindingFooter') modalFooter: TemplateRef<any>;

    get modalConfig() {
        return {
            title: this.getModalTitle(),
            width: 900,
            height: 500,
            showButtons: true
        };
    }

    codeEditorOptions = {
        theme: 'vs-dark',
        language: 'javascript',
        formatOnType: true,
        foldingStrategy: 'indentation',      // 显示缩进
        folding: true,                       // 启用代码折叠功能
        showFoldingControls: 'always',       // 默认显示装订线
        automaticLayout: true                // 监测父容器变化
    };

    monacoEditor;

    editingHtml: string;

    @HostBinding('class')
    cls = 'h-100 w-100 f-utils-overflow-hidden d-flex';

    onMonacoInit($event) {
        this.monacoEditor = $event.editor;
    }

    ngOnInit() {
        this.codeEditorOptions.language = this.editorParams.language;

        switch (this.editorParams.valueType) {
            case 'json': {
                this.editingHtml = this.value && JSON.stringify(this.value);
                break;
            }
            default: {
                this.editingHtml = this.value;
            }
        }

        if (this.editorParams.language === 'html') {
            this.cls = this.cls + ' flex-column';
        }


    }
    getModalTitle() {
        switch (this.editorParams.language) {
            case 'json': {
                return this.editorParams.modalTitle || 'JSON编辑器';

            }
            case 'javascript': {
                return this.editorParams.modalTitle || 'JavaScript编辑器';
            }
            case 'html': {
                return this.editorParams.modalTitle || 'HTML编辑器';

            }
            default: {
                return '代码编辑器';
            }
        }
    }

    /**
     * 确定
     */
    clickConfirm() {
        if (!this.editingHtml) {
            this.submitModal.emit({
                value: null,
                parameters: {
                    oldValue: this.value
                }
            });
            return;
        }

        let result = null;
        switch (this.editorParams.valueType) {
            case 'json': {
                result = JSON.parse(this.editingHtml);
                break;
            }
            default: {
                result = this.editingHtml;
            }
        }
        this.submitModal.emit({
            value: result,
            parameters: {
                oldValue: this.value
            }
        });
    }

    /**
     * 取消
     */
    clickCancel() {
        this.closeModal.emit();
    }
}
