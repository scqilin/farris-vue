import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionWithPropertyEditorComponent } from './collection-with-property-editor.component';

describe('CollectionWithPropertyEditorComponent', () => {
  let component: CollectionWithPropertyEditorComponent;
  let fixture: ComponentFixture<CollectionWithPropertyEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionWithPropertyEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionWithPropertyEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
