// import { TypeConverter } from '@farris/ide-property-panel';

import { TypeConverter } from "../../entity/property-entity";

export class BindingCustomEditorConverter implements TypeConverter {

    selectConfig: {
        idField: string,
        textField: string,
        options: any[]
    };
    constructor(selectConfig?: any) {
        this.selectConfig = selectConfig;
    }
    convertTo(data): any {
        if (typeof (data) === 'boolean') {
            return data;
        }
        // 枚举的场景
        if (typeof (data) === 'string') {
            if (this.selectConfig && this.selectConfig.options && this.selectConfig.idField) {
                const valueOption = this.selectConfig.options.find(option => option[this.selectConfig.idField] === data);
                if (valueOption) {
                    return valueOption[this.selectConfig.textField];
                } else {
                    return data;
                }
            } else {
                return data;
            }

        }
        if (data && data.type === 'Expression') {
            return '表达式';
        }
        if (data) {
            return data.type + ':' + data.path;
        }
        return data || '';
    }
}
