// import { TypeConverter } from '@farris/ide-property-panel';
import { FormBinding, FormBindingType, DomService } from '@farris/designer-services';
import { TypeConverter } from '../../entity/property-entity';

export class JavaScriptBooleanConverter implements TypeConverter {


    constructor(private domService: DomService, private binding: FormBinding) { }
    convertTo(data: any): any {
        if (typeof (data) === 'boolean') {
            return data ? '是' : '否';
        }
        if (typeof (data) === 'string') {
            return data;
        }
        if (data && data.type === 'Expression') {
            return this.getExprValue(data);
        }
        return data || '';
    }


    /**
     * 获取属性编辑器需要的展示值
     * @param expField 表达式配置字段
     * @param propertyValue 属性值
     */
    private getExprValue(propertyValue: any) {

        let expressionStr = null;
        if (propertyValue && propertyValue.expressionId) {
            const bindingFieldId = this.binding && this.binding.type === FormBindingType.Form && this.binding.field;
            const expField = this.domService.expressions && this.domService.expressions.find(e => e.fieldId === bindingFieldId);
            if (expField) {
                const originalExpConfig = expField.expression.find(exp => exp.id === propertyValue.expressionId);
                expressionStr = originalExpConfig && originalExpConfig.value;
            }


        }

        return expressionStr;
    }
}
