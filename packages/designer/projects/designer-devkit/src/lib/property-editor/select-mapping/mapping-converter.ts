// import { TypeConverter } from '@farris/ide-property-panel';

import { TypeConverter } from "../../entity/property-entity";

export class MappingConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (data) {
            const num = data.split(':');
            if (num.length > 0) {
                return '共 ' + (num.length - 1) + ' 项';
            }
            return '共 ' + num.length + ' 项';
        }
    }
}
