// import { TypeConverter } from '@farris/ide-property-panel';

import { TypeConverter } from "../../entity/property-entity";

export class ItemCollectionConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (data && data.length > 0) {
            return '共 ' + data.length + ' 项';
        }
        return '共 0 项';
    }
}
