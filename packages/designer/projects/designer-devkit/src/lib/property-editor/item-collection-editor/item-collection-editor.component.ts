import { Component, OnInit, Output, EventEmitter, Input, ViewChild, TemplateRef, HostBinding, NgZone } from '@angular/core';
import { NotifyService } from '@farris/ui-notify';
import { DatagridComponent } from '@farris/ui-datagrid';
import { IdService } from '@farris/ui-common';
import { cloneDeep } from 'lodash-es';

@Component({
    selector: 'app-item-collection-editor',
    templateUrl: './item-collection-editor.component.html'
})
export class ItemCollectionEditorComponent implements OnInit {
    @Output() closeModal = new EventEmitter<any>();
    @Output() submitModal = new EventEmitter<any>();

    @Input() value: any[];

    @Input() editorParams: any = {
        // 必填的列
        requiredFields: [],

        // 值唯一的列
        uniqueFields: [],

        // 列配置
        columns: [],

        // 模态框标题
        modalTitle: '',

        // 数据是否可以置空
        canEmpty: false,

        // 动态映射键值
        dynamicMappingKeys: false,

        // 动态映射键值时，枚举值的key
        valueField: '',

        // 动态映射键值时，枚举名称的key
        nameField: '',

        // 列表只有一列时，将结果集转化为字符串数组
        isSimpleArray: false,

        // 是否只读
        readonly: Boolean
    };

    @HostBinding('class')
    class = 'd-flex f-utils-fill-flex-column h-100';

    @ViewChild('itemsFooter') modalFooter: TemplateRef<any>;
    get modalConfig() {
        return {
            title: this.editorParams.modalTitle || '列表项集合编辑器',
            width: 900,
            height: 500,
            showButtons: true
        };
    }
    @ViewChild('dg') dg: DatagridComponent;

    // 当前编辑数据
    data = [];

    constructor(
        private notifyService: NotifyService,
        private idServ: IdService,
        private ngZone: NgZone) { }

    ngOnInit() {
        if (!this.value || this.value.length === 0 || this.editorParams.columns.length === 0) {
            this.data = [];
            return;
        }


        this.assembleData();



        // 默认选中行
        this.ngZone.runOutsideAngular(() => {
            setTimeout(() => {
                try {
                    this.dg.selectRow(this.data[0].hId);
                } catch (e) {
                    this.dg.clearSelections();
                }
            });
        });

        this.modalConfig.title = this.editorParams.modalTitle || '列表项集合编辑器';
    }

    private assembleData() {
        // 增加hid,作为列表的唯一id
        if (!this.editorParams.dynamicMappingKeys) {
            this.data = cloneDeep(this.value);
            if (this.editorParams.isSimpleArray) {
                this.data = this.data.map(d => {
                    return {
                        [this.editorParams.columns[0].field]: d
                    };
                });
            }
            this.data.forEach((v: any) => {
                v.hId = this.idServ.generate();
            });
            return;
        }

        // 动态指定枚举值和枚举名称key
        this.data = [];

        this.editorParams.valueField = this.editorParams.valueField ? this.editorParams.valueField.trim() : '';
        this.editorParams.nameField = this.editorParams.nameField ? this.editorParams.nameField.trim() : '';
        this.value.forEach(item => {
            if (this.editorParams.valueField && this.editorParams.nameField) {
                this.data.push(
                    {
                        hId: this.idServ.generate(),
                        value: item[this.editorParams.valueField],
                        name: item[this.editorParams.nameField]
                    }
                );
            }
        });

    }
    addItem() {

        if (this.editorParams.readonly) {
            return;
        }
        // 触发单元格结束编辑
        this.dg.endEditing();

        if (this.editorParams.dynamicMappingKeys) {
            if (!this.editorParams.valueField || !this.editorParams.valueField.trim()) {
                const co = this.editorParams.columns.find(c => c.field === 'value');
                this.notifyService.warning('请先填写' + co.title + '字段');
                return;
            }
            if (!this.editorParams.nameField || !this.editorParams.nameField.trim()) {
                const co = this.editorParams.columns.find(c => c.field === 'name');
                this.notifyService.warning('请先填写' + co.title + '字段');
                return;
            }
        }

        const newData = this.createData();
        const hId = this.idServ.generate();
        this.dg.appendRow({
            hId,
            ...newData
        });

        // 选中新增的行
        this.dg.selectRow(hId);
    }
    private createData(): any {
        const data = {};
        this.editorParams.columns.forEach(c => {
            data[c.field] = '';
        });
        return data;
    }

    removeItem() {
        if (this.editorParams.readonly) {
            return;
        }

        // 触发单元格结束编辑
        this.dg.endEditing();

        const row = this.dg.selectedRow;
        if (row) {
            const nextRowId = this.dg.data.length > row.index + 1 ? this.dg.data[row.index + 1].hId : null;

            this.dg.deleteRow(row.id);

            if (nextRowId) {
                this.dg.selectRow(nextRowId);
            } else {
                this.dg.clearSelections();
            }

        } else {
            this.notifyService.warning('请选中要删除的行');
        }


    }

    clickCancel() {
        this.closeModal.emit();
    }

    clickConfirm() {
        // 触发单元格结束编辑
        this.dg.endEditing();

        // 获取最新数组
        let latestData = [];
        this.dg.data.forEach(d => {
            const { hId, ...other } = d;
            latestData.push(other);
        });


        // 校验
        if (!this.checkBeforeSave(latestData)) {
            return;
        }

        if (!this.editorParams.dynamicMappingKeys) {
            if (this.editorParams.isSimpleArray) {
                const value = latestData.map(d => d[this.editorParams.columns[0].field]);
                this.submitModal.emit({ value });
            } else {
                this.submitModal.emit({ value: latestData });
            }
        } else {
            latestData = this.convertDynamicMappingkeys(latestData);
            this.submitModal.emit({
                value: latestData,
                parameters: {
                    valueField: this.editorParams.valueField.trim(),
                    nameField: this.editorParams.nameField.trim()
                }
            });

        }
    }



    /**
     * 保存前检查
     */
    checkBeforeSave(data: any[]): boolean {
        // ① 空数组
        if (!data || data.length === 0) {
            if (!this.editorParams.canEmpty) {
                this.notifyService.warning('请添加值。');
                return false;
            }
            return true;
        }

        const vali = this.editorParams;
        if (!vali) {
            return true;
        }

        // ② 非空，则校验每个的键值是否为空；（区分枚举值为布尔型的场景；排除枚举值为整数0的场景）
        const requiredFields = vali.requiredFields || [];
        for (const item of data) {
            for (const itemKey of Object.keys(item)) {
                const co = this.editorParams.columns.find(c => c.field === itemKey);
                if (co && co.editor && (co.editor.type === 'checkbox' || co.editor.type === 'switch')) {
                    if (requiredFields.includes(itemKey) && (item[itemKey] === null || item[itemKey] === undefined)) {
                        this.notifyService.warning(co.title + '不允许为空。');
                        return false;
                    }
                } else {
                    if (requiredFields.includes(itemKey) &&
                        (item[itemKey] === undefined || item[itemKey] === '' || item[itemKey] === null)) {
                        this.notifyService.warning(co.title + '不允许为空。');
                        return false;
                    }
                }

            }

        }

        // ③ 不允许重复
        const uniqueFields = vali.uniqueFields || [];

        for (const uniqueField of uniqueFields) {
            const fieldValues = data.map(e => e[uniqueField]);
            const keySet = new Set(fieldValues);
            const exclusiveKeys = Array.from(keySet);
            if (fieldValues.length !== exclusiveKeys.length) {
                const co = this.editorParams.columns.find(c => c.field === uniqueField);
                this.notifyService.warning(co.title + '不允许重复。');
                return false;
            }
        }

        // 校验动态key值
        if (this.editorParams.dynamicMappingKeys) {
            if (!this.editorParams.valueField || !this.editorParams.valueField.trim()) {
                const co = this.editorParams.columns.find(c => c.field === 'value');
                this.notifyService.warning(co.title + '字段不能为空。');
                return false;
            }
            if (!this.editorParams.nameField || !this.editorParams.nameField.trim()) {
                const co = this.editorParams.columns.find(c => c.field === 'name');
                this.notifyService.warning(co.title + '字段不能为空。');
                return false;
            }
        }

        return true;

    }

    private convertDynamicMappingkeys(latestData: any[]) {
        if (!latestData || latestData.length === 0) {
            return latestData;
        }
        const dynamicDataList = [];
        latestData.forEach(data => {
            const dynamicData = {};
            dynamicData[this.editorParams.valueField.trim()] = data.value;
            dynamicData[this.editorParams.nameField.trim()] = data.name;

            dynamicDataList.push(dynamicData);
        });

        return dynamicDataList;
    }
}
