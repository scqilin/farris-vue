// import { TypeConverter } from '@farris/ide-property-panel';

import { TypeConverter } from "../../entity/property-entity";

export class SelectMicroAppConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (typeof (data) === 'string') {
            return data || '';
        }
        if (data && data.relatedMicroApp) {
            return data.relatedMicroApp;
        }
        return '';
    }
}
