import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FarrisColorpickerPlusModule } from '@farris/colorpicker-plus';
import { ComboListModule } from '@farris/ui-combo-list';
import { ComboLookupModule } from '@farris/ui-combo-lookup';
import { FarrisFormsModule } from '@farris/ui-forms';
import { InputGroupModule } from '@farris/ui-input-group';
import { NumberSpinnerModule } from '@farris/ui-number-spinner';
import { PaginationModule } from '@farris/ui-pagination';
import { AdvancedStyleEditorComponent } from './advanced-style-editor.component';
import { BackgroundEditorComponent } from './background/background-editor.component';
import { BorderEditorComponent } from './border/border-editor.component';
import { MarginEditorComponent } from './margin/margin-editor.component';
import { SizeEditorComponent } from './size/size-editor.component';


@NgModule({
    declarations: [
        AdvancedStyleEditorComponent,
        MarginEditorComponent,
        BackgroundEditorComponent,
        BorderEditorComponent,
        SizeEditorComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        FarrisFormsModule,
        NumberSpinnerModule,
        ComboListModule,
        ComboLookupModule,
        BrowserAnimationsModule,
        InputGroupModule,
        PaginationModule,
        FarrisColorpickerPlusModule
    ],
    exports: [
        AdvancedStyleEditorComponent
    ],
    entryComponents: [
        AdvancedStyleEditorComponent
    ]
})
export class AdvancedStyleEditorModule {

}
