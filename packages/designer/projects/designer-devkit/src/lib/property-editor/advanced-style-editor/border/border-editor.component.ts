import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { cloneDeep } from 'lodash-es';

/**
 * 边框编辑器
 */
@Component({
    selector: 'app-border-editor',
    templateUrl: './border-editor.component.html',
    styleUrls: ['./border-editor.component.css']
})
export class BorderEditorComponent implements OnInit, OnChanges {

    /** 初始style列表 */
    @Input() originControlStyleList: string[];
    @Output() emitValueChange = new EventEmitter<string>();

    /** 是否显示详情面板 */
    showBorderDetail = false;

    /** 当前选中的边框方向 */
    selectedBorderDirection = 'all';

    /** 边框配置值 */
    border = {
        all: {
            style: 'none',
            width: 0,
            color: '#fff',
            colorConfig: {}
        },
        top: {
            style: 'none',
            width: 0,
            color: '#fff',
            colorConfig: {}
        },
        right: {
            style: 'none',
            width: 0,
            color: '#fff',
            colorConfig: {}
        },
        bottom: {
            style: 'none',
            width: 0,
            color: '#fff',
            colorConfig: {}
        },
        left: {
            style: 'none',
            width: 0,
            color: '#fff',
            colorConfig: {}
        }
    };
    /** 可选的线型列表 */
    borderStyleTypes = [
        { value: 'none', text: '无' },
        { value: 'solid', text: '实线' },
        { value: 'dotted', text: '点线' },
        { value: 'dashed', text: '虚线' },

    ];
    /** 预置边框颜色 */
    presetBorderColors = ['#000000', '#edf5ff', '#e6e9f0', '#dae9ff', '#2196f3', '#03a9f4', '#00bcd4', '#009688', '#4caf50', '#8bc34a', '#cddc39', '#ffeb3b', '#ffc107', '#ff9800', '#ff5722  ', '#9e9e9e'];

    /** 当前选中的圆角方向 */
    selectedRadiusDirection = 'all';

    /** 圆角值 */
    radius = {
        all: 0,
        'top-left': 0,
        'top-right': 0,
        'bottom-left': 0,
        'bottom-right': 0
    };
    constructor(private cd: ChangeDetectorRef) {

    }
    ngOnInit() {
        // this.initBorderStyle();
    }
    ngOnChanges(changes: SimpleChanges): void {
        this.initBorderStyle();
        this.initRadiusStyle();
    }

    initBorderStyle() {
        // 取统一配置
        this.getUnifiedBorderValue();

        // 取各方向的配置
        this.getBorderInDirection('top');
        this.getBorderInDirection('right');
        this.getBorderInDirection('bottom');
        this.getBorderInDirection('left');
    }

    private getBorderInDirection(direction: string) {

        const borderStyle = this.getBorderValueInDirection(direction, 'style');
        const borderWidth = this.getBorderValueInDirection(direction, 'width');
        const borderColor = this.getBorderValueInDirection(direction, 'color');

        this.border[direction] = {
            style: borderStyle || 'none',
            width: parseInt(borderWidth || '0', 10),
            color: borderColor || '#fff',
            colorConfig: {
                editorParams: {
                    preColor: borderColor || '#fff',
                    presets: this.presetBorderColors
                }
            }
        };
    }
    /**
     * 获取某个方向上某属性的最终生效值
     * 例如：左侧边框的线型值
     */
    private getBorderValueInDirection(direction: string, propKey: string) {
        let finalBorderStyle;

        // 以下四种场景，最终生效的线型值是按照样式出现的先后顺序，后面会覆盖前面的
        // border: 2px solid #fff; 提取solid
        // border-left: 2px solid #fff; 提取solid
        // border-left-style: solid;
        // border-style: solid;

        // TODO：考虑border-style: solid dotted;的场景


        this.originControlStyleList.forEach(item => {
            if (!item.includes('border')) {
                return;
            }
            const itemKey = item.split(':')[0];
            const itemValue = item.split(':')[1];
            const searchKeys = [
                `border-${propKey}`,
            ];
            if (propKey !== 'radius') {
                searchKeys.push(`border`);
            }
            if (direction) {
                searchKeys.push(
                    `border-${direction}-${propKey}`,
                    `border-${direction}`
                );
            }
            if (!searchKeys.includes(itemKey)) {
                return;
            }
            finalBorderStyle = this.getValueByPropKey(propKey, itemValue);
        });

        return finalBorderStyle;
    }

    /**
     * 根据border-left: 2px solid #fff; 提取solid
     * @param propKey  solid
     * @param styleValue border-left: 2px solid #fff
     */
    private getValueByPropKey(propKey: string, styleValue: string): string {
        if (!styleValue) {
            return;
        }
        let condition;
        switch (propKey) {
            case 'style': {
                condition = (item) => ['none', 'solid', 'dashed', 'dotted'].includes(item);
                break;
            }
            case 'width': case 'radius': {
                condition = (item) => item.includes('px');
                break;
            }
            case 'color': {
                condition = (item) => item.includes('#') || item.includes('rgba');
                break;
            }
        }
        if (condition) {
            return styleValue.split(' ').find(condition);
        }
    }
    /**
     * 获取border统一的值
     */
    private getUnifiedBorderValue() {
        const borderStyle = this.getBorderValueInDirection('', 'style');
        const borderWidth = this.getBorderValueInDirection('', 'width');
        const borderColor = this.getBorderValueInDirection('', 'color');

        this.border.all = {
            style: borderStyle || 'none',
            width: parseInt(borderWidth || '0', 10),
            color: borderColor || '#fff',
            colorConfig: {
                editorParams: {
                    preColor: borderColor || '#fff',
                    presets: this.presetBorderColors
                }
            }
        };
    }

    onChangeBorderDetailPanel() {
        this.showBorderDetail = !this.showBorderDetail;
    }
    /**
     * 切换边框方向
     */
    onChangeBorderDirection(key: string) {
        this.selectedBorderDirection = key;

        // 为了解决界面不检测变更的问题
        this.border = cloneDeep(this.border);
        this.cd.detectChanges();
    }

    /**
     * 改变某个方向的样式值
     * @param propKey 样式key，例如width/style
     */
    onChangeBorderInDirection(propKey: string) {
        let styleKey = `border-${propKey}`;

        if (this.selectedBorderDirection !== 'all') {
            styleKey = `border-${this.selectedBorderDirection}-${propKey}`;
        }

        // 组装style样式：例如border-right-width:4px或border-left-style:dotted
        let finalStyle = `${styleKey}:${this.border[this.selectedBorderDirection][propKey]}`;
        if (propKey === 'width') {
            finalStyle += 'px';
        }

        // 删掉旧值，并在最后追加新值
        const styleIndex = this.originControlStyleList.findIndex(item => item.split(':')[0] === styleKey);
        if (styleIndex > -1) {
            this.originControlStyleList.splice(styleIndex, 1);
        }
        this.originControlStyleList.push(finalStyle);

        this.emitValueChange.next(this.originControlStyleList.join(';'));

    }
    /**
     * 修改边框颜色
     */
    onChangeBorderColor(e) {
        const color = e.elementValue;
        this.border[this.selectedBorderDirection]['color'] = color;
        this.onChangeBorderInDirection('color');
    }

    /**
     * 初始圆角数据
     */
    initRadiusStyle() {
        const borderRadius = this.getBorderValueInDirection('', 'radius');

        this.radius.all = borderRadius;


        const borderTopLeftRadius = this.getBorderValueInDirection('top-left', 'radius');
        const borderTopRightRadius = this.getBorderValueInDirection('top-right', 'radius');
        const borderBottomLeftRadius = this.getBorderValueInDirection('bottom-left', 'radius');
        const borderBottomRightRadius = this.getBorderValueInDirection('bottom-right', 'radius');

        this.radius = {
            all: parseInt(borderRadius || '0', 10),
            'top-left': parseInt(borderTopLeftRadius || '0', 10),
            'top-right': parseInt(borderTopRightRadius || '0', 10),
            'bottom-left': parseInt(borderBottomLeftRadius || '0', 10),
            'bottom-right': parseInt(borderBottomRightRadius || '0', 10),
        };
    }

    /**
     * 切换圆角方向
     */
    onChangeRadiusDirection(direction: string) {
        this.selectedRadiusDirection = direction;

        // 为了解决界面不检测变更的问题
        this.radius = cloneDeep(this.radius);
        this.cd.detectChanges();
    }

    /**
     * 修改圆角值
     */
    onChangeRadiusValueInDirection() {
        let styleKey = `border-radius`;

        if (this.selectedRadiusDirection !== 'all') {
            styleKey = `border-${this.selectedRadiusDirection}-radius`;
        }

        // 组装style样式：例如border-bottom-right-radius:4px
        const finalStyle = `${styleKey}:${this.radius[this.selectedRadiusDirection]}px`;

        // 删掉旧值，并在最后追加新值
        const styleIndex = this.originControlStyleList.findIndex(item => item.split(':')[0] === styleKey);
        if (styleIndex > -1) {
            this.originControlStyleList.splice(styleIndex, 1);
        }
        this.originControlStyleList.push(finalStyle);

        this.emitValueChange.next(this.originControlStyleList.join(';'));
    }
}
