import { ChangeDetectorRef, Component, ComponentFactoryResolver, EventEmitter, Injector, OnInit, Output } from '@angular/core';
// import { PropertyEntity } from '@farris/ide-property-panel';
import { BsModalService, ModalOptions } from '@farris/ui-modal';
import { cloneDeep } from 'lodash-es';
import { PropertyEntity } from '../../entity/property-entity';
import { StyleEditorComponent } from '../style-editor/style-editor.component';

/**
 * 内联样式编辑器。不考虑class样式的影响
 */
@Component({
    selector: 'app-advanced-style-editor',
    templateUrl: './advanced-style-editor.component.html',
    styleUrls: ['./advanced-style-editor.component.css']
})
export class AdvancedStyleEditorComponent implements OnInit {
    @Output() valueChanged = new EventEmitter<any>();

    private _elementConfig: PropertyEntity;

    get elementConfig() {
        return this._elementConfig;
    }

    set elementConfig(value) {
        this._elementConfig = value;
    }

    /** 控件初始style样式 */
    private originControlStyle = '';
    originControlStyleList = [];

    constructor(
        private resolver: ComponentFactoryResolver,
        private injector: Injector,
        private modalService: BsModalService,
        private cd: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.initAppearance();
    }

    initAppearance() {
        const appearance = this.elementConfig && this.elementConfig.editorParams && this.elementConfig.editorParams.appearance || {};
        this.originControlStyle = appearance.style || '';
        const styleList = this.originControlStyle.split(';');
        this.originControlStyleList = cloneDeep(styleList.filter(item => !!item));
    }


    emitStyleValueChange(finalStyle: string) {
        this.originControlStyleList = finalStyle.split(';');
        if (!this.elementConfig) {
            return;
        }
        if (!this.elementConfig.editorParams) {
            this.elementConfig.editorParams = {
                appearance: {
                    style: ''
                }
            };
        }
        if (!this.elementConfig.editorParams.appearance) {
            this.elementConfig.editorParams.appearance = {
                style: ''
            };
        }
        this.elementConfig.editorParams.appearance.style = finalStyle;

        this.valueChanged.next({ $event: null, elementValue: null });

    }

    /**
     * 打开自定义编辑器
     */
    openCusomStyleEditor() {
        const compFactory = this.resolver.resolveComponentFactory(StyleEditorComponent);
        const compRef = compFactory.create(this.injector);
        compRef.instance.value = this.originControlStyleList.join(';');
        const modalConfig = compRef.instance.modalConfig as ModalOptions;
        modalConfig.buttons = compRef.instance.modalFooter;

        const modalPanel = this.modalService.show(compRef, modalConfig);
        compRef.instance.closeModal.subscribe(() => {
            modalPanel.close();
        });

        compRef.instance.submitModal.subscribe((value) => {
            this.emitStyleValueChange(value.value);
            this.cd.detectChanges();

            modalPanel.close();
        });
    }
}
