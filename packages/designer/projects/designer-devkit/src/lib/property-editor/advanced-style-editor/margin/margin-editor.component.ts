import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

/**
 * 外边距编辑器
 */
@Component({
    selector: 'app-margin-editor',
    templateUrl: './margin-editor.component.html',
    styleUrls: ['./margin-editor.component.css']
})
export class MarginEditorComponent implements OnInit, OnChanges {

    @Input() styleType: 'margin' | 'padding';
    /** 初始style列表 */
    @Input() originControlStyleList: string[];
    @Output() emitValueChange = new EventEmitter<string>();


    presettingMarginList = [
        { value: 'none', text: '系统默认' },
        { value: '20px', text: '大(20px)' },
        { value: '16px', text: '中(16px)' },
        { value: '12px', text: '小(12px)' },
        { value: '8px', text: '超小(8px)' },
        { value: '0px', text: '无(0px)' },
        { value: 'custom', text: '自定义' }
    ];

    marginData: {
        type: string,
        margin?: string,
        marginLeft?: string,
        marginRight?: string,
        marginTop?: string,
        marginBottom?: string
    };
    /** 是否显示外间距详情 */
    showMarginDetail = false;

    marginName = '';
    constructor(private cd: ChangeDetectorRef) { }


    // style:"margin:10px;margin-top:12px;margin-left:12px"
    ngOnInit() {
        this.marginName = this.styleType === 'margin' ? '外间距' : '内间距';
        // this.marginData = { type: 'custom' };

        // this.initMarginStyle();
    }
    ngOnChanges(changes: SimpleChanges): void {
        this.marginData = { type: 'custom' };
        this.initMarginStyle();
    }


    private initMarginStyle() {
        // 提取样式中的margin部分
        const margin = this.getStyleValue(`${this.styleType}`);
        let marginTop = this.getStyleValue(`${this.styleType}-top`);
        let marginBottom = this.getStyleValue(`${this.styleType}-bottom`);
        let marginLeft = this.getStyleValue(`${this.styleType}-left`);
        let marginRight = this.getStyleValue(`${this.styleType}-right`);

        marginTop = marginTop ? marginTop : margin;
        marginBottom = marginBottom ? marginBottom : margin;
        marginLeft = marginLeft ? marginLeft : margin;
        marginRight = marginRight ? marginRight : margin;


        this.marginData.type = 'custom';


        // 值都是null，则为类型为系统默认
        if (!margin && !marginTop && !marginBottom && !marginLeft && !marginRight) {
            this.marginData.type = 'none';
        } else {
            // 有值，且值相等，且值在预置列表里，则类型为间距值
            if (marginTop && marginTop === marginBottom && marginTop === marginLeft && marginTop === marginRight) {
                const presettingMarinKeys = this.presettingMarginList.map(item => item.value);
                if (presettingMarinKeys.includes(margin)) {
                    this.marginData.type = margin;
                }
            }
            // 有值，且值不相等，或者值不在预置列表里，则类型为自定义
            // this.marginData.type = 'custom';
        }
        this.marginData.marginTop = marginTop ? marginTop.replace('px', '') : null;
        this.marginData.marginBottom = marginBottom ? marginBottom.replace('px', '') : null;
        this.marginData.marginLeft = marginLeft ? marginLeft.replace('px', '') : null;
        this.marginData.marginRight = marginRight ? marginRight.replace('px', '') : null;

        // this.showMarginDetail = this.marginData.type !== 'none';

    }

    /**
     * 根据样式key值，获取value值
     * @param styleKey key值
     */
    private getStyleValue(styleKey: string) {
        const style = this.originControlStyleList.find(item => item && item.includes(styleKey + ':'));

        if (!style) {
            return;
        }
        return style.replace(styleKey + ':', '');

    }

    onChangeMarginType() {
        switch (this.marginData.type) {
            // 移除外边距
            case 'none': {
                this.showMarginDetail = false;
                this.marginData.marginTop = null;
                this.marginData.marginRight = null;
                this.marginData.marginBottom = null;
                this.marginData.marginLeft = null;
                this.marginData.margin = null;
                break;
            }
            // 自定义外边距
            case 'custom': {
                this.showMarginDetail = true;
                // this.marginData.margin = null;
                break;
            }
            // 固定边距，上下左右都一致
            default: {
                this.marginData.marginTop = this.marginData.type.replace('px', '');
                this.marginData.marginRight = this.marginData.marginTop;
                this.marginData.marginBottom = this.marginData.marginTop;
                this.marginData.marginLeft = this.marginData.marginTop;
                this.marginData.margin = this.marginData.marginTop;
            }
        }

        this.cd.detectChanges();
        this.emitStyleAfterMarginChanged();

    }
    onChangeMarginDetailPanel() {
        this.showMarginDetail = !this.showMarginDetail;
    }
    onChangeSingleMargin() {
        // 判断各方向的间距都有值，而且相等
        const hasSameMarginValue = this.marginData.marginTop + '' &&
            this.marginData.marginTop + '' === this.marginData.marginBottom + '' &&
            this.marginData.marginTop + '' === this.marginData.marginLeft + '' &&
            this.marginData.marginTop + '' === this.marginData.marginRight + '';

        // 值不同
        if (!hasSameMarginValue) {
            this.marginData.type = 'custom';
            this.emitStyleAfterMarginChanged();
            return;
        }
        // 值相同，且都为null
        if (!this.marginData.marginTop) {
            this.marginData.type = 'none';
            this.emitStyleAfterMarginChanged();
            return;
        }

        // 值相同，且在预置的列表里
        const presettingMarinKeys = this.presettingMarginList.map(item => item.value);
        if (presettingMarinKeys.includes(this.marginData.marginTop + 'px')) {
            this.marginData.type = this.marginData.marginTop + 'px';

            this.emitStyleAfterMarginChanged();
            return;
        }
        // 值相同，且不在预置的列表里
        this.marginData.type = 'custom';
        this.emitStyleAfterMarginChanged();

    }
    /**
     * 更改外间距后触发变更事件
     */
    emitStyleAfterMarginChanged() {
        const otherstyles = this.originControlStyleList.filter(item => !item.includes(this.styleType));

        let finalStyle = otherstyles.join(';');
        if (this.marginData.type !== 'none') {
            let marginStyle = '';
            if (this.marginData.marginTop && this.marginData.marginTop && this.marginData.marginTop === this.marginData.marginBottom
                && this.marginData.marginTop === this.marginData.marginLeft && this.marginData.marginTop === this.marginData.marginRight) {
                marginStyle += `${this.styleType}:${this.marginData.marginTop}px;`;
            } else {
                marginStyle += this.marginData.marginTop !== null ? `${this.styleType}-top:${this.marginData.marginTop}px;` : '';
                marginStyle += this.marginData.marginRight !== null ? `${this.styleType}-right:${this.marginData.marginRight}px;` : '';
                marginStyle += this.marginData.marginBottom !== null ? `${this.styleType}-bottom:${this.marginData.marginBottom}px;` : '';
                marginStyle += this.marginData.marginLeft !== null ? `${this.styleType}-left:${this.marginData.marginLeft}px;` : '';

            }

            finalStyle = marginStyle + finalStyle;
        }


        this.emitValueChange.next(finalStyle);

    }
}
