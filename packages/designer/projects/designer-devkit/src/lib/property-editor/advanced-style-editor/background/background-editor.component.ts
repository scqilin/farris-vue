import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

/**
 * 背景色编辑器
 */
@Component({
    selector: 'app-background-editor',
    templateUrl: './background-editor.component.html'
})
export class BackgroundEditorComponent implements OnInit, OnChanges {

    /** 初始style列表 */
    @Input() originControlStyleList: string[];
    @Output() emitValueChange = new EventEmitter<string>();


    typeList = [
        { value: 'none', text: '系统默认' },
        { value: 'custom', text: '自定义' }
    ];
    /** 当前颜色类型 */
    actualType = 'custom';

    /** 自定义颜色值 */
    customColor;

    presetColors = ['#fff', '#edf5ff', '#e6e9f0', '#dae9ff', '#673AB7', '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4', '#009688', '#4caf50', '#8bc34a', '#cddc39', '#ffeb3b', '#ffc107', '#ff9800', '#ff5722  ', '#9e9e9e'];
    /** 颜色选择器入参 */
    elementConfig;

    /** 是否显示详情 */
    showBackgroundDetail = false;

    ngOnInit() {
        // this.initBackgroundStyle();
    }
    ngOnChanges(changes: SimpleChanges): void {
        this.initBackgroundStyle();
    }

    initBackgroundStyle() {
        let background = this.getStyleValue('background');
        const backgroundColor = this.getStyleValue('background-color');

        // 提取background中颜色值
        if (background) {
            background = background.split(' ').find(item => item.includes('#') || item.includes('rgba'));
        }

        this.customColor = backgroundColor || background;

        this.actualType = !!this.customColor ? 'custom' : 'none';

        this.elementConfig = {
            editorParams: {
                preColor: this.customColor || '#fff',
                presets: this.presetColors
            }

        };
    }

    /**
     * 根据样式key值，获取value值
     * @param styleKey key值
     */
    private getStyleValue(styleKey: string) {
        const style = this.originControlStyleList.find(item => item && item.includes(styleKey + ':'));

        if (!style) {
            return;
        }
        return style.replace(styleKey + ':', '');

    }

    onChangeType() {

        if (this.actualType === 'none') {
            this.showBackgroundDetail = false;
            this.customColor = '#fff';
        } else {
            if (!this.customColor) {
                this.customColor = '#fff';
            }

            this.showBackgroundDetail = true;
        }

        this.elementConfig = {
            editorParams: {
                preColor: this.customColor || '#fff',
                presets: this.presetColors
            }

        };
        this.emitStyleAfterColorChanged();
    }

    onChangeCustomColor(e) {
        this.customColor = e.elementValue;
        this.actualType = 'custom';
        this.emitStyleAfterColorChanged();
    }
    /**
     * 更改颜色后触发变更事件
     */
    emitStyleAfterColorChanged() {
        const otherstyles = this.originControlStyleList.filter(item => !item.includes('background-color'));
        let finalStyle = otherstyles.join(';');

        let finalColor = this.actualType === 'none' ? null : this.customColor;

        if (finalColor) {
            finalColor = 'background-color:' + finalColor + ';';

            finalStyle = finalColor + finalStyle;
        }


        this.emitValueChange.next(finalStyle);

    }

    onChangeBackgroundDetailPanel() {
        this.showBackgroundDetail = !this.showBackgroundDetail;
    }
}
