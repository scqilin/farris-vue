import { HttpClient } from '@angular/common/http';
import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef, OnInit, HostBinding } from '@angular/core';

/**
 * 图标选择器-PC
 */
@Component({
    selector: 'icon-select-editor',
    templateUrl: './icon-select-editor.component.html',
    styleUrls: ['./icon-select-editor.component.css']
})
export class IconSelectEditorComponent implements OnInit {
    @Output() closeModal = new EventEmitter<any>();
    @Output() submitModal = new EventEmitter<any>();

    /** 现有的属性值，若需要图标样式，则是对象{iconName: string, iconClass:string}，若不需要样式，则是string */
    @Input() value: any;

    /** 是否同时定义图标样式 */
    @Input() editorParams = { needIconClass: true, iconUrl: '' };

    @ViewChild('bindingFooter') modalFooter: TemplateRef<any>;

    modalConfig = {
        title: '图标选择器',
        width: 900,
        height: 700,
        showButtons: true
    };
    @HostBinding('class')
    cls = 'd-flex flex-column h-100 f-utils-overflow-hidden';

    /** 图标样式 */
    iconClass = '';

    /** 可选的图标列表--全量 */
    iconList = [];

    /** 可选的图标列表--搜索后 */
    searchedIconList = [];

    /** 选中的图标 */
    selectedIcon = '';

    /** 搜索的图标名称 */
    searchIconName = '';

    /** 生效的图标 f-icon-** */
    fIconName = '';

    constructor(private http: HttpClient) { }

    ngOnInit() {
        this.getPresetIconList();

        if (!this.value) {
            return;
        }
        if (this.editorParams.needIconClass) {
            this.iconClass = this.value.iconClass || '';

            if (this.value.iconName) {
                this.selectedIcon = this.value.iconName.replace('f-icon-', '');
                this.fIconName = this.value.iconName;
            }
        } else {
            this.selectedIcon = this.value.replace('f-icon-', '');
            this.fIconName = this.value;
        }

    }

    /**
     * 获取图标列表
     * @param type type
     */
    private getPresetIconList() {
        const self = this;
        // 默认路径
        let iconUrl = 'assets/form/icon.json';

        if (this.editorParams.iconUrl) {
            iconUrl = this.editorParams.iconUrl;
        }
        this.http.get<any[]>(iconUrl + '?v=' + new Date().getTime()).subscribe(data => {
            data.forEach(item => {
                const itemArray = item.split('|');
                if (itemArray.length === 1) {
                    self.iconList.push(item);
                } else {
                    self.iconList = self.iconList.concat(itemArray);
                }

            });
            this.searchedIconList = Object.assign([], self.iconList);
        });


    }
    /**
     * 选择已有图标
     */
    clickIcon(icon: string) {
        this.selectedIcon = icon;
        this.fIconName = 'f-icon-' + icon;
    }

    /**
     * 手动修改图标样式
     */
    changeFIconName() {
        this.selectedIcon = this.fIconName.replace('f-icon-', '');
    }
    /**
     * 搜索
     */
    searchIcon() {
        if (this.searchIconName) {
            this.searchedIconList = this.iconList.filter(icon => icon.includes(this.searchIconName));
        } else {
            this.searchedIconList = Object.assign([], this.iconList);
        }
    }
    /**
     * 确定
     */
    clickConfirm() {
        if (this.editorParams.needIconClass) {
            this.submitModal.emit({
                value: {
                    iconName: this.fIconName,
                    iconClass: this.iconClass
                }
            });
        } else {
            this.submitModal.emit({ value: this.fIconName });
        }

    }

    /**
     * 取消
     */
    clickCancel() {
        this.closeModal.emit();
    }
}
