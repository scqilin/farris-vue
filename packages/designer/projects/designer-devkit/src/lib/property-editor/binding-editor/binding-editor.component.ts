import { Component, EventEmitter, HostBinding, Input, NgZone, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { DesignerHostSettingService, DesignViewModel, DesignViewModelService, DgControl, DomService, FormBinding, FormBindingType, FormSchemaEntityField$Type, FormVariableCategory, SchemaService, FormBasicService } from '@farris/designer-services';
import { NotifyOptions, NotifyService } from '@farris/ui-notify';
import { TreeNode, TreeTableComponent } from '@farris/ui-treetable';
import { merge } from 'lodash-es';

export class BindingEditorParam {
  /** 所属视图模型id */
  viewModelId: string;

  /** 控件类型 */
  controlType?: string;

  /** 允许选择的绑定类型：'Form'| 'RemoteVariable'|'LocaleVariable' */
  allowedBindingType?: string;

  /** 是否可以变更字段类型 */
  canChangeFieldType?: boolean;

  /** 是否将不可选的字段置灰 */
  unusedOnly?: boolean;

  /** DataGrid列绑定的字段类型 */
  gridFielType?: string;
}


/**
 * 用于控件绑定属性，可选字段或变量，选择后的字段和变量都会添加到viewModel.fields节点下
 */
@Component({
  selector: 'app-binding-editor',
  templateUrl: './binding-editor.component.html',
  styleUrls: ['./binding-editor.component.css']
})
export class BindingEditorComponent implements OnInit {
  /**
   * 关闭按钮事件
   */
  @Output() closeModal = new EventEmitter<any>();
  /**
   * 确定按钮事件
   */
  @Output() submitModal = new EventEmitter<any>();
  /**
   * 绑定信息
   */
  @Input() value: FormBinding;  // datagrid控件 fields字段
  /**
   * 编辑器属性
   */
  @Input() editorParams = new BindingEditorParam();
  /**
   * 绑定控件类型
   */
  @Input() bindingControlType: string;
  /**
   * 只选择未绑定的字段
   */
  @Input() unusedOnly = false;
  /**
   * 是否启用严格的匹配模式。例如严格模式下日期控件只能选择日期字段，非严格模式下，日期控件可以选择日期字段或字符串字段
   */
  @Input() useStrictMappingRule = false;

  /**
   * 绑定编辑器页尾组件
   */
  @ViewChild('bindingFooter') modalFooter: TemplateRef<any>;
  /**
   * 绑定编辑器默认弹出窗口显示属性
   */
  public modalConfig = {
    title: '绑定编辑器',
    width: 900,
    height: 700,
    showButtons: true,
    buttons: this.modalFooter
  };
  /**
   * 标记是否支持绑定变量
   */
  canBindVar = true;
  /**
   * 标记是否支持绑定字段
   */
  canBindForm = true;
  /**
   * 标记是否支持绑定本地变量
   */
  canBindLocaleVar = true;
  /**
   * 标记是否支持绑定后端变量
   */
  canBindRemoteVar = true;
  /**
   * 字段绑定方式
   */
  bindingType = 'Form';

  /**
   * 支持的绑定方式数组
   */
  bindingTypeEnumData = [];
  /**
   * 当前绑定树数据源
   */
  public currentBindingTreeDataSource = [];
  /**
   * 当前绑定树显示列
   */
  public currentBindingTreeColumns = [];
  /**
   * 绑定树
   */
  @ViewChild('treeTable') treeTable: TreeTableComponent;
  /**
   * 绑定字段树表信息
   */
  fieldsTreeData: TreeNode[] = [];
  /**
   * 绑定字段树显示列
   */
  fieldsTreeCols = [{ field: 'name', title: '名称' }, { field: 'bindingField', title: '绑定字段' }, { field: 'fieldType', title: '字段类型' }];
  /**
   * 本地变量树表信息
   */
  localVarTreeData: TreeNode[] = [];
  /**
   * 后端变量树信息
   */
  remoteVarTreeData: TreeNode[] = [];
  /**
   * 变量树显示列
   */
  variableTreeCols = [{ field: 'name', title: '名称' }, { field: 'code', title: '编号' }, { field: 'displayTypeName', title: '变量类型' }];

  @HostBinding('class')
  class = 'd-flex f-utils-fill-flex-column h-100';

  private displayedFieldSet: Set<string>;

  /** 是否变更了字段的类型 */
  private fieldTypeChanged = false;

  private schemaDOMMapping;

  /** 映射<控件类型,可选的字段类型> */
  private controlFieldMapping: any;

  constructor(
    private domService: DomService,
    private dgVMService: DesignViewModelService,
    private notifyService: NotifyService,
    private designerHostSettingService: DesignerHostSettingService,
    private schemaService: SchemaService,
    private formBasicService: FormBasicService,
    private ngZone: NgZone) { }

  ngOnInit() {
    this.schemaDOMMapping = this.designerHostSettingService.designerHost.getService('SchemaDOMMapping');

    const viewModelId = this.editorParams.viewModelId;
    const viewModel = this.domService.getViewModelById(viewModelId);

    // 字段
    this.fieldsTreeData = this.dgVMService.getAllFields2TreeByVMId(viewModelId);

    // 根组件的组件变量
    if (viewModel.parent) {
      const rootLocalVars = this.domService.getLocaleVariablesByViewModelId(viewModel.parent);
      if (rootLocalVars && rootLocalVars.length) {
        rootLocalVars[0].data.name = '根组件';
        this.localVarTreeData = this.localVarTreeData.concat(rootLocalVars);
      }
    }
    // 当前组件的组件变量
    const localVars = this.domService.getLocaleVariablesByViewModelId(viewModelId);
    this.localVarTreeData = this.localVarTreeData.concat(localVars);

    // 后端变量
    this.remoteVarTreeData = this.domService.getRemoteVariables();

    this.unusedOnly = this.unusedOnly || this.editorParams.unusedOnly;

    this.processDataSelectable();

    if (this.value) {
      this.init(this.value);
    } else {
      this.currentBindingTreeColumns = this.fieldsTreeCols;
      this.currentBindingTreeDataSource = this.fieldsTreeData;
    }

    if (this.editorParams.allowedBindingType) {
      const allowedBindingTypes = this.editorParams.allowedBindingType.split(',');
      if (!allowedBindingTypes.includes('Form')) {
        this.canBindForm = false;
      }
      if (!allowedBindingTypes.includes('RemoteVariable')) {
        this.canBindRemoteVar = false;
      }
      if (!allowedBindingTypes.includes('LocaleVariable')) {
        this.canBindLocaleVar = false;
      }

      if (!allowedBindingTypes.includes(this.bindingType)) {
        this.bindingType = allowedBindingTypes[0];
        this.changeBindingType();
      }
    }

    this.assembleBindingTypeEnumData();

  }


  init(value: FormBinding) {
    switch (value.type) {
      case FormBindingType.Form: {
        this.currentBindingTreeColumns = this.fieldsTreeCols;
        this.currentBindingTreeDataSource = this.fieldsTreeData;
        this.bindingType = 'Form';
        break;
      }
      case FormBindingType.Variable: {
        const variableCategory = this.getVaribleCategoryTypeById(value.field);

        if (variableCategory === FormVariableCategory.remote) {// 绑定remote变量
          this.currentBindingTreeColumns = this.variableTreeCols;
          this.currentBindingTreeDataSource = this.remoteVarTreeData;
          this.bindingType = 'RemoteVariable';
        } else { // 绑定组件本地变量
          this.currentBindingTreeColumns = this.variableTreeCols;
          this.currentBindingTreeDataSource = this.localVarTreeData;
          this.bindingType = 'LocaleVariable';
        }

      }
    }

    this.selectTreeNode(this.value.field);
  }

  /**
   * 组装当前支持的绑定方式数组，用于radiogroup
   */
  private assembleBindingTypeEnumData() {
    if (this.canBindForm) {
      this.bindingTypeEnumData.push({ value: 'Form', text: '绑定字段' });
    }
    if (this.canBindLocaleVar) {
      this.bindingTypeEnumData.push({ value: 'LocaleVariable', text: '绑定组件变量' });
    }
    if (this.canBindRemoteVar) {
      this.bindingTypeEnumData.push({ value: 'RemoteVariable', text: '绑定表单变量' });
    }
  }

  /**
   * 界面切换绑定类型响应方法
   * @param type 绑定类型
   */
  public changeBindingType() {
    switch (this.bindingType) {
      // 绑定字段
      case FormBindingType.Form: {
        // 更新绑定树数据源和显示列
        this.currentBindingTreeColumns = this.fieldsTreeCols;
        this.currentBindingTreeDataSource = this.fieldsTreeData;
        break;
      }
      // 绑定本地变量
      case 'LocaleVariable': {
        // 更新绑定树数据源和显示列
        this.currentBindingTreeColumns = this.variableTreeCols;
        this.currentBindingTreeDataSource = this.localVarTreeData;
        break;
      }
      // 绑定后端变量
      case 'RemoteVariable': {
        // 更新绑定树数据源和显示列
        this.currentBindingTreeColumns = this.variableTreeCols;
        this.currentBindingTreeDataSource = this.remoteVarTreeData;
      }
    }
    // 重置绑定树选择项
    this.treeTable.clearSelections();
  }
  /**
   * 定位已有记录
   * @param type 绑定树节点标识
   */
  public selectTreeNode(bindingNodeId: string) {
    this.ngZone.runOutsideAngular(() => {
      setTimeout(() => {
        try {
          this.treeTable.selectNode(bindingNodeId);
        } catch (e) {
          this.treeTable.clearSelections();
          // console.log('已绑定字段或变量不存在！');
        }
      });
    });
  }
  /**
   * 判断是否可选择当前待绑定字段或变量节点
   * @param bindingFieldTreeData 绑定树节点信息
   */
  public canChecked(bindingFieldTreeData: TreeNode) {
    switch (this.bindingType) {
      case 'Form': {
        if (bindingFieldTreeData.data.$type === 'SimpleField') {
          return true;
        } else {
          return false;
        }
      }
      default: {
        return bindingFieldTreeData.selectable;
      }
    }
  }
  /**
   * 取消按钮响应事件
   */
  public clickCancel() {
    this.closeModal.emit();
  }
  /**
   * 确定按钮响应事件
   */
  public clickConfirm() {
    // 校验绑定树选择信息
    if (!this.verifySelection()) {
      return;
    }
    // 记录原始绑定节点
    const originalBindingField = this.value ? this.value.field : null;
    // 读取绑定树当前选择项
    const selectedRow = this.treeTable.selectedRow;

    // 更新绑定结果。
    this.updateSelectionResult(selectedRow);

    // 更换绑定后，需要清除原绑定字段的表达式信息
    let expressionCleared = false;
    if (originalBindingField && originalBindingField !== this.value.field) {
      expressionCleared = this.clearExpresssionConfig(originalBindingField);
    }

    this.submitModal.emit({
      value: this.value,
      parameters: {
        selectedData: selectedRow.data,
        fieldTypeChanged: this.fieldTypeChanged,
        expressionCleared
      }
    });
  }

  /**
   * 校验绑定树选择信息
   */
  private verifySelection(): boolean {
    let result = true;
    // 校验绑定树选择信息
    if (!this.treeTable.selectedRow) {
      if (this.bindingType === FormBindingType.Form) {
        this.notifyService.warning({ msg: '请选择字段！', timeout: 3000 } as NotifyOptions);
      } else {
        this.notifyService.warning({ msg: '请选择变量！', timeout: 3000 } as NotifyOptions);
      }
      return false;
    }
    // tslint:disable-next-line: no-string-literal
    const viewModelId = this.editorParams['viewModelId'];
    // 读取绑定树当前选择项
    const selectedData = this.treeTable.selectedRow.data;
    if (this.value && this.value.field && this.value.field === selectedData.id) {
      return true;
    }

    // 校验当前选择字段是否已经被绑定，不允许多个控件重复绑定统一字段
    if (this.domService.checkViewModelDulplicated(viewModelId, selectedData.id)) {
      this.notifyService.warning('【' + selectedData.name + '】已绑定控件，请重新选择！');
      return false;
    }
    this.fieldTypeChanged = false;


    result = this.checkFieldTypeChanged();

    return result;
  }
  /**
   * 检查字段/变量类型是否有变更
   */
  private checkFieldTypeChanged(): boolean {
    if (!this.value) {
      return true;
    }
    let result = true;
    this.fieldTypeChanged = false;



    // 旧绑定字段类型
    let oldFieldType;

    // 旧绑定类型为复杂字段，适用于简单字段变为关联字段或udt的场景
    let isOldBindComplexField = false;

    if (this.value.type === FormBindingType.Form) {
      const schemaField = this.schemaService.getFieldByID(this.value.field);
      if (schemaField) {
        const oldField = this.getFieldType(schemaField, FormBindingType.Form);
        oldFieldType = oldField.fieldType;
        if (schemaField.$type === FormSchemaEntityField$Type.ComplexField) {
          isOldBindComplexField = true;
        }
      }

    } else {
      const varField = this.domService.getVariableById(this.value.field);
      if (varField) {
        oldFieldType = varField.type;
      }
    }

    // 新绑定类型
    const selectedData = this.treeTable.selectedRow.data;
    const { fieldType: newFieldType, fieldTypeName: newFieldTypeName } = this.getFieldType(selectedData, this.bindingType);

    // 检查当前控件类型是否支持新字段类型
    const allowedBindingFieldTypes = this.getFieldTypesByEditorType(this.editorParams.controlType);
    if (!allowedBindingFieldTypes || !allowedBindingFieldTypes.includes(newFieldType)) {
      this.notifyService.warning('不可以切换成' + newFieldTypeName + '类型字段，请重新选择！');
      result = false;
      return result;

    }
    if (isOldBindComplexField) {
      this.fieldTypeChanged = true;
    } else {
      if (oldFieldType && oldFieldType !== newFieldType) {
        if (!this.editorParams.canChangeFieldType) {
          this.notifyService.warning('不可以切换成' + newFieldTypeName + '类型字段，请重新选择！');
          result = false;
        } else {
          this.fieldTypeChanged = true;
        }
      }

    }


    return result;
  }

  private getFieldType(selectedData: any, bindingType: string) {
    if (selectedData.multiLanguage) {
      return { fieldType: 'multiLanguage', fieldTypeName: '多语言' };
    }

    const fieldType = bindingType === FormBindingType.Form ? selectedData.type.name : selectedData.type;
    const fieldTypeName = bindingType === FormBindingType.Form ? selectedData.type.displayName : selectedData.displayTypeName;
    return { fieldType, fieldTypeName };
  }

  /**
   * 更新当前选择结果
   */
  private updateSelectionResult(selectedRow: TreeNode) {
    const selectedData = selectedRow.data;
    const viewModelId = this.editorParams.viewModelId;

    // 如果未修改绑定，返回。
    if (this.value && selectedData.id === this.value.field) {
      return;
    }
    // 获取视图模型信息
    const dgViewModel = this.dgVMService.getDgViewModel(viewModelId);
    // 更新最新绑定信息
    let latestBinding: FormBinding;
    switch (this.bindingType) {
      case 'Form': {
        latestBinding = {
          type: FormBindingType.Form,
          path: selectedData.bindingField,
          field: selectedData.id,
          fullPath: selectedData.path
        };
        // 附件相关组件单独添加bindingPath属性
        if (DgControl.FilePreview && DgControl.FileUploadPreview && [DgControl.FilePreview.type, DgControl.FileUploadPreview.type].includes(this.editorParams.controlType)) {
          latestBinding.bindingPath = selectedData.bindingPath;
        }
        break;
      }
      case 'LocaleVariable': {
        latestBinding = {
          type: FormBindingType.Variable,
          path: selectedRow['viewModelId'] === viewModelId ? selectedData.code : 'root-component.' + selectedData.code,
          field: selectedData.id,
          fullPath: selectedData.code
        };
        break;
      }
      case 'RemoteVariable': {
        latestBinding = {
          type: FormBindingType.Variable,
          path: 'root-component.' + selectedData.code,
          field: selectedData.id,
          fullPath: selectedData.code
        };
        break;
      }
    }
    // 更新视图模型显示字段
    this.modifyViewModel(dgViewModel, this.bindingType, selectedData);
    // 更新绑定信息
    this.value = latestBinding;
  }
  /**
   * 更新视图模型显示字段
   * @param dgViewModel 当前绑定视图模型
   * @param bindingType 绑定类型
   * @param selectedData 当前选中绑定字段
   */
  private modifyViewModel(dgViewModel: DesignViewModel, bindingType: string, selectedData: any) {
    if (!dgViewModel) {
      return;
    }
    // 绑定字段
    if (bindingType === 'Form') {
      // 构造当前显示字段信息
      const currentSelectedField = merge({}, selectedData, { groupId: null, groupName: null });
      if (this.value && this.value.field) {

        // 查找原始字段
        const originalBindingField = dgViewModel.fields.find(f => f.id === this.value.field);
        dgViewModel.removeField([this.value.field]);
        if (originalBindingField) {
          // 保留原有分组信息、保留编辑器信息（待优化）、删除原DgViewModel字段
          const { groupId, groupName, editor } = originalBindingField;
          merge(currentSelectedField, { editor, groupId, groupName });
        }
      }
      // 向视图模型中添加新字段
      dgViewModel.addField(currentSelectedField);
      // 同步编辑器信息（目前只同步了编辑器信息，其他信息如必填只读等属性待优化后同步）
      dgViewModel.changeField(currentSelectedField.id, { editor: currentSelectedField.editor });
    } else {
      // 绑定变量
      const currentSelectedVar = {
        type: FormBindingType.Variable,
        id: selectedData.id,
        fieldName: selectedData.code,
        groupId: '',
        groupName: ''
      };
      if (this.value && this.value.field) {
        const originalBindingField = this.domService.getViewModelFieldById(this.editorParams.viewModelId, this.value.field);
        if (originalBindingField) {
          currentSelectedVar.groupName = originalBindingField.groupName;
          currentSelectedVar.groupId = originalBindingField.groupId;
        }

        // 删除原始绑定信息
        dgViewModel.removeField([this.value.field]);
      }
      // 向视图模型中添加新绑定
      this.domService.addViewModelField(this.editorParams.viewModelId, currentSelectedVar);
    }
  }

  private processDataSelectable() {
    if (!this.displayedFieldSet) {
      this.collectDisplayedFieldSet();
    }
    this.processDataSelectableByBindingType(this.fieldsTreeData, 'Form');
    this.processDataSelectableByBindingType(this.localVarTreeData, 'LocaleVariable');
    this.processDataSelectableByBindingType(this.remoteVarTreeData, 'RemoteVariable');
  }

  private processDataSelectableByBindingType(data: any[], bindingType: string) {
    if (!data || !data.length) {
      return;
    }
    data.forEach(item => {
      // 先判断类型是否匹配
      if (this.bindingControlType && item.selectable) {
        this.checkIsTypeMapping(item, bindingType);
      }
      if (this.unusedOnly && item.selectable) {
        if (this.displayedFieldSet.has(item.data.id)) {
          item.selectable = false;
        }
      }
      if (item.children) {
        this.processDataSelectableByBindingType(item.children, bindingType);
      }
    });
  }

  private checkIsTypeMapping(item: TreeNode, bindingType: string) {
    // 严格模式下，根据schema字段的editor类型判断
    if (this.useStrictMappingRule) {
      if (this.bindingControlType !== item.data.editor.$type) {
        item.selectable = false;
      }
    } else {
      // 非严格模式下，根据schema字段的type类型判断
      const matchingTypes = this.getFieldTypesByEditorType(this.bindingControlType);
      if (!matchingTypes || !matchingTypes.includes(this.getTypeFromBindingNode(item, bindingType))) {
        item.selectable = false;
      }
    }
  }

  private getTypeFromBindingNode(node: any, bindingType: string): string {
    if (node.data.multiLanguage) {
      return 'multiLanguage';
    }
    switch (bindingType) {
      case 'Form':
        return node.data && node.data.type && node.data.type.name;
      case 'LocaleVariable':
      case 'RemoteVariable':
        return node.data && node.data.type;
    }
  }

  /**
   * 获取实体中已在当前表单中被占用的字段
   */
  private collectDisplayedFieldSet() {
    const currentBindingId = this.value ? this.value.field : null;
    this.displayedFieldSet = new Set<string>();
    const componentNode = this.domService.getComponentByVMId(this.editorParams.viewModelId);

    /** 当前组件是否在侧边栏中 */
    const targetComponentInSidebar = this.checkComponentIfInSidebar(componentNode.id);

    /** 当前组件的类型 */
    let targetComponentType = componentNode.componentType;
    const targetViewModel = this.domService.getViewModelById(componentNode.viewModel);

    // 根组件和table组件内的输入控件与form内不能重复
    if (targetComponentType === 'Frame' || targetComponentType === 'table' || (targetComponentType && targetComponentType.startsWith('form'))) {
      targetComponentType = 'form';
    }
    this.domService.viewmodels.forEach(viewModel => {
      if (this.formBasicService.envType === 'mobileDesigner' && (viewModel.id !== this.editorParams.viewModelId)) {
        return;
      }
      if (!viewModel.fields || viewModel.fields.length === 0) {
        return;
      }
      const cmpNode = this.domService.getComponentByVMId(viewModel.id);
      let componentType = cmpNode.componentType;
      if (componentType === 'Frame' || componentType === 'table' || (componentType && componentType.startsWith('form'))) {
        componentType = 'form';
      }
      // 同类型的组件并且绑定同一个实体（form类、dataGrid类...）
      if (componentType !== targetComponentType || viewModel.bindTo !== targetViewModel.bindTo) {
        return;
      }

      /** 组件是否在侧边栏中 */
      const componentInSidebar = this.checkComponentIfInSidebar(cmpNode.id);

      // 若当前组件在侧边栏中，那么只收集同样在侧边栏中的字段。
      if (targetComponentInSidebar && componentInSidebar) {
        viewModel.fields.forEach(field => {
          if (field.id !== currentBindingId) {
            this.displayedFieldSet.add(field.id);
          }
        });
      }

      // 若当前组件不在侧边栏中，那么只收集不在侧边栏中的字段
      if (!targetComponentInSidebar && !componentInSidebar) {
        viewModel.fields.forEach(field => {
          if (field.id !== currentBindingId) {
            this.displayedFieldSet.add(field.id);
          }
        });

      }


    });

  }


  /**
   * 检查卡片组件是否在侧边栏中，在侧边栏中的字段可以与非侧边栏中的字段重复。
   */
  private checkComponentIfInSidebar(cmpNodeId: string) {

    const sidebar = this.domService.selectNode(this.domService.components[0],
      item => item.type === DgControl.Sidebar && DgControl.Sidebar.type && item.contents && item.contents.length &&
        item.contents.find(child => child.type === DgControl.ComponentRef.type && child.component === cmpNodeId));
    if (sidebar) {
      return true;
    }
    return false;
  }
  /**
   * 根据变量id获取变量分类
   * @param variableId 变量id
   */
  private getVaribleCategoryTypeById(variableId: string): string {
    const viewModels = this.domService.viewmodels;
    if (!viewModels || viewModels.length === 0) {
      return;
    }

    for (const viewModel of viewModels) {
      const variable = viewModel.states.find(v => v.id === variableId);
      if (variable) {
        return variable.category;
      }
    }


  }
  /***
   * 获取当前控件类型下可选的字段类型
   */
  private getFieldTypesByEditorType(editorType: string): string[] {
    if (!this.controlFieldMapping) {
      this.reverseFieldControlMapping();
    }
    // if (editorType === (DgControl.GridField && DgControl.GridField.type) || editorType === (DgControl.TreeGridField && DgControl.TreeGridField.type) && this.editorParams.gridFieldEditorType) {
    //   return this.controlFieldMapping[this.editorParams.gridFieldEditorType];

    // } else {
    //   return this.controlFieldMapping[editorType];
    // }

    if (editorType === (DgControl.GridField && DgControl.GridField.type) || editorType === (DgControl.TreeGridField && DgControl.TreeGridField.type)) {
      if (this.editorParams.gridFielType) {
        return [this.editorParams.gridFielType];
      }
    } else {
      return this.controlFieldMapping[editorType];
    }
  }

  /**
   * 将<字段类型,控件类型列表>的映射反转为<控件类型，可选的字段类型列表>
   */
  private reverseFieldControlMapping() {
    if (this.controlFieldMapping) {
      return;
    }
    this.controlFieldMapping = {};
    const fieldControlTypeMapping = this.schemaDOMMapping.fieldControlTypeMapping;

    for (const fieldType in fieldControlTypeMapping) {
      if (fieldControlTypeMapping.hasOwnProperty(fieldType)) {
        const controlTypes = fieldControlTypeMapping[fieldType];
        if (!controlTypes || !controlTypes.length) {
          continue;
        }
        controlTypes.forEach(item => {
          this.controlFieldMapping[item.key] = this.controlFieldMapping[item.key] || [];
          this.controlFieldMapping[item.key].push(fieldType);
        });
      }
    }

    // 追加多语控件
    this.controlFieldMapping.LanguageTextBox = ['multiLanguage'];
  }

  private clearExpresssionConfig(originalBindingField: string): boolean {

    if (this.domService.expressions && this.domService.expressions.length) {
      const expFieldIndex = this.domService.expressions.findIndex(e => e.fieldId === originalBindingField);
      if (expFieldIndex > -1) {
        this.domService.expressions.splice(expFieldIndex, 1);

        this.notifyService.warning('切换绑定后，请重新配置表达式');
        return true;
      }

    }
  }
}
