// import { TypeConverter } from '@farris/ide-property-panel';

import { TypeConverter } from "../../entity/property-entity";

export class BindingEditorConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        // 目前运行时定制需要展示字段名称
        if (data && data.fieldName) {
            return this.getTypeName(data.type) + ':' + data.fieldName;
        }
        if (data) {
            return data.type + ':' + data.path;
        }
        return '';
    }

    getTypeName(type: string) {
        switch (type) {
            case 'Form': { return '字段'; }
            case 'Variable': { return '变量'; }
        }
    }
}
