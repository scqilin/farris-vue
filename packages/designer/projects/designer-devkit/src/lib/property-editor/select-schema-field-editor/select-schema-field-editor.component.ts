import { Component, OnInit, Output, Input, EventEmitter, ViewChild, TemplateRef, HostBinding, NgZone } from '@angular/core';
import { NotifyService } from '@farris/ui-notify';
import { TreeNode, TreeTableComponent } from '@farris/ui-treetable';
import { DesignViewModelService } from '@farris/designer-services';

/**
 * 选择表单schema字段参数信息
 */
export class SelectSchemaFieldEditorParam {
    /** 视图模型id */
    viewModelId: string;
    /** 是否允许多选，默认false */
    isMultiSelect: boolean;
    /** 允许选择的字段类型，不传则默认全部可选 */
    allowedFieldTypes?: string[];
}
/**
 * 选择表单scheme字段
 */
@Component({
    selector: 'select-schema-field-editor',
    templateUrl: './select-schema-field-editor.component.html',
    styleUrls: ['./select-schema-field-editor.component.css']
})
export class SelectSchemaFieldEditorComponent implements OnInit {
    @Output() closeModal = new EventEmitter<any>();
    @Output() submitModal = new EventEmitter<any>();
    @Input() value;
    @Input() editorParams: SelectSchemaFieldEditorParam = { viewModelId: '', isMultiSelect: false };
    @ViewChild('modalFooter') modalFooter: TemplateRef<any>;

    @HostBinding('class')
    class = 'f-utils-fill-flex-column h-100 mx-3 border';

    modalConfig = {
        title: '字段选择器',
        width: 900,
        height: 500,
        showButtons: true,
        showMaxButton: false
    };

    // 树表数据
    treeData: TreeNode[] = [];

    // 树表列配置
    treeCols = [{ field: 'name', title: '名称' }, { field: 'label', title: '编号' }, { field: 'bindingPath', title: '绑定字段' }];

    // 树表实例
    @ViewChild('treeTable') treeTable: TreeTableComponent;

    constructor(
        private notifyService: NotifyService,
        private dgVMService: DesignViewModelService,
        private ngZone: NgZone
    ) { }

    ngOnInit(): void {
        if (!this.editorParams || !this.editorParams.viewModelId) {
            return;
        }
        this.treeData = this.dgVMService.getAllFields2TreeByVMId(this.editorParams.viewModelId);

        this.ngZone.runOutsideAngular(() => {
            setTimeout(() => {
                if (!this.value) { return; }
                try {
                    if (this.editorParams.isMultiSelect) {
                        this.treeTable.checkedNodes(this.value.split(','));
                    } else {
                        this.treeTable.selectNode(this.value);
                    }
                } catch (e) {
                    this.treeTable.clearSelections();
                    // console.log('已绑定字段不存在！');
                }
            });
        });
    }

    /**
     * 取消
     */
    clickCancel() {
        this.closeModal.emit();
    }
    /**
     * 确定
     */
    clickConfirm() {
        // 单选
        if (!this.editorParams.isMultiSelect) {
            if (!this.treeTable.selectedRow) {
                this.notifyService.warning('请选择字段');
                return;
            }

            const selectedData = this.treeTable.selectedRow.data;

            this.submitModal.emit({ value: selectedData.bindingPath });
            return;
        }

        // 多选
        if (this.treeTable.checkeds && !this.treeTable.checkeds.length) {
            this.notifyService.warning('请选择字段');
            return;
        }
        const checkedData = this.treeTable.checkeds.map(c => c.data.bindingPath);

        this.submitModal.emit({ value: checkedData.join(',') });

    }


}
