import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportCmpComponent } from './import-cmp.component';

describe('ImportCmpComponent', () => {
  let component: ImportCmpComponent;
  let fixture: ComponentFixture<ImportCmpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportCmpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
