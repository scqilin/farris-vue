// import { TypeConverter } from '@farris/ide-property-panel';

import { TypeConverter } from "../../entity/property-entity";

export class ImportCmpConverter implements TypeConverter {

    constructor() { }
    convertTo(data): string {
        if (data) {
            return data;
        }
        return '';
    }
}
