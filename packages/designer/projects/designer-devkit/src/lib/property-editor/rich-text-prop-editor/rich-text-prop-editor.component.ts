import {
  Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild
} from '@angular/core';
import { EditorComponent } from '@farris/ui-editor';

@Component({
  selector: 'rich-text-prop-editor',
  templateUrl: './rich-text-prop-editor.component.html',
  styleUrls: ['./rich-text-prop-editor.component.css'],
})
export class RichTextPropEditor implements OnInit {

  @Output() closeModal = new EventEmitter<any>();
  @Output() submitModal = new EventEmitter<any>();
  @Input() value: string;

  @Input('editorParams')
  private editorParams: any

  @ViewChild('editor')
  private editor: EditorComponent;

  @ViewChild('bindingFooter') modalFooter: TemplateRef<any>;

  ngOnInit() {
    this.editorParams.value = this.value || '';
    // this.editor.registerOnChange((contentStr = '') => {
    //   this.contentString = contentStr
    // });
  }

  get modalConfig() {
    return {
      title: '文本编辑',
      width: 900,
      height: 500,
      showButtons: true
    };
  }

  /**
   * 确定
   */
  clickConfirm() {
    this.submitModal.emit({ value: this.value });
  }

  /**
   * 取消
   */
  clickCancel() {
    this.closeModal.emit();
  }

}
