import { NgModule } from '@angular/core';
import { BindingEditorComponent } from './property-editor/binding-editor/binding-editor.component';
import { StyleEditorComponent } from './property-editor/style-editor/style-editor.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DatagridModule } from '@farris/ui-datagrid';
import { DatagridEditorsModule, EditorProviders } from '@farris/ui-datagrid-editors';
import { LoadingModule } from '@farris/ui-loading';
import { TreeTableModule } from '@farris/ui-treetable';
import { NotifyModule } from '@farris/ui-notify';
import { EditorModule } from '@farris/ui-editor';
import { FarrisFormsModule } from '@farris/ui-forms';
import { ControlBoxComponent } from './components/control-box/control-box.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ItemCollectionEditorComponent } from './property-editor/item-collection-editor/item-collection-editor.component';
import { NumberSpinnerModule } from '@farris/ui-number-spinner';
import { SwitchModule } from '@farris/ui-switch';
import { ComboLookupModule } from '@farris/ui-combo-lookup';
import { ComboListModule } from '@farris/ui-combo-list';
import { InnerComponentCreatorComponent } from './components/inner-component-creator/inner-component-creator.component';
import { ProgressStepModule } from '@farris/ui-progress-step';
import { CollectionWithPropertyEditorComponent } from './property-editor/collection-with-property-editor/collection-with-property-editor.component';
import { IconSelectEditorComponent } from './property-editor/icon-select-editor/icon-select-editor.component';
import { CodeEditorComponent } from './property-editor/code-editor/code-editor.component';
import { ExpressionEditorComponent } from './property-editor/expression-editor/expression-editor.component';
// import { PropertyPanelModule } from '@farris/ide-property-panel';
import { AngularMonacoEditorConfig } from './components/angular-editor/model/config';
import { AngularMonacoEditorModule } from './components/angular-editor/editor.module';
import { JavaScriptBooleanEditorComponent } from './property-editor/javascript-boolean-editor/javascript-boolean-editor.component';
import { RichTextPropEditor } from './property-editor/rich-text-prop-editor/rich-text-prop-editor.component';
import { SelectSchemaFieldEditorComponent } from './property-editor/select-schema-field-editor/select-schema-field-editor.component';
import { UpdateSchemaComponent } from './components/update-schema/update-schema.component';
import { FarrisTabsModule } from '@farris/ui-tabs';
import { PopoverModule } from '@farris/ui-popover';
import { FieldManagerComponent } from './components/field-manage/field-manager.component';
import { FieldTreeTableComponent } from './components/field-manage/field-treetable/field-treetable.component';
import { BindingCustomEditorComponent } from './property-editor/binding-custom-editor/binding-custom-editor.component';
import { FarrisDatePickerModule } from '@farris/ui-datepicker';
import { SelectMappingComponent } from './property-editor/select-mapping/select-mapping.component';
import { SelectMicroAppComponent } from './property-editor/select-microapp/select-microapp.component';
import { ImportCmpComponent } from './property-editor/import-cmp/import-cmp.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ControlTreeComponent } from './components/control-tree/control-tree.component';
import { MobileControlTreeComponent } from './components/control-tree/mobile-control-tree.component';
import { IdeTreeModule } from './components/ide-tree/ide-tree.module';
import { SchemaTreeComponent } from './components/schema-tree/schema-tree.component';
// import { ExpressionModule } from '@gsp-svc/expression';
import { InputGroupModule } from '@farris/ui-input-group';
import { PaginationControlsComponent, PaginationModule } from '@farris/ui-pagination';
import { AdvancedStyleEditorComponent } from './property-editor/advanced-style-editor/advanced-style-editor.component';
import { AdvancedStyleEditorModule } from './property-editor/advanced-style-editor/advanced-style.editor.module';
import { SaveAsTemplateEditorComponent } from './property-editor/save-as-template-editor/save-as-template-editor.component';
import { ComponentTemplateManageComponent } from './components/control-box/component-template-manage/component-template-manage.component';

const monacoConfig: AngularMonacoEditorConfig = {
  baseUrl: 'assets',
  defaultOptions: {
    theme: 'vs-dark',
    language: 'json',
    formatOnType: true,
    foldingStrategy: 'indentation',      // 显示缩进
    folding: true,                       // 启用代码折叠功能
    showFoldingControls: 'always',       // 默认显示装订线
    automaticLayout: true,               // 监测父容器变化
    scrollBeyondLastLine: false
  }
};

@NgModule({
  declarations: [
    StyleEditorComponent,
    BindingEditorComponent,
    ControlBoxComponent,
    SchemaTreeComponent,
    ItemCollectionEditorComponent,
    InnerComponentCreatorComponent,
    CollectionWithPropertyEditorComponent,
    IconSelectEditorComponent,
    CodeEditorComponent,
    ExpressionEditorComponent,
    JavaScriptBooleanEditorComponent,
    RichTextPropEditor,
    SelectSchemaFieldEditorComponent,
    UpdateSchemaComponent,
    FieldManagerComponent,
    FieldTreeTableComponent,
    BindingCustomEditorComponent,
    SelectMappingComponent,
    SelectMicroAppComponent,
    ImportCmpComponent,
    ControlTreeComponent,
    MobileControlTreeComponent,
    SaveAsTemplateEditorComponent,
    ComponentTemplateManageComponent
  ],
  imports: [
    EditorModule.forRoot('/platform/common/web/assets/tinymce/tinymce.min.js'),
    CommonModule,
    FormsModule,
    TreeTableModule,
    LoadingModule.forRoot({
      message: '加载中，请稍候...'
    }),
    DatagridEditorsModule,
    DatagridModule.forRoot([
      ...EditorProviders
    ]),
    NotifyModule,
    FarrisFormsModule,
    NumberSpinnerModule,
    SwitchModule,
    ComboListModule,
    ComboLookupModule,
    ContextMenuModule.forRoot({
      autoFocus: false,
      useBootstrap4: true,
    }),
    ProgressStepModule,
    // PropertyPanelModule,
    AngularMonacoEditorModule.forRoot(monacoConfig),
    FarrisTabsModule,
    PopoverModule,
    FarrisDatePickerModule,
    BrowserAnimationsModule,
    IdeTreeModule,
    // ExpressionModule,
    InputGroupModule,
    PaginationModule,
    AdvancedStyleEditorModule
  ],
  exports: [
    ControlBoxComponent,
    SchemaTreeComponent,
    ItemCollectionEditorComponent,
    InnerComponentCreatorComponent,
    CollectionWithPropertyEditorComponent,
    IconSelectEditorComponent,
    CodeEditorComponent,
    ExpressionEditorComponent,
    JavaScriptBooleanEditorComponent,
    RichTextPropEditor,
    SelectSchemaFieldEditorComponent,
    AngularMonacoEditorModule,
    FieldManagerComponent,
    BindingCustomEditorComponent,
    SelectMappingComponent,
    SelectMicroAppComponent,
    ImportCmpComponent,
    ControlTreeComponent,
    MobileControlTreeComponent,
    UpdateSchemaComponent,
    AdvancedStyleEditorComponent,
    SaveAsTemplateEditorComponent
  ],
  entryComponents: [
    StyleEditorComponent,
    BindingEditorComponent,
    ItemCollectionEditorComponent,
    InnerComponentCreatorComponent,
    CollectionWithPropertyEditorComponent,
    IconSelectEditorComponent,
    CodeEditorComponent,
    ExpressionEditorComponent,
    JavaScriptBooleanEditorComponent,
    RichTextPropEditor,
    SelectSchemaFieldEditorComponent,
    UpdateSchemaComponent,
    FieldManagerComponent,
    BindingCustomEditorComponent,
    SelectMappingComponent,
    SelectMicroAppComponent,
    ImportCmpComponent,
    MobileControlTreeComponent,
    PaginationControlsComponent,
    SaveAsTemplateEditorComponent,
    ComponentTemplateManageComponent
  ]
})
export class FarrisDesignerDevkitModule {

}
