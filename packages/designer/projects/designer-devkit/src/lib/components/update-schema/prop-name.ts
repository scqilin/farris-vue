export class SchemaPropName {
    static propNameMap = {
        $type: '类型',
        id: 'ID',
        originalId: '原始ID',
        code: '编号',
        name: '名称',
        label: '标签',
        bindingField: '绑定字段',
        defaultValue: '默认值',
        require: '必填',
        readonly: '只读',
        type: '类型属性',
        length: '长度',
        precision: '精度',
        editor: '编辑器属性',
        format: '格式',
        valueType: '值类型',
        enumValues: '枚举项',
        dataSource: '帮助数据源',
        textField: '文本字段',
        valueField: '值字段',
        displayType: '显示类型',
        helpId: '帮助元数据ID',
        mapFields: '帮助映射',
        path: '路径',
        editable: '允许编辑',
        multiLanguage: '多语'
    };

    static getName(propCode: string) {
        return this.propNameMap[propCode] || propCode;
    }
}


