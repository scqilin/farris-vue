
/**
 * 表单schema更新后，同步控件的服务
 */
export abstract class IRefreshAfterChangedService {

    /** 表单schema更新后，同步控件的逻辑 */
    public abstract refreshAfterSchemaChange(changes: SchemaChangeEntity);
}

/**
 * 变更实体
 */
export class SchemaChangeEntity {
    [fieldId: string]: [{
        propPath: string;
        newValue: any;
    }]
}