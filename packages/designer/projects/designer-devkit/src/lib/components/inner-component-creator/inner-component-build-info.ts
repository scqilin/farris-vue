import { FormSchemaEntity } from "@farris/designer-services";

export class ComponentBuildInfo {
    componentId: string;
    componentIdPrefix: string;
    componentName: string;
    componentType?: string;
    componentAppearance?: string;
    resolvedComponentType: string;
    formColumns?: number;
    parentContainerId?: string;
    parentContainerType?: string;
    editable?: boolean; // 目前列表类型组件只能选择子表，并且均可编辑
    selectedFields: any[];
    bindTo: string;
    dataSource: string;
    selectedEntity?: FormSchemaEntity;
}
