import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerComponentCreatorComponent } from './inner-component-creator.component';

describe('InnerComponentCreatorComponent', () => {
  let component: InnerComponentCreatorComponent;
  let fixture: ComponentFixture<InnerComponentCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerComponentCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerComponentCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
