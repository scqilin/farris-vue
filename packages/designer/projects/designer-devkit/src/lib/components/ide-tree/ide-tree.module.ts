import { NotifyModule } from '@farris/ui-notify';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TreeTableModule } from '@farris/ui-treetable';
import { IdeTreeComponent } from './ide-tree.component';
import { IdeTreeToolbarComponent } from './tool-bar/ide-tree-toolbar.component';
import { IdeTreeNodeDirective } from './ide-treenode.directive';

@NgModule({
    declarations: [
        IdeTreeComponent,
        IdeTreeToolbarComponent,
        IdeTreeNodeDirective
    ],
    imports: [
        CommonModule,
        FormsModule,
        TreeTableModule,
        NotifyModule.forRoot()
    ],
    exports: [IdeTreeComponent]
})
export class IdeTreeModule { }
