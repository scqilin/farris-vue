import { IdeTreeComponent } from './ide-tree.component';
import { Directive, Input, ElementRef, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
// import { fromEvent, merge } from 'rxjs';
// import { async } from 'rxjs/internal/scheduler/async';
// import { buffer, debounceTime, filter, map, throttleTime } from 'rxjs/operators';

@Directive({
    selector: '[ide-treename]',
})
export class IdeTreeNodeDirective implements OnInit, OnDestroy {
    @Input() data;

    @Output() singleClick = new EventEmitter();

    // 暂时不实现双击修改节点名称的功能
    // @Output() nodeDblClick = new EventEmitter();


    private click$ = null;
    constructor(private elRef: ElementRef, private ideTreeRef: IdeTreeComponent) { }

    ngOnInit(): void {
        this.click$ = this.onClick();
    }

    ngOnDestroy() {
        if (this.click$) {
            this.click$();
        }
    }

    /**
     * 点击事件，区分点击和双击--暂不实现双击功能
     */
    // private onClick() {
    //     const target = this.elRef.nativeElement;

    //     const throttleConfig = {
    //         leading: false,
    //         trailing: true
    //     };

    //     const btnClick$ = fromEvent(target, 'click');

    //     const throttled$ = btnClick$.pipe(throttleTime(250, async, throttleConfig));

    //     const btnDblClick$ = btnClick$.pipe(
    //         buffer(throttled$),
    //         map(arr => arr.length),
    //         filter(len => len >= 2),
    //     );

    //     const eventsMerged = merge(btnClick$, btnDblClick$).pipe(debounceTime(255));

    //     const subscription = eventsMerged.subscribe((e: any) => {
    //         if (typeof e === 'number') {
    //             this.nodeDblClick.emit(this.data);
    //         } else {
    //             this.singleClick.emit({ data: this.data, el: this.elRef.nativeElement });
    //         }
    //     });
    //     return () => {
    //         subscription.unsubscribe();
    //     };
    // }


    private onClick() {
        this.singleClick.emit({ data: this.data, el: this.elRef.nativeElement });

    }
}
