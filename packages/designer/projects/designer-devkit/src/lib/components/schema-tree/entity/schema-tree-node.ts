
import { TreeNode } from '@farris/ui-treetable';

export class SchemaTreeNode implements TreeNode {
    id?: string;

    data: any;


    /** 子节点集合 */
    children?: SchemaTreeNode[];


    /** 是否展开 */
    expanded?: boolean;


    /** 父节点 */
    parent?: SchemaTreeNode;

    /** 节点类型 */
    nodeType?: 'entity' | 'childEntity' | 'field';

    /** 实体绑定路径 */
    entityPath?: string;
}
