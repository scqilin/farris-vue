import { ContextMenuItem } from '@farris/ui-context-menu';
import { RowNode } from '@farris/ui-treetable';
import { NotifyService } from '@farris/ui-notify';
import { RefreshFormService, DgControl } from '@farris/designer-services';
import { ControlTreeNode } from '../entity/tree-node';

/**
 * 控件树上移下移菜单
 */
export class MoveUpDownContextMenuManger {

    /** 平铺的完整的控件树数据，用来替换被克隆的rawElement */
    plainCompleteControlTreeNodes: ControlTreeNode[];

    parentRowNode: ControlTreeNode;

    constructor(
        private notifyService: NotifyService,
        private refreshFormService: RefreshFormService) { }

    /**
     * 配置上移下移菜单。
     */
    setMoveMenuConfig(rowNode: RowNode): ContextMenuItem[] {

        this.getRealParentNode(rowNode);

        if (!this.parentRowNode) {
            return [];
        }

        // 1、支持卡片区域内的输入控件和分组控件
        if (this.parentRowNode.rawElement && this.parentRowNode.rawElement.type === DgControl.Form.type) {
            return this.confirmSetMoveMenuConfig(rowNode);
        }
        if (this.parentRowNode.rawElement && this.parentRowNode.rawElement.type === DgControl.FieldSet.type) {
            return this.confirmSetMoveMenuConfig(rowNode);
        }

        // 2、支持卡片组件
        if (rowNode.node.type === DgControl.Component.type) {
            const treeNode = rowNode.node as ControlTreeNode;
            const cmpType = treeNode.rawElement && treeNode.rawElement.componentType;
            if (cmpType && cmpType.includes('form')) {
                return this.confirmSetMoveMenuConfig(rowNode);
            }
        }
        return [];

    }
    /**
     * 获取右键节点真正的父节点。对于收折后的控件树，直接在rowNode上取parent是不准的。
     * @param rowNode  右键的节点
     */
    private getRealParentNode(rowNode: RowNode) {

        const rowData = rowNode.node as ControlTreeNode;

        // 收折后的控件树，获取父节点要通过rawParentNodeId
        if (rowData.rawParentNodeId) {
            this.parentRowNode = this.plainCompleteControlTreeNodes.find(n => n.data.id === rowData.rawParentNodeId);
        } else {
            // 完整的控件树
            this.parentRowNode = rowNode.parent as ControlTreeNode;
        }

    }

    private confirmSetMoveMenuConfig(rowNode: RowNode) {
        let moveUpDisable = () => false;
        let moveDownDisable = () => false;

        let index;
        if (this.parentRowNode) {
            index = this.parentRowNode.children.findIndex(c => c.data.id === rowNode.id);
        }

        if (index === 0) {
            moveUpDisable = () => true;
        }
        if (index === this.parentRowNode.children.length - 1) {
            moveDownDisable = () => true;
        }
        // 若是卡片组件节点，需要判断当前是不是最后一个卡片组件
        if (rowNode.node.type === DgControl.Component.type) {
            const cardCmps = this.parentRowNode.children.filter(c => c.rawElement && c.rawElement.type === DgControl.Component.type && c.rawElement.componentType && c.rawElement.componentType.startsWith('form'));
            const cardCmpIndex = cardCmps.findIndex(c => c.data.id === rowNode.id);

            if (cardCmpIndex === 0) {
                moveUpDisable = () => true;
            }
            if (cardCmpIndex === cardCmps.length - 1) {
                moveDownDisable = () => true;
            }

        }
        return [
            {
                id: 'moveUp',
                title: '上移',
                disable: moveUpDisable,
                handle: (e) => this.moveUpDownMenuHandle(e)
            },
            {
                id: 'moveDown',
                title: '下移',
                disable: moveDownDisable,
                handle: (e) => this.moveUpDownMenuHandle(e)
            }
        ];
    }


    /**
     * 控件节点上移下移点击事件
     */
    public moveUpDownMenuHandle(e: { data: RowNode, menu: ContextMenuItem }) {
        const menu = e.menu;
        if (!this.parentRowNode) {
            return;
        }

        const rowNode = e.data.node as ControlTreeNode;
        const rightMenuIndex = this.parentRowNode.children.findIndex(c => c.data.id === rowNode.id);
        const parentDomElement = this.parentRowNode.rawElement;


        switch (menu.id) {
            case 'moveUp': {
                if (rightMenuIndex === 0) {
                    this.notifyService.warning('已经是第一个节点');
                    return;
                }

                const rightMenuDomElement = parentDomElement.contents[rightMenuIndex];
                parentDomElement.contents.splice(rightMenuIndex, 1);
                parentDomElement.contents.splice(rightMenuIndex - 1, 0, rightMenuDomElement);


                this.refreshFormService.refreshFormDesigner.next(parentDomElement.id);


                break;
            }
            case 'moveDown': {
                const isLastNode = rightMenuIndex === this.parentRowNode.children.length - 1;

                if (isLastNode) {
                    this.notifyService.warning('已经是最后一个节点');
                    return;
                }

                const rightMenuDomElement = parentDomElement.contents[rightMenuIndex];
                parentDomElement.contents.splice(rightMenuIndex, 1);
                parentDomElement.contents.splice(rightMenuIndex + 1, 0, rightMenuDomElement);

                this.refreshFormService.refreshFormDesigner.next(parentDomElement.id);
                break;
            }
        }
    }
}
