
import { FormComponentType, DgControl, DomService, FormComponent } from '@farris/designer-services';

export class ControlTreeNodeBuilderService {

  constructor(private domService: DomService) { }
  /**
   * 获取树节点图标
   * @param element 表单DOM元素
   */
  getControlIconClass(element: any) {
    const controlType = element && element.type || '';

    // 多视图项节点使用各自的icon
    if (DgControl.MultiViewItem && controlType === DgControl.MultiViewItem.type && element.icon) {
      return element.icon;
    }

    // 其余控件使用farris control图标库里的icon
    const iconType = DgControl[controlType] && DgControl[controlType].icon;
    if (!iconType) {
      return;
    }

    return 'fd-i-Family fd_pc-' + iconType;

  }
  /**
   * 获取树节点名称
   * @param element 表单DOM元素
   */
  public getNodeName(element: any, parentElement?: any) {

    const name = element.text || element.name || element.title || element.caption || element.mainTitle;

    if (name && typeof (name) === 'string') {
      return name.trim();
    }

    switch (element.type) {
      case DgControl.Component.type:
        return this.getComponentShowName(element);

      case DgControl.ContentContainer.type: {
        return this.getContentContainerTitle(element);
      }
      case DgControl.SplitterPane && DgControl.SplitterPane.type: {
        return this.getSplitterPaneTitle(element, parentElement);
      }
      default: {
        // 显示控件类型名称
        return (DgControl[element.type] && DgControl[element.type].name) || element.id;
      }
    }


  }

  private getSplitterPaneTitle(splitterPaneElement: any, splitterElement: any) {
    const panes = splitterElement.contents;
    if (panes.length !== 2) {
      return DgControl[splitterPaneElement.type] && DgControl[splitterPaneElement.type].name;
    }
    const firstPane = panes[0];
    const firstPaneClass = (firstPane.appearance && firstPane.appearance.class) || '';
    // 是否水平布局
    const isHorizontal = firstPane.resizeHandlers === 'e' && firstPaneClass.includes('f-col-w');

    // 第一个区域
    if (splitterPaneElement.id === firstPane.id) {
      return isHorizontal ? '左侧区域' : '上方区域';

    } else {
      // 第二个区域
      return isHorizontal ? '右侧区域' : '下方区域';
    }
  }

  private getContentContainerTitle(element: any) {
    if (element.isLikeCardContainer) {
      return '区块';
    }
    const elementClass = element.appearance && element.appearance.class || '';
    const elementClassList = elementClass.split(' ');
    if (element.id === 'page-header' && elementClass && elementClass === 'f-page-header') {
      return '页头';
    }
    if (elementClassList.includes('f-page-header-base')) {
      return '页头容器';
    }
    if (elementClassList.includes('f-page-header-extend')) {
      return '页头扩展容器';
    }
    if (elementClassList.includes('f-title')) {
      return '标题容器';
    }
    if (elementClassList.includes('f-page')) {
      return '根容器';
    }
    if (elementClassList.includes('f-page-main')) {
      return '内容区域';
    }
    if (elementClassList.includes('f-scrollspy-content') && element.isScrollspyContainer) {
      return '滚动监听容器';
    }
    if (elementClassList.includes('f-grid-is-sub')) {
      return '表格容器';
    }

    if (elementClassList.includes('f-filter-container')) {
      return '筛选条容器';
    }
    return '容器';
  }

  /**
   * 获取组件展示名称
   */
  private getComponentShowName(element: any) {
    const componentType = element.componentType;

    switch (componentType) {
      case FormComponentType.Frame: {
        return '根组件';
      }
      case FormComponentType.dataGrid: case FormComponentType.table: {
        return this.checkDataGridComponentName(element);
      }
      case FormComponentType.attachmentPanel: {
        return '附件组件';
      }
      case FormComponentType.listView: case 'ListView': {
        return '列表视图组件';
      }
      case FormComponentType.modalFrame: {
        return '弹窗页面';
      }
      case FormComponentType.appointmentCalendar: {
        return '预约日历组件';
      }
      default: {
        if (componentType.startsWith('form')) {
          return '卡片组件';
        }
      }
    }

    return '组件';
  }

  private checkDataGridComponentName(element: any) {
    const treeGrid = this.domService.selectNode(element, (item) => item.type === (DgControl.TreeGrid && DgControl.TreeGrid.type));
    if (treeGrid) {
      return '树表格组件';
    }
    return '表格组件';
  }
}
