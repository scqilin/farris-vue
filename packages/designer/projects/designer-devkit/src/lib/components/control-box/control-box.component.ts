import { HttpClient } from '@angular/common/http';
import { Component, ComponentFactoryResolver, Injector, Input, NgZone, OnDestroy, OnInit } from '@angular/core';
import { FormBasicService } from '@farris/designer-services';
import { BsModalService } from '@farris/ui-modal';
import { ComponentTemplateManageComponent } from './component-template-manage/component-template-manage.component';
import { ControlCategoryService } from './control-category.service';

@Component({
  selector: 'control-box',
  templateUrl: './control-box.component.html',
  styleUrls: ['./control-box.component.css']
})
export class ControlBoxComponent implements OnInit, OnDestroy {

  /** 可视化区域构造器 */
  @Input() designerBuilder: any;

  /** 设计器支持的控件分类 */
  public controlCategoryList: any[];

  timer: any;

  constructor(
    private http: HttpClient,
    private ngZone: NgZone,
    private formBasicService: FormBasicService,
    private injector: Injector,
    private resolver: ComponentFactoryResolver,
    private modalService: BsModalService) {
  }

  ngOnInit() { }

  ngOnDestroy(): void {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  /**
   * 获取工具箱控件列表，需要等到可视化区域渲染完成后执行，否则DgControl为空
   */
  getDisplayedControls() {

    const controlCategoryService = new ControlCategoryService(this.http, this.formBasicService);
    controlCategoryService.getControlCategoryList().subscribe(data => {
      this.controlCategoryList = data;

      this.ngZone.runOutsideAngular(() => {
        this.timer = setTimeout(() => {
          this.addControlBoxToDragula();
        });
      });

    });
  }
  /**
   * 更新拖拽列表中的工具箱节点，用于页面重新渲染后执行
   */
  updateCategoryPanelsInDragula() {
    this.addControlBoxToDragula();

  }
  /**
   * 将工具箱各容器添加到dragula的拖拽列表中
   */
  private addControlBoxToDragula() {
    if (!this.designerBuilder) {
      return;
    }
    const controlPanels = document.getElementsByClassName('controlCategory');
    if (!controlPanels) {
      return;
    }

    if (this.designerBuilder.instance.dragula) {

      this.designerBuilder.instance.dragula.containers =
        this.designerBuilder.instance.dragula.containers.filter(e => !e.className.includes('controlCategory'));

      Array.from(controlPanels).forEach(panelElement => {
        this.designerBuilder.instance.dragula.containers.push(panelElement);
      });

    }
  }

  /**
   * 维护分类下的控件。目前支持组件模板类
   * @param categoryType 
   */
  onManageTemplateCategory(e) {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }

    const compFactory = this.resolver.resolveComponentFactory(ComponentTemplateManageComponent);
    const compRef = compFactory.create(this.injector);
    const modalConfig = compRef.instance.modalConfig as any;
    modalConfig.buttons = compRef.instance.modalFooter;


    const modalPanel = this.modalService.show(compRef, modalConfig);
    compRef.instance.closeModal.subscribe(() => {
      modalPanel.close();
    });
    compRef.instance.submitModal.subscribe(() => {
      this.getDisplayedControls();
      modalPanel.close();

    });
  }

  /**
   * 刷新模板
   * @param e 
   */
  onRefreshTemplateCategory(e) {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }

    this.getDisplayedControls();
  }
}
