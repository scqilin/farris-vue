## 更新日志

返回[主题文档](README.md)

### v0.0.8 Date:2023-1-5--2023-1-6

1. gulpfile: 修改 gulpfile 文件

2. 公共样式文件:

   a. 迁移 farris 公共样式文件

   b. 公共样式站点调整（暂不显示暗黑主题相关内容）
   
   c. 增加 farris-pub.sh 文件

   d. 增加 公共样式文件说明文档

3. 主题样式文件:

   a. 提供 farris 新增主题（不同于现有主题风格）的说明文档

   b. 提供 farris 新增主题（以现有主题为基础）的说明文档

4. 字体图标文件:

   a. 提供字体图标文件使用的说明文档

### v0.0.7 Date:2023-1-4

1. 主题样式文件:

   a. 调整样式文件变量

   b. 新增 farris override 相关样式

   c. 统一商务风拟物风下的部分样式

2. 图标样式文件: 迁移已废弃工程文件的部分图标样式

3. 验证 farris-all.css: 验证生成的样式文件

### v0.0.6 Date:2023-12-31 至 2023-1-3

1. gulpfile: 修改 gulpfile 文件

2. 主题样式文件:

   a. 迭代样式: 修改完成 iteration mixins/utilities/alert/button-group/buttons/card/close/dropdown/forms/functions/grid/images/input-group/list-group/mixins/modal/nav/navbar/pagination/popover/progress/tooltip/utilities/variable 新增样式

   b. 组件样式: 修改完成 farris 下的所有组件样式

   c. 复写样式: 修改完成 farris-iteration-b 下的所有组件样式

   d. 第三方样式: 修改完成第三方 calendar/animation 样式

### v0.0.5 Date:2022-12-29--2022-12-30

1. 主题样式文件:

   a. 迭代样式: 修改完成 iteration breakpoints/variable 样式

   b. 组件样式: 修改完成 combolist/dropdown/footer/input/loading/lookup/notify/farris mixin input/farris page-tmpl \_old_layout/pagination/primeng/scheme/static-text/switch/treetable/template/time-picker/utils/common variables 新增样式

### v0.0.4 Date:2022-12-26--2022-12-28

1. 主题样式文件:

   a. 迭代样式: 修改完成 iteration mixin border-radius/variable 新增样式

   b. 组件样式: 修改完成 theme common/variable/mixin/farris accordion/badge/common function/switch/page-tmpl/tree-table/section/message/table/perfect-scrollbar/datagrid/tooltip/selected-wrapper/scroll/template/input/listnav/accordion/farris variable badge/listviewbar/switch/common function 样式

### v0.0.3 Date:2022-12-14--2022-12-23

1. 主题样式文件:

   a. 组件样式: 修改完成 theme common、variable 及 mixin 相关文件

   b. 组件样式: 修改完成 farris pagination/primeng/scheme/onepage/process/progress/template/time-picker/utils/progress-step/view-change/badge/static text/tabs/sidebar/scrollbar/scrollspy 样式

### v0.0.2 Date:2022-12-12--2022-12-13

1. gulpfile: 修改 gulpfile 文件

2. 主题样式文件:

   a. 组件样式: 修改完成 farris panel/collapse-scroll/combolist 部分样式

   b. 组件样式: 修改完成 farris datepicker/discussion/filter/tags/listview/modal/multi-select/nav/onepage/dropdown/footer/notify/input/loading/pagination/primeng 部分样式

3. 图标样式文件: 更新自定义图标部分样式

### v0.0.1 Date:2022-12-08

1. gulpfile: 修改 gulpfile 文件

2. 图标样式文件: farris 字体文件生成

3. 主题样式文件:

   a. 组件样式: 修改完成 farris tags/notify/btn/btn group 组件部分样式

   b. 新增 themes.sh 文件
   
4. 迁移 farris-theme 主要框架
