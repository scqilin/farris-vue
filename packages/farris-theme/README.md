<p align="center">
  <a href="#" target="_blank" rel="noopener noreferrer">
    <img alt="Farris UI Logo" src="../../farris_design.jpg"  style="max-width:50%;">
  </a>
</p>

<h1 align="center">Farris Theme</h1>

<p align="center">Farris Theme 是基于 Farris Design 的前端主题库。</p>

# Farris 主题

    Farris提供了多种内置主题，其中包含两种风格；商务风（默认风格）及拟物风；三种颜色:蓝色（默认颜色）、绿色、红色；两种尺寸:紧凑版（默认尺寸）、宽松版

## 1. 主题文件

### 1.1 主题文件生成

#### 1.1.1 初次生成

1. 全局安装 gulp 包 :
    ```
    npm install gulp -g
    ```
2. 安装本工程的包:

    ```
    npm install
    ```

3. 生成默认主题:
    ```
    gulp theme
    ```

#### 1.1.2 后续生成

1. 生成所有主题

    在终端运行如下命令可以生成 farris 下的所有主题:

    ```
    ./themes.sh
    ```

2. 生成特定主题

    ```
    gulp theme --code 主题编码 --type 主题尺寸
    ```

    例:（具体示例可参考 themes.sh）

    ```
    gulp theme --code red --type loose 生成红色主题宽松型
    ```

3. 文件生成后的路径在:

    ```
    packages/farris-theme/src/assets/themes/主题风格-颜色
    ```

### 1.2 运行主题工程

在终端输入`npm run start`命令运行主题工程,端口号为 `4700`。

在浏览器中输入地址`http://localhost:4700/`可查看当前主题工程。

### 1.3 新增主题

#### 1.3.1 新增主题以现有主题为基础

Farris 主题工具支持用户新增类似风格的主题，以下示例为如何以商务风绿色主题为基础，快速新增加一个主题:

1. 修改 `packages/farris-theme/themes/setting.json`文件，在最后面追加新的主题节点

    以下示例假设新增主题名称为`test-green`:

    ```
    {
       "theme": "test-green",
       "types": ["default", "loose"],
       "dist": "src/assets/themes/test-green"
    }
    ```

2. 拷贝 `green` 文件夹，重名为新主题的名字 `test-green`

    a. 修改 `test-green/base` 文件，根据具体需求调整颜色变量和尺寸变量

    b. 修改 `test-green/default` 文件中的 `extend.scss` ，调整自有样式同时在`index.scss`文件中调整变量

    c. 修改 `test-green/loose` 文件中的 `extend.scss` ，调整自有样式同时在`index.scss` 文件中调整变量

3. 执行如下命令生成该主题的紧凑版:
    ```
    gulp theme --code test-green --type default
    ```
    生成文件路径为:
    ```
    packages/farris-theme/src/assets/themes/test-green/default
    ```

#### 1.3.2 新增主题不同于现有风格

Farris 主题工具支持用户新增不同风格的主题，以下示例为如何增加一个完全不同于现有风格的主题:

1. 修改 `packages/farris-theme/themes/setting.json`文件，在最后面追加新的主题节点

    以下示例假设新增主题名称为`test-default`:

    ```
    {
       "theme": "test-default",
       "types": ["default", "loose"],
       "dist": "src/assets/themes/test-default"
    }
    ```

2. 拷贝 `mimicry` 文件夹，重命名为 `test-default`

    a. 根据需求决定是否修改 `assets/imgs` 文件夹，调整图片资源

    b. 根据需求决定是否修改 `base/color`文件夹，调整颜色变量保留结构`_color.scss`。 作为当前文件夹的出口文件，可增加、删除、修改文件

    c. 根据需求决定是否修改 `base/extend` 文件夹，调整自有样式保留结构`_extend.scss`。 作为当前文件夹的出口文件，可增加、删除、修改文件

    d. 根据需求决定是否修改 `base/size` 文件夹，调整尺寸变量保留结构`size.scss` 和`loose.scss`。 作为紧凑尺寸和宽松尺寸的出口文件，可增加、删除、修改文件

3. 执行如下命令生成该主题的紧凑版:

    ```
    gulp theme --code test-default --type default
    ```

    生成文件路径为:

    ```
    packages/farris-theme/src/assets/themes/test-default/default
    ```

## 2. 公共样式文件

### 2.1 公共样式界面示例

当前可生成的公共样式包括:商务风-蓝色、绿色、红色主题，拟物风-蓝色、绿色、红色主题；暗黑主题将在后续迭代更新

<p align="center">
    <img src="./img/public.png"  style="max-width:60%;">
</p>

### 2.2 公共样式生成

#### 2.2.1 初次生成

1. 生成默认公共样式

    在终端输入如下命令来生成默认公共样式（即商务风下的蓝色主题公共样式）:

    ```
    gulp pubsite
    ```

#### 2.2.2 后续生成

1. 生成所有公共样式

    在终端输入如下命令来生成所有主题样式:

    ```
    ./farris-pub.sh
    ```

2. 生成特定公共样式
    ```
    gulp  pubcss --code 主题风格
    ```
    例:（具体示例可参考 farris-pub.sh）
    ```
    gulp pubcss --code mimicry-red 生成拟物风红色主题公共样式
    ```
3. 文件生成后的路径在:

    ```
    packages/farris-theme/dist/farris-pub
    ```

    双击 `index.html` 文件，可在浏览器中查看所有生成的公共样式若某个风格下的样式还没有生成，则该风格下的色值展示将全部置灰。

4. 重新运行如下命令，刷新页面，即可查看

    ```
    ./farris-pub.sh
    ```

## 3. 字体图标文件

#### 3.1 字体文件目录

字体图标相关定义都在文件夹 `packages/farris-theme/farris-icon` 下，其目录如下:

```
farris-icon/
│
├── commpatible/ 处理图标兼容
│
├── dist/ 生成的图标NPM包文件，用于发布
│
├── entrance/ 入口文件
|   |
│   ├──farrisicon.html NPM包中用于说明、展示和查询图标的页面
|   └──farrisicon.scss 图标样式文件的入口，生成命令会调用此文件
│
├──scss
|   |
│   ├── extend/
|   |   |
│   │   ├── _icons.scss 字体图标的样式
│   │   ├── _varriables.scss 相关变量
│   │   └── farrisicon-extend.ttf 字体图标文件
|   |
│   ├── farris/ farris字体图标
|   |   |
│   │   ├── _basic.scss 字体family的定义
│   │   ├── _new.scss 新追加的Farris字体图标样式
│   │   ├── _rewrite.scss Farris字体图标库
│   │   ├── _variables.scss 相关变量
│   │   └── farrisicon.ttf farris字体图标文件
│   │
│   ├──package.json NPM包中的版本号
|   └──readme.md NPM包中的说明文件
```

#### 3.2 使用已有图标

1. 字体图标文件介绍：

    Farris 主题内置了字体图标文件,其中包括三个主要文件: `farrisicon.css`,`farrisicon-extend.ttf`,`farrisicon.ttf`。

2. 如何使用字体图标文件

    双击`packages/farris-theme/farris-icon/dist`文件下的`farrisicon.html`可以查看目前已有的 farris 图标并进行使用。

    具体使用步骤可以在`farrisicon.html`文件中查看。

#### 3.3 新增图标

若用户想要`新增图标`，则可以按照如下步骤生成:

1. 提前确定好新增的`样式名`和`图标编号`

2. 由设计组同事提供最新的图标文件，覆盖现有的 `farrisicon.ttf`。文件具体位置在:`packages/farris-theme/farris-icon/scss/farris/farrisicon.ttf`

3. 在 `packages/farris-theme/farris-icon/scss/farris/_new.scss` 中参照之前的写法新增样式定义。此次需要重新命名图标，并增加 `font-family`。

4. 在 `packages/farris-theme/farris-icon/entrance/farrisicon.html` 的底部`icon`数组中，增加样式名称；

5. 处理图标兼容

    处理 `farrisicon.ttf`文件在 IE 浏览器上的兼容问题，需下载`ttfpatch.exe`，经转化文件后的`xx.ttf`文件可支持 IE 浏览器。

    具体操作方式如下:

    a. 在`ttfpatch.exe`上右击`属性`-> `兼容性` ->勾选`以兼容模式运行这个程序`，点击`应用`

    b. `cmd` 打开命令窗口，定位到 `ttfpatch.exe` 所在的目录

    c. 执行如下命令

    ```
    ttfpatch.exe ../scss/farris/farrisicon.ttf
    ```

    d. `farris/farrisicon.ttf`的修改日期变化，说明生成成功

6. 执行命令

    ```
    gulp farrisicon
    ```

    生成目录:`farris-icon/dist`

    在 `dist` 文件的 `html` 中验证是否无误

7. 因为图标的变动影响所有主题，需要执行如下命令重新生成所有主题:

    ```
    themes.sh
    ```

## 4. 更新日志

见[log](log.md)文件
