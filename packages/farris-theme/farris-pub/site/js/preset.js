/**
 * 颜色与样式之间的关系
 */
const colorRelation = {
    "text": {
        "default": {
            "01": "$f-theme-03",
            "02": "$f-text-05",
            "03": "$f-text-01",
            "10": "$f-text-08",
            "11": "$f-text-09",
            "12": "$f-text-13"
        },
        "hover": {
            "01": "$f-theme-05"
        },
        "active": {
            "01": "$f-theme-01"
        },
        "selected": {
            "01": "$f-theme-03"
        },
        "action": {
            "01": {
                "cursor": ["$f-theme-03", "$f-theme-05", "$f-theme-01"]
            }
        }
    },

    "bg": {
        "default": {
            "01": "$f-theme-04",
            "02": "$f-theme-09",
            "03": "$f-theme-11",
            "04": "$f-theme-13",
            "05": "$f-aid-06",
            "06": "$f-aid-07",
            "07": "$f-neutral-01",
            "08": "$f-neutral-04",
            "09": "$f-neutral-05",
            "10": "$f-neutral-06",
            "11": "$f-neutral-07",
            "12": "$f-neutral-09",
            "13": "$f-neutral-10",
            "14": "$f-neutral-11",
            "15": "$f-neutral-12",
            "16": "$f-neutral-18",
            "17": "$f-neutral-19",
            "18": "$f-neutral-20",
            "19": "$f-neutral-21",
            "20": "$f-ornament-01",
            "21": "$f-ornament-02",
            "22": "$f-ornament-03",
            "23": "$f-ornament-04",
            "24": "$f-ornament-05"
        },
        "hover": {
            "01": "$f-theme-03",
            "02": "$f-theme-06",
            "03": "$f-aid-03",
            "04": "$f-neutral-03",
            "05": "$f-neutral-11"
        },
        "active": {
            "01": "$f-theme-02",
            "02": "$f-theme-04",
            "03": "$f-aid-07",
            "04": "$f-neutral-03",
            "05": "$f-aid-02",
            "06": "$f-aid-04",
            "07": "$f-neutral-10"
        },
        "disabled": {
            "01": "$f-neutral-08",
            "02": "$f-neutral-10",
            "03": "$f-neutral-14"
        },
        "selected": {
            "01": "$f-theme-04",
            "02": "$f-theme-07",
            "03": "$f-aid-02",
            "04": "$f-aid-04",
            "05": "$f-neutral-10"
        },
        "action": {
            "01": {
                "cursor": ["$f-theme-04", "$f-theme-06", "$f-theme-02"]
            },
            "02": {
                "cursor": ["$f-neutral-04", "$f-neutral-03", "$f-neutral-03"]
            }
        }
    },
    "border": {
        "default": {
            "01": "$f-theme-03",
            "02": "$f-neutral-05",
            "03": "$f-neutral-07",
            "04": "$f-neutral-08"
        },
        "hover": {
            "01": "$f-theme-05",
            "02": "$f-theme-08",
            "03": "$f-neutral-07"
        },
        "active": {
            "01": "$f-theme-01",
            "02": "$f-aid-04"
        },
        "disabled": {
            "01": "$f-neutral-04"
        },
        "action": {
            "01": {
                "cursor": ["$f-theme-03", "$f-theme-05", "$f-theme-01"]
            },
            "02": {
                "changeDisabled": ["$f-neutral-05", "$f-neutral-04"]
            }
        }
    },

    "icon": {
        "default": {
            "01": "$f-theme-03",
            "02": "$f-text-11",
            "03": "$f-text-04",
            "04": "$f-text-10"
        },
        "hover": {
            "01": "$f-theme-05",
            "02": "$f-text-08"
        },
        "active": {
            "01": "$f-theme-01",
            "02": "$f-text-08"
        },
        "disabled": {
            "01": "$f-text-09"
        },
        "action": {
            "01": {
                "cursor": ["$f-theme-03", "$f-theme-05", "$f-theme-01"]
            }
        }
    },

    "info": {
        "border": {
            "01": "$f-semantic-info-02"
        },
        "bg": {
            "01": "$f-semantic-info-05",
            "02": "$f-semantic-info-03",
            "03": "$f-semantic-info-04"
        },
        "text": {
            "01": "$f-semantic-info-01"
        },
        "icon": {
            "01": "$f-semantic-info-01"
        }
    },

    "submit": {
        "bg": {
            "01": "$f-semantic-submit-03",
            "02": "$f-semantic-submit-04"
        },
        "text": {
            "01": "$f-semantic-submit-01"
        },
        "border": {
            "01": "$f-semantic-submit-02"
        },
        "icon": {
            "01": "$f-semantic-info-01"
        }
    },

    "success": {
        "bg": {
            "01": "$f-semantic-success-05",
            "02": "$f-semantic-success-03"
        },
        "text": {
            "01": "$f-semantic-success-01"
        },
        "border": {
            "01": "$f-semantic-success-02"
        },
        "icon": {
            "01": "$f-semantic-info-01"
        }
    },

    "warning": {
        "bg": {
            "01": "$f-semantic-warning-05",
            "02": "$f-semantic-warning-03",
            "03": "$f-semantic-warning-04"
        },
        "text": {
            "01": "$f-semantic-warning-01"
        },
        "border": {
            "01": "$f-semantic-warning-02"
        },
        "icon": {
            "01": "$f-semantic-info-01",
            "02": "$f-semantic-warning-06"
        }
    },

    "danger": {
        "bg": {
            "01": "$f-semantic-danger-05",
            "02": "$f-semantic-danger-03",
            "03": "$f-semantic-danger-04"
        },
        "text": {
            "01": "$f-semantic-danger-01"
        },
        "border": {
            "01": "$f-semantic-danger-02"
        },
        "icon": {
            "01": "$f-semantic-info-01"
        }
    }

};

// 根据此判断css中:root预先定义的变量值
const cssColorsVariables = {
    "theme": {
        len: 11,
        cPrefix: 'fp-',
        fPrefix: '$f-'
    },
    "aid": {
        len: 10,
        cPrefix: 'fp-',
        fPrefix: '$f-'
    },
    "ornament": {
        len: 5,
        cPrefix: 'fp-',
        fPrefix: '$f-'
    },
    "neutral": {
        len: 21,
        cPrefix: 'fp-',
        fPrefix: '$f-'
    },
    "text": {
        len: 13,
        cPrefix: 'fp-',
        fPrefix: '$f-'
    },
    "info": {
        len: 5,
        cPrefix: 'fp-',
        fPrefix: '$f-semantic-'
    },
    "submit": {
        len: 5,
        cPrefix: 'fp-',
        fPrefix: '$f-semantic-'
    },
    "success": {
        len: 5,
        cPrefix: 'fp-',
        fPrefix: '$f-semantic-'
    },
    "warning": {
        len: 5,
        cPrefix: 'fp-',
        fPrefix: '$f-semantic-'
    },
    "danger": {
        len: 5,
        cPrefix: 'fp-',
        fPrefix: '$f-semantic-'
    }
};
let themesColor = {};

// 模式配置
const modeSettings = [{
    "name": "亮色模式",
    "value": "light"
}];
// 默认模式
const currentMode = modeSettings[0].value;

// 主题配置
const themeSettings = {
    "light": [{
        "name": "商务风",
        "value": [{
            "name": "沉稳蓝",
            "value": "default"
        },
        {
            "name": "清新绿",
            "value": "green"
        },
        {
            "name": "中国红",
            "value": "red"
        },
        ]
    },
    {
        "name": "拟物风",
        "value": [{
            "name": "沉稳蓝",
            "value": "mimicry-default"
        },
        {
            "name": "清新绿",
            "value": "mimicry-green"
        },
        {
            "name": "中国红",
            "value": "mimicry-red"
        },
        ]
    }
    ]
    // ,
    // "dark": [{
    //     "name": "拟物风",
    //     "value": [{
    //         "name": "暗黑蓝",
    //         "value": "mimicry-default-dark"
    //     },]
    // }]
};
// 默认主题
const currentTheme = "default";

const searchTypeSettings = [{
    "name": "颜色",
    "value": "color"
}, {
    "name": "样式名",
    "value": "clsname"
}];
const currentSearchType = "color";

// 预定义交互的状态
const actionState = {
    "cursor": ['默认', '滑过', '点击'],
    "changeDisabled":['默认','禁用'],
    "default": ['默认'],
    "hover": ['滑过'],
    "active": ['点击'],
    "disabled": ['禁用'],
    "selected": ['选中'],
    "action": ['交互']
};

/**
 *  从颜色变量中预定义中获取值
 */
function getThemesColorFromVariables() {
    themesColor = {};
    for (const typeP in cssColorsVariables) {
        const typeDetail = cssColorsVariables[typeP];
        for (let k = 1; k <= typeDetail.len; k++) {
            const numCode = '-' + (k < 10 ? '0' + k : k);
            themesColor[typeDetail.fPrefix + typeP + numCode] = getComputedStyle(document.documentElement).getPropertyValue("--" + typeDetail.cPrefix + typeP + numCode);
        }
    }
}
/**
 * 根据变量名称获取css变量名
 * @param {*} fVariableName
 */
function getCSSVariableFromFVariable(fVariableName){
    const findItem=false;
    let result='';
    for (const typeP in cssColorsVariables) {
        if(findItem){return;}
        const typeDetail = cssColorsVariables[typeP];
        if(fVariableName.indexOf('-'+typeP+'-')>-1){
            result=fVariableName.replace(typeDetail.fPrefix,typeDetail.cPrefix);
        }
    }
    return '--'+result;
}
/**
 * 加载CSS
 * @param {*} src
 * @param {*} loadedCallback
 */
function loadCSS(src, loadedCallback) {
    const themelinkEl = document.head.querySelector("#themelink");
    themelinkEl.rel = 'stylesheet';
    themelinkEl.href = src;

    // document.head.insertBefore(themelinkEl, document.head.firstChild);

    if (themelinkEl.attachEvent) {
        // 针对IE
        themelinkEl.attachEvent('onload', () => {
            loadedCallback(null, themelinkEl);
        });
    } else {
        // 其他浏览器
        setTimeout(() => {
            poll(themelinkEl, loadedCallback);
        }, 0);
    }

    function poll(themelinkEl, callback) {
        let isLoaded = false;
        if (/webkit/i.test(navigator.userAgent)) {
        // webkit内核
            if (themelinkEl.sheet) {
                isLoaded = true;
            }
        } else if (themelinkEl.sheet) {
        // 火狐
            try {
                if (themelinkEl.sheet.cssRules) {
                    isLoaded = true;
                }
            } catch (ex) {
                if (ex.code === 1000) {
                    isLoaded = true;
                }
            }
        }
        if (isLoaded) {
            setTimeout(() => {
                callback(null, themelinkEl);
            }, 1);
        } else {
            setTimeout(() => {
                poll(themelinkEl, callback);
            }, 10);
        }
    }

    themelinkEl.onLoad = function () {
        loadedCallback(null, themelinkEl);
    };
}
