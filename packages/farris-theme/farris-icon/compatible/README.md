# 兼容IE浏览器
因为目前生成的ttf在IE 浏览器下会提示未安装。需要通过工具将字体文件转换之后才能兼容IE浏览器。
## 使用方式
1. ttfpatch.exe 右键属性-》兼容性 -》勾选 以兼容模式运行这个程序，点击[应用] 
2. cmd打开命令窗口，定位到ttfpatch.exe所在的目录
3. 执行命令 ttfpatch.exe ../scss/farris/farrisicon.ttf 0
4. 然后发现 farris/farrisicon.ttf的修改日期变了，说明生成成功