module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    extends: ['plugin:vue/vue3-recommended', 'airbnb-base', 'plugin:@typescript-eslint/recommended', 'prettier'],
    parserOptions: {
        parser: '@typescript-eslint/parser',
        ecmaVersion: 2019,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
            tsx: true
        },
        extraFileExtensions: ['.vue']
    },
    plugins: ['@typescript-eslint'],
    env: {
        es6: true,
        node: true,
        jest: true,
        browser: true
    },
    rules: {
        'accessor-pairs': 'off',
        'array-callback-return': 'off',
        'arrow-body-style': 'off',
        curly: 'error',
        'class-methods-use-this': 'off',
        complexity: [
            'error',
            {
                max: 40
            }
        ],
        'consistent-return': 'off',
        'default-case': 'off',
        'eol-last': 'error',
        eqeqeq: ['error', 'smart'],
        'func-names': 'off',
        'import/order': 'off',
        'import/extensions': 'off',
        'import/no-unresolved': 'off',
        'import/prefer-default-export': 'off',
        'import/no-extraneous-dependencies': 'off',
        indent: 0,
        'max-depth': 'off',
        'max-len': ['error', { code: 140 }],
        'max-nested-callbacks': ['error', 6],
        'max-params': 'off',
        'no-new': 'off',
        'no-bitwise': 'off',
        'no-console': [
            'error',
            {
                allow: [
                    'log',
                    'warn',
                    'dir',
                    'timeLog',
                    'assert',
                    'clear',
                    'count',
                    'countReset',
                    'group',
                    'groupEnd',
                    'table',
                    'dirxml',
                    'error',
                    'groupCollapsed',
                    'Console',
                    'profile',
                    'profileEnd',
                    'timeStamp',
                    'context'
                ]
            }
        ],
        'no-multiple-empty-lines': 'error',
        'no-restricted-globals': 'off',
        'no-shadow': 'off',
        'no-trailing-spaces': 'error',
        'no-param-reassign': 'off',
        'no-plusplus': 'off',
        'no-nested-ternary': 'off',
        'no-underscore-dangle': 'off',
        'no-unused-expressions': 'off',
        'no-unused-labels': 'error',
        'no-use-before-define': 'error',
        'no-useless-constructor': 'off',
        'no-useless-concat': 'off',
        'no-var': 'error',
        'prefer-const': 'error',
        'prefer-destructuring': ['error', { object: true, array: false }],
        'prefer-promise-reject-errors': 'off',
        'prefer-template': 'off',
        semi: 'error',
        'space-in-parens': ['error', 'never'],
        'spaced-comment': ['error', 'always'],
        '@typescript-eslint/camelcase': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/no-unused-vars': 'off',
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/no-useless-constructor': 'off',
        '@typescript-eslint/no-parameter-properties': 'off',
        '@typescript-eslint/no-require-imports': 'off',
        '@typescript-eslint/dot-notation': 'off',
        '@typescript-eslint/indent': [
            'error',
            4,
            { FunctionDeclaration: { parameters: 'first' }, FunctionExpression: { parameters: 'first' } }
        ],
        '@typescript-eslint/member-delimiter-style': [
            'error',
            {
                multiline: {
                    delimiter: 'semi',
                    requireLast: true
                },
                singleline: {
                    delimiter: 'semi',
                    requireLast: false
                }
            }
        ],
        '@typescript-eslint/no-misused-new': 'error',
        '@typescript-eslint/prefer-function-type': 'error',
        '@typescript-eslint/semi': ['error', 'always'],
        '@typescript-eslint/type-annotation-spacing': 'error',
        '@typescript-eslint/unified-signatures': 'error',
        '@typescript-eslint/no-shadow': 'off',
        '@typescript-eslint/member-ordering': 'off',
        '@typescript-eslint/no-this-alias': 'off',
        'vue/no-v-html': 'off',
        'vue/attributes-order': 'off',
        'vue/require-v-for-key': 'off',
        'vue/require-default-prop': 'off',
        'vue/no-unused-components': 'off',
        'vue/multi-word-component-names': 'off',
        'vue/return-in-computed-property': 'off'
    },
    overrides: [
        {
            files: ['*.vue'],
            parser: require.resolve('vue-eslint-parser')
        },
        {
            files: ['**/*.md/*.js', '**/*.md/*.ts'],
            rules: {
                '@typescript-eslint/no-unused-vars': 'off'
            }
        }
    ]
};
