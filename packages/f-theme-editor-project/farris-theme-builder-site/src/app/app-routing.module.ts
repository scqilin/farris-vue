import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { PreviewLayoutComponent } from './layout/preview-layout/preview-layout.component';
import { ThemeNavComponent } from './layout-nav/theme-nav/theme-nav.component';
import { IndexTmplComponent } from './preview-page/index-tmpl/index-tmpl.component';
import { IndexWidgetComponent } from './preview-widget/index-widget/index-widget.component';

const routes: Routes = [
    {
        path: '',
        component: AppLayoutComponent,
        children: [{ path: 'preview/:theme/:colorScheme', component: ThemeNavComponent }]
    },
    {
        path: '',
        component: PreviewLayoutComponent,
        children: [
            {
                path: 'preview-widget/:theme/:colorScheme',
                component: IndexWidgetComponent
            },
            {
                path: 'preview-page/:theme/:colorScheme',
                component: IndexTmplComponent
            }
        ]
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
