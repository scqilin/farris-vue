import { Component, OnInit } from '@angular/core';
import { CommonWidgetComponent } from '../common-widget/common-widget.component';

@Component({
    selector: 'app-button-widget',
    templateUrl: './button-widget.component.html',
    styleUrls: ['./button-widget.component.css']
})
export class ButtonWidgetComponent extends CommonWidgetComponent implements OnInit {
    widgetTitle = '按钮';

    data1 = [
        {
            id: 'b1',
            text: '增加',
            type: 'link'
        },
        {
            id: 'b2',
            text: '删除',
            type: 'link'
        },
        {
            id: 'b3',
            text: '保存',
            type: 'link'
        },
        {
            id: 'b4',
            text: '编辑',
            type: 'link'
        }
    ];

    constructor() {
        super();
    }

    ngOnInit() {}
}
