import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleFilterWidgetComponent } from './simple-filter-widget.component';

describe('SimpleFilterWidgetComponent', () => {
    let component: SimpleFilterWidgetComponent;
    let fixture: ComponentFixture<SimpleFilterWidgetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SimpleFilterWidgetComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SimpleFilterWidgetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
