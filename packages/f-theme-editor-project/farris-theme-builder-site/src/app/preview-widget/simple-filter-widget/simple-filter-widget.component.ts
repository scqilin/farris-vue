import { Component, OnInit } from '@angular/core';
import { CommonWidgetComponent } from '../common-widget/common-widget.component';

@Component({
    selector: 'app-simple-filter-widget',
    templateUrl: './simple-filter-widget.component.html',
    styleUrls: ['./simple-filter-widget.component.css']
})
export class SimpleFilterWidgetComponent extends CommonWidgetComponent implements OnInit {
    widgetTitle = '简单筛选';

    // 前置任务列表
    preTaskList = [
        {
            id: 'text',
            labelCode: 'text',
            code: 'text',
            name: '用户编号',
            control: {
                controltype: 'text',
                placeholder: '请输入用户编号'
                // "required": true
            }
        },
        {
            id: 'text1',
            labelCode: 'text1',
            code: 'text1',
            name: '用户姓名',
            control: {
                controltype: 'text',
                placeholder: '请输入用户姓名'
                // "required": true
            }
        }
    ];

    // 任务
    listFilterData = [
        {
            id: 'text1',
            labelCode: 'text1',
            code: 'text1',
            name: '文本',
            control: {
                controltype: 'text'
            },
            placeHolder: '请输入'
        },
        {
            id: '8df4bbdf-b1e2-4a26-befe-feb837f13aa9',
            labelCode: 'boolcheckbox',
            code: 'boolcheckbox',
            name: '是否支持',
            control: {
                controltype: 'bool-check',
                isExtend: false
                //"required": true,
            },
            value: {
                value: [false]
            }
        },
        {
            id: 'flexibleDate',
            labelCode: 'flexibleDate',
            code: 'flexibleDate',
            name: '可变日期',
            control: {
                controltype: 'flexibleDate',
                single: false,
                //"showType": 3,
                format: 'yyyy年MM月dd日'
                //   "isExtend": true,
                //   "required": true
                //"showTime": true
            },
            value: {
                value: '2021-08-19',
                startValue: '2020-08-13',
                endValue: '2020-08-17'
            },
            placeHolder: '请输入',
            beginPlaceHolder: '起始日期1',
            endPlaceHolder: '结束日期1'
        },
        {
            id: 'flexibleNumber',
            labelCode: 'flexibleNumber',
            code: 'flexibleNumber',
            name: '可变数值',
            control: {
                controltype: 'flexibleNumber',
                bigNumber: false,
                single: false,
                isExtend: true
                //   "required": true
            },
            // "value":{
            //     "value": 20,
            //     "startValue": 11,
            //     "endValue": 123
            // },
            placeHolder: '请输入',
            beginPlaceHolder: '起始数值',
            endPlaceHolder: '结束数值'
        },
        {
            id: '1111',
            labelCode: 'radio',
            name: '种类',
            control: {
                controltype: 'radio',
                enumValues: [
                    {
                        value: 'aa',
                        name: '足球'
                    },
                    {
                        value: 'bb',
                        name: '篮球'
                    },
                    {
                        value: 'cc',
                        name: '乒乓球'
                    },
                    {
                        value: 'dd',
                        name: '其他'
                    }
                ],
                isExtend: true
            }
            // "value": {
            //     "value": "bb"
            // },
        },
        {
            id: '1',
            labelCode: 'search',
            code: 'search',
            name: '搜索条',
            control: {
                controltype: 'search',
                isExtend: true
            },
            //"value": {"value":"搜索结果"},
            placeHolder: '111'
        },
        {
            id: 'dropdown001',
            labelCode: 'dropdown',
            code: 'dropdown1',
            name: '下拉选择',
            control: {
                controltype: 'dropdown',
                // "placeholder": "请选择",
                enumValues: [
                    {
                        value: 'yx',
                        name: '有效'
                    },
                    {
                        value: 'wx',
                        name: '无效'
                    }
                ],
                isExtend: true
                // "required": true
            },
            placeHolder: '请选择'
            // "value": "yx"
        },
        {
            id: 'singledate',
            labelCode: 'singledate',
            code: 'singledate',
            name: '日期',
            control: {
                controltype: 'singleDate',
                //"placeholder": "请选择日期",
                format: 'yyyy年MM月dd日',
                isExtend: true
                // "required": true
            },
            placeHolder: '444'
            // "value":"2020-07-02"
        },
        {
            id: 'date',
            labelCode: 'date',
            code: 'date',
            name: '日期范围区间',
            control: {
                controltype: 'date',
                //"placeholder": "请选择日期范围",
                isExtend: true,
                format: 'yyyy年MM月dd日'
                // "required": true
                // "required": true
            },
            beginPlaceHolder: '起始日期',
            endPlaceHolder: '结束日期',
            // "placeHolder":'555'
            value: {
                startTime: '2020-08-13',
                endTime: '2020-08-17'
            }
        },
        {
            id: 'datetime',
            labelCode: 'datetime',
            code: 'datetime',
            name: '日期时间范围',
            control: {
                controltype: 'datetime'
                //"placeholder": "请选择日期范围和时间",
                // "format": "yyyy年MM月dd日 HH:mm:ss",
                // "required": true
                // "required": true
            },
            beginPlaceHolder: '起始日期时间',
            endPlaceHolder: '结束日期时间',
            // "placeHolder":'666'
            value: {
                startTime: '2020-07-01 15:00:00',
                endTime: '2020-07-02 15:00:00'
            }
        },
        {
            id: 'number',
            labelCode: 'number',
            code: 'number2',
            name: '数字',
            control: {
                controltype: 'number',
                //"placeholder": "请输入",
                precision: 0,
                isExtend: true
                // "required": true
            },
            beginPlaceHolder: '开始数值',
            endPlaceHolder: '结束数值'
            // "placeHolder":'777'
            // "value": {
            //     "startValue": 20,
            //     "endValue": 30
            // }
        },
        {
            id: 'checkboxGroup',
            labelCode: 'checkboxGroup',
            code: 'checkboxGroup',
            name: '复选组',
            control: {
                controltype: 'checkboxgroup',
                enumValues: [
                    { value: 'aa', name: '标签一' },
                    { value: 'bb', name: '标签二' },
                    { value: 'cc', name: '标签三' },
                    { value: 'dd', name: '标签四' }
                ],
                isExtend: true
            }
            // "placeHolder":'888'
            // "value": ['aa','cc']
        }
    ];

    constructor() {
        super();
    }

    ngOnInit() {}
}
