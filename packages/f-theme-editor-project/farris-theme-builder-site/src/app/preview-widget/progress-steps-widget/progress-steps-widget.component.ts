import { Component, OnInit } from '@angular/core';
import { CommonWidgetComponent } from '../common-widget/common-widget.component';

@Component({
    selector: 'app-progress-steps-widget',
    templateUrl: './progress-steps-widget.component.html',
    styleUrls: ['./progress-steps-widget.component.css']
})
export class ProgressStepsWidgetComponent extends CommonWidgetComponent implements OnInit {
    widgetTitle = '步骤条';

    progressData = {
        activeIndex: 3,
        stepMessages: [
            {
                id: '1',
                title: '审批中',
                description: '正在审批',
                state: 'success'
            },
            {
                id: '2',
                title: '待复核',
                description: '等待复核',
                hidden: true
            },
            {
                id: '3',
                title: '允许发送银行',
                description: '允许发送银行的描述文字'
            },
            {
                id: '4',
                title: '网银发送待确认',
                description: '网银发送待确认描述文字'
            },
            {
                id: '5',
                title: '等待发送银行',
                description: '等待发送银行描述文字'
            },
            {
                id: '6',
                title: '银行正在处理',
                description: '银行正在处理描述文字'
            },
            {
                id: '7',
                title: '银行付款成功',
                description: '银行付款成功描述文字'
            }
        ]
    };

    constructor() {
        super();
    }

    ngOnInit() {}
}
