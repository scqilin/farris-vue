import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressStepsWidgetComponent } from './progress-steps-widget.component';

describe('ProgressStepsWidgetComponent', () => {
    let component: ProgressStepsWidgetComponent;
    let fixture: ComponentFixture<ProgressStepsWidgetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProgressStepsWidgetComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProgressStepsWidgetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
