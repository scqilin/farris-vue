import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarrisButtonModule } from '@farris/ui-button';
import { ProgressStepModule } from '@farris/ui-progress-step';
import { FilterPanelModule } from '@farris/ui-filter-panel';
import { SimpleFilterModule } from '@farris/ui-filter';
import { ProgressModule } from '@farris/ui-progress';

import { IndexWidgetComponent } from './index-widget/index-widget.component';
import { ButtonWidgetComponent } from './button-widget/button-widget.component';
import { WidgetViewerComponent } from './widget-viewer/widget-viewer.component';
import { CommonWidgetComponent } from './common-widget/common-widget.component';
import { ProgressStepsWidgetComponent } from './progress-steps-widget/progress-steps-widget.component';
import { SimpleFilterWidgetComponent } from './simple-filter-widget/simple-filter-widget.component';
import { ProgressWidgetComponent } from './progress-widget/progress-widget.component';

@NgModule({
    declarations: [
        IndexWidgetComponent,
        ButtonWidgetComponent,
        WidgetViewerComponent,
        CommonWidgetComponent,
        ProgressStepsWidgetComponent,
        SimpleFilterWidgetComponent,
        ProgressWidgetComponent
    ],
    imports: [CommonModule, FarrisButtonModule, ProgressStepModule, FilterPanelModule, SimpleFilterModule, ProgressModule]
})
export class PreviewWidgetModule {}
