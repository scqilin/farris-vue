import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'docs-widget-viewer',
    templateUrl: './widget-viewer.component.html',
    styleUrls: ['./widget-viewer.component.css']
})
export class WidgetViewerComponent implements OnInit {
    // 标题
    @Input() title = '';

    constructor() {}

    ngOnInit() {}
}
