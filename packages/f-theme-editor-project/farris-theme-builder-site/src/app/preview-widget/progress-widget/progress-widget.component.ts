import { Component, OnInit } from '@angular/core';
import { CommonWidgetComponent } from '../common-widget/common-widget.component';

@Component({
    selector: 'app-progress-widget',
    templateUrl: './progress-widget.component.html',
    styleUrls: ['./progress-widget.component.css']
})
export class ProgressWidgetComponent extends CommonWidgetComponent implements OnInit {
    widgetTitle = '进度条';

    constructor() {
        super();
    }

    ngOnInit() {}
}
