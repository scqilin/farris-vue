import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
    selector: 'app-common-widget',
    templateUrl: './common-widget.component.html',
    styleUrls: ['./common-widget.component.css']
})
export class CommonWidgetComponent implements OnInit {
    @HostBinding('class.col-12') cls1 = true;

    @HostBinding('class.col-lg-6') cls2 = true;

    constructor() {}

    ngOnInit() {}
}
