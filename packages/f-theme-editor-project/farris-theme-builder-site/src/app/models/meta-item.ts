/* eslint-disable max-classes-per-file */
/* eslint-disable no-use-before-define */
export class MetaItem {
    Name: string;

    Key: string;

    Value: string;

    Type?: string;

    TypeValues?: string;

    TypeValuesArray?: string[];

    Items?: MetaItem[];
}

export class MetaValueChange {
    /** 修改后的值  */
    changeValue: any;

    /** 控件编号 */
    controlKey: string;

    /** 变量修改的类型，基础变量还是高级变量 */
    variableType: string;
}
