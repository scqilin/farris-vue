import { ThemeConfig } from './theme';
import { MetaItem } from './meta-item';

export class Metadata {
    baseParameters?: string[];

    framework?: MetaItem[];

    farris?: MetaItem[];

    themes: ThemeConfig[];

    version: string;
}
