/* eslint-disable no-use-before-define */
import { MetaItem } from './meta-item';

export class MenuItem {
    name: string;

    regs?: {
        [themeName: string]: RegExp[];
    };

    equivalents?: string;

    groups?: MenuItem[];

    items?: MetaItem[];

    route?: string;

    code?: string;

    isFold?: boolean;
}
