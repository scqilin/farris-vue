import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { MenuItem } from '../../models/left-menu-item';
import { SearchNameService } from '../../service/search-name.service';

@Component({
    selector: 'app-search-header',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    @ViewChild('searchInput') searchInput: ElementRef;

    @Input() variableName = '基础变量';

    // 菜单数据
    navDatas: MenuItem[];

    // 是否进入了查询
    inSearch = false;

    // 查询文本
    searchText = '';

    // 过滤出来的数据
    filteredData: MenuItem[] = [];

    /**
     * 查询
     */
    formGroup = new FormGroup({
        searchControl: new FormControl('')
    });

    constructor(private searchNameSer: SearchNameService) {
        this.searchNameSer.getNavDatas().subscribe((datas: MenuItem[]) => {
            this.navDatas = [...datas];
        });
    }

    ngOnInit() {
        this.formGroup.valueChanges.pipe(debounceTime(600)).subscribe((data) => {
            this.searchText = data.searchControl;
            this.menuSearch();
        });
    }

    /**
     * 查询状态变更
     * @param e
     */
    changeSearchState(e: MouseEvent): void {
        this.inSearch = !this.inSearch;
        this.searchText = '';
        if (this.inSearch) {
            // 确定在查询中，聚焦
            setTimeout(() => this.searchInput.nativeElement.focus(), 100);
        } else {
            this.menuSearch();
        }

        e.stopPropagation();
    }

    /**
     * 查询变量列表
     */
    menuSearch(): void {
        const keyword = this.searchNameSer.getRealName(this.searchText.toLowerCase());

        const addFilteredMenuItem = (item: MenuItem, itemsArray: MenuItem[]): void => {
            // 当前项如果名称包含在其中,则取他的item构造结果
            if (item.name.toLowerCase().indexOf(keyword) >= 0) {
                itemsArray.push(item);
                return;
            }

            if (!item.items) return;

            const filteredItems = item.items.filter((metaItem) => {
                const itemName = this.searchNameSer.getRealName(metaItem.Name).toLowerCase();
                return itemName.indexOf(keyword) >= 0;
            });

            if (filteredItems.length) {
                itemsArray.push({ name: item.name, items: filteredItems, route: item.route });
            }
        };

        if (!keyword.length) {
            this.filteredData = [];
            // this.filteredData[0] = this.workArea;
            this.searchNameSer.updateFilterDatas(this.navDatas);
            return;
        }

        this.filteredData = [];
        // 归集数据后的所有的变量集合 []
        this.navDatas.forEach((navDataItem) => {
            // 先构造items里面的数据
            addFilteredMenuItem(navDataItem, this.filteredData);

            if (navDataItem.groups) {
                // 如果有分组再构造分组里的数据
                const filteredDataGroups: MenuItem[] = [];

                navDataItem.groups.forEach((group) => addFilteredMenuItem(group, filteredDataGroups));

                if (filteredDataGroups.length) {
                    const existingGroup = this.filteredData.filter((i) => i.name === navDataItem.name);
                    if (!existingGroup.length) {
                        this.filteredData.push({ name: navDataItem.name, groups: filteredDataGroups, route: navDataItem.route });
                    }
                }
            }
        });
        this.searchNameSer.updateFilterDatas(this.filteredData);
    }
}
