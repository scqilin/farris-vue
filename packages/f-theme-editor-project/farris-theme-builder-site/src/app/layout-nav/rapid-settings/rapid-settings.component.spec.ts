import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RapidSettingsComponent } from './rapid-settings.component';

describe('RapidSettingsComponent', () => {
    let component: RapidSettingsComponent;
    let fixture: ComponentFixture<RapidSettingsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RapidSettingsComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RapidSettingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
