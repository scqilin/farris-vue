import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { SettingsAlias } from './rapid-settings.aliases';
import { MetadataService } from '../../service/metadata.service';
import { SearchNameService } from '../../service/search-name.service';
import { ThemeRouteChangeService } from '../../service/theme-route-change.service';
import { MenuItem } from 'src/app/models/left-menu-item';

@Component({
    selector: 'app-rapid-settings',
    templateUrl: './rapid-settings.component.html',
    styleUrls: ['./rapid-settings.component.css']
})
export class RapidSettingsComponent implements OnInit, OnDestroy {
    value = 'rgba(255,0,0,0.5)';

    subscription: Subscription;

    // 变量数据
    editorsData: MenuItem[];

    themeSettings = null;

    // 颜色
    themeColor: string;

    // 大小
    themeSize: string;

    // 清空css事件
    @Output() clearCSSEvent = new EventEmitter();

    constructor(
        private metadataService: MetadataService,
        private nameSer: SearchNameService,
        private router: Router,
        private themeRouteSer: ThemeRouteChangeService
    ) {
        // 主题大小
        this.themeSize = this.themeRouteSer.getThemeSize();
        // 默认主题颜色
        this.themeColor = this.themeRouteSer.getThemeColor();
        // 获取主题配置
        this.themeSettings = this.themeRouteSer.getThemSettings();
        // 关联菜单数据
        this.nameSer.getFilterDatas().subscribe((datas: MenuItem[]) => {
            this.editorsData = [...datas];
        });
    }

    updateData(): void {
        // 获取基础变量的定义
        const presetRapidSettings = SettingsAlias.getRapidSettings();
        // 查找配置，构造需要几个主题的基础变量
        const needThemes = ['farris'];
        // 整理主题类型
        presetRapidSettings.forEach((item) => {
            const equThemes = item.equivalents.split(',');
            equThemes.forEach((themeItem) => {
                if (needThemes.findIndex((needThemeItem) => themeItem === needThemeItem) < 0) {
                    needThemes.push(themeItem);
                }
            });
        });

        // 获取基础变量
        this.metadataService.getBaseParameters(needThemes.join(',')).then((commonParameters) => {
            // let datas = SettingsAlias.formatSettings(SettingsAlias.getRapidSettings(), tParameters, this.nameSer);
            const datas = SettingsAlias.formatSettings(presetRapidSettings, commonParameters, this.nameSer);
            // 更新nav数据
            this.nameSer.updateNavDatas(datas);
            // 赋初始值
            this.editorsData = [...datas];
        });
    }

    ngOnInit() {
        this.updateData();
        /**
         * css 每次变更获取最新的变量
         */
        this.subscription = this.metadataService.css.subscribe(({ css, theme }) => {
            this.updateData();
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    /**
     * 修改主题颜色
     */
    changeThemeColor(type: string) {
        if (this.themeColor !== type) {
            this.themeColor = type;
            this.clearCSSEvent.emit();
            this.themeRouteSer.updateThemeColor(type);
        }
    }

    /**
     * 修改主题大小
     */
    changeThemeSize(type: string) {
        if (this.themeSize !== type) {
            this.themeSize = type;
            this.clearCSSEvent.emit();
            this.themeRouteSer.updateThemeSize(type);
        }
    }

    /**
     * 判断収折
     * @param groupInfoIndex
     * @param groupIndex
     */
    foldOrExpandGroup(groupInfoIndex, groupIndex) {
        const groupInfos = this.editorsData[groupInfoIndex];
        const currentGroup = groupInfos.groups[groupIndex];
        if (currentGroup.items.length <= 3) {
            return false;
        }
        currentGroup.isFold = !currentGroup.isFold;
    }
}
