/* eslint-disable no-useless-escape */
/* eslint-disable max-len */
import { Component, OnInit, HostBinding, Renderer2, ElementRef, ChangeDetectorRef } from '@angular/core';
import { LoadingService } from '../../service/loading.service';
import { ThemeExtendCSSService } from '../../service/theme-extend-css.service';

@Component({
    selector: 'app-theme-nav',
    templateUrl: './theme-nav.component.html',
    styleUrls: ['./theme-nav.component.css']
})
export class ThemeNavComponent implements OnInit {
    @HostBinding('class.tool-layout-nav') cls = true;

    tabItems = [
        {
            code: 'rapid',
            name: '快速配置',
            icon: 'nav-icon-rapid'
        },
        {
            code: 'extend',
            name: '自定义',
            icon: 'nav-icon-css'
        }
    ];

    // 标记当前标签的code
    tabActiveCode;

    // 样式扩展区域
    themeExtend = '';

    // 标记 monacoInited是否要初始化
    monacoInited = false;

    /**
     * monaco配置
     */
    monacoOptions = {
        theme: 'vs-dark',
        language: 'css',
        automaticLayout: true
    };

    /**
     * monaco 编辑器指针
     */
    private _editor: any;

    constructor(
        private cd: ChangeDetectorRef,
        private loadingSer: LoadingService,
        private render: Renderer2,
        private el: ElementRef,
        private extendCSSSer: ThemeExtendCSSService
    ) {
        this.navTabClickHandler(this.tabItems[0].code);
    }

    ngOnInit() {}

    /**
     * 标签点击
     * @param code
     */
    navTabClickHandler(code: string) {
        if (!this.monacoInited && code === 'extend') {
            this.loadingSer.show();
        }
        this.tabActiveCode = code;
    }

    /**
     *
     * @param ev
     */
    navStateChangeHandler(ev) {}

    /**
     * 代码编辑工具
     * @param event
     */
    onMonacoInitHandler(event) {
        this.monacoInited = true;
        this.loadingSer.hide();

        this._editor = event.editor;
        this.cd.detectChanges();
    }

    /**
     * css格式化后重新赋值
     */
    formatCodeHandler() {
        this.themeExtend = this._cssFormat(this.themeExtend);
    }

    /**
     * css格式化
     * @param code
     */
    private _cssFormat(code) {
        code = code.replace(/(\s){2,}/gi, '$1');
        code = code.replace(/(\S)\s*\{/gi, '$1 {');
        code = code.replace(/\*\/(.[^\}\{]*)}/gi, '*/\n$1}');
        code = code.replace(/\/\*/gi, '\n/*');
        code = code.replace(/;\s*(\S)/gi, ';\n\t$1');
        code = code.replace(/\}\s*(\S)/gi, '}\n$1');
        code = code.replace(/\n\s*\}/gi, '\n}');
        code = code.replace(/\{\s*(\S)/gi, '{\n\t$1');
        code = code.replace(/(\S)\s*\*\//gi, '$1*/');
        code = code.replace(/\*\/\s*([^\}\{]\S)/gi, '*/\n\t$1');
        code = code.replace(/(\S)\}/gi, '$1\n}');
        code = code.replace(/(\n){2,}/gi, '\n');
        code = code.replace(/:/gi, ': ');
        code = code.replace(/ {2}/gi, ' ');
        return code;
    }

    /**
     * 清空CSS
     */
    clearCSSHandler() {
        this.themeExtend = '';
        this.extendCSSSer.updateCSS(this.themeExtend);
    }

    /**
     * 创建主题
     */
    buildThemeHandler() {
        this.extendCSSSer.updateCSS(this.themeExtend);
    }
}
