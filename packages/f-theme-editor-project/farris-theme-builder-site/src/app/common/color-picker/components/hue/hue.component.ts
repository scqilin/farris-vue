import {
    Component,
    OnInit,
    Input,
    SimpleChanges,
    Renderer2,
    Inject,
    ElementRef,
    Output,
    EventEmitter,
    ViewChild,
    HostListener
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { BaseComponent } from '../base-component';
import { Color } from '../../helpers/color.class';

@Component({
    selector: 'hue',
    templateUrl: './hue.component.html',
    styleUrls: [`./hue.component.scss`]
})
export class HueComponent extends BaseComponent implements OnInit {
    @Input()
    public hue: Color;

    @Input()
    public color: Color;

    private isVertical = false;

    @Input()
    public set vertical(value: string) {
        this.isVertical = true;
    }

    @Output()
    public hueChange = new EventEmitter<Color>(false);

    @Output()
    public colorChange = new EventEmitter<Color>(false);

    @ViewChild('thumb')
    public thumb: ElementRef;

    constructor(renderer: Renderer2, @Inject(DOCUMENT) document, elementRef: ElementRef) {
        super(document, elementRef, renderer);
    }

    @HostListener('mousedown', ['$event'])
    @HostListener('touchstart', ['$event'])
    public onClick(event: any): void {
        this.onEventChange(event);
    }

    ngOnInit(): void {}

    // color 改变时更改 cursor 位置
    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.hue && changes.hue.previousValue !== changes.hue.currentValue) {
            const hsva = this.hue.getHsva();
            this.changePointerPosition(hsva.hue);
        }
    }

    public movePointer({ x, y, height, width }): void {
        const hue = this.isVertical ? (y / height) * 360 : (x / width) * 360;
        this.changePointerPosition(hue);

        const color = this.color.getHsva();
        const newColor = new Color().setHsva(hue, color.saturation, color.value, color.alpha);
        const newHueColor = new Color().setHsva(hue, 100, 100, color.alpha);

        this.hueChange.emit(newHueColor);
        this.colorChange.emit(newColor);
    }

    private changePointerPosition(hue: number): void {
        const x = Math.max(0, Math.min((hue / 360) * 100, 100));
        const orientation = this.isVertical ? 'top' : 'left';
        this.renderer.setStyle(this.thumb.nativeElement, orientation, `${x}%`);
    }
}
