import {
    Component,
    OnInit,
    Input,
    SimpleChanges,
    Renderer2,
    Inject,
    ElementRef,
    Output,
    EventEmitter,
    ViewChild,
    HostBinding,
    HostListener
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { BaseComponent } from '../base-component';
import { Color } from '../../helpers/color.class';

@Component({
    selector: 'alpha',
    templateUrl: './alpha.component.html',
    styleUrls: [`./alpha.component.scss`]
})
export class AlphaComponent extends BaseComponent implements OnInit {
    @Input()
    public color: Color;

    private isVertical = false;

    @Input()
    public set vertical(value: string) {
        this.isVertical = true;
    }

    @Output()
    public colorChange = new EventEmitter<Color>(false);

    @ViewChild('thumb')
    public thumb: ElementRef;

    constructor(renderer: Renderer2, @Inject(DOCUMENT) document, elementRef: ElementRef) {
        super(document, elementRef, renderer);
    }

    @HostListener('mousedown', ['$event'])
    @HostListener('touchstart', ['$event'])
    public onClick(event: any): void {
        this.onEventChange(event);
    }

    ngOnInit(): void {}

    // color 改变时更改 cursor 位置
    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.color && changes.color.previousValue !== changes.color.currentValue) {
            const hsva = this.color.getHsva();
            this.changePointerPosition(hsva.alpha);
        }
    }

    public movePointer({ x, y, height, width }): void {
        const alpha = this.isVertical ? y / height : x / width;
        this.changePointerPosition(alpha);

        const hsva = this.color.getHsva();
        const newColor = new Color().setHsva(hsva.hue, hsva.saturation, hsva.value, alpha);
        this.colorChange.emit(newColor);
    }

    private changePointerPosition(alpha: number): void {
        const x = Math.max(0, Math.min(alpha * 100, 100));
        const orientation = this.isVertical ? 'top' : 'left';
        this.renderer.setStyle(this.thumb.nativeElement, orientation, `${x}%`);
    }

    public get gradient(): string {
        const rgba = this.color.getRgba();
        const orientation = this.isVertical ? 'bottom' : 'right';
        return `linear-gradient(to ${orientation}, rgba(${rgba.red}, ${rgba.green}, ${rgba.blue}, 0) 0%, rgb(${rgba.red}, ${rgba.green}, ${rgba.blue}) 100%)`;
    }
}
