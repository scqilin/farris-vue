import {
    Component,
    OnInit,
    Input,
    SimpleChanges,
    Renderer2,
    Inject,
    ElementRef,
    Output,
    EventEmitter,
    ViewChild,
    HostBinding,
    HostListener
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { BaseComponent } from '../base-component';
import { Color } from '../../helpers/color.class';

@Component({
    selector: 'input-btns',
    templateUrl: './input-btns.component.html',
    styleUrls: [`./input-btns.component.scss`]
})
export class InputBtnsComponent extends BaseComponent implements OnInit {
    @Input()
    public hue: Color;

    @Input()
    public color: Color;

    @Output()
    public colorChange = new EventEmitter<Color>(false);

    @ViewChild('cursor')
    public cursor: ElementRef;

    constructor(renderer: Renderer2, @Inject(DOCUMENT) document, elementRef: ElementRef) {
        super(document, elementRef, renderer);
    }

    @HostListener('mousedown', ['$event'])
    @HostListener('touchstart', ['$event'])
    public onClick(event: any): void {
        this.onEventChange(event);
    }

    ngOnInit(): void {}

    // color 改变时更改 cursor 位置
    public ngOnChanges(changes: SimpleChanges): void {}

    public movePointer({ x, y, height, width }): void {}

    private changePointerPosition(x: number, y: number): void {
        this.renderer.setStyle(this.cursor.nativeElement, 'top', `${100 - y}%`);
        this.renderer.setStyle(this.cursor.nativeElement, 'left', `${x}%`);
    }
}
