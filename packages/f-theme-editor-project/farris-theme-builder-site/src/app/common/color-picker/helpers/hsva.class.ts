import { BaseColor } from './base-color.class';

export class Hsva extends BaseColor {

    constructor(public hue: number, public saturation: number, public value: number, public alpha: number) {
        super();
    }

    public toString(showAlphaChannel = true): string {
        return showAlphaChannel ? `hsva(${this.getHue()}, ${this.getSaturation()}%, ${this.getValue()}%, ${this.getAlpha()})`
            : `hsv(${this.getHue()}, ${this.getSaturation()}%, ${this.getValue()}%)`;
    }

    public getHue() {
        return Math.round(this.hue);
    }

    public getSaturation() {
        return Math.round(this.saturation);
    }

    public getValue() {
        return Math.round(this.value);
    }

    public getAlpha(): number {
        return Math.round(this.alpha * 100) / 100;
    }
}
