import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ColorPickerComponent } from './color-picker.component';
import { SvPanelComponent } from './components/sv-panel/sv-panel.component';
import { HueComponent } from './components/hue/hue.component';
import { AlphaComponent } from './components/alpha/alpha.component';
import { PresetComponent } from './components/preset/preset.component';

@NgModule({
    declarations: [ColorPickerComponent, SvPanelComponent, HueComponent, AlphaComponent, PresetComponent],
    providers: [],
    imports: [CommonModule, FormsModule],
    exports: [ColorPickerComponent]
})
export class ColorPickerModule {}
