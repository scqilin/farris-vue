import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrameworkHeaderComponent } from './framework-header/framework-header.component';

@NgModule({
    declarations: [FrameworkHeaderComponent],
    imports: [CommonModule],
    exports: [FrameworkHeaderComponent]
})
export class FrameworkContainerModule {}
