import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameworkHeaderComponent } from './framework-header.component';

describe('FrameworkHeaderComponent', () => {
    let component: FrameworkHeaderComponent;
    let fixture: ComponentFixture<FrameworkHeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FrameworkHeaderComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FrameworkHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
