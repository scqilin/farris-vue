import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ThemeExtendCSSService {
    private extendCSS = '';

    constructor() {}

    getCSS() {
        return this.extendCSS;
    }

    updateCSS(cssStr) {
        this.extendCSS = cssStr;
    }
}
