import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ThemeRouteChangeService {
    // 当前主题名称
    private themeName = 'farris';

    // 当前主题大小
    private themeSize = 'default';

    // 当前主题颜色
    private themeColor = 'default';

    // 修改的类型
    private viewType = 'preview-page';

    private urlChangeState = new BehaviorSubject('');

    constructor() {
        this.updateFormatUrl();
    }

    /** 更新主题名称 */
    updateThemeName(str) {
        if (str !== this.themeName) {
            this.themeName = str;
            this.updateFormatUrl();
        }
    }

    /** 获取主题名称 */
    getThemeName() {
        return this.themeName;
    }

    /** 更新主题大小 */
    updateThemeSize(str) {
        if (str !== this.themeSize) {
            this.themeSize = str;
            this.updateFormatUrl();
        }
    }

    getThemeSize() {
        return this.themeSize;
    }

    updateThemeColor(str) {
        if (str !== this.themeColor) {
            this.themeColor = str;
            this.updateFormatUrl();
        }
    }

    getThemeColor() {
        return this.themeColor;
    }

    /** 更新预览类型 */
    updateViewType(str) {
        if (str !== this.viewType) {
            this.viewType = str;
            this.updateFormatUrl();
        }
    }

    getViewType() {
        return this.viewType;
    }

    /** 获取Url更新 */
    getUrlChange() {
        return this.urlChangeState;
    }

    /** 获取默认主题 */
    getDefaultTheme() {
        return { name: this.themeName, colorScheme: this.themeColor + '-' + this.themeSize };
    }

    updateFormatUrl() {
        const tUrl = '' + this.themeName + '/' + this.themeColor + '-' + this.themeSize;
        this.urlChangeState.next(tUrl);
    }

    getFormatUrl() {
        const tUrl = this.themeName + '/' + this.themeColor + '-' + this.themeSize;
        return tUrl;
    }

    /** 获取主题配置 */
    getThemSettings(themeType = '') {
        const themeSettings = {
            farris: {
                themeColor: [
                    {
                        type: 'default',
                        color: 'default',
                        name: '默认主题'
                    },
                    {
                        type: 'green',
                        color: 'green',
                        name: '清新绿'
                    },
                    {
                        type: 'red',
                        color: 'red',
                        name: '中国红'
                    }
                ],
                themeSize: [
                    {
                        type: 'default',
                        size: 'default',
                        name: '紧凑版'
                    },
                    {
                        type: 'loose',
                        size: 'loose',
                        name: '宽松版'
                    }
                ]
            }
        };
        if (themeType === '') {
            themeType = this.themeName;
        }
        return themeSettings[themeType];
    }
}
