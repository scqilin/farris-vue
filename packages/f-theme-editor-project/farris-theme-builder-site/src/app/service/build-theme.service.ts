import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BuilderResult } from '../models/builder-result';
import { BuilderConfig } from '../models/builder-config';
import { Theme } from '../models/theme';
import { Metadata } from '../models/metadata';

@Injectable({
    providedIn: 'root'
})
export class BuildThemeService {
    private url = 'http://localhost:3000';

    constructor(private http: HttpClient) {}

    /**
     * @param theme 比如{colorScheme: "light", name: "farris"}
     * @param config 比如 {"makeSwatch":false,"items":[],"widgets":[],"noClean":true}
     */
    buildTheme(theme, config: BuilderConfig): Promise<BuilderResult> {
        return this.build(theme, config);
    }

    build(theme, config: BuilderConfig): Promise<BuilderResult> {
        config.baseTheme = `${theme.name}.${theme.colorScheme.replace(/-/g, '.')}`;
        // 根据配置信息，发起请求获取当前主题当前颜色下的具体定义；
        // 其中items是修改变量的集合 [{"key":"$f-border-radius","value":"8"}]
        return this.http.post(`${this.url}/buildtheme`, config).toPromise();
    }

    getMetadata(): Promise<Metadata> {
        return this.http.get(`${this.url}/metadata`).toPromise() as Promise<Metadata>;
    }
}
