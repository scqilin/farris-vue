// module
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMonacoEditorModule } from 'angular-monaco-editor';
import { ListNavModule } from '@farris/ui-list-nav';
import { ComboListModule } from '@farris/ui-combo-list';
import { ColorPickerModule } from './common/color-picker/color-picker.module';
import { SwitchModule } from '@farris/ui-switch';
import { FieldGroupModule } from '@farris/ui-field-group';
import { InputGroupModule } from '@farris/ui-input-group';
import { PreviewPageModule } from './preview-page/preview-page.module';
import { PreviewWidgetModule } from './preview-widget/preview-widget.module';
import { AppRoutingModule } from './app-routing.module';
// component
import { AppComponent } from './app.component';
import { ThemeMainComponent } from './layout/theme-main/theme-main.component';
import { LoadingComponent } from './common/loading/loading.component';
import { IframeComponent } from './common/iframe/iframe.component';
import { SearchComponent } from './layout-nav/search/search.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { PreviewLayoutComponent } from './layout/preview-layout/preview-layout.component';
import { IndexTmplComponent } from './preview-page/index-tmpl/index-tmpl.component';
import { ThemeNavComponent } from './layout-nav/theme-nav/theme-nav.component';
import { TextComponent } from './editor/text/text.component';
import { SelectComponent } from './editor/select/select.component';
import { ColorComponent } from './editor/color/color.component';
import { BaseComponent } from './editor/base/base.component';
import { SwitchComponent } from './editor/switch/switch.component';
import { RapidSettingsComponent } from './layout-nav/rapid-settings/rapid-settings.component';
import { SettingsHeaderComponent } from './layout-nav/settings-header/settings-header.component';
import { EditorComponent } from './editor/editor/editor.component';

@NgModule({
    declarations: [
        AppComponent,
        ThemeNavComponent,
        ThemeMainComponent,
        LoadingComponent,
        IframeComponent,
        EditorComponent,
        TextComponent,
        SelectComponent,
        ColorComponent,
        BaseComponent,
        SwitchComponent,
        RapidSettingsComponent,
        SettingsHeaderComponent,
        SearchComponent,
        AppLayoutComponent,
        PreviewLayoutComponent,
        IndexTmplComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        AngularMonacoEditorModule.forRoot({
            baseUrl: 'assets',
            defaultOptions: { scrollBeyondLastLine: false }
        }),
        ReactiveFormsModule,
        AppRoutingModule,
        ListNavModule,
        ColorPickerModule,
        ComboListModule,
        SwitchModule,
        FieldGroupModule,
        InputGroupModule,
        PreviewPageModule,
        PreviewWidgetModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
