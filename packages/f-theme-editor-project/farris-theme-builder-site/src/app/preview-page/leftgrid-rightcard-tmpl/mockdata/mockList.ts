export const mockdata = [
    {
        id: '6a967bdb-2a8e-41f9-afe6-1c918b3d75ad',
        labelCode: '6a967bdb-2a8e-41f9-afe6-1c918b3d75ad',
        code: 'search',
        name: '搜索',
        control: {
            controltype: 'search',
            placeholder: '请输入编号或名称',
            showLabel: false
        }
    },
    {
        id: '6a967bdb-2a8e-41f9-afe6-1c918b3d75aa',
        labelCode: '6a967bdb-2a8e-41f9-afe6-1c918b3d75aa',
        code: 'checkboxGroup',
        name: '业务类型',
        control: {
            controltype: 'checkboxgroup',
            enumValues: [
                { value: 'aa', name: '类型一' },
                { value: 'bb', name: '类型二' },
                { value: 'cc', name: '类型三' },
                { value: 'dd', name: '类型四' }
            ],
            isExtend: true,
            // "required":true
            showLabel: false
        }
    },

    {
        id: '6a967bdb-2a8e-41f9-afe6-1c918b3d75af',
        labelCode: '6a967bdb-2a8e-41f9-afe6-1c918b3d75af',
        code: 'dropdown',
        name: '审批状态',
        control: {
            controltype: 'dropdown',
            valueType: '1',
            enumValues: [
                { value: 'Billing', name: '制单' },
                { value: 'SubmitApproval', name: '提交审批' },
                { value: 'Approved', name: '审批通过' },
                { value: 'ApprovalNotPassed', name: '审批不通过' }
            ],
            isExtend: true,
            showLabel: false
        }
    },
    {
        id: '6a967bdb-2a8e-41f9-afe6-1c918b3d75ae',
        labelCode: '6a967bdb-2a8e-41f9-afe6-1c918b3d75ae',
        code: 'singledate',
        name: '单据日期',
        control: {
            controltype: 'singleDate',
            placeholder: '请选择日期',
            isExtend: true
        }
    },
    {
        id: '6a967bdb-2a8e-41f9-afe6-1c918b3d755',
        labelCode: '6a967bdb-2a8e-41f9-afe6-1c918b3d755',
        code: 'text',
        name: '姓名',
        control: {
            controltype: 'text',
            placeholder: '请输入',
            isExtend: true,
            showLabel: false
        }
    }
];
