import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftgridRightcardTmplComponent } from './leftgrid-rightcard-tmpl.component';

describe('LeftgridRightcardTmplComponent', () => {
    let component: LeftgridRightcardTmplComponent;
    let fixture: ComponentFixture<LeftgridRightcardTmplComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LeftgridRightcardTmplComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LeftgridRightcardTmplComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
