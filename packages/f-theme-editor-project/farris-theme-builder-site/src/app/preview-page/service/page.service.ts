import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PageService {
    private allTabData = [
        {
            active: false,
            appPath: 'dashboard',
            appType: '',
            removable: false,
            tabId: 'defaultpage-dashboard',
            title: '首页',
            visible: true
        },
        {
            active: true,
            appPath: 'bo-xempattachfront',
            appType: 'menu',
            funcId: 'bulk-edit-tmpl',
            mode: 'spa',
            removable: true,
            tabId: 'bulk-edit-tmpl',
            title: '批量编辑列表',
            visible: true
        },
        {
            active: false,
            appPath: 'bo-xempattachfront',
            appType: 'menu',
            funcId: 'master-slave-tmpl',
            mode: 'spa',
            removable: true,
            tabId: 'master-slave-tmpl',
            title: '主从表',
            visible: true
        },
        {
            active: false,
            appPath: 'bo-xempattachfront',
            appType: 'menu',
            funcId: 'leftgrid-rightcard-tmpl',
            mode: 'spa',
            removable: true,
            tabId: 'leftgrid-rightcard-tmpl',
            title: '左列右卡',
            visible: true
        }
    ];

    private data: any;

    // 标记
    private selectedTabIdSubject = new BehaviorSubject('bulk-edit-tmpl');

    constructor(private http: HttpClient) {}

    // 返回Tab所有列表
    getTabList() {
        return this.allTabData;
    }

    // 获取当前选中标签的动态动向
    getSelectedTabId() {
        return this.selectedTabIdSubject;
    }

    // 更新
    updataSelectedTabId(tabId) {
        this.selectedTabIdSubject.next(tabId);
    }

    // 卡片
    viewCard() {
        return this.http.get('assets/demo/order-card-view.json');
    }

    // 树
    // viewTreeGrid() {
    //   return this.http.get('assets/demo/order-tree.json');
    // }
    // 列表
    viewList() {
        return this.http.get('assets/demo/order-list.json');
    }

    // 物料
    viewMaterial() {
        return this.http.get('assets/demo/order-material.json');
    }
    // 在线演示
    // viewListOnline() {
    //   return this.http.get('assets/demo/sales-order-data-online.json');
    // }
    // 邮件
    // viewMail() {
    //   return this.http.get('assets/demo/mail.json');
    // }
    // 订单向导中的数据
    // viewWizardList() {
    //   return this.http.get('assets/demo/order.json');
    // }
}
