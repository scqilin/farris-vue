import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexTmplComponent } from './index-tmpl.component';

describe('IndexTmplComponent', () => {
    let component: IndexTmplComponent;
    let fixture: ComponentFixture<IndexTmplComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [IndexTmplComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IndexTmplComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
