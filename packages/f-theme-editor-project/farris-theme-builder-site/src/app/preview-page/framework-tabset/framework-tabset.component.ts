import { Component, OnInit} from '@angular/core';
import { PageService } from '../service/page.service';

@Component({
    selector: 'app-framework-tabset',
    templateUrl: './framework-tabset.component.html',
    styleUrls: ['./framework-tabset.component.css']
})
export class FrameworkTabsetComponent implements OnInit {
    // 临时赋值
    allTabs = [];

    // 页面服务
    constructor(private pgService: PageService) {
        this.allTabs = this.pgService.getTabList();
    }

    ngOnInit() {}

    ngAfterViewInit() {}

    removeTabHandler(app) {}

    /**
     * 关闭所有，除了首页
     */
    removeAllTabs(arg: any) {
        // if (arg && arg === "closeAllTabs") {
        // }
    }

    /**
     *
     * @param app
     */
    selectTabHandler(app) {
        this.pgService.updataSelectedTabId(app.tabId);
    }

    /**
     * header hide mode:
     * 1. header hide
     * 2. tabs hide
     * 3. header、tabs hide
     * 4. home page hide
     * 7. header、tabs、dashboard hide
     */
    private headerHide(): void {
        // const hideMode = this.frmConfig.headerHideMode();
        // if (!!hideMode) {
        //   switch (hideMode) {
        //     case '5':
        //     case '1': this.hideStyle('header'); break;
        //     case '6':
        //     case '2': this.hideStyle('iframetab'); break;
        //     case '7':
        //     case '3':
        //       this.hideStyle('header');
        //       this.hideStyle('iframetab');
        //       break;
        //     default: break;
        //   }
        // }
    }

    private hideStyle(elementId: string): void {
        if (document.getElementById(elementId)) {
            document.getElementById(elementId).style.display = 'none';
        }
    }
}
