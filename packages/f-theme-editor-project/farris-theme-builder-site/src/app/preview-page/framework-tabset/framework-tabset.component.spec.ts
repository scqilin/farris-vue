import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameworkTabsetComponent } from './framework-tabset.component';

describe('FrameworTabsetComponent', () => {
    let component: FrameworkTabsetComponent;
    let fixture: ComponentFixture<FrameworkTabsetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FrameworkTabsetComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FrameworkTabsetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
