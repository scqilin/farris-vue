import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterSlaveTmplComponent } from './master-slave-tmpl.component';

describe('MasterSlaveTmplComponent', () => {
    let component: MasterSlaveTmplComponent;
    let fixture: ComponentFixture<MasterSlaveTmplComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MasterSlaveTmplComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MasterSlaveTmplComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
