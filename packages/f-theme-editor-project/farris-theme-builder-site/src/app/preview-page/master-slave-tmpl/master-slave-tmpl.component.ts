/* eslint-disable no-prototype-builtins */
import { Component, OnInit } from '@angular/core';
import { PageService } from '../service/page.service';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-master-slave-tmpl',
    templateUrl: './master-slave-tmpl.component.html',
    styleUrls: ['./master-slave-tmpl.component.css']
})
export class MasterSlaveTmplComponent implements OnInit {
    // 主表卡片数据
    mainCardData = null;

    subGridColumnA = [];

    subGridDataA = [];

    subGridDataTotalA = 0;

    subGridColumnB = [];

    subGridDataB = [];

    subGridDataTotalB = 0;

    // 子表Tab标签按钮
    subGridToolbarA = {
        position: 'inHead',
        contents: [
            {
                id: 'a',
                disabled: false,
                title: '新增',
                click: 'addRecord',
                appearance: {
                    class: 'btn btn-primary f-btn-ml'
                },
                visible: true
            },
            {
                id: 'b',
                disabled: false,
                title: '删除',
                click: 'deleteRecord',
                appearance: {
                    class: 'btn btn-primary f-btn-ml'
                },
                visible: true
            },
            {
                id: 'c',
                disabled: false,
                title: '更新',
                click: 'updateRecord',
                appearance: {
                    class: 'btn btn-primary f-btn-ml'
                },
                visible: true
            },
            {
                id: 'd',
                disabled: true,
                title: '查询',
                click: 'selectRecord',
                appearance: {
                    class: 'btn btn-primary f-btn-ml'
                },
                visible: false
            }
        ]
    };

    subGridToolbarB = {
        position: 'inHead',
        contents: [
            {
                id: 'a',
                disabled: false,
                title: '新增',
                click: 'addRecord',
                appearance: {
                    class: 'btn btn-primary f-btn-ml'
                },
                visible: true
            },
            {
                id: 'b',
                disabled: false,
                title: '删除',
                click: 'deleteRecord',
                appearance: {
                    class: 'btn btn-primary f-btn-ml'
                },
                visible: true
            }
        ]
    };

    // Tab 处理按钮的禁用状态
    subGridBtnStates = new BehaviorSubject({});

    // 处理下拉
    valObj = { id: '9', text: '中国4' };

    listItemsDropDown = [
        { id: '1', text: '中国' },
        { id: '2', text: '俄罗斯' },
        { id: '3', text: '菲律宾' },
        { id: '4', text: '越南' },
        { id: '5', text: '老挝' },
        { id: '6', text: '中国1' },
        { id: '7', text: '中国2' },
        { id: '8', text: '中国3' },
        { id: '9', text: '中国4' }
    ];

    constructor(private orderService: PageService) {
        // 卡片取数
        this.orderService.viewCard().subscribe((data: any) => {
            if (!data) {
                return;
            }
            this.mainCardData = data;
        });
        // 从表一取数
        this.orderService.viewMaterial().subscribe((data: any) => {
            this.subGridDataA = data;
            this.subGridDataTotalA = this.subGridDataA.length;
        });
    }

    ngOnInit() {
        const stateResults = {};
        [this.subGridToolbarA.contents, this.subGridToolbarB.contents].forEach((item, index) => {
            item.forEach((btnInfo) => {
                if (!stateResults.hasOwnProperty(btnInfo.id)) {
                    stateResults[btnInfo.id] = false;
                }
            });
        });
        this.subGridBtnStates.next(stateResults);
        // 从表一 列配置
        this.subGridColumnA = [
            { field: 'WLBH', width: 180, title: '物料编号' },
            { field: 'WLMC', width: 180, title: '物料名称' },
            { field: 'WLMS', width: 180, title: '物料描述' },
            { field: 'WLTZ', width: 180, title: '物料特征' },
            { field: 'WLBM', width: 180, title: '物料别名' },
            { field: 'KHBH', width: 200, title: '客户物料编号' },
            { field: 'ZSL', width: 180, title: '主数量' },
            { field: 'ZDW', width: 180, title: '主单位' },
            { field: 'ZHZDJ', width: 200, title: '折前主单价' },
            { field: 'ZDJ', width: 200, title: '主单价' },
            { field: 'FSL', width: 200, title: '辅数量' },
            { field: 'FDW', width: 200, title: '辅单位' },
            { field: 'ZQFDJ', width: 200, title: '折前辅单价' },
            { field: 'FDJ', width: 180, title: '辅单价' }
        ];
        // 从表二 列配置
        this.subGridColumnB = [
            { field: '', width: 180, title: '送达方' },
            { field: '', width: 180, title: '送货地址' },
            { field: '', width: 180, title: '收货人' },
            { field: '', width: 180, title: '收货电话' },
            { field: '', width: 180, title: '收获地区' },
            { field: '', width: 200, title: '工厂' },
            { field: '', width: 180, title: '发货库存组织' },
            { field: '', width: 180, title: '发货仓库' },
            { field: '', width: 200, title: '发货条件' },
            { field: '', width: 200, title: '物流组织' },
            { field: '', width: 200, title: '计划交货日期' },
            { field: '', width: 200, title: '客户要求交货日期' },
            { field: '', width: 200, title: '货位' },
            { field: '', width: 180, title: '允许不足交货' },
            { field: '', width: 180, title: '不足交货容差(%)' },
            { field: '', width: 180, title: '允许超量交货' },
            { field: '', width: 180, title: '超量交货容差(%)' },
            { field: '', width: 180, title: '交货相关' },
            { field: '', width: 180, title: '补货相关' }
        ];
    }
}
