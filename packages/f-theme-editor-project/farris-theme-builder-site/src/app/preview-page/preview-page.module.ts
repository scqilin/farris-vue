import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FDropdownDirectiveTypeModule } from '@farris/ui-dropdown';
import { FarrisSectionModule } from '@farris/ui-section';
import { FarrisDatePickerModule } from '@farris/ui-datepicker';
import { LookupModule } from '@farris/ui-lookup';
import { DatagridModule } from '@farris/ui-datagrid';
import { FarrisTabsModule } from '@farris/ui-tabs';
import { ComboListModule } from '@farris/ui-combo-list';
import { FarrisTooltipModule } from '@farris/ui-tooltip';
import { FResponseToolbarModule } from '@farris/ui-response-toolbar';
import { SplitterModule } from '@farris/ui-splitter';
import { AngularDraggableModule } from '@farris/ui-draggable';
import { ListFilterModule } from '@farris/ui-list-filter';
import { FrameworkContainerModule } from '../framework/framework-container.module';
import { FrameworkTabsModule } from '../framework-tabs/tabs.module';
import { PageService } from './service/page.service';
import { BulkEditTmplComponent } from './bulk-edit-tmpl/bulk-edit-tmpl.component';
import { MasterSlaveTmplComponent } from './master-slave-tmpl/master-slave-tmpl.component';
import { LeftgridRightcardTmplComponent } from './leftgrid-rightcard-tmpl/leftgrid-rightcard-tmpl.component';
import { SchemeComponent } from './common/scheme/scheme.component';
import { HeaderComponent } from './common/header/header.component';
import { FrameworkTabsetComponent } from './framework-tabset/framework-tabset.component';
import { PageTmplComponent } from './page-tmpl/page-tmpl.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FarrisSectionModule,
        FDropdownDirectiveTypeModule,
        FarrisDatePickerModule,
        FarrisTabsModule,
        ComboListModule,
        FarrisTooltipModule,
        LookupModule,
        DatagridModule,
        SplitterModule,
        ListFilterModule,
        AngularDraggableModule,
        FResponseToolbarModule,
        FrameworkContainerModule,
        FrameworkTabsModule.forRoot()
    ],
    declarations: [
        BulkEditTmplComponent,
        MasterSlaveTmplComponent,
        LeftgridRightcardTmplComponent,
        SchemeComponent,
        HeaderComponent,
        FrameworkTabsetComponent,
        PageTmplComponent
    ],
    exports: [FrameworkTabsetComponent, PageTmplComponent],
    providers: [PageService]
})
export class PreviewPageModule {}
