import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkEditTmplComponent } from './bulk-edit-tmpl.component';

describe('BulkEditTmplComponent', () => {
    let component: BulkEditTmplComponent;
    let fixture: ComponentFixture<BulkEditTmplComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BulkEditTmplComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BulkEditTmplComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
