import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { PageService } from '../service/page.service';

@Component({
    selector: 'app-bulk-edit-tmpl',
    templateUrl: './bulk-edit-tmpl.component.html',
    styleUrls: ['./bulk-edit-tmpl.component.css']
})
export class BulkEditTmplComponent implements OnInit {
    maingridcolumns = [];

    mainGridData = [];

    mainGridTotal = 0;

    @ViewChild('cell2') cell2: TemplateRef<any>;

    @ViewChild('cell3') cell3: TemplateRef<any>;

    constructor(private orderService: PageService) {
        this.orderService.viewList().subscribe((data: any) => {
            this.mainGridData = data;
            this.mainGridTotal = data.length;
        });
    }

    ngOnInit() {
        this.maingridcolumns = [
            { field: '', width: 100, title: '加急', template: this.cell2 },
            {
                field: 'DDRQ',
                width: 200,
                title: '单据日期'
            },
            { field: 'DDBH', width: 200, title: '订单编号', template: this.cell3, sortable: true },
            { field: 'KHMC', width: 200, title: '客户名称', sortable: true },
            { field: 'KHBM', width: 200, title: '客户别名' },
            { field: 'BM', width: 200, title: '部门' },
            { field: 'YWY', width: 200, title: '业务员' },
            { field: 'ZJE', width: 100, title: '总金额', sortable: true },
            { field: 'BZ', width: 100, title: '币种' },
            {
                title: '管理',
                width: 200,
                align: 'center',
                hAlign: 'center'
            }
        ];
    }

    getBadgeCls(rowIndex) {
        const result = rowIndex % 6;
        let cls = '';
        switch (result) {
        case 0:
            cls = '-info';
            break;
        case 1:
            cls = '-success';
            break;
        case 2:
            cls = '-warning';
            break;
        case 3:
            cls = '-danger';
            break;
        case 4:
            cls = '-continue';
            break;
        default:
            cls = '-primary';
        }
        return 'badge badge-arrow-left' + cls;
    }
}
