import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    private _type = 'list';

    @Input()
    set type(typeStr: string) {
        switch (typeStr) {
        case 'card':
            this.title = this._cardTitle;
            this.toolbarData=[].concat(this._cardToolbar);
            break;
        default:
            this.title = this._listTitle;
            this.toolbarData = [].concat(this._listToolbar);
        }
    }

    get type() {
        return this._type;
    }

    // 卡片标题
    private _cardTitle = "销售详情";

    // 列表标题
    private _listTitle = "销售列表";

    // 卡片-工具栏数据
    private _cardToolbar = [
        {
            id: 'cardtoolbar-001',
            text: '引入银行交易明细',
            class: '',
            disabled: false
        },
        {
            id: 'cardtoolbar-002',
            text: '引入存款变动通知',
            disabled: true
        },
        {
            id: 'cardtoolbar-003',
            text: '编辑',
            disabled: true
        },
        {
            id: 'cardtoolbar-004',
            text: '保存',
            class: 'btn-primary',
            disabled: false
        },
        {
            id: 'cardtoolbar-005',
            text: '取消',
            disabled: false
        },
        {
            id: 'cardtoolbar-006',
            text: '提交',
            disabled: true
        },
        {
            id: 'cardtoolbar-007',
            text: '撤回',
            disabled: true
        },
        {
            id: 'cardtoolbar-008',
            text: '电子影像',
            disabled: false
        },
        {
            id: 'cardtoolbar-009',
            text: '关闭',
            disabled: false
        }
    ];

    // 列表-工具栏数据
    private _listToolbar = [
        {
            id: 'toolbar-001',
            text: '新增',
            class: 'btn-primary',
            disabled: true
        },
        {
            id: 'toolbar-002',
            text: '编辑',
            disabled: false
        },
        {
            id: 'toolbar-003',
            text: '查看',
            disabled: false
        },
        {
            id: 'toolbar-004',
            text: '删除',
            disabled: true
        },
        {
            id: 'toolbar-005',
            text: '关闭',
            disabled: false
        }
    ];

    // 初始化
    title = this._listTitle;

    toolbarData = this._listToolbar;

    constructor() { }

    ngOnInit() {
    }

}
