import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTmplComponent } from './page-tmpl.component';

describe('PageTmplComponent', () => {
    let component: PageTmplComponent;
    let fixture: ComponentFixture<PageTmplComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PageTmplComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PageTmplComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
