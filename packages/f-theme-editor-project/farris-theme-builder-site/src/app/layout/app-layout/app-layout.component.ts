import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
    selector: 'app-app-layout',
    templateUrl: './app-layout.component.html',
    styleUrls: ['./app-layout.component.css']
})
export class AppLayoutComponent implements OnInit {
    @HostBinding('class.tool-layout') cls = true;

    constructor() {}

    ngOnInit() {}
}
