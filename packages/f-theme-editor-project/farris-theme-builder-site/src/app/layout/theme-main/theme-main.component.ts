import { Component, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { ThemeRouteChangeService } from '../../service/theme-route-change.service';

@Component({
    selector: 'app-theme-main',
    templateUrl: './theme-main.component.html',
    styleUrls: ['./theme-main.component.css']
})
export class ThemeMainComponent implements OnInit {
    @HostBinding('class.tool-layout-main') cls = true;

    previewType = '';

    // 设置支持类型
    previewDatas = [
        {
            code: 'preview-page',
            name: '模板'
        },
        {
            code: 'preview-widget',
            name: '组件'
        }
    ];

    constructor(public router: Router, private routeChangeSer: ThemeRouteChangeService) {
        this.previewType = this.routeChangeSer.getViewType();
        this.routeChangeSer.getUrlChange().subscribe((url) => {
            if (url) {
                this.router.navigateByUrl('preview/' + url);
            }
        });
    }

    ngOnInit() {}

    /**
     * 修改路由
     * @param type
     */
    changePreviewType(type: string) {
        if (type !== this.previewType) {
            this.previewType = type || 'preview-page';
            this.routeChangeSer.updateViewType(this.previewType);
            this.router.navigateByUrl('preview/' + this.routeChangeSer.getFormatUrl());
        }
    }
}
