import { Component, OnInit } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { BaseComponent } from '../base/base.component';

@Component({
    selector: 'app-text',
    templateUrl: './text.component.html',
    styleUrls: ['./text.component.css']
})
export class TextComponent extends BaseComponent implements OnInit, ControlValueAccessor {
    public value = 0;

    public disable = false;

    onChange: any = () => {};

    onTouch: any = () => {};

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouch = fn;
    }
}
