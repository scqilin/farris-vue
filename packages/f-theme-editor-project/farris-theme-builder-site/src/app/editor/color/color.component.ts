import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
    selector: 'app-color',
    templateUrl: './color.component.html',
    styleUrls: ['./color.component.css']
})
export class ColorComponent extends BaseComponent implements OnInit {
    presets = [
        '#F44336',
        '#E91E63',
        '#9C27B0',
        '#673AB7',
        '#3f51b5',
        '#2196f3',
        '#03a9f4',
        '#00bcd4',
        '#009688',
        '#4caf50',
        '#8bc34a',
        '#cddc39',
        '#ffeb3b',
        '#ffc107',
        '#ff9800',
        '#ff5722  ',
        '#9e9e9e'
    ];

    constructor() {
        super();
    }

    ngOnInit() {}

    /**
     * 值改变
     */
    valueChangeHandler(val: string) {
        this.valueChange.emit(val.toLocaleLowerCase());
    }
}
