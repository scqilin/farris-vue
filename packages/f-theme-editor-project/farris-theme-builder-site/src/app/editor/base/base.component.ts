import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-base',
    templateUrl: './base.component.html',
    styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {
    // 值
    @Input() value: any;

    // 只读
    @Input() readonly = false;

    // 事件
    @Output() valueChange: EventEmitter<any> = new EventEmitter();

    constructor() {}

    ngOnInit() {}

    /**
     * 值改变
     */
    valueChangeHandler(val: string) {
        this.valueChange.emit(val);
    }
}
