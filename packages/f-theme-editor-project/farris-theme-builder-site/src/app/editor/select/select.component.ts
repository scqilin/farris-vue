import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { MetaItem } from 'src/app/models/meta-item';

@Component({
    selector: 'app-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.css']
})
export class SelectComponent extends BaseComponent implements OnInit {
    // 数据
    @Input() items: MetaItem[];

    constructor() {
        super();
    }

    ngOnInit() {}
}
