import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { MetaItem } from '../../models/meta-item';
import { MetadataService } from '../../service/metadata.service';
import { SafeHtml } from '@angular/platform-browser';
import { SearchNameService } from '../../service/search-name.service';

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.css']
})
export class EditorComponent {
    @HostBinding('class.tool-editor') cls = true;

    @Input('item') item: MetaItem;

    /* 标记变量变更类型，如果是：快速配置 'rapidSettings', 普通配置 ''
     * 快速配置有特殊的颜色关系，需要在服务器端进行特殊处理，所以此处有差异
     */
    @Input('variableType') variableType = '';

    // 查询
    @Input() searchText = '';

    constructor(private names: SearchNameService, private metaRepository: MetadataService) {}

    highlight(text: string): SafeHtml {
        return this.names.getHighlightedForLeftMenuName(text, this.searchText);
    }

    /**
     * 变量修改后是否符合条件要加校验
     * @param e
     * @param key
     */
    valueChanged(e: any, key: string): void {
        this.metaRepository.updateSingleVariable({ changeValue: e, controlKey: key, variableType: this.variableType });
    }
}
