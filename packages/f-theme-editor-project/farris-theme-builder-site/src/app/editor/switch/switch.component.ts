import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
    selector: 'app-switch',
    templateUrl: './switch.component.html',
    styleUrls: ['./switch.component.css']
})
export class SwitchComponent extends BaseComponent implements OnInit {
    constructor() {
        super();
    }

    ngOnInit() {}
}
