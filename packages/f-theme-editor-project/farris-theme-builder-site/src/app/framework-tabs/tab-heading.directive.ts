import { Directive, TemplateRef } from '@angular/core';

import { TabDirective } from './tab.directive';

@Directive({ selector: '[tabHeading]' })
export class TabHeadingDirective {
    templateRef: TemplateRef<any>;

    constructor(templateRef: TemplateRef<any>, tab: TabDirective) {
        tab.headingRef = templateRef;
    }
}
