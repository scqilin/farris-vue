import { defineComponent, onUnmounted } from "vue";
import FTLoading from "./Loading";
import FTLayoutMain from "../components/layout/main";
import FTLayoutNav from "../components/layout/nav";
import FTNotify from "./Notify";
import { themeMetadataUtils } from "@/utils/theme-metadata";
import { useThemeChangeStore } from "@/store/index";
import { StorageThemeCSSUtils } from "@/utils/storage-theme-css";


export default defineComponent({
  name: "FTLayout",
  props: {},
  components: {
    "ft-layout-main": FTLayoutMain,
    "ft-loading": FTLoading,
    "ft-nav": FTLayoutNav,
    "ft-notify":FTNotify
  },
  setup() {
    // 获取主题信息
    const { buildTheme, clearModifiedDataCache } = themeMetadataUtils();
    const themeChangeStore = useThemeChangeStore();
    themeChangeStore.$subscribe((args, state) => {
       // 主题变更时候清除数据，重新发起请求获取信息
      if (state.themeName && state.themeColor) {
        clearModifiedDataCache();
        buildTheme();
      }
    });
    // 发起请求获取信息
    buildTheme();

    onUnmounted(()=>{
      // 移除存储的数据
      StorageThemeCSSUtils().clearCSS();
    });

    return () => (
      <div class="tool-layout">
        <ft-nav></ft-nav>
        <ft-layout-main></ft-layout-main>
        <ft-loading></ft-loading>
        <ft-notify></ft-notify>
      </div>
    );
  }
});
