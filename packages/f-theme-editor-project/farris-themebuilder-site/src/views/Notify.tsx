import { Teleport, defineComponent } from "vue";
import { useLoadingStore, useNotifyStore } from "../store/index";

export default defineComponent({
  name: "FTNotify",
  setup() {
    const notify=useNotifyStore();
    return () => (
        <div>
          {notify.show && (
            <Teleport to="body">
              <div
                class="farris-notify toasty-position-bottom-right"
                style="right: 12px; top: 30px"
              >
                <div>
                  <div class={["toast toasty-theme-bootstrap animated fadeOut",`toasty-type-${notify.type}`]}>
                    <button class="toast-close f-btn-icon f-bare">
                      <span class="f-icon modal_close"></span>
                    </button>
                    <section class="modal-tips">
                      <div class="float-left modal-tips-iconwrap">
                        <span class="f-icon f-icon-warning"></span>
                      </div>
                      <div class="modal-tips-content">
                        <h5 class="toast-title modal-tips-title only-toast-msg">
                          {notify.text}
                        </h5>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </Teleport>
          )}
        </div>
    );
  }
});
