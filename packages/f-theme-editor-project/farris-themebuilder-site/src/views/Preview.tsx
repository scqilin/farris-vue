import { defineComponent } from "vue";

export default defineComponent({
  name: "FTPreview",
  setup() {   
    return () => (
        <router-view/>
    );
  }
});
