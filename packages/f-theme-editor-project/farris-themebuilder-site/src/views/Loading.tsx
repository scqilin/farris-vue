import { Teleport, computed, defineComponent } from "vue";
import { useLoadingStore } from "../store/index";


export default defineComponent({
  name: "FTLoading",
  setup() {
    const store = useLoadingStore();
    return () => {
      return (
        <div>
          {store.show && (
            <Teleport to="body">
              <div class="farris-loading-backdrop loading-wait"></div>
              <div class="farris-loading">
                <div class="ng-busy-default-wrapper">
                  <div class="ng-busy-default-sign">
                    <div class="common-loading--container">
                      <div class="f-loading-dot-wrapper">
                        <div class="f-loading-dot">
                          <div class="dot dot1"></div>
                          <div class="dot dot2"></div>
                          <div class="dot dot3"></div>
                          <div class="dot dot4"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Teleport>
          )}
        </div>
      );
    };
  },
});
