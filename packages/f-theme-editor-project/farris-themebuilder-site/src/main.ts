import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { storeInstance } from "@/store/init";
import Farris from '@farris/ui-vue';
import  '@farris/ui-vue/style.css';

createApp(App).use(storeInstance).use(Farris).use(router).mount("#app");
