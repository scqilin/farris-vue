import { Theme } from "@/common/theme";
import { defineStore } from "pinia";

export const useThemeChangeStore = defineStore("ThemeChange", {
  state: () => {
    return {
      // 当前主题名称
      themeName: "farris",
      // 当前主题大小
      themeSize: "default",
      // 当前主题颜色
      themeColor: "default",
      // // 修改的类型
      // viewType: "preview-page"
    };
  },
  getters: {
    themeUrl(): string {
      return this.themeName + "/" + this.themeColor + "-" + this.themeSize;
    },
    /**
     * 获取主题配置
     * 
     * themeType目前只支持farris主题的配置，此处简化处理
     */
    getThemSettings(themeType = "") {
      let themeSettings = {
        farris: {
          themeColor: [
            {
              type: "default",
              color: "default",
              name: "默认主题"
            },
            {
              type: "green",
              color: "green",
              name: "清新绿"
            },
            {
              type: "red",
              color: "red",
              name: "中国红"
            }
          ],
          themeSize: [
            {
              type: "default",
              size: "default",
              name: "紧凑版"
            },
            {
              type: "loose",
              size: "loose",
              name: "宽松版"
            }
          ]
        }
      };
      // if (themeType == "") {
      //   themeType = this.themeName;
      // }
      return themeSettings['farris'];
    },
    /**
     * 获取当前主题
     * @returns 
     */
    currentTheme():Theme{
      return {
        name: this.themeName,
        colorScheme: this.themeColor + "-" + this.themeSize
      }
    }
  },
  actions: {
    // 更新主题名称
    updateThemeName(str: string) {
      if (str != this.themeName) {
        this.themeName = str;
        //this.updateFormatUrl();
      }
    },
    //更新主题大小
    updateThemeSize(str: string) {
      if (str != this.themeSize) {
        this.themeSize = str;
        // this.updateFormatUrl();
      }
    },
    updateThemeColor(str: string) {
      if (str != this.themeColor) {
        this.themeColor = str;
        // this.updateFormatUrl();
      }
    },
    // // 更新预览类型
    // updateViewType(str: string) {
    //   if (str != this.viewType) {
    //     this.viewType = str;
    //     // this.updateFormatUrl();
    //   }
    // }

    //  updateFormatUrl() {
    //     let tUrl = '' + this.themeName + '/' + this.themeColor + '-' + this.themeSize;
    //     this.urlChangeState.next(tUrl);
    //   },
    //   getFormatUrl() {
    //     let tUrl = this.themeName + '/' + this.themeColor + '-' + this.themeSize;
    //     return tUrl;
    //   }
  }
});
