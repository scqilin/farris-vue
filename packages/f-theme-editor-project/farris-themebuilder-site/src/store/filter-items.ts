import { MenuItem } from "@/common/left-menu-item";
import { filterNameUtils } from "@/utils/filter-name";
import { SettingsAliasUtils } from "@/utils/settings-alias";
import { themeMetadataUtils } from "@/utils/theme-metadata";
import { defineStore } from "pinia";
/**
 * 处理根据关键字过滤变量相关
 */
export const useFilterMetaItemsStore = defineStore("filterMetaItems", {
  state: () => {
    return {
      filterDatas: [] as Array<MenuItem>,
      navDatas: [] as Array<MenuItem>
    };
  },
  actions: {
    addFilteredMenuItem(
      keyword: string,
      item: MenuItem,
      itemsArray: MenuItem[]
    ): void {
      // 当前项如果名称包含在其中,则取他的item构造结果
      if (item.name.toLowerCase().indexOf(keyword) >= 0) {
        itemsArray.push(item);
        return;
      }

      if (!item.items) return;
      const {getRealName} =filterNameUtils();
      const filteredItems = item.items.filter((metaItem) => {
        const itemName = getRealName(metaItem.Name).toLowerCase();
        return itemName.indexOf(keyword) >= 0;
      });

      if (filteredItems.length) {
        itemsArray.push({
          name: item.name,
          items: filteredItems,
          route: item.route
        });
      }
    },
    menuFilter(searchText: string) {
      const {getRealName} =filterNameUtils();
      const keyword = getRealName(searchText.toLowerCase());
      // 如果没有查询
      if (!keyword.length) {
        this.filterDatas = [...this.navDatas];
        return;
      }
      this.filterDatas = [];
      // 归集数据后的所有的变量集合 []
      this.navDatas.forEach((navDataItem) => {
        // 先构造items里面的数据
        this.addFilteredMenuItem(keyword, navDataItem, this.filterDatas);

        if (navDataItem.groups) {
          // 如果有分组再构造分组里的数据
          const filteredDataGroups: MenuItem[] = [];

          navDataItem.groups.forEach((group) =>
            this.addFilteredMenuItem(keyword, group, filteredDataGroups)
          );

          if (filteredDataGroups.length) {
            const existingGroup = this.filterDatas.filter(
              (i) => i.name === navDataItem.name
            );
            if (!existingGroup.length) {
              this.filterDatas.push({
                name: navDataItem.name,
                groups: filteredDataGroups,
                route: navDataItem.route
              });
            }
          }
        }
      });
    },
    /**
     * 更新数据
     */
    updateNavDatas() {
      // 获取Utils
      const { formatSettings, getRapidSettings } = SettingsAliasUtils();
      const {getBaseParameters} =themeMetadataUtils();
      // 获取基础变量的定义
      let presetRapidSettings = getRapidSettings();
      // 查找配置，构造需要几个主题的基础变量
      let needThemes = ["farris"];
      // 整理主题类型
      presetRapidSettings.forEach((item) => {
        let equThemes = (item.equivalents || "").split(",");
        equThemes.forEach((themeItem) => {
          if (
            needThemes.findIndex(
              (needThemeItem) => themeItem == needThemeItem
            ) < 0
          ) {
            needThemes.push(themeItem);
          }
        });
      });

      // 获取基础变量
      getBaseParameters(needThemes.join(",")).then((commonParameters) => {
          //赋值 形如，[{"Key":"$base-accent","Name":"10. Accent color","Type":"color","Value":"[object Object]"} , ...]
          // baseParameters.forEach(item => {
          //   // 目前出现赋值不对的问题，先临时补偿一下
          //   if (item.Type == 'color' && item.Value == "[object Object]") {
          //     item.Value = "rgba(0,0,0,0.25)";
          //   }
          // });
          debugger;
          console.log("基础变量");
          // let tParameters = [
          //   {
          //     "Key": "$f-theme-01",
          //     "Name": "10. 収折颜色",
          //     "Type": "color",
          //     "Value": "rgba(0,0,0,0.25)"
          //   },
          //   {
          //     "Key": "$f-aid-01",
          //     "Name": "20. 辅助色",
          //     "Type": "color",
          //     "Value": "rgba(0,0,0,0.25)"
          //   },
          //   {
          //     "Key": "$f-ornament-01",
          //     "Name": "30. 点缀色",
          //     "Type": "color",
          //     "Value": "rgba(0,0,0,0.25)"
          //   },
          //   {
          //     "Key": "$f-neutral-01",
          //     "Name": "40. 中性色",
          //     "Type": "color",
          //     "Value": "rgba(0,0,0,0.25)"
          //   }
          // ];
          let datas = formatSettings(presetRapidSettings, commonParameters);
          // 更新nav数据
          this.navDatas = datas;
          // 赋初始值
          this.filterDatas = [...datas];
        });
    }
  }
});
