import { defineStore } from "pinia";
import { Theme } from "@/common/theme";
import { Metadata } from "@/common/metadata";
import { useThemeChangeStore } from "./theme-change";
import {useFilterMetaItemsStore} from "./filter-items";
import { ExportedItem } from "@/common/exported-item";
import { MetaItem } from "@/common/meta-item";

export const useThemeMetadataStore = defineStore("themeMetadata", {
  state: () => {

    let metadata: Metadata | any = null;
    // 修改的变量的合集
    let modifiedCollection: ExportedItem[] = [];
    // 修改的变量类型是否快速配置
    let modifiedIsRapid = true;
    // 标记主题的赋值之后的原始值
    let originalMetadata: MetaItem[]=[];
    return {
      metadata,
      modifiedCollection,
      modifiedIsRapid,
      originalMetadata
    };
  },
  actions: {
    updateMetadata(meta: Metadata | any) {
      // this.show =loadingState;
      this.metadata = meta;
    },
    updateMetadataByThemeName(meta: Metadata) {
      let themeChange = useThemeChangeStore();
      this.metadata[themeChange.currentTheme['name']] = meta;
      const filterMetaItems= useFilterMetaItemsStore();
      filterMetaItems.updateNavDatas();
    },
    /**
     * 修改项
     */
    updateModified(datas: Array<ExportedItem>) {
      this.modifiedCollection = [...datas];
    },
    /**
     * 更新变量类型是否是快速配置
     * @param isRapid 
     */
    updateIsRapid(isRapid:boolean){
      this.modifiedIsRapid = isRapid;
    },
    /**
     * 保存原始值
     * @param datas 
     */
    updateOriginalMetadata(datas:MetaItem[]){
      this.originalMetadata=datas;
    }
  }
});
