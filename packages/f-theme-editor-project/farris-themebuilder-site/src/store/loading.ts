import { defineStore } from "pinia";

export const useLoadingStore = defineStore("loading", {
  state: () => {
    return {
      show: false,
    };
  },
  actions:{
    updateState(loadingState:boolean){
      this.show =loadingState;
    }
  }
});
