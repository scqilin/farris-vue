import { BuilderConfig } from "@/common/builder-config";
import api from "./api";
import { BuilderResult } from "@/common/builder-result";
import { Metadata } from "@/common/metadata";

/**
 * 根据配置信息，发起请求获取当前主题当前颜色下的具体定义
 * 比如 {"makeSwatch":false,"items":[],"widgets":[],"noClean":true}
 *  其中items是修改变量的集合 [{"key":"$base-border-radius","value":"8"}]
 * @param configs 
 * @returns 
 */
export function buildThemeAPI(configs:BuilderConfig):Promise<BuilderResult>{
    return api.post('/buildtheme', configs);
}
/**
 * 获取元数据
 
    * 获取元数据信息
    * 元数据的返回结构形如
    * generic和material两类主题
    * {
    *  baseParameters:["@base-accent",...],
    *  generic:[{
    *          Key:"$accordion-color",
    *          Name:"50. Text color",
    *          Type:"color"
   *        },
   *        {
   *           Key:"$base-border-radius-small",
   *           Name:"70. Border radius", 这个70不知道何用
   *           Type:"text"  
   *      }
    * ],
    *  material:[结构同generic], 
    *  themes:[{
    *          themeId:1,
    *          name:"generic",
    *          colorScheme:"light",
    *          text:"Light",
    *          group:"Generic"
    *      },
    *      {
    *          themeId:26,
    *          name:"material",
    *          colorScheme:"purple-light",
    *          text:"Material Design",
    *          group:"Material Design"
    *      }
    *  ],
    *  version:"21.2.3"
    * }

 * @returns 
 */
export function getMetadataAPI():Promise<Metadata>{
    return api.get('/metadata');
}