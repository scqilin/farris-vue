
  // 标记导航数据
//   private navDatas = new BehaviorSubject<MenuItem[]>([]);
//   private filterDatas= new BehaviorSubject<MenuItem[]>([]);

export function filterNameUtils(){

  /** 01. 说明文字 经过转化后变成 =》 说明文字 */
 const getRealName =(orderedName:string): string => {
     const ORDER_REGEX = /^(\d+).\s/;
    return orderedName.replace(ORDER_REGEX, '');
  }

  /**标黄 */
  const hightlightedName=(orderedName:string, searchText:string): string=>{
    const text = getRealName(orderedName);
    if (!searchText) return text;
    return text.replace(new RegExp(`(${searchText})`, 'ig'), '<span style="color:#f05b41">$1</span>'); 
  }

  /**排序 */
  const sortNames=(name1:string, name2:string): number =>{
    const redix = 10;
    return Number.parseInt(name1, redix) - Number.parseInt(name2, redix);
  }

return{hightlightedName,sortNames,getRealName}

}
