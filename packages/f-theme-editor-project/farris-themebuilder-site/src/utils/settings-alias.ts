import { MenuItem } from "@/common/left-menu-item";
import { MetaItem, ThemeMetaItem } from "@/common/meta-item";
import { filterNameUtils } from "./filter-name";

export function SettingsAliasUtils() {
    const rapidSettings: MenuItem[] = [
        {
            name: '快速设置',
            code: 'rapid-settings',
            equivalents: 'farris,framework',
            route: '',
            groups: [
                {
                    name: '字号、圆角',
                    code: 'font-size',
                    equivalents: '',
                    isFold: true,
                    regs: {
                        farris: [/farris-font/i, /farris-border-radius/i, /f-pt-split-section/i],
                        framework: [/theme01/i]
                    }
                }, {
                    name: '主题色',
                    code: 'theme-color',
                    equivalents: '',
                    isFold: true,
                    regs: { farris: [/f-theme-03/i,/f-text-02/i,/f-semantic-info-01/i,/f-semantic-submit-01/i,/f-semantic-success-01/i,/f-semantic-warning-01/i,/f-semantic-danger-01/i] }
                },
                // {
                //     name: '辅助色',
                //     code: 'aid-color',
                //     isFold: true,
                //     equivalents: '',
                //     regs: [/f-aid-/i]
                // }, {
                //     name: '点缀色',
                //     code: 'ornament-color',
                //     equivalents: '',
                //     isFold: true,
                //     regs: [/f-ornament-/i]
                // }, {
                //     name: '中性色',
                //     code: 'neutral-color',
                //     equivalents: '',
                //     isFold: true,
                //     regs: [/f-neutral-/i]
                // }, {
                //     name: '文字色',
                //     code: 'text-color',
                //     equivalents: '',
                //     isFold: true,
                //     regs: [/f-text-/i]
                // }, {
                //     name: '语义色',
                //     code: 'semantic-color',
                //     equivalents: '',
                //     isFold: true,
                //     regs: [/f-semantic-/i]
                // }
            ]
        }
    ];

    const getRapidSettings= ():Array<MenuItem>=>{
        return rapidSettings;
    }
    /**
     * 
     * @param dataAlias 
     * @param metadata 
     */
    const formatSettings=(dataAlias:Array<MenuItem>, metadata:ThemeMetaItem):Array<MenuItem>=>  {
        // [].concat(dataAlias)
        let menuData = [...dataAlias];
        const {sortNames}=filterNameUtils();

        const fillItems = (menuItem: MenuItem): void => {
            // 进入之前 {"name":"Basic Settings","regs":[{规则},{},{}],"route":"base.common"}
            if (menuItem.regs) {
                menuItem.items = [];
                for (let themeKey in menuItem.regs) {
                    menuItem.regs[themeKey].forEach((regex) => {
                        Array.prototype.push.apply(menuItem.items, getMatchedItems(themeKey,regex));
                    });
                }
                // 数据再处理
                menuItem.items.forEach((metaItem: MetaItem) => {
                    if (metaItem.TypeValues) {
                        // 比如 normal|bold|light
                        metaItem.TypeValuesArray = metaItem.TypeValues.split('|');
                    }
                });
                // 内部排序
                menuItem.items.sort((item1, item2) => sortNames(item1.Name, item2.Name));
            }
        };
        const getMatchedItems = (themeName:string,regex: RegExp): MetaItem[] => {
            // 所有变量里，存在Name并且key符合规则
            return metadata[themeName].filter((value) => value.Name && regex.test(value.Key));
        };

        menuData.forEach((item) => {
            // 填充menuData项的Items
            fillItems(item);
         
                /**
                 * {"name":"Button",
                 *  "equivalents":"dxButton, dx-button",
                 *  "route":"buttons",
                 * "groups":[
                 *  {"name":"Default Type","equivalents":"dxButton, dx-button","regs":[{}]},
                 *  {"name":"Normal Type","equivalents":"dxButton, dx-button","regs":[{}]},
                 *  {"name":"Success Type","equivalents":"dxButton, dx-button","regs":[{}]},
                 *  {"name":"Danger Type","equivalents":"dxButton, dx-button","regs":[{}]}
                 * ]
                 * }
                 * 内部有分组，分组内会有规则，在构成Item
                 */
                (item?.groups||[]).forEach((groupItem) => fillItems(groupItem));
           
        });
        return menuData;
    }
    return {
        formatSettings,getRapidSettings
    }
}
