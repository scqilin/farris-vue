import { ThemeSettingItem } from "@/common/theme";
import { useThemeChangeStore } from "@/store";
import { useRoute } from "vue-router";
import { StorageThemeCSSUtils } from "./storage-theme-css";

export function PreviewUtils() {
    /**
     * 预览页初始化
     * @returns 
     */
  const initPreview = () => {
    const themeChange = useThemeChangeStore();
    let themeInfo: ThemeSettingItem = {
      name: themeChange.themeName,
      color: themeChange.themeColor,
      size: themeChange.themeSize,
      type: ""
    };
    // 使用路由
    const route = useRoute();
    console.log("预览页面");
    themeInfo.name = route.params["themeName"] as string;
    const tColorTheme = (route.params["themeColorSize"] as string).split("-");
    if (tColorTheme.length > 1) {
      themeInfo.color = tColorTheme[0];
      themeInfo.size = tColorTheme[1];
    } else {
      themeInfo.color = tColorTheme[0];
      themeInfo.size = "default";
    }
    return themeInfo;
  };
  /**
   * 处理预览页上的主题样式
   * @param themeInfo 
   */
  const handleLinks = (themeInfo: ThemeSettingItem) => {
    // 处理farris
    addHeadLinks(themeInfo, "dynamic-farris", "farrisThemeLink");
    // 处理framework
    addHeadLinks(themeInfo, "dynamic-framework", "frameworkThemeLink");
  };
  /**
   * 追加通过变量生成的样式
   * <style type="text/css" id="dynamic-styles">
   * </style>
   * @param css
   */
  const addHeadStyles = (themeName: string): void => {
    const head = document.getElementsByTagName("head")[0];
    const style = document.createElement("style");
    const DYNAMIC_STYLES_ID = "dynamic-styles-" + themeName;
    let storedCSS = StorageThemeCSSUtils().getCSS();

    const dynamicStylesElement = document.getElementById(DYNAMIC_STYLES_ID);

    if ((!storedCSS && dynamicStylesElement) || dynamicStylesElement) {
      dynamicStylesElement.parentNode?.removeChild(dynamicStylesElement);
    }
    if (storedCSS) {
      console.log("存储了css");
      style.type = "text/css";
      style.id = DYNAMIC_STYLES_ID;
      // 可以再次调整已生成的样式
      // css = css
      //   .replace(//gi, 'content/css/icons/dxicons')

      style.appendChild(document.createTextNode(storedCSS));
      head.appendChild(style);
      // 当样式已经加载成功
    }
  };




  function insterAfter(newElement: HTMLElement, targetElement: any) {
    const parent = targetElement?.parentNode;
    if (parent?.lastChild === targetElement) {
      parent.appendChild(newElement);
    } else {
      parent?.insertBefore(newElement, targetElement?.nextSibling);
    }
  }

  /**
   * 添加link比追加css方式节省性能
   */
  function addHeadLinks(
    themeInfo: ThemeSettingItem,
    linkId: string,
    insertAfterId: string
  ): void {
    if (
      themeInfo.color !== "default" ||
      (themeInfo.size !== "default" && linkId === "dynamic-farris")
    ) {
      const head = document.getElementsByTagName("head")[0];
      const linkEl = document.createElement("link");
      const DYNAMIC_LINKS_ID = linkId;
      // 应该不存在这种场景
      const dynamicLinksElement = document.getElementById(DYNAMIC_LINKS_ID);
      if (dynamicLinksElement) {
        dynamicLinksElement.parentNode?.removeChild(dynamicLinksElement);
      }
      linkEl.rel = "stylesheet";
      linkEl.id = DYNAMIC_LINKS_ID;
      if (linkId === "dynamic-framework") {
        linkEl.href = `./static/framework/themes/${themeInfo.color}/gsp-cloud-web.css`;
      } else {
        linkEl.href = `./static/farris/${themeInfo.color}/${themeInfo.size}/farris-all.css`;
      }
      insterAfter(linkEl, head.querySelector("#" + insertAfterId));
    }
  }
  return {
    initPreview,
    handleLinks,
    addHeadStyles
  };
}
