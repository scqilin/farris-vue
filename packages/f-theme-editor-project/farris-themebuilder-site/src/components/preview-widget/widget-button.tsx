import { defineComponent } from "vue";

export default defineComponent({
  name: "FTWidgetButton",
  setup() {   
    return () => (
      <>
       <button class="btn btn-primary mx-2">Primary</button>
       <button class="btn btn-info mx-2">Info</button>
       <button class="btn btn-danger mx-2">Danger</button>
       <button class="btn btn-success mx-2">Success</button>
      </>
    );
  }
});
