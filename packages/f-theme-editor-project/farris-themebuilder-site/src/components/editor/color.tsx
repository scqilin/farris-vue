import {
  defineComponent,
  toRefs
} from "vue";
import {
  EditorChildProps,
  editorChildProps,
  useEditorChildEvent
} from "./editor.types";

export default defineComponent({
  name: "FTColor",
  props: editorChildProps,
  inheritAttrs: false,
  emits: ["change"],
  setup(props:EditorChildProps, ctx) {
    const colorPreset=['#F44336','#E91E63','#9C27B0','#673AB7','#3f51b5','#2196f3','#03a9f4','#00bcd4','#009688','#4caf50','#8bc34a','#cddc39','#ffeb3b','#ffc107','#ff9800','#ff5722  ','#9e9e9e'];
    const { modelValue, disable } = toRefs(props);
    //  const input = shallowRef<HTMLInputElement>();
    const onChange = (e:any) => {
      console.log(e);
      ctx.emit("change", e.elementValue._value);
    };
    /**
     * 这个颜色选择器，当值变更的时候，并不更新--------------待处理
     */
    return () => (
      <f-color-picker class="w-100" color={modelValue.value} presets={colorPreset} disabled={disable.value} onValueChanged={onChange}></f-color-picker>
    );
  }
});
