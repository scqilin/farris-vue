import { SetupContext, computed, defineComponent, toRefs } from "vue";
import FTInput from "./text";
import FTColor from "./color";
import { EditorProps, editorProps } from "./editor.types";
import { filterNameUtils } from "../../utils/filter-name";
import { themeMetadataUtils } from "@/utils/theme-metadata";

export default defineComponent({
  name: "FTEditor",
  props: editorProps,
  components: { "ft-input": FTInput,"ft-color":FTColor },
  setup(props: EditorProps, ctx: SetupContext) {
   const { updateSingleVariable } = themeMetadataUtils();
    const { hightlightedName } = filterNameUtils();
    const { item } = toRefs(props);
    const horizontalClass = computed(() =>
      hightlightedName(props.item.Name, props.searchText)
    );

    const inputValueChange = ($event: string) => {
      console.log("属性：" + props.item.Key + "修改后值:" + $event);
      updateSingleVariable({
        changeValue: $event,
        controlKey: props.item.Key,
        variableType: props.variableType
      });
    };
    // updateSingleVariable({ changeValue: e, controlKey: key, variableType: this.variableType });
    /**
     * 没有区分展示类型：下拉，颜色，文本 ---todo
     */

    return () => (
      <div class="tool-editor">
        
        <div class="variables-header" innerHTML={horizontalClass.value}></div>
        <div class="variables-component">
         {item.value.Type =='color'&&item.value.Value&&(<ft-color v-model={item.value.Value} onChange={inputValueChange} ></ft-color>)} 
         {item.value.Type =='text'&&item.value.Value&&(<ft-input v-model={item.value.Value} onChange={inputValueChange} ></ft-input>)}          
        </div>
      </div>
    );
  }
});
