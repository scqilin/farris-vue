import { defineComponent, ref } from "vue";
import { DownloadThemeUtils } from "@/utils/download-theme";
import { themeMetadataUtils } from "@/utils/theme-metadata";
/**
 *  未添加
 */
export default defineComponent({
  name: "FTNavHeader",
  props: {},
//   emits:['buildTheme'],
  setup(props, ctx) {
    // 显示下拉面板
   const showDownloadDP=ref(false);
  // 下载主题
  const {downloadZipByTye}=DownloadThemeUtils();
  const {resetVariables,buildAll}=themeMetadataUtils();
     /**
     * 下载下拉面板
     * @param ev
     */
   const toggleDropdown=(ev:MouseEvent,showOrHide='')=>{
       ev&& ev.stopPropagation();
        switch(showOrHide){
            case 'show':
                showDownloadDP.value=true;
                break;
            case 'hide':
                showDownloadDP.value=false;
                break;
            default:
                showDownloadDP.value = !showDownloadDP.value;
        }        
    };
    /**
     * 下载不同压缩包
     * @param ev 
     */
    const downloadZip=(ev:MouseEvent,type='')=>{
        ev&& ev.stopPropagation();
        showDownloadDP.value=false;
        downloadZipByTye(type);
    };
    /**
     * 重置已修改的变量
     */
    const reset=(ev:MouseEvent)=>{
        ev&& ev.stopPropagation();
        resetVariables();
    }
    /**
     * 刷新界面：根据原始变量生成主题信息和样式
     */
    const refresh=(ev:MouseEvent)=>{
        ev&& ev.stopPropagation();
        buildAll();
    }

    return () => (
        <div class="settings-header--container">
        <div class="mr-auto">
            <button type="button" class="btn btn-primary mr-auto" onClick={(ev)=>toggleDropdown(ev,'')}>下载</button>
            <div
                class={["download-dropdown--wrapper d-none", showDownloadDP.value?"d-block":"d-none"]}
                onMouseleave={(ev)=>toggleDropdown(ev,'hide')}
            >
                <ul>
                    <li class="download-dropdown-item" onClick={(ev)=>{toggleDropdown(ev,'hide');downloadZip(ev,'farris')}}>
                        <i class="f-icon f-icon-download"></i>
                        <span>下载当前Faris主题</span>
                    </li>
                    <li class="download-dropdown-item" onClick={(ev)=>downloadZip(ev,'framework')}>
                        <i class="f-icon f-icon-download"></i>
                        <span>下载当前Framework主题</span>
                    </li>
                    <li class="download-dropdown-item" onClick={(ev)=>downloadZip(ev,'')}>
                        <i class="f-icon f-icon-download"></i>
                        <span>下载当前Farris和Framework主题</span>
                    </li>
                </ul>
            </div>
        </div>
        <button type="button" class="btn btn-secondary" title="重置变量" onClick={(ev)=>reset(ev)}>
            {/* { current == 'extend' ? '清空CSS' : '重置变量' } */}
            重置变量
        </button>
        <button type="button" class="btn btn-primary" onClick={refresh} title="刷新预览">刷新</button>
    </div>
    
    );
  }
});
