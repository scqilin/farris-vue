import { MenuItem } from "@/common/left-menu-item";
import { useBuildCSSStore, useFilterMetaItemsStore } from "@/store/index";
import { useThemeChangeStore } from "@/store/index";
import {  computed, defineComponent, ref, watch } from "vue";
import FTEditor from "../editor/editor";
import editor from "../editor/editor";

/**
 *
 * 遇到问题
 * 1、MenuItem的groups 类型取属性变成了大写？
 * 2、多层for循环，括号和大括号嵌套问题
 */

export default defineComponent({
  name: "FTNavRapidSettings",
  props: {},
  components: { "ft-editor": FTEditor },
  setup() {
    // 实例化容器 处理主题变更
    const themeChange = useThemeChangeStore();
    // 过滤数据项
    const filterMetaItems = useFilterMetaItemsStore();

    // const test001=ref(1);
    // const modifiedNavDatas=computed(()=>{
    //   console.log('数据变更');
    //   test001.value++;
    //   return filterMetaItems.filterDatas;
    // });
    // 变更css
    const buildCSS=useBuildCSSStore();
    // 更新数据
    filterMetaItems.updateNavDatas();
  
    // 更新
    /**
     * 判断収折
     * @param groupInfoIndex
     * @param groupIndex
     */
    const foldOrExpandGroup = (groupInfoIndex: number, groupIndex: number) => {
      let groupInfos = filterMetaItems.filterDatas[groupInfoIndex] as MenuItem;
      let currentGroup = (groupInfos.groups || [])[groupIndex];
      if (!currentGroup || !currentGroup.items) {
        return false;
      }
      if (currentGroup.items.length <= 3) {
        return false;
      }
      currentGroup.isFold = !currentGroup.isFold;
    };
    /**
     * css 每次变更获取最新的变量
     */
    // this.subscription = this.metadataService.css.subscribe(({ css, theme }) => {
    //   this.updateData();
    // });
    // watch(
    //   () => buildCSS.change,
    //   (count, prevCount) => {
    //     console.log("--------------变更"+count);
    //   }
    // )

    return () => (
      <div class="nav-rapid-settings">
        <div class="settings-group">
          <div class="group-header">
            <span class="group-header--text">主题色</span>
          </div>
          <div class="group-items">
            {themeChange.getThemSettings ? (
              <ul class="rapid-settings--theme-color-list">
                {themeChange.getThemSettings["themeColor"].map(
                  (themeColorItem) => {
                    return (
                      <li
                        class={[
                          themeChange.themeColor == themeColorItem.type
                            ? "f-state-selected"
                            : ""
                        ]}
                        onClick={(ev) => {
                          themeChange.updateThemeColor(themeColorItem.type);
                        }}
                      >
                        <span
                          class={[
                            "color-item",
                            "color-" + themeColorItem.color
                          ]}
                          title={themeColorItem.name}
                        ></span>
                        <span class="f-icon f-icon-check"></span>
                      </li>
                    );
                  }
                )}
              </ul>
            ) : null}
          </div>
        </div>
        <div class="settings-group">
          <div class="group-header">
            <span class="group-header--text">密度</span>
          </div>
          <div class="group-items">
            {themeChange.getThemSettings ? (
              <ul class="rapid-settings--theme-density-list">
                {themeChange.getThemSettings["themeSize"].map(
                  (themeSizeItem) => {
                    return (
                      <li>
                        <div
                          class={[
                            "density-item",
                            "density-" + themeSizeItem.size,
                            themeChange.themeSize == themeSizeItem.type
                              ? "f-state-selected"
                              : ""
                          ]}
                          onClick={(ev) => {
                            themeChange.updateThemeSize(themeSizeItem.type);
                          }}
                        >
                          <span class="desity-imgcontainer"></span>
                          <span class="desity-icon-wrapper">
                            <i class="f-icon f-icon-check"></i>
                          </span>
                        </div>
                        <span class="density-text">{themeSizeItem.name}</span>
                      </li>
                    );
                  }
                )}
              </ul>
            ) : null}
          </div>
        </div>
        {filterMetaItems.filterDatas.map((groupInfos, groupInfosIndex) => {
        return (
          (groupInfos?.groups || []).map((group, groupIndex) => {
            return (
              <div class="settings-group">
                <div
                  class="group-header"
                  onClick={(ev) =>
                    foldOrExpandGroup(groupInfosIndex, groupIndex)
                  }
                >
                  <span class="group-header--text">{group.name}</span>
                </div>
                {(group?.items || []).map((editorItem) => {
                  return <div class="group-items">
                    <ft-editor item={editorItem} variableType="'rapidSettings'"></ft-editor>
                  </div>;
                })}
              </div>
            );
          })
        )
        })}
      </div>
    );
  }
});
