/**
 * 导出的接口类
 */
export interface ExportedItem {
    key: string;
    value: string;
}
