import { MetaItem } from "./meta-item";
/**
 * 绑定导航的数据接口类
 */
export interface MenuItem {
    name: string;
    regs?: {
        [themeName:string]: Array<RegExp>
    };
    equivalents?: string;
    groups?: Array<MenuItem>;
    items?: Array<MetaItem>;
    route?: string;
    code?:string;
    isFold?:boolean;
}


