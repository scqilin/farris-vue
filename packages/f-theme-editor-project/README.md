<p align="center">
  <a href="#" target="_blank" rel="noopener noreferrer">
    <img alt="Farris UI Logo" src="../../farris_design.jpg"  style="max-width:50%;">
  </a>
</p>

<h1 align="center">Farris Theme Editor</h1>

<p align="center">Farris Theme Editor 是基于 Farris Design 的主题编辑器。</p>

# Farris 主题编辑器

    Farris主题编辑器：
    1. 提供了可视化配置主题样式界面。零基础开发者可以轻松地选择和配置页面的颜色、字体、布局等元素，无需深入学习CSS代码。
    2. 内置了常用的模板和组件样式。通过刷新界面，可以实时查看模板、组件及界面整体样式。
    3. 支持直接下载CSS样式文件。配置主题颜色后，点击导出，可以自动生成对应的CSS样式文件。

## 1. 主题编辑器页面展示

### 1.1 主题编辑器模板界面

待更新

### 1.2 主题编辑器组件界面

待更新

## 2. 主题编辑器介绍

Farris-theme-editor-project工程目录下主要包括以下3个工程：

1. 主题元数据生成工程：farris-theme-editor（待更新）

2. 服务器端工程：farris-themebuilder-service  

3. 主题编辑器前端站点工程：farris-theme-builder-site （待更新）

   

## 3. 主题编辑器使用步骤

#### 3.1 生成主题元数据（待更新）

1. 在终端打开`主题元数据生成工程（farris-theme-editor）`的`themebuilder`目录，执行

    `npm run farrisTheme`

2. 复制生成后的`farris-themebuilder`文件，生成目录为`farris-theme-editor/themebuilder/farris-themebuilder`.

   拷贝至`服务器端工程(farris-themebuidler-service)`的`node_modules`目录下。

#### 3.2 运行服务器端工程

1. 在终端打开`服务器端工程（farris-themebuilder-service）`目录，执行`npm run farrisTheme`, `localhost:3000`启动

#### 3.3 运行主题编辑器前端站点工程 （待更新）

1. 在终端打开`主题编辑器前端站点工程（farris-theme-builder-site）`目录,执行`npm run farrisTheme`，`localhost:4500`启动
