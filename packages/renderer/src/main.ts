import { createApp } from 'vue';
import Farris from '@farris/ui-vue';
import DynamicView from './components/dynamic-view';
import './style.css';
import '@farris/ui-vue/package/style.css';
import App from './app.vue';

createApp(App).use(Farris).use(DynamicView).mount('#app');
