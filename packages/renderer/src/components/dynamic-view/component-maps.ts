import FAccordion from '@farris/ui-vue/package/accordion';
import FButtonEdit from '@farris/ui-vue/package/button-edit';
import FDataGrid from '@farris/ui-vue/package/data-grid';
import FSection from '@farris/ui-vue/package/section';

const componentMap: Record<string, any> = {};
const componentPropsConverter: Record<string, any> = {};

FAccordion.register(componentMap, componentPropsConverter);
FButtonEdit.register(componentMap, componentPropsConverter);
FDataGrid.register(componentMap, componentPropsConverter);
FSection.register(componentMap, componentPropsConverter);

export { componentMap, componentPropsConverter };
