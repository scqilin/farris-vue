import type { App } from 'vue';
import DynamicView from './dynamic-view.component';

export * from './dynamic-view.props';

export { DynamicView };

export default {
    install(app: App): void {
        app.component(DynamicView.name, DynamicView);
    }
};
