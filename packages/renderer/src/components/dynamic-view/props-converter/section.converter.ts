import { SectionProps } from '@farris/ui-vue';

export default function (sectionSchema: any): SectionProps {
    return {
        'main-title': sectionSchema .title
    };
}
